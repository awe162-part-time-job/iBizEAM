import { Http } from '@/utils';
import { Util } from '@/utils';
import EMApplyServiceBase from './emapply-service-base';


/**
 * 外委申请服务对象
 *
 * @export
 * @class EMApplyService
 * @extends {EMApplyServiceBase}
 */
export default class EMApplyService extends EMApplyServiceBase {

    /**
     * Creates an instance of  EMApplyService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMApplyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}