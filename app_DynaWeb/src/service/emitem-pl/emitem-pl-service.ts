import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemPLServiceBase from './emitem-pl-service-base';


/**
 * 损溢单服务对象
 *
 * @export
 * @class EMItemPLService
 * @extends {EMItemPLServiceBase}
 */
export default class EMItemPLService extends EMItemPLServiceBase {

    /**
     * Creates an instance of  EMItemPLService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPLService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}