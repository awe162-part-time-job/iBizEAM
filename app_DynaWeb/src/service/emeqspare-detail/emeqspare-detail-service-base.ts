import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 备件包明细服务对象基类
 *
 * @export
 * @class EMEQSpareDetailServiceBase
 * @extends {EntityServie}
 */
export default class EMEQSpareDetailServiceBase extends EntityService {

    /**
     * Creates an instance of  EMEQSpareDetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareDetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMEQSpareDetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emeqsparedetail';
        this.APPDEKEY = 'emeqsparedetailid';
        this.APPDENAME = 'emeqsparedetails';
        this.APPDETEXT = 'emeqsparedetailname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emeqsparedetails/${context.emeqsparedetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        if(context.emitem && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        if(context.emeqspare && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparedetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emeqsparedetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emeqsparedetails/${context.emeqsparedetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let res:any = await Http.getInstance().delete(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
        }
            let res:any = await Http.getInstance().delete(`/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emeqsparedetails/${context.emeqsparedetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
            if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparedetails/getdraft`,tempData,isloading);
            res.data.emeqsparedetail = data.emeqsparedetail;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emeqsparedetail) delete tempData.emeqsparedetail;
        if(tempData.emeqsparedetailid) delete tempData.emeqsparedetailid;
        let res:any = await  Http.getInstance().get(`/emeqsparedetails/getdraft`,tempData,isloading);
        res.data.emeqsparedetail = data.emeqsparedetail;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/emeqsparedetails/${context.emeqsparedetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emeqspare && context.emeqsparedetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emeqsparedetails/${context.emeqsparedetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emeqspares/${context.emeqspare}/emeqsparedetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/emeqsparedetails/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMEQSpareDetailServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        if(context.emitem && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        if(context.emeqspare && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emeqspares/${context.emeqspare}/emeqsparedetails/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emeqsparedetails/searchdefault`,tempData,isloading);
    }
}