import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQCheckServiceBase from './emeqcheck-service-base';


/**
 * 维修记录服务对象
 *
 * @export
 * @class EMEQCheckService
 * @extends {EMEQCheckServiceBase}
 */
export default class EMEQCheckService extends EMEQCheckServiceBase {

    /**
     * Creates an instance of  EMEQCheckService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQCheckService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}