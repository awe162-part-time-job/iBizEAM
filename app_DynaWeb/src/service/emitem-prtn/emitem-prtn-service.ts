import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemPRtnServiceBase from './emitem-prtn-service-base';


/**
 * 还料单服务对象
 *
 * @export
 * @class EMItemPRtnService
 * @extends {EMItemPRtnServiceBase}
 */
export default class EMItemPRtnService extends EMItemPRtnServiceBase {

    /**
     * Creates an instance of  EMItemPRtnService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPRtnService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}