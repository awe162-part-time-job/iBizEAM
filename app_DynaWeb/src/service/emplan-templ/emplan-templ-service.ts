import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPlanTemplServiceBase from './emplan-templ-service-base';


/**
 * 计划模板服务对象
 *
 * @export
 * @class EMPlanTemplService
 * @extends {EMPlanTemplServiceBase}
 */
export default class EMPlanTemplService extends EMPlanTemplServiceBase {

    /**
     * Creates an instance of  EMPlanTemplService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanTemplService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}