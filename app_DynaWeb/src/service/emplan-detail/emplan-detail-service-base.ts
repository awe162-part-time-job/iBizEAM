import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 计划步骤服务对象基类
 *
 * @export
 * @class EMPlanDetailServiceBase
 * @extends {EntityServie}
 */
export default class EMPlanDetailServiceBase extends EntityService {

    /**
     * Creates an instance of  EMPlanDetailServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanDetailServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMPlanDetailServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emplandetail';
        this.APPDEKEY = 'emplandetailid';
        this.APPDENAME = 'emplandetails';
        this.APPDETEXT = 'emplandetailname';
        this.APPNAME = 'dynaweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
        if(context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emplandetails/${context.emplandetail}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        if(context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplandetails`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emplandetails`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emplandetails/${context.emplandetail}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
        if(context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().delete(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            return res;
        }
            let res:any = await Http.getInstance().delete(`/emplandetails/${context.emplandetail}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
        if(context.emplan && context.emplandetail){
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emplandetails/${context.emplandetail}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emplandetail) delete tempData.emplandetail;
            if(tempData.emplandetailid) delete tempData.emplandetailid;
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplandetails/getdraft`,tempData,isloading);
            res.data.emplandetail = data.emplandetail;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emplandetail) delete tempData.emplandetail;
        if(tempData.emplandetailid) delete tempData.emplandetailid;
        let res:any = await  Http.getInstance().get(`/emplandetails/getdraft`,tempData,isloading);
        res.data.emplandetail = data.emplandetail;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/emplandetails/${context.emplandetail}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.emplandetail){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emplandetails/${context.emplandetail}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/emplandetails/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/emplandetails/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMPlanDetailServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplans/${context.emplan}/emplandetails/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emplandetails/searchdefault`,tempData,isloading);
    }
}