import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULE_MServiceBase from './planschedule-m-service-base';


/**
 * 计划_按月服务对象
 *
 * @export
 * @class PLANSCHEDULE_MService
 * @extends {PLANSCHEDULE_MServiceBase}
 */
export default class PLANSCHEDULE_MService extends PLANSCHEDULE_MServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_MService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_MService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}