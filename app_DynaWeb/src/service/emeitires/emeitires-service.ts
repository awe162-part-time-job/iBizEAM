import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEITIResServiceBase from './emeitires-service-base';


/**
 * 轮胎清单服务对象
 *
 * @export
 * @class EMEITIResService
 * @extends {EMEITIResServiceBase}
 */
export default class EMEITIResService extends EMEITIResServiceBase {

    /**
     * Creates an instance of  EMEITIResService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEITIResService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}