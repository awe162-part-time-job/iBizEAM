import EMBerthUIServiceBase from './emberth-ui-service-base';

/**
 * 泊位UI服务对象
 *
 * @export
 * @class EMBerthUIService
 */
export default class EMBerthUIService extends EMBerthUIServiceBase {

    /**
     * Creates an instance of  EMBerthUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMBerthUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}