import EMEQLCTGSSUIServiceBase from './emeqlctgss-ui-service-base';

/**
 * 钢丝绳位置UI服务对象
 *
 * @export
 * @class EMEQLCTGSSUIService
 */
export default class EMEQLCTGSSUIService extends EMEQLCTGSSUIServiceBase {

    /**
     * Creates an instance of  EMEQLCTGSSUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTGSSUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}