import EMEQLCTMapUIServiceBase from './emeqlctmap-ui-service-base';

/**
 * 位置关系UI服务对象
 *
 * @export
 * @class EMEQLCTMapUIService
 */
export default class EMEQLCTMapUIService extends EMEQLCTMapUIServiceBase {

    /**
     * Creates an instance of  EMEQLCTMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}