import PLANSCHEDULE_WUIServiceBase from './planschedule-w-ui-service-base';

/**
 * 计划_按周UI服务对象
 *
 * @export
 * @class PLANSCHEDULE_WUIService
 */
export default class PLANSCHEDULE_WUIService extends PLANSCHEDULE_WUIServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_WUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_WUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}