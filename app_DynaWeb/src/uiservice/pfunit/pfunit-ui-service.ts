import PFUnitUIServiceBase from './pfunit-ui-service-base';

/**
 * 计量单位UI服务对象
 *
 * @export
 * @class PFUnitUIService
 */
export default class PFUnitUIService extends PFUnitUIServiceBase {

    /**
     * Creates an instance of  PFUnitUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFUnitUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}