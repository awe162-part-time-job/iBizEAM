import EMEQMaintanceUIServiceBase from './emeqmaintance-ui-service-base';

/**
 * 抢修记录UI服务对象
 *
 * @export
 * @class EMEQMaintanceUIService
 */
export default class EMEQMaintanceUIService extends EMEQMaintanceUIServiceBase {

    /**
     * Creates an instance of  EMEQMaintanceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMaintanceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}