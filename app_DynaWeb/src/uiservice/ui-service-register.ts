/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister {

    /**
     * 所有UI实体服务Map
     *
     * @protected
     * @type {*}
     * @memberof UIServiceRegister
     */
    protected allUIService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载UI实体服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof UIServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                this.allUIService.set('emobjmap', () => import('@/uiservice/emobj-map/emobj-map-ui-service'));
        this.allUIService.set('emeqwl', () => import('@/uiservice/emeqwl/emeqwl-ui-service'));
        this.allUIService.set('planschedule_d', () => import('@/uiservice/planschedule-d/planschedule-d-ui-service'));
        this.allUIService.set('planschedule_w', () => import('@/uiservice/planschedule-w/planschedule-w-ui-service'));
        this.allUIService.set('planschedule_t', () => import('@/uiservice/planschedule-t/planschedule-t-ui-service'));
        this.allUIService.set('emresitem', () => import('@/uiservice/emres-item/emres-item-ui-service'));
        this.allUIService.set('emwork', () => import('@/uiservice/emwork/emwork-ui-service'));
        this.allUIService.set('emeqmpmtr', () => import('@/uiservice/emeqmpmtr/emeqmpmtr-ui-service'));
        this.allUIService.set('planschedule_m', () => import('@/uiservice/planschedule-m/planschedule-m-ui-service'));
        this.allUIService.set('emeqlctmap', () => import('@/uiservice/emeqlctmap/emeqlctmap-ui-service'));
        this.allUIService.set('emeqmp', () => import('@/uiservice/emeqmp/emeqmp-ui-service'));
        this.allUIService.set('emeqkprcd', () => import('@/uiservice/emeqkprcd/emeqkprcd-ui-service'));
        this.allUIService.set('planschedule', () => import('@/uiservice/planschedule/planschedule-ui-service'));
        this.allUIService.set('emdprct', () => import('@/uiservice/emdprct/emdprct-ui-service'));
        this.allUIService.set('emeqkpmap', () => import('@/uiservice/emeqkpmap/emeqkpmap-ui-service'));
        this.allUIService.set('emplan', () => import('@/uiservice/emplan/emplan-ui-service'));
        this.allUIService.set('emeqtype', () => import('@/uiservice/emeqtype/emeqtype-ui-service'));
        this.allUIService.set('emitem', () => import('@/uiservice/emitem/emitem-ui-service'));
        this.allUIService.set('emequip', () => import('@/uiservice/emequip/emequip-ui-service'));
        this.allUIService.set('emdrwgmap', () => import('@/uiservice/emdrwgmap/emdrwgmap-ui-service'));
        this.allUIService.set('emeqmonitor', () => import('@/uiservice/emeqmonitor/emeqmonitor-ui-service'));
        this.allUIService.set('emcab', () => import('@/uiservice/emcab/emcab-ui-service'));
        this.allUIService.set('emrfodemap', () => import('@/uiservice/emrfodemap/emrfodemap-ui-service'));
        this.allUIService.set('emeqlocation', () => import('@/uiservice/emeqlocation/emeqlocation-ui-service'));
        this.allUIService.set('emservice', () => import('@/uiservice/emservice/emservice-ui-service'));
        this.allUIService.set('emitemtype', () => import('@/uiservice/emitem-type/emitem-type-ui-service'));
        this.allUIService.set('emeqkp', () => import('@/uiservice/emeqkp/emeqkp-ui-service'));
        this.allUIService.set('emstorepart', () => import('@/uiservice/emstore-part/emstore-part-ui-service'));
        this.allUIService.set('empodetail', () => import('@/uiservice/empodetail/empodetail-ui-service'));
        this.allUIService.set('emobject', () => import('@/uiservice/emobject/emobject-ui-service'));
        this.allUIService.set('emapply', () => import('@/uiservice/emapply/emapply-ui-service'));
        this.allUIService.set('emeqlctgss', () => import('@/uiservice/emeqlctgss/emeqlctgss-ui-service'));
        this.allUIService.set('emwo_osc', () => import('@/uiservice/emwo-osc/emwo-osc-ui-service'));
        this.allUIService.set('emmachinecategory', () => import('@/uiservice/emmachine-category/emmachine-category-ui-service'));
        this.allUIService.set('emdrwg', () => import('@/uiservice/emdrwg/emdrwg-ui-service'));
        this.allUIService.set('emrfode', () => import('@/uiservice/emrfode/emrfode-ui-service'));
        this.allUIService.set('emstore', () => import('@/uiservice/emstore/emstore-ui-service'));
        this.allUIService.set('emeqdebug', () => import('@/uiservice/emeqdebug/emeqdebug-ui-service'));
        this.allUIService.set('emeqlctfdj', () => import('@/uiservice/emeqlctfdj/emeqlctfdj-ui-service'));
        this.allUIService.set('emwplistcost', () => import('@/uiservice/emwplist-cost/emwplist-cost-ui-service'));
        this.allUIService.set('pfteam', () => import('@/uiservice/pfteam/pfteam-ui-service'));
        this.allUIService.set('pfemppostmap', () => import('@/uiservice/pfemp-post-map/pfemp-post-map-ui-service'));
        this.allUIService.set('emacclass', () => import('@/uiservice/emacclass/emacclass-ui-service'));
        this.allUIService.set('pfcontract', () => import('@/uiservice/pfcontract/pfcontract-ui-service'));
        this.allUIService.set('emeitires', () => import('@/uiservice/emeitires/emeitires-ui-service'));
        this.allUIService.set('emoutput', () => import('@/uiservice/emoutput/emoutput-ui-service'));
        this.allUIService.set('emrfodetype', () => import('@/uiservice/emrfodetype/emrfodetype-ui-service'));
        this.allUIService.set('dynachart', () => import('@/uiservice/dyna-chart/dyna-chart-ui-service'));
        this.allUIService.set('empurplan', () => import('@/uiservice/empur-plan/empur-plan-ui-service'));
        this.allUIService.set('emproduct', () => import('@/uiservice/emproduct/emproduct-ui-service'));
        this.allUIService.set('embrand', () => import('@/uiservice/embrand/embrand-ui-service'));
        this.allUIService.set('pfdept', () => import('@/uiservice/pfdept/pfdept-ui-service'));
        this.allUIService.set('emeqsparedetail', () => import('@/uiservice/emeqspare-detail/emeqspare-detail-ui-service'));
        this.allUIService.set('pfemp', () => import('@/uiservice/pfemp/pfemp-ui-service'));
        this.allUIService.set('emstock', () => import('@/uiservice/emstock/emstock-ui-service'));
        this.allUIService.set('dynadashboard', () => import('@/uiservice/dyna-dashboard/dyna-dashboard-ui-service'));
        this.allUIService.set('emwoori', () => import('@/uiservice/emwoori/emwoori-ui-service'));
        this.allUIService.set('emplandetail', () => import('@/uiservice/emplan-detail/emplan-detail-ui-service'));
        this.allUIService.set('emplantempl', () => import('@/uiservice/emplan-templ/emplan-templ-ui-service'));
        this.allUIService.set('emwo_dp', () => import('@/uiservice/emwo-dp/emwo-dp-ui-service'));
        this.allUIService.set('planschedule_o', () => import('@/uiservice/planschedule-o/planschedule-o-ui-service'));
        this.allUIService.set('emen', () => import('@/uiservice/emen/emen-ui-service'));
        this.allUIService.set('empo', () => import('@/uiservice/empo/empo-ui-service'));
        this.allUIService.set('pfunit', () => import('@/uiservice/pfunit/pfunit-ui-service'));
        this.allUIService.set('emitemprtn', () => import('@/uiservice/emitem-prtn/emitem-prtn-ui-service'));
        this.allUIService.set('emberth', () => import('@/uiservice/emberth/emberth-ui-service'));
        this.allUIService.set('emeqlcttires', () => import('@/uiservice/emeqlcttires/emeqlcttires-ui-service'));
        this.allUIService.set('emitemrout', () => import('@/uiservice/emitem-rout/emitem-rout-ui-service'));
        this.allUIService.set('emeqkeep', () => import('@/uiservice/emeqkeep/emeqkeep-ui-service'));
        this.allUIService.set('emplanrecord', () => import('@/uiservice/emplanrecord/emplanrecord-ui-service'));
        this.allUIService.set('emitemrin', () => import('@/uiservice/emitem-rin/emitem-rin-ui-service'));
        this.allUIService.set('emeqmaintance', () => import('@/uiservice/emeqmaintance/emeqmaintance-ui-service'));
        this.allUIService.set('emitempuse', () => import('@/uiservice/emitem-puse/emitem-puse-ui-service'));
        this.allUIService.set('emeqsetup', () => import('@/uiservice/emeqsetup/emeqsetup-ui-service'));
        this.allUIService.set('emitemtrade', () => import('@/uiservice/emitem-trade/emitem-trade-ui-service'));
        this.allUIService.set('emwo_en', () => import('@/uiservice/emwo-en/emwo-en-ui-service'));
        this.allUIService.set('emrfoac', () => import('@/uiservice/emrfoac/emrfoac-ui-service'));
        this.allUIService.set('emassetclear', () => import('@/uiservice/emasset-clear/emasset-clear-ui-service'));
        this.allUIService.set('emmachmodel', () => import('@/uiservice/emmach-model/emmach-model-ui-service'));
        this.allUIService.set('emassetclass', () => import('@/uiservice/emasset-class/emasset-class-ui-service'));
        this.allUIService.set('emeqspare', () => import('@/uiservice/emeqspare/emeqspare-ui-service'));
        this.allUIService.set('emitempl', () => import('@/uiservice/emitem-pl/emitem-pl-ui-service'));
        this.allUIService.set('emeqah', () => import('@/uiservice/emeqah/emeqah-ui-service'));
        this.allUIService.set('emwo', () => import('@/uiservice/emwo/emwo-ui-service'));
        this.allUIService.set('emeqlctrhy', () => import('@/uiservice/emeqlctrhy/emeqlctrhy-ui-service'));
        this.allUIService.set('emitemcs', () => import('@/uiservice/emitem-cs/emitem-cs-ui-service'));
        this.allUIService.set('emenconsum', () => import('@/uiservice/emenconsum/emenconsum-ui-service'));
        this.allUIService.set('emserviceevl', () => import('@/uiservice/emservice-evl/emservice-evl-ui-service'));
        this.allUIService.set('emplancdt', () => import('@/uiservice/emplan-cdt/emplan-cdt-ui-service'));
        this.allUIService.set('emeqsparemap', () => import('@/uiservice/emeqspare-map/emeqspare-map-ui-service'));
        this.allUIService.set('emoutputrct', () => import('@/uiservice/emoutput-rct/emoutput-rct-ui-service'));
        this.allUIService.set('emrfoca', () => import('@/uiservice/emrfoca/emrfoca-ui-service'));
        this.allUIService.set('emwo_inner', () => import('@/uiservice/emwo-inner/emwo-inner-ui-service'));
        this.allUIService.set('emwplist', () => import('@/uiservice/emwplist/emwplist-ui-service'));
        this.allUIService.set('emrfomo', () => import('@/uiservice/emrfomo/emrfomo-ui-service'));
        this.allUIService.set('emasset', () => import('@/uiservice/emasset/emasset-ui-service'));
        this.allUIService.set('emeqcheck', () => import('@/uiservice/emeqcheck/emeqcheck-ui-service'));
    }

    /**
     * 加载服务实体
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allUIService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const uiServiceRegister: UIServiceRegister = new UIServiceRegister();