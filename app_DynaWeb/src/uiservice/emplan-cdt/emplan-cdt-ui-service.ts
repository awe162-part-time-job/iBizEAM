import EMPlanCDTUIServiceBase from './emplan-cdt-ui-service-base';

/**
 * 计划条件UI服务对象
 *
 * @export
 * @class EMPlanCDTUIService
 */
export default class EMPlanCDTUIService extends EMPlanCDTUIServiceBase {

    /**
     * Creates an instance of  EMPlanCDTUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanCDTUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}