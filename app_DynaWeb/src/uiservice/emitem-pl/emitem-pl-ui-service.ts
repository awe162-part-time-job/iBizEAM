import EMItemPLUIServiceBase from './emitem-pl-ui-service-base';

/**
 * 损溢单UI服务对象
 *
 * @export
 * @class EMItemPLUIService
 */
export default class EMItemPLUIService extends EMItemPLUIServiceBase {

    /**
     * Creates an instance of  EMItemPLUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPLUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}