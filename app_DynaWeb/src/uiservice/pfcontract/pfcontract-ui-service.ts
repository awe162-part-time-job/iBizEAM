import PFContractUIServiceBase from './pfcontract-ui-service-base';

/**
 * 合同UI服务对象
 *
 * @export
 * @class PFContractUIService
 */
export default class PFContractUIService extends PFContractUIServiceBase {

    /**
     * Creates an instance of  PFContractUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFContractUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}