import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import EMItemTypeService from '@/service/emitem-type/emitem-type-service';
import EMItemTypeAuthService from '@/authservice/emitem-type/emitem-type-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import EMItemTypeUIService from '@/uiservice/emitem-type/emitem-type-ui-service';

/**
 * 物品视图基类
 *
 * @export
 * @class EMItemTypeInfoTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class EMItemTypeInfoTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected appDeName: string = 'emitemtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected appDeKey: string = 'emitemtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected appDeMajor: string = 'emitemtypename';

    /**
     * 实体服务对象
     *
     * @type {EMItemTypeService}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected appEntityService: EMItemTypeService = new EMItemTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemTypeUIService
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public appUIService: EMItemTypeUIService = new EMItemTypeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemtype.views.infotreeexpview.caption',
        srfTitle: 'entities.emitemtype.views.infotreeexpview.title',
        srfSubTitle: 'entities.emitemtype.views.infotreeexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: {
            name: 'treeexpbar',
            type: 'TREEEXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
	protected viewtag: string = '3d19af9a5f72a337dfa93c58c0d5f227';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */ 
    protected viewName: string = 'EMItemTypeInfoTreeExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemTypeInfoTreeExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'emitemtype',
            majorPSDEField: 'emitemtypename',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMItemTypeInfoTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTypeInfoTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMItemTypeInfoTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMItemTypeInfoTreeExpView
     */
    public viewUID: string = 'eam-mat-emitem-type-info-tree-exp-view';


}