import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PFTeamService from '@/service/pfteam/pfteam-service';
import PFTeamAuthService from '@/authservice/pfteam/pfteam-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PFTeamUIService from '@/uiservice/pfteam/pfteam-ui-service';

/**
 * 班组数据选择视图视图基类
 *
 * @export
 * @class PFTEAMPickupViewBase
 * @extends {PickupViewBase}
 */
export class PFTEAMPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PFTEAMPickupViewBase
     */
    protected appDeName: string = 'pfteam';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PFTEAMPickupViewBase
     */
    protected appDeKey: string = 'pfteamid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PFTEAMPickupViewBase
     */
    protected appDeMajor: string = 'pfteamname';

    /**
     * 实体服务对象
     *
     * @type {PFTeamService}
     * @memberof PFTEAMPickupViewBase
     */
    protected appEntityService: PFTeamService = new PFTeamService;

    /**
     * 实体权限服务对象
     *
     * @type PFTeamUIService
     * @memberof PFTEAMPickupViewBase
     */
    public appUIService: PFTeamUIService = new PFTeamUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PFTEAMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.pfteam.views.pickupview.caption',
        srfTitle: 'entities.pfteam.views.pickupview.title',
        srfSubTitle: 'entities.pfteam.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PFTEAMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PFTEAMPickupViewBase
     */
	protected viewtag: string = '1a3909429db5bd3f88ce516fdc7554c6';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PFTEAMPickupViewBase
     */ 
    protected viewName: string = 'PFTEAMPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PFTEAMPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PFTEAMPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PFTEAMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'pfteam',
            majorPSDEField: 'pfteamname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTEAMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTEAMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PFTEAMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}