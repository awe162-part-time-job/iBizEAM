import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMPLANRECORDService from '@/service/emplanrecord/emplanrecord-service';
import EMPLANRECORDAuthService from '@/authservice/emplanrecord/emplanrecord-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMPLANRECORDUIService from '@/uiservice/emplanrecord/emplanrecord-ui-service';

/**
 * 触发记录编辑视图视图基类
 *
 * @export
 * @class EMPLANRECORDEditViewBase
 * @extends {EditViewBase}
 */
export class EMPLANRECORDEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected appDeName: string = 'emplanrecord';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected appDeKey: string = 'emplanrecordid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected appDeMajor: string = 'emplanrecordname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMPLANRECORDService}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected appEntityService: EMPLANRECORDService = new EMPLANRECORDService;

    /**
     * 实体权限服务对象
     *
     * @type EMPLANRECORDUIService
     * @memberof EMPLANRECORDEditViewBase
     */
    public appUIService: EMPLANRECORDUIService = new EMPLANRECORDUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMPLANRECORDEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emplanrecord.views.editview.caption',
        srfTitle: 'entities.emplanrecord.views.editview.title',
        srfSubTitle: 'entities.emplanrecord.views.editview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPLANRECORDEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMPLANRECORDEditView
     */
    public toolBarModels: any = {
        tbitem2: { name: 'tbitem2', caption: 'entities.emplanrecord.editviewtoolbar_toolbar.tbitem2.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emplanrecord.editviewtoolbar_toolbar.tbitem2.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */
	protected viewtag: string = '7073c118bddf20ea6f61278f77da89c1';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPLANRECORDEditViewBase
     */ 
    protected viewName: string = 'EMPLANRECORDEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPLANRECORDEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPLANRECORDEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPLANRECORDEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emplanrecord',
            majorPSDEField: 'emplanrecordname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDEditViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem2')) {
            this.toolbar_tbitem2_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPLANRECORDEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem2_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMPLANRECORD");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMPLANRECORDEditViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}