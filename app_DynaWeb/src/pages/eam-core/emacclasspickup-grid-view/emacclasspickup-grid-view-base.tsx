import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMACClassService from '@/service/emacclass/emacclass-service';
import EMACClassAuthService from '@/authservice/emacclass/emacclass-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMACClassUIService from '@/uiservice/emacclass/emacclass-ui-service';

/**
 * 总帐科目选择表格视图视图基类
 *
 * @export
 * @class EMACCLASSPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMACCLASSPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected appDeName: string = 'emacclass';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected appDeKey: string = 'emacclassid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected appDeMajor: string = 'emacclassname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMACClassService}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected appEntityService: EMACClassService = new EMACClassService;

    /**
     * 实体权限服务对象
     *
     * @type EMACClassUIService
     * @memberof EMACCLASSPickupGridViewBase
     */
    public appUIService: EMACClassUIService = new EMACClassUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emacclass.views.pickupgridview.caption',
        srfTitle: 'entities.emacclass.views.pickupgridview.title',
        srfSubTitle: 'entities.emacclass.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */
	protected viewtag: string = '82686d401a6da9cae639f8fd63aa4d0f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMACCLASSPickupGridViewBase
     */ 
    protected viewName: string = 'EMACCLASSPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMACCLASSPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMACCLASSPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMACCLASSPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emacclass',
            majorPSDEField: 'emacclassname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMACCLASSPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMACCLASSPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}