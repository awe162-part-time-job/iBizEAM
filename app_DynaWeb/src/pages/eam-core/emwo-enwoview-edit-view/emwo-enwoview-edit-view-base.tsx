import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_ENService from '@/service/emwo-en/emwo-en-service';
import EMWO_ENAuthService from '@/authservice/emwo-en/emwo-en-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_ENUIService from '@/uiservice/emwo-en/emwo-en-ui-service';

/**
 * 能耗登记工单视图基类
 *
 * @export
 * @class EMWO_ENWOViewEditViewBase
 * @extends {EditViewBase}
 */
export class EMWO_ENWOViewEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected appDeName: string = 'emwo_en';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected appDeKey: string = 'emwo_enid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected appDeMajor: string = 'emwo_enname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_ENService}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected appEntityService: EMWO_ENService = new EMWO_ENService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_ENUIService
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public appUIService: EMWO_ENUIService = new EMWO_ENUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_ENWOViewEditViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_en.views.wovieweditview.caption',
        srfTitle: 'entities.emwo_en.views.wovieweditview.title',
        srfSubTitle: 'entities.emwo_en.views.wovieweditview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */
	protected viewtag: string = '5b0140697252eddda4dbf4e1a66cdc6e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_ENWOViewEditViewBase
     */ 
    protected viewName: string = 'EMWO_ENWOViewEditView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_ENWOViewEditViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_en',
            majorPSDEField: 'emwo_enname',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_ENWOViewEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}