import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditView9Base } from '@/studio-core';
import EMPOService from '@/service/empo/empo-service';
import EMPOAuthService from '@/authservice/empo/empo-auth-service';
import EditView9Engine from '@engine/view/edit-view9-engine';
import EMPOUIService from '@/uiservice/empo/empo-ui-service';

/**
 * 采购订单视图基类
 *
 * @export
 * @class EMPOEditView9Base
 * @extends {EditView9Base}
 */
export class EMPOEditView9Base extends EditView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */
    protected appDeName: string = 'empo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */
    protected appDeKey: string = 'empoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */
    protected appDeMajor: string = 'emponame';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMPOService}
     * @memberof EMPOEditView9Base
     */
    protected appEntityService: EMPOService = new EMPOService;

    /**
     * 实体权限服务对象
     *
     * @type EMPOUIService
     * @memberof EMPOEditView9Base
     */
    public appUIService: EMPOUIService = new EMPOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPOEditView9Base
     */
    protected model: any = {
        srfCaption: 'entities.empo.views.editview9.caption',
        srfTitle: 'entities.empo.views.editview9.title',
        srfSubTitle: 'entities.empo.views.editview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPOEditView9Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMPOEditView9
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.empo.editview9toolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.empo.editview9toolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:1,dataaccaction: 'EDIT', uiaction: { tag: 'OpenEditMode', target: 'SINGLEKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */
	protected viewtag: string = 'a34bb9c8c4dfd43234dce4be299bd300';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOEditView9Base
     */ 
    protected viewName: string = 'EMPOEditView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPOEditView9Base
     */
    public engine: EditView9Engine = new EditView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPOEditView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPOEditView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'empo',
            majorPSDEField: 'emponame',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOEditView9Base
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOEditView9Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOEditView9Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOEditView9Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.OpenEditMode(datas, contextJO,paramJO,  $event, xData,this,"EMPO");
    }

    /**
     * 编辑
     *
     * @param {any[]} args 当前数据
     * @param {any} context 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @param {*} [srfParentDeName] 父实体名称
     * @returns {Promise<any>}
     */
    public async OpenEditMode(args: any[], context:any = {} ,params: any={}, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
    
        xData = $event;
        $event = params;
        params = context;
        let context2: any = {};
        let data: any = {};
 			let parentContext:any = {};
        let parentViewParam:any = {};
        const _args: any[] = this.$util.deepCopy(args);
        const _this: any = this;
        const actionTarget: string | null = 'SINGLEKEY';
        Object.assign(context2, { res_partner: '%id%' });
        Object.assign(params, { id: '%id%' });
        Object.assign(params, { name: '%name%' })
 			if(actionContext.context){
            parentContext = actionContext.context;
        }
        if(actionContext.viewparams){
            parentViewParam = actionContext.viewparams;
        }
        context = UIActionTool.handleContextParam(actionTarget,_args,parentContext,parentViewParam,context);
        data = UIActionTool.handleActionParam(actionTarget,_args,parentContext,parentViewParam,params);
        Object.assign(context,this.context,context);
        if(context && context.srfsessionid){
          context.srfsessionkey = context.srfsessionid;
            delete context.srfsessionid;
        }
        const parameters: any[] = [
            { pathName: 'res_partners', parameterName: 'res_partner' },
        ];
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, context,data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                const _this: any = this;
                if (xData && xData.refresh && xData.refresh instanceof Function) {
                    xData.refresh(args);
                }
                return result.datas;
            });
        }
        const view: any = {
            viewname: 'empoedit-view9-edit-mode', 
            height: 0, 
            width: 0,  
            title: '采购订单', 
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);

    }



    /**
     * 视图加载完毕
     *
     * @protected
     * @memberof EMPOEditView9Base
     */
    protected viewMounted(): void {
        if (this.panelState) {
            this.panelState.subscribe((res:any) => {
                if (Object.is(res.tag,'meditviewpanel')) {
                    if (Object.is(res.action,'save')) {
                        this.viewState.next({ tag:'form', action: 'save', data:res.data});
                    }
                    if (Object.is(res.action,'remove')) {
                        this.viewState.next({ tag:'form', action: 'remove', data:res.data});
                    }
                }
            });
        }
    }


}