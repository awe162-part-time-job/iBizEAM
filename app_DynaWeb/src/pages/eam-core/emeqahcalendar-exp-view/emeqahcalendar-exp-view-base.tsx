import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { CalendarExpViewBase } from '@/studio-core';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import EMEQAHAuthService from '@/authservice/emeqah/emeqah-auth-service';
import CalendarExpViewEngine from '@engine/view/calendar-exp-view-engine';
import EMEQAHUIService from '@/uiservice/emeqah/emeqah-ui-service';

/**
 * 活动历史日历导航视图视图基类
 *
 * @export
 * @class EMEQAHCalendarExpViewBase
 * @extends {CalendarExpViewBase}
 */
export class EMEQAHCalendarExpViewBase extends CalendarExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected appDeName: string = 'emeqah';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected appDeKey: string = 'emeqahid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected appDeMajor: string = 'emeqahname';

    /**
     * 实体服务对象
     *
     * @type {EMEQAHService}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected appEntityService: EMEQAHService = new EMEQAHService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQAHUIService
     * @memberof EMEQAHCalendarExpViewBase
     */
    public appUIService: EMEQAHUIService = new EMEQAHUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMEQAHCalendarExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqah.views.calendarexpview.caption',
        srfTitle: 'entities.emeqah.views.calendarexpview.title',
        srfSubTitle: 'entities.emeqah.views.calendarexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQAHCalendarExpViewBase
     */
    protected containerModel: any = {
        view_calendarexpbar: {
            name: 'calendarexpbar',
            type: 'CALENDAREXPBAR',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHCalendarExpViewBase
     */
	protected viewtag: string = '748b93fda990b371e4d2f30b4fa1f5ed';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQAHCalendarExpViewBase
     */ 
    protected viewName: string = 'EMEQAHCalendarExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQAHCalendarExpViewBase
     */
    public engine: CalendarExpViewEngine = new CalendarExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQAHCalendarExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQAHCalendarExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            calendarexpbar: this.$refs.calendarexpbar,
            keyPSDEField: 'emeqah',
            majorPSDEField: 'emeqahname',
            isLoadDefault: true,
        });
    }

    /**
     * calendarexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHCalendarExpViewBase
     */
    public calendarexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'selectionchange', $event);
    }

    /**
     * calendarexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHCalendarExpViewBase
     */
    public calendarexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'activated', $event);
    }

    /**
     * calendarexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQAHCalendarExpViewBase
     */
    public calendarexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('calendarexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQAHCalendarExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        this.$Notice.warning({ title: '错误', desc: '请添加新建数据向导视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMEQAHCalendarExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof EMEQAHCalendarExpView
     */
    public viewUID: string = 'eam-core-emeqahcalendar-exp-view';


}