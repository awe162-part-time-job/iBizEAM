
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { GridView9Base } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import EMWPListCostAuthService from '@/authservice/emwplist-cost/emwplist-cost-auth-service';
import GridView9Engine from '@engine/view/grid-view9-engine';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 询价单表格视图视图基类
 *
 * @export
 * @class EMWPListCostGridView9Base
 * @extends {GridView9Base}
 */
export class EMWPListCostGridView9Base extends GridView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */
    protected appDeKey: string = 'emwplistcostid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */
    protected appDeMajor: string = 'emwplistcostname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof EMWPListCostGridView9Base
     */
    protected appEntityService: EMWPListCostService = new EMWPListCostService;

    /**
     * 实体权限服务对象
     *
     * @type EMWPListCostUIService
     * @memberof EMWPListCostGridView9Base
     */
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof EMWPListCostGridView9Base
	 */
    protected customViewNavContexts: any = {
        'N_ITEMNAME_EQ': {
            isRawValue: false,
            value: 'itemname',
        },
        'N_ITEMID_EQ': {
            isRawValue: false,
            value: 'itemid',
        }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWPListCostGridView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emwplistcost.views.gridview9.caption',
        srfTitle: 'entities.emwplistcost.views.gridview9.title',
        srfSubTitle: 'entities.emwplistcost.views.gridview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWPListCostGridView9Base
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */
	protected viewtag: string = '8f6ebd50823b9699bb236852630bbe91';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWPListCostGridView9Base
     */ 
    protected viewName: string = 'EMWPListCostGridView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWPListCostGridView9Base
     */
    public engine: GridView9Engine = new GridView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWPListCostGridView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWPListCostGridView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            keyPSDEField: 'emwplistcost',
            majorPSDEField: 'emwplistcostname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostGridView9Base
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostGridView9Base
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostGridView9Base
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostGridView9Base
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWPListCostGridView9Base
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWPListCostGridView9
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emwplistcost;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emitem && true){
            deResParameters = [
            { pathName: 'emitems', parameterName: 'emitem' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emwplistcosts', parameterName: 'emwplistcost' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const _data: any = { w: (new Date().getTime()) };
            Object.assign(_data, data);
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, _data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMWPListCostGridView9
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emitem && true){
            deResParameters = [
            { pathName: 'emitems', parameterName: 'emitem' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emwplistcosts', parameterName: 'emwplistcost' },
        ];
        const _this: any = this;
        const openPopupModal = (view: any, data: any) => {
            let container: Subject<any> = this.$appmodal.openModal(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emwplist-cost-fill-cost-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emwplistcost.views.fillcostview.title'),
        };
        openPopupModal(view, data);
    }


}