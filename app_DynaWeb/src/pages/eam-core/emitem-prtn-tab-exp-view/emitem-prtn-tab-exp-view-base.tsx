import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMItemPRtnService from '@/service/emitem-prtn/emitem-prtn-service';
import EMItemPRtnAuthService from '@/authservice/emitem-prtn/emitem-prtn-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMItemPRtnUIService from '@/uiservice/emitem-prtn/emitem-prtn-ui-service';

/**
 * 还料单分页导航视图视图基类
 *
 * @export
 * @class EMItemPRtnTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMItemPRtnTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected appDeName: string = 'emitemprtn';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected appDeKey: string = 'emitemprtnid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected appDeMajor: string = 'itemname';

    /**
     * 实体服务对象
     *
     * @type {EMItemPRtnService}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected appEntityService: EMItemPRtnService = new EMItemPRtnService;

    /**
     * 实体权限服务对象
     *
     * @type EMItemPRtnUIService
     * @memberof EMItemPRtnTabExpViewBase
     */
    public appUIService: EMItemPRtnUIService = new EMItemPRtnUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMItemPRtnTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emitemprtn.views.tabexpview.caption',
        srfTitle: 'entities.emitemprtn.views.tabexpview.title',
        srfSubTitle: 'entities.emitemprtn.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMItemPRtnTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMItemPRtnTabExpViewBase
     */
	protected viewtag: string = 'add22def704924475ab10c8be6ace5b0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMItemPRtnTabExpViewBase
     */ 
    protected viewName: string = 'EMItemPRtnTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMItemPRtnTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMItemPRtnTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMItemPRtnTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emitemprtn',
            majorPSDEField: 'itemname',
            isLoadDefault: true,
        });
    }


}