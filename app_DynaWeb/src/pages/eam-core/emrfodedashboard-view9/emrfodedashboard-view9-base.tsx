import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { DashboardView9Base } from '@/studio-core';
import EMRFODEService from '@/service/emrfode/emrfode-service';
import EMRFODEAuthService from '@/authservice/emrfode/emrfode-auth-service';
import PortalView9Engine from '@engine/view/portal-view9-engine';
import EMRFODEUIService from '@/uiservice/emrfode/emrfode-ui-service';

/**
 * 现象数据看板视图视图基类
 *
 * @export
 * @class EMRFODEDashboardView9Base
 * @extends {DashboardView9Base}
 */
export class EMRFODEDashboardView9Base extends DashboardView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEDashboardView9Base
     */
    protected appDeName: string = 'emrfode';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEDashboardView9Base
     */
    protected appDeKey: string = 'emrfodeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEDashboardView9Base
     */
    protected appDeMajor: string = 'emrfodename';

    /**
     * 实体服务对象
     *
     * @type {EMRFODEService}
     * @memberof EMRFODEDashboardView9Base
     */
    protected appEntityService: EMRFODEService = new EMRFODEService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFODEUIService
     * @memberof EMRFODEDashboardView9Base
     */
    public appUIService: EMRFODEUIService = new EMRFODEUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMRFODEDashboardView9Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEDashboardView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emrfode.views.dashboardview9.caption',
        srfTitle: 'entities.emrfode.views.dashboardview9.title',
        srfSubTitle: 'entities.emrfode.views.dashboardview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFODEDashboardView9Base
     */
    protected containerModel: any = {
        view_dashboard: {
            name: 'dashboard',
            type: 'DASHBOARD',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEDashboardView9Base
     */
	protected viewtag: string = '7e94985f9897365ba210b5415ddf5c93';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFODEDashboardView9Base
     */ 
    protected viewName: string = 'EMRFODEDashboardView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFODEDashboardView9Base
     */
    public engine: PortalView9Engine = new PortalView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFODEDashboardView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFODEDashboardView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'emrfode',
            majorPSDEField: 'emrfodename',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFODEDashboardView9Base
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }

    /** 
     * 数据看板部件刷新状态
     * 
     * @type {boolean}
     * @memberof EMRFODEDashboardView9Base
     */
    public state: boolean = true;

    /** 
     * 刷新
     * 
     * @memberof EMRFODEDashboardView9Base
     */
    public refresh(args: any){
        this.state = false;
        setTimeout(() => {
            this.state = true;
            this.loadModel();
        }, 0);
    }

}