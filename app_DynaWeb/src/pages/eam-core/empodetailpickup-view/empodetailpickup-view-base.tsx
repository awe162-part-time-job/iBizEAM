import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMPODetailService from '@/service/empodetail/empodetail-service';
import EMPODetailAuthService from '@/authservice/empodetail/empodetail-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMPODetailUIService from '@/uiservice/empodetail/empodetail-ui-service';

/**
 * 订单条目数据选择视图视图基类
 *
 * @export
 * @class EMPODETAILPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMPODETAILPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupViewBase
     */
    protected appDeName: string = 'empodetail';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupViewBase
     */
    protected appDeKey: string = 'empodetailid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupViewBase
     */
    protected appDeMajor: string = 'empodetailname';

    /**
     * 实体服务对象
     *
     * @type {EMPODetailService}
     * @memberof EMPODETAILPickupViewBase
     */
    protected appEntityService: EMPODetailService = new EMPODetailService;

    /**
     * 实体权限服务对象
     *
     * @type EMPODetailUIService
     * @memberof EMPODETAILPickupViewBase
     */
    public appUIService: EMPODetailUIService = new EMPODetailUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPODETAILPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.empodetail.views.pickupview.caption',
        srfTitle: 'entities.empodetail.views.pickupview.title',
        srfSubTitle: 'entities.empodetail.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPODETAILPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupViewBase
     */
	protected viewtag: string = '61a889ee67b58613b54261c8b316e865';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPODETAILPickupViewBase
     */ 
    protected viewName: string = 'EMPODETAILPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPODETAILPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPODETAILPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPODETAILPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'empodetail',
            majorPSDEField: 'empodetailname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPODETAILPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}