import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMApplyService from '@/service/emapply/emapply-service';
import EMApplyAuthService from '@/authservice/emapply/emapply-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMApplyUIService from '@/uiservice/emapply/emapply-ui-service';

/**
 * 外委申请分页导航视图视图基类
 *
 * @export
 * @class EMApplyTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMApplyTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMApplyTabExpViewBase
     */
    protected appDeName: string = 'emapply';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMApplyTabExpViewBase
     */
    protected appDeKey: string = 'emapplyid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMApplyTabExpViewBase
     */
    protected appDeMajor: string = 'emapplyname';

    /**
     * 实体服务对象
     *
     * @type {EMApplyService}
     * @memberof EMApplyTabExpViewBase
     */
    protected appEntityService: EMApplyService = new EMApplyService;

    /**
     * 实体权限服务对象
     *
     * @type EMApplyUIService
     * @memberof EMApplyTabExpViewBase
     */
    public appUIService: EMApplyUIService = new EMApplyUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMApplyTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMApplyTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emapply.views.tabexpview.caption',
        srfTitle: 'entities.emapply.views.tabexpview.title',
        srfSubTitle: 'entities.emapply.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMApplyTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMApplyTabExpViewBase
     */
	protected viewtag: string = 'f2f9d266225351e3a2789bfdc0ebbbc8';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMApplyTabExpViewBase
     */ 
    protected viewName: string = 'EMApplyTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMApplyTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMApplyTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMApplyTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emapply',
            majorPSDEField: 'emapplyname',
            isLoadDefault: true,
        });
    }


}