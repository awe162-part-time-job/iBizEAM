import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMRFOCAService from '@/service/emrfoca/emrfoca-service';
import EMRFOCAAuthService from '@/authservice/emrfoca/emrfoca-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMRFOCAUIService from '@/uiservice/emrfoca/emrfoca-ui-service';

/**
 * 原因数据选择视图视图基类
 *
 * @export
 * @class EMRFOCAPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMRFOCAPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAPickupViewBase
     */
    protected appDeName: string = 'emrfoca';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAPickupViewBase
     */
    protected appDeKey: string = 'emrfocaid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAPickupViewBase
     */
    protected appDeMajor: string = 'emrfocaname';

    /**
     * 实体服务对象
     *
     * @type {EMRFOCAService}
     * @memberof EMRFOCAPickupViewBase
     */
    protected appEntityService: EMRFOCAService = new EMRFOCAService;

    /**
     * 实体权限服务对象
     *
     * @type EMRFOCAUIService
     * @memberof EMRFOCAPickupViewBase
     */
    public appUIService: EMRFOCAUIService = new EMRFOCAUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMRFOCAPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emrfoca.views.pickupview.caption',
        srfTitle: 'entities.emrfoca.views.pickupview.title',
        srfSubTitle: 'entities.emrfoca.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMRFOCAPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAPickupViewBase
     */
	protected viewtag: string = '4dfde07b484fab438363b02dd0348793';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMRFOCAPickupViewBase
     */ 
    protected viewName: string = 'EMRFOCAPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMRFOCAPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMRFOCAPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMRFOCAPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emrfoca',
            majorPSDEField: 'emrfocaname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMRFOCAPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}