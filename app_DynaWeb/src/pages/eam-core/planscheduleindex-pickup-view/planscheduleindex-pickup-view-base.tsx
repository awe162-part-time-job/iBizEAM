import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import PLANSCHEDULEService from '@/service/planschedule/planschedule-service';
import PLANSCHEDULEAuthService from '@/authservice/planschedule/planschedule-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import PLANSCHEDULEUIService from '@/uiservice/planschedule/planschedule-ui-service';

/**
 * 计划时刻设置数据选择视图视图基类
 *
 * @export
 * @class PLANSCHEDULEIndexPickupViewBase
 * @extends {PickupViewBase}
 */
export class PLANSCHEDULEIndexPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected appDeName: string = 'planschedule';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected appDeKey: string = 'planscheduleid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected appDeMajor: string = 'planschedulename';

    /**
     * 实体服务对象
     *
     * @type {PLANSCHEDULEService}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected appEntityService: PLANSCHEDULEService = new PLANSCHEDULEService;

    /**
     * 实体权限服务对象
     *
     * @type PLANSCHEDULEUIService
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public appUIService: PLANSCHEDULEUIService = new PLANSCHEDULEUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.planschedule.views.indexpickupview.caption',
        srfTitle: 'entities.planschedule.views.indexpickupview.title',
        srfSubTitle: 'entities.planschedule.views.indexpickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
	protected viewtag: string = 'be890cca9106c9ff64282f1fbd49cc6e';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */ 
    protected viewName: string = 'PLANSCHEDULEIndexPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'planschedule',
            majorPSDEField: 'planschedulename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof PLANSCHEDULEIndexPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}