import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import EMStoreService from '@/service/emstore/emstore-service';
import EMStoreAuthService from '@/authservice/emstore/emstore-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import EMStoreUIService from '@/uiservice/emstore/emstore-ui-service';

/**
 * 仓库选项操作视图视图基类
 *
 * @export
 * @class EMStoreOptionViewBase
 * @extends {OptionViewBase}
 */
export class EMStoreOptionViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */
    protected appDeName: string = 'emstore';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */
    protected appDeKey: string = 'emstoreid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */
    protected appDeMajor: string = 'emstorename';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMStoreService}
     * @memberof EMStoreOptionViewBase
     */
    protected appEntityService: EMStoreService = new EMStoreService;

    /**
     * 实体权限服务对象
     *
     * @type EMStoreUIService
     * @memberof EMStoreOptionViewBase
     */
    public appUIService: EMStoreUIService = new EMStoreUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMStoreOptionViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMStoreOptionViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstore.views.optionview.caption',
        srfTitle: 'entities.emstore.views.optionview.title',
        srfSubTitle: 'entities.emstore.views.optionview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMStoreOptionViewBase
     */
    protected containerModel: any = {
        view_form: {
            name: 'form',
            type: 'FORM',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */
	protected viewtag: string = '1c33ac2a63e59b33dcf794ef8a874d05';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMStoreOptionViewBase
     */ 
    protected viewName: string = 'EMStoreOptionView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMStoreOptionViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMStoreOptionViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMStoreOptionViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emstore',
            majorPSDEField: 'emstorename',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStoreOptionViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStoreOptionViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStoreOptionViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}