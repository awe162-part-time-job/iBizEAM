/**
 * 资产类别
 *
 * @export
 * @interface EMAssetClass
 */
export interface EMAssetClass {

    /**
     * 资产科目代码
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclasscode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    orgid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    createdate?: any;

    /**
     * RESERVER
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    reserver?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    updateman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    description?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    updatedate?: any;

    /**
     * RESERVER2
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    reserver2?: any;

    /**
     * 资产类别名称
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    emassetclassname?: any;

    /**
     * 固定资产科目分类
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclassgroup?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    createman?: any;

    /**
     * 资产类别标识
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    emassetclassid?: any;

    /**
     * 折旧期
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    life?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    enable?: any;

    /**
     * 资产科目信息
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclassinfo?: any;

    /**
     * 上级科目
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclasspname?: any;

    /**
     * 上级科目代码
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclasspcode?: any;

    /**
     * 上级科目编号
     *
     * @returns {*}
     * @memberof EMAssetClass
     */
    assetclasspid?: any;
}