/**
 * 退货单
 *
 * @export
 * @interface EMItemROut
 */
export interface EMItemROut {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    updateman?: any;

    /**
     * sap传输异常文本
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sapreason1?: any;

    /**
     * 退货单名称
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    emitemroutname?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    batcode?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    wfstate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    createdate?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    price?: any;

    /**
     * 退货单号
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    emitemroutid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    description?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    amount?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    wfinstanceid?: any;

    /**
     * sap传输状态
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sap?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    enable?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    wfstep?: any;

    /**
     * 出库日期
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    createman?: any;

    /**
     * 退货单信息
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    itemroutinfo?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    orgid?: any;

    /**
     * 退货状态
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    tradestate?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    shf?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sapcontrol?: any;

    /**
     * sap传输失败原因
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sapreason?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    psum?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    teamid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    labserviceid?: any;

    /**
     * 发票号
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    civo?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    storepartname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    storename?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    rname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    itemname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    storeid?: any;

    /**
     * 入库单
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    rid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    storepartid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    itemid?: any;

    /**
     * 退货人
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sempid?: any;

    /**
     * 退货人
     *
     * @returns {*}
     * @memberof EMItemROut
     */
    sempname?: any;
}