/**
 * 资产
 *
 * @export
 * @interface EMAsset
 */
export interface EMAsset {

    /**
     * 第几号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    num?: any;

    /**
     * 是否停机
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqisservice?: any;

    /**
     * 使用人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    empname?: any;

    /**
     * 保修日期
     *
     * @returns {*}
     * @memberof EMAsset
     */
    warrantydate?: any;

    /**
     * 内部总成本
     *
     * @returns {*}
     * @memberof EMAsset
     */
    innerlaborcost?: any;

    /**
     * 设备代码
     *
     * @returns {*}
     * @memberof EMAsset
     */
    keyattparam?: any;

    /**
     * 外部总成本
     *
     * @returns {*}
     * @memberof EMAsset
     */
    foreignlaborcost?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    createman?: any;

    /**
     * 集团设备编码
     *
     * @returns {*}
     * @memberof EMAsset
     */
    jtsb?: any;

    /**
     * 使用期限
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqlife?: any;

    /**
     * 最后折旧日期
     *
     * @returns {*}
     * @memberof EMAsset
     */
    lastzjdate?: any;

    /**
     * 使用部门
     *
     * @returns {*}
     * @memberof EMAsset
     */
    deptname?: any;

    /**
     * 资产类别
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assettype?: any;

    /**
     * 资产余值
     *
     * @returns {*}
     * @memberof EMAsset
     */
    now?: any;

    /**
     * 已提折旧
     *
     * @returns {*}
     * @memberof EMAsset
     */
    ytzj?: any;

    /**
     * 报废日期
     *
     * @returns {*}
     * @memberof EMAsset
     */
    disdate?: any;

    /**
     * 资产状态
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetstate?: any;

    /**
     * 资产原值
     *
     * @returns {*}
     * @memberof EMAsset
     */
    originalcost?: any;

    /**
     * 使用部门
     *
     * @returns {*}
     * @memberof EMAsset
     */
    deptid?: any;

    /**
     * 工艺编号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    techcode?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMAsset
     */
    orgid?: any;

    /**
     * 材料费
     *
     * @returns {*}
     * @memberof EMAsset
     */
    materialcost?: any;

    /**
     * 残值
     *
     * @returns {*}
     * @memberof EMAsset
     */
    discost?: any;

    /**
     * 经办人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    rempid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMAsset
     */
    description?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    updateman?: any;

    /**
     * 已使用年限
     *
     * @returns {*}
     * @memberof EMAsset
     */
    usedyear?: any;

    /**
     * 资产名称
     *
     * @returns {*}
     * @memberof EMAsset
     */
    emassetname?: any;

    /**
     * 预计残值
     *
     * @returns {*}
     * @memberof EMAsset
     */
    replacecost?: any;

    /**
     * 设备编号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetequipid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMAsset
     */
    enable?: any;

    /**
     * 使用人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    empid?: any;

    /**
     * 使用年限
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqlifeyear?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetsort?: any;

    /**
     * 管理部门
     *
     * @returns {*}
     * @memberof EMAsset
     */
    mgrdeptid?: any;

    /**
     * 发动机号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    blsystemdesc?: any;

    /**
     * 经办人
     *
     * @returns {*}
     * @memberof EMAsset
     */
    rempname?: any;

    /**
     * 成本中心
     *
     * @returns {*}
     * @memberof EMAsset
     */
    costcenterid?: any;

    /**
     * 采购日期
     *
     * @returns {*}
     * @memberof EMAsset
     */
    purchdate?: any;

    /**
     * 投运日期
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqstartdate?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqpriority?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMAsset
     */
    createdate?: any;

    /**
     * 报废原因
     *
     * @returns {*}
     * @memberof EMAsset
     */
    disdesc?: any;

    /**
     * 剩余期限
     *
     * @returns {*}
     * @memberof EMAsset
     */
    syqx?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMAsset
     */
    updatedate?: any;

    /**
     * 残值率(%)
     *
     * @returns {*}
     * @memberof EMAsset
     */
    replacerate?: any;

    /**
     * 资产信息
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetinfo?: any;

    /**
     * 资产标识
     *
     * @returns {*}
     * @memberof EMAsset
     */
    emassetid?: any;

    /**
     * 合计
     *
     * @returns {*}
     * @memberof EMAsset
     */
    hj?: any;

    /**
     * 资产代码
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetcode?: any;

    /**
     * 资产备注
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetdesc?: any;

    /**
     * 管理部门
     *
     * @returns {*}
     * @memberof EMAsset
     */
    mgrdeptname?: any;

    /**
     * 车架号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqserialcode?: any;

    /**
     * 存放地点
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetlct?: any;

    /**
     * 规格型号
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqmodelcode?: any;

    /**
     * 总运行时间
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqsumstoptime?: any;

    /**
     * 税费
     *
     * @returns {*}
     * @memberof EMAsset
     */
    sf?: any;

    /**
     * 产地
     *
     * @returns {*}
     * @memberof EMAsset
     */
    pplace?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    labservicename?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof EMAsset
     */
    contractname?: any;

    /**
     * 资产科目
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetclassname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMAsset
     */
    acclassname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqlocationname?: any;

    /**
     * 服务提供商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    rservicename?: any;

    /**
     * 资产科目代码
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetclasscode?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMAsset
     */
    unitname?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    mservicename?: any;

    /**
     * 资产科目
     *
     * @returns {*}
     * @memberof EMAsset
     */
    assetclassid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMAsset
     */
    acclassid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMAsset
     */
    eqlocationid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMAsset
     */
    unitid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    mserviceid?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    labserviceid?: any;

    /**
     * 服务提供商
     *
     * @returns {*}
     * @memberof EMAsset
     */
    rserviceid?: any;

    /**
     * 合同
     *
     * @returns {*}
     * @memberof EMAsset
     */
    contractid?: any;
}