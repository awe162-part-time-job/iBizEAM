/**
 * 班组
 *
 * @export
 * @interface PFTeam
 */
export interface PFTeam {

    /**
     * 班组代码
     *
     * @returns {*}
     * @memberof PFTeam
     */
    teamcode?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFTeam
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFTeam
     */
    orgid?: any;

    /**
     * 班组标识
     *
     * @returns {*}
     * @memberof PFTeam
     */
    pfteamid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFTeam
     */
    createman?: any;

    /**
     * 班组类型
     *
     * @returns {*}
     * @memberof PFTeam
     */
    teamtypeid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFTeam
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFTeam
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFTeam
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFTeam
     */
    description?: any;

    /**
     * 职能
     *
     * @returns {*}
     * @memberof PFTeam
     */
    teamfn?: any;

    /**
     * 班组名称
     *
     * @returns {*}
     * @memberof PFTeam
     */
    pfteamname?: any;

    /**
     * 班组信息
     *
     * @returns {*}
     * @memberof PFTeam
     */
    teaminfo?: any;

    /**
     * 能力
     *
     * @returns {*}
     * @memberof PFTeam
     */
    teamcapacity?: any;
}