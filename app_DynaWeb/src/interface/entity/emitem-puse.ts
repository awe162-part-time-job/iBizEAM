/**
 * 领料单
 *
 * @export
 * @interface EMItemPUse
 */
export interface EMItemPUse {

    /**
     * 备注
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    remark?: any;

    /**
     * 领料单名称
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    emitempusename?: any;

    /**
     * 领料单信息
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    itempuseinfo?: any;

    /**
     * 删除标识
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    deltype?: any;

    /**
     * 处理意见
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    opinion?: any;

    /**
     * 当前仓库库存
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    stocknum?: any;

    /**
     * 审核成功次数
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    approknum?: any;

    /**
     * 设备集合
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    equips?: any;

    /**
     * 实发数
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    psum?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    orgid?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    price?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    batcode?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    bz?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    updatedate?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    amount?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    wfstate?: any;

    /**
     * sap传输异常文本
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sapreason1?: any;

    /**
     * 领料状态
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    pusestate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    updateman?: any;

    /**
     * 审核意见
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    apprdesc?: any;

    /**
     * sap成本中心
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sapcbzx?: any;

    /**
     * sap领料用途
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sapllyt?: any;

    /**
     * 请领实发差
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    numdiff?: any;

    /**
     * 请领数
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    asum?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    enable?: any;

    /**
     * sap传输状态
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sap?: any;

    /**
     * 发料日期
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sdate?: any;

    /**
     * 寿命周期
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    life2?: any;

    /**
     * 领料单号
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    emitempuseid?: any;

    /**
     * 未摊销数量
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    stock2num?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    wfinstanceid?: any;

    /**
     * 用途
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    useto?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    wfstep?: any;

    /**
     * 领料分类
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    pusetype?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sapcontrol?: any;

    /**
     * 申请日期
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    adate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    description?: any;

    /**
     * 批准日期
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    apprdate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    createdate?: any;

    /**
     * 寿命周期(天)
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    life?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    storename?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    unitname?: any;

    /**
     * 领料位置
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    objname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    unitid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    itembtypeid?: any;

    /**
     * 领料班组
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    teamname?: any;

    /**
     * 物品分组
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    itemgroup?: any;

    /**
     * 建议供应商
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    labservicename?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    avgprice?: any;

    /**
     * 领料物品
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    itemname?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    mservicename?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    storepartname?: any;

    /**
     * 领料设备
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    equipname?: any;

    /**
     * 采购计划
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    purplanname?: any;

    /**
     * 领料工单
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    woname?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    storeid?: any;

    /**
     * 领料物品
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    itemid?: any;

    /**
     * 领料班组
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    teamid?: any;

    /**
     * 领料设备
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    equipid?: any;

    /**
     * 建议供应商
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    labserviceid?: any;

    /**
     * 领料工单
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    woid?: any;

    /**
     * 制造商
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    mserviceid?: any;

    /**
     * 领料位置
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    objid?: any;

    /**
     * 采购计划
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    purplanid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    storepartid?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    aempid?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    aempname?: any;

    /**
     * 发料人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sempid?: any;

    /**
     * 发料人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    sempname?: any;

    /**
     * 领料人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    empid?: any;

    /**
     * 领料人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    empname?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    apprempid?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    apprempname?: any;

    /**
     * 领料部门
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    deptid?: any;

    /**
     * 领料部门
     *
     * @returns {*}
     * @memberof EMItemPUse
     */
    deptname?: any;
}