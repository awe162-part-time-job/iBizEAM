/**
 * 机种编号
 *
 * @export
 * @interface EMMachineCategory
 */
export interface EMMachineCategory {

    /**
     * 流水号
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    emmachinecategoryid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    enable?: any;

    /**
     * 机种名称
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    emmachinecategoryname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    createman?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    remarks?: any;

    /**
     * 机种信息
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    jzinfo?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    createdate?: any;

    /**
     * 机种编码
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    machtypecode?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMMachineCategory
     */
    updatedate?: any;
}