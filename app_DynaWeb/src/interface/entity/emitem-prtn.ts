/**
 * 还料单
 *
 * @export
 * @interface EMItemPRtn
 */
export interface EMItemPRtn {

    /**
     * 还料分类
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    prtntype?: any;

    /**
     * sap传输错误原因
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sapreason?: any;

    /**
     * 还料单名称
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    emitemprtnname?: any;

    /**
     * sap传输状态
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sap?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    createman?: any;

    /**
     * 还料状态
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    tradestate?: any;

    /**
     * 还料单信息
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    itemprtninfo?: any;

    /**
     * 实还数
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    psum?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    createdate?: any;

    /**
     * sap传输异常文本
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sapreason1?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    wfinstanceid?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    price?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    orgid?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    wfstep?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    amount?: any;

    /**
     * 还料单号
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    emitemprtnid?: any;

    /**
     * sap控制
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sapcontrol?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    description?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    wfstate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    updateman?: any;

    /**
     * 还料日期
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    enable?: any;

    /**
     * 批次
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    batcode?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    storename?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    unitname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    itemname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    unitid?: any;

    /**
     * 领料单
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    rname?: any;

    /**
     * 领料单
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    pusetype?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    storepartname?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    avgprice?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    itemid?: any;

    /**
     * 库位
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    storepartid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    storeid?: any;

    /**
     * 领料单
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    rid?: any;

    /**
     * 领料部门
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    deptid?: any;

    /**
     * 还料人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    empid?: any;

    /**
     * 还料人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    empname?: any;

    /**
     * 收料人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sempid?: any;

    /**
     * 收料人
     *
     * @returns {*}
     * @memberof EMItemPRtn
     */
    sempname?: any;
}