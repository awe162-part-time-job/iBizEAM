/**
 * 发动机位置
 *
 * @export
 * @interface EMEQLCTFDJ
 */
export interface EMEQLCTFDJ {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    orgid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    updateman?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    eqmodelcode?: any;

    /**
     * 参数
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    params?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    description?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    enable?: any;

    /**
     * 含涡
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    havew?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    equipname?: any;

    /**
     * 发动机信息
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    eqlocationinfo?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    equipid?: any;

    /**
     * 位置标识
     *
     * @returns {*}
     * @memberof EMEQLCTFDJ
     */
    emeqlocationid?: any;
}