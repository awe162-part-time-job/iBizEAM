import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWO_OSCService from '@/service/emwo-osc/emwo-osc-service';
import Main4Service from './main4-form-service';
import EMWO_OSCUIService from '@/uiservice/emwo-osc/emwo-osc-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main4EditFormBase}
 */
export class Main4EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main4Service}
     * @memberof Main4EditFormBase
     */
    public service: Main4Service = new Main4Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWO_OSCService}
     * @memberof Main4EditFormBase
     */
    public appEntityService: EMWO_OSCService = new EMWO_OSCService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeName: string = 'emwo_osc';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeLogicName: string = '外委保养工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWO_OSCUIService}
     * @memberof Main4Base
     */  
    public appUIService: EMWO_OSCUIService = new EMWO_OSCUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main4EditFormBase
     */
    protected formKeyItemName: string = 'emwo_oscid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emwo_oscid: null,
        emwo_oscname: null,
        wogroup: null,
        wotype: null,
        wodate: null,
        activelengths: null,
        eqstoplength: null,
        equipname: null,
        objname: null,
        priority: null,
        wodesc: null,
        regionbegindate: null,
        regionenddate: null,
        worklength: null,
        wpersonid: null,
        wpersonname: null,
        wresult: null,
        mfee: null,
        cplanflag: null,
        arg1: null,
        arg2: null,
        arg3: null,
        arg4: null,
        arg5: null,
        rempid: null,
        rempname: null,
        rdeptid: null,
        rdeptname: null,
        recvpersonid: null,
        recvpersonname: null,
        mpersonid: null,
        mpersonname: null,
        rservicename: null,
        rteamname: null,
        emwo_osc: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public majorMessageField: string = 'emwo_oscname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '工单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_osc.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel14: new FormGroupPanelModel({ caption: '执行信息', detailType: 'GROUPPANEL', name: 'grouppanel14', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_osc.main4_form', extractMode: 'ITEM', details: [] } }),

        grouppanel27: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel27', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo_osc.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '工单编号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwo_oscid: new FormItemModel({
    caption: '工单编号(自动)', detailType: 'FORMITEM', name: 'emwo_oscid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        emwo_oscname: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'emwo_oscname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wogroup: new FormItemModel({
    caption: '工单分组', detailType: 'FORMITEM', name: 'wogroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        wotype: new FormItemModel({
    caption: '工单类型', detailType: 'FORMITEM', name: 'wotype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodate: new FormItemModel({
    caption: '安排执行日期', detailType: 'FORMITEM', name: 'wodate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activelengths: new FormItemModel({
    caption: '安排工时(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqstoplength: new FormItemModel({
    caption: '停运时间(分)', detailType: 'FORMITEM', name: 'eqstoplength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        priority: new FormItemModel({
    caption: '优先级', detailType: 'FORMITEM', name: 'priority', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodesc: new FormItemModel({
    caption: '工单内容', detailType: 'FORMITEM', name: 'wodesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '起始时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionenddate: new FormItemModel({
    caption: '结束时间', detailType: 'FORMITEM', name: 'regionenddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        worklength: new FormItemModel({
    caption: '实际工时(分)', detailType: 'FORMITEM', name: 'worklength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonid: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonname: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wresult: new FormItemModel({
    caption: '执行结果', detailType: 'FORMITEM', name: 'wresult', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mfee: new FormItemModel({
    caption: '约合材料费(￥)', detailType: 'FORMITEM', name: 'mfee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        cplanflag: new FormItemModel({
    caption: '转计划标志', detailType: 'FORMITEM', name: 'cplanflag', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        arg1: new FormItemModel({
    caption: '黄油', detailType: 'FORMITEM', name: 'arg1', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        arg2: new FormItemModel({
    caption: '棉纱', detailType: 'FORMITEM', name: 'arg2', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        arg3: new FormItemModel({
    caption: '柴油', detailType: 'FORMITEM', name: 'arg3', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        arg4: new FormItemModel({
    caption: '钢丝绳润滑油', detailType: 'FORMITEM', name: 'arg4', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        arg5: new FormItemModel({
    caption: '金属去污粉', detailType: 'FORMITEM', name: 'arg5', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '负责人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '负责人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptid: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonid: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonname: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonid: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonname: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rservicename: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamname: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

    };

    /**
     * 新建默认值
     * @memberof Main4EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('wogroup')) {
            this.data['wogroup'] = 'TASK';
        }
        if (this.data.hasOwnProperty('wodate')) {
            this.data['wodate'] = this.$util.dateFormat(new Date());
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main4Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}