/**
 * ByEQ 部件模型
 *
 * @export
 * @class ByEQModel
 */
export default class ByEQModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ByEQDashboard_sysportlet3_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'drwgid',
			},
			{
				name: 'refobjid',
			},
			{
				name: 'refobjname',
			},
			{
				name: 'drwgname',
			},
			{
				name: 'srfkey',
				prop: 'emdrwgmapid',
				dataType: 'GUID',
			},
			{
				name: 'srfmajortext',
				prop: 'emdrwgmapname',
				dataType: 'TEXT',
			},
			{
				name: 'emdrwgmap',
				prop: 'emdrwgmapid',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}