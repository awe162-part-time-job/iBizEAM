import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import EMDRWGMapService from '@/service/emdrwgmap/emdrwgmap-service';
import ByEQService from './by-eq-list-service';
import EMDRWGMapUIService from '@/uiservice/emdrwgmap/emdrwgmap-ui-service';

/**
 * dashboard_sysportlet3_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {ByEQListBase}
 */
export class ByEQListBase extends ListControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {ByEQService}
     * @memberof ByEQListBase
     */
    public service: ByEQService = new ByEQService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMDRWGMapService}
     * @memberof ByEQListBase
     */
    public appEntityService: EMDRWGMapService = new EMDRWGMapService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected appDeName: string = 'emdrwgmap';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQListBase
     */
    protected appDeLogicName: string = '文档引用';

    /**
     * 界面UI服务对象
     *
     * @type {EMDRWGMapUIService}
     * @memberof ByEQBase
     */  
    public appUIService: EMDRWGMapUIService = new EMDRWGMapUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ByEQListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof ByEQListBase
     */
    public minorSortDir: string = '';




}