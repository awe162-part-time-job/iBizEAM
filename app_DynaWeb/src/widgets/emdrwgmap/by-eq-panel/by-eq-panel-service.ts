import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ByEQ 部件服务对象
 *
 * @export
 * @class ByEQService
 */
export default class ByEQService extends ControlService {
}