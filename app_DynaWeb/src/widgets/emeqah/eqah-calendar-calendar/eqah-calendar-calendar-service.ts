import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import EQAhCalendarModel from './eqah-calendar-calendar-model';
import EMEQDebugService from '@/service/emeqdebug/emeqdebug-service';
import EMEQKeepService from '@/service/emeqkeep/emeqkeep-service';
import EMEQMaintanceService from '@/service/emeqmaintance/emeqmaintance-service';
import EMEQSetupService from '@/service/emeqsetup/emeqsetup-service';
import EMEQCheckService from '@/service/emeqcheck/emeqcheck-service';


/**
 * EQAhCalendar 部件服务对象
 *
 * @export
 * @class EQAhCalendarService
 */
export default class EQAhCalendarService extends ControlService {

    /**
     * 活动历史服务对象
     *
     * @type {EMEQAHService}
     * @memberof EQAhCalendarService
     */
    public appEntityService: EMEQAHService = new EMEQAHService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EQAhCalendarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EQAhCalendarService.
     * 
     * @param {*} [opts={}]
     * @memberof EQAhCalendarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EQAhCalendarModel();
    }

    /**
     * 事故记录服务对象
     *
     * @type {EMEQDebugService}
     * @memberof EQAhCalendarService
     */
    public emeqdebugService: EMEQDebugService = new EMEQDebugService();
    /**
     * 维护保养服务对象
     *
     * @type {EMEQKeepService}
     * @memberof EQAhCalendarService
     */
    public emeqkeepService: EMEQKeepService = new EMEQKeepService();
    /**
     * 抢修记录服务对象
     *
     * @type {EMEQMaintanceService}
     * @memberof EQAhCalendarService
     */
    public emeqmaintanceService: EMEQMaintanceService = new EMEQMaintanceService();
    /**
     * 更换安装服务对象
     *
     * @type {EMEQSetupService}
     * @memberof EQAhCalendarService
     */
    public emeqsetupService: EMEQSetupService = new EMEQSetupService();
    /**
     * 维修记录服务对象
     *
     * @type {EMEQCheckService}
     * @memberof EQAhCalendarService
     */
    public emeqcheckService: EMEQCheckService = new EMEQCheckService();

    /**
     * 事件配置集合
     *
     * @public
     * @type {any[]}
     * @memberof EQAhCalendar
     */
    public eventsConfig: any[] = [
        {
          itemName : '事故记录',
          itemType : 'EMEQDEBUG',
          color : 'rgba(255, 0, 0, 1)',
          textColor : '',
        },
        {
          itemName : '保养记录',
          itemType : 'EMEQKEEP',
          color : 'rgba(103, 58, 183, 1)',
          textColor : '',
        },
        {
          itemName : '抢修记录',
          itemType : 'EMEQMAINTANCE',
          color : 'rgba(255, 149, 0, 1)',
          textColor : '',
        },
        {
          itemName : '更换安装',
          itemType : 'EMEQSETUP',
          color : 'rgba(4, 0, 255, 1)',
          textColor : '',
        },
        {
          itemName : '维修记录',
          itemType : 'EMEQCHECK',
          color : 'rgba(0, 255, 47, 1)',
          textColor : '',
        },
    ];

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EQAhCalendarService
     */
    @Errorlog
    public search(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let _this = this;
        return new Promise((resolve: any, reject: any) => {
            let promises:any = [];
            let tempRequest:any;
            tempRequest = this.handleRequestData(action,context,data,true,"EMEQDEBUG");
            promises.push(this.emeqdebugService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMEQKEEP");
            promises.push(this.emeqkeepService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMEQMAINTANCE");
            promises.push(this.emeqmaintanceService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMEQSETUP");
            promises.push(this.emeqsetupService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            tempRequest = this.handleRequestData(action,context,data,true,"EMEQCHECK");
            promises.push(this.emeqcheckService.FetchCalendar(tempRequest.context, tempRequest.data, isloading));
            Promise.all(promises).then((resArray: any) => {
                let _data:any = [];
                resArray.forEach((response:any,resIndex:number) => {
                    if (!response || response.status !== 200) {
                        return;
                    }
                    let _response: any = JSON.parse(JSON.stringify(response));
                    _response.data.forEach((item:any,index:number) =>{
                        _response.data[index].color = _this.eventsConfig[resIndex].color;
                        _response.data[index].textColor = _this.eventsConfig[resIndex].textColor;
                        _response.data[index].itemType = _this.eventsConfig[resIndex].itemType;
                    });
                    ;
                    _this.handleResponse(action, _response,false,_this.eventsConfig[resIndex].itemType);
                    _data.push(..._response.data);
                });
                // 排序
                _data.sort((a:any, b:any)=>{
                    let dateA = new Date(Date.parse(a.start.replace(/-/g, "/")));
                    let dateB = new Date(Date.parse(b.start.replace(/-/g, "/")));
                    return dateA > dateB ? 1 : -1 ;
                });
                let result = {status: 200, data: _data};
                resolve(result);
            }).catch((response: any) => {
                reject(response);
            });  
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EQAhCalendarService
     */
    @Errorlog
    public update(itemType: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        return new Promise((resolve: any, reject: any) => {
            let result: any;
            let tempRequest:any;
            switch(itemType) {
                case "EMEQDEBUG":
                    tempRequest = this.handleRequestData("",context,data,false,"EMEQDEBUG");
                    result = this.emeqdebugService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMEQKEEP":
                    tempRequest = this.handleRequestData("",context,data,false,"EMEQKEEP");
                    result = this.emeqkeepService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMEQMAINTANCE":
                    tempRequest = this.handleRequestData("",context,data,false,"EMEQMAINTANCE");
                    result = this.emeqmaintanceService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMEQSETUP":
                    tempRequest = this.handleRequestData("",context,data,false,"EMEQSETUP");
                    result = this.emeqsetupService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
                case "EMEQCHECK":
                    tempRequest = this.handleRequestData("",context,data,false,"EMEQCHECK");
                    result = this.emeqcheckService.Update(tempRequest.context, tempRequest.data, isloading);
                    break;
            }
            if(result){
                result.then((response: any) => {
                    this.handleResponse("", response);
                    resolve(response);
                }).catch((response: any) => {
                    reject(response);
                });
            }else{
              reject("没有匹配的实体服务");
            }
        });
    }

    /**
     * 处理request请求数据
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof ControlService
     */
    public handleRequestData(action: string,context:any ={},data: any = {},isMerge:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        return super.handleRequestData(action,context,data,isMerge);
    }

    /**
     * 处理response返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlService
     */
    public async handleResponse(action: string, response: any,isCreate:boolean = false,itemType:string=""){
        let model: any = this.getMode();
        model.itemType = itemType;
        super.handleResponse(action,response,isCreate);
    }

}