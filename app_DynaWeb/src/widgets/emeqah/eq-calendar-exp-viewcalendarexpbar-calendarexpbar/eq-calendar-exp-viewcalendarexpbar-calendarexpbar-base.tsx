
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, CalendarViewExpBarControlBase } from '@/studio-core';
import EMEQAHService from '@/service/emeqah/emeqah-service';
import EqCalendarExpViewcalendarexpbarService from './eq-calendar-exp-viewcalendarexpbar-calendarexpbar-service';
import EMEQAHUIService from '@/uiservice/emeqah/emeqah-ui-service';
import CodeListService from "@service/app/codelist-service";

/**
 * calendarexpbar部件基类
 *
 * @export
 * @class CalendarViewExpBarControlBase
 * @extends {EqCalendarExpViewcalendarexpbarCalendarexpbarBase}
 */
export class EqCalendarExpViewcalendarexpbarCalendarexpbarBase extends CalendarViewExpBarControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected controlType: string = 'CALENDAREXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {EqCalendarExpViewcalendarexpbarService}
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public service: EqCalendarExpViewcalendarexpbarService = new EqCalendarExpViewcalendarexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQAHService}
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public appEntityService: EMEQAHService = new EMEQAHService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected appDeName: string = 'emeqah';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected appDeLogicName: string = '活动历史';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQAHUIService}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */  
    public appUIService: EMEQAHUIService = new EMEQAHUIService(this.$store);

    /**
     * calendarexpbar_calendar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public calendarexpbar_calendar_selectionchange($event: any, $event2?: any) {
        this.calendarexpbar_selectionchange($event, 'calendarexpbar_calendar', $event2);
    }

    /**
     * calendarexpbar_calendar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EqCalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public calendarexpbar_calendar_load($event: any, $event2?: any) {
        this.calendarexpbar_load($event, 'calendarexpbar_calendar', $event2);
    }



    /**
     * 导航视图名称
     *
     * @type {string}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public navViewName: any = {
        EMEQDEBUG: "emeqdebug-cal-edit-view9",
        EMEQKEEP: "emeqkeep-cal-edit-view9",
        EMEQMAINTANCE: "emeqmaintance-cal-edit-view9",
        EMEQSETUP: "emeqsetup-cal-edit-view9",
        EMEQCHECK: "emeqcheck-cal-edit-view9"
    };

    /**
     * 导航参数
     *
     * @type {*}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public navParam: any = {
        EMEQDEBUG: {
            navigateContext:null,
            navigateParams:null
        },
        EMEQKEEP: {
            navigateContext:null,
            navigateParams:null
        },
        EMEQMAINTANCE: {
            navigateContext:null,
            navigateParams:null
        },
        EMEQSETUP: {
            navigateContext:null,
            navigateParams:null
        },
        EMEQCHECK: {
            navigateContext:null,
            navigateParams:null
        }
    };

    /**
     * 导航过滤项
     *
     * @type {*}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public navFilter: any = {
        EMEQDEBUG: "",
        EMEQKEEP: "",
        EMEQMAINTANCE: "",
        EMEQSETUP: "",
        EMEQCHECK: ""
    };

    /**
     * 导航关系
     *
     * @type {*}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public navPSDer: any = {
        EMEQDEBUG: "",
        EMEQKEEP: "",
        EMEQMAINTANCE: "",
        EMEQSETUP: "",
        EMEQCHECK: ""
    };




    /**
     * 搜索字段名称
     * 
     * @type {(string)}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public placeholder="活动历史名称";

    /**
     * 呈现模式，可选值：horizontal或者vertical
     * 
     * @public
     * @type {(string)}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public showMode:string ="horizontal";

    /**
     * 控件高度
     *
     * @type {number}
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public ctrlHeight: number = 0;
    
    /**
     * calendarexpbar的选中数据事件
     * 
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public calendarexpbar_selectionchange(args: any [], tag?: string, $event2?: any): void {
        let tempContext:any = {};
        let tempViewParam:any = {};
        if (args.length === 0) {
            this.calcToolbarItemState(true);
            return ;
        }
        const arg:any = args[0];
        if(this.context){
            Object.assign(tempContext,JSON.parse(JSON.stringify(this.context)));
        }
        switch(arg.itemType) {
            case "EMEQDEBUG":
                Object.assign(tempContext,{ emeqdebug : arg.emeqdebug});
                Object.assign(tempContext,{srfparentdename:'EMEQDebug',srfparentkey:arg['emeqdebug']});
                if(this.navFilter && this.navFilter['EMEQDEBUG'] && !Object.is(this.navFilter['EMEQDEBUG'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMEQDEBUG']]:arg['emeqdebug']});
                }
                if(this.navPSDer && this.navFilter['EMEQDEBUG'] && !Object.is(this.navPSDer['EMEQDEBUG'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMEQDEBUG']]:arg['emeqdebug']});
                }
                if(this.navParam && this.navParam['EMEQDEBUG'] && this.navParam['EMEQDEBUG'].navigateContext && Object.keys(this.navParam['EMEQDEBUG'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQDEBUG'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMEQDEBUG'] && this.navParam['EMEQDEBUG'].navigateParams && Object.keys(this.navParam['EMEQDEBUG'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQDEBUG'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMEQKEEP":
                Object.assign(tempContext,{ emeqkeep : arg.emeqkeep});
                Object.assign(tempContext,{srfparentdename:'EMEQKeep',srfparentkey:arg['emeqkeep']});
                if(this.navFilter && this.navFilter['EMEQKEEP'] && !Object.is(this.navFilter['EMEQKEEP'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMEQKEEP']]:arg['emeqkeep']});
                }
                if(this.navPSDer && this.navFilter['EMEQKEEP'] && !Object.is(this.navPSDer['EMEQKEEP'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMEQKEEP']]:arg['emeqkeep']});
                }
                if(this.navParam && this.navParam['EMEQKEEP'] && this.navParam['EMEQKEEP'].navigateContext && Object.keys(this.navParam['EMEQKEEP'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQKEEP'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMEQKEEP'] && this.navParam['EMEQKEEP'].navigateParams && Object.keys(this.navParam['EMEQKEEP'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQKEEP'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMEQMAINTANCE":
                Object.assign(tempContext,{ emeqmaintance : arg.emeqmaintance});
                Object.assign(tempContext,{srfparentdename:'EMEQMaintance',srfparentkey:arg['emeqmaintance']});
                if(this.navFilter && this.navFilter['EMEQMAINTANCE'] && !Object.is(this.navFilter['EMEQMAINTANCE'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMEQMAINTANCE']]:arg['emeqmaintance']});
                }
                if(this.navPSDer && this.navFilter['EMEQMAINTANCE'] && !Object.is(this.navPSDer['EMEQMAINTANCE'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMEQMAINTANCE']]:arg['emeqmaintance']});
                }
                if(this.navParam && this.navParam['EMEQMAINTANCE'] && this.navParam['EMEQMAINTANCE'].navigateContext && Object.keys(this.navParam['EMEQMAINTANCE'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQMAINTANCE'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMEQMAINTANCE'] && this.navParam['EMEQMAINTANCE'].navigateParams && Object.keys(this.navParam['EMEQMAINTANCE'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQMAINTANCE'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMEQSETUP":
                Object.assign(tempContext,{ emeqsetup : arg.emeqsetup});
                Object.assign(tempContext,{srfparentdename:'EMEQSetup',srfparentkey:arg['emeqsetup']});
                if(this.navFilter && this.navFilter['EMEQSETUP'] && !Object.is(this.navFilter['EMEQSETUP'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMEQSETUP']]:arg['emeqsetup']});
                }
                if(this.navPSDer && this.navFilter['EMEQSETUP'] && !Object.is(this.navPSDer['EMEQSETUP'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMEQSETUP']]:arg['emeqsetup']});
                }
                if(this.navParam && this.navParam['EMEQSETUP'] && this.navParam['EMEQSETUP'].navigateContext && Object.keys(this.navParam['EMEQSETUP'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQSETUP'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMEQSETUP'] && this.navParam['EMEQSETUP'].navigateParams && Object.keys(this.navParam['EMEQSETUP'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQSETUP'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMEQCHECK":
                Object.assign(tempContext,{ emeqcheck : arg.emeqcheck});
                Object.assign(tempContext,{srfparentdename:'EMEQCheck',srfparentkey:arg['emeqcheck']});
                if(this.navFilter && this.navFilter['EMEQCHECK'] && !Object.is(this.navFilter['EMEQCHECK'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMEQCHECK']]:arg['emeqcheck']});
                }
                if(this.navPSDer && this.navFilter['EMEQCHECK'] && !Object.is(this.navPSDer['EMEQCHECK'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMEQCHECK']]:arg['emeqcheck']});
                }
                if(this.navParam && this.navParam['EMEQCHECK'] && this.navParam['EMEQCHECK'].navigateContext && Object.keys(this.navParam['EMEQCHECK'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQCHECK'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMEQCHECK'] && this.navParam['EMEQCHECK'].navigateParams && Object.keys(this.navParam['EMEQCHECK'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMEQCHECK'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
        }
        this.selection = {};
        Object.assign(this.selection, { view: { viewname: this.navViewName[arg.itemType] }, context:tempContext,viewparam:tempViewParam });
        this.calcToolbarItemState(false);
        this.$emit('selectionchange',args);
        this.$forceUpdate();
    }

    /**
     * calendarexpbar的load完成事件
     * 
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public calendarexpbar_load(args:any, tag?: string, $event2?: any){
        this.calcToolbarItemState(true);
        this.$emit('load',args);
    }

    /**
     * 设置导航区工具栏禁用状态
     *
     * @param {boolean} state
     * @return {*} 
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public calcToolbarItemState(state: boolean) {
        let _this: any = this;
        const models:any = _this.eqcalendarexpviewcalendarexpbar_toolbarModels;
        if (models) {
            for (const key in models) {
                if (!models.hasOwnProperty(key)) {
                    return;
                }
                const _item = models[key];
                if (_item.uiaction && (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))) {
                    _item.disabled = state;
                }
                _item.visible = true;
                if (_item.noprivdisplaymode && _item.noprivdisplaymode === 6) {
                    _item.visible = false;
                }
            }
            this.calcNavigationToolbarState();
        }
    }

    /**
     * 计算导航工具栏权限状态
     * 
     * @memberof EqCalendarExpViewcalendarexpbarBase
     */
    public calcNavigationToolbarState(){
        let _this: any = this;
        // 界面行为
        if(_this.eqcalendarexpviewcalendarexpbar_toolbarModels){
            const curUIService:EMEQAHUIService  = new EMEQAHUIService();
            ViewTool.calcActionItemAuthState({},_this.eqcalendarexpviewcalendarexpbar_toolbarModels,curUIService);
        }
    }

    /**
    * 执行搜索
    *
    * @memberof EqCalendarExpViewcalendarexpbarBase
    */
    public onSearch($event:any) {
        let calendar:any = this.$refs.calendarexpbar_calendar;
        calendar.searchEvents({ query: this.searchText });
    }

}