/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemtypeid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemtypename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'itemtypecode',
        prop: 'itemtypecode',
        dataType: 'TEXT',
      },
      {
        name: 'emitemtypename',
        prop: 'emitemtypename',
        dataType: 'TEXT',
      },
      {
        name: 'itemtypepname',
        prop: 'itemtypepname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itembtypename',
        prop: 'itembtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemmtypename',
        prop: 'itemmtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemmtypeid',
        prop: 'itemmtypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemtypepid',
        prop: 'itemtypepid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemtypeid',
        prop: 'emitemtypeid',
        dataType: 'GUID',
      },
      {
        name: 'itembtypeid',
        prop: 'itembtypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemtype',
        prop: 'emitemtypeid',
        dataType: 'FONTKEY',
      },
    ]
  }

}