import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, TreeExpBarControlBase } from '@/studio-core';
import EMItemTypeService from '@/service/emitem-type/emitem-type-service';
import InfoTreeExpViewtreeexpbarService from './info-tree-exp-viewtreeexpbar-treeexpbar-service';
import EMItemTypeUIService from '@/uiservice/emitem-type/emitem-type-ui-service';

/**
 * treeexpbar部件基类
 *
 * @export
 * @class TreeExpBarControlBase
 * @extends {InfoTreeExpViewtreeexpbarTreeExpBarBase}
 */
export class InfoTreeExpViewtreeexpbarTreeExpBarBase extends TreeExpBarControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected controlType: string = 'TREEEXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {InfoTreeExpViewtreeexpbarService}
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    public service: InfoTreeExpViewtreeexpbarService = new InfoTreeExpViewtreeexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemTypeService}
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    public appEntityService: EMItemTypeService = new EMItemTypeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected appDeName: string = 'emitemtype';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    protected appDeLogicName: string = '物品类型';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemTypeUIService}
     * @memberof InfoTreeExpViewtreeexpbarBase
     */  
    public appUIService: EMItemTypeUIService = new EMItemTypeUIService(this.$store);

    /**
     * treeexpbar_tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_selectionchange($event: any, $event2?: any) {
        this.treeexpbar_selectionchange($event, 'treeexpbar_tree', $event2);
    }

    /**
     * treeexpbar_tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof InfoTreeExpViewtreeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_load($event: any, $event2?: any) {
        this.treeexpbar_load($event, 'treeexpbar_tree', $event2);
    }


    /**
     * 控件宽度
     *
     * @type {number}
     * @memberof InfoTreeExpViewtreeexpbarBase
     */
    public ctrlWidth:number = 250;

    /**
     * 获取关系项视图
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof InfoTreeExpViewtreeexpbarBase
     */
    public getExpItemView(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        if (Object.is(expmode, 'ROOTTYPE')) {
            return {  
                viewname: 'emitem-type-grid-exp-view', 
                parentdata: {"srfparentdefname":"emitemtypeid"},
                deKeyField:'emitemtype'
			};
        }
        if (Object.is(expmode, 'ITEMTYPE')) {
            return {  
                viewname: 'emitem-type-grid-view', 
                parentdata: {},
                deKeyField:'emitemtype'
			};
        }
        if (Object.is(expmode, 'CHILDTYPE')) {
            return {  
                viewname: 'emitem-type-grid-exp-view', 
                parentdata: {"srfparentdefname":"emitemtypeid","srfparentdename":"EMITEMTYPE","srfparentmode":"DER1N_EMITEM_EMITEMTYPE_ITEMTYPEID"},
                deKeyField:'emitemtype'
			};
        }
        return null;
    }

    /**
    * 执行mounted后的逻辑
    *
    * @memberof InfoTreeExpViewtreeexpbarBase
    */
    public ctrlMounted(){ 
        if(this.$store.getters.getViewSplit(this.viewUID)){
            this.split = this.$store.getters.getViewSplit(this.viewUID);
        }else{
            let containerWidth:number = (document.getElementById("infotreeexpviewtreeexpbar") as any).offsetWidth;
            if(this.ctrlWidth){
                    this.split = this.ctrlWidth/containerWidth;
            }
            this.$store.commit("setViewSplit",{viewUID:this.viewUID,viewSplit:this.split}); 
        }  
    }

    /**
     * 视图数据加载完成
     *
     * @param {*} $event
     * @memberof InfoTreeExpViewtreeexpbarBase
     */
    public onViewLoad($event: any): void {
        this.$emit('load', $event);
    }
}