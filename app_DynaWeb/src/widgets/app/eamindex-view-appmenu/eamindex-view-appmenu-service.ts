import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EAMIndexViewModel from './eamindex-view-appmenu-model';


/**
 * EAMIndexView 部件服务对象
 *
 * @export
 * @class EAMIndexViewService
 */
export default class EAMIndexViewService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EAMIndexViewService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EAMIndexViewService.
     * 
     * @param {*} [opts={}]
     * @memberof EAMIndexViewService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EAMIndexViewModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof EAMIndexView
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/eamindex-viewappmenu', params);
    }

}