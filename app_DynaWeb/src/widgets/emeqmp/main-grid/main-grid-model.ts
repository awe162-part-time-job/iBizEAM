/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emeqmpname',
          prop: 'emeqmpname',
          dataType: 'TEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'normalrefval',
          prop: 'normalrefval',
          dataType: 'TEXT',
        },
        {
          name: 'mpdesc',
          prop: 'mpdesc',
          dataType: 'TEXT',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqmpname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqmpid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqmpid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqmp',
          prop: 'emeqmpid',
        },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}