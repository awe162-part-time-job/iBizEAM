/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'mpname',
          prop: 'mpname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'woname',
          prop: 'woname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'edate',
          prop: 'edate',
          dataType: 'DATETIME',
        },
        {
          name: 'val',
          prop: 'val',
          dataType: 'TEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'woid',
          prop: 'woid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqmpmtrname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emeqmpmtrid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqmpmtrid',
          dataType: 'GUID',
        },
        {
          name: 'mpid',
          prop: 'mpid',
          dataType: 'PICKUP',
        },
        {
          name: 'emeqmpmtr',
          prop: 'emeqmpmtrid',
        },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_mpname_like',
        prop: 'n_mpname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_woname_like',
        prop: 'n_woname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}