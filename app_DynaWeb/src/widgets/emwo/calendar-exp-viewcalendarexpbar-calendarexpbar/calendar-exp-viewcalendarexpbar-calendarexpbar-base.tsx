
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, CalendarViewExpBarControlBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import CalendarExpViewcalendarexpbarService from './calendar-exp-viewcalendarexpbar-calendarexpbar-service';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';
import CodeListService from "@service/app/codelist-service";

/**
 * calendarexpbar部件基类
 *
 * @export
 * @class CalendarViewExpBarControlBase
 * @extends {CalendarExpViewcalendarexpbarCalendarexpbarBase}
 */
export class CalendarExpViewcalendarexpbarCalendarexpbarBase extends CalendarViewExpBarControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected controlType: string = 'CALENDAREXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {CalendarExpViewcalendarexpbarService}
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public service: CalendarExpViewcalendarexpbarService = new CalendarExpViewcalendarexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    protected appDeLogicName: string = '工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWOUIService}
     * @memberof CalendarExpViewcalendarexpbarBase
     */  
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);

    /**
     * calendarexpbar_calendar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public calendarexpbar_calendar_selectionchange($event: any, $event2?: any) {
        this.calendarexpbar_selectionchange($event, 'calendarexpbar_calendar', $event2);
    }

    /**
     * calendarexpbar_calendar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof CalendarExpViewcalendarexpbarCalendarexpbarBase
     */
    public calendarexpbar_calendar_load($event: any, $event2?: any) {
        this.calendarexpbar_load($event, 'calendarexpbar_calendar', $event2);
    }



    /**
     * 导航视图名称
     *
     * @type {string}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public navViewName: any = {
        EMWO_INNER: "emwo-innerwoview-edit-view",
        EMWO_OSC: "emwo-oscwoview-edit-view",
        EMWO_DP: "emwo-dpwoview-edit-view",
        EMWO_EN: "emwo-enwoview-edit-view"
    };

    /**
     * 导航参数
     *
     * @type {*}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public navParam: any = {
        EMWO_INNER: {
            navigateContext:null,
            navigateParams:null
        },
        EMWO_OSC: {
            navigateContext:null,
            navigateParams:null
        },
        EMWO_DP: {
            navigateContext:null,
            navigateParams:null
        },
        EMWO_EN: {
            navigateContext:null,
            navigateParams:null
        }
    };

    /**
     * 导航过滤项
     *
     * @type {*}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public navFilter: any = {
        EMWO_INNER: "",
        EMWO_OSC: "",
        EMWO_DP: "",
        EMWO_EN: ""
    };

    /**
     * 导航关系
     *
     * @type {*}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public navPSDer: any = {
        EMWO_INNER: "",
        EMWO_OSC: "",
        EMWO_DP: "",
        EMWO_EN: ""
    };




    /**
     * 搜索字段名称
     * 
     * @type {(string)}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public placeholder="工单名称";

    /**
     * 呈现模式，可选值：horizontal或者vertical
     * 
     * @public
     * @type {(string)}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public showMode:string ="horizontal";

    /**
     * 控件高度
     *
     * @type {number}
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public ctrlHeight: number = 0;
    
    /**
     * calendarexpbar的选中数据事件
     * 
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public calendarexpbar_selectionchange(args: any [], tag?: string, $event2?: any): void {
        let tempContext:any = {};
        let tempViewParam:any = {};
        if (args.length === 0) {
            this.calcToolbarItemState(true);
            return ;
        }
        const arg:any = args[0];
        if(this.context){
            Object.assign(tempContext,JSON.parse(JSON.stringify(this.context)));
        }
        switch(arg.itemType) {
            case "EMWO_INNER":
                Object.assign(tempContext,{ emwo_inner : arg.emwo_inner});
                Object.assign(tempContext,{srfparentdename:'EMWO_INNER',srfparentkey:arg['emwo_inner']});
                if(this.navFilter && this.navFilter['EMWO_INNER'] && !Object.is(this.navFilter['EMWO_INNER'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMWO_INNER']]:arg['emwo_inner']});
                }
                if(this.navPSDer && this.navFilter['EMWO_INNER'] && !Object.is(this.navPSDer['EMWO_INNER'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMWO_INNER']]:arg['emwo_inner']});
                }
                if(this.navParam && this.navParam['EMWO_INNER'] && this.navParam['EMWO_INNER'].navigateContext && Object.keys(this.navParam['EMWO_INNER'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_INNER'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMWO_INNER'] && this.navParam['EMWO_INNER'].navigateParams && Object.keys(this.navParam['EMWO_INNER'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_INNER'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMWO_OSC":
                Object.assign(tempContext,{ emwo_osc : arg.emwo_osc});
                Object.assign(tempContext,{srfparentdename:'EMWO_OSC',srfparentkey:arg['emwo_osc']});
                if(this.navFilter && this.navFilter['EMWO_OSC'] && !Object.is(this.navFilter['EMWO_OSC'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMWO_OSC']]:arg['emwo_osc']});
                }
                if(this.navPSDer && this.navFilter['EMWO_OSC'] && !Object.is(this.navPSDer['EMWO_OSC'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMWO_OSC']]:arg['emwo_osc']});
                }
                if(this.navParam && this.navParam['EMWO_OSC'] && this.navParam['EMWO_OSC'].navigateContext && Object.keys(this.navParam['EMWO_OSC'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_OSC'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMWO_OSC'] && this.navParam['EMWO_OSC'].navigateParams && Object.keys(this.navParam['EMWO_OSC'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_OSC'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMWO_DP":
                Object.assign(tempContext,{ emwo_dp : arg.emwo_dp});
                Object.assign(tempContext,{srfparentdename:'EMWO_DP',srfparentkey:arg['emwo_dp']});
                if(this.navFilter && this.navFilter['EMWO_DP'] && !Object.is(this.navFilter['EMWO_DP'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMWO_DP']]:arg['emwo_dp']});
                }
                if(this.navPSDer && this.navFilter['EMWO_DP'] && !Object.is(this.navPSDer['EMWO_DP'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMWO_DP']]:arg['emwo_dp']});
                }
                if(this.navParam && this.navParam['EMWO_DP'] && this.navParam['EMWO_DP'].navigateContext && Object.keys(this.navParam['EMWO_DP'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_DP'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMWO_DP'] && this.navParam['EMWO_DP'].navigateParams && Object.keys(this.navParam['EMWO_DP'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_DP'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
            case "EMWO_EN":
                Object.assign(tempContext,{ emwo_en : arg.emwo_en});
                Object.assign(tempContext,{srfparentdename:'EMWO_EN',srfparentkey:arg['emwo_en']});
                if(this.navFilter && this.navFilter['EMWO_EN'] && !Object.is(this.navFilter['EMWO_EN'],"")){
                    Object.assign(tempViewParam,{[this.navFilter['EMWO_EN']]:arg['emwo_en']});
                }
                if(this.navPSDer && this.navFilter['EMWO_EN'] && !Object.is(this.navPSDer['EMWO_EN'],"")){
                    Object.assign(tempViewParam,{[this.navPSDer['EMWO_EN']]:arg['emwo_en']});
                }
                if(this.navParam && this.navParam['EMWO_EN'] && this.navParam['EMWO_EN'].navigateContext && Object.keys(this.navParam['EMWO_EN'].navigateContext).length >0){
                    let _context:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_EN'].navigateContext);
                    Object.assign(tempContext,_context);
                }
                if(this.navParam && this.navParam['EMWO_EN'] && this.navParam['EMWO_EN'].navigateParams && Object.keys(this.navParam['EMWO_EN'].navigateParams).length >0){
                    let _params:any = this.$util.computedNavData(arg,tempContext,tempViewParam,this.navParam['EMWO_EN'].navigateParams);
                    Object.assign(tempViewParam,_params);
                }
                break;
        }
        this.selection = {};
        Object.assign(this.selection, { view: { viewname: this.navViewName[arg.itemType] }, context:tempContext,viewparam:tempViewParam });
        this.calcToolbarItemState(false);
        this.$emit('selectionchange',args);
        this.$forceUpdate();
    }

    /**
     * calendarexpbar的load完成事件
     * 
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public calendarexpbar_load(args:any, tag?: string, $event2?: any){
        this.calcToolbarItemState(true);
        this.$emit('load',args);
    }

    /**
     * 设置导航区工具栏禁用状态
     *
     * @param {boolean} state
     * @return {*} 
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public calcToolbarItemState(state: boolean) {
        let _this: any = this;
        const models:any = _this.calendarexpviewcalendarexpbar_toolbarModels;
        if (models) {
            for (const key in models) {
                if (!models.hasOwnProperty(key)) {
                    return;
                }
                const _item = models[key];
                if (_item.uiaction && (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))) {
                    _item.disabled = state;
                }
                _item.visible = true;
                if (_item.noprivdisplaymode && _item.noprivdisplaymode === 6) {
                    _item.visible = false;
                }
            }
            this.calcNavigationToolbarState();
        }
    }

    /**
     * 计算导航工具栏权限状态
     * 
     * @memberof CalendarExpViewcalendarexpbarBase
     */
    public calcNavigationToolbarState(){
        let _this: any = this;
        // 界面行为
        if(_this.calendarexpviewcalendarexpbar_toolbarModels){
            const curUIService:EMWOUIService  = new EMWOUIService();
            ViewTool.calcActionItemAuthState({},_this.calendarexpviewcalendarexpbar_toolbarModels,curUIService);
        }
    }

    /**
    * 执行搜索
    *
    * @memberof CalendarExpViewcalendarexpbarBase
    */
    public onSearch($event:any) {
        let calendar:any = this.$refs.calendarexpbar_calendar;
        calendar.searchEvents({ query: this.searchText });
    }

}