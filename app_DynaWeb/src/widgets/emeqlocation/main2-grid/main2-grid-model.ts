/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'eqlocationcode',
          prop: 'eqlocationcode',
          dataType: 'TEXT',
        },
        {
          name: 'emeqlocationname',
          prop: 'emeqlocationname',
          dataType: 'TEXT',
        },
        {
          name: 'eqlocationtype',
          prop: 'eqlocationtype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'majorequipid',
          prop: 'majorequipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emeqlocationname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqlocationid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emeqlocationid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emeqlocation',
          prop: 'emeqlocationid',
        },
      {
        name: 'n_eqlocationcode_like',
        prop: 'n_eqlocationcode_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emeqlocationname_like',
        prop: 'n_emeqlocationname_like',
        dataType: 'TEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}