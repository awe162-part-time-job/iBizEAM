/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emassetclearid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emassetclearname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emassetclearname',
        prop: 'emassetclearname',
        dataType: 'TEXT',
      },
      {
        name: 'emassetname',
        prop: 'emassetname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'assetinnum',
        prop: 'assetinnum',
        dataType: 'INT',
      },
      {
        name: 'assetinprice',
        prop: 'assetinprice',
        dataType: 'CURRENCY',
      },
      {
        name: 'assetoutnum',
        prop: 'assetoutnum',
        dataType: 'INT',
      },
      {
        name: 'assetoutprice',
        prop: 'assetoutprice',
        dataType: 'CURRENCY',
      },
      {
        name: 'assetchecknum',
        prop: 'assetchecknum',
        dataType: 'INT',
      },
      {
        name: 'assetcheckprice',
        prop: 'assetcheckprice',
        dataType: 'CURRENCY',
      },
      {
        name: 'assetclearstate',
        prop: 'assetclearstate',
        dataType: 'TEXT',
      },
      {
        name: 'assetclearlct',
        prop: 'assetclearlct',
        dataType: 'TEXT',
      },
      {
        name: 'emassetclearid',
        prop: 'emassetclearid',
        dataType: 'GUID',
      },
      {
        name: 'emassetclear',
        prop: 'emassetclearid',
        dataType: 'FONTKEY',
      },
    ]
  }

}