/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emassetclearname',
          prop: 'emassetclearname',
          dataType: 'TEXT',
        },
        {
          name: 'assetinprice',
          prop: 'assetinprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetoutprice',
          prop: 'assetoutprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetinnum',
          prop: 'assetinnum',
          dataType: 'INT',
        },
        {
          name: 'assetoutnum',
          prop: 'assetoutnum',
          dataType: 'INT',
        },
        {
          name: 'assetchecknum',
          prop: 'assetchecknum',
          dataType: 'INT',
        },
        {
          name: 'assetcheckprice',
          prop: 'assetcheckprice',
          dataType: 'CURRENCY',
        },
        {
          name: 'assetclearstate',
          prop: 'assetclearstate',
          dataType: 'TEXT',
        },
        {
          name: 'assetclearlct',
          prop: 'assetclearlct',
          dataType: 'TEXT',
        },
        {
          name: 'assetcleardate',
          prop: 'assetcleardate',
          dataType: 'DATE',
        },
        {
          name: 'emassetname',
          prop: 'emassetname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqmodelcode',
          prop: 'eqmodelcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'purchdate',
          prop: 'purchdate',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'originalcost',
          prop: 'originalcost',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetcode',
          prop: 'assetcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetclassname',
          prop: 'assetclassname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetsort',
          prop: 'assetsort',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'srfmajortext',
          prop: 'emassetclearname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emassetclearid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emassetclearid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emassetid',
          prop: 'emassetid',
          dataType: 'PICKUP',
        },
        {
          name: 'emassetclear',
          prop: 'emassetclearid',
        },
      {
        name: 'n_emassetclearname_like',
        prop: 'n_emassetclearname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_emassetname_like',
        prop: 'n_emassetname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}