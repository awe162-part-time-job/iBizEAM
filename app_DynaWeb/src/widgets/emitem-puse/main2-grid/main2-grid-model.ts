/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emitempuseid',
          prop: 'emitempuseid',
          dataType: 'GUID',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'adate',
          prop: 'adate',
          dataType: 'DATETIME',
        },
        {
          name: 'asum',
          prop: 'asum',
          dataType: 'FLOAT',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'unitname',
          prop: 'unitname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'FLOAT',
        },
        {
          name: 'wfstep',
          prop: 'wfstep',
          dataType: 'SSCODELIST',
        },
        {
          name: 'pusestate',
          prop: 'pusestate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'sdate',
          prop: 'sdate',
          dataType: 'DATETIME',
        },
        {
          name: 'useto',
          prop: 'useto',
          dataType: 'SSCODELIST',
        },
        {
          name: 'storename',
          prop: 'storename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'storepartname',
          prop: 'storepartname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'woname',
          prop: 'woname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'teamname',
          prop: 'teamname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'batcode',
          prop: 'batcode',
          dataType: 'TEXT',
        },
        {
          name: 'pusetype',
          prop: 'pusetype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'apprdate',
          prop: 'apprdate',
          dataType: 'DATETIME',
        },
        {
          name: 'purplanname',
          prop: 'purplanname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'apprdesc',
          prop: 'apprdesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'labserviceid',
          prop: 'labserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmstag',
        },
        {
          name: 'srfmajortext',
          prop: 'itempuseinfo',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emitempuseid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emitempuseid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'empid',
          prop: 'empid',
          dataType: 'PICKUP',
        },
        {
          name: 'deptid',
          prop: 'deptid',
          dataType: 'PICKUP',
        },
        {
          name: 'apprempid',
          prop: 'apprempid',
          dataType: 'PICKUP',
        },
        {
          name: 'storepartid',
          prop: 'storepartid',
          dataType: 'PICKUP',
        },
        {
          name: 'storeid',
          prop: 'storeid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'teamid',
          prop: 'teamid',
          dataType: 'PICKUP',
        },
        {
          name: 'woid',
          prop: 'woid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'purplanid',
          prop: 'purplanid',
          dataType: 'PICKUP',
        },
        {
          name: 'sempid',
          prop: 'sempid',
          dataType: 'PICKUP',
        },
        {
          name: 'aempid',
          prop: 'aempid',
          dataType: 'PICKUP',
        },
        {
          name: 'mserviceid',
          prop: 'mserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'emitempuse',
          prop: 'emitempuseid',
        },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_storeid_eq',
        prop: 'n_storeid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_storepartid_eq',
        prop: 'n_storepartid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_woid_eq',
        prop: 'n_woid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_pusestate_eq',
        prop: 'n_pusestate_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}