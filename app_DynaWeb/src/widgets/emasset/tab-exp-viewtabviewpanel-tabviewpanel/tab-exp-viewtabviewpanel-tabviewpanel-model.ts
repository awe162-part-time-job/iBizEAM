/**
 * TabExpViewtabviewpanel 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanelModel
 */
export default class TabExpViewtabviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'num',
      },
      {
        name: 'eqisservice',
      },
      {
        name: 'empname',
      },
      {
        name: 'warrantydate',
      },
      {
        name: 'innerlaborcost',
      },
      {
        name: 'keyattparam',
      },
      {
        name: 'foreignlaborcost',
      },
      {
        name: 'createman',
      },
      {
        name: 'jtsb',
      },
      {
        name: 'eqlife',
      },
      {
        name: 'lastzjdate',
      },
      {
        name: 'deptname',
      },
      {
        name: 'assettype',
      },
      {
        name: 'now',
      },
      {
        name: 'ytzj',
      },
      {
        name: 'disdate',
      },
      {
        name: 'assetstate',
      },
      {
        name: 'originalcost',
      },
      {
        name: 'deptid',
      },
      {
        name: 'techcode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'materialcost',
      },
      {
        name: 'discost',
      },
      {
        name: 'rempid',
      },
      {
        name: 'description',
      },
      {
        name: 'updateman',
      },
      {
        name: 'usedyear',
      },
      {
        name: 'emassetname',
      },
      {
        name: 'replacecost',
      },
      {
        name: 'assetequipid',
      },
      {
        name: 'enable',
      },
      {
        name: 'empid',
      },
      {
        name: 'eqlifeyear',
      },
      {
        name: 'assetsort',
      },
      {
        name: 'mgrdeptid',
      },
      {
        name: 'blsystemdesc',
      },
      {
        name: 'rempname',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'purchdate',
      },
      {
        name: 'eqstartdate',
      },
      {
        name: 'eqpriority',
      },
      {
        name: 'createdate',
      },
      {
        name: 'disdesc',
      },
      {
        name: 'syqx',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'replacerate',
      },
      {
        name: 'assetinfo',
      },
      {
        name: 'emasset',
        prop: 'emassetid',
      },
      {
        name: 'hj',
      },
      {
        name: 'assetcode',
      },
      {
        name: 'assetdesc',
      },
      {
        name: 'mgrdeptname',
      },
      {
        name: 'eqserialcode',
      },
      {
        name: 'assetlct',
      },
      {
        name: 'eqmodelcode',
      },
      {
        name: 'eqsumstoptime',
      },
      {
        name: 'sf',
      },
      {
        name: 'pplace',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'contractname',
      },
      {
        name: 'assetclassname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'eqlocationname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'assetclasscode',
      },
      {
        name: 'unitname',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'assetclassid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'eqlocationid',
      },
      {
        name: 'unitid',
      },
      {
        name: 'mserviceid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'contractid',
      },
    ]
  }


}