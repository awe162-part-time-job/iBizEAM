/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emassetid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emassetname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'assetcode',
        prop: 'assetcode',
        dataType: 'TEXT',
      },
      {
        name: 'emassetname',
        prop: 'emassetname',
        dataType: 'TEXT',
      },
      {
        name: 'num',
        prop: 'num',
        dataType: 'TEXT',
      },
      {
        name: 'assetclassname',
        prop: 'assetclassname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqlife',
        prop: 'eqlife',
        dataType: 'FLOAT',
      },
      {
        name: 'assettype',
        prop: 'assettype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'eqstartdate',
        prop: 'eqstartdate',
        dataType: 'DATE',
      },
      {
        name: 'assetstate',
        prop: 'assetstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'assetlct',
        prop: 'assetlct',
        dataType: 'TEXT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'deptname',
        prop: 'deptname',
        dataType: 'TEXT',
      },
      {
        name: 'mgrdeptname',
        prop: 'mgrdeptname',
        dataType: 'TEXT',
      },
      {
        name: 'eqmodelcode',
        prop: 'eqmodelcode',
        dataType: 'TEXT',
      },
      {
        name: 'pplace',
        prop: 'pplace',
        dataType: 'TEXT',
      },
      {
        name: 'blsystemdesc',
        prop: 'blsystemdesc',
        dataType: 'TEXT',
      },
      {
        name: 'eqserialcode',
        prop: 'eqserialcode',
        dataType: 'TEXT',
      },
      {
        name: 'keyattparam',
        prop: 'keyattparam',
        dataType: 'TEXT',
      },
      {
        name: 'jtsb',
        prop: 'jtsb',
        dataType: 'TEXT',
      },
      {
        name: 'assetdesc',
        prop: 'assetdesc',
        dataType: 'TEXT',
      },
      {
        name: 'originalcost',
        prop: 'originalcost',
        dataType: 'CURRENCY',
      },
      {
        name: 'sf',
        prop: 'sf',
        dataType: 'CURRENCY',
      },
      {
        name: 'hj',
        prop: 'hj',
        dataType: 'FLOAT',
      },
      {
        name: 'replacerate',
        prop: 'replacerate',
        dataType: 'FLOAT',
      },
      {
        name: 'lastzjdate',
        prop: 'lastzjdate',
        dataType: 'DATE',
      },
      {
        name: 'replacecost',
        prop: 'replacecost',
        dataType: 'FLOAT',
      },
      {
        name: 'ytzj',
        prop: 'ytzj',
        dataType: 'FLOAT',
      },
      {
        name: 'now',
        prop: 'now',
        dataType: 'FLOAT',
      },
      {
        name: 'syqx',
        prop: 'syqx',
        dataType: 'TEXT',
      },
      {
        name: 'disdate',
        prop: 'disdate',
        dataType: 'DATE',
      },
      {
        name: 'discost',
        prop: 'discost',
        dataType: 'CURRENCY',
      },
      {
        name: 'disdesc',
        prop: 'disdesc',
        dataType: 'TEXT',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mservicename',
        prop: 'mservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rservicename',
        prop: 'rservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'contractname',
        prop: 'contractname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'purchdate',
        prop: 'purchdate',
        dataType: 'DATE',
      },
      {
        name: 'warrantydate',
        prop: 'warrantydate',
        dataType: 'DATE',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emassetid',
        prop: 'emassetid',
        dataType: 'GUID',
      },
      {
        name: 'emasset',
        prop: 'emassetid',
        dataType: 'FONTKEY',
      },
    ]
  }

}