import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Store 部件服务对象
 *
 * @export
 * @class StoreService
 */
export default class StoreService extends ControlService {
}
