/**
 * Info 部件模型
 *
 * @export
 * @class InfoModel
 */
export default class InfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof InfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'itemcode',
        prop: 'itemcode',
        dataType: 'TEXT',
      },
      {
        name: 'emitemname',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'itemtypename',
        prop: 'itemtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemgroup',
        prop: 'itemgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'dens',
        prop: 'dens',
        dataType: 'FLOAT',
      },
      {
        name: 'itemmodelcode',
        prop: 'itemmodelcode',
        dataType: 'TEXT',
      },
      {
        name: 'itemserialcode',
        prop: 'itemserialcode',
        dataType: 'TEXT',
      },
      {
        name: 'acclassname',
        prop: 'acclassname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'isassetflag',
        prop: 'isassetflag',
        dataType: 'YESNO',
      },
      {
        name: 'checkmethod',
        prop: 'checkmethod',
        dataType: 'TEXT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'TEXT',
      },
      {
        name: 'isnew',
        prop: 'isnew',
        dataType: 'SSCODELIST',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}