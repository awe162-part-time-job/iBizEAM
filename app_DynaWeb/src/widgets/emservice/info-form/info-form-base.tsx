import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import InfoService from './info-form-service';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {InfoEditFormBase}
 */
export class InfoEditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {InfoService}
     * @memberof InfoEditFormBase
     */
    public service: InfoService = new InfoService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof InfoEditFormBase
     */
    public appEntityService: EMServiceService = new EMServiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof InfoEditFormBase
     */
    protected appDeLogicName: string = '服务商';

    /**
     * 界面UI服务对象
     *
     * @type {EMServiceUIService}
     * @memberof InfoBase
     */  
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        servicecode: null,
        emservicename: null,
        range: null,
        servicegroup: null,
        labservicelevelid: null,
        labservicetypeid: null,
        sums: null,
        servicestate: null,
        emserviceid: null,
        emservice: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public majorMessageField: string = 'emservicename';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof InfoBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof InfoEditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: 'PUR服务商信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emservice.info_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '服务商标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '服务商名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        servicecode: new FormItemModel({
    caption: '服务商代码', detailType: 'FORMITEM', name: 'servicecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        emservicename: new FormItemModel({
    caption: '服务商名称', detailType: 'FORMITEM', name: 'emservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        range: new FormItemModel({
    caption: '服务商归属', detailType: 'FORMITEM', name: 'range', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        servicegroup: new FormItemModel({
    caption: '服务商分组', detailType: 'FORMITEM', name: 'servicegroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labservicelevelid: new FormItemModel({
    caption: '级别', detailType: 'FORMITEM', name: 'labservicelevelid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        labservicetypeid: new FormItemModel({
    caption: '服务商类型', detailType: 'FORMITEM', name: 'labservicetypeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sums: new FormItemModel({
    caption: '评估得分', detailType: 'FORMITEM', name: 'sums', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        servicestate: new FormItemModel({
    caption: '服务商状态', detailType: 'FORMITEM', name: 'servicestate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emserviceid: new FormItemModel({
    caption: '服务商标识', detailType: 'FORMITEM', name: 'emserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof InfoBase
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}