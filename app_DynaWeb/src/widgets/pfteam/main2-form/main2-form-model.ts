/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'pfteamid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'pfteamname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'teamcode',
        prop: 'teamcode',
        dataType: 'TEXT',
      },
      {
        name: 'pfteamname',
        prop: 'pfteamname',
        dataType: 'TEXT',
      },
      {
        name: 'teamtypeid',
        prop: 'teamtypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'teamfn',
        prop: 'teamfn',
        dataType: 'TEXT',
      },
      {
        name: 'teamcapacity',
        prop: 'teamcapacity',
        dataType: 'INT',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'pfteamid',
        prop: 'pfteamid',
        dataType: 'GUID',
      },
      {
        name: 'pfteam',
        prop: 'pfteamid',
        dataType: 'FONTKEY',
      },
    ]
  }

}