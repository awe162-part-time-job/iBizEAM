/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'rfodecode',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfodeinfo',
      },
      {
        name: 'emrfode',
        prop: 'emrfodeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emrfodename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'updateman',
      },
    ]
  }


}