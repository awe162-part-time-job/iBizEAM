/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emeqkp',
        prop: 'emeqkpid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'kpscope',
      },
      {
        name: 'kpdesc',
      },
      {
        name: 'createman',
      },
      {
        name: 'normalrefval',
      },
      {
        name: 'kpinfo',
      },
      {
        name: 'kptypeid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'kpcode',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'emeqkpname',
      },
    ]
  }


}