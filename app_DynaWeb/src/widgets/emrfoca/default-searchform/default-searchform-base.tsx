import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMRFOCAService from '@/service/emrfoca/emrfoca-service';
import DefaultService from './default-searchform-service';
import EMRFOCAUIService from '@/uiservice/emrfoca/emrfoca-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMRFOCAService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMRFOCAService = new EMRFOCAService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emrfoca';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '原因';

    /**
     * 界面UI服务对象
     *
     * @type {EMRFOCAUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMRFOCAUIService = new EMRFOCAUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_emrfocaname_like: null,
        n_rfodename_like: null,
        n_rfomoname_like: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_emrfocaname_like: new FormItemModel({ caption: '原因名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_emrfocaname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_rfodename_like: new FormItemModel({ caption: '现象(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfodename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_rfomoname_like: new FormItemModel({ caption: '模式(文本包含(%))', detailType: 'FORMITEM', name: 'n_rfomoname_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}