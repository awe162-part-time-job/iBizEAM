/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emapplyid',
          prop: 'emapplyid',
          dataType: 'GUID',
        },
        {
          name: 'emapplyname',
          prop: 'emapplyname',
          dataType: 'TEXT',
        },
        {
          name: 'mpersonname',
          prop: 'mpersonname',
          dataType: 'TEXT',
        },
        {
          name: 'applydate',
          prop: 'applydate',
          dataType: 'DATETIME',
        },
        {
          name: 'rempname',
          prop: 'rempname',
          dataType: 'TEXT',
        },
        {
          name: 'prefee1',
          prop: 'prefee1',
          dataType: 'FLOAT',
        },
        {
          name: 'prefee',
          prop: 'prefee',
          dataType: 'FLOAT',
        },
        {
          name: 'rservicename',
          prop: 'rservicename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'fp',
          prop: 'fp',
          dataType: 'TEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'pfee',
          prop: 'pfee',
          dataType: 'FLOAT',
        },
        {
          name: 'mfee',
          prop: 'mfee',
          dataType: 'CURRENCY',
        },
        {
          name: 'sfee',
          prop: 'sfee',
          dataType: 'FLOAT',
        },
        {
          name: 'applystate',
          prop: 'applystate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'applytype',
          prop: 'applytype',
          dataType: 'SSCODELIST',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'priority',
          prop: 'priority',
          dataType: 'SSCODELIST',
        },
        {
          name: 'applybdate',
          prop: 'applybdate',
          dataType: 'DATETIME',
        },
        {
          name: 'applyedate',
          prop: 'applyedate',
          dataType: 'DATETIME',
        },
        {
          name: 'closeempname',
          prop: 'closeempname',
          dataType: 'TEXT',
        },
        {
          name: 'closedate',
          prop: 'closedate',
          dataType: 'DATETIME',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfocaid',
          prop: 'rfocaid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmstag',
        },
        {
          name: 'srfmajortext',
          prop: 'emapplyname',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'emapplyid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'emapplyid',
          dataType: 'GUID',
        },
        {
          name: 'rteamid',
          prop: 'rteamid',
          dataType: 'PICKUP',
        },
        {
          name: 'objid',
          prop: 'objid',
          dataType: 'PICKUP',
        },
        {
          name: 'rserviceid',
          prop: 'rserviceid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfoacid',
          prop: 'rfoacid',
          dataType: 'PICKUP',
        },
        {
          name: 'emapply',
          prop: 'emapplyid',
        },
      {
        name: 'n_equipname_like',
        prop: 'n_equipname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'n_objname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_applytype_eq',
        prop: 'n_applytype_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_applystate_eq',
        prop: 'n_applystate_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}