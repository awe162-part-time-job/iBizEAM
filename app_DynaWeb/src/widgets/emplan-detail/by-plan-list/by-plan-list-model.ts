/**
 * ByPlan 部件模型
 *
 * @export
 * @class ByPlanModel
 */
export default class ByPlanModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ByPlanDashboard_sysportlet4_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'dptype',
        codelist:{tag:'EMOBJTYPE',codelistType:'STATIC'},
			},
			{
				name: 'orderflag',
			},
			{
				name: 'recvpersonid',
			},
			{
				name: 'equipid',
			},
			{
				name: 'objname',
			},
			{
				name: 'objid',
			},
			{
				name: 'emplandetailname',
			},
			{
				name: 'dpname',
			},
			{
				name: 'equipname',
			},
			{
				name: 'recvpersonname',
			},
			{
				name: 'dpid',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}