/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main6Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'empodetailid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'empodetailname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'empodetailid',
        prop: 'empodetailid',
        dataType: 'GUID',
      },
      {
        name: 'poname',
        prop: 'poname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'porempid',
        prop: 'porempid',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'porempname',
        prop: 'porempname',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'wplistname',
        prop: 'wplistname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'listprice',
        prop: 'listprice',
        dataType: 'FLOAT',
      },
      {
        name: 'discnt',
        prop: 'discnt',
        dataType: 'FLOAT',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'taxrate',
        prop: 'taxrate',
        dataType: 'FLOAT',
      },
      {
        name: 'totalprice',
        prop: 'totalprice',
        dataType: 'FLOAT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'unitrate',
        prop: 'unitrate',
        dataType: 'FLOAT',
      },
      {
        name: 'civo',
        prop: 'civo',
        dataType: 'TEXT',
      },
      {
        name: 'podetailstate',
        prop: 'podetailstate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'yiju',
        prop: 'yiju',
        dataType: 'TEXT',
      },
      {
        name: 'civocopy',
        prop: 'civocopy',
        dataType: 'TEXT',
      },
      {
        name: 'useto',
        prop: 'useto',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'equips',
        prop: 'equips',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rdate',
        prop: 'rdate',
        dataType: 'DATETIME',
      },
      {
        name: 'rsum',
        prop: 'rsum',
        dataType: 'FLOAT',
      },
      {
        name: 'runitname',
        prop: 'runitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rprice',
        prop: 'rprice',
        dataType: 'FLOAT',
      },
      {
        name: 'avgtaxfee',
        prop: 'avgtaxfee',
        dataType: 'FLOAT',
      },
      {
        name: 'avgtsfee',
        prop: 'avgtsfee',
        dataType: 'FLOAT',
      },
      {
        name: 'shf',
        prop: 'shf',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'sumall',
        prop: 'sumall',
        dataType: 'FLOAT',
      },
      {
        name: 'attprice',
        prop: 'attprice',
        dataType: 'INT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'unitid',
        prop: 'unitid',
        dataType: 'PICKUP',
      },
      {
        name: 'runitid',
        prop: 'runitid',
        dataType: 'PICKUP',
      },
      {
        name: 'poid',
        prop: 'poid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'wplistid',
        prop: 'wplistid',
        dataType: 'PICKUP',
      },
      {
        name: 'empodetail',
        prop: 'empodetailid',
        dataType: 'FONTKEY',
      },
    ]
  }

}