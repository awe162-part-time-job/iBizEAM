/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'attprice',
          prop: 'attprice',
          dataType: 'INT',
        },
        {
          name: 'shf',
          prop: 'shf',
          dataType: 'FLOAT',
        },
        {
          name: 'orderflag',
          prop: 'orderflag',
          dataType: 'INT',
        },
        {
          name: 'poname',
          prop: 'poname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'podetailstate',
          prop: 'podetailstate',
          dataType: 'NSCODELIST',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'psum',
          prop: 'psum',
          dataType: 'FLOAT',
        },
        {
          name: 'totalprice',
          prop: 'totalprice',
          dataType: 'FLOAT',
        },
        {
          name: 'unitrate',
          prop: 'unitrate',
          dataType: 'FLOAT',
        },
        {
          name: 'civo',
          prop: 'civo',
          dataType: 'TEXT',
        },
        {
          name: 'rdate',
          prop: 'rdate',
          dataType: 'DATETIME',
        },
        {
          name: 'rsum',
          prop: 'rsum',
          dataType: 'FLOAT',
        },
        {
          name: 'runitname',
          prop: 'runitname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rprice',
          prop: 'rprice',
          dataType: 'FLOAT',
        },
        {
          name: 'avgtsfee',
          prop: 'avgtsfee',
          dataType: 'FLOAT',
        },
        {
          name: 'avgtaxfee',
          prop: 'avgtaxfee',
          dataType: 'FLOAT',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'FLOAT',
        },
        {
          name: 'wplistname',
          prop: 'wplistname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'useto',
          prop: 'useto',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'objname',
          prop: 'objname',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'equips',
          prop: 'equips',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'updatedate',
          prop: 'updatedate',
          dataType: 'DATETIME',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'empodetailname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'empodetailid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'empodetailid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'empid',
          prop: 'empid',
          dataType: 'PICKUP',
        },
        {
          name: 'unitid',
          prop: 'unitid',
          dataType: 'PICKUP',
        },
        {
          name: 'rempid',
          prop: 'rempid',
          dataType: 'PICKUP',
        },
        {
          name: 'runitid',
          prop: 'runitid',
          dataType: 'PICKUP',
        },
        {
          name: 'wplistid',
          prop: 'wplistid',
          dataType: 'PICKUP',
        },
        {
          name: 'poid',
          prop: 'poid',
          dataType: 'PICKUP',
        },
        {
          name: 'empodetail',
          prop: 'empodetailid',
        },
      {
        name: 'n_itemname_like',
        prop: 'n_itemname_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wplistid_eq',
        prop: 'n_wplistid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_poid_eq',
        prop: 'n_poid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_podetailstate_eq',
        prop: 'n_podetailstate_eq',
        dataType: 'NSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}