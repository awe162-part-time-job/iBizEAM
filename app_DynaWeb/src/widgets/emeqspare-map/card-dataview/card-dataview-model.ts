/**
 * Card 部件模型
 *
 * @export
 * @class CardModel
 */
export default class CardModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof CardDataViewMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'refobjname',
			},
			{
				name: 'srfmajortext',
				prop: 'emeqsparemapname',
				dataType: 'TEXT',
			},
			{
				name: 'srfkey',
				prop: 'emeqsparemapid',
				dataType: 'GUID',
			},

			{
				name: 'emeqsparemap',
				prop: 'emeqsparemapid',
				dataType: 'FONTKEY',
			},


      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      {
        name:'srfparentdata',
        prop:'srfparentdata'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}