import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMResItemService from '@/service/emres-item/emres-item-service';
import AmountByTypeByEQModel from './amount-by-type-by-eq-chart-model';


/**
 * AmountByTypeByEQ 部件服务对象
 *
 * @export
 * @class AmountByTypeByEQService
 */
export default class AmountByTypeByEQService extends ControlService {

    /**
     * 物品资源服务对象
     *
     * @type {EMResItemService}
     * @memberof AmountByTypeByEQService
     */
    public appEntityService: EMResItemService = new EMResItemService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof AmountByTypeByEQService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of AmountByTypeByEQService.
     * 
     * @param {*} [opts={}]
     * @memberof AmountByTypeByEQService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new AmountByTypeByEQModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AmountByTypeByEQService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}