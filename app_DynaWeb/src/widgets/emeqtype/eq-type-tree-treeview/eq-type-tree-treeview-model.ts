/**
 * EqTypeTree 部件模型
 *
 * @export
 * @class EqTypeTreeModel
 */
export default class EqTypeTreeModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof EqTypeTreeModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'eqtypegroup',
      },
      {
        name: 'createman',
      },
      {
        name: 'eqstateinfo',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'eqtypecode',
      },
      {
        name: 'stype',
      },
      {
        name: 'eqtypeinfo',
      },
      {
        name: 'arg',
      },
      {
        name: 'orgid',
      },
      {
        name: 'emeqtype',
        prop: 'emeqtypeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'sname',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqtypename',
      },
      {
        name: 'description',
      },
      {
        name: 'eqtypepcode',
      },
      {
        name: 'eqtypepname',
      },
      {
        name: 'eqtypepid',
      },
    ]
  }


}