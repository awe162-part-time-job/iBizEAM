import EMEITIRes_en_US_Base from './emeitires_en_US_base';

function getLocaleResource(){
    const EMEITIRes_en_US_OwnData = {};
    const targetData = Object.assign(EMEITIRes_en_US_Base(), EMEITIRes_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
