import EMMachModel_zh_CN_Base from './emmach-model_zh_CN_base';

function getLocaleResource(){
    const EMMachModel_zh_CN_OwnData = {};
    const targetData = Object.assign(EMMachModel_zh_CN_Base(), EMMachModel_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;