import EMPODetail_en_US_Base from './empodetail_en_US_base';

function getLocaleResource(){
    const EMPODetail_en_US_OwnData = {};
    const targetData = Object.assign(EMPODetail_en_US_Base(), EMPODetail_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
