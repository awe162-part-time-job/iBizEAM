import EMEQSpareDetail_en_US_Base from './emeqspare-detail_en_US_base';

function getLocaleResource(){
    const EMEQSpareDetail_en_US_OwnData = {};
    const targetData = Object.assign(EMEQSpareDetail_en_US_Base(), EMEQSpareDetail_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
