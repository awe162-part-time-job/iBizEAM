import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			assetclasscode: commonLogic.appcommonhandle("资产科目代码",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			reserver: commonLogic.appcommonhandle("RESERVER",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			description: commonLogic.appcommonhandle("描述",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			reserver2: commonLogic.appcommonhandle("RESERVER2",null),
			emassetclassname: commonLogic.appcommonhandle("资产类别名称",null),
			assetclassgroup: commonLogic.appcommonhandle("固定资产科目分类",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			emassetclassid: commonLogic.appcommonhandle("资产类别标识",null),
			life: commonLogic.appcommonhandle("折旧期",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			assetclassinfo: commonLogic.appcommonhandle("资产科目信息",null),
			assetclasspname: commonLogic.appcommonhandle("上级科目",null),
			assetclasspcode: commonLogic.appcommonhandle("上级科目代码",null),
			assetclasspid: commonLogic.appcommonhandle("上级科目编号",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("资产类别",null),
					title: commonLogic.appcommonhandle("资产类别",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("资产类别",null),
					title: commonLogic.appcommonhandle("资产类别选择表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("ASSET资产类别信息",null),
					title: commonLogic.appcommonhandle("ASSET资产类别信息",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("资产类别",null),
					title: commonLogic.appcommonhandle("资产类别数据选择视图",null),
				},
				editview_4778: {
					caption: commonLogic.appcommonhandle("ASSET资产类别信息",null),
					title: commonLogic.appcommonhandle("ASSET资产类别信息",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("ASSET资产类别信息",null), 
					grouppanel7: commonLogic.appcommonhandle("附属信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("资产类别标识",null), 
					srfmajortext: commonLogic.appcommonhandle("资产类别名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					assetclasscode: commonLogic.appcommonhandle("资产科目代码",null), 
					emassetclassname: commonLogic.appcommonhandle("资产类别名称",null), 
					assetclasspname: commonLogic.appcommonhandle("上级科目",null), 
					assetclassgroup: commonLogic.appcommonhandle("固定资产科目分类",null), 
					life: commonLogic.appcommonhandle("折旧期",null), 
					emassetclassid: commonLogic.appcommonhandle("资产类别标识",null), 
					assetclasspid: commonLogic.appcommonhandle("上级科目编号",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					emassetclassname: commonLogic.appcommonhandle("资产类别名称",null),
					assetclasscode: commonLogic.appcommonhandle("资产科目代码",null),
					life: commonLogic.appcommonhandle("折旧期",null),
					assetclasspname: commonLogic.appcommonhandle("上级科目",null),
					assetclasspcode: commonLogic.appcommonhandle("上级科目代码",null),
					assetclassgroup: commonLogic.appcommonhandle("固定资产科目分类",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editview_4778toolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;