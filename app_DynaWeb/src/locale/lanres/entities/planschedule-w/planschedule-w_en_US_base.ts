import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			planschedule_wid: commonLogic.appcommonhandle("计划_按周标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			planschedule_wname: commonLogic.appcommonhandle("计划_按周名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			scheduleparam: commonLogic.appcommonhandle("时刻参数",null),
			emplanid: commonLogic.appcommonhandle("计划编号",null),
			intervalminute: commonLogic.appcommonhandle("间隔时间",null),
			cyclestarttime: commonLogic.appcommonhandle("循环开始时间",null),
			cycleendtime: commonLogic.appcommonhandle("循环结束时间",null),
			emplanname: commonLogic.appcommonhandle("计划名称",null),
			schedulestate: commonLogic.appcommonhandle("时刻设置状态",null),
			scheduletype: commonLogic.appcommonhandle("时刻类型",null),
			description: commonLogic.appcommonhandle("描述",null),
			lastminute: commonLogic.appcommonhandle("持续时间",null),
			rundate: commonLogic.appcommonhandle("运行日期",null),
			scheduleparam4: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam2: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam3: commonLogic.appcommonhandle("时刻参数",null),
			runtime: commonLogic.appcommonhandle("执行时间",null),
			taskid: commonLogic.appcommonhandle("定时任务",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("计划_按周",null),
					title: commonLogic.appcommonhandle("计划_按周编辑视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("计划_按周基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划_按周标识",null), 
					srfmajortext: commonLogic.appcommonhandle("计划_按周名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanname: commonLogic.appcommonhandle("计划",null), 
					cyclestarttime: commonLogic.appcommonhandle("循环开始时间",null), 
					cycleendtime: commonLogic.appcommonhandle("循环结束时间",null), 
					scheduleparam2: commonLogic.appcommonhandle("每周的",null), 
					runtime: commonLogic.appcommonhandle("执行时间",null), 
					schedulestate: commonLogic.appcommonhandle("时刻设置状态",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
					planschedule_wid: commonLogic.appcommonhandle("计划_按周标识",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;