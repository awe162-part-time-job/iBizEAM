import PLANSCHEDULE_W_zh_CN_Base from './planschedule-w_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_W_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_W_zh_CN_Base(), PLANSCHEDULE_W_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;