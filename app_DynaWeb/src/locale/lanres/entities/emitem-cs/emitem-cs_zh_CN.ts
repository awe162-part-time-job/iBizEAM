import EMItemCS_zh_CN_Base from './emitem-cs_zh_CN_base';

function getLocaleResource(){
    const EMItemCS_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemCS_zh_CN_Base(), EMItemCS_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;