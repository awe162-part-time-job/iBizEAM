import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("计划时刻设置", null),
		fields: {
			planscheduleid: commonLogic.appcommonhandle("计划时刻设置标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			planschedulename: commonLogic.appcommonhandle("计划时刻设置名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			scheduletype: commonLogic.appcommonhandle("时刻类型",null),
			cyclestarttime: commonLogic.appcommonhandle("循环开始时间",null),
			cycleendtime: commonLogic.appcommonhandle("循环结束时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			intervalminute: commonLogic.appcommonhandle("间隔时间",null),
			rundate: commonLogic.appcommonhandle("运行日期",null),
			runtime: commonLogic.appcommonhandle("执行时间",null),
			scheduleparam: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam2: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam3: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam4: commonLogic.appcommonhandle("时刻参数",null),
			schedulestate: commonLogic.appcommonhandle("时刻设置状态",null),
			emplanid: commonLogic.appcommonhandle("计划编号",null),
			emplanname: commonLogic.appcommonhandle("计划名称",null),
			lastminute: commonLogic.appcommonhandle("持续时间",null),
			taskid: commonLogic.appcommonhandle("定时任务",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("计划时刻设置",null),
					title: commonLogic.appcommonhandle("计划时刻设置编辑视图",null),
				},
				indexpickupview: {
					caption: commonLogic.appcommonhandle("计划时刻设置",null),
					title: commonLogic.appcommonhandle("计划时刻设置数据选择视图",null),
				},
				indexpickupdataview: {
					caption: commonLogic.appcommonhandle("计划时刻设置",null),
					title: commonLogic.appcommonhandle("计划时刻设置索引关系选择数据视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("计划时刻设置",null),
					title: commonLogic.appcommonhandle("计划时刻设置表格视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("计划时刻设置基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划时刻设置标识",null), 
					srfmajortext: commonLogic.appcommonhandle("计划时刻设置名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					planschedulename: commonLogic.appcommonhandle("计划时刻设置名称",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					planscheduleid: commonLogic.appcommonhandle("计划时刻设置标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					emplanname: commonLogic.appcommonhandle("计划名称",null),
					scheduletype: commonLogic.appcommonhandle("时刻类型",null),
					rundate: commonLogic.appcommonhandle("运行日期",null),
					scheduleparam: commonLogic.appcommonhandle("时刻参数",null),
					scheduleparam2: commonLogic.appcommonhandle("时刻参数",null),
					schedulestate: commonLogic.appcommonhandle("时刻设置状态",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			indextype_dataview: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;