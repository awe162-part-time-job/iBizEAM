import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			lastprice: commonLogic.appcommonhandle("最新价格",null),
			objid: commonLogic.appcommonhandle("对象编号",null),
			iteminfo: commonLogic.appcommonhandle("物品信息",null),
			isassetflag: commonLogic.appcommonhandle("按资产",null),
			highsum: commonLogic.appcommonhandle("最高库存",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			amount: commonLogic.appcommonhandle("库存金额",null),
			isbatchflag: commonLogic.appcommonhandle("按批次",null),
			checkmethod: commonLogic.appcommonhandle("验收方法",null),
			emitemid: commonLogic.appcommonhandle("物品标识",null),
			stockdesum: commonLogic.appcommonhandle("库存定额余量",null),
			abctype: commonLogic.appcommonhandle("ABC分类",null),
			shfprice: commonLogic.appcommonhandle("平均税费",null),
			repsum: commonLogic.appcommonhandle("重订量",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			stockextime: commonLogic.appcommonhandle("库存超期",null),
			isnew: commonLogic.appcommonhandle("物品新旧标识",null),
			itemnid: commonLogic.appcommonhandle("物品编码(新)",null),
			itemserialcode: commonLogic.appcommonhandle("产品系列号",null),
			registerdat: commonLogic.appcommonhandle("登记日期",null),
			itemmodelcode: commonLogic.appcommonhandle("产品型号",null),
			itemncode: commonLogic.appcommonhandle("物品代码(新)",null),
			itemcode: commonLogic.appcommonhandle("物品代码",null),
			no3q: commonLogic.appcommonhandle("不足3家供应商",null),
			emitemname: commonLogic.appcommonhandle("物品名称",null),
			dens: commonLogic.appcommonhandle("密度",null),
			sapcontrol: commonLogic.appcommonhandle("sap控制",null),
			batchtype: commonLogic.appcommonhandle("批次类型",null),
			itemgroup: commonLogic.appcommonhandle("物品分组",null),
			lastsum: commonLogic.appcommonhandle("最低库存",null),
			stocksum: commonLogic.appcommonhandle("库存量",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			lastindate: commonLogic.appcommonhandle("最后入料时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			sapcontrolcode: commonLogic.appcommonhandle("sap控制代码",null),
			life: commonLogic.appcommonhandle("寿命周期(天)",null),
			price: commonLogic.appcommonhandle("平均价",null),
			costcenterid: commonLogic.appcommonhandle("成本中心",null),
			stockinl: commonLogic.appcommonhandle("库存周期(天)",null),
			itemdesc: commonLogic.appcommonhandle("物品备注",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			warrantyday: commonLogic.appcommonhandle("保修天数",null),
			storename: commonLogic.appcommonhandle("最新存储仓库",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			emcabname: commonLogic.appcommonhandle("货架",null),
			storecode: commonLogic.appcommonhandle("最新存储仓库",null),
			storepartname: commonLogic.appcommonhandle("最新存储库位",null),
			labservicename: commonLogic.appcommonhandle("建议供应商",null),
			itemmtypeid: commonLogic.appcommonhandle("物品二级类型",null),
			mservicename: commonLogic.appcommonhandle("制造商",null),
			itembtypename: commonLogic.appcommonhandle("物品一级类型",null),
			itembtypeid: commonLogic.appcommonhandle("物品一级类型",null),
			itemtypecode: commonLogic.appcommonhandle("物品类型代码",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类型",null),
			itemtypename: commonLogic.appcommonhandle("物品类型",null),
			emcabid: commonLogic.appcommonhandle("货架",null),
			unitid: commonLogic.appcommonhandle("单位",null),
			storepartid: commonLogic.appcommonhandle("最新存储库位",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			mserviceid: commonLogic.appcommonhandle("制造商",null),
			labserviceid: commonLogic.appcommonhandle("建议供应商",null),
			storeid: commonLogic.appcommonhandle("最新存储仓库",null),
			empid: commonLogic.appcommonhandle("最新采购员",null),
			empname: commonLogic.appcommonhandle("最新采购员",null),
			sempid: commonLogic.appcommonhandle("库管员",null),
			sempname: commonLogic.appcommonhandle("库管员",null),
			lastaempid: commonLogic.appcommonhandle("最新请购人",null),
			lastaempname: commonLogic.appcommonhandle("最新请购人",null),
		},
			views: {
				dashboardview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品数据看板视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品选项操作视图",null),
				},
				allgridview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				baseinfoview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品分页导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				supplyinfoview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				storeinfoview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品选择表格视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("物品",null),
					title: commonLogic.appcommonhandle("物品数据选择视图",null),
				},
			},
			info_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("物品信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemcode: commonLogic.appcommonhandle("物品代码",null), 
					emitemname: commonLogic.appcommonhandle("物品名称",null), 
					itemtypename: commonLogic.appcommonhandle("物品类型",null), 
					itemgroup: commonLogic.appcommonhandle("物品分组",null), 
					dens: commonLogic.appcommonhandle("密度",null), 
					itemmodelcode: commonLogic.appcommonhandle("产品型号",null), 
					itemserialcode: commonLogic.appcommonhandle("产品系列号",null), 
					acclassname: commonLogic.appcommonhandle("总帐科目",null), 
					isassetflag: commonLogic.appcommonhandle("按资产",null), 
					checkmethod: commonLogic.appcommonhandle("验收方法",null), 
					itemdesc: commonLogic.appcommonhandle("物品备注",null), 
					isnew: commonLogic.appcommonhandle("物品新旧标识",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			store_form: {
				details: {
					grouppanel15: commonLogic.appcommonhandle("库存信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storename: commonLogic.appcommonhandle("最新存储仓库",null), 
					storepartname: commonLogic.appcommonhandle("最新存储库位",null), 
					stocksum: commonLogic.appcommonhandle("库存量",null), 
					unitname: commonLogic.appcommonhandle("单位",null), 
					price: commonLogic.appcommonhandle("平均价",null), 
					amount: commonLogic.appcommonhandle("库存金额",null), 
					lastsum: commonLogic.appcommonhandle("最低库存",null), 
					highsum: commonLogic.appcommonhandle("最高库存",null), 
					repsum: commonLogic.appcommonhandle("重订量",null), 
					abctype: commonLogic.appcommonhandle("ABC分类",null), 
					lastprice: commonLogic.appcommonhandle("最新价格",null), 
					stockinl: commonLogic.appcommonhandle("库存周期(天)",null), 
					lastindate: commonLogic.appcommonhandle("最后入料时间",null), 
					sempid: commonLogic.appcommonhandle("库管员",null), 
					sempname: commonLogic.appcommonhandle("库管员",null), 
					lastaempid: commonLogic.appcommonhandle("最新请购人",null), 
					lastaempname: commonLogic.appcommonhandle("最新请购人",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			supply_form: {
				details: {
					grouppanel31: commonLogic.appcommonhandle("供应信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					labservicename: commonLogic.appcommonhandle("建议供应商",null), 
					mservicename: commonLogic.appcommonhandle("制造商",null), 
					warrantyday: commonLogic.appcommonhandle("保修天数",null), 
					no3q: commonLogic.appcommonhandle("不足3家供应商",null), 
					life: commonLogic.appcommonhandle("寿命周期(天)",null), 
					empid: commonLogic.appcommonhandle("最新采购员",null), 
					empname: commonLogic.appcommonhandle("最新采购员",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("ITEM物品信息",null), 
					grouppanel15: commonLogic.appcommonhandle("库存信息",null), 
					grouppanel31: commonLogic.appcommonhandle("供应信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemcode: commonLogic.appcommonhandle("物品代码",null), 
					emitemname: commonLogic.appcommonhandle("物品名称",null), 
					itemgroup: commonLogic.appcommonhandle("物品分组",null), 
					itemtypename: commonLogic.appcommonhandle("物品类型",null), 
					acclassname: commonLogic.appcommonhandle("总帐科目",null), 
					itemmodelcode: commonLogic.appcommonhandle("产品型号",null), 
					itemserialcode: commonLogic.appcommonhandle("产品系列号",null), 
					isassetflag: commonLogic.appcommonhandle("按资产",null), 
					checkmethod: commonLogic.appcommonhandle("验收方法",null), 
					itemdesc: commonLogic.appcommonhandle("物品备注",null), 
					dens: commonLogic.appcommonhandle("密度",null), 
					isnew: commonLogic.appcommonhandle("物品新旧标识",null), 
					storename: commonLogic.appcommonhandle("最新存储仓库",null), 
					storepartname: commonLogic.appcommonhandle("最新存储库位",null), 
					stocksum: commonLogic.appcommonhandle("库存量",null), 
					unitname: commonLogic.appcommonhandle("单位",null), 
					price: commonLogic.appcommonhandle("平均价",null), 
					amount: commonLogic.appcommonhandle("库存金额",null), 
					lastsum: commonLogic.appcommonhandle("最低库存",null), 
					highsum: commonLogic.appcommonhandle("最高库存",null), 
					repsum: commonLogic.appcommonhandle("重订量",null), 
					abctype: commonLogic.appcommonhandle("ABC分类",null), 
					lastprice: commonLogic.appcommonhandle("最新价格",null), 
					stockinl: commonLogic.appcommonhandle("库存周期(天)",null), 
					lastindate: commonLogic.appcommonhandle("最后入料时间",null), 
					sempid: commonLogic.appcommonhandle("库管员",null), 
					sempname: commonLogic.appcommonhandle("库管员",null), 
					lastaempid: commonLogic.appcommonhandle("最新请购人",null), 
					lastaempname: commonLogic.appcommonhandle("最新请购人",null), 
					empid: commonLogic.appcommonhandle("最新采购员",null), 
					empname: commonLogic.appcommonhandle("最新采购员",null), 
					labservicename: commonLogic.appcommonhandle("建议供应商",null), 
					mservicename: commonLogic.appcommonhandle("制造商",null), 
					warrantyday: commonLogic.appcommonhandle("保修天数",null), 
					no3q: commonLogic.appcommonhandle("不足3家供应商",null), 
					life: commonLogic.appcommonhandle("寿命周期(天)",null), 
					acclassid: commonLogic.appcommonhandle("总帐科目",null), 
					storepartid: commonLogic.appcommonhandle("最新存储库位",null), 
					mserviceid: commonLogic.appcommonhandle("制造商",null), 
					storeid: commonLogic.appcommonhandle("最新存储仓库",null), 
					unitid: commonLogic.appcommonhandle("单位",null), 
					labserviceid: commonLogic.appcommonhandle("建议供应商",null), 
					itemtypeid: commonLogic.appcommonhandle("物品类型",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("ITEM物品信息",null), 
					grouppanel15: commonLogic.appcommonhandle("库存信息",null), 
					grouppanel31: commonLogic.appcommonhandle("供应信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel39: commonLogic.appcommonhandle("操作信息",null), 
					formpage38: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemcode: commonLogic.appcommonhandle("物品代码",null), 
					emitemname: commonLogic.appcommonhandle("物品名称",null), 
					itemgroup: commonLogic.appcommonhandle("物品分组",null), 
					itemtypename: commonLogic.appcommonhandle("物品类型",null), 
					acclassname: commonLogic.appcommonhandle("总帐科目",null), 
					itemmodelcode: commonLogic.appcommonhandle("产品型号",null), 
					itemserialcode: commonLogic.appcommonhandle("产品系列号",null), 
					isassetflag: commonLogic.appcommonhandle("按资产",null), 
					checkmethod: commonLogic.appcommonhandle("验收方法",null), 
					itemdesc: commonLogic.appcommonhandle("物品备注",null), 
					dens: commonLogic.appcommonhandle("密度",null), 
					isnew: commonLogic.appcommonhandle("物品新旧标识",null), 
					storename: commonLogic.appcommonhandle("最新存储仓库",null), 
					storepartname: commonLogic.appcommonhandle("最新存储库位",null), 
					stocksum: commonLogic.appcommonhandle("库存量",null), 
					unitname: commonLogic.appcommonhandle("单位",null), 
					price: commonLogic.appcommonhandle("平均价",null), 
					amount: commonLogic.appcommonhandle("库存金额",null), 
					lastsum: commonLogic.appcommonhandle("最低库存",null), 
					highsum: commonLogic.appcommonhandle("最高库存",null), 
					repsum: commonLogic.appcommonhandle("重订量",null), 
					abctype: commonLogic.appcommonhandle("ABC分类",null), 
					lastprice: commonLogic.appcommonhandle("最新价格",null), 
					stockinl: commonLogic.appcommonhandle("库存周期(天)",null), 
					lastindate: commonLogic.appcommonhandle("最后入料时间",null), 
					sempid: commonLogic.appcommonhandle("库管员",null), 
					sempname: commonLogic.appcommonhandle("库管员",null), 
					lastaempid: commonLogic.appcommonhandle("最新请购人",null), 
					lastaempname: commonLogic.appcommonhandle("最新请购人",null), 
					empid: commonLogic.appcommonhandle("最新采购员",null), 
					empname: commonLogic.appcommonhandle("最新采购员",null), 
					labservicename: commonLogic.appcommonhandle("建议供应商",null), 
					mservicename: commonLogic.appcommonhandle("制造商",null), 
					warrantyday: commonLogic.appcommonhandle("保修天数",null), 
					no3q: commonLogic.appcommonhandle("不足3家供应商",null), 
					life: commonLogic.appcommonhandle("寿命周期(天)",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			quickcreate_form: {
				details: {
					group1: commonLogic.appcommonhandle("物品基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("物品标识",null), 
					srfmajortext: commonLogic.appcommonhandle("物品名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemcode: commonLogic.appcommonhandle("物品代码",null), 
					emitemname: commonLogic.appcommonhandle("物品名称",null), 
					itemgroup: commonLogic.appcommonhandle("物品分组",null), 
					itemtypename: commonLogic.appcommonhandle("物品类型",null), 
					unitname: commonLogic.appcommonhandle("单位",null), 
					storename: commonLogic.appcommonhandle("最新存储仓库",null), 
					storepartname: commonLogic.appcommonhandle("最新存储库位",null), 
					storepartid: commonLogic.appcommonhandle("最新存储库位",null), 
					storeid: commonLogic.appcommonhandle("最新存储仓库",null), 
					unitid: commonLogic.appcommonhandle("单位",null), 
					itemtypeid: commonLogic.appcommonhandle("物品类型",null), 
					emitemid: commonLogic.appcommonhandle("物品标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					itemcode: commonLogic.appcommonhandle("物品代码",null),
					emitemname: commonLogic.appcommonhandle("物品名称",null),
					itemtypename: commonLogic.appcommonhandle("物品类型",null),
					stocksum: commonLogic.appcommonhandle("库存量",null),
					unitname: commonLogic.appcommonhandle("单位",null),
					amount: commonLogic.appcommonhandle("库存金额",null),
					uagridcolumn1: commonLogic.appcommonhandle("操作",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				emitem_openeditview: commonLogic.appcommonhandle("编辑",null),
				emitem_remove: commonLogic.appcommonhandle("删除",null),
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emitemname_like: commonLogic.appcommonhandle("物品名称(文本包含(%))",null), 
					n_itemtypename_like: commonLogic.appcommonhandle("物品类型(文本包含(%))",null), 
					n_labservicename_like: commonLogic.appcommonhandle("建议供应商(文本包含(%))",null), 
					n_mservicename_like: commonLogic.appcommonhandle("制造商(文本包含(%))",null), 
					n_storeid_eq: commonLogic.appcommonhandle("最新存储仓库(等于(=))",null), 
					n_storepartid_eq: commonLogic.appcommonhandle("最新存储库位(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			allgridviewtoolbar_toolbar: {
				tbitem1_openquickcreate: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem1_openquickcreate: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("物品信息",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("库存数量",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("出入记录",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("采购信息",null),
					}
				},
				uiactions: {
				},
			},
			dashboardviewdashboard_container1_portlet: {
				uiactions: {
				},
			},
			baseinfo_portlet: {
				baseinfo: {
					title: commonLogic.appcommonhandle("基本信息", null)
			  	},
				uiactions: {
				},
			},
			store_portlet: {
				store: {
					title: commonLogic.appcommonhandle("库存信息", null)
			  	},
				uiactions: {
				},
			},
			supply_portlet: {
				supply: {
					title: commonLogic.appcommonhandle("供应信息", null)
			  	},
				uiactions: {
				},
			},
			dashboardviewdashboard_container2_portlet: {
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;