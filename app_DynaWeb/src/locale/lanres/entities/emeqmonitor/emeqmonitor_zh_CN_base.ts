import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备状态监控", null),
		fields: {
			edate: commonLogic.appcommonhandle("状态区间截至",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			val: commonLogic.appcommonhandle("运行状态",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emeqmonitorid: commonLogic.appcommonhandle("设备状态监控标识",null),
			emeqmonitorname: commonLogic.appcommonhandle("设备状态监控名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			bdate: commonLogic.appcommonhandle("状态区间起始",null),
			woname: commonLogic.appcommonhandle("工单",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			woid: commonLogic.appcommonhandle("工单",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			views: {
				monitoreditview: {
					caption: commonLogic.appcommonhandle("设备状态监控",null),
					title: commonLogic.appcommonhandle("设备状态监控编辑视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("设备状态监控",null),
					title: commonLogic.appcommonhandle("设备状态监控表格视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("设备状态监控信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备状态监控标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备状态监控名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					val: commonLogic.appcommonhandle("运行状态",null), 
					bdate: commonLogic.appcommonhandle("状态区间起始",null), 
					edate: commonLogic.appcommonhandle("状态区间截至",null), 
					woname: commonLogic.appcommonhandle("工单",null), 
					emeqmonitorid: commonLogic.appcommonhandle("设备状态监控标识",null), 
					woid: commonLogic.appcommonhandle("工单",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					equipname: commonLogic.appcommonhandle("设备",null),
					val: commonLogic.appcommonhandle("运行状态",null),
					bdate: commonLogic.appcommonhandle("状态区间起始",null),
					edate: commonLogic.appcommonhandle("状态区间截至",null),
					woname: commonLogic.appcommonhandle("工单",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_val_eq: commonLogic.appcommonhandle("运行状态(等于(=))",null), 
					n_woname_like: commonLogic.appcommonhandle("工单(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			monitoreditviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;