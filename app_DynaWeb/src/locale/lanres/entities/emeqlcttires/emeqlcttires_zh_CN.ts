import EMEQLCTTIRes_zh_CN_Base from './emeqlcttires_zh_CN_base';

function getLocaleResource(){
    const EMEQLCTTIRes_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQLCTTIRes_zh_CN_Base(), EMEQLCTTIRes_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;