import EMEQDebug_en_US_Base from './emeqdebug_en_US_base';

function getLocaleResource(){
    const EMEQDebug_en_US_OwnData = {};
    const targetData = Object.assign(EMEQDebug_en_US_Base(), EMEQDebug_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
