import EMService_zh_CN_Base from './emservice_zh_CN_base';

function getLocaleResource(){
    const EMService_zh_CN_OwnData = {};
    const targetData = Object.assign(EMService_zh_CN_Base(), EMService_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;