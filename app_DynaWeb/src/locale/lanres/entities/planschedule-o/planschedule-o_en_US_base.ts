import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			planschedule_oid: commonLogic.appcommonhandle("自定义间隔天数标识",null),
			planschedule_oname: commonLogic.appcommonhandle("自定义间隔天数名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			intervalminute: commonLogic.appcommonhandle("间隔时间",null),
			emplanid: commonLogic.appcommonhandle("计划编号",null),
			scheduleparam: commonLogic.appcommonhandle("时刻参数",null),
			cyclestarttime: commonLogic.appcommonhandle("循环开始时间",null),
			scheduletype: commonLogic.appcommonhandle("时刻类型",null),
			cycleendtime: commonLogic.appcommonhandle("循环结束时间",null),
			emplanname: commonLogic.appcommonhandle("计划名称",null),
			schedulestate: commonLogic.appcommonhandle("时刻设置状态",null),
			lastminute: commonLogic.appcommonhandle("持续时间",null),
			scheduleparam4: commonLogic.appcommonhandle("时刻参数",null),
			scheduleparam2: commonLogic.appcommonhandle("时刻参数",null),
			description: commonLogic.appcommonhandle("描述",null),
			runtime: commonLogic.appcommonhandle("执行时间",null),
			rundate: commonLogic.appcommonhandle("运行日期",null),
			scheduleparam3: commonLogic.appcommonhandle("时刻参数",null),
			taskid: commonLogic.appcommonhandle("定时任务",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("自定义间隔天数",null),
					title: commonLogic.appcommonhandle("自定义间隔天数编辑视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("自定义间隔天数基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("自定义间隔天数标识",null), 
					srfmajortext: commonLogic.appcommonhandle("自定义间隔天数名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanname: commonLogic.appcommonhandle("计划",null), 
					cyclestarttime: commonLogic.appcommonhandle("首次执行日期",null), 
					intervalminute: commonLogic.appcommonhandle("间隔时间",null), 
					schedulestate: commonLogic.appcommonhandle("时刻设置状态",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
					planschedule_oid: commonLogic.appcommonhandle("自定义间隔天数标识",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;