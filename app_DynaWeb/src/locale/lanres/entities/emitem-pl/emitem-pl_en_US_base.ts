import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			sdate: commonLogic.appcommonhandle("损溢日期",null),
			price: commonLogic.appcommonhandle("单价",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			amount: commonLogic.appcommonhandle("总金额",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			batcode: commonLogic.appcommonhandle("批次",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			psum: commonLogic.appcommonhandle("数量",null),
			tradestate: commonLogic.appcommonhandle("损溢状态",null),
			sap: commonLogic.appcommonhandle("sap传输状态",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			emitemplid: commonLogic.appcommonhandle("损溢单号",null),
			itemroutinfo: commonLogic.appcommonhandle("损溢单信息",null),
			emitemplname: commonLogic.appcommonhandle("损溢单名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			sapreason1: commonLogic.appcommonhandle("sap传输失败原因",null),
			inoutflag: commonLogic.appcommonhandle("损益出入标志",null),
			description: commonLogic.appcommonhandle("描述",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			sapcontrol: commonLogic.appcommonhandle("sap控制",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			storepartname: commonLogic.appcommonhandle("库位",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			rname: commonLogic.appcommonhandle("入库单",null),
			rid: commonLogic.appcommonhandle("入库单",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			storepartid: commonLogic.appcommonhandle("库位",null),
			sempid: commonLogic.appcommonhandle("损溢人",null),
			sempname: commonLogic.appcommonhandle("损溢人",null),
		},
			views: {
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("损溢单-已确认",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单编辑视图",null),
				},
				draftgridview: {
					caption: commonLogic.appcommonhandle("损溢单-草稿",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单分页导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				editview9_new: {
					caption: commonLogic.appcommonhandle("损溢单",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("损溢单-待确认",null),
					title: commonLogic.appcommonhandle("损溢单",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("损溢出单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel15: commonLogic.appcommonhandle("操作信息",null), 
					formpage14: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("损溢单号",null), 
					srfmajortext: commonLogic.appcommonhandle("损溢单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemplid: commonLogic.appcommonhandle("损溢出单号(自动)",null), 
					inoutflag: commonLogic.appcommonhandle("损益出入标志",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					sdate: commonLogic.appcommonhandle("损溢日期",null), 
					storeid: commonLogic.appcommonhandle("仓库",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartid: commonLogic.appcommonhandle("库位",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					sempid: commonLogic.appcommonhandle("损溢人",null), 
					sempname: commonLogic.appcommonhandle("损溢人",null), 
					emitemplname: commonLogic.appcommonhandle("损溢单名称",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("损溢出单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel15: commonLogic.appcommonhandle("操作信息",null), 
					formpage14: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("损溢单号",null), 
					srfmajortext: commonLogic.appcommonhandle("损溢单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emitemplid: commonLogic.appcommonhandle("损溢出单号(自动)",null), 
					inoutflag: commonLogic.appcommonhandle("损益出入标志",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					sdate: commonLogic.appcommonhandle("损溢日期",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					psum: commonLogic.appcommonhandle("数量",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					amount: commonLogic.appcommonhandle("总金额",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					sempid: commonLogic.appcommonhandle("损溢人",null), 
					sempname: commonLogic.appcommonhandle("损溢人",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("损溢单基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("损溢单号",null), 
					srfmajortext: commonLogic.appcommonhandle("损溢单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					itemroutinfo: commonLogic.appcommonhandle("损溢单信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emitemplid: commonLogic.appcommonhandle("损溢单号",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					emitemplid: commonLogic.appcommonhandle("损溢单号",null),
					inoutflag: commonLogic.appcommonhandle("损益出入标志",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					sdate: commonLogic.appcommonhandle("损溢日期",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					storepartname: commonLogic.appcommonhandle("库位",null),
					psum: commonLogic.appcommonhandle("数量",null),
					price: commonLogic.appcommonhandle("单价",null),
					amount: commonLogic.appcommonhandle("总金额",null),
					batcode: commonLogic.appcommonhandle("批次",null),
					tradestate: commonLogic.appcommonhandle("损溢状态",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_itemname_like: commonLogic.appcommonhandle("物品(文本包含(%))",null), 
					n_inoutflag_eq: commonLogic.appcommonhandle("损益出入标志(等于(=))",null), 
					n_storeid_eq: commonLogic.appcommonhandle("仓库(等于(=))",null), 
					n_storepartid_eq: commonLogic.appcommonhandle("库位(等于(=))",null), 
					n_tradestate_eq: commonLogic.appcommonhandle("损溢状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			editview9_newtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_rejected: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_confirm: {
					caption: commonLogic.appcommonhandle("确认",null),
					tip: commonLogic.appcommonhandle("确认",null),
				},
				tbitem1_rejected: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("全部损溢单",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("待确认",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已确认",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;