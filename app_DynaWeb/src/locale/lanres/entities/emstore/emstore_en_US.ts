import EMStore_en_US_Base from './emstore_en_US_base';

function getLocaleResource(){
    const EMStore_en_US_OwnData = {};
    const targetData = Object.assign(EMStore_en_US_Base(), EMStore_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
