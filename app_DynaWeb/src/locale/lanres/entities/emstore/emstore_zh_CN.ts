import EMStore_zh_CN_Base from './emstore_zh_CN_base';

function getLocaleResource(){
    const EMStore_zh_CN_OwnData = {};
    const targetData = Object.assign(EMStore_zh_CN_Base(), EMStore_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;