import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("仓库", null),
		fields: {
			description: commonLogic.appcommonhandle("描述",null),
			storecode: commonLogic.appcommonhandle("仓库代码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			storeinfo: commonLogic.appcommonhandle("仓库信息",null),
			standpriceflag: commonLogic.appcommonhandle("标准价标志",null),
			poweravgflag: commonLogic.appcommonhandle("加权平均标志",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			emstoreid: commonLogic.appcommonhandle("仓库标识",null),
			newstoretypeid: commonLogic.appcommonhandle("NEW仓库类型",null),
			costcenterid: commonLogic.appcommonhandle("成本中心",null),
			mgrpersonid: commonLogic.appcommonhandle("主管经理",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			ioalgo: commonLogic.appcommonhandle("出入算法",null),
			storetypeid: commonLogic.appcommonhandle("仓库类型",null),
			storeaddr: commonLogic.appcommonhandle("地址",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emstorename: commonLogic.appcommonhandle("仓库名称",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			storetel: commonLogic.appcommonhandle("联系电话",null),
			storefax: commonLogic.appcommonhandle("传真",null),
			empid: commonLogic.appcommonhandle("库管员",null),
			empname: commonLogic.appcommonhandle("库管员",null),
		},
			views: {
				treeexpview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库选择表格视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库选项操作视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库",null),
				},
				editview_9924: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库数据选择视图",null),
				},
				gridview_7848: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("仓库",null),
					title: commonLogic.appcommonhandle("仓库",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("仓库信息",null), 
					druipart1: commonLogic.appcommonhandle("库位",null), 
					grouppanel1: commonLogic.appcommonhandle("库位信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("仓库标识",null), 
					srfmajortext: commonLogic.appcommonhandle("仓库名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storecode: commonLogic.appcommonhandle("仓库代码",null), 
					emstoreid: commonLogic.appcommonhandle("仓库标识",null), 
					emstorename: commonLogic.appcommonhandle("仓库名称",null), 
					storetypeid: commonLogic.appcommonhandle("仓库类型",null), 
					empid: commonLogic.appcommonhandle("库管员",null), 
					empname: commonLogic.appcommonhandle("库管员",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("仓库信息",null), 
					druipart1: commonLogic.appcommonhandle("库位",null), 
					grouppanel7: commonLogic.appcommonhandle("库位信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("仓库标识",null), 
					srfmajortext: commonLogic.appcommonhandle("仓库名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storecode: commonLogic.appcommonhandle("仓库代码",null), 
					emstoreid: commonLogic.appcommonhandle("仓库标识",null), 
					emstorename: commonLogic.appcommonhandle("仓库名称",null), 
					storetypeid: commonLogic.appcommonhandle("仓库类型",null), 
					empid: commonLogic.appcommonhandle("库管员",null), 
					empname: commonLogic.appcommonhandle("库管员",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					storecode: commonLogic.appcommonhandle("仓库代码",null),
					emstorename: commonLogic.appcommonhandle("仓库名称",null),
					storetypeid: commonLogic.appcommonhandle("仓库类型",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_1919_grid: {
				columns: {
					storecode: commonLogic.appcommonhandle("仓库代码",null),
					emstorename: commonLogic.appcommonhandle("仓库名称",null),
					storetypeid: commonLogic.appcommonhandle("仓库类型",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview_9924toolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridview_7848toolbar_toolbar: {
				tbitem24: {
					caption: commonLogic.appcommonhandle("行编辑",null),
					tip: commonLogic.appcommonhandle("行编辑",null),
				},
				tbitem25: {
					caption: commonLogic.appcommonhandle("新建行",null),
					tip: commonLogic.appcommonhandle("新建行",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			storetree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
				},
				uiactions: {
					emstore_newstore: commonLogic.appcommonhandle("添加仓库",null),
					emstore_editstore: commonLogic.appcommonhandle("编辑",null),
					remove: commonLogic.appcommonhandle("删除",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;