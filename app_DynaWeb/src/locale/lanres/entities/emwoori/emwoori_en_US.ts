import EMWOORI_en_US_Base from './emwoori_en_US_base';

function getLocaleResource(){
    const EMWOORI_en_US_OwnData = {};
    const targetData = Object.assign(EMWOORI_en_US_Base(), EMWOORI_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
