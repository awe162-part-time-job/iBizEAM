import EMStorePart_zh_CN_Base from './emstore-part_zh_CN_base';

function getLocaleResource(){
    const EMStorePart_zh_CN_OwnData = {};
    const targetData = Object.assign(EMStorePart_zh_CN_Base(), EMStorePart_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;