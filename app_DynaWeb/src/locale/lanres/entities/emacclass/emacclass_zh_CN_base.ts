import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("总帐科目", null),
		fields: {
			emacclassid: commonLogic.appcommonhandle("总帐科目标识",null),
			code: commonLogic.appcommonhandle("代码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			description: commonLogic.appcommonhandle("描述",null),
			emacclassname: commonLogic.appcommonhandle("总帐科目名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("总帐科目",null),
					title: commonLogic.appcommonhandle("总帐科目数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("总帐科目",null),
					title: commonLogic.appcommonhandle("总帐科目选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					emacclassname: commonLogic.appcommonhandle("总帐科目名称",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emacclassname_like: commonLogic.appcommonhandle("总帐科目名称(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;