import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			bpersonid: commonLogic.appcommonhandle("最后借阅人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			content: commonLogic.appcommonhandle("内容",null),
			rempid: commonLogic.appcommonhandle("保管人",null),
			drwgcode: commonLogic.appcommonhandle("文档代码",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			rempname: commonLogic.appcommonhandle("保管人",null),
			efilecontent: commonLogic.appcommonhandle("档案文件",null),
			description: commonLogic.appcommonhandle("描述",null),
			drwgtype: commonLogic.appcommonhandle("文档类型",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			lct: commonLogic.appcommonhandle("存放位置",null),
			emdrwgname: commonLogic.appcommonhandle("文档名称",null),
			drwgstate: commonLogic.appcommonhandle("文档状态",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			emdrwgid: commonLogic.appcommonhandle("文档标识",null),
			drwginfo: commonLogic.appcommonhandle("文档信息",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			bpersonname: commonLogic.appcommonhandle("最后借阅人",null),
			deptid: commonLogic.appcommonhandle("部门查询",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("文档",null),
					title: commonLogic.appcommonhandle("文档",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("文档",null),
					title: commonLogic.appcommonhandle("文档",null),
				},
				treeexpview: {
					caption: commonLogic.appcommonhandle("文档",null),
					title: commonLogic.appcommonhandle("文档",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("文档",null),
					title: commonLogic.appcommonhandle("文档",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("文档",null),
					title: commonLogic.appcommonhandle("文档选项操作视图",null),
				},
			},
			new_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("文档信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("文档标识",null), 
					srfmajortext: commonLogic.appcommonhandle("文档名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					drwgcode: commonLogic.appcommonhandle("文档代码",null), 
					emdrwgname: commonLogic.appcommonhandle("文档名称",null), 
					drwgtype: commonLogic.appcommonhandle("文档类型",null), 
					drwgstate: commonLogic.appcommonhandle("文档状态",null), 
					rempid: commonLogic.appcommonhandle("保管人",null), 
					rempname: commonLogic.appcommonhandle("保管人",null), 
					bpersonname: commonLogic.appcommonhandle("最后借阅人",null), 
					bpersonid: commonLogic.appcommonhandle("最后借阅人",null), 
					lct: commonLogic.appcommonhandle("存放位置",null), 
					emdrwgid: commonLogic.appcommonhandle("文档标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("文档信息",null), 
					grouppanel11: commonLogic.appcommonhandle("文件",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("文档标识",null), 
					srfmajortext: commonLogic.appcommonhandle("文档名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					drwgcode: commonLogic.appcommonhandle("文档代码",null), 
					emdrwgname: commonLogic.appcommonhandle("文档名称",null), 
					drwgtype: commonLogic.appcommonhandle("文档类型",null), 
					drwgstate: commonLogic.appcommonhandle("文档状态",null), 
					rempid: commonLogic.appcommonhandle("保管人",null), 
					rempname: commonLogic.appcommonhandle("保管人",null), 
					bpersonid: commonLogic.appcommonhandle("最后借阅人",null), 
					bpersonname: commonLogic.appcommonhandle("最后借阅人",null), 
					lct: commonLogic.appcommonhandle("存放位置",null), 
					content: commonLogic.appcommonhandle("内容",null), 
					efilecontent: commonLogic.appcommonhandle("档案文件",null), 
					emdrwgid: commonLogic.appcommonhandle("文档标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("文档信息",null), 
					grouppanel11: commonLogic.appcommonhandle("文件",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("文档标识",null), 
					srfmajortext: commonLogic.appcommonhandle("文档名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					drwgcode: commonLogic.appcommonhandle("文档代码",null), 
					emdrwgname: commonLogic.appcommonhandle("文档名称",null), 
					drwgtype: commonLogic.appcommonhandle("文档类型",null), 
					drwgstate: commonLogic.appcommonhandle("文档状态",null), 
					rempid: commonLogic.appcommonhandle("保管人",null), 
					rempname: commonLogic.appcommonhandle("保管人",null), 
					bpersonid: commonLogic.appcommonhandle("最后借阅人",null), 
					bpersonname: commonLogic.appcommonhandle("最后借阅人",null), 
					lct: commonLogic.appcommonhandle("存放位置",null), 
					content: commonLogic.appcommonhandle("内容",null), 
					efilecontent: commonLogic.appcommonhandle("档案文件",null), 
					emdrwgid: commonLogic.appcommonhandle("文档标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					drwgcode: commonLogic.appcommonhandle("文档代码",null),
					emdrwgname: commonLogic.appcommonhandle("文档名称",null),
					drwgtype: commonLogic.appcommonhandle("文档类型",null),
					drwgstate: commonLogic.appcommonhandle("文档状态",null),
					rempname: commonLogic.appcommonhandle("保管人",null),
					lct: commonLogic.appcommonhandle("存放位置",null),
					efilecontent: commonLogic.appcommonhandle("档案文件",null),
					bpersonname: commonLogic.appcommonhandle("最后借阅人",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_drwgcode_like: commonLogic.appcommonhandle("文档代码(%)",null), 
					n_emdrwgname_like: commonLogic.appcommonhandle("文档名称(%)",null), 
					n_drwgtype_eq: commonLogic.appcommonhandle("文档类型(=)",null), 
					n_drwgstate_eq: commonLogic.appcommonhandle("文档状态(=)",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			drtree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
					drtype: commonLogic.appcommonhandle("文档类型",null),
				},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;