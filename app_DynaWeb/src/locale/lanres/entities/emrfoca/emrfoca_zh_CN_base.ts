import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("原因", null),
		fields: {
			description: commonLogic.appcommonhandle("描述",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emrfocaid: commonLogic.appcommonhandle("原因标识",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			rfocacode: commonLogic.appcommonhandle("原因代码",null),
			emrfocaname: commonLogic.appcommonhandle("原因名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			rfocainfo: commonLogic.appcommonhandle("信息",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			objid: commonLogic.appcommonhandle("对象编号",null),
			rfodename: commonLogic.appcommonhandle("现象",null),
			rfomoname: commonLogic.appcommonhandle("模式",null),
			rfodeid: commonLogic.appcommonhandle("现象",null),
			rfomoid: commonLogic.appcommonhandle("模式",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("原因信息",null),
					title: commonLogic.appcommonhandle("原因信息",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("原因",null),
					title: commonLogic.appcommonhandle("原因数据选择视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("原因",null),
					title: commonLogic.appcommonhandle("原因",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("原因",null),
					title: commonLogic.appcommonhandle("原因选择表格视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("原因信息",null), 
					grouppanel7: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("原因标识",null), 
					srfmajortext: commonLogic.appcommonhandle("原因名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					rfocacode: commonLogic.appcommonhandle("原因代码",null), 
					emrfocaname: commonLogic.appcommonhandle("原因名称",null), 
					rfodename: commonLogic.appcommonhandle("现象",null), 
					rfomoname: commonLogic.appcommonhandle("模式",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					rfodeid: commonLogic.appcommonhandle("现象",null), 
					emrfocaid: commonLogic.appcommonhandle("原因标识",null), 
					rfomoid: commonLogic.appcommonhandle("模式",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					rfocainfo: commonLogic.appcommonhandle("信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					rfocacode: commonLogic.appcommonhandle("原因代码",null),
					emrfocaname: commonLogic.appcommonhandle("原因名称",null),
					rfodename: commonLogic.appcommonhandle("现象",null),
					rfomoname: commonLogic.appcommonhandle("模式",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emrfocaname_like: commonLogic.appcommonhandle("原因名称(文本包含(%))",null), 
					n_rfodename_like: commonLogic.appcommonhandle("现象(文本包含(%))",null), 
					n_rfomoname_like: commonLogic.appcommonhandle("模式(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;