import EMRFOMO_en_US_Base from './emrfomo_en_US_base';

function getLocaleResource(){
    const EMRFOMO_en_US_OwnData = {};
    const targetData = Object.assign(EMRFOMO_en_US_Base(), EMRFOMO_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
