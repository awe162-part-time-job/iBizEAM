import EMServiceEvl_zh_CN_Base from './emservice-evl_zh_CN_base';

function getLocaleResource(){
    const EMServiceEvl_zh_CN_OwnData = {};
    const targetData = Object.assign(EMServiceEvl_zh_CN_Base(), EMServiceEvl_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;