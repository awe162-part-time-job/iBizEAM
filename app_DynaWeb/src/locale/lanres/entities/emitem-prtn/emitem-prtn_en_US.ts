import EMItemPRtn_en_US_Base from './emitem-prtn_en_US_base';

function getLocaleResource(){
    const EMItemPRtn_en_US_OwnData = {};
    const targetData = Object.assign(EMItemPRtn_en_US_Base(), EMItemPRtn_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
