import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("设备档案", null),
		fields: {
			pic9: commonLogic.appcommonhandle("图片",null),
			efcheck: commonLogic.appcommonhandle("强检周期(年)",null),
			efcheckndate: commonLogic.appcommonhandle("下次检测日期",null),
			replacecost: commonLogic.appcommonhandle("更换价格",null),
			eqpriority: commonLogic.appcommonhandle("设备优先级",null),
			efficiency_y: commonLogic.appcommonhandle("利用率(当年)",null),
			intactrate_y: commonLogic.appcommonhandle("完好率(当年)",null),
			intactrate_q: commonLogic.appcommonhandle("完好率(当季度)",null),
			efficiency_m: commonLogic.appcommonhandle("利用率(当月)",null),
			emequipname: commonLogic.appcommonhandle("设备名称",null),
			emequipid: commonLogic.appcommonhandle("设备标识",null),
			efcheckdesc: commonLogic.appcommonhandle("强检备注",null),
			failurerate_m: commonLogic.appcommonhandle("故障率(当月)",null),
			deptid: commonLogic.appcommonhandle("专责部门",null),
			pic6: commonLogic.appcommonhandle("图片",null),
			materialcost: commonLogic.appcommonhandle("材料成本",null),
			haltstate: commonLogic.appcommonhandle("停机类型",null),
			intactrate_m: commonLogic.appcommonhandle("完好率(当月)",null),
			pic8: commonLogic.appcommonhandle("图片",null),
			purchdate: commonLogic.appcommonhandle("购买日期",null),
			haltcause: commonLogic.appcommonhandle("停机原因",null),
			productparam: commonLogic.appcommonhandle("生产属性",null),
			pic4: commonLogic.appcommonhandle("图片",null),
			equip_bh: commonLogic.appcommonhandle("集团设备编号",null),
			maintenancecost: commonLogic.appcommonhandle("维护成本累计",null),
			eqstate: commonLogic.appcommonhandle("设备状态",null),
			eqlife: commonLogic.appcommonhandle("寿命",null),
			originalcost: commonLogic.appcommonhandle("初始价格",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			params: commonLogic.appcommonhandle("基本参数",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			outputrct_dj: commonLogic.appcommonhandle("箱量操作量(当季度)",null),
			eqserialcode: commonLogic.appcommonhandle("产品系列号",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			pic2: commonLogic.appcommonhandle("图片",null),
			pic3: commonLogic.appcommonhandle("图片",null),
			equipcode: commonLogic.appcommonhandle("设备代码",null),
			outputrct_dy: commonLogic.appcommonhandle("箱量操作量(当月)",null),
			costcenterid: commonLogic.appcommonhandle("成本中心",null),
			eqstartdate: commonLogic.appcommonhandle("投运日期",null),
			warrantydate: commonLogic.appcommonhandle("保修终止日期",null),
			eqisservice1: commonLogic.appcommonhandle("统计大型设备",null),
			failurerate_q: commonLogic.appcommonhandle("故障率(当季度)",null),
			pic7: commonLogic.appcommonhandle("图片",null),
			pic: commonLogic.appcommonhandle("图片",null),
			blsystemdesc: commonLogic.appcommonhandle("所属系统备注",null),
			efficiency_q: commonLogic.appcommonhandle("利用率(当季度)",null),
			eqmodelcode: commonLogic.appcommonhandle("产品型号",null),
			pic5: commonLogic.appcommonhandle("图片",null),
			equipdesc: commonLogic.appcommonhandle("设备备注",null),
			description: commonLogic.appcommonhandle("描述",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			innerlaborcost: commonLogic.appcommonhandle("人工成本",null),
			keyattparam: commonLogic.appcommonhandle("关键属性参数",null),
			empid: commonLogic.appcommonhandle("专责人",null),
			techcode: commonLogic.appcommonhandle("工艺代码",null),
			outputrct_dn: commonLogic.appcommonhandle("箱量操作量(当年)",null),
			eqisservice: commonLogic.appcommonhandle("是否在工作",null),
			failurerate_y: commonLogic.appcommonhandle("故障率(当年)",null),
			efcheckdate: commonLogic.appcommonhandle("本次检测日期",null),
			foreignlaborcost: commonLogic.appcommonhandle("服务成本",null),
			equipgroup: commonLogic.appcommonhandle("设备分组",null),
			deptname: commonLogic.appcommonhandle("专责部门",null),
			haltstateinfo: commonLogic.appcommonhandle("设备状态情况",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			efcheckcdate: commonLogic.appcommonhandle("本次发证日期",null),
			equipinfo: commonLogic.appcommonhandle("设备信息",null),
			eqsumstoptime: commonLogic.appcommonhandle("总停机时间",null),
			empname: commonLogic.appcommonhandle("专责人",null),
			emberthcode: commonLogic.appcommonhandle("泊位编码",null),
			rteamname: commonLogic.appcommonhandle("责任班组",null),
			jzbh1: commonLogic.appcommonhandle("机种编号1",null),
			emmachinecategoryname: commonLogic.appcommonhandle("机种",null),
			stype: commonLogic.appcommonhandle("统计归口类型分组",null),
			mservicename: commonLogic.appcommonhandle("制造商",null),
			eqtypecode: commonLogic.appcommonhandle("设备类型代码",null),
			assetclasscode: commonLogic.appcommonhandle("资产科目代码",null),
			equippcode: commonLogic.appcommonhandle("上级设备代码",null),
			eqtypename: commonLogic.appcommonhandle("设备类型",null),
			emmachmodelname: commonLogic.appcommonhandle("机型",null),
			embrandcode: commonLogic.appcommonhandle("品牌编码",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			labservicename: commonLogic.appcommonhandle("产品供应商",null),
			assetname: commonLogic.appcommonhandle("资产",null),
			assetclassname: commonLogic.appcommonhandle("资产科目",null),
			embrandname: commonLogic.appcommonhandle("品牌",null),
			eqlocationname: commonLogic.appcommonhandle("位置",null),
			equippname: commonLogic.appcommonhandle("上级设备",null),
			assetclassid: commonLogic.appcommonhandle("资产科目",null),
			contractname: commonLogic.appcommonhandle("合同",null),
			emberthname: commonLogic.appcommonhandle("泊位",null),
			assetcode: commonLogic.appcommonhandle("资产代码",null),
			rservicename: commonLogic.appcommonhandle("服务提供商",null),
			eqlocationcode: commonLogic.appcommonhandle("位置代码",null),
			machtypecode: commonLogic.appcommonhandle("机种编码",null),
			eqtypepid: commonLogic.appcommonhandle("上级设备类型",null),
			sname: commonLogic.appcommonhandle("统计归口类型",null),
			eqlocationid: commonLogic.appcommonhandle("位置",null),
			emmachinecategoryid: commonLogic.appcommonhandle("机种",null),
			emberthid: commonLogic.appcommonhandle("泊位",null),
			rteamid: commonLogic.appcommonhandle("责任班组",null),
			embrandid: commonLogic.appcommonhandle("品牌",null),
			equippid: commonLogic.appcommonhandle("上级设备",null),
			labserviceid: commonLogic.appcommonhandle("产品供应商",null),
			rserviceid: commonLogic.appcommonhandle("服务提供商",null),
			emmachmodelid: commonLogic.appcommonhandle("机型",null),
			contractid: commonLogic.appcommonhandle("合同",null),
			eqtypeid: commonLogic.appcommonhandle("设备类型",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			assetid: commonLogic.appcommonhandle("资产",null),
			mserviceid: commonLogic.appcommonhandle("制造商",null),
		},
			views: {
				editview9: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案编辑视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案选项操作视图",null),
				},
				dashboardview9: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备主信息图表数据看板视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案选择表格视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("基本信息",null),
					title: commonLogic.appcommonhandle("基本信息",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案",null),
				},
				editview2: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案编辑视图",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案分页导航视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案数据选择视图",null),
				},
				allgridview: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案",null),
				},
				runinfo: {
					caption: commonLogic.appcommonhandle("设备档案",null),
					title: commonLogic.appcommonhandle("设备档案编辑视图",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipcode: commonLogic.appcommonhandle("设备代码",null), 
					emequipname: commonLogic.appcommonhandle("设备名称",null), 
					equippname: commonLogic.appcommonhandle("上级设备",null), 
					eqtypename: commonLogic.appcommonhandle("设备类型",null), 
					eqlocationname: commonLogic.appcommonhandle("位置",null), 
					deptname: commonLogic.appcommonhandle("专责部门",null), 
					empname: commonLogic.appcommonhandle("专责人",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					equipgroup: commonLogic.appcommonhandle("设备分组",null), 
					empid: commonLogic.appcommonhandle("专责人",null), 
					emequipid: commonLogic.appcommonhandle("设备标识",null), 
				},
				uiactions: {
				},
			},
			main4_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipcode: commonLogic.appcommonhandle("设备代码",null), 
					emequipname: commonLogic.appcommonhandle("设备名称",null), 
					equippname: commonLogic.appcommonhandle("上级设备",null), 
					eqtypename: commonLogic.appcommonhandle("设备类型",null), 
					eqlocationname: commonLogic.appcommonhandle("位置",null), 
					deptname: commonLogic.appcommonhandle("专责部门",null), 
					empname: commonLogic.appcommonhandle("专责人",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					equipgroup: commonLogic.appcommonhandle("设备分组",null), 
					empid: commonLogic.appcommonhandle("专责人",null), 
					eqlocationid: commonLogic.appcommonhandle("位置",null), 
					emequipid: commonLogic.appcommonhandle("设备标识",null), 
					equippid: commonLogic.appcommonhandle("上级设备",null), 
					rteamid: commonLogic.appcommonhandle("责任班组",null), 
					eqtypeid: commonLogic.appcommonhandle("设备类型",null), 
				},
				uiactions: {
				},
			},
			main5_form: {
				details: {
					grouppanel13: commonLogic.appcommonhandle("运行信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					eqisservice: commonLogic.appcommonhandle("是否在工作",null), 
					eqstate: commonLogic.appcommonhandle("设备状态",null), 
					eqstartdate: commonLogic.appcommonhandle("投运日期",null), 
					eqsumstoptime: commonLogic.appcommonhandle("总停机时间",null), 
					haltstate: commonLogic.appcommonhandle("停机类型",null), 
					haltcause: commonLogic.appcommonhandle("停机原因",null), 
					emequipid: commonLogic.appcommonhandle("设备标识",null), 
				},
				uiactions: {
				},
			},
			quickcreate_form: {
				details: {
					group1: commonLogic.appcommonhandle("设备档案基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("设备标识",null), 
					srfmajortext: commonLogic.appcommonhandle("设备名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipcode: commonLogic.appcommonhandle("设备代码",null), 
					emequipname: commonLogic.appcommonhandle("设备名称",null), 
					eqtypename: commonLogic.appcommonhandle("设备类型",null), 
					eqlocationname: commonLogic.appcommonhandle("位置",null), 
					equipgroup: commonLogic.appcommonhandle("设备分组",null), 
					empid: commonLogic.appcommonhandle("专责人",null), 
					empname: commonLogic.appcommonhandle("专责人",null), 
					deptname: commonLogic.appcommonhandle("专责部门",null), 
					eqlocationid: commonLogic.appcommonhandle("位置",null), 
					emequipid: commonLogic.appcommonhandle("设备标识",null), 
					eqtypeid: commonLogic.appcommonhandle("设备类型",null), 
				},
				uiactions: {
				},
			},
			pickup_grid: {
				columns: {
					equipcode: commonLogic.appcommonhandle("设备代码",null),
					emequipname: commonLogic.appcommonhandle("设备名称",null),
					eqtypename: commonLogic.appcommonhandle("设备类型",null),
					eqlocationname: commonLogic.appcommonhandle("位置",null),
					eqstate: commonLogic.appcommonhandle("设备状态",null),
					deptname: commonLogic.appcommonhandle("专责部门",null),
					empname: commonLogic.appcommonhandle("专责人",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					equipcode: commonLogic.appcommonhandle("设备代码",null),
					emequipname: commonLogic.appcommonhandle("设备名称",null),
					eqtypename: commonLogic.appcommonhandle("设备类型",null),
					eqlocationname: commonLogic.appcommonhandle("位置",null),
					eqstate: commonLogic.appcommonhandle("设备状态",null),
					uagridcolumn1: commonLogic.appcommonhandle("操作",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
					edit: commonLogic.appcommonhandle("编辑",null),
					remove: commonLogic.appcommonhandle("删除",null),
				},
			},
			typeeqnum_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					grouppanel1: commonLogic.appcommonhandle("分组面板",null), 
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_equipcode_like: commonLogic.appcommonhandle("设备代码（%）",null), 
					n_emequipname_like: commonLogic.appcommonhandle("设备名称（%）",null), 
					n_eqlocationname_eq: commonLogic.appcommonhandle("位置（=）",null), 
					n_equipgroup_eq: commonLogic.appcommonhandle("设备分组（=）",null), 
				},
				uiactions: {
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem1_openquickcreate: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
			},
			allgridviewtoolbar_toolbar: {
				tbitem1_openquickcreate: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_opendashboardview9: {
					caption: commonLogic.appcommonhandle("统计",null),
					tip: commonLogic.appcommonhandle("统计",null),
				},
			},
			editview2toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("设备档案",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("工单信息",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("活动历史",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("维护计划",null),
					},
					tabviewpanel5: {
						caption: commonLogic.appcommonhandle("外委申请",null),
					},
					tabviewpanel6: {
						caption: commonLogic.appcommonhandle("运行日志",null),
					}
				},
				uiactions: {
				},
			},
			dashboardview9dashboard_container2_portlet: {
				uiactions: {
				},
			},
			dashboardview9dashboard_container1_portlet: {
				uiactions: {
				},
			},
			info_portlet: {
				info: {
					title: commonLogic.appcommonhandle("基本信息", null)
				},
				uiactions: {
					emequip_openmainview: commonLogic.appcommonhandle("打开主信息视图",null),
				},
			},
			dashboardview9dashboard_container4_portlet: {
				uiactions: {
				},
			},
			dashboardview9dashboard_container5_portlet: {
				uiactions: {
				},
			},
			dashboardview9dashboard_container3_portlet: {
				uiactions: {
				},
			},
			runinfo_portlet: {
				runinfo: {
					title: commonLogic.appcommonhandle("运行信息", null)
				},
				uiactions: {
				},
			},
			eqnumpie_portlet: {
				eqnumpie: {
					title: commonLogic.appcommonhandle("各类型设备数量统计", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;