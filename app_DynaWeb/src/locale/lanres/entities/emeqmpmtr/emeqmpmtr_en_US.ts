import EMEQMPMTR_en_US_Base from './emeqmpmtr_en_US_base';

function getLocaleResource(){
    const EMEQMPMTR_en_US_OwnData = {};
    const targetData = Object.assign(EMEQMPMTR_en_US_Base(), EMEQMPMTR_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
