import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			wplistcostinfo: commonLogic.appcommonhandle("询价信息",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			price: commonLogic.appcommonhandle("单价",null),
			rempname: commonLogic.appcommonhandle("询价人",null),
			rempid: commonLogic.appcommonhandle("询价人",null),
			adate: commonLogic.appcommonhandle("询价时间",null),
			emwplistcostname: commonLogic.appcommonhandle("询价单名称",null),
			discnt: commonLogic.appcommonhandle("折扣(%)",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			itemdesc: commonLogic.appcommonhandle("物品备注",null),
			unitrate: commonLogic.appcommonhandle("单位转换率",null),
			begindate: commonLogic.appcommonhandle("有效期起始",null),
			listprice: commonLogic.appcommonhandle("标价",null),
			items: commonLogic.appcommonhandle("物品",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			pricecdt: commonLogic.appcommonhandle("价格条件",null),
			intunitflag: commonLogic.appcommonhandle("整单位购买",null),
			description: commonLogic.appcommonhandle("描述",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emwplistcostid: commonLogic.appcommonhandle("询价单标识",null),
			wplistcosteval: commonLogic.appcommonhandle("采购试算(单价排序)",null),
			unitdesc: commonLogic.appcommonhandle("询价单位备注",null),
			enddate: commonLogic.appcommonhandle("有效期截至",null),
			wplistcostresult: commonLogic.appcommonhandle("-",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			taxrate: commonLogic.appcommonhandle("税率",null),
			sunitprice: commonLogic.appcommonhandle("标准单位单价",null),
			sunitid: commonLogic.appcommonhandle("标准单位",null),
			unitname: commonLogic.appcommonhandle("询价单位",null),
			sunitname: commonLogic.appcommonhandle("标准单位",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			labservicename: commonLogic.appcommonhandle("产品供应商",null),
			avgprice: commonLogic.appcommonhandle("物品均价",null),
			wplistid: commonLogic.appcommonhandle("采购申请号",null),
			labserviceid: commonLogic.appcommonhandle("产品供应商",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			unitid: commonLogic.appcommonhandle("询价单位",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单编辑视图",null),
				},
				fillcostview: {
					caption: commonLogic.appcommonhandle("询价单填报",null),
					title: commonLogic.appcommonhandle("询价单填报",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单数据选择视图",null),
				},
				confirmgridview9: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单表格视图",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单",null),
				},
				gridview9: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单表格视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单选择表格视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("询价单",null),
					title: commonLogic.appcommonhandle("询价单",null),
				},
			},
			main4_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("询价单信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel21: commonLogic.appcommonhandle("操作信息",null), 
					formpage20: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("询价单标识",null), 
					srfmajortext: commonLogic.appcommonhandle("询价单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					adate: commonLogic.appcommonhandle("询价时间",null), 
					labservicename: commonLogic.appcommonhandle("产品供应商",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					itemdesc: commonLogic.appcommonhandle("物品备注",null), 
					listprice: commonLogic.appcommonhandle("标价",null), 
					discnt: commonLogic.appcommonhandle("折扣(%)",null), 
					price: commonLogic.appcommonhandle("单价",null), 
					taxrate: commonLogic.appcommonhandle("税率",null), 
					pricecdt: commonLogic.appcommonhandle("价格条件",null), 
					unitname: commonLogic.appcommonhandle("询价单位",null), 
					unitdesc: commonLogic.appcommonhandle("询价单位备注",null), 
					unitrate: commonLogic.appcommonhandle("单位转换率",null), 
					rempid: commonLogic.appcommonhandle("询价人",null), 
					rempname: commonLogic.appcommonhandle("询价人",null), 
					begindate: commonLogic.appcommonhandle("有效期起始",null), 
					enddate: commonLogic.appcommonhandle("有效期截至",null), 
					intunitflag: commonLogic.appcommonhandle("整单位购买",null), 
					sunitprice: commonLogic.appcommonhandle("标准单位单价",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					unitid: commonLogic.appcommonhandle("询价单位",null), 
					labserviceid: commonLogic.appcommonhandle("产品供应商",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
					emwplistcostid: commonLogic.appcommonhandle("询价单标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("询价单基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("询价单标识",null), 
					srfmajortext: commonLogic.appcommonhandle("询价单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					wplistcostinfo: commonLogic.appcommonhandle("询价信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emwplistcostid: commonLogic.appcommonhandle("询价单标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					wplistcostresult: commonLogic.appcommonhandle("-",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					labservicename: commonLogic.appcommonhandle("产品供应商",null),
					wplistcosteval: commonLogic.appcommonhandle("采购试算(单价排序)",null),
					price: commonLogic.appcommonhandle("单价",null),
					taxrate: commonLogic.appcommonhandle("税率",null),
					unitname: commonLogic.appcommonhandle("询价单位",null),
					unitrate: commonLogic.appcommonhandle("单位转换率",null),
					intunitflag: commonLogic.appcommonhandle("整单位购买",null),
					listprice: commonLogic.appcommonhandle("标价",null),
					discnt: commonLogic.appcommonhandle("折扣(%)",null),
					unitdesc: commonLogic.appcommonhandle("询价单位备注",null),
					pricecdt: commonLogic.appcommonhandle("价格条件",null),
					rempname: commonLogic.appcommonhandle("询价人",null),
					adate: commonLogic.appcommonhandle("询价时间",null),
					begindate: commonLogic.appcommonhandle("有效期起始",null),
					enddate: commonLogic.appcommonhandle("有效期截至",null),
					itemdesc: commonLogic.appcommonhandle("物品备注",null),
					uagridcolumn1: commonLogic.appcommonhandle("操作",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				edit: commonLogic.appcommonhandle("Edit",null),
				remove: commonLogic.appcommonhandle("Remove",null),
				},
			},
			main4_grid: {
				columns: {
					wplistcostresult: commonLogic.appcommonhandle("-",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					labservicename: commonLogic.appcommonhandle("产品供应商",null),
					wplistcosteval: commonLogic.appcommonhandle("采购试算(单价排序)",null),
					price: commonLogic.appcommonhandle("单价",null),
					taxrate: commonLogic.appcommonhandle("税率",null),
					unitname: commonLogic.appcommonhandle("询价单位",null),
					unitrate: commonLogic.appcommonhandle("单位转换率",null),
					intunitflag: commonLogic.appcommonhandle("整单位购买",null),
					listprice: commonLogic.appcommonhandle("标价",null),
					discnt: commonLogic.appcommonhandle("折扣(%)",null),
					unitdesc: commonLogic.appcommonhandle("询价单位备注",null),
					pricecdt: commonLogic.appcommonhandle("价格条件",null),
					rempname: commonLogic.appcommonhandle("询价人",null),
					adate: commonLogic.appcommonhandle("询价时间",null),
					begindate: commonLogic.appcommonhandle("有效期起始",null),
					enddate: commonLogic.appcommonhandle("有效期截至",null),
					itemdesc: commonLogic.appcommonhandle("物品备注",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			confirm_grid: {
				columns: {
					wplistcostresult: commonLogic.appcommonhandle("-",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					labservicename: commonLogic.appcommonhandle("产品供应商",null),
					wplistcosteval: commonLogic.appcommonhandle("采购试算(单价排序)",null),
					price: commonLogic.appcommonhandle("单价",null),
					taxrate: commonLogic.appcommonhandle("税率",null),
					unitname: commonLogic.appcommonhandle("询价单位",null),
					unitrate: commonLogic.appcommonhandle("单位转换率",null),
					intunitflag: commonLogic.appcommonhandle("整单位购买",null),
					listprice: commonLogic.appcommonhandle("标价",null),
					discnt: commonLogic.appcommonhandle("折扣(%)",null),
					unitdesc: commonLogic.appcommonhandle("询价单位备注",null),
					pricecdt: commonLogic.appcommonhandle("价格条件",null),
					rempname: commonLogic.appcommonhandle("询价人",null),
					adate: commonLogic.appcommonhandle("询价时间",null),
					begindate: commonLogic.appcommonhandle("有效期起始",null),
					enddate: commonLogic.appcommonhandle("有效期截至",null),
					itemdesc: commonLogic.appcommonhandle("物品备注",null),
					uagridcolumn1: commonLogic.appcommonhandle("操作",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				emwplistcost_confirm: commonLogic.appcommonhandle("确认询价",null),
				},
			},
			costbyitem_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("Save",null),
					tip: commonLogic.appcommonhandle("Save",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Save And New",null),
					tip: commonLogic.appcommonhandle("Save And New",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("Remove And Close",null),
					tip: commonLogic.appcommonhandle("Remove And Close Window",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("Copy",null),
					tip: commonLogic.appcommonhandle("Copy {0}",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("Help",null),
					tip: commonLogic.appcommonhandle("Help",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			costbyitem_portlet: {
				costbyitem: {
					title: commonLogic.appcommonhandle("最新询价", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;