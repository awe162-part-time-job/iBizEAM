import EMWOORIAuthServiceBase from './emwoori-auth-service-base';


/**
 * 工单来源权限服务对象
 *
 * @export
 * @class EMWOORIAuthService
 * @extends {EMWOORIAuthServiceBase}
 */
export default class EMWOORIAuthService extends EMWOORIAuthServiceBase {

    /**
     * Creates an instance of  EMWOORIAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOORIAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}