import EMItemTradeAuthServiceBase from './emitem-trade-auth-service-base';


/**
 * 物品交易权限服务对象
 *
 * @export
 * @class EMItemTradeAuthService
 * @extends {EMItemTradeAuthServiceBase}
 */
export default class EMItemTradeAuthService extends EMItemTradeAuthServiceBase {

    /**
     * Creates an instance of  EMItemTradeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTradeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}