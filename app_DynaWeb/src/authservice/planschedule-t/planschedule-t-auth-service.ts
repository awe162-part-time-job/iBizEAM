import PLANSCHEDULE_TAuthServiceBase from './planschedule-t-auth-service-base';


/**
 * 计划_定时权限服务对象
 *
 * @export
 * @class PLANSCHEDULE_TAuthService
 * @extends {PLANSCHEDULE_TAuthServiceBase}
 */
export default class PLANSCHEDULE_TAuthService extends PLANSCHEDULE_TAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_TAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_TAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}