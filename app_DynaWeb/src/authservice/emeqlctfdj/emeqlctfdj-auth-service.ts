import EMEQLCTFDJAuthServiceBase from './emeqlctfdj-auth-service-base';


/**
 * 发动机位置权限服务对象
 *
 * @export
 * @class EMEQLCTFDJAuthService
 * @extends {EMEQLCTFDJAuthServiceBase}
 */
export default class EMEQLCTFDJAuthService extends EMEQLCTFDJAuthServiceBase {

    /**
     * Creates an instance of  EMEQLCTFDJAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTFDJAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}