import EMEQCheckAuthServiceBase from './emeqcheck-auth-service-base';


/**
 * 维修记录权限服务对象
 *
 * @export
 * @class EMEQCheckAuthService
 * @extends {EMEQCheckAuthServiceBase}
 */
export default class EMEQCheckAuthService extends EMEQCheckAuthServiceBase {

    /**
     * Creates an instance of  EMEQCheckAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQCheckAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}