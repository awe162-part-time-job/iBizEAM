import EMProductAuthServiceBase from './emproduct-auth-service-base';


/**
 * 试用品权限服务对象
 *
 * @export
 * @class EMProductAuthService
 * @extends {EMProductAuthServiceBase}
 */
export default class EMProductAuthService extends EMProductAuthServiceBase {

    /**
     * Creates an instance of  EMProductAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMProductAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}