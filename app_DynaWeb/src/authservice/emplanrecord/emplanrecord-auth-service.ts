import EMPLANRECORDAuthServiceBase from './emplanrecord-auth-service-base';


/**
 * 触发记录权限服务对象
 *
 * @export
 * @class EMPLANRECORDAuthService
 * @extends {EMPLANRECORDAuthServiceBase}
 */
export default class EMPLANRECORDAuthService extends EMPLANRECORDAuthServiceBase {

    /**
     * Creates an instance of  EMPLANRECORDAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPLANRECORDAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}