import EMItemTypeAuthServiceBase from './emitem-type-auth-service-base';


/**
 * 物品类型权限服务对象
 *
 * @export
 * @class EMItemTypeAuthService
 * @extends {EMItemTypeAuthServiceBase}
 */
export default class EMItemTypeAuthService extends EMItemTypeAuthServiceBase {

    /**
     * Creates an instance of  EMItemTypeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTypeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}