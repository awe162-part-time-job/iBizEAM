import PFUnitAuthServiceBase from './pfunit-auth-service-base';


/**
 * 计量单位权限服务对象
 *
 * @export
 * @class PFUnitAuthService
 * @extends {PFUnitAuthServiceBase}
 */
export default class PFUnitAuthService extends PFUnitAuthServiceBase {

    /**
     * Creates an instance of  PFUnitAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFUnitAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}