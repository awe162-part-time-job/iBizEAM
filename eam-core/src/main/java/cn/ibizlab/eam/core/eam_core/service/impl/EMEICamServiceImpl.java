package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEICam;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEICamService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEICamMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[探头] 服务对象接口实现
 */
@Slf4j
@Service("EMEICamServiceImpl")
public class EMEICamServiceImpl extends ServiceImpl<EMEICamMapper, EMEICam> implements IEMEICamService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamDropService emeicamdropService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamHistService emeicamhistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEICamSetupService emeicamsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEICam et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicamid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEICam> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEICam et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeicamid", et.getEmeicamid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeicamid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEICam> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEICam get(String key) {
        EMEICam et = getById(key);
        if(et == null){
            et = new EMEICam();
            et.setEmeicamid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEICam getDraft(EMEICam et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEICam et) {
        return (!ObjectUtils.isEmpty(et.getEmeicamid())) && (!Objects.isNull(this.getById(et.getEmeicamid())));
    }
    @Override
    @Transactional
    public boolean save(EMEICam et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEICam et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEICam> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICam> create = new ArrayList<>();
        List<EMEICam> update = new ArrayList<>();
        for (EMEICam et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicamid()) || ObjectUtils.isEmpty(getById(et.getEmeicamid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEICam> list) {
        list.forEach(item->fillParentData(item));
        List<EMEICam> create = new ArrayList<>();
        List<EMEICam> update = new ArrayList<>();
        for (EMEICam et : list) {
            if (ObjectUtils.isEmpty(et.getEmeicamid()) || ObjectUtils.isEmpty(getById(et.getEmeicamid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEICam> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }
    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEICam>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEICam> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEICam>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEICam> selectByItempuseid(String emitempuseid) {
        return baseMapper.selectByItempuseid(emitempuseid);
    }
    @Override
    public void removeByItempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMEICam>().eq("itempuseid",emitempuseid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEICam> searchDefault(EMEICamSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEICam> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEICam>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEICam et){
        //实体关系[DER1N_EMEICAM_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setEqlocationname(eqlocation.getEqlocationinfo());
        }
        //实体关系[DER1N_EMEICAM_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEICAM_EMITEMPUSE_ITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getItempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse itempuse=et.getItempuse();
            if(ObjectUtils.isEmpty(itempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getItempuseid());
                et.setItempuse(majorEntity);
                itempuse=majorEntity;
            }
            et.setItempusename(itempuse.getEmitempusename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEICam> getEmeicamByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEICam> getEmeicamByEntities(List<EMEICam> entities) {
        List ids =new ArrayList();
        for(EMEICam entity : entities){
            Serializable id=entity.getEmeicamid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEICamService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



