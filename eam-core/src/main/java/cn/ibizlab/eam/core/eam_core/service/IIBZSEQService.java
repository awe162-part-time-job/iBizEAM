package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.IBZSEQ;
import cn.ibizlab.eam.core.eam_core.filter.IBZSEQSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBZSEQ] 服务对象接口
 */
public interface IIBZSEQService extends IService<IBZSEQ> {

    boolean create(IBZSEQ et);
    void createBatch(List<IBZSEQ> list);
    boolean update(IBZSEQ et);
    void updateBatch(List<IBZSEQ> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    IBZSEQ get(String key);
    IBZSEQ getDraft(IBZSEQ et);
    boolean checkKey(IBZSEQ et);
    IBZSEQ genId(IBZSEQ et);
    boolean genIdBatch(List<IBZSEQ> etList);
    boolean save(IBZSEQ et);
    void saveBatch(List<IBZSEQ> list);
    Page<IBZSEQ> searchDefault(IBZSEQSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<IBZSEQ> getIbzseqByIds(List<String> ids);
    List<IBZSEQ> getIbzseqByEntities(List<IBZSEQ> entities);
}


