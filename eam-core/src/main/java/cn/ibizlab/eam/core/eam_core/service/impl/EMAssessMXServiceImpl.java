package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMX;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMXSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMXService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMAssessMXMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备停用明细考核] 服务对象接口实现
 */
@Slf4j
@Service("EMAssessMXServiceImpl")
public class EMAssessMXServiceImpl extends ServiceImpl<EMAssessMXMapper, EMAssessMX> implements IEMAssessMXService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDisableAssessService emdisableassessService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMAssessMX et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassessmxid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMAssessMX> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMAssessMX et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emassessmxid", et.getEmassessmxid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmassessmxid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMAssessMX> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMAssessMX get(String key) {
        EMAssessMX et = getById(key);
        if(et == null){
            et = new EMAssessMX();
            et.setEmassessmxid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMAssessMX getDraft(EMAssessMX et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMAssessMX et) {
        return (!ObjectUtils.isEmpty(et.getEmassessmxid())) && (!Objects.isNull(this.getById(et.getEmassessmxid())));
    }
    @Override
    @Transactional
    public boolean save(EMAssessMX et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMAssessMX et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMAssessMX> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssessMX> create = new ArrayList<>();
        List<EMAssessMX> update = new ArrayList<>();
        for (EMAssessMX et : list) {
            if (ObjectUtils.isEmpty(et.getEmassessmxid()) || ObjectUtils.isEmpty(getById(et.getEmassessmxid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMAssessMX> list) {
        list.forEach(item->fillParentData(item));
        List<EMAssessMX> create = new ArrayList<>();
        List<EMAssessMX> update = new ArrayList<>();
        for (EMAssessMX et : list) {
            if (ObjectUtils.isEmpty(et.getEmassessmxid()) || ObjectUtils.isEmpty(getById(et.getEmassessmxid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMAssessMX> selectByEmdisableassessid(String emdisableassessid) {
        return baseMapper.selectByEmdisableassessid(emdisableassessid);
    }
    @Override
    public void removeByEmdisableassessid(String emdisableassessid) {
        this.remove(new QueryWrapper<EMAssessMX>().eq("emdisableassessid",emdisableassessid));
    }

	@Override
    public List<EMAssessMX> selectByEmequipid(String emequipid) {
        return baseMapper.selectByEmequipid(emequipid);
    }
    @Override
    public void removeByEmequipid(String emequipid) {
        this.remove(new QueryWrapper<EMAssessMX>().eq("emequipid",emequipid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMAssessMX> searchDefault(EMAssessMXSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMAssessMX> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMAssessMX>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMAssessMX et){
        //实体关系[DER1N_EMASSESSMX_EMDISABLEASSESS_EMDISABLEASSESSID]
        if(!ObjectUtils.isEmpty(et.getEmdisableassessid())){
            cn.ibizlab.eam.core.eam_core.domain.EMDisableAssess emdisableassess=et.getEmdisableassess();
            if(ObjectUtils.isEmpty(emdisableassess)){
                cn.ibizlab.eam.core.eam_core.domain.EMDisableAssess majorEntity=emdisableassessService.get(et.getEmdisableassessid());
                et.setEmdisableassess(majorEntity);
                emdisableassess=majorEntity;
            }
            et.setEmdisableassessname(emdisableassess.getEmdisableassessname());
        }
        //实体关系[DER1N_EMASSESSMX_EMEQUIP_EMEQUIPID]
        if(!ObjectUtils.isEmpty(et.getEmequipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip=et.getEmequip();
            if(ObjectUtils.isEmpty(emequip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEmequipid());
                et.setEmequip(majorEntity);
                emequip=majorEntity;
            }
            et.setEmequipname(emequip.getEquipinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMAssessMX> getEmassessmxByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMAssessMX> getEmassessmxByEntities(List<EMAssessMX> entities) {
        List ids =new ArrayList();
        for(EMAssessMX entity : entities){
            Serializable id=entity.getEmassessmxid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMAssessMXService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



