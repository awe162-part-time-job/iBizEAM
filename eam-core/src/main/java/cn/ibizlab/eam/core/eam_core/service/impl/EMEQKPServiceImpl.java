package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKP;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQKPMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备关键点] 服务对象接口实现
 */
@Slf4j
@Service("EMEQKPServiceImpl")
public class EMEQKPServiceImpl extends ServiceImpl<EMEQKPMapper, EMEQKP> implements IEMEQKPService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKPMapService emeqkpmapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKPRCDService emeqkprcdService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQKP et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqkpid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQKP> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQKP et) {
        emobjectService.update(emeqkpInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqkpid", et.getEmeqkpid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqkpid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQKP> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQKP get(String key) {
        EMEQKP et = getById(key);
        if(et == null){
            et = new EMEQKP();
            et.setEmeqkpid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQKP getDraft(EMEQKP et) {
        return et;
    }

    @Override
    public boolean checkKey(EMEQKP et) {
        return (!ObjectUtils.isEmpty(et.getEmeqkpid())) && (!Objects.isNull(this.getById(et.getEmeqkpid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQKP et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQKP et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQKP> list) {
        List<EMEQKP> create = new ArrayList<>();
        List<EMEQKP> update = new ArrayList<>();
        for (EMEQKP et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqkpid()) || ObjectUtils.isEmpty(getById(et.getEmeqkpid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQKP> list) {
        List<EMEQKP> create = new ArrayList<>();
        List<EMEQKP> update = new ArrayList<>();
        for (EMEQKP et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqkpid()) || ObjectUtils.isEmpty(getById(et.getEmeqkpid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQKP> searchDefault(EMEQKPSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQKP> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQKP>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQKPInheritMapping emeqkpInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQKP et){
        if(ObjectUtils.isEmpty(et.getEmeqkpid()))
            et.setEmeqkpid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emeqkpInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","EQKP");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQKP> getEmeqkpByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQKP> getEmeqkpByEntities(List<EMEQKP> entities) {
        List ids =new ArrayList();
        for(EMEQKP entity : entities){
            Serializable id=entity.getEmeqkpid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQKPService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



