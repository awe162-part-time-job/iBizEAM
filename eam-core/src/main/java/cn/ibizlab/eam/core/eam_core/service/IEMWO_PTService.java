package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_PTSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO_PT] 服务对象接口
 */
public interface IEMWO_PTService extends IService<EMWO_PT> {

    boolean create(EMWO_PT et);
    void createBatch(List<EMWO_PT> list);
    boolean update(EMWO_PT et);
    void updateBatch(List<EMWO_PT> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWO_PT get(String key);
    EMWO_PT getDraft(EMWO_PT et);
    boolean checkKey(EMWO_PT et);
    boolean save(EMWO_PT et);
    void saveBatch(List<EMWO_PT> list);
    Page<EMWO_PT> searchDefault(EMWO_PTSearchContext context);
    List<EMWO_PT> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMWO_PT> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWO_PT> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMWO_PT> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWO_PT> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMWO_PT> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMWO_PT> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMWO_PT> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMWO_PT> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMWO_PT> selectByWooriid(String emwooriid);
    void removeByWooriid(String emwooriid);
    List<EMWO_PT> selectByWopid(String emwoid);
    void removeByWopid(String emwoid);
    List<EMWO_PT> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO_PT> getEmwoPtByIds(List<String> ids);
    List<EMWO_PT> getEmwoPtByEntities(List<EMWO_PT> entities);
}


