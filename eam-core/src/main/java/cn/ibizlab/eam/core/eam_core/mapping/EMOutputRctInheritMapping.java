

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMOutputRct;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMOutputRctInheritMapping {

    @Mappings({
        @Mapping(source ="emoutputrctid",target = "emdprctid"),
        @Mapping(source ="emoutputrctname",target = "emdprctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="outputname",target = "dpname"),
        @Mapping(source ="outputid",target = "dpid"),
    })
    EMDPRCT toEmdprct(EMOutputRct minorEntity);

    @Mappings({
        @Mapping(source ="emdprctid" ,target = "emoutputrctid"),
        @Mapping(source ="emdprctname" ,target = "emoutputrctname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="dpname",target = "outputname"),
        @Mapping(source ="dpid",target = "outputid"),
    })
    EMOutputRct toEmoutputrct(EMDPRCT majorEntity);

    List<EMDPRCT> toEmdprct(List<EMOutputRct> minorEntities);

    List<EMOutputRct> toEmoutputrct(List<EMDPRCT> majorEntities);

}


