package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_D;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_DSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface PLANSCHEDULE_DMapper extends BaseMapper<PLANSCHEDULE_D> {

    Page<PLANSCHEDULE_D> searchDefault(IPage page, @Param("srf") PLANSCHEDULE_DSearchContext context, @Param("ew") Wrapper<PLANSCHEDULE_D> wrapper);
    @Override
    PLANSCHEDULE_D selectById(Serializable id);
    @Override
    int insert(PLANSCHEDULE_D entity);
    @Override
    int updateById(@Param(Constants.ENTITY) PLANSCHEDULE_D entity);
    @Override
    int update(@Param(Constants.ENTITY) PLANSCHEDULE_D entity, @Param("ew") Wrapper<PLANSCHEDULE_D> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

}
