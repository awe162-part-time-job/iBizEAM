package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
/**
 * 关系型数据实体[EMEQLocation] 查询条件对象
 */
@Slf4j
@Data
public class EMEQLocationSearchContext extends QueryWrapperContext<EMEQLocation> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_eqlocationinfo_like;//[位置信息]
	public void setN_eqlocationinfo_like(String n_eqlocationinfo_like) {
        this.n_eqlocationinfo_like = n_eqlocationinfo_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationinfo_like)){
            this.getSearchCond().like("eqlocationinfo", n_eqlocationinfo_like);
        }
    }
	private String n_emeqlocationname_like;//[位置名称]
	public void setN_emeqlocationname_like(String n_emeqlocationname_like) {
        this.n_emeqlocationname_like = n_emeqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqlocationname_like)){
            this.getSearchCond().like("emeqlocationname", n_emeqlocationname_like);
        }
    }
	private String n_emeqlocationid_eq;//[位置标识]
	public void setN_emeqlocationid_eq(String n_emeqlocationid_eq) {
        this.n_emeqlocationid_eq = n_emeqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_emeqlocationid_eq)){
            this.getSearchCond().eq("emeqlocationid", n_emeqlocationid_eq);
        }
    }
	private String n_eqlocationcode_like;//[位置代码]
	public void setN_eqlocationcode_like(String n_eqlocationcode_like) {
        this.n_eqlocationcode_like = n_eqlocationcode_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationcode_like)){
            this.getSearchCond().like("eqlocationcode", n_eqlocationcode_like);
        }
    }
	private String n_eqlocationtype_eq;//[位置类型]
	public void setN_eqlocationtype_eq(String n_eqlocationtype_eq) {
        this.n_eqlocationtype_eq = n_eqlocationtype_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationtype_eq)){
            this.getSearchCond().eq("eqlocationtype", n_eqlocationtype_eq);
        }
    }
	private String n_majorequipname_eq;//[主设备]
	public void setN_majorequipname_eq(String n_majorequipname_eq) {
        this.n_majorequipname_eq = n_majorequipname_eq;
        if(!ObjectUtils.isEmpty(this.n_majorequipname_eq)){
            this.getSearchCond().eq("majorequipname", n_majorequipname_eq);
        }
    }
	private String n_majorequipname_like;//[主设备]
	public void setN_majorequipname_like(String n_majorequipname_like) {
        this.n_majorequipname_like = n_majorequipname_like;
        if(!ObjectUtils.isEmpty(this.n_majorequipname_like)){
            this.getSearchCond().like("majorequipname", n_majorequipname_like);
        }
    }
	private String n_majorequipid_eq;//[主设备]
	public void setN_majorequipid_eq(String n_majorequipid_eq) {
        this.n_majorequipid_eq = n_majorequipid_eq;
        if(!ObjectUtils.isEmpty(this.n_majorequipid_eq)){
            this.getSearchCond().eq("majorequipid", n_majorequipid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqlocationname", query)
            );
		 }
	}
}



