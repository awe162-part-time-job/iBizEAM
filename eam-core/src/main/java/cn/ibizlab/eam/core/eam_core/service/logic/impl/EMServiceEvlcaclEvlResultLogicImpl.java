package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMServiceEvlcaclEvlResultLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;

/**
 * 关系型数据实体[caclEvlResult] 对象
 */
@Slf4j
@Service
public class EMServiceEvlcaclEvlResultLogicImpl implements IEMServiceEvlcaclEvlResultLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService emserviceevlservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService getEmserviceevlService() {
        return this.emserviceevlservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMServiceEvl et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emserviceevlcaclevlresultdefault", et);
            kieSession.setGlobal("emserviceevlservice", emserviceevlservice);
            kieSession.setGlobal("iBzSysEmserviceevlDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emserviceevlcaclevlresult");

        } catch (Exception e) {
            throw new RuntimeException("执行[计算总分数]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
