package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTRHY;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTRHYSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQLCTRHY] 服务对象接口
 */
public interface IEMEQLCTRHYService extends IService<EMEQLCTRHY> {

    boolean create(EMEQLCTRHY et);
    void createBatch(List<EMEQLCTRHY> list);
    boolean update(EMEQLCTRHY et);
    void updateBatch(List<EMEQLCTRHY> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQLCTRHY get(String key);
    EMEQLCTRHY getDraft(EMEQLCTRHY et);
    boolean checkKey(EMEQLCTRHY et);
    boolean save(EMEQLCTRHY et);
    void saveBatch(List<EMEQLCTRHY> list);
    Page<EMEQLCTRHY> searchDefault(EMEQLCTRHYSearchContext context);
    List<EMEQLCTRHY> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQLCTRHY> getEmeqlctrhyByIds(List<String> ids);
    List<EMEQLCTRHY> getEmeqlctrhyByEntities(List<EMEQLCTRHY> entities);
}


