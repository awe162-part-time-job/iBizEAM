

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemRInInheritMapping {

    @Mappings({
        @Mapping(source ="emitemrinid",target = "emitemtradeid"),
        @Mapping(source ="emitemrinname",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="rinstate",target = "tradestate"),
    })
    EMItemTrade toEmitemtrade(EMItemRIn minorEntity);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitemrinid"),
        @Mapping(source ="emitemtradename" ,target = "emitemrinname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="tradestate",target = "rinstate"),
    })
    EMItemRIn toEmitemrin(EMItemTrade majorEntity);

    List<EMItemTrade> toEmitemtrade(List<EMItemRIn> minorEntities);

    List<EMItemRIn> toEmitemrin(List<EMItemTrade> majorEntities);

}


