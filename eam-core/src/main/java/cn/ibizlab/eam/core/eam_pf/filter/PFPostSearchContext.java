package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFPost;
/**
 * 关系型数据实体[PFPost] 查询条件对象
 */
@Slf4j
@Data
public class PFPostSearchContext extends QueryWrapperContext<PFPost> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_wageslevelid_eq;//[工资级别]
	public void setN_wageslevelid_eq(String n_wageslevelid_eq) {
        this.n_wageslevelid_eq = n_wageslevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_wageslevelid_eq)){
            this.getSearchCond().eq("wageslevelid", n_wageslevelid_eq);
        }
    }
	private Integer n_postlevel_eq;//[岗位层次]
	public void setN_postlevel_eq(Integer n_postlevel_eq) {
        this.n_postlevel_eq = n_postlevel_eq;
        if(!ObjectUtils.isEmpty(this.n_postlevel_eq)){
            this.getSearchCond().eq("postlevel", n_postlevel_eq);
        }
    }
	private String n_pfpostname_like;//[岗位名称]
	public void setN_pfpostname_like(String n_pfpostname_like) {
        this.n_pfpostname_like = n_pfpostname_like;
        if(!ObjectUtils.isEmpty(this.n_pfpostname_like)){
            this.getSearchCond().like("pfpostname", n_pfpostname_like);
        }
    }
	private String n_posttypeid_eq;//[岗位类型]
	public void setN_posttypeid_eq(String n_posttypeid_eq) {
        this.n_posttypeid_eq = n_posttypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_posttypeid_eq)){
            this.getSearchCond().eq("posttypeid", n_posttypeid_eq);
        }
    }
	private String n_postinfo_like;//[岗位信息]
	public void setN_postinfo_like(String n_postinfo_like) {
        this.n_postinfo_like = n_postinfo_like;
        if(!ObjectUtils.isEmpty(this.n_postinfo_like)){
            this.getSearchCond().like("postinfo", n_postinfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfpostname", query)
            );
		 }
	}
}



