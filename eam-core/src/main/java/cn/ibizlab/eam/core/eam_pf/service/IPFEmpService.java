package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFEmp;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFEmp] 服务对象接口
 */
public interface IPFEmpService extends IService<PFEmp> {

    boolean create(PFEmp et);
    void createBatch(List<PFEmp> list);
    boolean update(PFEmp et);
    void updateBatch(List<PFEmp> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFEmp get(String key);
    PFEmp getDraft(PFEmp et);
    boolean checkKey(PFEmp et);
    boolean save(PFEmp et);
    void saveBatch(List<PFEmp> list);
    Page<PFEmp> searchDefault(PFEmpSearchContext context);
    Page<PFEmp> searchDeptEmp(PFEmpSearchContext context);
    List<PFEmp> selectByMajordeptid(String pfdeptid);
    void removeByMajordeptid(String pfdeptid);
    List<PFEmp> selectByMajorteamid(String pfteamid);
    void removeByMajorteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFEmp> getPfempByIds(List<String> ids);
    List<PFEmp> getPfempByEntities(List<PFEmp> entities);
}


