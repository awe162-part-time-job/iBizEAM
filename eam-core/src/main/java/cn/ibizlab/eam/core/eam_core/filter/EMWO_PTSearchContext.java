package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
/**
 * 关系型数据实体[EMWO_PT] 查询条件对象
 */
@Slf4j
@Data
public class EMWO_PTSearchContext extends QueryWrapperContext<EMWO_PT> {

	private String n_priority_eq;//[优先级]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_recvpersonid_eq;//[接收人]
	public void setN_recvpersonid_eq(String n_recvpersonid_eq) {
        this.n_recvpersonid_eq = n_recvpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonid_eq)){
            this.getSearchCond().eq("recvpersonid", n_recvpersonid_eq);
        }
    }
	private String n_wpersonname_eq;//[执行人]
	public void setN_wpersonname_eq(String n_wpersonname_eq) {
        this.n_wpersonname_eq = n_wpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_wpersonname_eq)){
            this.getSearchCond().eq("wpersonname", n_wpersonname_eq);
        }
    }
	private String n_wpersonname_like;//[执行人]
	public void setN_wpersonname_like(String n_wpersonname_like) {
        this.n_wpersonname_like = n_wpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_wpersonname_like)){
            this.getSearchCond().like("wpersonname", n_wpersonname_like);
        }
    }
	private String n_recvpersonname_eq;//[接收人]
	public void setN_recvpersonname_eq(String n_recvpersonname_eq) {
        this.n_recvpersonname_eq = n_recvpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_eq)){
            this.getSearchCond().eq("recvpersonname", n_recvpersonname_eq);
        }
    }
	private String n_recvpersonname_like;//[接收人]
	public void setN_recvpersonname_like(String n_recvpersonname_like) {
        this.n_recvpersonname_like = n_recvpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_recvpersonname_like)){
            this.getSearchCond().like("recvpersonname", n_recvpersonname_like);
        }
    }
	private String n_rempid_eq;//[责任人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_wogroup_eq;//[工单分组]
	public void setN_wogroup_eq(String n_wogroup_eq) {
        this.n_wogroup_eq = n_wogroup_eq;
        if(!ObjectUtils.isEmpty(this.n_wogroup_eq)){
            this.getSearchCond().eq("wogroup", n_wogroup_eq);
        }
    }
	private String n_mpersonid_eq;//[制定人]
	public void setN_mpersonid_eq(String n_mpersonid_eq) {
        this.n_mpersonid_eq = n_mpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_mpersonid_eq)){
            this.getSearchCond().eq("mpersonid", n_mpersonid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emwo_ptname_like;//[工单名称]
	public void setN_emwo_ptname_like(String n_emwo_ptname_like) {
        this.n_emwo_ptname_like = n_emwo_ptname_like;
        if(!ObjectUtils.isEmpty(this.n_emwo_ptname_like)){
            this.getSearchCond().like("emwo_ptname", n_emwo_ptname_like);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_rdeptid_eq;//[责任部门]
	public void setN_rdeptid_eq(String n_rdeptid_eq) {
        this.n_rdeptid_eq = n_rdeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptid_eq)){
            this.getSearchCond().eq("rdeptid", n_rdeptid_eq);
        }
    }
	private String n_rempname_eq;//[责任人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[责任人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_mpersonname_eq;//[制定人]
	public void setN_mpersonname_eq(String n_mpersonname_eq) {
        this.n_mpersonname_eq = n_mpersonname_eq;
        if(!ObjectUtils.isEmpty(this.n_mpersonname_eq)){
            this.getSearchCond().eq("mpersonname", n_mpersonname_eq);
        }
    }
	private String n_mpersonname_like;//[制定人]
	public void setN_mpersonname_like(String n_mpersonname_like) {
        this.n_mpersonname_like = n_mpersonname_like;
        if(!ObjectUtils.isEmpty(this.n_mpersonname_like)){
            this.getSearchCond().like("mpersonname", n_mpersonname_like);
        }
    }
	private String n_wpersonid_eq;//[执行人]
	public void setN_wpersonid_eq(String n_wpersonid_eq) {
        this.n_wpersonid_eq = n_wpersonid_eq;
        if(!ObjectUtils.isEmpty(this.n_wpersonid_eq)){
            this.getSearchCond().eq("wpersonid", n_wpersonid_eq);
        }
    }
	private String n_rdeptname_eq;//[责任部门]
	public void setN_rdeptname_eq(String n_rdeptname_eq) {
        this.n_rdeptname_eq = n_rdeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_eq)){
            this.getSearchCond().eq("rdeptname", n_rdeptname_eq);
        }
    }
	private String n_rdeptname_like;//[责任部门]
	public void setN_rdeptname_like(String n_rdeptname_like) {
        this.n_rdeptname_like = n_rdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_like)){
            this.getSearchCond().like("rdeptname", n_rdeptname_like);
        }
    }
	private Integer n_wostate_eq;//[工单状态]
	public void setN_wostate_eq(Integer n_wostate_eq) {
        this.n_wostate_eq = n_wostate_eq;
        if(!ObjectUtils.isEmpty(this.n_wostate_eq)){
            this.getSearchCond().eq("wostate", n_wostate_eq);
        }
    }
	private String n_wotype_eq;//[工单类型]
	public void setN_wotype_eq(String n_wotype_eq) {
        this.n_wotype_eq = n_wotype_eq;
        if(!ObjectUtils.isEmpty(this.n_wotype_eq)){
            this.getSearchCond().eq("wotype", n_wotype_eq);
        }
    }
	private String n_rfoacname_eq;//[方案]
	public void setN_rfoacname_eq(String n_rfoacname_eq) {
        this.n_rfoacname_eq = n_rfoacname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfoacname_eq)){
            this.getSearchCond().eq("rfoacname", n_rfoacname_eq);
        }
    }
	private String n_rfoacname_like;//[方案]
	public void setN_rfoacname_like(String n_rfoacname_like) {
        this.n_rfoacname_like = n_rfoacname_like;
        if(!ObjectUtils.isEmpty(this.n_rfoacname_like)){
            this.getSearchCond().like("rfoacname", n_rfoacname_like);
        }
    }
	private String n_wooriname_eq;//[工单来源]
	public void setN_wooriname_eq(String n_wooriname_eq) {
        this.n_wooriname_eq = n_wooriname_eq;
        if(!ObjectUtils.isEmpty(this.n_wooriname_eq)){
            this.getSearchCond().eq("wooriname", n_wooriname_eq);
        }
    }
	private String n_wooriname_like;//[工单来源]
	public void setN_wooriname_like(String n_wooriname_like) {
        this.n_wooriname_like = n_wooriname_like;
        if(!ObjectUtils.isEmpty(this.n_wooriname_like)){
            this.getSearchCond().like("wooriname", n_wooriname_like);
        }
    }
	private String n_rteamname_eq;//[责任班组]
	public void setN_rteamname_eq(String n_rteamname_eq) {
        this.n_rteamname_eq = n_rteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamname_eq)){
            this.getSearchCond().eq("rteamname", n_rteamname_eq);
        }
    }
	private String n_rteamname_like;//[责任班组]
	public void setN_rteamname_like(String n_rteamname_like) {
        this.n_rteamname_like = n_rteamname_like;
        if(!ObjectUtils.isEmpty(this.n_rteamname_like)){
            this.getSearchCond().like("rteamname", n_rteamname_like);
        }
    }
	private String n_rfodename_eq;//[现象]
	public void setN_rfodename_eq(String n_rfodename_eq) {
        this.n_rfodename_eq = n_rfodename_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodename_eq)){
            this.getSearchCond().eq("rfodename", n_rfodename_eq);
        }
    }
	private String n_rfodename_like;//[现象]
	public void setN_rfodename_like(String n_rfodename_like) {
        this.n_rfodename_like = n_rfodename_like;
        if(!ObjectUtils.isEmpty(this.n_rfodename_like)){
            this.getSearchCond().like("rfodename", n_rfodename_like);
        }
    }
	private String n_rfocaname_eq;//[原因]
	public void setN_rfocaname_eq(String n_rfocaname_eq) {
        this.n_rfocaname_eq = n_rfocaname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfocaname_eq)){
            this.getSearchCond().eq("rfocaname", n_rfocaname_eq);
        }
    }
	private String n_rfocaname_like;//[原因]
	public void setN_rfocaname_like(String n_rfocaname_like) {
        this.n_rfocaname_like = n_rfocaname_like;
        if(!ObjectUtils.isEmpty(this.n_rfocaname_like)){
            this.getSearchCond().like("rfocaname", n_rfocaname_like);
        }
    }
	private String n_dpname_eq;//[测点]
	public void setN_dpname_eq(String n_dpname_eq) {
        this.n_dpname_eq = n_dpname_eq;
        if(!ObjectUtils.isEmpty(this.n_dpname_eq)){
            this.getSearchCond().eq("dpname", n_dpname_eq);
        }
    }
	private String n_dpname_like;//[测点]
	public void setN_dpname_like(String n_dpname_like) {
        this.n_dpname_like = n_dpname_like;
        if(!ObjectUtils.isEmpty(this.n_dpname_like)){
            this.getSearchCond().like("dpname", n_dpname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_wopname_eq;//[上级工单]
	public void setN_wopname_eq(String n_wopname_eq) {
        this.n_wopname_eq = n_wopname_eq;
        if(!ObjectUtils.isEmpty(this.n_wopname_eq)){
            this.getSearchCond().eq("wopname", n_wopname_eq);
        }
    }
	private String n_wopname_like;//[上级工单]
	public void setN_wopname_like(String n_wopname_like) {
        this.n_wopname_like = n_wopname_like;
        if(!ObjectUtils.isEmpty(this.n_wopname_like)){
            this.getSearchCond().like("wopname", n_wopname_like);
        }
    }
	private String n_rfomoname_eq;//[模式]
	public void setN_rfomoname_eq(String n_rfomoname_eq) {
        this.n_rfomoname_eq = n_rfomoname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_eq)){
            this.getSearchCond().eq("rfomoname", n_rfomoname_eq);
        }
    }
	private String n_rfomoname_like;//[模式]
	public void setN_rfomoname_like(String n_rfomoname_like) {
        this.n_rfomoname_like = n_rfomoname_like;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_like)){
            this.getSearchCond().like("rfomoname", n_rfomoname_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_rservicename_eq;//[服务商]
	public void setN_rservicename_eq(String n_rservicename_eq) {
        this.n_rservicename_eq = n_rservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_rservicename_eq)){
            this.getSearchCond().eq("rservicename", n_rservicename_eq);
        }
    }
	private String n_rservicename_like;//[服务商]
	public void setN_rservicename_like(String n_rservicename_like) {
        this.n_rservicename_like = n_rservicename_like;
        if(!ObjectUtils.isEmpty(this.n_rservicename_like)){
            this.getSearchCond().like("rservicename", n_rservicename_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_rfocaid_eq;//[原因]
	public void setN_rfocaid_eq(String n_rfocaid_eq) {
        this.n_rfocaid_eq = n_rfocaid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfocaid_eq)){
            this.getSearchCond().eq("rfocaid", n_rfocaid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_wopid_eq;//[上级工单]
	public void setN_wopid_eq(String n_wopid_eq) {
        this.n_wopid_eq = n_wopid_eq;
        if(!ObjectUtils.isEmpty(this.n_wopid_eq)){
            this.getSearchCond().eq("wopid", n_wopid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_rteamid_eq;//[责任班组]
	public void setN_rteamid_eq(String n_rteamid_eq) {
        this.n_rteamid_eq = n_rteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamid_eq)){
            this.getSearchCond().eq("rteamid", n_rteamid_eq);
        }
    }
	private String n_wooriid_eq;//[工单来源]
	public void setN_wooriid_eq(String n_wooriid_eq) {
        this.n_wooriid_eq = n_wooriid_eq;
        if(!ObjectUtils.isEmpty(this.n_wooriid_eq)){
            this.getSearchCond().eq("wooriid", n_wooriid_eq);
        }
    }
	private String n_dpid_eq;//[测点]
	public void setN_dpid_eq(String n_dpid_eq) {
        this.n_dpid_eq = n_dpid_eq;
        if(!ObjectUtils.isEmpty(this.n_dpid_eq)){
            this.getSearchCond().eq("dpid", n_dpid_eq);
        }
    }
	private String n_rserviceid_eq;//[服务商]
	public void setN_rserviceid_eq(String n_rserviceid_eq) {
        this.n_rserviceid_eq = n_rserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_rserviceid_eq)){
            this.getSearchCond().eq("rserviceid", n_rserviceid_eq);
        }
    }
	private String n_rfoacid_eq;//[方案]
	public void setN_rfoacid_eq(String n_rfoacid_eq) {
        this.n_rfoacid_eq = n_rfoacid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfoacid_eq)){
            this.getSearchCond().eq("rfoacid", n_rfoacid_eq);
        }
    }
	private String n_rfodeid_eq;//[现象]
	public void setN_rfodeid_eq(String n_rfodeid_eq) {
        this.n_rfodeid_eq = n_rfodeid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodeid_eq)){
            this.getSearchCond().eq("rfodeid", n_rfodeid_eq);
        }
    }
	private String n_rfomoid_eq;//[模式]
	public void setN_rfomoid_eq(String n_rfomoid_eq) {
        this.n_rfomoid_eq = n_rfomoid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoid_eq)){
            this.getSearchCond().eq("rfomoid", n_rfomoid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emwo_ptname", query)
            );
		 }
	}
}



