package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[损溢单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMPL_BASE", resultMap = "EMItemPLResultMap")
@ApiModel("损溢单")
public class EMItemPL extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 损溢日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sdate")
    @ApiModelProperty("损溢日期")
    private Timestamp sdate;
    /**
     * 单价
     */
    @TableField(value = "price")
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @ApiModelProperty("流程步骤")
    private String wfstep;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 总金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @ApiModelProperty("总金额")
    private Double amount;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 批次
     */
    @DEField(defaultValue = "NA")
    @TableField(value = "batcode")
    @JSONField(name = "batcode")
    @JsonProperty("batcode")
    @ApiModelProperty("批次")
    private String batcode;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;
    /**
     * 数量
     */
    @TableField(value = "psum")
    @JSONField(name = "psum")
    @JsonProperty("psum")
    @ApiModelProperty("数量")
    private Double psum;
    /**
     * 损溢状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "tradestate")
    @JSONField(name = "tradestate")
    @JsonProperty("tradestate")
    @ApiModelProperty("损溢状态")
    private Integer tradestate;
    /**
     * sap传输状态
     */
    @TableField(value = "sap")
    @JSONField(name = "sap")
    @JsonProperty("sap")
    @ApiModelProperty("sap传输状态")
    private String sap;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;
    /**
     * 损溢单号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemplid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemplid")
    @JsonProperty("emitemplid")
    @ApiModelProperty("损溢单号")
    private String emitemplid;
    /**
     * 损溢单信息
     */
    @TableField(exist = false)
    @JSONField(name = "itemroutinfo")
    @JsonProperty("itemroutinfo")
    @ApiModelProperty("损溢单信息")
    private String itemroutinfo;
    /**
     * 损溢单名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emitemplname")
    @JSONField(name = "emitemplname")
    @JsonProperty("emitemplname")
    @ApiModelProperty("损溢单名称")
    private String emitemplname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * sap传输失败原因
     */
    @TableField(value = "sapreason1")
    @JSONField(name = "sapreason1")
    @JsonProperty("sapreason1")
    @ApiModelProperty("sap传输失败原因")
    private String sapreason1;
    /**
     * 损益出入标志
     */
    @TableField(value = "inoutflag")
    @JSONField(name = "inoutflag")
    @JsonProperty("inoutflag")
    @ApiModelProperty("损益出入标志")
    private Integer inoutflag;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * sap控制
     */
    @TableField(value = "sapcontrol")
    @JSONField(name = "sapcontrol")
    @JsonProperty("sapcontrol")
    @ApiModelProperty("sap控制")
    private Integer sapcontrol;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 库位
     */
    @TableField(exist = false)
    @JSONField(name = "storepartname")
    @JsonProperty("storepartname")
    @ApiModelProperty("库位")
    private String storepartname;
    /**
     * 物品
     */
    @TableField(exist = false)
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @ApiModelProperty("物品")
    private String itemname;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "storename")
    @JsonProperty("storename")
    @ApiModelProperty("仓库")
    private String storename;
    /**
     * 入库单
     */
    @TableField(exist = false)
    @JSONField(name = "rname")
    @JsonProperty("rname")
    @ApiModelProperty("入库单")
    private String rname;
    /**
     * 入库单
     */
    @TableField(value = "rid")
    @JSONField(name = "rid")
    @JsonProperty("rid")
    @ApiModelProperty("入库单")
    private String rid;
    /**
     * 仓库
     */
    @TableField(value = "storeid")
    @JSONField(name = "storeid")
    @JsonProperty("storeid")
    @ApiModelProperty("仓库")
    private String storeid;
    /**
     * 物品
     */
    @TableField(value = "itemid")
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @ApiModelProperty("物品")
    private String itemid;
    /**
     * 库位
     */
    @TableField(value = "storepartid")
    @JSONField(name = "storepartid")
    @JsonProperty("storepartid")
    @ApiModelProperty("库位")
    private String storepartid;
    /**
     * 损溢人
     */
    @TableField(value = "sempid")
    @JSONField(name = "sempid")
    @JsonProperty("sempid")
    @ApiModelProperty("损溢人")
    private String sempid;
    /**
     * 损溢人
     */
    @TableField(exist = false)
    @JSONField(name = "sempname")
    @JsonProperty("sempname")
    @ApiModelProperty("损溢人")
    private String sempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemRIn r;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItem item;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMStore store;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid;



    /**
     * 设置 [损溢日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [损溢日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sdate);
    }
    /**
     * 设置 [单价]
     */
    public void setPrice(Double price) {
        this.price = price;
        this.modify("price", price);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [总金额]
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.modify("amount", amount);
    }

    /**
     * 设置 [批次]
     */
    public void setBatcode(String batcode) {
        this.batcode = batcode;
        this.modify("batcode", batcode);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [数量]
     */
    public void setPsum(Double psum) {
        this.psum = psum;
        this.modify("psum", psum);
    }

    /**
     * 设置 [损溢状态]
     */
    public void setTradestate(Integer tradestate) {
        this.tradestate = tradestate;
        this.modify("tradestate", tradestate);
    }

    /**
     * 设置 [sap传输状态]
     */
    public void setSap(String sap) {
        this.sap = sap;
        this.modify("sap", sap);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [损溢单名称]
     */
    public void setEmitemplname(String emitemplname) {
        this.emitemplname = emitemplname;
        this.modify("emitemplname", emitemplname);
    }

    /**
     * 设置 [sap传输失败原因]
     */
    public void setSapreason1(String sapreason1) {
        this.sapreason1 = sapreason1;
        this.modify("sapreason1", sapreason1);
    }

    /**
     * 设置 [损益出入标志]
     */
    public void setInoutflag(Integer inoutflag) {
        this.inoutflag = inoutflag;
        this.modify("inoutflag", inoutflag);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [sap控制]
     */
    public void setSapcontrol(Integer sapcontrol) {
        this.sapcontrol = sapcontrol;
        this.modify("sapcontrol", sapcontrol);
    }

    /**
     * 设置 [入库单]
     */
    public void setRid(String rid) {
        this.rid = rid;
        this.modify("rid", rid);
    }

    /**
     * 设置 [仓库]
     */
    public void setStoreid(String storeid) {
        this.storeid = storeid;
        this.modify("storeid", storeid);
    }

    /**
     * 设置 [物品]
     */
    public void setItemid(String itemid) {
        this.itemid = itemid;
        this.modify("itemid", itemid);
    }

    /**
     * 设置 [库位]
     */
    public void setStorepartid(String storepartid) {
        this.storepartid = storepartid;
        this.modify("storepartid", storepartid);
    }

    /**
     * 设置 [损溢人]
     */
    public void setSempid(String sempid) {
        this.sempid = sempid;
        this.modify("sempid", sempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemplid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


