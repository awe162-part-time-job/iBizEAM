package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLocationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQLocation] 服务对象接口
 */
public interface IEMEQLocationService extends IService<EMEQLocation> {

    boolean create(EMEQLocation et);
    void createBatch(List<EMEQLocation> list);
    boolean update(EMEQLocation et);
    void updateBatch(List<EMEQLocation> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQLocation get(String key);
    EMEQLocation getDraft(EMEQLocation et);
    boolean checkKey(EMEQLocation et);
    boolean save(EMEQLocation et);
    void saveBatch(List<EMEQLocation> list);
    Page<EMEQLocation> searchDefault(EMEQLocationSearchContext context);
    Page<EMEQLocation> searchSub(EMEQLocationSearchContext context);
    List<EMEQLocation> selectByMajorequipid(String emequipid);
    void removeByMajorequipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQLocation> getEmeqlocationByIds(List<String> ids);
    List<EMEQLocation> getEmeqlocationByEntities(List<EMEQLocation> entities);
}


