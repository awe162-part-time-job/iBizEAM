package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWGMap;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWGMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWGMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMDRWGMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[文档引用] 服务对象接口实现
 */
@Slf4j
@Service("EMDRWGMapServiceImpl")
public class EMDRWGMapServiceImpl extends ServiceImpl<EMDRWGMapMapper, EMDRWGMap> implements IEMDRWGMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDRWGService emdrwgService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMDRWGMap et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdrwgmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMDRWGMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMDRWGMap et) {
        fillParentData(et);
        emobjmapService.update(emdrwgmapInheritMapping.toEmobjmap(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emdrwgmapid", et.getEmdrwgmapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmdrwgmapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMDRWGMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMDRWGMap get(String key) {
        EMDRWGMap et = getById(key);
        if(et == null){
            et = new EMDRWGMap();
            et.setEmdrwgmapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMDRWGMap getDraft(EMDRWGMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMDRWGMap et) {
        return (!ObjectUtils.isEmpty(et.getEmdrwgmapid())) && (!Objects.isNull(this.getById(et.getEmdrwgmapid())));
    }
    @Override
    @Transactional
    public boolean save(EMDRWGMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMDRWGMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMDRWGMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMDRWGMap> create = new ArrayList<>();
        List<EMDRWGMap> update = new ArrayList<>();
        for (EMDRWGMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmdrwgmapid()) || ObjectUtils.isEmpty(getById(et.getEmdrwgmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMDRWGMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMDRWGMap> create = new ArrayList<>();
        List<EMDRWGMap> update = new ArrayList<>();
        for (EMDRWGMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmdrwgmapid()) || ObjectUtils.isEmpty(getById(et.getEmdrwgmapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMDRWGMap> selectByDrwgid(String emdrwgid) {
        return baseMapper.selectByDrwgid(emdrwgid);
    }
    @Override
    public void removeByDrwgid(String emdrwgid) {
        this.remove(new QueryWrapper<EMDRWGMap>().eq("drwgid",emdrwgid));
    }

	@Override
    public List<EMDRWGMap> selectByRefobjid(String emobjectid) {
        return baseMapper.selectByRefobjid(emobjectid);
    }
    @Override
    public void removeByRefobjid(String emobjectid) {
        this.remove(new QueryWrapper<EMDRWGMap>().eq("refobjid",emobjectid));
    }


    /**
     * 查询集合 ByEQ
     */
    @Override
    public Page<EMDRWGMap> searchByEQ(EMDRWGMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMDRWGMap> pages=baseMapper.searchByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMDRWGMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMDRWGMap> searchDefault(EMDRWGMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMDRWGMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMDRWGMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMDRWGMap et){
        //实体关系[DER1N_EMDRWGMAP_EMDRWG_DRWGID]
        if(!ObjectUtils.isEmpty(et.getDrwgid())){
            cn.ibizlab.eam.core.eam_core.domain.EMDRWG drwg=et.getDrwg();
            if(ObjectUtils.isEmpty(drwg)){
                cn.ibizlab.eam.core.eam_core.domain.EMDRWG majorEntity=emdrwgService.get(et.getDrwgid());
                et.setDrwg(majorEntity);
                drwg=majorEntity;
            }
            et.setDrwgname(drwg.getEmdrwgname());
        }
        //实体关系[DER1N_EMDRWGMAP_EMOBJECT_REFOBJID]
        if(!ObjectUtils.isEmpty(et.getRefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject refobj=et.getRefobj();
            if(ObjectUtils.isEmpty(refobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getRefobjid());
                et.setRefobj(majorEntity);
                refobj=majorEntity;
            }
            et.setRefobjname(refobj.getEmobjectname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMDRWGMapInheritMapping emdrwgmapInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMDRWGMap et){
        if(ObjectUtils.isEmpty(et.getEmdrwgmapid()))
            et.setEmdrwgmapid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emdrwgmapInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","DRWGMAP");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMDRWGMap> getEmdrwgmapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMDRWGMap> getEmdrwgmapByEntities(List<EMDRWGMap> entities) {
        List ids =new ArrayList();
        for(EMDRWGMap entity : entities){
            Serializable id=entity.getEmdrwgmapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMDRWGMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



