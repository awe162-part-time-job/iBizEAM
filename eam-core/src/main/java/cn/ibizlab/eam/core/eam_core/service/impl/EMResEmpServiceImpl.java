package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMResEmp;
import cn.ibizlab.eam.core.eam_core.filter.EMResEmpSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMResEmpService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMResEmpMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[员工资源] 服务对象接口实现
 */
@Slf4j
@Service("EMResEmpServiceImpl")
public class EMResEmpServiceImpl extends ServiceImpl<EMResEmpMapper, EMResEmp> implements IEMResEmpService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMResEmp et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresempid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMResEmp> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMResEmp et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emresempid", et.getEmresempid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresempid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMResEmp> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMResEmp get(String key) {
        EMResEmp et = getById(key);
        if(et == null){
            et = new EMResEmp();
            et.setEmresempid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMResEmp getDraft(EMResEmp et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMResEmp et) {
        return (!ObjectUtils.isEmpty(et.getEmresempid())) && (!Objects.isNull(this.getById(et.getEmresempid())));
    }
    @Override
    @Transactional
    public boolean save(EMResEmp et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMResEmp et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMResEmp> list) {
        list.forEach(item->fillParentData(item));
        List<EMResEmp> create = new ArrayList<>();
        List<EMResEmp> update = new ArrayList<>();
        for (EMResEmp et : list) {
            if (ObjectUtils.isEmpty(et.getEmresempid()) || ObjectUtils.isEmpty(getById(et.getEmresempid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMResEmp> list) {
        list.forEach(item->fillParentData(item));
        List<EMResEmp> create = new ArrayList<>();
        List<EMResEmp> update = new ArrayList<>();
        for (EMResEmp et : list) {
            if (ObjectUtils.isEmpty(et.getEmresempid()) || ObjectUtils.isEmpty(getById(et.getEmresempid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMResEmp> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMResEmp>().eq("equipid",emequipid));
    }

	@Override
    public List<EMResEmp> selectByResrefobjid(String emresrefobjid) {
        return baseMapper.selectByResrefobjid(emresrefobjid);
    }
    @Override
    public void removeByResrefobjid(String emresrefobjid) {
        this.remove(new QueryWrapper<EMResEmp>().eq("resrefobjid",emresrefobjid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMResEmp> searchDefault(EMResEmpSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResEmp> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResEmp>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMResEmp et){
        //实体关系[DER1N_EMRESEMP_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMRESEMP_EMRESREFOBJ_RESREFOBJID]
        if(!ObjectUtils.isEmpty(et.getResrefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMResRefObj resrefobj=et.getResrefobj();
            if(ObjectUtils.isEmpty(resrefobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMResRefObj majorEntity=emresrefobjService.get(et.getResrefobjid());
                et.setResrefobj(majorEntity);
                resrefobj=majorEntity;
            }
            et.setResrefobjname(resrefobj.getEmresrefobjname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMResEmp> getEmresempByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMResEmp> getEmresempByEntities(List<EMResEmp> entities) {
        List ids =new ArrayList();
        for(EMResEmp entity : entities){
            Serializable id=entity.getEmresempid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMResEmpService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



