

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareMap;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQSpareMapInheritMapping {

    @Mappings({
        @Mapping(source ="emeqsparemapid",target = "emobjmapid"),
        @Mapping(source ="emeqsparemapname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="eqspareid",target = "objid"),
        @Mapping(source ="refobjid",target = "objpid"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObjMap toEmobjmap(EMEQSpareMap minorEntity);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emeqsparemapid"),
        @Mapping(source ="emobjmapname" ,target = "emeqsparemapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objid",target = "eqspareid"),
        @Mapping(source ="objpid",target = "refobjid"),
    })
    EMEQSpareMap toEmeqsparemap(EMObjMap majorEntity);

    List<EMObjMap> toEmobjmap(List<EMEQSpareMap> minorEntities);

    List<EMEQSpareMap> toEmeqsparemap(List<EMObjMap> majorEntities);

}


