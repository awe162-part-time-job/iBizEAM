package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAssetHist;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetHistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAssetHist] 服务对象接口
 */
public interface IEMAssetHistService extends IService<EMAssetHist> {

    boolean create(EMAssetHist et);
    void createBatch(List<EMAssetHist> list);
    boolean update(EMAssetHist et);
    void updateBatch(List<EMAssetHist> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAssetHist get(String key);
    EMAssetHist getDraft(EMAssetHist et);
    boolean checkKey(EMAssetHist et);
    boolean save(EMAssetHist et);
    void saveBatch(List<EMAssetHist> list);
    Page<EMAssetHist> searchDefault(EMAssetHistSearchContext context);
    List<EMAssetHist> selectByAssetid(String emassetid);
    void removeByAssetid(String emassetid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAssetHist> getEmassethistByIds(List<String> ids);
    List<EMAssetHist> getEmassethistByEntities(List<EMAssetHist> entities);
}


