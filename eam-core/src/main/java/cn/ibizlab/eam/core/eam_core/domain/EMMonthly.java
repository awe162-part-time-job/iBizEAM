package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[维修中心月度计划]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMMONTHLY_BASE", resultMap = "EMMonthlyResultMap")
@ApiModel("维修中心月度计划")
public class EMMonthly extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 编号
     */
    @DEField(isKeyField = true)
    @TableId(value = "emmonthlyid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emmonthlyid")
    @JsonProperty("emmonthlyid")
    @ApiModelProperty("编号")
    private String emmonthlyid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 安全工作
     */
    @TableField(value = "safework")
    @JSONField(name = "safework")
    @JsonProperty("safework")
    @ApiModelProperty("安全工作")
    private String safework;
    /**
     * 上个月设备例会布置任务完成情况
     */
    @TableField(value = "lastmonth2")
    @JSONField(name = "lastmonth2")
    @JsonProperty("lastmonth2")
    @ApiModelProperty("上个月设备例会布置任务完成情况")
    private String lastmonth2;
    /**
     * 报告名称
     */
    @TableField(value = "emmonthlyname")
    @JSONField(name = "emmonthlyname")
    @JsonProperty("emmonthlyname")
    @ApiModelProperty("报告名称")
    private String emmonthlyname;
    /**
     * 上月份其他主要应急维修及临时任务完成情况
     */
    @TableField(value = "lastmonth")
    @JSONField(name = "lastmonth")
    @JsonProperty("lastmonth")
    @ApiModelProperty("上月份其他主要应急维修及临时任务完成情况")
    private String lastmonth;
    /**
     * 本月份计划维修工作
     */
    @TableField(value = "thismonth")
    @JSONField(name = "thismonth")
    @JsonProperty("thismonth")
    @ApiModelProperty("本月份计划维修工作")
    private String thismonth;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;



    /**
     * 设置 [安全工作]
     */
    public void setSafework(String safework) {
        this.safework = safework;
        this.modify("safework", safework);
    }

    /**
     * 设置 [上个月设备例会布置任务完成情况]
     */
    public void setLastmonth2(String lastmonth2) {
        this.lastmonth2 = lastmonth2;
        this.modify("lastmonth2", lastmonth2);
    }

    /**
     * 设置 [报告名称]
     */
    public void setEmmonthlyname(String emmonthlyname) {
        this.emmonthlyname = emmonthlyname;
        this.modify("emmonthlyname", emmonthlyname);
    }

    /**
     * 设置 [上月份其他主要应急维修及临时任务完成情况]
     */
    public void setLastmonth(String lastmonth) {
        this.lastmonth = lastmonth;
        this.modify("lastmonth", lastmonth);
    }

    /**
     * 设置 [本月份计划维修工作]
     */
    public void setThismonth(String thismonth) {
        this.thismonth = thismonth;
        this.modify("thismonth", thismonth);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emmonthlyid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


