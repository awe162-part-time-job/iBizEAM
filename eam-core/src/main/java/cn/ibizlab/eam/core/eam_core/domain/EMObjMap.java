package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[对象关系]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMOBJMAP_BASE", resultMap = "EMObjMapResultMap")
@ApiModel("对象关系")
public class EMObjMap extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 对象关系标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emobjmapid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emobjmapid")
    @JsonProperty("emobjmapid")
    @ApiModelProperty("对象关系标识")
    private String emobjmapid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 设备编号
     */
    @TableField(exist = false)
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @ApiModelProperty("设备编号")
    private String equipid;
    /**
     * 对象关系类型
     */
    @TableField(value = "emobjmaptype")
    @JSONField(name = "emobjmaptype")
    @JsonProperty("emobjmaptype")
    @ApiModelProperty("对象关系类型")
    private String emobjmaptype;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 对象关系名称
     */
    @DEField(defaultValue = "OBJMAPNAME")
    @TableField(value = "emobjmapname")
    @JSONField(name = "emobjmapname")
    @JsonProperty("emobjmapname")
    @ApiModelProperty("对象关系名称")
    private String emobjmapname;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 上级对象类型
     */
    @TableField(exist = false)
    @JSONField(name = "objptype")
    @JsonProperty("objptype")
    @ApiModelProperty("上级对象类型")
    private String objptype;
    /**
     * 主设备
     */
    @TableField(exist = false)
    @JSONField(name = "majorequipname")
    @JsonProperty("majorequipname")
    @ApiModelProperty("主设备")
    private String majorequipname;
    /**
     * 主设备
     */
    @TableField(exist = false)
    @JSONField(name = "majorequipid")
    @JsonProperty("majorequipid")
    @ApiModelProperty("主设备")
    private String majorequipid;
    /**
     * 上级对象
     */
    @TableField(exist = false)
    @JSONField(name = "objpname")
    @JsonProperty("objpname")
    @ApiModelProperty("上级对象")
    private String objpname;
    /**
     * 对象类型
     */
    @TableField(exist = false)
    @JSONField(name = "objtype")
    @JsonProperty("objtype")
    @ApiModelProperty("对象类型")
    private String objtype;
    /**
     * 对象
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @ApiModelProperty("对象")
    private String objname;
    /**
     * 对象
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @ApiModelProperty("对象")
    private String objid;
    /**
     * 上级对象
     */
    @TableField(value = "objpid")
    @JSONField(name = "objpid")
    @JsonProperty("objpid")
    @ApiModelProperty("上级对象")
    private String objpid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject objp;



    /**
     * 设置 [对象关系类型]
     */
    public void setEmobjmaptype(String emobjmaptype) {
        this.emobjmaptype = emobjmaptype;
        this.modify("emobjmaptype", emobjmaptype);
    }

    /**
     * 设置 [对象关系名称]
     */
    public void setEmobjmapname(String emobjmapname) {
        this.emobjmapname = emobjmapname;
        this.modify("emobjmapname", emobjmapname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [对象]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [上级对象]
     */
    public void setObjpid(String objpid) {
        this.objpid = objpid;
        this.modify("objpid", objpid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emobjmapid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


