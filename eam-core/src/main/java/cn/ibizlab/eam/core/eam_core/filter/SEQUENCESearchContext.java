package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.SEQUENCE;
/**
 * 关系型数据实体[SEQUENCE] 查询条件对象
 */
@Slf4j
@Data
public class SEQUENCESearchContext extends QueryWrapperContext<SEQUENCE> {

	private String n_sequencename_like;//[序列号名称]
	public void setN_sequencename_like(String n_sequencename_like) {
        this.n_sequencename_like = n_sequencename_like;
        if(!ObjectUtils.isEmpty(this.n_sequencename_like)){
            this.getSearchCond().like("sequencename", n_sequencename_like);
        }
    }
	private String n_code_eq;//[实体名]
	public void setN_code_eq(String n_code_eq) {
        this.n_code_eq = n_code_eq;
        if(!ObjectUtils.isEmpty(this.n_code_eq)){
            this.getSearchCond().eq("code", n_code_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("sequencename", query)
            );
		 }
	}
}



