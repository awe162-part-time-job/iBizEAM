package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_INNER;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_INNERSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO_INNER] 服务对象接口
 */
public interface IEMWO_INNERService extends IService<EMWO_INNER> {

    boolean create(EMWO_INNER et);
    void createBatch(List<EMWO_INNER> list);
    boolean update(EMWO_INNER et);
    void updateBatch(List<EMWO_INNER> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWO_INNER get(String key);
    EMWO_INNER getDraft(EMWO_INNER et);
    EMWO_INNER acceptance(EMWO_INNER et);
    boolean acceptanceBatch(List<EMWO_INNER> etList);
    boolean checkKey(EMWO_INNER et);
    EMWO_INNER formUpdateByEmequipid(EMWO_INNER et);
    boolean save(EMWO_INNER et);
    void saveBatch(List<EMWO_INNER> list);
    EMWO_INNER submit(EMWO_INNER et);
    EMWO_INNER unAcceptance(EMWO_INNER et);
    Page<EMWO_INNER> searchCalendar(EMWO_INNERSearchContext context);
    Page<EMWO_INNER> searchConfirmed(EMWO_INNERSearchContext context);
    Page<EMWO_INNER> searchDefault(EMWO_INNERSearchContext context);
    Page<EMWO_INNER> searchDraft(EMWO_INNERSearchContext context);
    Page<EMWO_INNER> searchToConfirm(EMWO_INNERSearchContext context);
    List<EMWO_INNER> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMWO_INNER> selectByEmeitiresid(String emeitiresid);
    void removeByEmeitiresid(String emeitiresid);
    List<EMWO_INNER> selectByEmeqlctgssid(String emeqlocationid);
    void removeByEmeqlctgssid(String emeqlocationid);
    List<EMWO_INNER> selectByEmeqlcttiresid(String emeqlocationid);
    void removeByEmeqlcttiresid(String emeqlocationid);
    List<EMWO_INNER> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWO_INNER> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMWO_INNER> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWO_INNER> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMWO_INNER> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMWO_INNER> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMWO_INNER> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMWO_INNER> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMWO_INNER> selectByWooriid(String emwooriid);
    void removeByWooriid(String emwooriid);
    List<EMWO_INNER> selectByWopid(String emwoid);
    void removeByWopid(String emwoid);
    List<EMWO_INNER> selectByRdeptid(String pfdeptid);
    void removeByRdeptid(String pfdeptid);
    List<EMWO_INNER> selectByMpersonid(String pfempid);
    void removeByMpersonid(String pfempid);
    List<EMWO_INNER> selectByRecvpersonid(String pfempid);
    void removeByRecvpersonid(String pfempid);
    List<EMWO_INNER> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMWO_INNER> selectByWpersonid(String pfempid);
    void removeByWpersonid(String pfempid);
    List<EMWO_INNER> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO_INNER> getEmwoInnerByIds(List<String> ids);
    List<EMWO_INNER> getEmwoInnerByEntities(List<EMWO_INNER> entities);
}


