package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_EN;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_ENSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO_EN] 服务对象接口
 */
public interface IEMWO_ENService extends IService<EMWO_EN> {

    boolean create(EMWO_EN et);
    void createBatch(List<EMWO_EN> list);
    boolean update(EMWO_EN et);
    void updateBatch(List<EMWO_EN> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWO_EN get(String key);
    EMWO_EN getDraft(EMWO_EN et);
    EMWO_EN acceptance(EMWO_EN et);
    boolean acceptanceBatch(List<EMWO_EN> etList);
    boolean checkKey(EMWO_EN et);
    EMWO_EN formUpdateByEmequipid(EMWO_EN et);
    boolean save(EMWO_EN et);
    void saveBatch(List<EMWO_EN> list);
    EMWO_EN submit(EMWO_EN et);
    EMWO_EN unAcceptance(EMWO_EN et);
    Page<EMWO_EN> searchCalendar(EMWO_ENSearchContext context);
    Page<EMWO_EN> searchConfirmed(EMWO_ENSearchContext context);
    Page<EMWO_EN> searchDefault(EMWO_ENSearchContext context);
    Page<EMWO_EN> searchDraft(EMWO_ENSearchContext context);
    Page<EMWO_EN> searchToConfirm(EMWO_ENSearchContext context);
    List<EMWO_EN> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMWO_EN> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWO_EN> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMWO_EN> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWO_EN> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMWO_EN> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMWO_EN> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMWO_EN> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMWO_EN> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMWO_EN> selectByWooriid(String emwooriid);
    void removeByWooriid(String emwooriid);
    List<EMWO_EN> selectByWopid(String emwoid);
    void removeByWopid(String emwoid);
    List<EMWO_EN> selectByRdeptid(String pfdeptid);
    void removeByRdeptid(String pfdeptid);
    List<EMWO_EN> selectByMpersonid(String pfempid);
    void removeByMpersonid(String pfempid);
    List<EMWO_EN> selectByRecvpersonid(String pfempid);
    void removeByRecvpersonid(String pfempid);
    List<EMWO_EN> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMWO_EN> selectByWpersonid(String pfempid);
    void removeByWpersonid(String pfempid);
    List<EMWO_EN> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO_EN> getEmwoEnByIds(List<String> ids);
    List<EMWO_EN> getEmwoEnByEntities(List<EMWO_EN> entities);
}


