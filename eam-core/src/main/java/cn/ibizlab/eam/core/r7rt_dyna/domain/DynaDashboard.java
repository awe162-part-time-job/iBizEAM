package cn.ibizlab.eam.core.r7rt_dyna.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import cn.ibizlab.eam.util.domain.EntityClient;

/**
 * ServiceApi [动态数据看板] 对象
 */
@Data
@ApiModel("动态数据看板")
public class DynaDashboard extends EntityClient implements Serializable {

    /**
     * 用户标识
     */
    @JSONField(name = "userid")
    @JsonProperty("userid")
    @ApiModelProperty("用户标识")
    private String userid;

    /**
     * 实体标识
     */
    @DEField(isKeyField = true)
    @JSONField(name = "dynadashboardid")
    @JsonProperty("dynadashboardid")
    @ApiModelProperty("实体标识")
    private String dynadashboardid;

    /**
     * 实体名称
     */
    @JSONField(name = "dynadashboardname")
    @JsonProperty("dynadashboardname")
    @ApiModelProperty("实体名称")
    private String dynadashboardname;

    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 模型标识
     */
    @JSONField(name = "modelid")
    @JsonProperty("modelid")
    @ApiModelProperty("模型标识")
    private String modelid;

    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate" , format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 应用标识
     */
    @JSONField(name = "appid")
    @JsonProperty("appid")
    @ApiModelProperty("应用标识")
    private String appid;

    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate" , format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    @ApiModelProperty("模型")
    private String model;





    /**
     * 设置 [用户标识]
     */
    public void setUserid(String userid) {
        this.userid = userid ;
        this.modify("userid",userid);
    }

    /**
     * 设置 [实体名称]
     */
    public void setDynadashboardname(String dynadashboardname) {
        this.dynadashboardname = dynadashboardname ;
        this.modify("dynadashboardname",dynadashboardname);
    }

    /**
     * 设置 [模型标识]
     */
    public void setModelid(String modelid) {
        this.modelid = modelid ;
        this.modify("modelid",modelid);
    }

    /**
     * 设置 [应用标识]
     */
    public void setAppid(String appid) {
        this.appid = appid ;
        this.modify("appid",appid);
    }

    /**
     * 设置 [模型]
     */
    public void setModel(String model) {
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("dynadashboardid");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


