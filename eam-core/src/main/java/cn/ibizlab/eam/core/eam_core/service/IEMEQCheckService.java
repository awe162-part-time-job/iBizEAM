package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQCheck;
import cn.ibizlab.eam.core.eam_core.filter.EMEQCheckSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQCheck] 服务对象接口
 */
public interface IEMEQCheckService extends IService<EMEQCheck> {

    boolean create(EMEQCheck et);
    void createBatch(List<EMEQCheck> list);
    boolean update(EMEQCheck et);
    void updateBatch(List<EMEQCheck> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQCheck get(String key);
    EMEQCheck getDraft(EMEQCheck et);
    boolean checkKey(EMEQCheck et);
    boolean save(EMEQCheck et);
    void saveBatch(List<EMEQCheck> list);
    Page<EMEQCheck> searchCalendar(EMEQCheckSearchContext context);
    Page<EMEQCheck> searchDefault(EMEQCheckSearchContext context);
    List<EMEQCheck> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQCheck> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQCheck> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQCheck> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQCheck> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQCheck> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQCheck> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQCheck> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQCheck> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQCheck> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQCheck> getEmeqcheckByIds(List<String> ids);
    List<EMEQCheck> getEmeqcheckByEntities(List<EMEQCheck> entities);
}


