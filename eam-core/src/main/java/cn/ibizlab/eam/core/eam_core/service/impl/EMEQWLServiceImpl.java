package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQWL;
import cn.ibizlab.eam.core.eam_core.filter.EMEQWLSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQWLService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQWLMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备运行日志] 服务对象接口实现
 */
@Slf4j
@Service("EMEQWLServiceImpl")
public class EMEQWLServiceImpl extends ServiceImpl<EMEQWLMapper, EMEQWL> implements IEMEQWLService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQWL et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqwlid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQWL> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQWL et) {
        fillParentData(et);
        emdprctService.update(emeqwlInheritMapping.toEmdprct(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqwlid", et.getEmeqwlid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqwlid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQWL> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQWL get(String key) {
        EMEQWL et = getById(key);
        if(et == null){
            et = new EMEQWL();
            et.setEmeqwlid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQWL getDraft(EMEQWL et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQWL et) {
        return (!ObjectUtils.isEmpty(et.getEmeqwlid())) && (!Objects.isNull(this.getById(et.getEmeqwlid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQWL et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQWL et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQWL> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQWL> create = new ArrayList<>();
        List<EMEQWL> update = new ArrayList<>();
        for (EMEQWL et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqwlid()) || ObjectUtils.isEmpty(getById(et.getEmeqwlid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQWL> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQWL> create = new ArrayList<>();
        List<EMEQWL> update = new ArrayList<>();
        for (EMEQWL et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqwlid()) || ObjectUtils.isEmpty(getById(et.getEmeqwlid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQWL> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEQWL>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEQWL> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQWL>().eq("objid",emobjectid));
    }

	@Override
    public List<EMEQWL> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }
    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMEQWL>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQWL> searchDefault(EMEQWLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQWL> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQWL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 设备日志-最近30天
     */
    @Override
    public Page<EMEQWL> searchNearest30DayByEQ(EMEQWLSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQWL> pages=baseMapper.searchNearest30DayByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQWL>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQWL et){
        //实体关系[DER1N_EMEQWL_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEQWL_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMEQWL_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQWLInheritMapping emeqwlInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQWL et){
        if(ObjectUtils.isEmpty(et.getEmeqwlid()))
            et.setEmeqwlid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emeqwlInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","EQWL");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQWL> getEmeqwlByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQWL> getEmeqwlByEntities(List<EMEQWL> entities) {
        List ids =new ArrayList();
        for(EMEQWL entity : entities){
            Serializable id=entity.getEmeqwlid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQWLService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



