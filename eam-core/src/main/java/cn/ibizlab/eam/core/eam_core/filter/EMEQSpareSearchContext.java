package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpare;
/**
 * 关系型数据实体[EMEQSpare] 查询条件对象
 */
@Slf4j
@Data
public class EMEQSpareSearchContext extends QueryWrapperContext<EMEQSpare> {

	private String n_emeqsparename_like;//[备件包名称]
	public void setN_emeqsparename_like(String n_emeqsparename_like) {
        this.n_emeqsparename_like = n_emeqsparename_like;
        if(!ObjectUtils.isEmpty(this.n_emeqsparename_like)){
            this.getSearchCond().like("emeqsparename", n_emeqsparename_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_eqsparecode_like;//[备件包代码]
	public void setN_eqsparecode_like(String n_eqsparecode_like) {
        this.n_eqsparecode_like = n_eqsparecode_like;
        if(!ObjectUtils.isEmpty(this.n_eqsparecode_like)){
            this.getSearchCond().like("eqsparecode", n_eqsparecode_like);
        }
    }
	private String n_eqspareinfo_like;//[备件包信息]
	public void setN_eqspareinfo_like(String n_eqspareinfo_like) {
        this.n_eqspareinfo_like = n_eqspareinfo_like;
        if(!ObjectUtils.isEmpty(this.n_eqspareinfo_like)){
            this.getSearchCond().like("eqspareinfo", n_eqspareinfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqsparename", query)
            );
		 }
	}
}



