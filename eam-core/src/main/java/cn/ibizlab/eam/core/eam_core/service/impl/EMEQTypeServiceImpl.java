package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQType;
import cn.ibizlab.eam.core.eam_core.filter.EMEQTypeSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQTypeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[设备类型] 服务对象接口实现
 */
@Slf4j
@Service("EMEQTypeServiceImpl")
public class EMEQTypeServiceImpl extends ServiceImpl<EMEQTypeMapper, EMEQType> implements IEMEQTypeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopMoniService emeqstopmoniService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSTopService emeqstopService;

    protected cn.ibizlab.eam.core.eam_core.service.IEMEQTypeService emeqtypeService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQType et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqtypeid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQType> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQType et) {
        fillParentData(et);
        emobjectService.update(emeqtypeInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqtypeid", et.getEmeqtypeid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqtypeid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQType> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQType get(String key) {
        EMEQType et = getById(key);
        if(et == null){
            et = new EMEQType();
            et.setEmeqtypeid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQType getDraft(EMEQType et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQType et) {
        return (!ObjectUtils.isEmpty(et.getEmeqtypeid())) && (!Objects.isNull(this.getById(et.getEmeqtypeid())));
    }
    @Override
    @Transactional
    public EMEQType genId(EMEQType et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean genIdBatch(List<EMEQType> etList) {
        for(EMEQType et : etList) {
            genId(et);
        }
        return true;
    }

    @Override
    @Transactional
    public boolean save(EMEQType et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQType et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQType> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQType> create = new ArrayList<>();
        List<EMEQType> update = new ArrayList<>();
        for (EMEQType et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqtypeid()) || ObjectUtils.isEmpty(getById(et.getEmeqtypeid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQType> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQType> create = new ArrayList<>();
        List<EMEQType> update = new ArrayList<>();
        for (EMEQType et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqtypeid()) || ObjectUtils.isEmpty(getById(et.getEmeqtypeid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQType> selectByEqtypepid(String emeqtypeid) {
        return baseMapper.selectByEqtypepid(emeqtypeid);
    }
    @Override
    public void removeByEqtypepid(String emeqtypeid) {
        this.remove(new QueryWrapper<EMEQType>().eq("eqtypepid",emeqtypeid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQType> searchDefault(EMEQTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQType> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 EQTypeTree
     */
    @Override
    public Page<EMEQType> searchEQTypeTree(EMEQTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQType> pages=baseMapper.searchEQTypeTree(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 上级设备类型
     */
    @Override
    public Page<EMEQType> searchPeqType(EMEQTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQType> pages=baseMapper.searchPeqType(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQType et){
        //实体关系[DER1N_EMEQTYPE_EMEQTYPE_EQTYPEPID]
        if(!ObjectUtils.isEmpty(et.getEqtypepid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQType eqtypep=et.getEqtypep();
            if(ObjectUtils.isEmpty(eqtypep)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQType majorEntity=emeqtypeService.get(et.getEqtypepid());
                et.setEqtypep(majorEntity);
                eqtypep=majorEntity;
            }
            et.setEqtypepcode(eqtypep.getEqtypecode());
            et.setEqtypepname(eqtypep.getEqtypeinfo());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQTypeInheritMapping emeqtypeInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQType et){
        if(ObjectUtils.isEmpty(et.getEmeqtypeid()))
            et.setEmeqtypeid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emeqtypeInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","EQTYPE");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQType> getEmeqtypeByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQType> getEmeqtypeByEntities(List<EMEQType> entities) {
        List ids =new ArrayList();
        for(EMEQType entity : entities){
            Serializable id=entity.getEmeqtypeid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQTypeService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



