package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[资产历史记录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMASSETHIST_BASE", resultMap = "EMAssetHistResultMap")
@ApiModel("资产历史记录")
public class EMAssetHist extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资产历史记录标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emassethistid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emassethistid")
    @JsonProperty("emassethistid")
    @ApiModelProperty("资产历史记录标识")
    private String emassethistid;
    /**
     * 责任人
     */
    @TableField(value = "empid")
    @JSONField(name = "empid")
    @JsonProperty("empid")
    @ApiModelProperty("责任人")
    private String empid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 责任人
     */
    @TableField(value = "empname")
    @JSONField(name = "empname")
    @JsonProperty("empname")
    @ApiModelProperty("责任人")
    private String empname;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 时间
     */
    @TableField(value = "activedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "activedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("activedate")
    @ApiModelProperty("时间")
    private Timestamp activedate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 记录内容
     */
    @TableField(value = "emassethistname")
    @JSONField(name = "emassethistname")
    @JsonProperty("emassethistname")
    @ApiModelProperty("记录内容")
    private String emassethistname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 资产
     */
    @TableField(exist = false)
    @JSONField(name = "assetname")
    @JsonProperty("assetname")
    @ApiModelProperty("资产")
    private String assetname;
    /**
     * 资产
     */
    @TableField(value = "assetid")
    @JSONField(name = "assetid")
    @JsonProperty("assetid")
    @ApiModelProperty("资产")
    private String assetid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMAsset asset;



    /**
     * 设置 [责任人]
     */
    public void setEmpid(String empid) {
        this.empid = empid;
        this.modify("empid", empid);
    }

    /**
     * 设置 [责任人]
     */
    public void setEmpname(String empname) {
        this.empname = empname;
        this.modify("empname", empname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [时间]
     */
    public void setActivedate(Timestamp activedate) {
        this.activedate = activedate;
        this.modify("activedate", activedate);
    }

    /**
     * 格式化日期 [时间]
     */
    public String formatActivedate() {
        if (this.activedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(activedate);
    }
    /**
     * 设置 [记录内容]
     */
    public void setEmassethistname(String emassethistname) {
        this.emassethistname = emassethistname;
        this.modify("emassethistname", emassethistname);
    }

    /**
     * 设置 [资产]
     */
    public void setAssetid(String assetid) {
        this.assetid = assetid;
        this.modify("assetid", assetid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emassethistid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


