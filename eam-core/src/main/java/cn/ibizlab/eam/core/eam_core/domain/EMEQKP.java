package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备关键点]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQKP_BASE", resultMap = "EMEQKPResultMap")
@ApiModel("设备关键点")
public class EMEQKP extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备关键点标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqkpid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqkpid")
    @JsonProperty("emeqkpid")
    @ApiModelProperty("设备关键点标识")
    private String emeqkpid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 关键点范围
     */
    @TableField(value = "kpscope")
    @JSONField(name = "kpscope")
    @JsonProperty("kpscope")
    @ApiModelProperty("关键点范围")
    private String kpscope;
    /**
     * 关键点备注
     */
    @TableField(value = "kpdesc")
    @JSONField(name = "kpdesc")
    @JsonProperty("kpdesc")
    @ApiModelProperty("关键点备注")
    private String kpdesc;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 正常参考值
     */
    @TableField(value = "normalrefval")
    @JSONField(name = "normalrefval")
    @JsonProperty("normalrefval")
    @ApiModelProperty("正常参考值")
    private String normalrefval;
    /**
     * 关键点信息
     */
    @TableField(exist = false)
    @JSONField(name = "kpinfo")
    @JsonProperty("kpinfo")
    @ApiModelProperty("关键点信息")
    private String kpinfo;
    /**
     * 关键点类型
     */
    @TableField(value = "kptypeid")
    @JSONField(name = "kptypeid")
    @JsonProperty("kptypeid")
    @ApiModelProperty("关键点类型")
    private String kptypeid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 关键点代码
     */
    @TableField(value = "kpcode")
    @JSONField(name = "kpcode")
    @JsonProperty("kpcode")
    @ApiModelProperty("关键点代码")
    private String kpcode;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 关键点名称
     */
    @TableField(value = "emeqkpname")
    @JSONField(name = "emeqkpname")
    @JsonProperty("emeqkpname")
    @ApiModelProperty("关键点名称")
    private String emeqkpname;



    /**
     * 设置 [关键点范围]
     */
    public void setKpscope(String kpscope) {
        this.kpscope = kpscope;
        this.modify("kpscope", kpscope);
    }

    /**
     * 设置 [关键点备注]
     */
    public void setKpdesc(String kpdesc) {
        this.kpdesc = kpdesc;
        this.modify("kpdesc", kpdesc);
    }

    /**
     * 设置 [正常参考值]
     */
    public void setNormalrefval(String normalrefval) {
        this.normalrefval = normalrefval;
        this.modify("normalrefval", normalrefval);
    }

    /**
     * 设置 [关键点类型]
     */
    public void setKptypeid(String kptypeid) {
        this.kptypeid = kptypeid;
        this.modify("kptypeid", kptypeid);
    }

    /**
     * 设置 [关键点代码]
     */
    public void setKpcode(String kpcode) {
        this.kpcode = kpcode;
        this.modify("kpcode", kpcode);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [关键点名称]
     */
    public void setEmeqkpname(String emeqkpname) {
        this.emeqkpname = emeqkpname;
        this.modify("emeqkpname", emeqkpname);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqkpid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


