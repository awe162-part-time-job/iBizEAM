package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMPurchangeHis;
/**
 * 关系型数据实体[EMPurchangeHis] 查询条件对象
 */
@Slf4j
@Data
public class EMPurchangeHisSearchContext extends QueryWrapperContext<EMPurchangeHis> {

	private String n_empurchangehisname_like;//[领料单换料记录名称]
	public void setN_empurchangehisname_like(String n_empurchangehisname_like) {
        this.n_empurchangehisname_like = n_empurchangehisname_like;
        if(!ObjectUtils.isEmpty(this.n_empurchangehisname_like)){
            this.getSearchCond().like("empurchangehisname", n_empurchangehisname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_pfdeptname_eq;//[部门]
	public void setN_pfdeptname_eq(String n_pfdeptname_eq) {
        this.n_pfdeptname_eq = n_pfdeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfdeptname_eq)){
            this.getSearchCond().eq("pfdeptname", n_pfdeptname_eq);
        }
    }
	private String n_pfdeptname_like;//[部门]
	public void setN_pfdeptname_like(String n_pfdeptname_like) {
        this.n_pfdeptname_like = n_pfdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_pfdeptname_like)){
            this.getSearchCond().like("pfdeptname", n_pfdeptname_like);
        }
    }
	private String n_pfempname_eq;//[换料人]
	public void setN_pfempname_eq(String n_pfempname_eq) {
        this.n_pfempname_eq = n_pfempname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempname_eq)){
            this.getSearchCond().eq("pfempname", n_pfempname_eq);
        }
    }
	private String n_pfempname_like;//[换料人]
	public void setN_pfempname_like(String n_pfempname_like) {
        this.n_pfempname_like = n_pfempname_like;
        if(!ObjectUtils.isEmpty(this.n_pfempname_like)){
            this.getSearchCond().like("pfempname", n_pfempname_like);
        }
    }
	private String n_pfdeptid_eq;//[部门]
	public void setN_pfdeptid_eq(String n_pfdeptid_eq) {
        this.n_pfdeptid_eq = n_pfdeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfdeptid_eq)){
            this.getSearchCond().eq("pfdeptid", n_pfdeptid_eq);
        }
    }
	private String n_pfempid_eq;//[换料人]
	public void setN_pfempid_eq(String n_pfempid_eq) {
        this.n_pfempid_eq = n_pfempid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempid_eq)){
            this.getSearchCond().eq("pfempid", n_pfempid_eq);
        }
    }
	private String n_emitempusename_eq;//[领料单]
	public void setN_emitempusename_eq(String n_emitempusename_eq) {
        this.n_emitempusename_eq = n_emitempusename_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_eq)){
            this.getSearchCond().eq("emitempusename", n_emitempusename_eq);
        }
    }
	private String n_emitempusename_like;//[领料单]
	public void setN_emitempusename_like(String n_emitempusename_like) {
        this.n_emitempusename_like = n_emitempusename_like;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_like)){
            this.getSearchCond().like("emitempusename", n_emitempusename_like);
        }
    }
	private String n_emitempuseid_eq;//[领料单]
	public void setN_emitempuseid_eq(String n_emitempuseid_eq) {
        this.n_emitempuseid_eq = n_emitempuseid_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempuseid_eq)){
            this.getSearchCond().eq("emitempuseid", n_emitempuseid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("empurchangehisname", query)
            );
		 }
	}
}



