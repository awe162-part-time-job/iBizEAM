package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFTeam;
import cn.ibizlab.eam.core.eam_pf.filter.PFTeamSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFTeam] 服务对象接口
 */
public interface IPFTeamService extends IService<PFTeam> {

    boolean create(PFTeam et);
    void createBatch(List<PFTeam> list);
    boolean update(PFTeam et);
    void updateBatch(List<PFTeam> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFTeam get(String key);
    PFTeam getDraft(PFTeam et);
    boolean checkKey(PFTeam et);
    boolean save(PFTeam et);
    void saveBatch(List<PFTeam> list);
    Page<PFTeam> searchDefault(PFTeamSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFTeam> getPfteamByIds(List<String> ids);
    List<PFTeam> getPfteamByEntities(List<PFTeam> entities);
}


