package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQSetup;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSetupSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQSetup] 服务对象接口
 */
public interface IEMEQSetupService extends IService<EMEQSetup> {

    boolean create(EMEQSetup et);
    void createBatch(List<EMEQSetup> list);
    boolean update(EMEQSetup et);
    void updateBatch(List<EMEQSetup> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQSetup get(String key);
    EMEQSetup getDraft(EMEQSetup et);
    boolean checkKey(EMEQSetup et);
    boolean save(EMEQSetup et);
    void saveBatch(List<EMEQSetup> list);
    Page<EMEQSetup> searchCalendar(EMEQSetupSearchContext context);
    Page<EMEQSetup> searchDefault(EMEQSetupSearchContext context);
    List<EMEQSetup> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQSetup> selectByEitiresid(String emeitiresid);
    void removeByEitiresid(String emeitiresid);
    List<EMEQSetup> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQSetup> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQSetup> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQSetup> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQSetup> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQSetup> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQSetup> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQSetup> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQSetup> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQSetup> getEmeqsetupByIds(List<String> ids);
    List<EMEQSetup> getEmeqsetupByEntities(List<EMEQSetup> entities);
}


