package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQTYPE_BASE", resultMap = "EMEQTypeResultMap")
@ApiModel("设备类型")
public class EMEQType extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类型分组
     */
    @TableField(value = "eqtypegroup")
    @JSONField(name = "eqtypegroup")
    @JsonProperty("eqtypegroup")
    @ApiModelProperty("类型分组")
    private String eqtypegroup;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 设备状态情况
     */
    @TableField(exist = false)
    @JSONField(name = "eqstateinfo")
    @JsonProperty("eqstateinfo")
    @ApiModelProperty("设备状态情况")
    private String eqstateinfo;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 类型代码
     */
    @TableField(value = "eqtypecode")
    @JSONField(name = "eqtypecode")
    @JsonProperty("eqtypecode")
    @ApiModelProperty("类型代码")
    private String eqtypecode;
    /**
     * 统计归口分组
     */
    @TableField(value = "stype")
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @ApiModelProperty("统计归口分组")
    private String stype;
    /**
     * 设备类型信息
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypeinfo")
    @JsonProperty("eqtypeinfo")
    @ApiModelProperty("设备类型信息")
    private String eqtypeinfo;
    /**
     * 辅助设施故障/维护
     */
    @TableField(value = "arg")
    @JSONField(name = "arg")
    @JsonProperty("arg")
    @ApiModelProperty("辅助设施故障/维护")
    private String arg;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 设备类型标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqtypeid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    @ApiModelProperty("设备类型标识")
    private String emeqtypeid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 统计归口名称
     */
    @TableField(value = "sname")
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @ApiModelProperty("统计归口名称")
    private String sname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 设备类型名称
     */
    @TableField(value = "emeqtypename")
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    @ApiModelProperty("设备类型名称")
    private String emeqtypename;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 上级设备类型代码
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypepcode")
    @JsonProperty("eqtypepcode")
    @ApiModelProperty("上级设备类型代码")
    private String eqtypepcode;
    /**
     * 上级设备类型
     */
    @TableField(exist = false)
    @JSONField(name = "eqtypepname")
    @JsonProperty("eqtypepname")
    @ApiModelProperty("上级设备类型")
    private String eqtypepname;
    /**
     * 上级设备类型
     */
    @TableField(value = "eqtypepid")
    @JSONField(name = "eqtypepid")
    @JsonProperty("eqtypepid")
    @ApiModelProperty("上级设备类型")
    private String eqtypepid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQType eqtypep;



    /**
     * 设置 [类型分组]
     */
    public void setEqtypegroup(String eqtypegroup) {
        this.eqtypegroup = eqtypegroup;
        this.modify("eqtypegroup", eqtypegroup);
    }

    /**
     * 设置 [类型代码]
     */
    public void setEqtypecode(String eqtypecode) {
        this.eqtypecode = eqtypecode;
        this.modify("eqtypecode", eqtypecode);
    }

    /**
     * 设置 [统计归口分组]
     */
    public void setStype(String stype) {
        this.stype = stype;
        this.modify("stype", stype);
    }

    /**
     * 设置 [辅助设施故障/维护]
     */
    public void setArg(String arg) {
        this.arg = arg;
        this.modify("arg", arg);
    }

    /**
     * 设置 [统计归口名称]
     */
    public void setSname(String sname) {
        this.sname = sname;
        this.modify("sname", sname);
    }

    /**
     * 设置 [设备类型名称]
     */
    public void setEmeqtypename(String emeqtypename) {
        this.emeqtypename = emeqtypename;
        this.modify("emeqtypename", emeqtypename);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [上级设备类型]
     */
    public void setEqtypepid(String eqtypepid) {
        this.eqtypepid = eqtypepid;
        this.modify("eqtypepid", eqtypepid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqtypeid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


