

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMaintance;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQMaintanceInheritMapping {

    @Mappings({
        @Mapping(source ="emeqmaintanceid",target = "emeqahid"),
        @Mapping(source ="emeqmaintancename",target = "emeqahname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQAH toEmeqah(EMEQMaintance minorEntity);

    @Mappings({
        @Mapping(source ="emeqahid" ,target = "emeqmaintanceid"),
        @Mapping(source ="emeqahname" ,target = "emeqmaintancename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQMaintance toEmeqmaintance(EMEQAH majorEntity);

    List<EMEQAH> toEmeqah(List<EMEQMaintance> minorEntities);

    List<EMEQMaintance> toEmeqmaintance(List<EMEQAH> majorEntities);

}


