package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[产能]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMOUTPUTRCT_BASE", resultMap = "EMOutputRctResultMap")
@ApiModel("产能")
public class EMOutputRct extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 产能区间起始
     */
    @TableField(value = "edate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "edate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    @ApiModelProperty("产能区间起始")
    private Timestamp edate;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 产能区间截至
     */
    @TableField(value = "bdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "bdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    @ApiModelProperty("产能区间截至")
    private Timestamp bdate;
    /**
     * 产能名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emoutputrctname")
    @JSONField(name = "emoutputrctname")
    @JsonProperty("emoutputrctname")
    @ApiModelProperty("产能名称")
    private String emoutputrctname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 产能值
     */
    @TableField(value = "nval")
    @JSONField(name = "nval")
    @JsonProperty("nval")
    @ApiModelProperty("产能值")
    private Double nval;
    /**
     * 产能标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emoutputrctid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emoutputrctid")
    @JsonProperty("emoutputrctid")
    @ApiModelProperty("产能标识")
    private String emoutputrctid;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @ApiModelProperty("设备")
    private String equipname;
    /**
     * 能力
     */
    @TableField(exist = false)
    @JSONField(name = "outputname")
    @JsonProperty("outputname")
    @ApiModelProperty("能力")
    private String outputname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @ApiModelProperty("位置")
    private String objname;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @ApiModelProperty("工单")
    private String woname;
    /**
     * 工单
     */
    @TableField(value = "woid")
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @ApiModelProperty("工单")
    private String woid;
    /**
     * 能力
     */
    @TableField(value = "outputid")
    @JSONField(name = "outputid")
    @JsonProperty("outputid")
    @ApiModelProperty("能力")
    private String outputid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @ApiModelProperty("设备")
    private String equipid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @ApiModelProperty("位置")
    private String objid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMOutput output;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wo;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [产能区间起始]
     */
    public void setEdate(Timestamp edate) {
        this.edate = edate;
        this.modify("edate", edate);
    }

    /**
     * 格式化日期 [产能区间起始]
     */
    public String formatEdate() {
        if (this.edate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(edate);
    }
    /**
     * 设置 [产能区间截至]
     */
    public void setBdate(Timestamp bdate) {
        this.bdate = bdate;
        this.modify("bdate", bdate);
    }

    /**
     * 格式化日期 [产能区间截至]
     */
    public String formatBdate() {
        if (this.bdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(bdate);
    }
    /**
     * 设置 [产能名称]
     */
    public void setEmoutputrctname(String emoutputrctname) {
        this.emoutputrctname = emoutputrctname;
        this.modify("emoutputrctname", emoutputrctname);
    }

    /**
     * 设置 [产能值]
     */
    public void setNval(Double nval) {
        this.nval = nval;
        this.modify("nval", nval);
    }

    /**
     * 设置 [工单]
     */
    public void setWoid(String woid) {
        this.woid = woid;
        this.modify("woid", woid);
    }

    /**
     * 设置 [能力]
     */
    public void setOutputid(String outputid) {
        this.outputid = outputid;
        this.modify("outputid", outputid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emoutputrctid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


