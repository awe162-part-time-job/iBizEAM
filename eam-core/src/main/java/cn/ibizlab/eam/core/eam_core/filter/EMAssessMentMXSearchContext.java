package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMentMX;
/**
 * 关系型数据实体[EMAssessMentMX] 查询条件对象
 */
@Slf4j
@Data
public class EMAssessMentMXSearchContext extends QueryWrapperContext<EMAssessMentMX> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_pfempid_eq;//[主要责任人]
	public void setN_pfempid_eq(String n_pfempid_eq) {
        this.n_pfempid_eq = n_pfempid_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempid_eq)){
            this.getSearchCond().eq("pfempid", n_pfempid_eq);
        }
    }
	private String n_emassessmentmxname_like;//[计划内容]
	public void setN_emassessmentmxname_like(String n_emassessmentmxname_like) {
        this.n_emassessmentmxname_like = n_emassessmentmxname_like;
        if(!ObjectUtils.isEmpty(this.n_emassessmentmxname_like)){
            this.getSearchCond().like("emassessmentmxname", n_emassessmentmxname_like);
        }
    }
	private String n_pfempname_eq;//[主要责任人]
	public void setN_pfempname_eq(String n_pfempname_eq) {
        this.n_pfempname_eq = n_pfempname_eq;
        if(!ObjectUtils.isEmpty(this.n_pfempname_eq)){
            this.getSearchCond().eq("pfempname", n_pfempname_eq);
        }
    }
	private String n_pfempname_like;//[主要责任人]
	public void setN_pfempname_like(String n_pfempname_like) {
        this.n_pfempname_like = n_pfempname_like;
        if(!ObjectUtils.isEmpty(this.n_pfempname_like)){
            this.getSearchCond().like("pfempname", n_pfempname_like);
        }
    }
	private String n_emassessmentname_eq;//[计划及项目进程考核]
	public void setN_emassessmentname_eq(String n_emassessmentname_eq) {
        this.n_emassessmentname_eq = n_emassessmentname_eq;
        if(!ObjectUtils.isEmpty(this.n_emassessmentname_eq)){
            this.getSearchCond().eq("emassessmentname", n_emassessmentname_eq);
        }
    }
	private String n_emassessmentname_like;//[计划及项目进程考核]
	public void setN_emassessmentname_like(String n_emassessmentname_like) {
        this.n_emassessmentname_like = n_emassessmentname_like;
        if(!ObjectUtils.isEmpty(this.n_emassessmentname_like)){
            this.getSearchCond().like("emassessmentname", n_emassessmentname_like);
        }
    }
	private String n_emassessmentid_eq;//[计划及项目进程考核]
	public void setN_emassessmentid_eq(String n_emassessmentid_eq) {
        this.n_emassessmentid_eq = n_emassessmentid_eq;
        if(!ObjectUtils.isEmpty(this.n_emassessmentid_eq)){
            this.getSearchCond().eq("emassessmentid", n_emassessmentid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassessmentmxname", query)
            );
		 }
	}
}



