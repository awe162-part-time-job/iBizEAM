

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemROutInheritMapping {

    @Mappings({
        @Mapping(source ="emitemroutid",target = "emitemtradeid"),
        @Mapping(source ="emitemroutname",target = "emitemtradename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="shf",target = "shf"),
    })
    EMItemTrade toEmitemtrade(EMItemROut minorEntity);

    @Mappings({
        @Mapping(source ="emitemtradeid" ,target = "emitemroutid"),
        @Mapping(source ="emitemtradename" ,target = "emitemroutname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMItemROut toEmitemrout(EMItemTrade majorEntity);

    List<EMItemTrade> toEmitemtrade(List<EMItemROut> minorEntities);

    List<EMItemROut> toEmitemrout(List<EMItemTrade> majorEntities);

}


