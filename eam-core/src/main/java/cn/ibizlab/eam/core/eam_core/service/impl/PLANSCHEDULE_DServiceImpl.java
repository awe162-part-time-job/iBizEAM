package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_D;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_DSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_DService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.PLANSCHEDULE_DMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划_按天] 服务对象接口实现
 */
@Slf4j
@Service("PLANSCHEDULE_DServiceImpl")
public class PLANSCHEDULE_DServiceImpl extends ServiceImpl<PLANSCHEDULE_DMapper, PLANSCHEDULE_D> implements IPLANSCHEDULE_DService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PLANSCHEDULE_D et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleDid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PLANSCHEDULE_D> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PLANSCHEDULE_D et) {
        planscheduleService.update(planschedule_dInheritMapping.toPlanschedule(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("planschedule_did", et.getPlanscheduleDid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleDid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PLANSCHEDULE_D> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        planscheduleService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PLANSCHEDULE_D get(String key) {
        PLANSCHEDULE_D et = getById(key);
        if(et == null){
            et = new PLANSCHEDULE_D();
            et.setPlanscheduleDid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PLANSCHEDULE_D getDraft(PLANSCHEDULE_D et) {
        return et;
    }

    @Override
    public boolean checkKey(PLANSCHEDULE_D et) {
        return (!ObjectUtils.isEmpty(et.getPlanscheduleDid())) && (!Objects.isNull(this.getById(et.getPlanscheduleDid())));
    }
    @Override
    @Transactional
    public boolean save(PLANSCHEDULE_D et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PLANSCHEDULE_D et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PLANSCHEDULE_D> list) {
        List<PLANSCHEDULE_D> create = new ArrayList<>();
        List<PLANSCHEDULE_D> update = new ArrayList<>();
        for (PLANSCHEDULE_D et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleDid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleDid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PLANSCHEDULE_D> list) {
        List<PLANSCHEDULE_D> create = new ArrayList<>();
        List<PLANSCHEDULE_D> update = new ArrayList<>();
        for (PLANSCHEDULE_D et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleDid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleDid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 数据集
     */
    @Override
    public Page<PLANSCHEDULE_D> searchDefault(PLANSCHEDULE_DSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PLANSCHEDULE_D> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PLANSCHEDULE_D>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.PLANSCHEDULE_DInheritMapping planschedule_dInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService planscheduleService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(PLANSCHEDULE_D et){
        if(ObjectUtils.isEmpty(et.getPlanscheduleDid()))
            et.setPlanscheduleDid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE planschedule =planschedule_dInheritMapping.toPlanschedule(et);
        planschedule.set("scheduletype","5");
        planscheduleService.create(planschedule);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PLANSCHEDULE_D> getPlanscheduleDByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PLANSCHEDULE_D> getPlanscheduleDByEntities(List<PLANSCHEDULE_D> entities) {
        List ids =new ArrayList();
        for(PLANSCHEDULE_D entity : entities){
            Serializable id=entity.getPlanscheduleDid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPLANSCHEDULE_DService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



