package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMProduct;
import cn.ibizlab.eam.core.eam_core.filter.EMProductSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMProductService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMProductMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[试用品] 服务对象接口实现
 */
@Slf4j
@Service("EMProductServiceImpl")
public class EMProductServiceImpl extends ServiceImpl<EMProductMapper, EMProduct> implements IEMProductService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMProduct et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmproductid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMProduct> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMProduct et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emproductid", et.getEmproductid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmproductid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMProduct> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMProduct get(String key) {
        EMProduct et = getById(key);
        if(et == null){
            et = new EMProduct();
            et.setEmproductid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMProduct getDraft(EMProduct et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMProduct et) {
        return (!ObjectUtils.isEmpty(et.getEmproductid())) && (!Objects.isNull(this.getById(et.getEmproductid())));
    }
    @Override
    @Transactional
    public boolean save(EMProduct et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMProduct et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMProduct> list) {
        list.forEach(item->fillParentData(item));
        List<EMProduct> create = new ArrayList<>();
        List<EMProduct> update = new ArrayList<>();
        for (EMProduct et : list) {
            if (ObjectUtils.isEmpty(et.getEmproductid()) || ObjectUtils.isEmpty(getById(et.getEmproductid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMProduct> list) {
        list.forEach(item->fillParentData(item));
        List<EMProduct> create = new ArrayList<>();
        List<EMProduct> update = new ArrayList<>();
        for (EMProduct et : list) {
            if (ObjectUtils.isEmpty(et.getEmproductid()) || ObjectUtils.isEmpty(getById(et.getEmproductid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMProduct> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMProduct>().eq("equipid",emequipid));
    }

	@Override
    public List<EMProduct> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMProduct>().eq("itemid",emitemid));
    }

	@Override
    public List<EMProduct> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMProduct>().eq("objid",emobjectid));
    }

	@Override
    public List<EMProduct> selectByServiceid(String emserviceid) {
        return baseMapper.selectByServiceid(emserviceid);
    }
    @Override
    public void removeByServiceid(String emserviceid) {
        this.remove(new QueryWrapper<EMProduct>().eq("serviceid",emserviceid));
    }

	@Override
    public List<EMProduct> selectByTeamid(String pfteamid) {
        return baseMapper.selectByTeamid(pfteamid);
    }
    @Override
    public void removeByTeamid(String pfteamid) {
        this.remove(new QueryWrapper<EMProduct>().eq("teamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMProduct> searchDefault(EMProductSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMProduct> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMProduct>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMProduct et){
        //实体关系[DER1N_EMPRODUCT_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMPRODUCT_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
        }
        //实体关系[DER1N_EMPRODUCT_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMPRODUCT_EMSERVICE_SERVICEID]
        if(!ObjectUtils.isEmpty(et.getServiceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService service=et.getService();
            if(ObjectUtils.isEmpty(service)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getServiceid());
                et.setService(majorEntity);
                service=majorEntity;
            }
            et.setServicename(service.getEmservicename());
        }
        //实体关系[DER1N_EMPRODUCT_PFTEAM_TEAMID]
        if(!ObjectUtils.isEmpty(et.getTeamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam team=et.getTeam();
            if(ObjectUtils.isEmpty(team)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getTeamid());
                et.setTeam(majorEntity);
                team=majorEntity;
            }
            et.setTeamname(team.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMProduct> getEmproductByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMProduct> getEmproductByEntities(List<EMProduct> entities) {
        List ids =new ArrayList();
        for(EMProduct entity : entities){
            Serializable id=entity.getEmproductid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMProductService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



