package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPO;
import cn.ibizlab.eam.core.eam_core.filter.EMPOSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPOService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPOMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[订单] 服务对象接口实现
 */
@Slf4j
@Service("EMPOServiceImpl")
public class EMPOServiceImpl extends ServiceImpl<EMPOMapper, EMPO> implements IEMPOService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPODetailService empodetailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPO et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpoid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPO et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("empoid", et.getEmpoid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmpoid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPO get(String key) {
        EMPO et = getById(key);
        if(et == null){
            et = new EMPO();
            et.setEmpoid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPO getDraft(EMPO et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public EMPO arrival(EMPO et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean arrivalBatch(List<EMPO> etList) {
        for(EMPO et : etList) {
            arrival(et);
        }
        return true;
    }

    @Override
    public boolean checkKey(EMPO et) {
        return (!ObjectUtils.isEmpty(et.getEmpoid())) && (!Objects.isNull(this.getById(et.getEmpoid())));
    }
    @Override
    @Transactional
    public EMPO placeOrder(EMPO et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMPO et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPO et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPO> list) {
        list.forEach(item->fillParentData(item));
        List<EMPO> create = new ArrayList<>();
        List<EMPO> update = new ArrayList<>();
        for (EMPO et : list) {
            if (ObjectUtils.isEmpty(et.getEmpoid()) || ObjectUtils.isEmpty(getById(et.getEmpoid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPO> list) {
        list.forEach(item->fillParentData(item));
        List<EMPO> create = new ArrayList<>();
        List<EMPO> update = new ArrayList<>();
        for (EMPO et : list) {
            if (ObjectUtils.isEmpty(et.getEmpoid()) || ObjectUtils.isEmpty(getById(et.getEmpoid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPO> selectByLabserviceid(String emserviceid) {
        return baseMapper.selectByLabserviceid(emserviceid);
    }
    @Override
    public void removeByLabserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMPO>().eq("labserviceid",emserviceid));
    }

	@Override
    public List<EMPO> selectByApprempid(String pfempid) {
        return baseMapper.selectByApprempid(pfempid);
    }
    @Override
    public void removeByApprempid(String pfempid) {
        this.remove(new QueryWrapper<EMPO>().eq("apprempid",pfempid));
    }

	@Override
    public List<EMPO> selectByFgempid(String pfempid) {
        return baseMapper.selectByFgempid(pfempid);
    }
    @Override
    public void removeByFgempid(String pfempid) {
        this.remove(new QueryWrapper<EMPO>().eq("fgempid",pfempid));
    }

	@Override
    public List<EMPO> selectByRempid(String pfempid) {
        return baseMapper.selectByRempid(pfempid);
    }
    @Override
    public void removeByRempid(String pfempid) {
        this.remove(new QueryWrapper<EMPO>().eq("rempid",pfempid));
    }

	@Override
    public List<EMPO> selectByZjlempid(String pfempid) {
        return baseMapper.selectByZjlempid(pfempid);
    }
    @Override
    public void removeByZjlempid(String pfempid) {
        this.remove(new QueryWrapper<EMPO>().eq("zjlempid",pfempid));
    }


    /**
     * 查询集合 已关闭订单
     */
    @Override
    public Page<EMPO> searchClosedOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchClosedOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPO> searchDefault(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 在途
     */
    @Override
    public Page<EMPO> searchOnOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchOnOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 等待下单
     */
    @Override
    public Page<EMPO> searchPlaceOrder(EMPOSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPO> pages=baseMapper.searchPlaceOrder(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPO>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPO et){
        //实体关系[DER1N_EMPO_EMSERVICE_LABSERVICEID]
        if(!ObjectUtils.isEmpty(et.getLabserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService labservice=et.getLabservice();
            if(ObjectUtils.isEmpty(labservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getLabserviceid());
                et.setLabservice(majorEntity);
                labservice=majorEntity;
            }
            et.setLabservicetypeid(labservice.getLabservicetypeid());
            et.setLabservicename(labservice.getEmservicename());
        }
        //实体关系[DER1N_EMPO_PFEMP_APPREMPID]
        if(!ObjectUtils.isEmpty(et.getApprempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfeapprempid=et.getPfeapprempid();
            if(ObjectUtils.isEmpty(pfeapprempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getApprempid());
                et.setPfeapprempid(majorEntity);
                pfeapprempid=majorEntity;
            }
            et.setApprempname(pfeapprempid.getEmpinfo());
        }
        //实体关系[DER1N_EMPO_PFEMP_FGEMPID]
        if(!ObjectUtils.isEmpty(et.getFgempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfefgempid=et.getPfefgempid();
            if(ObjectUtils.isEmpty(pfefgempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getFgempid());
                et.setPfefgempid(majorEntity);
                pfefgempid=majorEntity;
            }
            et.setFgempname(pfefgempid.getEmpinfo());
        }
        //实体关系[DER1N_EMPO_PFEMP_REMPID]
        if(!ObjectUtils.isEmpty(et.getRempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfemprempid=et.getPfemprempid();
            if(ObjectUtils.isEmpty(pfemprempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getRempid());
                et.setPfemprempid(majorEntity);
                pfemprempid=majorEntity;
            }
            et.setRempname(pfemprempid.getEmpinfo());
        }
        //实体关系[DER1N_EMPO_PFEMP_ZJLEMPID]
        if(!ObjectUtils.isEmpty(et.getZjlempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfezjlempid=et.getPfezjlempid();
            if(ObjectUtils.isEmpty(pfezjlempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getZjlempid());
                et.setPfezjlempid(majorEntity);
                pfezjlempid=majorEntity;
            }
            et.setZjlempname(pfezjlempid.getEmpinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPO> getEmpoByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPO> getEmpoByEntities(List<EMPO> entities) {
        List ids =new ArrayList();
        for(EMPO entity : entities){
            Serializable id=entity.getEmpoid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPOService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



