package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划修理]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPURPLAN_BASE", resultMap = "EMPurPlanResultMap")
@ApiModel("计划修理")
public class EMPurPlan extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 采购起始时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "yearfrom", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("yearfrom")
    @ApiModelProperty("采购起始时间")
    private Timestamp yearfrom;
    /**
     * 评估报告
     */
    @TableField(value = "assessreport")
    @JSONField(name = "assessreport")
    @JsonProperty("assessreport")
    @ApiModelProperty("评估报告")
    private String assessreport;
    /**
     * 采购计划项目
     */
    @TableField(value = "empurplanname")
    @JSONField(name = "empurplanname")
    @JsonProperty("empurplanname")
    @ApiModelProperty("采购计划项目")
    private String empurplanname;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;
    /**
     * 采购年度
     */
    @TableField(value = "years")
    @JSONField(name = "years")
    @JsonProperty("years")
    @ApiModelProperty("采购年度")
    private String years;
    /**
     * 物品类型
     */
    @TableField(value = "msitemtype")
    @JSONField(name = "msitemtype")
    @JsonProperty("msitemtype")
    @ApiModelProperty("物品类型")
    private String msitemtype;
    /**
     * 采购计划标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "empurplanid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "empurplanid")
    @JsonProperty("empurplanid")
    @ApiModelProperty("采购计划标识")
    private String empurplanid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 计划修理状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "planstate")
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    @ApiModelProperty("计划修理状态")
    private Integer planstate;
    /**
     * 验收时间
     */
    @TableField(value = "acceptancedate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "acceptancedate", format = "yyyy-MM-dd")
    @JsonProperty("acceptancedate")
    @ApiModelProperty("验收时间")
    private Timestamp acceptancedate;
    /**
     * 数量完成比
     */
    @TableField(exist = false)
    @JSONField(name = "cocnt")
    @JsonProperty("cocnt")
    @ApiModelProperty("数量完成比")
    private Double cocnt;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 采购结束时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "yearto", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("yearto")
    @ApiModelProperty("采购结束时间")
    private Timestamp yearto;
    /**
     * 剩余金额额度
     */
    @TableField(exist = false)
    @JSONField(name = "nowamount")
    @JsonProperty("nowamount")
    @ApiModelProperty("剩余金额额度")
    private Double nowamount;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 服务商编号
     */
    @TableField(value = "servicecode")
    @JSONField(name = "servicecode")
    @JsonProperty("servicecode")
    @ApiModelProperty("服务商编号")
    private String servicecode;
    /**
     * 验收结果
     */
    @TableField(value = "acceptanceresult")
    @JSONField(name = "acceptanceresult")
    @JsonProperty("acceptanceresult")
    @ApiModelProperty("验收结果")
    private String acceptanceresult;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;
    /**
     * 采购金额
     */
    @TableField(value = "puramount")
    @JSONField(name = "puramount")
    @JsonProperty("puramount")
    @ApiModelProperty("采购金额")
    private String puramount;
    /**
     * 跟踪规则
     */
    @TableField(value = "trackrule")
    @JSONField(name = "trackrule")
    @JsonProperty("trackrule")
    @ApiModelProperty("跟踪规则")
    private String trackrule;
    /**
     * 经理指定询价数
     */
    @DEField(defaultValue = "0")
    @TableField(value = "m3q")
    @JSONField(name = "m3q")
    @JsonProperty("m3q")
    @ApiModelProperty("经理指定询价数")
    private Integer m3q;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 剩余数量额度
     */
    @TableField(exist = false)
    @JSONField(name = "nowcnt")
    @JsonProperty("nowcnt")
    @ApiModelProperty("剩余数量额度")
    private Double nowcnt;
    /**
     * 金额完成比
     */
    @TableField(exist = false)
    @JSONField(name = "coamount")
    @JsonProperty("coamount")
    @ApiModelProperty("金额完成比")
    private Double coamount;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @ApiModelProperty("流程步骤")
    private String wfstep;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 负责人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("负责人")
    private String rempid;
    /**
     * 负责人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("负责人")
    private String rempname;
    /**
     * 是否跟踪结束
     */
    @TableField(exist = false)
    @JSONField(name = "istrackok")
    @JsonProperty("istrackok")
    @ApiModelProperty("是否跟踪结束")
    private Integer istrackok;
    /**
     * 合同扫描件
     */
    @TableField(value = "contractscan")
    @JSONField(name = "contractscan")
    @JsonProperty("contractscan")
    @ApiModelProperty("合同扫描件")
    private String contractscan;
    /**
     * 采购数量
     */
    @TableField(value = "pursum")
    @JSONField(name = "pursum")
    @JsonProperty("pursum")
    @ApiModelProperty("采购数量")
    private Double pursum;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @ApiModelProperty("单位")
    private String unitname;
    /**
     * 物品类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypename")
    @JsonProperty("itemtypename")
    @ApiModelProperty("物品类型")
    private String itemtypename;
    /**
     * 计划修理
     */
    @TableField(exist = false)
    @JSONField(name = "embidinquiryname")
    @JsonProperty("embidinquiryname")
    @ApiModelProperty("计划修理")
    private String embidinquiryname;
    /**
     * 物品类型
     */
    @TableField(value = "itemtypeid")
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @ApiModelProperty("物品类型")
    private String itemtypeid;
    /**
     * 计划修理
     */
    @TableField(value = "embidinquiryid")
    @JSONField(name = "embidinquiryid")
    @JsonProperty("embidinquiryid")
    @ApiModelProperty("计划修理")
    private String embidinquiryid;
    /**
     * 单位
     */
    @TableField(value = "unitid")
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @ApiModelProperty("单位")
    private String unitid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry embidinquiry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemType itemtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFUnit unit;



    /**
     * 设置 [评估报告]
     */
    public void setAssessreport(String assessreport) {
        this.assessreport = assessreport;
        this.modify("assessreport", assessreport);
    }

    /**
     * 设置 [采购计划项目]
     */
    public void setEmpurplanname(String empurplanname) {
        this.empurplanname = empurplanname;
        this.modify("empurplanname", empurplanname);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [采购年度]
     */
    public void setYears(String years) {
        this.years = years;
        this.modify("years", years);
    }

    /**
     * 设置 [物品类型]
     */
    public void setMsitemtype(String msitemtype) {
        this.msitemtype = msitemtype;
        this.modify("msitemtype", msitemtype);
    }

    /**
     * 设置 [计划修理状态]
     */
    public void setPlanstate(Integer planstate) {
        this.planstate = planstate;
        this.modify("planstate", planstate);
    }

    /**
     * 设置 [验收时间]
     */
    public void setAcceptancedate(Timestamp acceptancedate) {
        this.acceptancedate = acceptancedate;
        this.modify("acceptancedate", acceptancedate);
    }

    /**
     * 格式化日期 [验收时间]
     */
    public String formatAcceptancedate() {
        if (this.acceptancedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(acceptancedate);
    }
    /**
     * 设置 [服务商编号]
     */
    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
        this.modify("servicecode", servicecode);
    }

    /**
     * 设置 [验收结果]
     */
    public void setAcceptanceresult(String acceptanceresult) {
        this.acceptanceresult = acceptanceresult;
        this.modify("acceptanceresult", acceptanceresult);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [采购金额]
     */
    public void setPuramount(String puramount) {
        this.puramount = puramount;
        this.modify("puramount", puramount);
    }

    /**
     * 设置 [跟踪规则]
     */
    public void setTrackrule(String trackrule) {
        this.trackrule = trackrule;
        this.modify("trackrule", trackrule);
    }

    /**
     * 设置 [经理指定询价数]
     */
    public void setM3q(Integer m3q) {
        this.m3q = m3q;
        this.modify("m3q", m3q);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [负责人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [负责人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [合同扫描件]
     */
    public void setContractscan(String contractscan) {
        this.contractscan = contractscan;
        this.modify("contractscan", contractscan);
    }

    /**
     * 设置 [采购数量]
     */
    public void setPursum(Double pursum) {
        this.pursum = pursum;
        this.modify("pursum", pursum);
    }

    /**
     * 设置 [物品类型]
     */
    public void setItemtypeid(String itemtypeid) {
        this.itemtypeid = itemtypeid;
        this.modify("itemtypeid", itemtypeid);
    }

    /**
     * 设置 [计划修理]
     */
    public void setEmbidinquiryid(String embidinquiryid) {
        this.embidinquiryid = embidinquiryid;
        this.modify("embidinquiryid", embidinquiryid);
    }

    /**
     * 设置 [单位]
     */
    public void setUnitid(String unitid) {
        this.unitid = unitid;
        this.modify("unitid", unitid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("empurplanid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


