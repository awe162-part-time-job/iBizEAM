package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMPODetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPODetailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMPODetail] 服务对象接口
 */
public interface IEMPODetailService extends IService<EMPODetail> {

    boolean create(EMPODetail et);
    void createBatch(List<EMPODetail> list);
    boolean update(EMPODetail et);
    void updateBatch(List<EMPODetail> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMPODetail get(String key);
    EMPODetail getDraft(EMPODetail et);
    EMPODetail check(EMPODetail et);
    boolean checkBatch(List<EMPODetail> etList);
    boolean checkKey(EMPODetail et);
    EMPODetail createRin(EMPODetail et);
    boolean createRinBatch(List<EMPODetail> etList);
    EMPODetail genId(EMPODetail et);
    boolean genIdBatch(List<EMPODetail> etList);
    boolean save(EMPODetail et);
    void saveBatch(List<EMPODetail> list);
    Page<EMPODetail> searchClosed(EMPODetailSearchContext context);
    Page<EMPODetail> searchDefault(EMPODetailSearchContext context);
    Page<Map> searchLaterYear(EMPODetailSearchContext context);
    Page<EMPODetail> searchMain2(EMPODetailSearchContext context);
    Page<EMPODetail> searchWaitBook(EMPODetailSearchContext context);
    Page<EMPODetail> searchWaitCheck(EMPODetailSearchContext context);
    List<EMPODetail> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMPODetail> selectByPoid(String empoid);
    void removeByPoid(String empoid);
    List<EMPODetail> selectByWplistid(String emwplistid);
    void removeByWplistid(String emwplistid);
    List<EMPODetail> selectByEmpid(String pfempid);
    void removeByEmpid(String pfempid);
    List<EMPODetail> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMPODetail> selectByRunitid(String pfunitid);
    void removeByRunitid(String pfunitid);
    List<EMPODetail> selectByUnitid(String pfunitid);
    void removeByUnitid(String pfunitid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMPODetail> getEmpodetailByIds(List<String> ids);
    List<EMPODetail> getEmpodetailByEntities(List<EMPODetail> entities);
}


