package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICamHist;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamHistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICamHist] 服务对象接口
 */
public interface IEMEICamHistService extends IService<EMEICamHist> {

    boolean create(EMEICamHist et);
    void createBatch(List<EMEICamHist> list);
    boolean update(EMEICamHist et);
    void updateBatch(List<EMEICamHist> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICamHist get(String key);
    EMEICamHist getDraft(EMEICamHist et);
    boolean checkKey(EMEICamHist et);
    boolean save(EMEICamHist et);
    void saveBatch(List<EMEICamHist> list);
    Page<EMEICamHist> searchDefault(EMEICamHistSearchContext context);
    List<EMEICamHist> selectByEiobjid(String emeicamid);
    void removeByEiobjid(String emeicamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICamHist> getEmeicamhistByIds(List<String> ids);
    List<EMEICamHist> getEmeicamhistByEntities(List<EMEICamHist> entities);
}


