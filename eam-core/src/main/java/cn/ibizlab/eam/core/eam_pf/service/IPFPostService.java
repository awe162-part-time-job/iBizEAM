package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFPost;
import cn.ibizlab.eam.core.eam_pf.filter.PFPostSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFPost] 服务对象接口
 */
public interface IPFPostService extends IService<PFPost> {

    boolean create(PFPost et);
    void createBatch(List<PFPost> list);
    boolean update(PFPost et);
    void updateBatch(List<PFPost> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFPost get(String key);
    PFPost getDraft(PFPost et);
    boolean checkKey(PFPost et);
    boolean save(PFPost et);
    void saveBatch(List<PFPost> list);
    Page<PFPost> searchDefault(PFPostSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFPost> getPfpostByIds(List<String> ids);
    List<PFPost> getPfpostByEntities(List<PFPost> entities);
}


