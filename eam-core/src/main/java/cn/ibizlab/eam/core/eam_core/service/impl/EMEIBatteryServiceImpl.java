package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBattery;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatterySearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEIBatteryMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[电瓶] 服务对象接口实现
 */
@Slf4j
@Service("EMEIBatteryServiceImpl")
public class EMEIBatteryServiceImpl extends ServiceImpl<EMEIBatteryMapper, EMEIBattery> implements IEMEIBatteryService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryFillService emeibatteryfillService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatteryHistService emeibatteryhistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEIBatterySetupService emeibatterysetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLocationService emeqlocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEIBattery et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeibatteryid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEIBattery> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEIBattery et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeibatteryid", et.getEmeibatteryid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeibatteryid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEIBattery> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEIBattery get(String key) {
        EMEIBattery et = getById(key);
        if(et == null){
            et = new EMEIBattery();
            et.setEmeibatteryid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEIBattery getDraft(EMEIBattery et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEIBattery et) {
        return (!ObjectUtils.isEmpty(et.getEmeibatteryid())) && (!Objects.isNull(this.getById(et.getEmeibatteryid())));
    }
    @Override
    @Transactional
    public boolean save(EMEIBattery et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEIBattery et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEIBattery> list) {
        list.forEach(item->fillParentData(item));
        List<EMEIBattery> create = new ArrayList<>();
        List<EMEIBattery> update = new ArrayList<>();
        for (EMEIBattery et : list) {
            if (ObjectUtils.isEmpty(et.getEmeibatteryid()) || ObjectUtils.isEmpty(getById(et.getEmeibatteryid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEIBattery> list) {
        list.forEach(item->fillParentData(item));
        List<EMEIBattery> create = new ArrayList<>();
        List<EMEIBattery> update = new ArrayList<>();
        for (EMEIBattery et : list) {
            if (ObjectUtils.isEmpty(et.getEmeibatteryid()) || ObjectUtils.isEmpty(getById(et.getEmeibatteryid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEIBattery> selectByEqlocationid(String emeqlocationid) {
        return baseMapper.selectByEqlocationid(emeqlocationid);
    }
    @Override
    public void removeByEqlocationid(String emeqlocationid) {
        this.remove(new QueryWrapper<EMEIBattery>().eq("eqlocationid",emeqlocationid));
    }

	@Override
    public List<EMEIBattery> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMEIBattery>().eq("equipid",emequipid));
    }

	@Override
    public List<EMEIBattery> selectByItempuseid(String emitempuseid) {
        return baseMapper.selectByItempuseid(emitempuseid);
    }
    @Override
    public void removeByItempuseid(String emitempuseid) {
        this.remove(new QueryWrapper<EMEIBattery>().eq("itempuseid",emitempuseid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEIBattery> searchDefault(EMEIBatterySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEIBattery> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEIBattery>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEIBattery et){
        //实体关系[DER1N_EMEIBATTERY_EMEQLOCATION_EQLOCATIONID]
        if(!ObjectUtils.isEmpty(et.getEqlocationid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQLocation eqlocation=et.getEqlocation();
            if(ObjectUtils.isEmpty(eqlocation)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQLocation majorEntity=emeqlocationService.get(et.getEqlocationid());
                et.setEqlocation(majorEntity);
                eqlocation=majorEntity;
            }
            et.setEqlocationname(eqlocation.getEqlocationinfo());
        }
        //实体关系[DER1N_EMEIBATTERY_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMEIBATTERY_EMITEMPUSE_ITEMPUSEID]
        if(!ObjectUtils.isEmpty(et.getItempuseid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemPUse itempuse=et.getItempuse();
            if(ObjectUtils.isEmpty(itempuse)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemPUse majorEntity=emitempuseService.get(et.getItempuseid());
                et.setItempuse(majorEntity);
                itempuse=majorEntity;
            }
            et.setItempusename(itempuse.getEmitempusename());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEIBattery> getEmeibatteryByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEIBattery> getEmeibatteryByEntities(List<EMEIBattery> entities) {
        List ids =new ArrayList();
        for(EMEIBattery entity : entities){
            Serializable id=entity.getEmeibatteryid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEIBatteryService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



