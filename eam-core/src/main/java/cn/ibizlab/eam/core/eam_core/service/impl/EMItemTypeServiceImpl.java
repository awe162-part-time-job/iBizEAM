package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTypeSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemTypeService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemTypeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[物品类型] 服务对象接口实现
 */
@Slf4j
@Service("EMItemTypeServiceImpl")
public class EMItemTypeServiceImpl extends ServiceImpl<EMItemTypeMapper, EMItemType> implements IEMItemTypeService {


    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTypeService emitemtypeService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPurPlanService empurplanService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemType et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemtypeid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemType> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemType et) {
        fillParentData(et);
        emobjectService.update(emitemtypeInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emitemtypeid", et.getEmitemtypeid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemtypeid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemType> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemType get(String key) {
        EMItemType et = getById(key);
        if(et == null){
            et = new EMItemType();
            et.setEmitemtypeid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMItemType getDraft(EMItemType et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemType et) {
        return (!ObjectUtils.isEmpty(et.getEmitemtypeid())) && (!Objects.isNull(this.getById(et.getEmitemtypeid())));
    }
    @Override
    @Transactional
    public boolean save(EMItemType et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemType et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemType> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemType> create = new ArrayList<>();
        List<EMItemType> update = new ArrayList<>();
        for (EMItemType et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemtypeid()) || ObjectUtils.isEmpty(getById(et.getEmitemtypeid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemType> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemType> create = new ArrayList<>();
        List<EMItemType> update = new ArrayList<>();
        for (EMItemType et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemtypeid()) || ObjectUtils.isEmpty(getById(et.getEmitemtypeid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMItemType> selectByItembtypeid(String emitemtypeid) {
        return baseMapper.selectByItembtypeid(emitemtypeid);
    }
    @Override
    public void removeByItembtypeid(String emitemtypeid) {
        this.remove(new QueryWrapper<EMItemType>().eq("itembtypeid",emitemtypeid));
    }

	@Override
    public List<EMItemType> selectByItemmtypeid(String emitemtypeid) {
        return baseMapper.selectByItemmtypeid(emitemtypeid);
    }
    @Override
    public void removeByItemmtypeid(String emitemtypeid) {
        this.remove(new QueryWrapper<EMItemType>().eq("itemmtypeid",emitemtypeid));
    }

	@Override
    public List<EMItemType> selectByItemtypepid(String emitemtypeid) {
        return baseMapper.selectByItemtypepid(emitemtypeid);
    }
    @Override
    public void removeByItemtypepid(String emitemtypeid) {
        this.remove(new QueryWrapper<EMItemType>().eq("itemtypepid",emitemtypeid));
    }


    /**
     * 查询集合 ChildAll
     */
    @Override
    public Page<EMItemType> searchChildAll(EMItemTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemType> pages=baseMapper.searchChildAll(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemType> searchDefault(EMItemTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemType> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 上级类型查询
     */
    @Override
    public Page<EMItemType> searchRoot(EMItemTypeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemType> pages=baseMapper.searchRoot(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemType>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemType et){
        //实体关系[DER1N_EMITEMTYPE_EMITEMTYPE_ITEMBTYPEID]
        if(!ObjectUtils.isEmpty(et.getItembtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemType itembtype=et.getItembtype();
            if(ObjectUtils.isEmpty(itembtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemType majorEntity=emitemtypeService.get(et.getItembtypeid());
                et.setItembtype(majorEntity);
                itembtype=majorEntity;
            }
            et.setItembtypename(itembtype.getItemtypeinfo());
        }
        //实体关系[DER1N_EMITEMTYPE_EMITEMTYPE_ITEMMTYPEID]
        if(!ObjectUtils.isEmpty(et.getItemmtypeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemType itemmtype=et.getItemmtype();
            if(ObjectUtils.isEmpty(itemmtype)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemType majorEntity=emitemtypeService.get(et.getItemmtypeid());
                et.setItemmtype(majorEntity);
                itemmtype=majorEntity;
            }
            et.setItemmtypename(itemmtype.getItemtypeinfo());
        }
        //实体关系[DER1N_EMITEMTYPE_EMITEMTYPE_ITEMTYPEPID]
        if(!ObjectUtils.isEmpty(et.getItemtypepid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemType itemtypep=et.getItemtypep();
            if(ObjectUtils.isEmpty(itemtypep)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemType majorEntity=emitemtypeService.get(et.getItemtypepid());
                et.setItemtypep(majorEntity);
                itemtypep=majorEntity;
            }
            et.setItemtypepcode(itemtypep.getItemtypecode());
            et.setItemtypepname(itemtypep.getItemtypeinfo());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMItemTypeInheritMapping emitemtypeInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMItemType et){
        if(ObjectUtils.isEmpty(et.getEmitemtypeid()))
            et.setEmitemtypeid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emitemtypeInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","ITEMTYPE");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemType> getEmitemtypeByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemType> getEmitemtypeByEntities(List<EMItemType> entities) {
        List ids =new ArrayList();
        for(EMItemType entity : entities){
            Serializable id=entity.getEmitemtypeid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMItemTypeService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



