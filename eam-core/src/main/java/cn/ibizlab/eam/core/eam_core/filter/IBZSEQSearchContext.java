package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.IBZSEQ;
/**
 * 关系型数据实体[IBZSEQ] 查询条件对象
 */
@Slf4j
@Data
public class IBZSEQSearchContext extends QueryWrapperContext<IBZSEQ> {

	private String n_ibzseqid_eq;//[序列号标识]
	public void setN_ibzseqid_eq(String n_ibzseqid_eq) {
        this.n_ibzseqid_eq = n_ibzseqid_eq;
        if(!ObjectUtils.isEmpty(this.n_ibzseqid_eq)){
            this.getSearchCond().eq("ibzseqid", n_ibzseqid_eq);
        }
    }
	private String n_ibzseqname_like;//[序列号名称]
	public void setN_ibzseqname_like(String n_ibzseqname_like) {
        this.n_ibzseqname_like = n_ibzseqname_like;
        if(!ObjectUtils.isEmpty(this.n_ibzseqname_like)){
            this.getSearchCond().like("ibzseqname", n_ibzseqname_like);
        }
    }
	private String n_entity_eq;//[实体名称]
	public void setN_entity_eq(String n_entity_eq) {
        this.n_entity_eq = n_entity_eq;
        if(!ObjectUtils.isEmpty(this.n_entity_eq)){
            this.getSearchCond().eq("entity", n_entity_eq);
        }
    }
	private String n_prefix_eq;//[前缀]
	public void setN_prefix_eq(String n_prefix_eq) {
        this.n_prefix_eq = n_prefix_eq;
        if(!ObjectUtils.isEmpty(this.n_prefix_eq)){
            this.getSearchCond().eq("prefix", n_prefix_eq);
        }
    }
	private String n_params_eq;//[参数]
	public void setN_params_eq(String n_params_eq) {
        this.n_params_eq = n_params_eq;
        if(!ObjectUtils.isEmpty(this.n_params_eq)){
            this.getSearchCond().eq("params", n_params_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("ibzseqname", query)
            );
		 }
	}
}



