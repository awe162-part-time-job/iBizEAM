

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_M;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PLANSCHEDULE_MInheritMapping {

    @Mappings({
        @Mapping(source ="planscheduleMid",target = "planscheduleid"),
        @Mapping(source ="planscheduleMname",target = "planschedulename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE toPlanschedule(PLANSCHEDULE_M minorEntity);

    @Mappings({
        @Mapping(source ="planscheduleid" ,target = "planscheduleMid"),
        @Mapping(source ="planschedulename" ,target = "planscheduleMname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE_M toPlanscheduleM(PLANSCHEDULE majorEntity);

    List<PLANSCHEDULE> toPlanschedule(List<PLANSCHEDULE_M> minorEntities);

    List<PLANSCHEDULE_M> toPlanscheduleM(List<PLANSCHEDULE> majorEntities);

}


