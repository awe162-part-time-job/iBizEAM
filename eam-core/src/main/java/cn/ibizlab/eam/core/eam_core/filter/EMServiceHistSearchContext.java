package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceHist;
/**
 * 关系型数据实体[EMServiceHist] 查询条件对象
 */
@Slf4j
@Data
public class EMServiceHistSearchContext extends QueryWrapperContext<EMServiceHist> {

	private Integer n_range_eq;//[距离]
	public void setN_range_eq(Integer n_range_eq) {
        this.n_range_eq = n_range_eq;
        if(!ObjectUtils.isEmpty(this.n_range_eq)){
            this.getSearchCond().eq("range", n_range_eq);
        }
    }
	private String n_labservicelevelid_eq;//[级别]
	public void setN_labservicelevelid_eq(String n_labservicelevelid_eq) {
        this.n_labservicelevelid_eq = n_labservicelevelid_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicelevelid_eq)){
            this.getSearchCond().eq("labservicelevelid", n_labservicelevelid_eq);
        }
    }
	private String n_emservicehistname_like;//[服务商历史审批名称]
	public void setN_emservicehistname_like(String n_emservicehistname_like) {
        this.n_emservicehistname_like = n_emservicehistname_like;
        if(!ObjectUtils.isEmpty(this.n_emservicehistname_like)){
            this.getSearchCond().like("emservicehistname", n_emservicehistname_like);
        }
    }
	private String n_taxtypeid_eq;//[税类型]
	public void setN_taxtypeid_eq(String n_taxtypeid_eq) {
        this.n_taxtypeid_eq = n_taxtypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_taxtypeid_eq)){
            this.getSearchCond().eq("taxtypeid", n_taxtypeid_eq);
        }
    }
	private String n_labservicetypeid_eq;//[服务商类型]
	public void setN_labservicetypeid_eq(String n_labservicetypeid_eq) {
        this.n_labservicetypeid_eq = n_labservicetypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicetypeid_eq)){
            this.getSearchCond().eq("labservicetypeid", n_labservicetypeid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_payway_eq;//[付款方式]
	public void setN_payway_eq(String n_payway_eq) {
        this.n_payway_eq = n_payway_eq;
        if(!ObjectUtils.isEmpty(this.n_payway_eq)){
            this.getSearchCond().eq("payway", n_payway_eq);
        }
    }
	private String n_servicestate_eq;//[服务商状态]
	public void setN_servicestate_eq(String n_servicestate_eq) {
        this.n_servicestate_eq = n_servicestate_eq;
        if(!ObjectUtils.isEmpty(this.n_servicestate_eq)){
            this.getSearchCond().eq("servicestate", n_servicestate_eq);
        }
    }
	private String n_emservicename_eq;//[服务商]
	public void setN_emservicename_eq(String n_emservicename_eq) {
        this.n_emservicename_eq = n_emservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_emservicename_eq)){
            this.getSearchCond().eq("emservicename", n_emservicename_eq);
        }
    }
	private String n_emservicename_like;//[服务商]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_emserviceid_eq;//[服务商]
	public void setN_emserviceid_eq(String n_emserviceid_eq) {
        this.n_emserviceid_eq = n_emserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_emserviceid_eq)){
            this.getSearchCond().eq("emserviceid", n_emserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emservicehistname", query)
            );
		 }
	}
}



