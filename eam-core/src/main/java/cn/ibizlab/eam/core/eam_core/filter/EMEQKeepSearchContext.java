package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKeep;
/**
 * 关系型数据实体[EMEQKeep] 查询条件对象
 */
@Slf4j
@Data
public class EMEQKeepSearchContext extends QueryWrapperContext<EMEQKeep> {

	private String n_emeqkeepname_like;//[保养记录名称]
	public void setN_emeqkeepname_like(String n_emeqkeepname_like) {
        this.n_emeqkeepname_like = n_emeqkeepname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqkeepname_like)){
            this.getSearchCond().like("emeqkeepname", n_emeqkeepname_like);
        }
    }
	private String n_rempid_eq;//[责任人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_rdeptname_eq;//[责任部门]
	public void setN_rdeptname_eq(String n_rdeptname_eq) {
        this.n_rdeptname_eq = n_rdeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_eq)){
            this.getSearchCond().eq("rdeptname", n_rdeptname_eq);
        }
    }
	private String n_rdeptname_like;//[责任部门]
	public void setN_rdeptname_like(String n_rdeptname_like) {
        this.n_rdeptname_like = n_rdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_rdeptname_like)){
            this.getSearchCond().like("rdeptname", n_rdeptname_like);
        }
    }
	private String n_rdeptid_eq;//[责任部门]
	public void setN_rdeptid_eq(String n_rdeptid_eq) {
        this.n_rdeptid_eq = n_rdeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_rdeptid_eq)){
            this.getSearchCond().eq("rdeptid", n_rdeptid_eq);
        }
    }
	private String n_rempname_eq;//[责任人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[责任人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_activedesc_like;//[维护保养记录]
	public void setN_activedesc_like(String n_activedesc_like) {
        this.n_activedesc_like = n_activedesc_like;
        if(!ObjectUtils.isEmpty(this.n_activedesc_like)){
            this.getSearchCond().like("activedesc", n_activedesc_like);
        }
    }
	private String n_rfomoname_eq;//[模式]
	public void setN_rfomoname_eq(String n_rfomoname_eq) {
        this.n_rfomoname_eq = n_rfomoname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_eq)){
            this.getSearchCond().eq("rfomoname", n_rfomoname_eq);
        }
    }
	private String n_rfomoname_like;//[模式]
	public void setN_rfomoname_like(String n_rfomoname_like) {
        this.n_rfomoname_like = n_rfomoname_like;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_like)){
            this.getSearchCond().like("rfomoname", n_rfomoname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_rfodename_eq;//[现象]
	public void setN_rfodename_eq(String n_rfodename_eq) {
        this.n_rfodename_eq = n_rfodename_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodename_eq)){
            this.getSearchCond().eq("rfodename", n_rfodename_eq);
        }
    }
	private String n_rfodename_like;//[现象]
	public void setN_rfodename_like(String n_rfodename_like) {
        this.n_rfodename_like = n_rfodename_like;
        if(!ObjectUtils.isEmpty(this.n_rfodename_like)){
            this.getSearchCond().like("rfodename", n_rfodename_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_rservicename_eq;//[服务商]
	public void setN_rservicename_eq(String n_rservicename_eq) {
        this.n_rservicename_eq = n_rservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_rservicename_eq)){
            this.getSearchCond().eq("rservicename", n_rservicename_eq);
        }
    }
	private String n_rservicename_like;//[服务商]
	public void setN_rservicename_like(String n_rservicename_like) {
        this.n_rservicename_like = n_rservicename_like;
        if(!ObjectUtils.isEmpty(this.n_rservicename_like)){
            this.getSearchCond().like("rservicename", n_rservicename_like);
        }
    }
	private String n_rfocaname_eq;//[原因]
	public void setN_rfocaname_eq(String n_rfocaname_eq) {
        this.n_rfocaname_eq = n_rfocaname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfocaname_eq)){
            this.getSearchCond().eq("rfocaname", n_rfocaname_eq);
        }
    }
	private String n_rfocaname_like;//[原因]
	public void setN_rfocaname_like(String n_rfocaname_like) {
        this.n_rfocaname_like = n_rfocaname_like;
        if(!ObjectUtils.isEmpty(this.n_rfocaname_like)){
            this.getSearchCond().like("rfocaname", n_rfocaname_like);
        }
    }
	private String n_rteamname_eq;//[责任班组]
	public void setN_rteamname_eq(String n_rteamname_eq) {
        this.n_rteamname_eq = n_rteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamname_eq)){
            this.getSearchCond().eq("rteamname", n_rteamname_eq);
        }
    }
	private String n_rteamname_like;//[责任班组]
	public void setN_rteamname_like(String n_rteamname_like) {
        this.n_rteamname_like = n_rteamname_like;
        if(!ObjectUtils.isEmpty(this.n_rteamname_like)){
            this.getSearchCond().like("rteamname", n_rteamname_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_woname_eq;//[工单]
	public void setN_woname_eq(String n_woname_eq) {
        this.n_woname_eq = n_woname_eq;
        if(!ObjectUtils.isEmpty(this.n_woname_eq)){
            this.getSearchCond().eq("woname", n_woname_eq);
        }
    }
	private String n_woname_like;//[工单]
	public void setN_woname_like(String n_woname_like) {
        this.n_woname_like = n_woname_like;
        if(!ObjectUtils.isEmpty(this.n_woname_like)){
            this.getSearchCond().like("woname", n_woname_like);
        }
    }
	private String n_rfoacname_eq;//[方案]
	public void setN_rfoacname_eq(String n_rfoacname_eq) {
        this.n_rfoacname_eq = n_rfoacname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfoacname_eq)){
            this.getSearchCond().eq("rfoacname", n_rfoacname_eq);
        }
    }
	private String n_rfoacname_like;//[方案]
	public void setN_rfoacname_like(String n_rfoacname_like) {
        this.n_rfoacname_like = n_rfoacname_like;
        if(!ObjectUtils.isEmpty(this.n_rfoacname_like)){
            this.getSearchCond().like("rfoacname", n_rfoacname_like);
        }
    }
	private String n_rteamid_eq;//[责任班组]
	public void setN_rteamid_eq(String n_rteamid_eq) {
        this.n_rteamid_eq = n_rteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_rteamid_eq)){
            this.getSearchCond().eq("rteamid", n_rteamid_eq);
        }
    }
	private String n_woid_eq;//[工单]
	public void setN_woid_eq(String n_woid_eq) {
        this.n_woid_eq = n_woid_eq;
        if(!ObjectUtils.isEmpty(this.n_woid_eq)){
            this.getSearchCond().eq("woid", n_woid_eq);
        }
    }
	private String n_rfocaid_eq;//[原因]
	public void setN_rfocaid_eq(String n_rfocaid_eq) {
        this.n_rfocaid_eq = n_rfocaid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfocaid_eq)){
            this.getSearchCond().eq("rfocaid", n_rfocaid_eq);
        }
    }
	private String n_rfodeid_eq;//[现象]
	public void setN_rfodeid_eq(String n_rfodeid_eq) {
        this.n_rfodeid_eq = n_rfodeid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodeid_eq)){
            this.getSearchCond().eq("rfodeid", n_rfodeid_eq);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_rfoacid_eq;//[方案]
	public void setN_rfoacid_eq(String n_rfoacid_eq) {
        this.n_rfoacid_eq = n_rfoacid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfoacid_eq)){
            this.getSearchCond().eq("rfoacid", n_rfoacid_eq);
        }
    }
	private String n_rserviceid_eq;//[服务商]
	public void setN_rserviceid_eq(String n_rserviceid_eq) {
        this.n_rserviceid_eq = n_rserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_rserviceid_eq)){
            this.getSearchCond().eq("rserviceid", n_rserviceid_eq);
        }
    }
	private String n_rfomoid_eq;//[模式]
	public void setN_rfomoid_eq(String n_rfomoid_eq) {
        this.n_rfomoid_eq = n_rfomoid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoid_eq)){
            this.getSearchCond().eq("rfomoid", n_rfomoid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqkeepname", query)
            );
		 }
	}
}



