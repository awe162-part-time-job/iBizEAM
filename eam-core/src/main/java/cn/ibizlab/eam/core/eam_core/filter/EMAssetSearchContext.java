package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAsset;
/**
 * 关系型数据实体[EMAsset] 查询条件对象
 */
@Slf4j
@Data
public class EMAssetSearchContext extends QueryWrapperContext<EMAsset> {

	private String n_empname_eq;//[使用人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[使用人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_deptname_eq;//[使用部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[使用部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_assettype_eq;//[资产类别]
	public void setN_assettype_eq(String n_assettype_eq) {
        this.n_assettype_eq = n_assettype_eq;
        if(!ObjectUtils.isEmpty(this.n_assettype_eq)){
            this.getSearchCond().eq("assettype", n_assettype_eq);
        }
    }
	private String n_assetstate_eq;//[资产状态]
	public void setN_assetstate_eq(String n_assetstate_eq) {
        this.n_assetstate_eq = n_assetstate_eq;
        if(!ObjectUtils.isEmpty(this.n_assetstate_eq)){
            this.getSearchCond().eq("assetstate", n_assetstate_eq);
        }
    }
	private String n_deptid_eq;//[使用部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_rempid_eq;//[经办人]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_emassetname_like;//[资产名称]
	public void setN_emassetname_like(String n_emassetname_like) {
        this.n_emassetname_like = n_emassetname_like;
        if(!ObjectUtils.isEmpty(this.n_emassetname_like)){
            this.getSearchCond().like("emassetname", n_emassetname_like);
        }
    }
	private String n_empid_eq;//[使用人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_mgrdeptid_eq;//[管理部门]
	public void setN_mgrdeptid_eq(String n_mgrdeptid_eq) {
        this.n_mgrdeptid_eq = n_mgrdeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_mgrdeptid_eq)){
            this.getSearchCond().eq("mgrdeptid", n_mgrdeptid_eq);
        }
    }
	private String n_rempname_eq;//[经办人]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[经办人]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }
	private String n_assetinfo_like;//[资产信息]
	public void setN_assetinfo_like(String n_assetinfo_like) {
        this.n_assetinfo_like = n_assetinfo_like;
        if(!ObjectUtils.isEmpty(this.n_assetinfo_like)){
            this.getSearchCond().like("assetinfo", n_assetinfo_like);
        }
    }
	private String n_mgrdeptname_eq;//[管理部门]
	public void setN_mgrdeptname_eq(String n_mgrdeptname_eq) {
        this.n_mgrdeptname_eq = n_mgrdeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_mgrdeptname_eq)){
            this.getSearchCond().eq("mgrdeptname", n_mgrdeptname_eq);
        }
    }
	private String n_mgrdeptname_like;//[管理部门]
	public void setN_mgrdeptname_like(String n_mgrdeptname_like) {
        this.n_mgrdeptname_like = n_mgrdeptname_like;
        if(!ObjectUtils.isEmpty(this.n_mgrdeptname_like)){
            this.getSearchCond().like("mgrdeptname", n_mgrdeptname_like);
        }
    }
	private String n_labservicename_eq;//[产品供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[产品供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_contractname_eq;//[合同]
	public void setN_contractname_eq(String n_contractname_eq) {
        this.n_contractname_eq = n_contractname_eq;
        if(!ObjectUtils.isEmpty(this.n_contractname_eq)){
            this.getSearchCond().eq("contractname", n_contractname_eq);
        }
    }
	private String n_contractname_like;//[合同]
	public void setN_contractname_like(String n_contractname_like) {
        this.n_contractname_like = n_contractname_like;
        if(!ObjectUtils.isEmpty(this.n_contractname_like)){
            this.getSearchCond().like("contractname", n_contractname_like);
        }
    }
	private String n_assetclassname_eq;//[资产科目]
	public void setN_assetclassname_eq(String n_assetclassname_eq) {
        this.n_assetclassname_eq = n_assetclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_assetclassname_eq)){
            this.getSearchCond().eq("assetclassname", n_assetclassname_eq);
        }
    }
	private String n_assetclassname_like;//[资产科目]
	public void setN_assetclassname_like(String n_assetclassname_like) {
        this.n_assetclassname_like = n_assetclassname_like;
        if(!ObjectUtils.isEmpty(this.n_assetclassname_like)){
            this.getSearchCond().like("assetclassname", n_assetclassname_like);
        }
    }
	private String n_acclassname_eq;//[总帐科目]
	public void setN_acclassname_eq(String n_acclassname_eq) {
        this.n_acclassname_eq = n_acclassname_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassname_eq)){
            this.getSearchCond().eq("acclassname", n_acclassname_eq);
        }
    }
	private String n_acclassname_like;//[总帐科目]
	public void setN_acclassname_like(String n_acclassname_like) {
        this.n_acclassname_like = n_acclassname_like;
        if(!ObjectUtils.isEmpty(this.n_acclassname_like)){
            this.getSearchCond().like("acclassname", n_acclassname_like);
        }
    }
	private String n_eqlocationname_eq;//[位置]
	public void setN_eqlocationname_eq(String n_eqlocationname_eq) {
        this.n_eqlocationname_eq = n_eqlocationname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_eq)){
            this.getSearchCond().eq("eqlocationname", n_eqlocationname_eq);
        }
    }
	private String n_eqlocationname_like;//[位置]
	public void setN_eqlocationname_like(String n_eqlocationname_like) {
        this.n_eqlocationname_like = n_eqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_like)){
            this.getSearchCond().like("eqlocationname", n_eqlocationname_like);
        }
    }
	private String n_rservicename_eq;//[服务提供商]
	public void setN_rservicename_eq(String n_rservicename_eq) {
        this.n_rservicename_eq = n_rservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_rservicename_eq)){
            this.getSearchCond().eq("rservicename", n_rservicename_eq);
        }
    }
	private String n_rservicename_like;//[服务提供商]
	public void setN_rservicename_like(String n_rservicename_like) {
        this.n_rservicename_like = n_rservicename_like;
        if(!ObjectUtils.isEmpty(this.n_rservicename_like)){
            this.getSearchCond().like("rservicename", n_rservicename_like);
        }
    }
	private String n_unitname_eq;//[单位]
	public void setN_unitname_eq(String n_unitname_eq) {
        this.n_unitname_eq = n_unitname_eq;
        if(!ObjectUtils.isEmpty(this.n_unitname_eq)){
            this.getSearchCond().eq("unitname", n_unitname_eq);
        }
    }
	private String n_unitname_like;//[单位]
	public void setN_unitname_like(String n_unitname_like) {
        this.n_unitname_like = n_unitname_like;
        if(!ObjectUtils.isEmpty(this.n_unitname_like)){
            this.getSearchCond().like("unitname", n_unitname_like);
        }
    }
	private String n_mservicename_eq;//[制造商]
	public void setN_mservicename_eq(String n_mservicename_eq) {
        this.n_mservicename_eq = n_mservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_mservicename_eq)){
            this.getSearchCond().eq("mservicename", n_mservicename_eq);
        }
    }
	private String n_mservicename_like;//[制造商]
	public void setN_mservicename_like(String n_mservicename_like) {
        this.n_mservicename_like = n_mservicename_like;
        if(!ObjectUtils.isEmpty(this.n_mservicename_like)){
            this.getSearchCond().like("mservicename", n_mservicename_like);
        }
    }
	private String n_assetclassid_eq;//[资产科目]
	public void setN_assetclassid_eq(String n_assetclassid_eq) {
        this.n_assetclassid_eq = n_assetclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_assetclassid_eq)){
            this.getSearchCond().eq("assetclassid", n_assetclassid_eq);
        }
    }
	private String n_acclassid_eq;//[总帐科目]
	public void setN_acclassid_eq(String n_acclassid_eq) {
        this.n_acclassid_eq = n_acclassid_eq;
        if(!ObjectUtils.isEmpty(this.n_acclassid_eq)){
            this.getSearchCond().eq("acclassid", n_acclassid_eq);
        }
    }
	private String n_eqlocationid_eq;//[位置]
	public void setN_eqlocationid_eq(String n_eqlocationid_eq) {
        this.n_eqlocationid_eq = n_eqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationid_eq)){
            this.getSearchCond().eq("eqlocationid", n_eqlocationid_eq);
        }
    }
	private String n_unitid_eq;//[单位]
	public void setN_unitid_eq(String n_unitid_eq) {
        this.n_unitid_eq = n_unitid_eq;
        if(!ObjectUtils.isEmpty(this.n_unitid_eq)){
            this.getSearchCond().eq("unitid", n_unitid_eq);
        }
    }
	private String n_mserviceid_eq;//[制造商]
	public void setN_mserviceid_eq(String n_mserviceid_eq) {
        this.n_mserviceid_eq = n_mserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_mserviceid_eq)){
            this.getSearchCond().eq("mserviceid", n_mserviceid_eq);
        }
    }
	private String n_labserviceid_eq;//[产品供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_rserviceid_eq;//[服务提供商]
	public void setN_rserviceid_eq(String n_rserviceid_eq) {
        this.n_rserviceid_eq = n_rserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_rserviceid_eq)){
            this.getSearchCond().eq("rserviceid", n_rserviceid_eq);
        }
    }
	private String n_contractid_eq;//[合同]
	public void setN_contractid_eq(String n_contractid_eq) {
        this.n_contractid_eq = n_contractid_eq;
        if(!ObjectUtils.isEmpty(this.n_contractid_eq)){
            this.getSearchCond().eq("contractid", n_contractid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassetname", query)
            );
		 }
	}
}



