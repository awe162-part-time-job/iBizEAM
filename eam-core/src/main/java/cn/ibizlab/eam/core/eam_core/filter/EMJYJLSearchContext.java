package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMJYJL;
/**
 * 关系型数据实体[EMJYJL] 查询条件对象
 */
@Slf4j
@Data
public class EMJYJLSearchContext extends QueryWrapperContext<EMJYJL> {

	private String n_empid_eq;//[人员]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_deptid_eq;//[部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_deptname_eq;//[部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emjyjlname_like;//[加油记录名称]
	public void setN_emjyjlname_like(String n_emjyjlname_like) {
        this.n_emjyjlname_like = n_emjyjlname_like;
        if(!ObjectUtils.isEmpty(this.n_emjyjlname_like)){
            this.getSearchCond().like("emjyjlname", n_emjyjlname_like);
        }
    }
	private String n_empname_eq;//[人员]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[人员]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_teamname_eq;//[班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_itempusename_eq;//[领料单]
	public void setN_itempusename_eq(String n_itempusename_eq) {
        this.n_itempusename_eq = n_itempusename_eq;
        if(!ObjectUtils.isEmpty(this.n_itempusename_eq)){
            this.getSearchCond().eq("itempusename", n_itempusename_eq);
        }
    }
	private String n_itempusename_like;//[领料单]
	public void setN_itempusename_like(String n_itempusename_like) {
        this.n_itempusename_like = n_itempusename_like;
        if(!ObjectUtils.isEmpty(this.n_itempusename_like)){
            this.getSearchCond().like("itempusename", n_itempusename_like);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_itempuseid_eq;//[领料单]
	public void setN_itempuseid_eq(String n_itempuseid_eq) {
        this.n_itempuseid_eq = n_itempuseid_eq;
        if(!ObjectUtils.isEmpty(this.n_itempuseid_eq)){
            this.getSearchCond().eq("itempuseid", n_itempuseid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_teamid_eq;//[班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emjyjlname", query)
            );
		 }
	}
}



