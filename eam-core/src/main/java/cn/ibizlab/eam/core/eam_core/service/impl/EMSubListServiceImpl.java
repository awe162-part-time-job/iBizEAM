package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMSubList;
import cn.ibizlab.eam.core.eam_core.filter.EMSubListSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMSubListService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMSubListMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[材料申购清单] 服务对象接口实现
 */
@Slf4j
@Service("EMSubListServiceImpl")
public class EMSubListServiceImpl extends ServiceImpl<EMSubListMapper, EMSubList> implements IEMSubListService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMSubList et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmsublistid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMSubList> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMSubList et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emsublistid", et.getEmsublistid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmsublistid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMSubList> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMSubList get(String key) {
        EMSubList et = getById(key);
        if(et == null){
            et = new EMSubList();
            et.setEmsublistid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMSubList getDraft(EMSubList et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMSubList et) {
        return (!ObjectUtils.isEmpty(et.getEmsublistid())) && (!Objects.isNull(this.getById(et.getEmsublistid())));
    }
    @Override
    @Transactional
    public boolean save(EMSubList et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMSubList et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMSubList> list) {
        list.forEach(item->fillParentData(item));
        List<EMSubList> create = new ArrayList<>();
        List<EMSubList> update = new ArrayList<>();
        for (EMSubList et : list) {
            if (ObjectUtils.isEmpty(et.getEmsublistid()) || ObjectUtils.isEmpty(getById(et.getEmsublistid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMSubList> list) {
        list.forEach(item->fillParentData(item));
        List<EMSubList> create = new ArrayList<>();
        List<EMSubList> update = new ArrayList<>();
        for (EMSubList et : list) {
            if (ObjectUtils.isEmpty(et.getEmsublistid()) || ObjectUtils.isEmpty(getById(et.getEmsublistid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMSubList> selectByPfteamid(String pfteamid) {
        return baseMapper.selectByPfteamid(pfteamid);
    }
    @Override
    public void removeByPfteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMSubList>().eq("pfteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMSubList> searchDefault(EMSubListSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMSubList> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMSubList>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMSubList et){
        //实体关系[DER1N_EMSUBLIST_PFTEAM_PFTEAMID]
        if(!ObjectUtils.isEmpty(et.getPfteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam=et.getPfteam();
            if(ObjectUtils.isEmpty(pfteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getPfteamid());
                et.setPfteam(majorEntity);
                pfteam=majorEntity;
            }
            et.setPfteamname(pfteam.getPfteamname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMSubList> getEmsublistByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMSubList> getEmsublistByEntities(List<EMSubList> entities) {
        List ids =new ArrayList();
        for(EMSubList entity : entities){
            Serializable id=entity.getEmsublistid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMSubListService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



