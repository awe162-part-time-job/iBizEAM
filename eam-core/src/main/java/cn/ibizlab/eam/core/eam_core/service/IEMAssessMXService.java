package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMAssessMX;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMXSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMAssessMX] 服务对象接口
 */
public interface IEMAssessMXService extends IService<EMAssessMX> {

    boolean create(EMAssessMX et);
    void createBatch(List<EMAssessMX> list);
    boolean update(EMAssessMX et);
    void updateBatch(List<EMAssessMX> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMAssessMX get(String key);
    EMAssessMX getDraft(EMAssessMX et);
    boolean checkKey(EMAssessMX et);
    boolean save(EMAssessMX et);
    void saveBatch(List<EMAssessMX> list);
    Page<EMAssessMX> searchDefault(EMAssessMXSearchContext context);
    List<EMAssessMX> selectByEmdisableassessid(String emdisableassessid);
    void removeByEmdisableassessid(String emdisableassessid);
    List<EMAssessMX> selectByEmequipid(String emequipid);
    void removeByEmequipid(String emequipid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMAssessMX> getEmassessmxByIds(List<String> ids);
    List<EMAssessMX> getEmassessmxByEntities(List<EMAssessMX> entities);
}


