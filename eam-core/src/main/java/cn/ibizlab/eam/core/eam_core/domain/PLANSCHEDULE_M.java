package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[计划_按月]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "T_PLANSCHEDULE_M", resultMap = "PLANSCHEDULE_MResultMap")
@ApiModel("计划_按月")
public class PLANSCHEDULE_M extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 计划_按月标识
     */
    @DEField(name = "planschedule_mid", isKeyField = true)
    @TableId(value = "planschedule_mid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "planschedule_mid")
    @JsonProperty("planschedule_mid")
    @ApiModelProperty("计划_按月标识")
    private String planscheduleMid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 计划_按月名称
     */
    @DEField(name = "planschedule_mname")
    @TableField(value = "planschedule_mname")
    @JSONField(name = "planschedule_mname")
    @JsonProperty("planschedule_mname")
    @ApiModelProperty("计划_按月名称")
    private String planscheduleMname;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 计划编号
     */
    @TableField(exist = false)
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @ApiModelProperty("计划编号")
    private String emplanid;
    /**
     * 循环开始时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "cyclestarttime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cyclestarttime")
    @ApiModelProperty("循环开始时间")
    private Timestamp cyclestarttime;
    /**
     * 间隔时间
     */
    @TableField(exist = false)
    @JSONField(name = "intervalminute")
    @JsonProperty("intervalminute")
    @ApiModelProperty("间隔时间")
    private Integer intervalminute;
    /**
     * 时刻参数
     */
    @TableField(exist = false)
    @JSONField(name = "scheduleparam")
    @JsonProperty("scheduleparam")
    @ApiModelProperty("时刻参数")
    private String scheduleparam;
    /**
     * 时刻类型
     */
    @TableField(exist = false)
    @JSONField(name = "scheduletype")
    @JsonProperty("scheduletype")
    @ApiModelProperty("时刻类型")
    private String scheduletype;
    /**
     * 循环结束时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "cycleendtime", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("cycleendtime")
    @ApiModelProperty("循环结束时间")
    private Timestamp cycleendtime;
    /**
     * 计划名称
     */
    @TableField(exist = false)
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @ApiModelProperty("计划名称")
    private String emplanname;
    /**
     * 描述
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 时刻设置状态
     */
    @TableField(exist = false)
    @JSONField(name = "schedulestate")
    @JsonProperty("schedulestate")
    @ApiModelProperty("时刻设置状态")
    private String schedulestate;
    /**
     * 时刻参数
     */
    @TableField(exist = false)
    @JSONField(name = "scheduleparam4")
    @JsonProperty("scheduleparam4")
    @ApiModelProperty("时刻参数")
    private String scheduleparam4;
    /**
     * 时刻参数
     */
    @TableField(exist = false)
    @JSONField(name = "scheduleparam2")
    @JsonProperty("scheduleparam2")
    @ApiModelProperty("时刻参数")
    private String scheduleparam2;
    /**
     * 执行时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "runtime", format = "HH:mm:ss")
    @JsonProperty("runtime")
    @ApiModelProperty("执行时间")
    private Timestamp runtime;
    /**
     * 运行日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "rundate", format = "yyyy-MM-dd")
    @JsonProperty("rundate")
    @ApiModelProperty("运行日期")
    private Timestamp rundate;
    /**
     * 时刻参数
     */
    @TableField(exist = false)
    @JSONField(name = "scheduleparam3")
    @JsonProperty("scheduleparam3")
    @ApiModelProperty("时刻参数")
    private String scheduleparam3;
    /**
     * 星期
     */
    @TableField(value = "nocode")
    @JSONField(name = "nocode")
    @JsonProperty("nocode")
    @ApiModelProperty("星期")
    private Integer nocode;
    /**
     * 第
     */
    @TableField(value = "noseq")
    @JSONField(name = "noseq")
    @JsonProperty("noseq")
    @ApiModelProperty("第")
    private Integer noseq;
    /**
     * 第
     */
    @TableField(value = "noseq2")
    @JSONField(name = "noseq2")
    @JsonProperty("noseq2")
    @ApiModelProperty("第")
    private Integer noseq2;
    /**
     * 持续时间
     */
    @TableField(exist = false)
    @JSONField(name = "lastminute")
    @JsonProperty("lastminute")
    @ApiModelProperty("持续时间")
    private Integer lastminute;
    /**
     * 定时任务
     */
    @TableField(exist = false)
    @JSONField(name = "taskid")
    @JsonProperty("taskid")
    @ApiModelProperty("定时任务")
    private String taskid;



    /**
     * 设置 [计划_按月名称]
     */
    public void setPlanscheduleMname(String planscheduleMname) {
        this.planscheduleMname = planscheduleMname;
        this.modify("planschedule_mname", planscheduleMname);
    }

    /**
     * 设置 [星期]
     */
    public void setNocode(Integer nocode) {
        this.nocode = nocode;
        this.modify("nocode", nocode);
    }

    /**
     * 设置 [第]
     */
    public void setNoseq(Integer noseq) {
        this.noseq = noseq;
        this.modify("noseq", noseq);
    }

    /**
     * 设置 [第]
     */
    public void setNoseq2(Integer noseq2) {
        this.noseq2 = noseq2;
        this.modify("noseq2", noseq2);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("planschedule_mid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


