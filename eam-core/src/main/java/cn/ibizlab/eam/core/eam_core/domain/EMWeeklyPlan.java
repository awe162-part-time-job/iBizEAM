package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[维修中心班组周计划]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMWEEKLYPLAN_BASE", resultMap = "EMWeeklyPlanResultMap")
@ApiModel("维修中心班组周计划")
public class EMWeeklyPlan extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 制定日期
     */
    @TableField(value = "setdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "setdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("setdate")
    @ApiModelProperty("制定日期")
    private Timestamp setdate;
    /**
     * 下周计划
     */
    @TableField(value = "nextweek")
    @JSONField(name = "nextweek")
    @JsonProperty("nextweek")
    @ApiModelProperty("下周计划")
    private String nextweek;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    @ApiModelProperty("状态")
    private String state;
    /**
     * 上周工作计划完成情况
     */
    @TableField(value = "lastweek")
    @JSONField(name = "lastweek")
    @JsonProperty("lastweek")
    @ApiModelProperty("上周工作计划完成情况")
    private String lastweek;
    /**
     * 班组周计划工作名称
     */
    @DEField(defaultValue = "EMWEEKLYPLAN")
    @TableField(value = "emweeklyplanname")
    @JSONField(name = "emweeklyplanname")
    @JsonProperty("emweeklyplanname")
    @ApiModelProperty("班组周计划工作名称")
    private String emweeklyplanname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 班组周计划工作标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emweeklyplanid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emweeklyplanid")
    @JsonProperty("emweeklyplanid")
    @ApiModelProperty("班组周计划工作标识")
    private String emweeklyplanid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 序号
     */
    @TableField(value = "num")
    @JSONField(name = "num")
    @JsonProperty("num")
    @ApiModelProperty("序号")
    private Integer num;
    /**
     * 本周主要工作完成情况（计划外）
     */
    @TableField(value = "weekplan")
    @JSONField(name = "weekplan")
    @JsonProperty("weekplan")
    @ApiModelProperty("本周主要工作完成情况（计划外）")
    private String weekplan;
    /**
     * 班组
     */
    @TableField(exist = false)
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @ApiModelProperty("班组")
    private String pfteamname;
    /**
     * 班组
     */
    @TableField(value = "pfteamid")
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @ApiModelProperty("班组")
    private String pfteamid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam pfteam;



    /**
     * 设置 [制定日期]
     */
    public void setSetdate(Timestamp setdate) {
        this.setdate = setdate;
        this.modify("setdate", setdate);
    }

    /**
     * 格式化日期 [制定日期]
     */
    public String formatSetdate() {
        if (this.setdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(setdate);
    }
    /**
     * 设置 [下周计划]
     */
    public void setNextweek(String nextweek) {
        this.nextweek = nextweek;
        this.modify("nextweek", nextweek);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state) {
        this.state = state;
        this.modify("state", state);
    }

    /**
     * 设置 [上周工作计划完成情况]
     */
    public void setLastweek(String lastweek) {
        this.lastweek = lastweek;
        this.modify("lastweek", lastweek);
    }

    /**
     * 设置 [班组周计划工作名称]
     */
    public void setEmweeklyplanname(String emweeklyplanname) {
        this.emweeklyplanname = emweeklyplanname;
        this.modify("emweeklyplanname", emweeklyplanname);
    }

    /**
     * 设置 [序号]
     */
    public void setNum(Integer num) {
        this.num = num;
        this.modify("num", num);
    }

    /**
     * 设置 [本周主要工作完成情况（计划外）]
     */
    public void setWeekplan(String weekplan) {
        this.weekplan = weekplan;
        this.modify("weekplan", weekplan);
    }

    /**
     * 设置 [班组]
     */
    public void setPfteamid(String pfteamid) {
        this.pfteamid = pfteamid;
        this.modify("pfteamid", pfteamid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emweeklyplanid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


