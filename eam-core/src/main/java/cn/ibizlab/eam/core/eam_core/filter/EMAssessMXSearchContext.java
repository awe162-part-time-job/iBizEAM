package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMX;
/**
 * 关系型数据实体[EMAssessMX] 查询条件对象
 */
@Slf4j
@Data
public class EMAssessMXSearchContext extends QueryWrapperContext<EMAssessMX> {

	private String n_emassessmxname_like;//[设备停用明细考核名称]
	public void setN_emassessmxname_like(String n_emassessmxname_like) {
        this.n_emassessmxname_like = n_emassessmxname_like;
        if(!ObjectUtils.isEmpty(this.n_emassessmxname_like)){
            this.getSearchCond().like("emassessmxname", n_emassessmxname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emdisableassessname_eq;//[设备停用考核]
	public void setN_emdisableassessname_eq(String n_emdisableassessname_eq) {
        this.n_emdisableassessname_eq = n_emdisableassessname_eq;
        if(!ObjectUtils.isEmpty(this.n_emdisableassessname_eq)){
            this.getSearchCond().eq("emdisableassessname", n_emdisableassessname_eq);
        }
    }
	private String n_emdisableassessname_like;//[设备停用考核]
	public void setN_emdisableassessname_like(String n_emdisableassessname_like) {
        this.n_emdisableassessname_like = n_emdisableassessname_like;
        if(!ObjectUtils.isEmpty(this.n_emdisableassessname_like)){
            this.getSearchCond().like("emdisableassessname", n_emdisableassessname_like);
        }
    }
	private String n_emequipname_eq;//[停用设备]
	public void setN_emequipname_eq(String n_emequipname_eq) {
        this.n_emequipname_eq = n_emequipname_eq;
        if(!ObjectUtils.isEmpty(this.n_emequipname_eq)){
            this.getSearchCond().eq("emequipname", n_emequipname_eq);
        }
    }
	private String n_emequipname_like;//[停用设备]
	public void setN_emequipname_like(String n_emequipname_like) {
        this.n_emequipname_like = n_emequipname_like;
        if(!ObjectUtils.isEmpty(this.n_emequipname_like)){
            this.getSearchCond().like("emequipname", n_emequipname_like);
        }
    }
	private String n_emdisableassessid_eq;//[设备停用考核]
	public void setN_emdisableassessid_eq(String n_emdisableassessid_eq) {
        this.n_emdisableassessid_eq = n_emdisableassessid_eq;
        if(!ObjectUtils.isEmpty(this.n_emdisableassessid_eq)){
            this.getSearchCond().eq("emdisableassessid", n_emdisableassessid_eq);
        }
    }
	private String n_emequipid_eq;//[停用设备]
	public void setN_emequipid_eq(String n_emequipid_eq) {
        this.n_emequipid_eq = n_emequipid_eq;
        if(!ObjectUtils.isEmpty(this.n_emequipid_eq)){
            this.getSearchCond().eq("emequipid", n_emequipid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emassessmxname", query)
            );
		 }
	}
}



