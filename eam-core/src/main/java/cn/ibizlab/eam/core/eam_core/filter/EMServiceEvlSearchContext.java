package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
/**
 * 关系型数据实体[EMServiceEvl] 查询条件对象
 */
@Slf4j
@Data
public class EMServiceEvlSearchContext extends QueryWrapperContext<EMServiceEvl> {

	private String n_emserviceevlname_like;//[服务商评估名称]
	public void setN_emserviceevlname_like(String n_emserviceevlname_like) {
        this.n_emserviceevlname_like = n_emserviceevlname_like;
        if(!ObjectUtils.isEmpty(this.n_emserviceevlname_like)){
            this.getSearchCond().like("emserviceevlname", n_emserviceevlname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_empname_eq;//[评估人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[评估人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_evlregion_eq;//[评估区间]
	public void setN_evlregion_eq(String n_evlregion_eq) {
        this.n_evlregion_eq = n_evlregion_eq;
        if(!ObjectUtils.isEmpty(this.n_evlregion_eq)){
            this.getSearchCond().eq("evlregion", n_evlregion_eq);
        }
    }
	private String n_empid_eq;//[评估人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_serviceevlstate_eq;//[评估状态]
	public void setN_serviceevlstate_eq(String n_serviceevlstate_eq) {
        this.n_serviceevlstate_eq = n_serviceevlstate_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceevlstate_eq)){
            this.getSearchCond().eq("serviceevlstate", n_serviceevlstate_eq);
        }
    }
	private String n_servicename_eq;//[服务商]
	public void setN_servicename_eq(String n_servicename_eq) {
        this.n_servicename_eq = n_servicename_eq;
        if(!ObjectUtils.isEmpty(this.n_servicename_eq)){
            this.getSearchCond().eq("servicename", n_servicename_eq);
        }
    }
	private String n_servicename_like;//[服务商]
	public void setN_servicename_like(String n_servicename_like) {
        this.n_servicename_like = n_servicename_like;
        if(!ObjectUtils.isEmpty(this.n_servicename_like)){
            this.getSearchCond().like("servicename", n_servicename_like);
        }
    }
	private String n_serviceid_eq;//[服务商]
	public void setN_serviceid_eq(String n_serviceid_eq) {
        this.n_serviceid_eq = n_serviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_serviceid_eq)){
            this.getSearchCond().eq("serviceid", n_serviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emserviceevlname", query)
            );
		 }
	}
}



