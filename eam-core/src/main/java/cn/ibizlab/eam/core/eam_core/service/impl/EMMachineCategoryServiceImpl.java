package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory;
import cn.ibizlab.eam.core.eam_core.filter.EMMachineCategorySearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMMachineCategoryService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMMachineCategoryMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[机种编号] 服务对象接口实现
 */
@Slf4j
@Service("EMMachineCategoryServiceImpl")
public class EMMachineCategoryServiceImpl extends ServiceImpl<EMMachineCategoryMapper, EMMachineCategory> implements IEMMachineCategoryService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMMachineCategory et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmmachinecategoryid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMMachineCategory> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMMachineCategory et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emmachinecategoryid", et.getEmmachinecategoryid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmmachinecategoryid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMMachineCategory> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMMachineCategory get(String key) {
        EMMachineCategory et = getById(key);
        if(et == null){
            et = new EMMachineCategory();
            et.setEmmachinecategoryid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMMachineCategory getDraft(EMMachineCategory et) {
        return et;
    }

    @Override
    public boolean checkKey(EMMachineCategory et) {
        return (!ObjectUtils.isEmpty(et.getEmmachinecategoryid())) && (!Objects.isNull(this.getById(et.getEmmachinecategoryid())));
    }
    @Override
    @Transactional
    public boolean save(EMMachineCategory et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMMachineCategory et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMMachineCategory> list) {
        List<EMMachineCategory> create = new ArrayList<>();
        List<EMMachineCategory> update = new ArrayList<>();
        for (EMMachineCategory et : list) {
            if (ObjectUtils.isEmpty(et.getEmmachinecategoryid()) || ObjectUtils.isEmpty(getById(et.getEmmachinecategoryid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMMachineCategory> list) {
        List<EMMachineCategory> create = new ArrayList<>();
        List<EMMachineCategory> update = new ArrayList<>();
        for (EMMachineCategory et : list) {
            if (ObjectUtils.isEmpty(et.getEmmachinecategoryid()) || ObjectUtils.isEmpty(getById(et.getEmmachinecategoryid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMMachineCategory> searchDefault(EMMachineCategorySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMMachineCategory> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMMachineCategory>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMMachineCategory> getEmmachinecategoryByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMMachineCategory> getEmmachinecategoryByEntities(List<EMMachineCategory> entities) {
        List ids =new ArrayList();
        for(EMMachineCategory entity : entities){
            Serializable id=entity.getEmmachinecategoryid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMMachineCategoryService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



