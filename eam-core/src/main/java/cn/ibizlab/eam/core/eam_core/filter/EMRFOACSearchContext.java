package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMRFOAC;
/**
 * 关系型数据实体[EMRFOAC] 查询条件对象
 */
@Slf4j
@Data
public class EMRFOACSearchContext extends QueryWrapperContext<EMRFOAC> {

	private String n_emrfoacname_like;//[方案名称]
	public void setN_emrfoacname_like(String n_emrfoacname_like) {
        this.n_emrfoacname_like = n_emrfoacname_like;
        if(!ObjectUtils.isEmpty(this.n_emrfoacname_like)){
            this.getSearchCond().like("emrfoacname", n_emrfoacname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_rfoacinfo_like;//[信息]
	public void setN_rfoacinfo_like(String n_rfoacinfo_like) {
        this.n_rfoacinfo_like = n_rfoacinfo_like;
        if(!ObjectUtils.isEmpty(this.n_rfoacinfo_like)){
            this.getSearchCond().like("rfoacinfo", n_rfoacinfo_like);
        }
    }
	private String n_rfodenane_eq;//[现象]
	public void setN_rfodenane_eq(String n_rfodenane_eq) {
        this.n_rfodenane_eq = n_rfodenane_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodenane_eq)){
            this.getSearchCond().eq("rfodenane", n_rfodenane_eq);
        }
    }
	private String n_rfodenane_like;//[现象]
	public void setN_rfodenane_like(String n_rfodenane_like) {
        this.n_rfodenane_like = n_rfodenane_like;
        if(!ObjectUtils.isEmpty(this.n_rfodenane_like)){
            this.getSearchCond().like("rfodenane", n_rfodenane_like);
        }
    }
	private String n_rfomoname_eq;//[模式]
	public void setN_rfomoname_eq(String n_rfomoname_eq) {
        this.n_rfomoname_eq = n_rfomoname_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_eq)){
            this.getSearchCond().eq("rfomoname", n_rfomoname_eq);
        }
    }
	private String n_rfomoname_like;//[模式]
	public void setN_rfomoname_like(String n_rfomoname_like) {
        this.n_rfomoname_like = n_rfomoname_like;
        if(!ObjectUtils.isEmpty(this.n_rfomoname_like)){
            this.getSearchCond().like("rfomoname", n_rfomoname_like);
        }
    }
	private String n_rfodeid_eq;//[现象]
	public void setN_rfodeid_eq(String n_rfodeid_eq) {
        this.n_rfodeid_eq = n_rfodeid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfodeid_eq)){
            this.getSearchCond().eq("rfodeid", n_rfodeid_eq);
        }
    }
	private String n_rfomoid_eq;//[模式]
	public void setN_rfomoid_eq(String n_rfomoid_eq) {
        this.n_rfomoid_eq = n_rfomoid_eq;
        if(!ObjectUtils.isEmpty(this.n_rfomoid_eq)){
            this.getSearchCond().eq("rfomoid", n_rfomoid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emrfoacname", query)
            );
		 }
	}
}



