

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_T;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PLANSCHEDULE_TInheritMapping {

    @Mappings({
        @Mapping(source ="planscheduleTid",target = "planscheduleid"),
        @Mapping(source ="planscheduleTname",target = "planschedulename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE toPlanschedule(PLANSCHEDULE_T minorEntity);

    @Mappings({
        @Mapping(source ="planscheduleid" ,target = "planscheduleTid"),
        @Mapping(source ="planschedulename" ,target = "planscheduleTname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE_T toPlanscheduleT(PLANSCHEDULE majorEntity);

    List<PLANSCHEDULE> toPlanschedule(List<PLANSCHEDULE_T> minorEntities);

    List<PLANSCHEDULE_T> toPlanscheduleT(List<PLANSCHEDULE> majorEntities);

}


