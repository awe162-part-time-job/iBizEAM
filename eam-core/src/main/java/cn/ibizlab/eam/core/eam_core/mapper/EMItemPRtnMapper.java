package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPRtnSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemPRtnMapper extends BaseMapper<EMItemPRtn> {

    Page<EMItemPRtn> searchConfirmed(IPage page, @Param("srf") EMItemPRtnSearchContext context, @Param("ew") Wrapper<EMItemPRtn> wrapper);
    Page<EMItemPRtn> searchDefault(IPage page, @Param("srf") EMItemPRtnSearchContext context, @Param("ew") Wrapper<EMItemPRtn> wrapper);
    Page<EMItemPRtn> searchDraft(IPage page, @Param("srf") EMItemPRtnSearchContext context, @Param("ew") Wrapper<EMItemPRtn> wrapper);
    Page<EMItemPRtn> searchToConfirm(IPage page, @Param("srf") EMItemPRtnSearchContext context, @Param("ew") Wrapper<EMItemPRtn> wrapper);
    @Override
    EMItemPRtn selectById(Serializable id);
    @Override
    int insert(EMItemPRtn entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemPRtn entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemPRtn entity, @Param("ew") Wrapper<EMItemPRtn> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemPRtn> selectByDeptid(@Param("emitempuseid") Serializable emitempuseid);

    List<EMItemPRtn> selectByRid(@Param("emitempuseid") Serializable emitempuseid);

    List<EMItemPRtn> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemPRtn> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemPRtn> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemPRtn> selectByEmpid(@Param("pfempid") Serializable pfempid);

    List<EMItemPRtn> selectBySempid(@Param("pfempid") Serializable pfempid);

}
