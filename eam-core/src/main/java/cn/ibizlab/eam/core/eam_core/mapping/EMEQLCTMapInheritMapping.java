

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTMap;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQLCTMapInheritMapping {

    @Mappings({
        @Mapping(source ="emeqlctmapid",target = "emobjmapid"),
        @Mapping(source ="emeqlctmapname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="eqlocationpname",target = "objpname"),
        @Mapping(source ="eqlocationid",target = "objid"),
        @Mapping(source ="eqlocationname",target = "objname"),
        @Mapping(source ="eqlocationpid",target = "objpid"),
    })
    EMObjMap toEmobjmap(EMEQLCTMap minorEntity);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emeqlctmapid"),
        @Mapping(source ="emobjmapname" ,target = "emeqlctmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objpname",target = "eqlocationpname"),
        @Mapping(source ="objid",target = "eqlocationid"),
        @Mapping(source ="objname",target = "eqlocationname"),
        @Mapping(source ="objpid",target = "eqlocationpid"),
    })
    EMEQLCTMap toEmeqlctmap(EMObjMap majorEntity);

    List<EMObjMap> toEmobjmap(List<EMEQLCTMap> minorEntities);

    List<EMEQLCTMap> toEmeqlctmap(List<EMObjMap> majorEntities);

}


