package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[活动历史]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQAH_BASE", resultMap = "EMEQAHResultMap")
@ApiModel("活动历史")
public class EMEQAH extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 活动日期
     */
    @TableField(value = "activedate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "activedate", format = "yyyy-MM-dd")
    @JsonProperty("activedate")
    @ApiModelProperty("活动日期")
    private Timestamp activedate;
    /**
     * 活动记录
     */
    @TableField(value = "activedesc")
    @JSONField(name = "activedesc")
    @JsonProperty("activedesc")
    @ApiModelProperty("活动记录")
    private String activedesc;
    /**
     * 活动历史名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emeqahname")
    @JSONField(name = "emeqahname")
    @JsonProperty("emeqahname")
    @ApiModelProperty("活动历史名称")
    private String emeqahname;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 结束时间
     */
    @TableField(value = "regionenddate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionenddate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    @ApiModelProperty("结束时间")
    private Timestamp regionenddate;
    /**
     * 停运时间(分)
     */
    @TableField(value = "eqstoplength")
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;
    /**
     * 材料费(￥)
     */
    @TableField(value = "mfee")
    @JSONField(name = "mfee")
    @JsonProperty("mfee")
    @ApiModelProperty("材料费(￥)")
    private Double mfee;
    /**
     * 活动前情况
     */
    @TableField(value = "activebdesc")
    @JSONField(name = "activebdesc")
    @JsonProperty("activebdesc")
    @ApiModelProperty("活动前情况")
    private String activebdesc;
    /**
     * 活动后情况
     */
    @TableField(value = "activeadesc")
    @JSONField(name = "activeadesc")
    @JsonProperty("activeadesc")
    @ApiModelProperty("活动后情况")
    private String activeadesc;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptname")
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @ApiModelProperty("责任部门")
    private String rdeptname;
    /**
     * 责任部门
     */
    @TableField(value = "rdeptid")
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @ApiModelProperty("责任部门")
    private String rdeptid;
    /**
     * 持续时间(H)
     */
    @TableField(value = "activelengths")
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 活动历史标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqahid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqahid")
    @JsonProperty("emeqahid")
    @ApiModelProperty("活动历史标识")
    private String emeqahid;
    /**
     * 预算(￥)
     */
    @TableField(value = "prefee")
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @ApiModelProperty("预算(￥)")
    private Double prefee;
    /**
     * 责任人
     */
    @TableField(value = "rempname")
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("责任人")
    private String rempname;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 起始时间
     */
    @TableField(value = "regionbegindate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "regionbegindate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    @ApiModelProperty("起始时间")
    private Timestamp regionbegindate;
    /**
     * 服务费(￥)
     */
    @TableField(value = "sfee")
    @JSONField(name = "sfee")
    @JsonProperty("sfee")
    @ApiModelProperty("服务费(￥)")
    private Double sfee;
    /**
     * 详细内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    @ApiModelProperty("详细内容")
    private String content;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 责任人
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("责任人")
    private String rempid;
    /**
     * 人工费(￥)
     */
    @TableField(value = "pfee")
    @JSONField(name = "pfee")
    @JsonProperty("pfee")
    @ApiModelProperty("人工费(￥)")
    private Double pfee;
    /**
     * 分组类型
     */
    @TableField(value = "emeqahtype")
    @JSONField(name = "emeqahtype")
    @JsonProperty("emeqahtype")
    @ApiModelProperty("分组类型")
    private String emeqahtype;
    /**
     * 总帐科目
     */
    @TableField(exist = false)
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @ApiModelProperty("总帐科目")
    private String acclassname;
    /**
     * 方案
     */
    @TableField(exist = false)
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @ApiModelProperty("方案")
    private String rfoacname;
    /**
     * 模式
     */
    @TableField(exist = false)
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @ApiModelProperty("模式")
    private String rfomoname;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @ApiModelProperty("工单")
    private String woname;
    /**
     * 现象
     */
    @TableField(exist = false)
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @ApiModelProperty("现象")
    private String rfodename;
    /**
     * 原因
     */
    @TableField(exist = false)
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @ApiModelProperty("原因")
    private String rfocaname;
    /**
     * 服务商
     */
    @TableField(exist = false)
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @ApiModelProperty("服务商")
    private String rservicename;
    /**
     * 责任班组
     */
    @TableField(exist = false)
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @ApiModelProperty("责任班组")
    private String rteamname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @ApiModelProperty("设备")
    private String equipname;
    /**
     * 位置
     */
    @TableField(exist = false)
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @ApiModelProperty("位置")
    private String objname;
    /**
     * 现象
     */
    @TableField(value = "rfodeid")
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @ApiModelProperty("现象")
    private String rfodeid;
    /**
     * 原因
     */
    @TableField(value = "rfocaid")
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @ApiModelProperty("原因")
    private String rfocaid;
    /**
     * 责任班组
     */
    @TableField(value = "rteamid")
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @ApiModelProperty("责任班组")
    private String rteamid;
    /**
     * 总帐科目
     */
    @TableField(value = "acclassid")
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @ApiModelProperty("总帐科目")
    private String acclassid;
    /**
     * 方案
     */
    @TableField(value = "rfoacid")
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @ApiModelProperty("方案")
    private String rfoacid;
    /**
     * 模式
     */
    @TableField(value = "rfomoid")
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @ApiModelProperty("模式")
    private String rfomoid;
    /**
     * 位置
     */
    @TableField(value = "objid")
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @ApiModelProperty("位置")
    private String objid;
    /**
     * 设备
     */
    @TableField(value = "equipid")
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @ApiModelProperty("设备")
    private String equipid;
    /**
     * 工单
     */
    @TableField(value = "woid")
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @ApiModelProperty("工单")
    private String woid;
    /**
     * 服务商
     */
    @TableField(value = "rserviceid")
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @ApiModelProperty("服务商")
    private String rserviceid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip equip;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMObject obj;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService rservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMWO wo;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam;



    /**
     * 设置 [活动日期]
     */
    public void setActivedate(Timestamp activedate) {
        this.activedate = activedate;
        this.modify("activedate", activedate);
    }

    /**
     * 格式化日期 [活动日期]
     */
    public String formatActivedate() {
        if (this.activedate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(activedate);
    }
    /**
     * 设置 [活动记录]
     */
    public void setActivedesc(String activedesc) {
        this.activedesc = activedesc;
        this.modify("activedesc", activedesc);
    }

    /**
     * 设置 [活动历史名称]
     */
    public void setEmeqahname(String emeqahname) {
        this.emeqahname = emeqahname;
        this.modify("emeqahname", emeqahname);
    }

    /**
     * 设置 [结束时间]
     */
    public void setRegionenddate(Timestamp regionenddate) {
        this.regionenddate = regionenddate;
        this.modify("regionenddate", regionenddate);
    }

    /**
     * 格式化日期 [结束时间]
     */
    public String formatRegionenddate() {
        if (this.regionenddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionenddate);
    }
    /**
     * 设置 [停运时间(分)]
     */
    public void setEqstoplength(Double eqstoplength) {
        this.eqstoplength = eqstoplength;
        this.modify("eqstoplength", eqstoplength);
    }

    /**
     * 设置 [材料费(￥)]
     */
    public void setMfee(Double mfee) {
        this.mfee = mfee;
        this.modify("mfee", mfee);
    }

    /**
     * 设置 [活动前情况]
     */
    public void setActivebdesc(String activebdesc) {
        this.activebdesc = activebdesc;
        this.modify("activebdesc", activebdesc);
    }

    /**
     * 设置 [活动后情况]
     */
    public void setActiveadesc(String activeadesc) {
        this.activeadesc = activeadesc;
        this.modify("activeadesc", activeadesc);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptname(String rdeptname) {
        this.rdeptname = rdeptname;
        this.modify("rdeptname", rdeptname);
    }

    /**
     * 设置 [责任部门]
     */
    public void setRdeptid(String rdeptid) {
        this.rdeptid = rdeptid;
        this.modify("rdeptid", rdeptid);
    }

    /**
     * 设置 [持续时间(H)]
     */
    public void setActivelengths(Double activelengths) {
        this.activelengths = activelengths;
        this.modify("activelengths", activelengths);
    }

    /**
     * 设置 [预算(￥)]
     */
    public void setPrefee(Double prefee) {
        this.prefee = prefee;
        this.modify("prefee", prefee);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempname(String rempname) {
        this.rempname = rempname;
        this.modify("rempname", rempname);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [起始时间]
     */
    public void setRegionbegindate(Timestamp regionbegindate) {
        this.regionbegindate = regionbegindate;
        this.modify("regionbegindate", regionbegindate);
    }

    /**
     * 格式化日期 [起始时间]
     */
    public String formatRegionbegindate() {
        if (this.regionbegindate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(regionbegindate);
    }
    /**
     * 设置 [服务费(￥)]
     */
    public void setSfee(Double sfee) {
        this.sfee = sfee;
        this.modify("sfee", sfee);
    }

    /**
     * 设置 [详细内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [责任人]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }

    /**
     * 设置 [人工费(￥)]
     */
    public void setPfee(Double pfee) {
        this.pfee = pfee;
        this.modify("pfee", pfee);
    }

    /**
     * 设置 [分组类型]
     */
    public void setEmeqahtype(String emeqahtype) {
        this.emeqahtype = emeqahtype;
        this.modify("emeqahtype", emeqahtype);
    }

    /**
     * 设置 [现象]
     */
    public void setRfodeid(String rfodeid) {
        this.rfodeid = rfodeid;
        this.modify("rfodeid", rfodeid);
    }

    /**
     * 设置 [原因]
     */
    public void setRfocaid(String rfocaid) {
        this.rfocaid = rfocaid;
        this.modify("rfocaid", rfocaid);
    }

    /**
     * 设置 [责任班组]
     */
    public void setRteamid(String rteamid) {
        this.rteamid = rteamid;
        this.modify("rteamid", rteamid);
    }

    /**
     * 设置 [总帐科目]
     */
    public void setAcclassid(String acclassid) {
        this.acclassid = acclassid;
        this.modify("acclassid", acclassid);
    }

    /**
     * 设置 [方案]
     */
    public void setRfoacid(String rfoacid) {
        this.rfoacid = rfoacid;
        this.modify("rfoacid", rfoacid);
    }

    /**
     * 设置 [模式]
     */
    public void setRfomoid(String rfomoid) {
        this.rfomoid = rfomoid;
        this.modify("rfomoid", rfomoid);
    }

    /**
     * 设置 [位置]
     */
    public void setObjid(String objid) {
        this.objid = objid;
        this.modify("objid", objid);
    }

    /**
     * 设置 [设备]
     */
    public void setEquipid(String equipid) {
        this.equipid = equipid;
        this.modify("equipid", equipid);
    }

    /**
     * 设置 [工单]
     */
    public void setWoid(String woid) {
        this.woid = woid;
        this.modify("woid", woid);
    }

    /**
     * 设置 [服务商]
     */
    public void setRserviceid(String rserviceid) {
        this.rserviceid = rserviceid;
        this.modify("rserviceid", rserviceid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqahid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


