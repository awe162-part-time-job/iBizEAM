package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMPMTR;
/**
 * 关系型数据实体[EMEQMPMTR] 查询条件对象
 */
@Slf4j
@Data
public class EMEQMPMTRSearchContext extends QueryWrapperContext<EMEQMPMTR> {

	private String n_emeqmpmtrname_like;//[设备仪表读数名称]
	public void setN_emeqmpmtrname_like(String n_emeqmpmtrname_like) {
        this.n_emeqmpmtrname_like = n_emeqmpmtrname_like;
        if(!ObjectUtils.isEmpty(this.n_emeqmpmtrname_like)){
            this.getSearchCond().like("emeqmpmtrname", n_emeqmpmtrname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_woname_eq;//[工单]
	public void setN_woname_eq(String n_woname_eq) {
        this.n_woname_eq = n_woname_eq;
        if(!ObjectUtils.isEmpty(this.n_woname_eq)){
            this.getSearchCond().eq("woname", n_woname_eq);
        }
    }
	private String n_woname_like;//[工单]
	public void setN_woname_like(String n_woname_like) {
        this.n_woname_like = n_woname_like;
        if(!ObjectUtils.isEmpty(this.n_woname_like)){
            this.getSearchCond().like("woname", n_woname_like);
        }
    }
	private String n_mpname_eq;//[仪表]
	public void setN_mpname_eq(String n_mpname_eq) {
        this.n_mpname_eq = n_mpname_eq;
        if(!ObjectUtils.isEmpty(this.n_mpname_eq)){
            this.getSearchCond().eq("mpname", n_mpname_eq);
        }
    }
	private String n_mpname_like;//[仪表]
	public void setN_mpname_like(String n_mpname_like) {
        this.n_mpname_like = n_mpname_like;
        if(!ObjectUtils.isEmpty(this.n_mpname_like)){
            this.getSearchCond().like("mpname", n_mpname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_woid_eq;//[工单]
	public void setN_woid_eq(String n_woid_eq) {
        this.n_woid_eq = n_woid_eq;
        if(!ObjectUtils.isEmpty(this.n_woid_eq)){
            this.getSearchCond().eq("woid", n_woid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_mpid_eq;//[仪表]
	public void setN_mpid_eq(String n_mpid_eq) {
        this.n_mpid_eq = n_mpid_eq;
        if(!ObjectUtils.isEmpty(this.n_mpid_eq)){
            this.getSearchCond().eq("mpid", n_mpid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeqmpmtrname", query)
            );
		 }
	}
}



