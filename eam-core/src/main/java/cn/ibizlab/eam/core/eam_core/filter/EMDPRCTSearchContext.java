package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMDPRCT;
/**
 * 关系型数据实体[EMDPRCT] 查询条件对象
 */
@Slf4j
@Data
public class EMDPRCTSearchContext extends QueryWrapperContext<EMDPRCT> {

	private String n_emdprcttype_eq;//[测点记录类型]
	public void setN_emdprcttype_eq(String n_emdprcttype_eq) {
        this.n_emdprcttype_eq = n_emdprcttype_eq;
        if(!ObjectUtils.isEmpty(this.n_emdprcttype_eq)){
            this.getSearchCond().eq("emdprcttype", n_emdprcttype_eq);
        }
    }
	private String n_emdprctname_like;//[测点记录名称]
	public void setN_emdprctname_like(String n_emdprctname_like) {
        this.n_emdprctname_like = n_emdprctname_like;
        if(!ObjectUtils.isEmpty(this.n_emdprctname_like)){
            this.getSearchCond().like("emdprctname", n_emdprctname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_woname_eq;//[工单]
	public void setN_woname_eq(String n_woname_eq) {
        this.n_woname_eq = n_woname_eq;
        if(!ObjectUtils.isEmpty(this.n_woname_eq)){
            this.getSearchCond().eq("woname", n_woname_eq);
        }
    }
	private String n_woname_like;//[工单]
	public void setN_woname_like(String n_woname_like) {
        this.n_woname_like = n_woname_like;
        if(!ObjectUtils.isEmpty(this.n_woname_like)){
            this.getSearchCond().like("woname", n_woname_like);
        }
    }
	private String n_dpname_eq;//[测点]
	public void setN_dpname_eq(String n_dpname_eq) {
        this.n_dpname_eq = n_dpname_eq;
        if(!ObjectUtils.isEmpty(this.n_dpname_eq)){
            this.getSearchCond().eq("dpname", n_dpname_eq);
        }
    }
	private String n_dpname_like;//[测点]
	public void setN_dpname_like(String n_dpname_like) {
        this.n_dpname_like = n_dpname_like;
        if(!ObjectUtils.isEmpty(this.n_dpname_like)){
            this.getSearchCond().like("dpname", n_dpname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_woid_eq;//[工单]
	public void setN_woid_eq(String n_woid_eq) {
        this.n_woid_eq = n_woid_eq;
        if(!ObjectUtils.isEmpty(this.n_woid_eq)){
            this.getSearchCond().eq("woid", n_woid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_dpid_eq;//[测点]
	public void setN_dpid_eq(String n_dpid_eq) {
        this.n_dpid_eq = n_dpid_eq;
        if(!ObjectUtils.isEmpty(this.n_dpid_eq)){
            this.getSearchCond().eq("dpid", n_dpid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emdprctname", query)
            );
		 }
	}
}



