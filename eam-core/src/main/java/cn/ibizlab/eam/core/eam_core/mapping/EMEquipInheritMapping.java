

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEquip;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEquipInheritMapping {

    @Mappings({
        @Mapping(source ="emequipid",target = "emobjectid"),
        @Mapping(source ="emequipname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="equipcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEquip minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emequipid"),
        @Mapping(source ="emobjectname" ,target = "emequipname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "equipcode"),
    })
    EMEquip toEmequip(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEquip> minorEntities);

    List<EMEquip> toEmequip(List<EMObject> majorEntities);

}


