package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemRInSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemRIn] 服务对象接口
 */
public interface IEMItemRInService extends IService<EMItemRIn> {

    boolean create(EMItemRIn et);
    void createBatch(List<EMItemRIn> list);
    boolean update(EMItemRIn et);
    void updateBatch(List<EMItemRIn> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMItemRIn get(String key);
    EMItemRIn getDraft(EMItemRIn et);
    boolean checkKey(EMItemRIn et);
    EMItemRIn confirm(EMItemRIn et);
    boolean confirmBatch(List<EMItemRIn> etList);
    boolean save(EMItemRIn et);
    void saveBatch(List<EMItemRIn> list);
    EMItemRIn submit(EMItemRIn et);
    Page<EMItemRIn> searchDefault(EMItemRInSearchContext context);
    Page<EMItemRIn> searchPutIn(EMItemRInSearchContext context);
    Page<EMItemRIn> searchWaitIn(EMItemRInSearchContext context);
    List<EMItemRIn> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMItemRIn> selectByPodetailid(String empodetailid);
    void removeByPodetailid(String empodetailid);
    List<EMItemRIn> selectByEmserviceid(String emserviceid);
    void removeByEmserviceid(String emserviceid);
    List<EMItemRIn> selectByStorepartid(String emstorepartid);
    void removeByStorepartid(String emstorepartid);
    List<EMItemRIn> selectByStoreid(String emstoreid);
    void removeByStoreid(String emstoreid);
    List<EMItemRIn> selectByEmpid(String pfempid);
    void removeByEmpid(String pfempid);
    List<EMItemRIn> selectBySempid(String pfempid);
    void removeBySempid(String pfempid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemRIn> getEmitemrinByIds(List<String> ids);
    List<EMItemRIn> getEmitemrinByEntities(List<EMItemRIn> entities);
}


