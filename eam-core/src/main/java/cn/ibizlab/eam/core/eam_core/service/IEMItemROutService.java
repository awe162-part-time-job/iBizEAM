package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
import cn.ibizlab.eam.core.eam_core.filter.EMItemROutSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemROut] 服务对象接口
 */
public interface IEMItemROutService extends IService<EMItemROut> {

    boolean create(EMItemROut et);
    void createBatch(List<EMItemROut> list);
    boolean update(EMItemROut et);
    void updateBatch(List<EMItemROut> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMItemROut get(String key);
    EMItemROut getDraft(EMItemROut et);
    boolean checkKey(EMItemROut et);
    EMItemROut confirm(EMItemROut et);
    boolean confirmBatch(List<EMItemROut> etList);
    EMItemROut formUpdateByRID(EMItemROut et);
    EMItemROut rejected(EMItemROut et);
    boolean save(EMItemROut et);
    void saveBatch(List<EMItemROut> list);
    EMItemROut submit(EMItemROut et);
    Page<EMItemROut> searchConfirmed(EMItemROutSearchContext context);
    Page<EMItemROut> searchDefault(EMItemROutSearchContext context);
    Page<EMItemROut> searchDraft(EMItemROutSearchContext context);
    Page<EMItemROut> searchToConfirm(EMItemROutSearchContext context);
    List<EMItemROut> selectByRid(String emitemrinid);
    void removeByRid(String emitemrinid);
    List<EMItemROut> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMItemROut> selectByStorepartid(String emstorepartid);
    void removeByStorepartid(String emstorepartid);
    List<EMItemROut> selectByStoreid(String emstoreid);
    void removeByStoreid(String emstoreid);
    List<EMItemROut> selectBySempid(String pfempid);
    void removeBySempid(String pfempid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemROut> getEmitemroutByIds(List<String> ids);
    List<EMItemROut> getEmitemroutByEntities(List<EMItemROut> entities);
}


