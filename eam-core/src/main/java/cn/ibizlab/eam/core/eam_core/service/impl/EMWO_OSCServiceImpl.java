package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_OSCSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWO_OSCMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[外委保养工单] 服务对象接口实现
 */
@Slf4j
@Service("EMWO_OSCServiceImpl")
public class EMWO_OSCServiceImpl extends ServiceImpl<EMWO_OSCMapper, EMWO_OSC> implements IEMWO_OSCService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOACService emrfoacService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService emrfocaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService emrfomoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOORIService emwooriService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFDeptService pfdeptService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWO_OSC et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwoOscid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWO_OSC> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWO_OSC et) {
        fillParentData(et);
        emresrefobjService.update(emwo_oscInheritMapping.toEmresrefobj(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emwo_oscid", et.getEmwoOscid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwoOscid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWO_OSC> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emresrefobjService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWO_OSC get(String key) {
        EMWO_OSC et = getById(key);
        if(et == null){
            et = new EMWO_OSC();
            et.setEmwoOscid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMWO_OSC getDraft(EMWO_OSC et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public EMWO_OSC acceptance(EMWO_OSC et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean acceptanceBatch(List<EMWO_OSC> etList) {
        for(EMWO_OSC et : etList) {
            acceptance(et);
        }
        return true;
    }

    @Override
    public boolean checkKey(EMWO_OSC et) {
        return (!ObjectUtils.isEmpty(et.getEmwoOscid())) && (!Objects.isNull(this.getById(et.getEmwoOscid())));
    }
    @Override
    @Transactional
    public EMWO_OSC formUpdateByEmequipid(EMWO_OSC et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMWO_OSC et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWO_OSC et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWO_OSC> list) {
        list.forEach(item->fillParentData(item));
        List<EMWO_OSC> create = new ArrayList<>();
        List<EMWO_OSC> update = new ArrayList<>();
        for (EMWO_OSC et : list) {
            if (ObjectUtils.isEmpty(et.getEmwoOscid()) || ObjectUtils.isEmpty(getById(et.getEmwoOscid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWO_OSC> list) {
        list.forEach(item->fillParentData(item));
        List<EMWO_OSC> create = new ArrayList<>();
        List<EMWO_OSC> update = new ArrayList<>();
        for (EMWO_OSC et : list) {
            if (ObjectUtils.isEmpty(et.getEmwoOscid()) || ObjectUtils.isEmpty(getById(et.getEmwoOscid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }

    @Override
    @Transactional
    public EMWO_OSC submit(EMWO_OSC et) {
         return et ;
    }

    @Override
    @Transactional
    public EMWO_OSC unAcceptance(EMWO_OSC et) {
         return et ;
    }


	@Override
    public List<EMWO_OSC> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }
    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMWO_OSC> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("equipid",emequipid));
    }

	@Override
    public List<EMWO_OSC> selectByDpid(String emobjectid) {
        return baseMapper.selectByDpid(emobjectid);
    }
    @Override
    public void removeByDpid(String emobjectid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("dpid",emobjectid));
    }

	@Override
    public List<EMWO_OSC> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("objid",emobjectid));
    }

	@Override
    public List<EMWO_OSC> selectByRfoacid(String emrfoacid) {
        return baseMapper.selectByRfoacid(emrfoacid);
    }
    @Override
    public void removeByRfoacid(String emrfoacid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rfoacid",emrfoacid));
    }

	@Override
    public List<EMWO_OSC> selectByRfocaid(String emrfocaid) {
        return baseMapper.selectByRfocaid(emrfocaid);
    }
    @Override
    public void removeByRfocaid(String emrfocaid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rfocaid",emrfocaid));
    }

	@Override
    public List<EMWO_OSC> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }
    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rfodeid",emrfodeid));
    }

	@Override
    public List<EMWO_OSC> selectByRfomoid(String emrfomoid) {
        return baseMapper.selectByRfomoid(emrfomoid);
    }
    @Override
    public void removeByRfomoid(String emrfomoid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rfomoid",emrfomoid));
    }

	@Override
    public List<EMWO_OSC> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }
    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMWO_OSC> selectByWooriid(String emwooriid) {
        return baseMapper.selectByWooriid(emwooriid);
    }
    @Override
    public void removeByWooriid(String emwooriid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("wooriid",emwooriid));
    }

	@Override
    public List<EMWO_OSC> selectByWopid(String emwoid) {
        return baseMapper.selectByWopid(emwoid);
    }
    @Override
    public void removeByWopid(String emwoid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("wopid",emwoid));
    }

	@Override
    public List<EMWO_OSC> selectByRdeptid(String pfdeptid) {
        return baseMapper.selectByRdeptid(pfdeptid);
    }
    @Override
    public void removeByRdeptid(String pfdeptid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rdeptid",pfdeptid));
    }

	@Override
    public List<EMWO_OSC> selectByMpersonid(String pfempid) {
        return baseMapper.selectByMpersonid(pfempid);
    }
    @Override
    public void removeByMpersonid(String pfempid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("mpersonid",pfempid));
    }

	@Override
    public List<EMWO_OSC> selectByRecvpersonid(String pfempid) {
        return baseMapper.selectByRecvpersonid(pfempid);
    }
    @Override
    public void removeByRecvpersonid(String pfempid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("recvpersonid",pfempid));
    }

	@Override
    public List<EMWO_OSC> selectByRempid(String pfempid) {
        return baseMapper.selectByRempid(pfempid);
    }
    @Override
    public void removeByRempid(String pfempid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rempid",pfempid));
    }

	@Override
    public List<EMWO_OSC> selectByWpersonid(String pfempid) {
        return baseMapper.selectByWpersonid(pfempid);
    }
    @Override
    public void removeByWpersonid(String pfempid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("wpersonid",pfempid));
    }

	@Override
    public List<EMWO_OSC> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }
    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMWO_OSC>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 日历
     */
    @Override
    public Page<EMWO_OSC> searchCalendar(EMWO_OSCSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_OSC> pages=baseMapper.searchCalendar(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_OSC>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 已完成
     */
    @Override
    public Page<EMWO_OSC> searchConfirmed(EMWO_OSCSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_OSC> pages=baseMapper.searchConfirmed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_OSC>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWO_OSC> searchDefault(EMWO_OSCSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_OSC> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_OSC>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<EMWO_OSC> searchDraft(EMWO_OSCSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_OSC> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_OSC>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 执行中
     */
    @Override
    public Page<EMWO_OSC> searchToConfirm(EMWO_OSCSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_OSC> pages=baseMapper.searchToConfirm(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_OSC>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMWO_OSC et){
        //实体关系[DER1N_EMWO_OSC_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMWO_OSC_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipcode(equip.getEquipcode());
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMWO_OSC_EMOBJECT_DPID]
        if(!ObjectUtils.isEmpty(et.getDpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject dp=et.getDp();
            if(ObjectUtils.isEmpty(dp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getDpid());
                et.setDp(majorEntity);
                dp=majorEntity;
            }
            et.setDptype(dp.getEmobjecttype());
            et.setDpname(dp.getEmobjectname());
        }
        //实体关系[DER1N_EMWO_OSC_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMWO_OSC_EMRFOAC_RFOACID]
        if(!ObjectUtils.isEmpty(et.getRfoacid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac=et.getRfoac();
            if(ObjectUtils.isEmpty(rfoac)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOAC majorEntity=emrfoacService.get(et.getRfoacid());
                et.setRfoac(majorEntity);
                rfoac=majorEntity;
            }
            et.setRfoacname(rfoac.getEmrfoacname());
        }
        //实体关系[DER1N_EMWO_OSC_EMRFOCA_RFOCAID]
        if(!ObjectUtils.isEmpty(et.getRfocaid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca=et.getRfoca();
            if(ObjectUtils.isEmpty(rfoca)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOCA majorEntity=emrfocaService.get(et.getRfocaid());
                et.setRfoca(majorEntity);
                rfoca=majorEntity;
            }
            et.setRfocaname(rfoca.getEmrfocaname());
        }
        //实体关系[DER1N_EMWO_OSC_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
        //实体关系[DER1N_EMWO_OSC_EMRFOMO_RFOMOID]
        if(!ObjectUtils.isEmpty(et.getRfomoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo=et.getRfomo();
            if(ObjectUtils.isEmpty(rfomo)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOMO majorEntity=emrfomoService.get(et.getRfomoid());
                et.setRfomo(majorEntity);
                rfomo=majorEntity;
            }
            et.setRfomoname(rfomo.getEmrfomoname());
        }
        //实体关系[DER1N_EMWO_OSC_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMWO_OSC_EMWOORI_WOORIID]
        if(!ObjectUtils.isEmpty(et.getWooriid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWOORI woori=et.getWoori();
            if(ObjectUtils.isEmpty(woori)){
                cn.ibizlab.eam.core.eam_core.domain.EMWOORI majorEntity=emwooriService.get(et.getWooriid());
                et.setWoori(majorEntity);
                woori=majorEntity;
            }
            et.setWooritype(woori.getEmwooritype());
            et.setWooriname(woori.getEmwooriname());
        }
        //实体关系[DER1N_EMWO_OSC_EMWO_WOPID]
        if(!ObjectUtils.isEmpty(et.getWopid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wop=et.getWop();
            if(ObjectUtils.isEmpty(wop)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWopid());
                et.setWop(majorEntity);
                wop=majorEntity;
            }
            et.setWopname(wop.getEmwoname());
        }
        //实体关系[DER1N_EMWO_OSC_PFDEPT_RDEPTID]
        if(!ObjectUtils.isEmpty(et.getRdeptid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFDept pfdeptdid=et.getPfdeptdid();
            if(ObjectUtils.isEmpty(pfdeptdid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFDept majorEntity=pfdeptService.get(et.getRdeptid());
                et.setPfdeptdid(majorEntity);
                pfdeptdid=majorEntity;
            }
            et.setRdeptname(pfdeptdid.getDeptinfo());
        }
        //实体关系[DER1N_EMWO_OSC_PFEMP_MPERSONID]
        if(!ObjectUtils.isEmpty(et.getMpersonid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid=et.getPfempid();
            if(ObjectUtils.isEmpty(pfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getMpersonid());
                et.setPfempid(majorEntity);
                pfempid=majorEntity;
            }
            et.setMpersonname(pfempid.getPfempname());
        }
        //实体关系[DER1N_EMWO_OSC_PFEMP_RECVPERSONID]
        if(!ObjectUtils.isEmpty(et.getRecvpersonid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp cvpersonid=et.getCvpersonid();
            if(ObjectUtils.isEmpty(cvpersonid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getRecvpersonid());
                et.setCvpersonid(majorEntity);
                cvpersonid=majorEntity;
            }
            et.setRecvpersonname(cvpersonid.getEmpinfo());
        }
        //实体关系[DER1N_EMWO_OSC_PFEMP_REMPID]
        if(!ObjectUtils.isEmpty(et.getRempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp rpfempid=et.getRpfempid();
            if(ObjectUtils.isEmpty(rpfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getRempid());
                et.setRpfempid(majorEntity);
                rpfempid=majorEntity;
            }
            et.setRempname(rpfempid.getEmpinfo());
        }
        //实体关系[DER1N_EMWO_OSC_PFEMP_WPERSONID]
        if(!ObjectUtils.isEmpty(et.getWpersonid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp empid=et.getEmpid();
            if(ObjectUtils.isEmpty(empid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getWpersonid());
                et.setEmpid(majorEntity);
                empid=majorEntity;
            }
            et.setWpersonname(empid.getEmpinfo());
        }
        //实体关系[DER1N_EMWO_OSC_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMWO_OSCInheritMapping emwo_oscInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMWO_OSC et){
        if(ObjectUtils.isEmpty(et.getEmwoOscid()))
            et.setEmwoOscid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMResRefObj emresrefobj =emwo_oscInheritMapping.toEmresrefobj(et);
        emresrefobj.set("emresrefobjtype","WO_OSC");
        emresrefobjService.create(emresrefobj);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWO_OSC> getEmwoOscByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWO_OSC> getEmwoOscByEntities(List<EMWO_OSC> entities) {
        List ids =new ArrayList();
        for(EMWO_OSC entity : entities){
            Serializable id=entity.getEmwoOscid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMWO_OSCService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



