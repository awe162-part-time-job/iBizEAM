package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEIReportM;
import cn.ibizlab.eam.core.eam_core.filter.EMEIReportMSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEIReportMService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEIReportMMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[月度工作报告] 服务对象接口实现
 */
@Slf4j
@Service("EMEIReportMServiceImpl")
public class EMEIReportMServiceImpl extends ServiceImpl<EMEIReportMMapper, EMEIReportM> implements IEMEIReportMService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEIReportM et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeireportmid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEIReportM> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEIReportM et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeireportmid", et.getEmeireportmid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeireportmid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEIReportM> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEIReportM get(String key) {
        EMEIReportM et = getById(key);
        if(et == null){
            et = new EMEIReportM();
            et.setEmeireportmid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEIReportM getDraft(EMEIReportM et) {
        return et;
    }

    @Override
    public boolean checkKey(EMEIReportM et) {
        return (!ObjectUtils.isEmpty(et.getEmeireportmid())) && (!Objects.isNull(this.getById(et.getEmeireportmid())));
    }
    @Override
    @Transactional
    public boolean save(EMEIReportM et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEIReportM et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEIReportM> list) {
        List<EMEIReportM> create = new ArrayList<>();
        List<EMEIReportM> update = new ArrayList<>();
        for (EMEIReportM et : list) {
            if (ObjectUtils.isEmpty(et.getEmeireportmid()) || ObjectUtils.isEmpty(getById(et.getEmeireportmid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEIReportM> list) {
        List<EMEIReportM> create = new ArrayList<>();
        List<EMEIReportM> update = new ArrayList<>();
        for (EMEIReportM et : list) {
            if (ObjectUtils.isEmpty(et.getEmeireportmid()) || ObjectUtils.isEmpty(getById(et.getEmeireportmid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEIReportM> searchDefault(EMEIReportMSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEIReportM> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEIReportM>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEIReportM> getEmeireportmByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEIReportM> getEmeireportmByEntities(List<EMEIReportM> entities) {
        List ids =new ArrayList();
        for(EMEIReportM entity : entities){
            Serializable id=entity.getEmeireportmid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEIReportMService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



