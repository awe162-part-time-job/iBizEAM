package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMBidding;
/**
 * 关系型数据实体[EMBidding] 查询条件对象
 */
@Slf4j
@Data
public class EMBiddingSearchContext extends QueryWrapperContext<EMBidding> {

	private String n_embiddingname_like;//[项目招投标名称]
	public void setN_embiddingname_like(String n_embiddingname_like) {
        this.n_embiddingname_like = n_embiddingname_like;
        if(!ObjectUtils.isEmpty(this.n_embiddingname_like)){
            this.getSearchCond().like("embiddingname", n_embiddingname_like);
        }
    }
	private String n_emservicename_eq;//[服务商]
	public void setN_emservicename_eq(String n_emservicename_eq) {
        this.n_emservicename_eq = n_emservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_emservicename_eq)){
            this.getSearchCond().eq("emservicename", n_emservicename_eq);
        }
    }
	private String n_emservicename_like;//[服务商]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_emserviceid_eq;//[服务商]
	public void setN_emserviceid_eq(String n_emserviceid_eq) {
        this.n_emserviceid_eq = n_emserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_emserviceid_eq)){
            this.getSearchCond().eq("emserviceid", n_emserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("embiddingname", query)
            );
		 }
	}
}



