package cn.ibizlab.eam.core.r7rt_dyna.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.eam.core.r7rt_dyna.filter.DynaDashboardSearchContext;
import cn.ibizlab.eam.core.r7rt_dyna.service.IDynaDashboardService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import cn.ibizlab.eam.core.r7rt_dyna.client.DynaDashboardFeignClient;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 实体[动态数据看板] 服务对象接口实现
 */
@Slf4j
@Service
public class DynaDashboardServiceImpl implements IDynaDashboardService {

    @Autowired
    DynaDashboardFeignClient dynaDashboardFeignClient;


    @Override
    public boolean create(DynaDashboard et) {
        DynaDashboard rt = dynaDashboardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt, et);
        return true;
    }

    public void createBatch(List<DynaDashboard> list){
        dynaDashboardFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(DynaDashboard et) {
        DynaDashboard rt = dynaDashboardFeignClient.update(et.getDynadashboardid(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt, et);
        return true;

    }

    public void updateBatch(List<DynaDashboard> list){
        dynaDashboardFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(String dynadashboardid) {
        boolean result=dynaDashboardFeignClient.remove(dynadashboardid) ;
        return result;
    }

    public void removeBatch(Collection<String> idList){
        dynaDashboardFeignClient.removeBatch(idList);
    }

    @Override
    public DynaDashboard get(String dynadashboardid) {
		DynaDashboard et=dynaDashboardFeignClient.get(dynadashboardid);
        if(et==null){
            et=new DynaDashboard();
            et.setDynadashboardid(dynadashboardid);
        }
        else{
        }
        return  et;
    }

    @Override
    public DynaDashboard getDraft(DynaDashboard et) {
        et=dynaDashboardFeignClient.getDraft(et);
        return et;
    }

    @Override
    public boolean checkKey(DynaDashboard et) {
        return dynaDashboardFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(DynaDashboard et) {
        boolean result = true;
        Object rt = dynaDashboardFeignClient.saveEntity(et);
        if(rt == null)
          return false;
        try {
            if (rt instanceof Map) {
                ObjectMapper mapper = new ObjectMapper();
                rt = mapper.readValue(mapper.writeValueAsString(rt), DynaDashboard.class);
                if (rt != null) {
                    CachedBeanCopier.copy(rt, et);
                }
            } else if (rt instanceof Boolean) {
                result = (boolean) rt;
            }
        } catch (Exception e) {
        }
            return result;
    }

    @Override
    public void saveBatch(List<DynaDashboard> list) {
        dynaDashboardFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<DynaDashboard> searchDefault(DynaDashboardSearchContext context) {
        Page<DynaDashboard> dynaDashboards=dynaDashboardFeignClient.searchDefault(context);
        return dynaDashboards;
    }

}



