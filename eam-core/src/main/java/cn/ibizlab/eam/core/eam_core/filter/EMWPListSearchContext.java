package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
/**
 * 关系型数据实体[EMWPList] 查询条件对象
 */
@Slf4j
@Data
public class EMWPListSearchContext extends QueryWrapperContext<EMWPList> {

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Timestamp n_adate_gtandeq;//[申请日期]
	public void setN_adate_gtandeq(Timestamp n_adate_gtandeq) {
        this.n_adate_gtandeq = n_adate_gtandeq;
        if(!ObjectUtils.isEmpty(this.n_adate_gtandeq)){
            this.getSearchCond().ge("adate", n_adate_gtandeq);
        }
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Timestamp n_adate_ltandeq;//[申请日期]
	public void setN_adate_ltandeq(Timestamp n_adate_ltandeq) {
        this.n_adate_ltandeq = n_adate_ltandeq;
        if(!ObjectUtils.isEmpty(this.n_adate_ltandeq)){
            this.getSearchCond().le("adate", n_adate_ltandeq);
        }
    }
	private Integer n_m3q_eq;//[经理指定询价数]
	public void setN_m3q_eq(Integer n_m3q_eq) {
        this.n_m3q_eq = n_m3q_eq;
        if(!ObjectUtils.isEmpty(this.n_m3q_eq)){
            this.getSearchCond().eq("m3q", n_m3q_eq);
        }
    }
	private String n_emwplistname_like;//[采购申请名称]
	public void setN_emwplistname_like(String n_emwplistname_like) {
        this.n_emwplistname_like = n_emwplistname_like;
        if(!ObjectUtils.isEmpty(this.n_emwplistname_like)){
            this.getSearchCond().like("emwplistname", n_emwplistname_like);
        }
    }
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Timestamp n_createdate_gtandeq;//[建立时间]
	public void setN_createdate_gtandeq(Timestamp n_createdate_gtandeq) {
        this.n_createdate_gtandeq = n_createdate_gtandeq;
        if(!ObjectUtils.isEmpty(this.n_createdate_gtandeq)){
            this.getSearchCond().ge("createdate", n_createdate_gtandeq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emwplistid_in;//[采购申请号]
	public void setN_emwplistid_in(String n_emwplistid_in) {
        this.n_emwplistid_in = n_emwplistid_in;
        if(!ObjectUtils.isEmpty(this.n_emwplistid_in)){
			this.getSearchCond().in("emwplistid",this.n_emwplistid_in.split(";"));
        }
    }
	private Integer n_enable_eq;//[逻辑有效标志]
	public void setN_enable_eq(Integer n_enable_eq) {
        this.n_enable_eq = n_enable_eq;
        if(!ObjectUtils.isEmpty(this.n_enable_eq)){
            this.getSearchCond().eq("enable", n_enable_eq);
        }
    }
	private String n_wplisttype_eq;//[请购类型]
	public void setN_wplisttype_eq(String n_wplisttype_eq) {
        this.n_wplisttype_eq = n_wplisttype_eq;
        if(!ObjectUtils.isEmpty(this.n_wplisttype_eq)){
            this.getSearchCond().eq("wplisttype", n_wplisttype_eq);
        }
    }
	private String n_useto_eq;//[用途]
	public void setN_useto_eq(String n_useto_eq) {
        this.n_useto_eq = n_useto_eq;
        if(!ObjectUtils.isEmpty(this.n_useto_eq)){
            this.getSearchCond().eq("useto", n_useto_eq);
        }
    }
	private Integer n_wpliststate_eq;//[请购状态]
	public void setN_wpliststate_eq(Integer n_wpliststate_eq) {
        this.n_wpliststate_eq = n_wpliststate_eq;
        if(!ObjectUtils.isEmpty(this.n_wpliststate_eq)){
            this.getSearchCond().eq("wpliststate", n_wpliststate_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_wplistinfo_like;//[采购申请信息]
	public void setN_wplistinfo_like(String n_wplistinfo_like) {
        this.n_wplistinfo_like = n_wplistinfo_like;
        if(!ObjectUtils.isEmpty(this.n_wplistinfo_like)){
            this.getSearchCond().like("wplistinfo", n_wplistinfo_like);
        }
    }
	private String n_wplistcostname_eq;//[询价结果]
	public void setN_wplistcostname_eq(String n_wplistcostname_eq) {
        this.n_wplistcostname_eq = n_wplistcostname_eq;
        if(!ObjectUtils.isEmpty(this.n_wplistcostname_eq)){
            this.getSearchCond().eq("wplistcostname", n_wplistcostname_eq);
        }
    }
	private String n_wplistcostname_like;//[询价结果]
	public void setN_wplistcostname_like(String n_wplistcostname_like) {
        this.n_wplistcostname_like = n_wplistcostname_like;
        if(!ObjectUtils.isEmpty(this.n_wplistcostname_like)){
            this.getSearchCond().like("wplistcostname", n_wplistcostname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_emservicename_eq;//[服务商]
	public void setN_emservicename_eq(String n_emservicename_eq) {
        this.n_emservicename_eq = n_emservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_emservicename_eq)){
            this.getSearchCond().eq("emservicename", n_emservicename_eq);
        }
    }
	private String n_emservicename_like;//[服务商]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_teamname_eq;//[申请班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[申请班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_objname_eq;//[位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_teamid_eq;//[申请班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }
	private String n_objid_eq;//[位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_emserviceid_eq;//[服务商]
	public void setN_emserviceid_eq(String n_emserviceid_eq) {
        this.n_emserviceid_eq = n_emserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_emserviceid_eq)){
            this.getSearchCond().eq("emserviceid", n_emserviceid_eq);
        }
    }
	private String n_wplistcostid_eq;//[询价结果]
	public void setN_wplistcostid_eq(String n_wplistcostid_eq) {
        this.n_wplistcostid_eq = n_wplistcostid_eq;
        if(!ObjectUtils.isEmpty(this.n_wplistcostid_eq)){
            this.getSearchCond().eq("wplistcostid", n_wplistcostid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_aempid_eq;//[申请人]
	public void setN_aempid_eq(String n_aempid_eq) {
        this.n_aempid_eq = n_aempid_eq;
        if(!ObjectUtils.isEmpty(this.n_aempid_eq)){
            this.getSearchCond().eq("aempid", n_aempid_eq);
        }
    }
	private String n_aempname_eq;//[申请人]
	public void setN_aempname_eq(String n_aempname_eq) {
        this.n_aempname_eq = n_aempname_eq;
        if(!ObjectUtils.isEmpty(this.n_aempname_eq)){
            this.getSearchCond().eq("aempname", n_aempname_eq);
        }
    }
	private String n_aempname_like;//[申请人]
	public void setN_aempname_like(String n_aempname_like) {
        this.n_aempname_like = n_aempname_like;
        if(!ObjectUtils.isEmpty(this.n_aempname_like)){
            this.getSearchCond().like("aempname", n_aempname_like);
        }
    }
	private String n_deptid_eq;//[申请部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_deptname_eq;//[申请部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[申请部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_apprempid_eq;//[批准人]
	public void setN_apprempid_eq(String n_apprempid_eq) {
        this.n_apprempid_eq = n_apprempid_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempid_eq)){
            this.getSearchCond().eq("apprempid", n_apprempid_eq);
        }
    }
	private String n_apprempname_eq;//[批准人]
	public void setN_apprempname_eq(String n_apprempname_eq) {
        this.n_apprempname_eq = n_apprempname_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempname_eq)){
            this.getSearchCond().eq("apprempname", n_apprempname_eq);
        }
    }
	private String n_apprempname_like;//[批准人]
	public void setN_apprempname_like(String n_apprempname_like) {
        this.n_apprempname_like = n_apprempname_like;
        if(!ObjectUtils.isEmpty(this.n_apprempname_like)){
            this.getSearchCond().like("apprempname", n_apprempname_like);
        }
    }
	private String n_rempid_eq;//[采购员]
	public void setN_rempid_eq(String n_rempid_eq) {
        this.n_rempid_eq = n_rempid_eq;
        if(!ObjectUtils.isEmpty(this.n_rempid_eq)){
            this.getSearchCond().eq("rempid", n_rempid_eq);
        }
    }
	private String n_rempname_eq;//[采购员]
	public void setN_rempname_eq(String n_rempname_eq) {
        this.n_rempname_eq = n_rempname_eq;
        if(!ObjectUtils.isEmpty(this.n_rempname_eq)){
            this.getSearchCond().eq("rempname", n_rempname_eq);
        }
    }
	private String n_rempname_like;//[采购员]
	public void setN_rempname_like(String n_rempname_like) {
        this.n_rempname_like = n_rempname_like;
        if(!ObjectUtils.isEmpty(this.n_rempname_like)){
            this.getSearchCond().like("rempname", n_rempname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emwplistid", query)
            );
		 }
	}
}



