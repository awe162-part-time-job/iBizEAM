package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQType;
import cn.ibizlab.eam.core.eam_core.filter.EMEQTypeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQType] 服务对象接口
 */
public interface IEMEQTypeService extends IService<EMEQType> {

    boolean create(EMEQType et);
    void createBatch(List<EMEQType> list);
    boolean update(EMEQType et);
    void updateBatch(List<EMEQType> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQType get(String key);
    EMEQType getDraft(EMEQType et);
    boolean checkKey(EMEQType et);
    EMEQType genId(EMEQType et);
    boolean genIdBatch(List<EMEQType> etList);
    boolean save(EMEQType et);
    void saveBatch(List<EMEQType> list);
    Page<EMEQType> searchDefault(EMEQTypeSearchContext context);
    Page<EMEQType> searchEQTypeTree(EMEQTypeSearchContext context);
    Page<EMEQType> searchPeqType(EMEQTypeSearchContext context);
    List<EMEQType> selectByEqtypepid(String emeqtypeid);
    void removeByEqtypepid(String emeqtypeid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQType> getEmeqtypeByIds(List<String> ids);
    List<EMEQType> getEmeqtypeByEntities(List<EMEQType> entities);
}


