package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmp;
/**
 * 关系型数据实体[PFEmp] 查询条件对象
 */
@Slf4j
@Data
public class PFEmpSearchContext extends QueryWrapperContext<PFEmp> {

	private String n_pfempname_like;//[职员名称]
	public void setN_pfempname_like(String n_pfempname_like) {
        this.n_pfempname_like = n_pfempname_like;
        if(!ObjectUtils.isEmpty(this.n_pfempname_like)){
            this.getSearchCond().like("pfempname", n_pfempname_like);
        }
    }
	private String n_empinfo_like;//[职员信息]
	public void setN_empinfo_like(String n_empinfo_like) {
        this.n_empinfo_like = n_empinfo_like;
        if(!ObjectUtils.isEmpty(this.n_empinfo_like)){
            this.getSearchCond().like("empinfo", n_empinfo_like);
        }
    }
	private String n_empsex_eq;//[性别]
	public void setN_empsex_eq(String n_empsex_eq) {
        this.n_empsex_eq = n_empsex_eq;
        if(!ObjectUtils.isEmpty(this.n_empsex_eq)){
            this.getSearchCond().eq("empsex", n_empsex_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_majorteamname_eq;//[主班组]
	public void setN_majorteamname_eq(String n_majorteamname_eq) {
        this.n_majorteamname_eq = n_majorteamname_eq;
        if(!ObjectUtils.isEmpty(this.n_majorteamname_eq)){
            this.getSearchCond().eq("majorteamname", n_majorteamname_eq);
        }
    }
	private String n_majorteamname_like;//[主班组]
	public void setN_majorteamname_like(String n_majorteamname_like) {
        this.n_majorteamname_like = n_majorteamname_like;
        if(!ObjectUtils.isEmpty(this.n_majorteamname_like)){
            this.getSearchCond().like("majorteamname", n_majorteamname_like);
        }
    }
	private String n_majorteamid_eq;//[主班组]
	public void setN_majorteamid_eq(String n_majorteamid_eq) {
        this.n_majorteamid_eq = n_majorteamid_eq;
        if(!ObjectUtils.isEmpty(this.n_majorteamid_eq)){
            this.getSearchCond().eq("majorteamid", n_majorteamid_eq);
        }
    }
	private String n_majordeptid_eq;//[主部门]
	public void setN_majordeptid_eq(String n_majordeptid_eq) {
        this.n_majordeptid_eq = n_majordeptid_eq;
        if(!ObjectUtils.isEmpty(this.n_majordeptid_eq)){
            this.getSearchCond().eq("majordeptid", n_majordeptid_eq);
        }
    }
	private String n_majordeptname_eq;//[主部门]
	public void setN_majordeptname_eq(String n_majordeptname_eq) {
        this.n_majordeptname_eq = n_majordeptname_eq;
        if(!ObjectUtils.isEmpty(this.n_majordeptname_eq)){
            this.getSearchCond().eq("majordeptname", n_majordeptname_eq);
        }
    }
	private String n_majordeptname_like;//[主部门]
	public void setN_majordeptname_like(String n_majordeptname_like) {
        this.n_majordeptname_like = n_majordeptname_like;
        if(!ObjectUtils.isEmpty(this.n_majordeptname_like)){
            this.getSearchCond().like("majordeptname", n_majordeptname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfempname", query)
            );
		 }
	}
}



