package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[设备停机监控明细]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMEQSTOPMONI_BASE", resultMap = "EMEQSTopMoniResultMap")
@ApiModel("设备停机监控明细")
public class EMEQSTopMoni extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 组织
     */
    @DEField(preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 停机时间
     */
    @TableField(value = "stime")
    @JSONField(name = "stime")
    @JsonProperty("stime")
    @ApiModelProperty("停机时间")
    private BigDecimal stime;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 日期
     */
    @TableField(value = "sdate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "sdate", format = "yyyy-MM-dd")
    @JsonProperty("sdate")
    @ApiModelProperty("日期")
    private Timestamp sdate;
    /**
     * 原因
     */
    @TableField(value = "causation")
    @JSONField(name = "causation")
    @JsonProperty("causation")
    @ApiModelProperty("原因")
    private String causation;
    /**
     * 停机类型
     */
    @TableField(value = "stype")
    @JSONField(name = "stype")
    @JsonProperty("stype")
    @ApiModelProperty("停机类型")
    private String stype;
    /**
     * 设备停机监控明细标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emeqstopmoniid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emeqstopmoniid")
    @JsonProperty("emeqstopmoniid")
    @ApiModelProperty("设备停机监控明细标识")
    private String emeqstopmoniid;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 设备停机监控明细名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emeqstopmoniname")
    @JSONField(name = "emeqstopmoniname")
    @JsonProperty("emeqstopmoniname")
    @ApiModelProperty("设备停机监控明细名称")
    private String emeqstopmoniname;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @ApiModelProperty("设备")
    private String emequipname;
    /**
     * 设备类型
     */
    @TableField(exist = false)
    @JSONField(name = "emeqtypename")
    @JsonProperty("emeqtypename")
    @ApiModelProperty("设备类型")
    private String emeqtypename;
    /**
     * 设备停用监控
     */
    @TableField(exist = false)
    @JSONField(name = "emeqstopname")
    @JsonProperty("emeqstopname")
    @ApiModelProperty("设备停用监控")
    private String emeqstopname;
    /**
     * 设备
     */
    @TableField(value = "emequipid")
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @ApiModelProperty("设备")
    private String emequipid;
    /**
     * 设备停用监控
     */
    @TableField(value = "emeqstopid")
    @JSONField(name = "emeqstopid")
    @JsonProperty("emeqstopid")
    @ApiModelProperty("设备停用监控")
    private String emeqstopid;
    /**
     * 设备类型
     */
    @TableField(value = "emeqtypeid")
    @JSONField(name = "emeqtypeid")
    @JsonProperty("emeqtypeid")
    @ApiModelProperty("设备类型")
    private String emeqtypeid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQSTop emeqstop;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEQType emeqtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [停机时间]
     */
    public void setStime(BigDecimal stime) {
        this.stime = stime;
        this.modify("stime", stime);
    }

    /**
     * 设置 [日期]
     */
    public void setSdate(Timestamp sdate) {
        this.sdate = sdate;
        this.modify("sdate", sdate);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatSdate() {
        if (this.sdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(sdate);
    }
    /**
     * 设置 [原因]
     */
    public void setCausation(String causation) {
        this.causation = causation;
        this.modify("causation", causation);
    }

    /**
     * 设置 [停机类型]
     */
    public void setStype(String stype) {
        this.stype = stype;
        this.modify("stype", stype);
    }

    /**
     * 设置 [设备停机监控明细名称]
     */
    public void setEmeqstopmoniname(String emeqstopmoniname) {
        this.emeqstopmoniname = emeqstopmoniname;
        this.modify("emeqstopmoniname", emeqstopmoniname);
    }

    /**
     * 设置 [设备]
     */
    public void setEmequipid(String emequipid) {
        this.emequipid = emequipid;
        this.modify("emequipid", emequipid);
    }

    /**
     * 设置 [设备停用监控]
     */
    public void setEmeqstopid(String emeqstopid) {
        this.emeqstopid = emeqstopid;
        this.modify("emeqstopid", emeqstopid);
    }

    /**
     * 设置 [设备类型]
     */
    public void setEmeqtypeid(String emeqtypeid) {
        this.emeqtypeid = emeqtypeid;
        this.modify("emeqtypeid", emeqtypeid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emeqstopmoniid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


