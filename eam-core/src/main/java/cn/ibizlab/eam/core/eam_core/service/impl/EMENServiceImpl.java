package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEN;
import cn.ibizlab.eam.core.eam_core.filter.EMENSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMENService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMENMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[能源] 服务对象接口实现
 */
@Slf4j
@Service("EMENServiceImpl")
public class EMENServiceImpl extends ServiceImpl<EMENMapper, EMEN> implements IEMENService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMENConsumService emenconsumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEN et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmenid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEN> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEN et) {
        fillParentData(et);
        emobjectService.update(emenInheritMapping.toEmobject(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emenid", et.getEmenid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmenid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEN> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjectService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEN get(String key) {
        EMEN et = getById(key);
        if(et == null){
            et = new EMEN();
            et.setEmenid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEN getDraft(EMEN et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEN et) {
        return (!ObjectUtils.isEmpty(et.getEmenid())) && (!Objects.isNull(this.getById(et.getEmenid())));
    }
    @Override
    @Transactional
    public boolean save(EMEN et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEN et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEN> list) {
        list.forEach(item->fillParentData(item));
        List<EMEN> create = new ArrayList<>();
        List<EMEN> update = new ArrayList<>();
        for (EMEN et : list) {
            if (ObjectUtils.isEmpty(et.getEmenid()) || ObjectUtils.isEmpty(getById(et.getEmenid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEN> list) {
        list.forEach(item->fillParentData(item));
        List<EMEN> create = new ArrayList<>();
        List<EMEN> update = new ArrayList<>();
        for (EMEN et : list) {
            if (ObjectUtils.isEmpty(et.getEmenid()) || ObjectUtils.isEmpty(getById(et.getEmenid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEN> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMEN>().eq("itemid",emitemid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEN> searchDefault(EMENSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEN> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEN>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEN et){
        //实体关系[DER1N_EMEN_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setUnitname(item.getUnitname());
            et.setItemmtypeid(item.getItemmtypeid());
            et.setItemname(item.getEmitemname());
            et.setItemprice(item.getPrice());
            et.setItemmtypename(item.getItemmtypename());
            et.setItemtypeid(item.getItemtypeid());
            et.setItembtypeid(item.getItembtypeid());
            et.setItembtypename(item.getItembtypename());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMENInheritMapping emenInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEN et){
        if(ObjectUtils.isEmpty(et.getEmenid()))
            et.setEmenid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObject emobject =emenInheritMapping.toEmobject(et);
        emobject.set("emobjecttype","EN");
        emobjectService.create(emobject);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEN> getEmenByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEN> getEmenByEntities(List<EMEN> entities) {
        List ids =new ArrayList();
        for(EMEN entity : entities){
            Serializable id=entity.getEmenid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMENService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



