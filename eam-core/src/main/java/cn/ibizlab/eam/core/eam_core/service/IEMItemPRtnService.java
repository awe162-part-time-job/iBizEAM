package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItemPRtn;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPRtnSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItemPRtn] 服务对象接口
 */
public interface IEMItemPRtnService extends IService<EMItemPRtn> {

    boolean create(EMItemPRtn et);
    void createBatch(List<EMItemPRtn> list);
    boolean update(EMItemPRtn et);
    void updateBatch(List<EMItemPRtn> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMItemPRtn get(String key);
    EMItemPRtn getDraft(EMItemPRtn et);
    boolean checkKey(EMItemPRtn et);
    EMItemPRtn confirm(EMItemPRtn et);
    boolean confirmBatch(List<EMItemPRtn> etList);
    EMItemPRtn formUpdateByEmitempuseid(EMItemPRtn et);
    EMItemPRtn rejected(EMItemPRtn et);
    boolean save(EMItemPRtn et);
    void saveBatch(List<EMItemPRtn> list);
    Page<EMItemPRtn> searchConfirmed(EMItemPRtnSearchContext context);
    Page<EMItemPRtn> searchDefault(EMItemPRtnSearchContext context);
    Page<EMItemPRtn> searchDraft(EMItemPRtnSearchContext context);
    Page<EMItemPRtn> searchToConfirm(EMItemPRtnSearchContext context);
    List<EMItemPRtn> selectByDeptid(String emitempuseid);
    void removeByDeptid(String emitempuseid);
    List<EMItemPRtn> selectByRid(String emitempuseid);
    void removeByRid(String emitempuseid);
    List<EMItemPRtn> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMItemPRtn> selectByStorepartid(String emstorepartid);
    void removeByStorepartid(String emstorepartid);
    List<EMItemPRtn> selectByStoreid(String emstoreid);
    void removeByStoreid(String emstoreid);
    List<EMItemPRtn> selectByEmpid(String pfempid);
    void removeByEmpid(String pfempid);
    List<EMItemPRtn> selectBySempid(String pfempid);
    void removeBySempid(String pfempid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItemPRtn> getEmitemprtnByIds(List<String> ids);
    List<EMItemPRtn> getEmitemprtnByEntities(List<EMItemPRtn> entities);
}


