package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import cn.ibizlab.eam.core.eam_core.service.impl.EMWPListCostServiceImpl;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

/**
 * 实体[询价单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMWPListCostExService")
public class EMWPListCostExService extends EMWPListCostServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Confirm:确认询价] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWPListCost confirm(EMWPListCost et) {
        if (et.getEmwplistcostid() == null){
            throw new RuntimeException("未确认询价,无法完成操作");
        }
        et = this.get(et.getEmwplistcostid());
        EMWPList emwpList = emwplistService.get(et.getWplistid());
        // 待确认询价
        emwpList.setWfstep(StaticDict.EMWPLISTWFSTEP.ITEM_50.getValue());
        emwpList.setWplistcostid(et.getEmwplistcostid());
        emwpList.setEmserviceid(et.getLabserviceid());
        emwplistService.update(emwpList);
        return super.confirm(et);
    }
}

