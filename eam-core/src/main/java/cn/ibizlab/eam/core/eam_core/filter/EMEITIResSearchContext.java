package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMEITIRes;
/**
 * 关系型数据实体[EMEITIRes] 查询条件对象
 */
@Slf4j
@Data
public class EMEITIResSearchContext extends QueryWrapperContext<EMEITIRes> {

	private String n_empid_eq;//[负责人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_eicaminfo_like;//[轮胎信息]
	public void setN_eicaminfo_like(String n_eicaminfo_like) {
        this.n_eicaminfo_like = n_eicaminfo_like;
        if(!ObjectUtils.isEmpty(this.n_eicaminfo_like)){
            this.getSearchCond().like("eicaminfo", n_eicaminfo_like);
        }
    }
	private String n_emeitiresname_like;//[编码/出场号]
	public void setN_emeitiresname_like(String n_emeitiresname_like) {
        this.n_emeitiresname_like = n_emeitiresname_like;
        if(!ObjectUtils.isEmpty(this.n_emeitiresname_like)){
            this.getSearchCond().like("emeitiresname", n_emeitiresname_like);
        }
    }
	private String n_eistate_eq;//[状态]
	public void setN_eistate_eq(String n_eistate_eq) {
        this.n_eistate_eq = n_eistate_eq;
        if(!ObjectUtils.isEmpty(this.n_eistate_eq)){
            this.getSearchCond().eq("eistate", n_eistate_eq);
        }
    }
	private String n_empname_eq;//[负责人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[负责人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_itempusename_eq;//[领料单]
	public void setN_itempusename_eq(String n_itempusename_eq) {
        this.n_itempusename_eq = n_itempusename_eq;
        if(!ObjectUtils.isEmpty(this.n_itempusename_eq)){
            this.getSearchCond().eq("itempusename", n_itempusename_eq);
        }
    }
	private String n_itempusename_like;//[领料单]
	public void setN_itempusename_like(String n_itempusename_like) {
        this.n_itempusename_like = n_itempusename_like;
        if(!ObjectUtils.isEmpty(this.n_itempusename_like)){
            this.getSearchCond().like("itempusename", n_itempusename_like);
        }
    }
	private String n_eqlocationname_eq;//[位置]
	public void setN_eqlocationname_eq(String n_eqlocationname_eq) {
        this.n_eqlocationname_eq = n_eqlocationname_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_eq)){
            this.getSearchCond().eq("eqlocationname", n_eqlocationname_eq);
        }
    }
	private String n_eqlocationname_like;//[位置]
	public void setN_eqlocationname_like(String n_eqlocationname_like) {
        this.n_eqlocationname_like = n_eqlocationname_like;
        if(!ObjectUtils.isEmpty(this.n_eqlocationname_like)){
            this.getSearchCond().like("eqlocationname", n_eqlocationname_like);
        }
    }
	private String n_eqlocationid_eq;//[位置]
	public void setN_eqlocationid_eq(String n_eqlocationid_eq) {
        this.n_eqlocationid_eq = n_eqlocationid_eq;
        if(!ObjectUtils.isEmpty(this.n_eqlocationid_eq)){
            this.getSearchCond().eq("eqlocationid", n_eqlocationid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_itempuseid_eq;//[领料单]
	public void setN_itempuseid_eq(String n_itempuseid_eq) {
        this.n_itempuseid_eq = n_itempuseid_eq;
        if(!ObjectUtils.isEmpty(this.n_itempuseid_eq)){
            this.getSearchCond().eq("itempuseid", n_itempuseid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emeitiresname", query)
            );
		 }
	}
}



