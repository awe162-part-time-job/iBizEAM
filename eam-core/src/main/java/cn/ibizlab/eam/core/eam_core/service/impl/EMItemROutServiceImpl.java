package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
import cn.ibizlab.eam.core.eam_core.filter.EMItemROutSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMItemROutService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMItemROutMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[退货单] 服务对象接口实现
 */
@Slf4j
@Service("EMItemROutServiceImpl")
public class EMItemROutServiceImpl extends ServiceImpl<EMItemROutMapper, EMItemROut> implements IEMItemROutService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStoreService emstoreService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMItemROut et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemroutid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMItemROut> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMItemROut et) {
        fillParentData(et);
        emitemtradeService.update(emitemroutInheritMapping.toEmitemtrade(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emitemroutid", et.getEmitemroutid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmitemroutid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMItemROut> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emitemtradeService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMItemROut get(String key) {
        EMItemROut et = getById(key);
        if(et == null){
            et = new EMItemROut();
            et.setEmitemroutid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMItemROut getDraft(EMItemROut et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMItemROut et) {
        return (!ObjectUtils.isEmpty(et.getEmitemroutid())) && (!Objects.isNull(this.getById(et.getEmitemroutid())));
    }
    @Override
    @Transactional
    public EMItemROut confirm(EMItemROut et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean confirmBatch(List<EMItemROut> etList) {
        for(EMItemROut et : etList) {
            confirm(et);
        }
        return true;
    }

    @Override
    @Transactional
    public EMItemROut formUpdateByRID(EMItemROut et) {
         return et ;
    }

    @Override
    @Transactional
    public EMItemROut rejected(EMItemROut et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMItemROut et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMItemROut et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMItemROut> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemROut> create = new ArrayList<>();
        List<EMItemROut> update = new ArrayList<>();
        for (EMItemROut et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemroutid()) || ObjectUtils.isEmpty(getById(et.getEmitemroutid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMItemROut> list) {
        list.forEach(item->fillParentData(item));
        List<EMItemROut> create = new ArrayList<>();
        List<EMItemROut> update = new ArrayList<>();
        for (EMItemROut et : list) {
            if (ObjectUtils.isEmpty(et.getEmitemroutid()) || ObjectUtils.isEmpty(getById(et.getEmitemroutid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }

    @Override
    @Transactional
    public EMItemROut submit(EMItemROut et) {
         return et ;
    }


	@Override
    public List<EMItemROut> selectByRid(String emitemrinid) {
        return baseMapper.selectByRid(emitemrinid);
    }
    @Override
    public void removeByRid(String emitemrinid) {
        this.remove(new QueryWrapper<EMItemROut>().eq("rid",emitemrinid));
    }

	@Override
    public List<EMItemROut> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMItemROut>().eq("itemid",emitemid));
    }

	@Override
    public List<EMItemROut> selectByStorepartid(String emstorepartid) {
        return baseMapper.selectByStorepartid(emstorepartid);
    }
    @Override
    public void removeByStorepartid(String emstorepartid) {
        this.remove(new QueryWrapper<EMItemROut>().eq("storepartid",emstorepartid));
    }

	@Override
    public List<EMItemROut> selectByStoreid(String emstoreid) {
        return baseMapper.selectByStoreid(emstoreid);
    }
    @Override
    public void removeByStoreid(String emstoreid) {
        this.remove(new QueryWrapper<EMItemROut>().eq("storeid",emstoreid));
    }

	@Override
    public List<EMItemROut> selectBySempid(String pfempid) {
        return baseMapper.selectBySempid(pfempid);
    }
    @Override
    public void removeBySempid(String pfempid) {
        this.remove(new QueryWrapper<EMItemROut>().eq("sempid",pfempid));
    }


    /**
     * 查询集合 已确认
     */
    @Override
    public Page<EMItemROut> searchConfirmed(EMItemROutSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemROut> pages=baseMapper.searchConfirmed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemROut>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMItemROut> searchDefault(EMItemROutSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemROut> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemROut>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 草稿
     */
    @Override
    public Page<EMItemROut> searchDraft(EMItemROutSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemROut> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemROut>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待确认
     */
    @Override
    public Page<EMItemROut> searchToConfirm(EMItemROutSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMItemROut> pages=baseMapper.searchToConfirm(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMItemROut>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMItemROut et){
        //实体关系[DER1N_EMITEMROUT_EMITEMRIN_RID]
        if(!ObjectUtils.isEmpty(et.getRid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItemRIn r=et.getR();
            if(ObjectUtils.isEmpty(r)){
                cn.ibizlab.eam.core.eam_core.domain.EMItemRIn majorEntity=emitemrinService.get(et.getRid());
                et.setR(majorEntity);
                r=majorEntity;
            }
            et.setTeamid(r.getTeamid());
            et.setLabserviceid(r.getLabserviceid());
            et.setCivo(r.getCivo());
            et.setRname(r.getItemrininfo());
        }
        //实体关系[DER1N_EMITEMROUT_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
        }
        //实体关系[DER1N_EMITEMROUT_EMSTOREPART_STOREPARTID]
        if(!ObjectUtils.isEmpty(et.getStorepartid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStorePart storepart=et.getStorepart();
            if(ObjectUtils.isEmpty(storepart)){
                cn.ibizlab.eam.core.eam_core.domain.EMStorePart majorEntity=emstorepartService.get(et.getStorepartid());
                et.setStorepart(majorEntity);
                storepart=majorEntity;
            }
            et.setStorepartname(storepart.getEmstorepartname());
        }
        //实体关系[DER1N_EMITEMROUT_EMSTORE_STOREID]
        if(!ObjectUtils.isEmpty(et.getStoreid())){
            cn.ibizlab.eam.core.eam_core.domain.EMStore store=et.getStore();
            if(ObjectUtils.isEmpty(store)){
                cn.ibizlab.eam.core.eam_core.domain.EMStore majorEntity=emstoreService.get(et.getStoreid());
                et.setStore(majorEntity);
                store=majorEntity;
            }
            et.setStorename(store.getEmstorename());
        }
        //实体关系[DER1N_EMITEMROUT_PFEMP_SEMPID]
        if(!ObjectUtils.isEmpty(et.getSempid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp spfempid=et.getSpfempid();
            if(ObjectUtils.isEmpty(spfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getSempid());
                et.setSpfempid(majorEntity);
                spfempid=majorEntity;
            }
            et.setSempname(spfempid.getEmpinfo());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMItemROutInheritMapping emitemroutInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMItemROut et){
        if(ObjectUtils.isEmpty(et.getEmitemroutid()))
            et.setEmitemroutid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMItemTrade emitemtrade =emitemroutInheritMapping.toEmitemtrade(et);
        emitemtrade.set("emitemtradetype","ROUT");
        emitemtradeService.create(emitemtrade);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMItemROut> getEmitemroutByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMItemROut> getEmitemroutByEntities(List<EMItemROut> entities) {
        List ids =new ArrayList();
        for(EMItemROut entity : entities){
            Serializable id=entity.getEmitemroutid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMItemROutService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



