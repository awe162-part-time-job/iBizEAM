package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMResItem;
import cn.ibizlab.eam.core.eam_core.filter.EMResItemSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMResItemService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMResItemMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[物品资源] 服务对象接口实现
 */
@Slf4j
@Service("EMResItemServiceImpl")
public class EMResItemServiceImpl extends ServiceImpl<EMResItemMapper, EMResItem> implements IEMResItemService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMResItem et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresitemid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMResItem> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMResItem et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emresitemid", et.getEmresitemid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmresitemid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMResItem> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMResItem get(String key) {
        EMResItem et = getById(key);
        if(et == null){
            et = new EMResItem();
            et.setEmresitemid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMResItem getDraft(EMResItem et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMResItem et) {
        return (!ObjectUtils.isEmpty(et.getEmresitemid())) && (!Objects.isNull(this.getById(et.getEmresitemid())));
    }
    @Override
    @Transactional
    public boolean save(EMResItem et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMResItem et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMResItem> list) {
        list.forEach(item->fillParentData(item));
        List<EMResItem> create = new ArrayList<>();
        List<EMResItem> update = new ArrayList<>();
        for (EMResItem et : list) {
            if (ObjectUtils.isEmpty(et.getEmresitemid()) || ObjectUtils.isEmpty(getById(et.getEmresitemid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMResItem> list) {
        list.forEach(item->fillParentData(item));
        List<EMResItem> create = new ArrayList<>();
        List<EMResItem> update = new ArrayList<>();
        for (EMResItem et : list) {
            if (ObjectUtils.isEmpty(et.getEmresitemid()) || ObjectUtils.isEmpty(getById(et.getEmresitemid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMResItem> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMResItem>().eq("equipid",emequipid));
    }

	@Override
    public List<EMResItem> selectByResid(String emitemid) {
        return baseMapper.selectByResid(emitemid);
    }
    @Override
    public void removeByResid(String emitemid) {
        this.remove(new QueryWrapper<EMResItem>().eq("resid",emitemid));
    }

	@Override
    public List<EMResItem> selectByResrefobjid(String emresrefobjid) {
        return baseMapper.selectByResrefobjid(emresrefobjid);
    }
    @Override
    public void removeByResrefobjid(String emresrefobjid) {
        this.remove(new QueryWrapper<EMResItem>().eq("resrefobjid",emresrefobjid));
    }


    /**
     * 查询集合 设备-物品消耗金额
     */
    @Override
    public Page<Map> searchAmountByTypeByEQ(EMResItemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map> pages=baseMapper.searchAmountByTypeByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Map>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMResItem> searchDefault(EMResItemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResItem> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResItem>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 物品最新使用记录TOP10
     */
    @Override
    public Page<EMResItem> searchUsedByEQ(EMResItemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResItem> pages=baseMapper.searchUsedByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResItem>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 物品最新使用记录TOP10
     */
    @Override
    public Page<EMResItem> searchUsedByItem(EMResItemSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMResItem> pages=baseMapper.searchUsedByItem(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMResItem>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMResItem et){
        //实体关系[DER1N_EMRESITEM_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setSname(equip.getSname());
            et.setEquipcode(equip.getEquipcode());
            et.setEquipname(equip.getEquipinfo());
            et.setTeamname(equip.getRteamname());
            et.setEqenable(equip.getEnable());
        }
        //实体关系[DER1N_EMRESITEM_EMITEM_RESID]
        if(!ObjectUtils.isEmpty(et.getResid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem res=et.getRes();
            if(ObjectUtils.isEmpty(res)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getResid());
                et.setRes(majorEntity);
                res=majorEntity;
            }
            et.setItembtypename(res.getItembtypename());
            et.setItembtypeid(res.getItembtypeid());
            et.setResnameShow(res.getEmitemname());
            et.setItemmtypename(res.getItemmtypename());
            et.setUnitname(res.getUnitname());
            et.setItemmtypeid(res.getItemmtypeid());
            et.setResname(res.getEmitemname());
            et.setItemtypeid(res.getItemtypeid());
        }
        //实体关系[DER1N_EMRESITEM_EMRESREFOBJ_RESREFOBJID]
        if(!ObjectUtils.isEmpty(et.getResrefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMResRefObj resrefobj=et.getResrefobj();
            if(ObjectUtils.isEmpty(resrefobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMResRefObj majorEntity=emresrefobjService.get(et.getResrefobjid());
                et.setResrefobj(majorEntity);
                resrefobj=majorEntity;
            }
            et.setResrefobjname(resrefobj.getEmresrefobjname());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMResItem> getEmresitemByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMResItem> getEmresitemByEntities(List<EMResItem> entities) {
        List ids =new ArrayList();
        for(EMResItem entity : entities){
            Serializable id=entity.getEmresitemid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMResItemService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



