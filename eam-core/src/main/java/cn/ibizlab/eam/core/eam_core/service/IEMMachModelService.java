package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMMachModel;
import cn.ibizlab.eam.core.eam_core.filter.EMMachModelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMMachModel] 服务对象接口
 */
public interface IEMMachModelService extends IService<EMMachModel> {

    boolean create(EMMachModel et);
    void createBatch(List<EMMachModel> list);
    boolean update(EMMachModel et);
    void updateBatch(List<EMMachModel> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMMachModel get(String key);
    EMMachModel getDraft(EMMachModel et);
    boolean checkKey(EMMachModel et);
    boolean save(EMMachModel et);
    void saveBatch(List<EMMachModel> list);
    Page<EMMachModel> searchDefault(EMMachModelSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMMachModel> getEmmachmodelByIds(List<String> ids);
    List<EMMachModel> getEmmachmodelByEntities(List<EMMachModel> entities);
}


