package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryHist;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatteryHistSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEIBatteryHist] 服务对象接口
 */
public interface IEMEIBatteryHistService extends IService<EMEIBatteryHist> {

    boolean create(EMEIBatteryHist et);
    void createBatch(List<EMEIBatteryHist> list);
    boolean update(EMEIBatteryHist et);
    void updateBatch(List<EMEIBatteryHist> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEIBatteryHist get(String key);
    EMEIBatteryHist getDraft(EMEIBatteryHist et);
    boolean checkKey(EMEIBatteryHist et);
    boolean save(EMEIBatteryHist et);
    void saveBatch(List<EMEIBatteryHist> list);
    Page<EMEIBatteryHist> searchDefault(EMEIBatteryHistSearchContext context);
    List<EMEIBatteryHist> selectByEiobjid(String emeibatteryid);
    void removeByEiobjid(String emeibatteryid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEIBatteryHist> getEmeibatteryhistByIds(List<String> ids);
    List<EMEIBatteryHist> getEmeibatteryhistByEntities(List<EMEIBatteryHist> entities);
}


