package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[订单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMPO_BASE", resultMap = "EMPOResultMap")
@ApiModel("订单")
public class EMPO extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 合同内容
     */
    @TableField(value = "content")
    @JSONField(name = "content")
    @JsonProperty("content")
    @ApiModelProperty("合同内容")
    private String content;
    /**
     * 物品金额
     */
    @DEField(defaultValue = "0")
    @TableField(value = "poamount")
    @JSONField(name = "poamount")
    @JsonProperty("poamount")
    @ApiModelProperty("物品金额")
    private Double poamount;
    /**
     * 预计到货日期
     */
    @TableField(value = "eadate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "eadate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("eadate")
    @ApiModelProperty("预计到货日期")
    private Timestamp eadate;
    /**
     * 订单号
     */
    @DEField(isKeyField = true)
    @TableId(value = "empoid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "empoid")
    @JsonProperty("empoid")
    @ApiModelProperty("订单号")
    private String empoid;
    /**
     * 订单状态
     */
    @DEField(defaultValue = "0")
    @TableField(value = "postate")
    @JSONField(name = "postate")
    @JsonProperty("postate")
    @ApiModelProperty("订单状态")
    private Integer postate;
    /**
     * 订购日期
     */
    @TableField(value = "pdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "pdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("pdate")
    @ApiModelProperty("订购日期")
    private Timestamp pdate;
    /**
     * 货物发票
     */
    @TableField(value = "civo")
    @JSONField(name = "civo")
    @JsonProperty("civo")
    @ApiModelProperty("货物发票")
    private String civo;
    /**
     * 供应商备注
     */
    @TableField(value = "labservicedesc")
    @JSONField(name = "labservicedesc")
    @JsonProperty("labservicedesc")
    @ApiModelProperty("供应商备注")
    private String labservicedesc;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 订单名称
     */
    @DEField(defaultValue = "NAME")
    @TableField(value = "emponame")
    @JSONField(name = "emponame")
    @JsonProperty("emponame")
    @ApiModelProperty("订单名称")
    private String emponame;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 运杂费
     */
    @DEField(defaultValue = "0")
    @TableField(value = "tsfee")
    @JSONField(name = "tsfee")
    @JsonProperty("tsfee")
    @ApiModelProperty("运杂费")
    private Double tsfee;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 关税
     */
    @DEField(defaultValue = "0")
    @TableField(value = "taxfee")
    @JSONField(name = "taxfee")
    @JsonProperty("taxfee")
    @ApiModelProperty("关税")
    private Double taxfee;
    /**
     * 合同校验
     */
    @TableField(exist = false)
    @JSONField(name = "htjy")
    @JsonProperty("htjy")
    @ApiModelProperty("合同校验")
    private Integer htjy;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 批准日期
     */
    @TableField(value = "apprdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "apprdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("apprdate")
    @ApiModelProperty("批准日期")
    private Timestamp apprdate;
    /**
     * 关税发票
     */
    @TableField(value = "taxivo")
    @JSONField(name = "taxivo")
    @JsonProperty("taxivo")
    @ApiModelProperty("关税发票")
    private String taxivo;
    /**
     * 附件
     */
    @TableField(value = "att")
    @JSONField(name = "att")
    @JsonProperty("att")
    @ApiModelProperty("附件")
    private String att;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 最高单价
     */
    @TableField(value = "maxprice")
    @JSONField(name = "maxprice")
    @JsonProperty("maxprice")
    @ApiModelProperty("最高单价")
    private Double maxprice;
    /**
     * 工作流实例
     */
    @TableField(value = "wfinstanceid")
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;
    /**
     * 流程步骤
     */
    @TableField(value = "wfstep")
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @ApiModelProperty("流程步骤")
    private String wfstep;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 工作流状态
     */
    @TableField(value = "wfstate")
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 付款方式
     */
    @TableField(value = "payway")
    @JSONField(name = "payway")
    @JsonProperty("payway")
    @ApiModelProperty("付款方式")
    private String payway;
    /**
     * 订单信息
     */
    @TableField(exist = false)
    @JSONField(name = "poinfo")
    @JsonProperty("poinfo")
    @ApiModelProperty("订单信息")
    private String poinfo;
    /**
     * 运杂费发票
     */
    @TableField(value = "tsivo")
    @JSONField(name = "tsivo")
    @JsonProperty("tsivo")
    @ApiModelProperty("运杂费发票")
    private String tsivo;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicetypeid")
    @JsonProperty("labservicetypeid")
    @ApiModelProperty("产品供应商")
    private String labservicetypeid;
    /**
     * 产品供应商
     */
    @TableField(exist = false)
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @ApiModelProperty("产品供应商")
    private String labservicename;
    /**
     * 产品供应商
     */
    @TableField(value = "labserviceid")
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @ApiModelProperty("产品供应商")
    private String labserviceid;
    /**
     * 总经理
     */
    @TableField(value = "zjlempid")
    @JSONField(name = "zjlempid")
    @JsonProperty("zjlempid")
    @ApiModelProperty("总经理")
    private String zjlempid;
    /**
     * 总经理
     */
    @TableField(exist = false)
    @JSONField(name = "zjlempname")
    @JsonProperty("zjlempname")
    @ApiModelProperty("总经理")
    private String zjlempname;
    /**
     * 采购分管副总
     */
    @TableField(value = "fgempid")
    @JSONField(name = "fgempid")
    @JsonProperty("fgempid")
    @ApiModelProperty("采购分管副总")
    private String fgempid;
    /**
     * 采购分管副总
     */
    @TableField(exist = false)
    @JSONField(name = "fgempname")
    @JsonProperty("fgempname")
    @ApiModelProperty("采购分管副总")
    private String fgempname;
    /**
     * 批准人
     */
    @TableField(value = "apprempid")
    @JSONField(name = "apprempid")
    @JsonProperty("apprempid")
    @ApiModelProperty("批准人")
    private String apprempid;
    /**
     * 批准人
     */
    @TableField(exist = false)
    @JSONField(name = "apprempname")
    @JsonProperty("apprempname")
    @ApiModelProperty("批准人")
    private String apprempname;
    /**
     * 采购员
     */
    @TableField(value = "rempid")
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @ApiModelProperty("采购员")
    private String rempid;
    /**
     * 采购员
     */
    @TableField(exist = false)
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @ApiModelProperty("采购员")
    private String rempname;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMService labservice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfeapprempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfefgempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfemprempid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfezjlempid;



    /**
     * 设置 [合同内容]
     */
    public void setContent(String content) {
        this.content = content;
        this.modify("content", content);
    }

    /**
     * 设置 [物品金额]
     */
    public void setPoamount(Double poamount) {
        this.poamount = poamount;
        this.modify("poamount", poamount);
    }

    /**
     * 设置 [预计到货日期]
     */
    public void setEadate(Timestamp eadate) {
        this.eadate = eadate;
        this.modify("eadate", eadate);
    }

    /**
     * 格式化日期 [预计到货日期]
     */
    public String formatEadate() {
        if (this.eadate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(eadate);
    }
    /**
     * 设置 [订单状态]
     */
    public void setPostate(Integer postate) {
        this.postate = postate;
        this.modify("postate", postate);
    }

    /**
     * 设置 [订购日期]
     */
    public void setPdate(Timestamp pdate) {
        this.pdate = pdate;
        this.modify("pdate", pdate);
    }

    /**
     * 格式化日期 [订购日期]
     */
    public String formatPdate() {
        if (this.pdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(pdate);
    }
    /**
     * 设置 [货物发票]
     */
    public void setCivo(String civo) {
        this.civo = civo;
        this.modify("civo", civo);
    }

    /**
     * 设置 [供应商备注]
     */
    public void setLabservicedesc(String labservicedesc) {
        this.labservicedesc = labservicedesc;
        this.modify("labservicedesc", labservicedesc);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [订单名称]
     */
    public void setEmponame(String emponame) {
        this.emponame = emponame;
        this.modify("emponame", emponame);
    }

    /**
     * 设置 [运杂费]
     */
    public void setTsfee(Double tsfee) {
        this.tsfee = tsfee;
        this.modify("tsfee", tsfee);
    }

    /**
     * 设置 [关税]
     */
    public void setTaxfee(Double taxfee) {
        this.taxfee = taxfee;
        this.modify("taxfee", taxfee);
    }

    /**
     * 设置 [批准日期]
     */
    public void setApprdate(Timestamp apprdate) {
        this.apprdate = apprdate;
        this.modify("apprdate", apprdate);
    }

    /**
     * 格式化日期 [批准日期]
     */
    public String formatApprdate() {
        if (this.apprdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(apprdate);
    }
    /**
     * 设置 [关税发票]
     */
    public void setTaxivo(String taxivo) {
        this.taxivo = taxivo;
        this.modify("taxivo", taxivo);
    }

    /**
     * 设置 [附件]
     */
    public void setAtt(String att) {
        this.att = att;
        this.modify("att", att);
    }

    /**
     * 设置 [最高单价]
     */
    public void setMaxprice(Double maxprice) {
        this.maxprice = maxprice;
        this.modify("maxprice", maxprice);
    }

    /**
     * 设置 [工作流实例]
     */
    public void setWfinstanceid(String wfinstanceid) {
        this.wfinstanceid = wfinstanceid;
        this.modify("wfinstanceid", wfinstanceid);
    }

    /**
     * 设置 [流程步骤]
     */
    public void setWfstep(String wfstep) {
        this.wfstep = wfstep;
        this.modify("wfstep", wfstep);
    }

    /**
     * 设置 [工作流状态]
     */
    public void setWfstate(Integer wfstate) {
        this.wfstate = wfstate;
        this.modify("wfstate", wfstate);
    }

    /**
     * 设置 [付款方式]
     */
    public void setPayway(String payway) {
        this.payway = payway;
        this.modify("payway", payway);
    }

    /**
     * 设置 [运杂费发票]
     */
    public void setTsivo(String tsivo) {
        this.tsivo = tsivo;
        this.modify("tsivo", tsivo);
    }

    /**
     * 设置 [产品供应商]
     */
    public void setLabserviceid(String labserviceid) {
        this.labserviceid = labserviceid;
        this.modify("labserviceid", labserviceid);
    }

    /**
     * 设置 [总经理]
     */
    public void setZjlempid(String zjlempid) {
        this.zjlempid = zjlempid;
        this.modify("zjlempid", zjlempid);
    }

    /**
     * 设置 [采购分管副总]
     */
    public void setFgempid(String fgempid) {
        this.fgempid = fgempid;
        this.modify("fgempid", fgempid);
    }

    /**
     * 设置 [批准人]
     */
    public void setApprempid(String apprempid) {
        this.apprempid = apprempid;
        this.modify("apprempid", apprempid);
    }

    /**
     * 设置 [采购员]
     */
    public void setRempid(String rempid) {
        this.rempid = rempid;
        this.modify("rempid", rempid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("empoid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


