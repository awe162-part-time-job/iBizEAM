package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.service.impl.PLANSCHEDULEServiceImpl;
import cn.ibizlab.eam.core.util.helper.CronUtil;
import cn.ibizlab.eam.core.util.helper.IdWorker;
import cn.ibizlab.eam.util.client.IBZTaskFeignClient;
import cn.ibizlab.eam.util.dict.StaticDict;
import cn.ibizlab.eam.util.domain.JobsInfoDTO;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

import java.sql.Timestamp;
import java.util.*;

/**
 * 实体[计划时刻设置] 自定义服务对象
 */
@Slf4j
@Primary
@Service("PLANSCHEDULEExService")
public class PLANSCHEDULEExService extends PLANSCHEDULEServiceImpl {

    @Autowired
    private IBZTaskFeignClient ibzTaskFeignClient;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [GenTask:定时任务] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public PLANSCHEDULE genTask(PLANSCHEDULE et) {

        if (et==null){
           throw  new RuntimeException("定时任务不存在，无法完成操作");
        }
        IdWorker idWorker=new IdWorker(0,0);
        long taskId = idWorker.nextId();

        // 生成定时任务
        JobsInfoDTO jobsInfoDTO = new JobsInfoDTO();

        //TODO 定时任务时刻
        //jobsInfoDTO.setCron(et.getCron());
        if (et.getTaskid()!=null){
            jobsInfoDTO.setId(et.getTaskid());
        }else{
            jobsInfoDTO.setId(Long.toString(taskId));
        }
        if (et.getScheduletype().equals("3")){
            String monthCronExpression = CronUtil.createMonthCronExpression(et);
            jobsInfoDTO.setCron(monthCronExpression);
        }else if (et.getScheduletype().equals("4")){
            String weekCronExpression = CronUtil.createWeekCronExpression(et);
            jobsInfoDTO.setCron(weekCronExpression);
        }else if (et.getScheduletype().equals("5")){
            String dayCronExpression = CronUtil.createDayCronExpression(et);
            jobsInfoDTO.setCron(dayCronExpression);
        }else if (et.getScheduletype().equals("1")){
            String customizeCronExpression = CronUtil.createCustomizeCronExpression(et);
            jobsInfoDTO.setCron(customizeCronExpression);
        }else if (et.getScheduletype().equals("6")){
            String assignCronExpression = CronUtil.createAssignCronExpression(et);
            jobsInfoDTO.setCron(assignCronExpression);
        }
        jobsInfoDTO.setApp("eam");
        jobsInfoDTO.setHandler("EAMServiceJobHandler");
        jobsInfoDTO.setLastTime(0L);
        jobsInfoDTO.setNextTime(1L);
        jobsInfoDTO.setTimeout(10);
        jobsInfoDTO.setStatus(0);
        jobsInfoDTO.setFailRetryCount(1);

        jobsInfoDTO.setCreateTime(new Timestamp(System.currentTimeMillis()));
        jobsInfoDTO.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        // 回调执行参数
        String param = "EMPLANID=" + et.getEmplanid();
        param += "," + "Planscheduleid=" + et.getPlanscheduleid();
        jobsInfoDTO.setParam(param);

        ibzTaskFeignClient.saveJob(jobsInfoDTO);

        et.setTaskid(Long.toString(taskId));

        return super.genTask(et);
    }
}

