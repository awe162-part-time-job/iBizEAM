package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
/**
 * 关系型数据实体[EMRiggingH] 查询条件对象
 */
@Slf4j
@Data
public class EMRiggingHSearchContext extends QueryWrapperContext<EMRiggingH> {

	private String n_emrigginghname_like;//[工索具出入记录名称]
	public void setN_emrigginghname_like(String n_emrigginghname_like) {
        this.n_emrigginghname_like = n_emrigginghname_like;
        if(!ObjectUtils.isEmpty(this.n_emrigginghname_like)){
            this.getSearchCond().like("emrigginghname", n_emrigginghname_like);
        }
    }
	private String n_iostate_eq;//[出入库状态]
	public void setN_iostate_eq(String n_iostate_eq) {
        this.n_iostate_eq = n_iostate_eq;
        if(!ObjectUtils.isEmpty(this.n_iostate_eq)){
            this.getSearchCond().eq("iostate", n_iostate_eq);
        }
    }
	private String n_emriggingname_eq;//[工索具清单]
	public void setN_emriggingname_eq(String n_emriggingname_eq) {
        this.n_emriggingname_eq = n_emriggingname_eq;
        if(!ObjectUtils.isEmpty(this.n_emriggingname_eq)){
            this.getSearchCond().eq("emriggingname", n_emriggingname_eq);
        }
    }
	private String n_emriggingname_like;//[工索具清单]
	public void setN_emriggingname_like(String n_emriggingname_like) {
        this.n_emriggingname_like = n_emriggingname_like;
        if(!ObjectUtils.isEmpty(this.n_emriggingname_like)){
            this.getSearchCond().like("emriggingname", n_emriggingname_like);
        }
    }
	private String n_emitempusename_eq;//[领料单]
	public void setN_emitempusename_eq(String n_emitempusename_eq) {
        this.n_emitempusename_eq = n_emitempusename_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_eq)){
            this.getSearchCond().eq("emitempusename", n_emitempusename_eq);
        }
    }
	private String n_emitempusename_like;//[领料单]
	public void setN_emitempusename_like(String n_emitempusename_like) {
        this.n_emitempusename_like = n_emitempusename_like;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_like)){
            this.getSearchCond().like("emitempusename", n_emitempusename_like);
        }
    }
	private String n_emriggingid_eq;//[工索具清单]
	public void setN_emriggingid_eq(String n_emriggingid_eq) {
        this.n_emriggingid_eq = n_emriggingid_eq;
        if(!ObjectUtils.isEmpty(this.n_emriggingid_eq)){
            this.getSearchCond().eq("emriggingid", n_emriggingid_eq);
        }
    }
	private String n_emitempuseid_eq;//[领料单]
	public void setN_emitempuseid_eq(String n_emitempuseid_eq) {
        this.n_emitempuseid_eq = n_emitempuseid_eq;
        if(!ObjectUtils.isEmpty(this.n_emitempuseid_eq)){
            this.getSearchCond().eq("emitempuseid", n_emitempuseid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emrigginghname", query)
            );
		 }
	}
}



