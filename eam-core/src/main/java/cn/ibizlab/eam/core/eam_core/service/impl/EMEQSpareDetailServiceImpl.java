package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareDetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareDetailService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQSpareDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[备件包明细] 服务对象接口实现
 */
@Slf4j
@Service("EMEQSpareDetailServiceImpl")
public class EMEQSpareDetailServiceImpl extends ServiceImpl<EMEQSpareDetailMapper, EMEQSpareDetail> implements IEMEQSpareDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSpareService emeqspareService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQSpareDetail et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqsparedetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQSpareDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQSpareDetail et) {
        fillParentData(et);
        emobjmapService.update(emeqsparedetailInheritMapping.toEmobjmap(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqsparedetailid", et.getEmeqsparedetailid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqsparedetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQSpareDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQSpareDetail get(String key) {
        EMEQSpareDetail et = getById(key);
        if(et == null){
            et = new EMEQSpareDetail();
            et.setEmeqsparedetailid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQSpareDetail getDraft(EMEQSpareDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQSpareDetail et) {
        return (!ObjectUtils.isEmpty(et.getEmeqsparedetailid())) && (!Objects.isNull(this.getById(et.getEmeqsparedetailid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQSpareDetail et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQSpareDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQSpareDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSpareDetail> create = new ArrayList<>();
        List<EMEQSpareDetail> update = new ArrayList<>();
        for (EMEQSpareDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqsparedetailid()) || ObjectUtils.isEmpty(getById(et.getEmeqsparedetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQSpareDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSpareDetail> create = new ArrayList<>();
        List<EMEQSpareDetail> update = new ArrayList<>();
        for (EMEQSpareDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqsparedetailid()) || ObjectUtils.isEmpty(getById(et.getEmeqsparedetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQSpareDetail> selectByEqspareid(String emeqspareid) {
        return baseMapper.selectByEqspareid(emeqspareid);
    }
    @Override
    public void removeByEqspareid(String emeqspareid) {
        this.remove(new QueryWrapper<EMEQSpareDetail>().eq("eqspareid",emeqspareid));
    }

	@Override
    public List<EMEQSpareDetail> selectByItemid(String emitemid) {
        return baseMapper.selectByItemid(emitemid);
    }
    @Override
    public void removeByItemid(String emitemid) {
        this.remove(new QueryWrapper<EMEQSpareDetail>().eq("itemid",emitemid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQSpareDetail> searchDefault(EMEQSpareDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSpareDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSpareDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQSpareDetail et){
        //实体关系[DER1N_EMEQSPAREDETAIL_EMEQSPARE_EQSPAREID]
        if(!ObjectUtils.isEmpty(et.getEqspareid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQSpare eqspare=et.getEqspare();
            if(ObjectUtils.isEmpty(eqspare)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQSpare majorEntity=emeqspareService.get(et.getEqspareid());
                et.setEqspare(majorEntity);
                eqspare=majorEntity;
            }
            et.setEqsparename(eqspare.getEmeqsparename());
        }
        //实体关系[DER1N_EMEQSPAREDETAIL_EMITEM_ITEMID]
        if(!ObjectUtils.isEmpty(et.getItemid())){
            cn.ibizlab.eam.core.eam_core.domain.EMItem item=et.getItem();
            if(ObjectUtils.isEmpty(item)){
                cn.ibizlab.eam.core.eam_core.domain.EMItem majorEntity=emitemService.get(et.getItemid());
                et.setItem(majorEntity);
                item=majorEntity;
            }
            et.setItemname(item.getEmitemname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQSpareDetailInheritMapping emeqsparedetailInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQSpareDetail et){
        if(ObjectUtils.isEmpty(et.getEmeqsparedetailid()))
            et.setEmeqsparedetailid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emeqsparedetailInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","SPAREITEM");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQSpareDetail> getEmeqsparedetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQSpareDetail> getEmeqsparedetailByEntities(List<EMEQSpareDetail> entities) {
        List ids =new ArrayList();
        for(EMEQSpareDetail entity : entities){
            Serializable id=entity.getEmeqsparedetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQSpareDetailService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



