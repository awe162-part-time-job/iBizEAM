

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQType;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQTypeInheritMapping {

    @Mappings({
        @Mapping(source ="emeqtypeid",target = "emobjectid"),
        @Mapping(source ="emeqtypename",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="eqtypecode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEQType minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emeqtypeid"),
        @Mapping(source ="emobjectname" ,target = "emeqtypename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "eqtypecode"),
    })
    EMEQType toEmeqtype(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEQType> minorEntities);

    List<EMEQType> toEmeqtype(List<EMObject> majorEntities);

}


