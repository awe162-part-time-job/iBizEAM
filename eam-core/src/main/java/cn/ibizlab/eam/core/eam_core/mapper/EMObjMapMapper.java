package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import cn.ibizlab.eam.core.eam_core.filter.EMObjMapSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMObjMapMapper extends BaseMapper<EMObjMap> {

    Page<EMObjMap> searchChildLocation(IPage page, @Param("srf") EMObjMapSearchContext context, @Param("ew") Wrapper<EMObjMap> wrapper);
    Page<EMObjMap> searchDefault(IPage page, @Param("srf") EMObjMapSearchContext context, @Param("ew") Wrapper<EMObjMap> wrapper);
    Page<EMObjMap> searchIndexDER(IPage page, @Param("srf") EMObjMapSearchContext context, @Param("ew") Wrapper<EMObjMap> wrapper);
    Page<EMObjMap> searchLocationByEQ(IPage page, @Param("srf") EMObjMapSearchContext context, @Param("ew") Wrapper<EMObjMap> wrapper);
    @Override
    EMObjMap selectById(Serializable id);
    @Override
    int insert(EMObjMap entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMObjMap entity);
    @Override
    int update(@Param(Constants.ENTITY) EMObjMap entity, @Param("ew") Wrapper<EMObjMap> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMObjMap> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMObjMap> selectByObjpid(@Param("emobjectid") Serializable emobjectid);

}
