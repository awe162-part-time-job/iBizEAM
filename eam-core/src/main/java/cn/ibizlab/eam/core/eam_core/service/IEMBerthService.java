package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMBerth;
import cn.ibizlab.eam.core.eam_core.filter.EMBerthSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMBerth] 服务对象接口
 */
public interface IEMBerthService extends IService<EMBerth> {

    boolean create(EMBerth et);
    void createBatch(List<EMBerth> list);
    boolean update(EMBerth et);
    void updateBatch(List<EMBerth> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMBerth get(String key);
    EMBerth getDraft(EMBerth et);
    boolean checkKey(EMBerth et);
    boolean save(EMBerth et);
    void saveBatch(List<EMBerth> list);
    Page<EMBerth> searchDefault(EMBerthSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMBerth> getEmberthByIds(List<String> ids);
    List<EMBerth> getEmberthByEntities(List<EMBerth> entities);
}


