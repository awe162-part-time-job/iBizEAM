package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.EMServiceHist;
import cn.ibizlab.eam.core.eam_core.service.impl.EMServiceServiceImpl;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.eam.core.eam_core.domain.EMService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;

/**
 * 实体[服务商] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMServiceExService")
public class EMServiceExService extends EMServiceServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * [Confirm:审核通过] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMService confirm(EMService et) {
        //获取服务商信息
        et = this.get(et.getEmserviceid());
        if(et == null){
            throw new RuntimeException("未找到对应的服务商信息，无法完成操作");
        }
        //修改服务商工作流状态WFSTATE为已完成(2)
        et.setWfstate(Integer.parseInt(StaticDict.WFStates.ITEM_2.getValue()));
        et.setServicestate(StaticDict.EMSERVICESTATE.ITEM_20.getValue());
        et.setWfstep(null);
        Aops.getSelf(this).update(et);
        //审核通过需要插入服务商历史审批记录
        EMServiceHist emServiceHist = new EMServiceHist();
        emServiceHist.setEmservicehistname("服务商历史记录");
        BeanUtils.copyProperties(et,emServiceHist);

        emservicehistService.create(emServiceHist);
        return super.confirm(et);
    }
}

