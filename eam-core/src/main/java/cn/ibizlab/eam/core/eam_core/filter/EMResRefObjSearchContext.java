package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
/**
 * 关系型数据实体[EMResRefObj] 查询条件对象
 */
@Slf4j
@Data
public class EMResRefObjSearchContext extends QueryWrapperContext<EMResRefObj> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emresrefobjname_like;//[资源引用对象名称]
	public void setN_emresrefobjname_like(String n_emresrefobjname_like) {
        this.n_emresrefobjname_like = n_emresrefobjname_like;
        if(!ObjectUtils.isEmpty(this.n_emresrefobjname_like)){
            this.getSearchCond().like("emresrefobjname", n_emresrefobjname_like);
        }
    }
	private String n_resrefobjinfo_like;//[引用对象信息]
	public void setN_resrefobjinfo_like(String n_resrefobjinfo_like) {
        this.n_resrefobjinfo_like = n_resrefobjinfo_like;
        if(!ObjectUtils.isEmpty(this.n_resrefobjinfo_like)){
            this.getSearchCond().like("resrefobjinfo", n_resrefobjinfo_like);
        }
    }
	private String n_emresrefobjtype_eq;//[引用对象类型]
	public void setN_emresrefobjtype_eq(String n_emresrefobjtype_eq) {
        this.n_emresrefobjtype_eq = n_emresrefobjtype_eq;
        if(!ObjectUtils.isEmpty(this.n_emresrefobjtype_eq)){
            this.getSearchCond().eq("emresrefobjtype", n_emresrefobjtype_eq);
        }
    }
	private String n_resrefobjpname_eq;//[上级引用对象]
	public void setN_resrefobjpname_eq(String n_resrefobjpname_eq) {
        this.n_resrefobjpname_eq = n_resrefobjpname_eq;
        if(!ObjectUtils.isEmpty(this.n_resrefobjpname_eq)){
            this.getSearchCond().eq("resrefobjpname", n_resrefobjpname_eq);
        }
    }
	private String n_resrefobjpname_like;//[上级引用对象]
	public void setN_resrefobjpname_like(String n_resrefobjpname_like) {
        this.n_resrefobjpname_like = n_resrefobjpname_like;
        if(!ObjectUtils.isEmpty(this.n_resrefobjpname_like)){
            this.getSearchCond().like("resrefobjpname", n_resrefobjpname_like);
        }
    }
	private String n_equipname_eq;//[设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_resrefobjpid_eq;//[上级引用对象]
	public void setN_resrefobjpid_eq(String n_resrefobjpid_eq) {
        this.n_resrefobjpid_eq = n_resrefobjpid_eq;
        if(!ObjectUtils.isEmpty(this.n_resrefobjpid_eq)){
            this.getSearchCond().eq("resrefobjpid", n_resrefobjpid_eq);
        }
    }
	private String n_equipid_eq;//[设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emresrefobjname", query)
            );
		 }
	}
}



