package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_DP;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_DPSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMWO_DPMapper extends BaseMapper<EMWO_DP> {

    Page<EMWO_DP> searchCalendar(IPage page, @Param("srf") EMWO_DPSearchContext context, @Param("ew") Wrapper<EMWO_DP> wrapper);
    Page<EMWO_DP> searchConfirmed(IPage page, @Param("srf") EMWO_DPSearchContext context, @Param("ew") Wrapper<EMWO_DP> wrapper);
    Page<EMWO_DP> searchDefault(IPage page, @Param("srf") EMWO_DPSearchContext context, @Param("ew") Wrapper<EMWO_DP> wrapper);
    Page<EMWO_DP> searchDraft(IPage page, @Param("srf") EMWO_DPSearchContext context, @Param("ew") Wrapper<EMWO_DP> wrapper);
    Page<EMWO_DP> searchToConfirm(IPage page, @Param("srf") EMWO_DPSearchContext context, @Param("ew") Wrapper<EMWO_DP> wrapper);
    @Override
    EMWO_DP selectById(Serializable id);
    @Override
    int insert(EMWO_DP entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMWO_DP entity);
    @Override
    int update(@Param(Constants.ENTITY) EMWO_DP entity, @Param("ew") Wrapper<EMWO_DP> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMWO_DP> selectByAcclassid(@Param("emacclassid") Serializable emacclassid);

    List<EMWO_DP> selectByEquipid(@Param("emequipid") Serializable emequipid);

    List<EMWO_DP> selectByDpid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO_DP> selectByObjid(@Param("emobjectid") Serializable emobjectid);

    List<EMWO_DP> selectByRfoacid(@Param("emrfoacid") Serializable emrfoacid);

    List<EMWO_DP> selectByRfocaid(@Param("emrfocaid") Serializable emrfocaid);

    List<EMWO_DP> selectByRfodeid(@Param("emrfodeid") Serializable emrfodeid);

    List<EMWO_DP> selectByRfomoid(@Param("emrfomoid") Serializable emrfomoid);

    List<EMWO_DP> selectByRserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMWO_DP> selectByWooriid(@Param("emwooriid") Serializable emwooriid);

    List<EMWO_DP> selectByWopid(@Param("emwoid") Serializable emwoid);

    List<EMWO_DP> selectByRdeptid(@Param("pfdeptid") Serializable pfdeptid);

    List<EMWO_DP> selectByMpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO_DP> selectByRecvpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO_DP> selectByRempid(@Param("pfempid") Serializable pfempid);

    List<EMWO_DP> selectByWpersonid(@Param("pfempid") Serializable pfempid);

    List<EMWO_DP> selectByRteamid(@Param("pfteamid") Serializable pfteamid);

}
