package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[物品类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMITEMTYPE_BASE", resultMap = "EMItemTypeResultMap")
@ApiModel("物品类型")
public class EMItemType extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 物品类型标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emitemtypeid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emitemtypeid")
    @JsonProperty("emitemtypeid")
    @ApiModelProperty("物品类型标识")
    private String emitemtypeid;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 物品类型代码
     */
    @TableField(value = "itemtypecode")
    @JSONField(name = "itemtypecode")
    @JsonProperty("itemtypecode")
    @ApiModelProperty("物品类型代码")
    private String itemtypecode;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 物品类型名称
     */
    @TableField(value = "emitemtypename")
    @JSONField(name = "emitemtypename")
    @JsonProperty("emitemtypename")
    @ApiModelProperty("物品类型名称")
    private String emitemtypename;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 物品类型信息
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypeinfo")
    @JsonProperty("itemtypeinfo")
    @ApiModelProperty("物品类型信息")
    private String itemtypeinfo;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 上级类型代码
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypepcode")
    @JsonProperty("itemtypepcode")
    @ApiModelProperty("上级类型代码")
    private String itemtypepcode;
    /**
     * 一级类
     */
    @TableField(exist = false)
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @ApiModelProperty("一级类")
    private String itembtypename;
    /**
     * 二级类
     */
    @TableField(exist = false)
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @ApiModelProperty("二级类")
    private String itemmtypename;
    /**
     * 上级类型
     */
    @TableField(exist = false)
    @JSONField(name = "itemtypepname")
    @JsonProperty("itemtypepname")
    @ApiModelProperty("上级类型")
    private String itemtypepname;
    /**
     * 上级类型
     */
    @TableField(value = "itemtypepid")
    @JSONField(name = "itemtypepid")
    @JsonProperty("itemtypepid")
    @ApiModelProperty("上级类型")
    private String itemtypepid;
    /**
     * 二级类
     */
    @TableField(value = "itemmtypeid")
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @ApiModelProperty("二级类")
    private String itemmtypeid;
    /**
     * 一级类
     */
    @TableField(value = "itembtypeid")
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @ApiModelProperty("一级类")
    private String itembtypeid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemType itembtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemType itemmtype;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMItemType itemtypep;



    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [物品类型代码]
     */
    public void setItemtypecode(String itemtypecode) {
        this.itemtypecode = itemtypecode;
        this.modify("itemtypecode", itemtypecode);
    }

    /**
     * 设置 [物品类型名称]
     */
    public void setEmitemtypename(String emitemtypename) {
        this.emitemtypename = emitemtypename;
        this.modify("emitemtypename", emitemtypename);
    }

    /**
     * 设置 [上级类型]
     */
    public void setItemtypepid(String itemtypepid) {
        this.itemtypepid = itemtypepid;
        this.modify("itemtypepid", itemtypepid);
    }

    /**
     * 设置 [二级类]
     */
    public void setItemmtypeid(String itemmtypeid) {
        this.itemmtypeid = itemmtypeid;
        this.modify("itemmtypeid", itemmtypeid);
    }

    /**
     * 设置 [一级类]
     */
    public void setItembtypeid(String itembtypeid) {
        this.itembtypeid = itembtypeid;
        this.modify("itembtypeid", itembtypeid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emitemtypeid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


