package cn.ibizlab.eam.core.eam_core.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.eam.util.domain.EntityBase;
import cn.ibizlab.eam.util.annotation.DEField;
import cn.ibizlab.eam.util.enums.DEPredefinedFieldType;
import cn.ibizlab.eam.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.eam.util.helper.DataObject;
import cn.ibizlab.eam.util.enums.DupCheck;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.eam.util.annotation.Audit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.eam.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[各月维修成本]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SRFT_EMREPAIRCOST_BASE", resultMap = "EMRepairCostResultMap")
@ApiModel("各月维修成本")
public class EMRepairCost extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 统计年月
     */
    @TableField(value = "recorddate")
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "recorddate", format = "yyyy-MM-dd")
    @JsonProperty("recorddate")
    @ApiModelProperty("统计年月")
    private Timestamp recorddate;
    /**
     * 建立人
     */
    @DEField(preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "createman", fill = FieldFill.INSERT)
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @ApiModelProperty("建立人")
    private String createman;
    /**
     * 成本
     */
    @TableField(value = "cost")
    @JSONField(name = "cost")
    @JsonProperty("cost")
    @ApiModelProperty("成本")
    private Double cost;
    /**
     * 组织
     */
    @DEField(defaultValue = "TIP", preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "orgid")
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @ApiModelProperty("组织")
    private String orgid;
    /**
     * 更新时间
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "updatedate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "updatedate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;
    /**
     * 建立时间
     */
    @DEField(preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "createdate", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    @JSONField(name = "createdate", format = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;
    /**
     * 描述
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    @ApiModelProperty("描述")
    private String description;
    /**
     * 各月维修成本名称
     */
    @TableField(value = "emrepaircostname")
    @JSONField(name = "emrepaircostname")
    @JsonProperty("emrepaircostname")
    @ApiModelProperty("各月维修成本名称")
    private String emrepaircostname;
    /**
     * 各月维修成本标识
     */
    @DEField(isKeyField = true)
    @TableId(value = "emrepaircostid", type = IdType.ASSIGN_UUID)
    @JSONField(name = "emrepaircostid")
    @JsonProperty("emrepaircostid")
    @ApiModelProperty("各月维修成本标识")
    private String emrepaircostid;
    /**
     * 逻辑有效标志
     */
    @DEField(defaultValue = "1", preType = DEPredefinedFieldType.LOGICVALID, logicval = "1", logicdelval = "0")
    @TableLogic(value = "1", delval = "0")
    @TableField(value = "enable")
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;
    /**
     * 更新人
     */
    @DEField(preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "updateman")
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @ApiModelProperty("更新人")
    private String updateman;
    /**
     * 设备
     */
    @TableField(exist = false)
    @JSONField(name = "emequipname")
    @JsonProperty("emequipname")
    @ApiModelProperty("设备")
    private String emequipname;
    /**
     * 设备
     */
    @TableField(value = "emequipid")
    @JSONField(name = "emequipid")
    @JsonProperty("emequipid")
    @ApiModelProperty("设备")
    private String emequipid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.eam.core.eam_core.domain.EMEquip emequip;



    /**
     * 设置 [统计年月]
     */
    public void setRecorddate(Timestamp recorddate) {
        this.recorddate = recorddate;
        this.modify("recorddate", recorddate);
    }

    /**
     * 格式化日期 [统计年月]
     */
    public String formatRecorddate() {
        if (this.recorddate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(recorddate);
    }
    /**
     * 设置 [成本]
     */
    public void setCost(Double cost) {
        this.cost = cost;
        this.modify("cost", cost);
    }

    /**
     * 设置 [描述]
     */
    public void setDescription(String description) {
        this.description = description;
        this.modify("description", description);
    }

    /**
     * 设置 [各月维修成本名称]
     */
    public void setEmrepaircostname(String emrepaircostname) {
        this.emrepaircostname = emrepaircostname;
        this.modify("emrepaircostname", emrepaircostname);
    }

    /**
     * 设置 [设备]
     */
    public void setEmequipid(String emequipid) {
        this.emequipid = emequipid;
        this.modify("emequipid", emequipid);
    }


    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("emrepaircostid");
        return super.copyTo(targetEntity, bIncEmpty);
    }
}


