package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEIBatteryFill;
import cn.ibizlab.eam.core.eam_core.filter.EMEIBatteryFillSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEIBatteryFill] 服务对象接口
 */
public interface IEMEIBatteryFillService extends IService<EMEIBatteryFill> {

    boolean create(EMEIBatteryFill et);
    void createBatch(List<EMEIBatteryFill> list);
    boolean update(EMEIBatteryFill et);
    void updateBatch(List<EMEIBatteryFill> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEIBatteryFill get(String key);
    EMEIBatteryFill getDraft(EMEIBatteryFill et);
    boolean checkKey(EMEIBatteryFill et);
    boolean save(EMEIBatteryFill et);
    void saveBatch(List<EMEIBatteryFill> list);
    Page<EMEIBatteryFill> searchDefault(EMEIBatteryFillSearchContext context);
    List<EMEIBatteryFill> selectByEiobjid(String emeibatteryid);
    void removeByEiobjid(String emeibatteryid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEIBatteryFill> getEmeibatteryfillByIds(List<String> ids);
    List<EMEIBatteryFill> getEmeibatteryfillByEntities(List<EMEIBatteryFill> entities);
}


