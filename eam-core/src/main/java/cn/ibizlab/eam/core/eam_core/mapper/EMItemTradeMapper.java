package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMItemTrade;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTradeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMItemTradeMapper extends BaseMapper<EMItemTrade> {

    Page<EMItemTrade> searchDefault(IPage page, @Param("srf") EMItemTradeSearchContext context, @Param("ew") Wrapper<EMItemTrade> wrapper);
    Page<EMItemTrade> searchIndexDER(IPage page, @Param("srf") EMItemTradeSearchContext context, @Param("ew") Wrapper<EMItemTrade> wrapper);
    Page<Map> searchYearNumByItem(IPage page, @Param("srf") EMItemTradeSearchContext context, @Param("ew") Wrapper<EMItemTrade> wrapper);
    @Override
    EMItemTrade selectById(Serializable id);
    @Override
    int insert(EMItemTrade entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMItemTrade entity);
    @Override
    int update(@Param(Constants.ENTITY) EMItemTrade entity, @Param("ew") Wrapper<EMItemTrade> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMItemTrade> selectByRid(@Param("emitemtradeid") Serializable emitemtradeid);

    List<EMItemTrade> selectByItemid(@Param("emitemid") Serializable emitemid);

    List<EMItemTrade> selectByLabserviceid(@Param("emserviceid") Serializable emserviceid);

    List<EMItemTrade> selectByStorepartid(@Param("emstorepartid") Serializable emstorepartid);

    List<EMItemTrade> selectByStoreid(@Param("emstoreid") Serializable emstoreid);

    List<EMItemTrade> selectByDeptid(@Param("pfdeptid") Serializable pfdeptid);

    List<EMItemTrade> selectByAempid(@Param("pfempid") Serializable pfempid);

    List<EMItemTrade> selectBySempid(@Param("pfempid") Serializable pfempid);

    List<EMItemTrade> selectByTeamid(@Param("pfteamid") Serializable pfteamid);

}
