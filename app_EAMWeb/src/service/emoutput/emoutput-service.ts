import { Http } from '@/utils';
import { Util } from '@/utils';
import EMOutputServiceBase from './emoutput-service-base';


/**
 * 能力服务对象
 *
 * @export
 * @class EMOutputService
 * @extends {EMOutputServiceBase}
 */
export default class EMOutputService extends EMOutputServiceBase {

    /**
     * Creates an instance of  EMOutputService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}