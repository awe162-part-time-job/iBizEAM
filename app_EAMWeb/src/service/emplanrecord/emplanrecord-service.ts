import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPLANRECORDServiceBase from './emplanrecord-service-base';


/**
 * 触发记录服务对象
 *
 * @export
 * @class EMPLANRECORDService
 * @extends {EMPLANRECORDServiceBase}
 */
export default class EMPLANRECORDService extends EMPLANRECORDServiceBase {

    /**
     * Creates an instance of  EMPLANRECORDService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPLANRECORDService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}