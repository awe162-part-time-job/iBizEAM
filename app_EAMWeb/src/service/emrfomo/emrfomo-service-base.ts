import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 模式服务对象基类
 *
 * @export
 * @class EMRFOMOServiceBase
 * @extends {EntityServie}
 */
export default class EMRFOMOServiceBase extends EntityService {

    /**
     * Creates an instance of  EMRFOMOServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOMOServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMRFOMOServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emrfomo';
        this.APPDEKEY = 'emrfomoid';
        this.APPDENAME = 'emrfomos';
        this.APPDETEXT = 'emrfomoname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/select`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emrfoacs',JSON.stringify(res.data.emrfoacs?res.data.emrfoacs:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_emrfocas',JSON.stringify(res.data.emrfocas?res.data.emrfocas:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emrfomos`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emrfoacs',JSON.stringify(res.data.emrfoacs?res.data.emrfoacs:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_emrfocas',JSON.stringify(res.data.emrfocas?res.data.emrfocas:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emrfomos/${context.emrfomo}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let res:any = await Http.getInstance().delete(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}`,isloading);
            return res;
        }
            let res:any = await Http.getInstance().delete(`/emrfomos/${context.emrfomo}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emrfomos/${context.emrfomo}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emrfomo) delete tempData.emrfomo;
            if(tempData.emrfomoid) delete tempData.emrfomoid;
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/getdraft`,tempData,isloading);
            res.data.emrfomo = data.emrfomo;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emrfomo) delete tempData.emrfomo;
        if(tempData.emrfomoid) delete tempData.emrfomoid;
        let res:any = await  Http.getInstance().get(`/emrfomos/getdraft`,tempData,isloading);
        res.data.emrfomo = data.emrfomo;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/emrfomos/${context.emrfomo}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && context.emrfomo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/${context.emrfomo}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emrfomos/${context.emrfomo}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emrfodes/${context.emrfode}/emrfomos/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/emrfomos/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMRFOMOServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emrfode && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emrfodes/${context.emrfode}/emrfomos/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emrfomos/searchdefault`,tempData,isloading);
    }
}