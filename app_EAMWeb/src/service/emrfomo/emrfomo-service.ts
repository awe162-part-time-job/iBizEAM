import { Http } from '@/utils';
import { Util } from '@/utils';
import EMRFOMOServiceBase from './emrfomo-service-base';


/**
 * 模式服务对象
 *
 * @export
 * @class EMRFOMOService
 * @extends {EMRFOMOServiceBase}
 */
export default class EMRFOMOService extends EMRFOMOServiceBase {

    /**
     * Creates an instance of  EMRFOMOService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOMOService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}