import { Http } from '@/utils';
import { Util } from '@/utils';
import EMProductServiceBase from './emproduct-service-base';


/**
 * 试用品服务对象
 *
 * @export
 * @class EMProductService
 * @extends {EMProductServiceBase}
 */
export default class EMProductService extends EMProductServiceBase {

    /**
     * Creates an instance of  EMProductService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMProductService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}