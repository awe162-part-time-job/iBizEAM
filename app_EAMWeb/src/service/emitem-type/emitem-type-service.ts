import { Http } from '@/utils';
import { Util } from '@/utils';
import EMItemTypeServiceBase from './emitem-type-service-base';


/**
 * 物品类型服务对象
 *
 * @export
 * @class EMItemTypeService
 * @extends {EMItemTypeServiceBase}
 */
export default class EMItemTypeService extends EMItemTypeServiceBase {

    /**
     * Creates an instance of  EMItemTypeService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTypeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}