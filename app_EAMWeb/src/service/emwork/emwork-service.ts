import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWorkServiceBase from './emwork-service-base';


/**
 * 加班工单服务对象
 *
 * @export
 * @class EMWorkService
 * @extends {EMWorkServiceBase}
 */
export default class EMWorkService extends EMWorkServiceBase {

    /**
     * Creates an instance of  EMWorkService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWorkService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}