import { Http } from '@/utils';
import { Util } from '@/utils';
import EMOutputRctServiceBase from './emoutput-rct-service-base';


/**
 * 产能服务对象
 *
 * @export
 * @class EMOutputRctService
 * @extends {EMOutputRctServiceBase}
 */
export default class EMOutputRctService extends EMOutputRctServiceBase {

    /**
     * Creates an instance of  EMOutputRctService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputRctService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}