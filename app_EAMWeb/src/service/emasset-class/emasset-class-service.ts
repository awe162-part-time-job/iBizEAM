import { Http } from '@/utils';
import { Util } from '@/utils';
import EMAssetClassServiceBase from './emasset-class-service-base';


/**
 * 资产类别服务对象
 *
 * @export
 * @class EMAssetClassService
 * @extends {EMAssetClassServiceBase}
 */
export default class EMAssetClassService extends EMAssetClassServiceBase {

    /**
     * Creates an instance of  EMAssetClassService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClassService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}