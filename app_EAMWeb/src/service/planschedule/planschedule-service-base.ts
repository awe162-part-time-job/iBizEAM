import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 计划时刻设置服务对象基类
 *
 * @export
 * @class PLANSCHEDULEServiceBase
 * @extends {EntityServie}
 */
export default class PLANSCHEDULEServiceBase extends EntityService {

    /**
     * Creates an instance of  PLANSCHEDULEServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULEServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof PLANSCHEDULEServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='planschedule';
        this.APPDEKEY = 'planscheduleid';
        this.APPDENAME = 'planschedules';
        this.APPDETEXT = 'planschedulename';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/planschedules/${context.planschedule}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        if(context.emplan && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/planschedules`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/planschedules`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emplans/${context.emplan}/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/planschedules/${context.planschedule}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
        if(context.emplan && context.planschedule){
            let res:any = await Http.getInstance().delete(`/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            return res;
        }
            let res:any = await Http.getInstance().delete(`/planschedules/${context.planschedule}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/planschedules/${context.planschedule}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/planschedules/${context.planschedule}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.planschedule) delete tempData.planschedule;
            if(tempData.planscheduleid) delete tempData.planscheduleid;
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/planschedules/getdraft`,tempData,isloading);
            res.data.planschedule = data.planschedule;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.planschedule) delete tempData.planschedule;
        if(tempData.planscheduleid) delete tempData.planscheduleid;
        let res:any = await  Http.getInstance().get(`/planschedules/getdraft`,tempData,isloading);
        res.data.planschedule = data.planschedule;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/planschedules/${context.planschedule}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/planschedules/${context.planschedule}/checkkey`,data,isloading);
            return res;
    }

    /**
     * GenTask接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async GenTask(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/planschedules/${context.planschedule}/gentask`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/planschedules/${context.planschedule}/gentask`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emplantempl && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emacclass && context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        if(context.emplan && context.planschedule){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emplans/${context.emplan}/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/planschedules/${context.planschedule}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emplans/${context.emplan}/planschedules/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/planschedules/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplans/${context.emplan}/planschedules/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/planschedules/searchdefault`,tempData,isloading);
    }

    /**
     * FetchIndexDER接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async FetchIndexDER(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let codelistModel:any = {tag:'CODELIST_SCHEDULETYPE',codelistType:'STATIC'};
        let res:any = await this.getCodeList(codelistModel.tag,codelistModel.codelistType,context,data);
        if(res && res.length > 0){
            res.forEach((ele:any) => {
                // 仿真返回数据集
                ele.planscheduleid = ele.value;
                ele.planschedulename = ele.text
            });
        } 
        return {status:200,data:res};
    }

    /**
     * searchIndexDER接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PLANSCHEDULEServiceBase
     */
    public async searchIndexDER(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.pfteam && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emservice && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emacclass && context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.pfteam && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emservice && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emplantempl && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplantempls/${context.emplantempl}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emequip && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emacclass && context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emacclasses/${context.emacclass}/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        if(context.emplan && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emplans/${context.emplan}/planschedules/searchindexder`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/planschedules/searchindexder`,tempData,isloading);
    }
}