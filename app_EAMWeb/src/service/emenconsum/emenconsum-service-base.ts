import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 能耗服务对象基类
 *
 * @export
 * @class EMENConsumServiceBase
 * @extends {EntityServie}
 */
export default class EMENConsumServiceBase extends EntityService {

    /**
     * Creates an instance of  EMENConsumServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENConsumServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof EMENConsumServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='emenconsum';
        this.APPDEKEY = 'emenconsumid';
        this.APPDENAME = 'emenconsums';
        this.APPDETEXT = 'emenconsumname';
        this.APPNAME = 'eamweb';
        this.SYSTEMNAME = 'eam';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
        if(context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emen/${context.emen}/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emenconsums/${context.emenconsum}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emequip && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emenconsums`,data,isloading);
            
            return res;
        }
        if(context.emen && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/emen/${context.emen}/emenconsums`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/emenconsums`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        if(context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/emen/${context.emen}/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/emenconsums/${context.emenconsum}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
        if(context.emen && context.emenconsum){
            let res:any = await Http.getInstance().delete(`/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            return res;
        }
            let res:any = await Http.getInstance().delete(`/emenconsums/${context.emenconsum}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emequip && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
        if(context.emen && context.emenconsum){
            let res:any = await Http.getInstance().get(`/emen/${context.emen}/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/emenconsums/${context.emenconsum}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        if(context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            if(tempData.emenconsum) delete tempData.emenconsum;
            if(tempData.emenconsumid) delete tempData.emenconsumid;
            let res:any = await Http.getInstance().get(`/emen/${context.emen}/emenconsums/getdraft`,tempData,isloading);
            res.data.emenconsum = data.emenconsum;
            
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        if(tempData.emenconsum) delete tempData.emenconsum;
        if(tempData.emenconsumid) delete tempData.emenconsumid;
        let res:any = await  Http.getInstance().get(`/emenconsums/getdraft`,tempData,isloading);
        res.data.emenconsum = data.emenconsum;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emen/${context.emen}/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().post(`/emenconsums/${context.emenconsum}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emstore && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emservice && context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.pfteam && context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emitem && context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emequip && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emequips/${context.emequip}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        if(context.emen && context.emenconsum){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/emen/${context.emen}/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/emenconsums/${context.emenconsum}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emen/${context.emen}/emenconsums/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/emenconsums/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * searchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async searchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emstore && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emservice && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emenconsums/searchdefault`,tempData,isloading);
        }
        if(context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emen/${context.emen}/emenconsums/searchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emenconsums/searchdefault`,tempData,isloading);
    }

    /**
     * FetchEqEnByYear接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async FetchEqEnByYear(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emstore && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emservice && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emequips/${context.emequip}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        if(context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = await Http.getInstance().get(`/emen/${context.emen}/emenconsums/fetcheqenbyyear`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = await Http.getInstance().get(`/emenconsums/fetcheqenbyyear`,tempData,isloading);
        return res;
    }

    /**
     * searchEqEnByYear接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EMENConsumServiceBase
     */
    public async searchEqEnByYear(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.emstore && context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emstore && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstores/${context.emstore}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emstorepart && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emstoreparts/${context.emstorepart}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emservice && context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emservices/${context.emservice}/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.pfteam && context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/pfteams/${context.pfteam}/emequips/${context.emequip}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emitem && context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emitems/${context.emitem}/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emequip && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emequips/${context.emequip}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        if(context.emen && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return await Http.getInstance().post(`/emen/${context.emen}/emenconsums/searcheqenbyyear`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return await Http.getInstance().post(`/emenconsums/searcheqenbyyear`,tempData,isloading);
    }
}