/**
 * 设备运行日志
 *
 * @export
 * @interface EMEQWL
 */
export interface EMEQWL {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    updateman?: any;

    /**
     * 运行区间起始
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    bdate?: any;

    /**
     * 运行时间(H)
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    nval?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    description?: any;

    /**
     * 运行区间截至
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    edate?: any;

    /**
     * 设备运行日志名称
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    emeqwlname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    createdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    updatedate?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    createman?: any;

    /**
     * 设备运行日志标识
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    emeqwlid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    woname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    objname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    equipid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    woid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQWL
     */
    objid?: any;
}