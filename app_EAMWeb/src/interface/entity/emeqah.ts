/**
 * 活动历史
 *
 * @export
 * @interface EMEQAH
 */
export interface EMEQAH {

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    orgid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    updateman?: any;

    /**
     * 活动日期
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    activedate?: any;

    /**
     * 活动记录
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    activedesc?: any;

    /**
     * 活动历史名称
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    emeqahname?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    enable?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    createdate?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    regionenddate?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    eqstoplength?: any;

    /**
     * 材料费(￥)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    mfee?: any;

    /**
     * 活动前情况
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    activebdesc?: any;

    /**
     * 活动后情况
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    activeadesc?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rdeptname?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rdeptid?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    activelengths?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    updatedate?: any;

    /**
     * 活动历史标识
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    emeqahid?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    prefee?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rempname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    description?: any;

    /**
     * 起始时间
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    regionbegindate?: any;

    /**
     * 服务费(￥)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    sfee?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    content?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    createman?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rempid?: any;

    /**
     * 人工费(￥)
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    pfee?: any;

    /**
     * 分组类型
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    emeqahtype?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    acclassname?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfoacname?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfomoname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    woname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfodename?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfocaname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rservicename?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rteamname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    equipname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    objname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfodeid?: any;

    /**
     * 原因
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfocaid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rteamid?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    acclassid?: any;

    /**
     * 方案
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfoacid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rfomoid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    equipid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    woid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMEQAH
     */
    rserviceid?: any;
}