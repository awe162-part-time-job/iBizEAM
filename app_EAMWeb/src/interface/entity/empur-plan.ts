/**
 * 计划修理
 *
 * @export
 * @interface EMPurPlan
 */
export interface EMPurPlan {

    /**
     * 采购起始时间
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    yearfrom?: any;

    /**
     * 评估报告
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    assessreport?: any;

    /**
     * 采购计划项目
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    empurplanname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    updateman?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    wfinstanceid?: any;

    /**
     * 采购年度
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    years?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    msitemtype?: any;

    /**
     * 采购计划标识
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    empurplanid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    createman?: any;

    /**
     * 计划修理状态
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    planstate?: any;

    /**
     * 验收时间
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    acceptancedate?: any;

    /**
     * 数量完成比
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    cocnt?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    enable?: any;

    /**
     * 采购结束时间
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    yearto?: any;

    /**
     * 剩余金额额度
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    nowamount?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    updatedate?: any;

    /**
     * 服务商编号
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    servicecode?: any;

    /**
     * 验收结果
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    acceptanceresult?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    wfstate?: any;

    /**
     * 采购金额
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    puramount?: any;

    /**
     * 跟踪规则
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    trackrule?: any;

    /**
     * 经理指定询价数
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    m3q?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    description?: any;

    /**
     * 剩余数量额度
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    nowcnt?: any;

    /**
     * 金额完成比
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    coamount?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    orgid?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    wfstep?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    createdate?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    rempid?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    rempname?: any;

    /**
     * 是否跟踪结束
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    istrackok?: any;

    /**
     * 合同扫描件
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    contractscan?: any;

    /**
     * 采购数量
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    pursum?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    unitname?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    itemtypename?: any;

    /**
     * 计划修理
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    embidinquiryname?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    itemtypeid?: any;

    /**
     * 计划修理
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    embidinquiryid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMPurPlan
     */
    unitid?: any;
}