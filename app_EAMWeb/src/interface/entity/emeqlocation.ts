/**
 * 位置
 *
 * @export
 * @interface EMEQLocation
 */
export interface EMEQLocation {

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    createman?: any;

    /**
     * 位置信息
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    eqlocationinfo?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    enable?: any;

    /**
     * 位置名称
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    emeqlocationname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    description?: any;

    /**
     * 位置标识
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    emeqlocationid?: any;

    /**
     * 位置代码
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    eqlocationcode?: any;

    /**
     * 位置类型
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    eqlocationtype?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    updatedate?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    majorequipname?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMEQLocation
     */
    majorequipid?: any;
}