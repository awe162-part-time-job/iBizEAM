/**
 * 物品资源
 *
 * @export
 * @interface EMResItem
 */
export interface EMResItem {

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMResItem
     */
    price?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMResItem
     */
    amount?: any;

    /**
     * 物品资源名称
     *
     * @returns {*}
     * @memberof EMResItem
     */
    emresitemname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMResItem
     */
    description?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMResItem
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMResItem
     */
    updatedate?: any;

    /**
     * 安排用量
     *
     * @returns {*}
     * @memberof EMResItem
     */
    pnum?: any;

    /**
     * 开始时间
     *
     * @returns {*}
     * @memberof EMResItem
     */
    bdate?: any;

    /**
     * 实际用量
     *
     * @returns {*}
     * @memberof EMResItem
     */
    snum?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMResItem
     */
    createman?: any;

    /**
     * 物品资源标识
     *
     * @returns {*}
     * @memberof EMResItem
     */
    emresitemid?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMResItem
     */
    orgid?: any;

    /**
     * 结束时间
     *
     * @returns {*}
     * @memberof EMResItem
     */
    edate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMResItem
     */
    enable?: any;

    /**
     * 数据来源
     *
     * @returns {*}
     * @memberof EMResItem
     */
    datafrom?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMResItem
     */
    createdate?: any;

    /**
     * 引用对象
     *
     * @returns {*}
     * @memberof EMResItem
     */
    resrefobjname?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMResItem
     */
    itembtypename?: any;

    /**
     * 设备统计
     *
     * @returns {*}
     * @memberof EMResItem
     */
    sname?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMResItem
     */
    equipcode?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMResItem
     */
    itembtypeid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMResItem
     */
    resname_show?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMResItem
     */
    itemmtypename?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMResItem
     */
    unitname?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMResItem
     */
    itemmtypeid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMResItem
     */
    equipname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMResItem
     */
    resname?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMResItem
     */
    itemtypeid?: any;

    /**
     * 班组
     *
     * @returns {*}
     * @memberof EMResItem
     */
    teamname?: any;

    /**
     * 设备逻辑有效
     *
     * @returns {*}
     * @memberof EMResItem
     */
    eqenable?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMResItem
     */
    resid?: any;

    /**
     * 引用对象
     *
     * @returns {*}
     * @memberof EMResItem
     */
    resrefobjid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMResItem
     */
    equipid?: any;
}