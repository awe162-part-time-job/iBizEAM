/**
 * 计划_按月
 *
 * @export
 * @interface PLANSCHEDULE_M
 */
export interface PLANSCHEDULE_M {

    /**
     * 计划_按月标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    planschedule_mid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    createman?: any;

    /**
     * 计划_按月名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    planschedule_mname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    updatedate?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    emplanid?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    cyclestarttime?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    intervalminute?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    scheduleparam?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    scheduletype?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    cycleendtime?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    emplanname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    description?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    schedulestate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    scheduleparam4?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    scheduleparam2?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    runtime?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    rundate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    scheduleparam3?: any;

    /**
     * 星期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    nocode?: any;

    /**
     * 第
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    noseq?: any;

    /**
     * 第
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    noseq2?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    lastminute?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_M
     */
    taskid?: any;
}