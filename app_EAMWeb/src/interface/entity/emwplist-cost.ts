/**
 * 询价单
 *
 * @export
 * @interface EMWPListCost
 */
export interface EMWPListCost {

    /**
     * 询价信息
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    wplistcostinfo?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    createdate?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    price?: any;

    /**
     * 询价人
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    rempname?: any;

    /**
     * 询价人
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    rempid?: any;

    /**
     * 询价时间
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    adate?: any;

    /**
     * 询价单名称
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    emwplistcostname?: any;

    /**
     * 折扣(%)
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    discnt?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    updatedate?: any;

    /**
     * 物品备注
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    itemdesc?: any;

    /**
     * 单位转换率
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    unitrate?: any;

    /**
     * 有效期起始
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    begindate?: any;

    /**
     * 标价
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    listprice?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    items?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    orgid?: any;

    /**
     * 价格条件
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    pricecdt?: any;

    /**
     * 整单位购买
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    intunitflag?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    updateman?: any;

    /**
     * 询价单标识
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    emwplistcostid?: any;

    /**
     * 采购试算(单价排序)
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    wplistcosteval?: any;

    /**
     * 询价单位备注
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    unitdesc?: any;

    /**
     * 有效期截至
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    enddate?: any;

    /**
     * -
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    wplistcostresult?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    enable?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    taxrate?: any;

    /**
     * 标准单位单价
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    sunitprice?: any;

    /**
     * 标准单位
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    sunitid?: any;

    /**
     * 询价单位
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    unitname?: any;

    /**
     * 标准单位
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    sunitname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    itemname?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    labservicename?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    avgprice?: any;

    /**
     * 采购申请号
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    wplistid?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    labserviceid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    itemid?: any;

    /**
     * 询价单位
     *
     * @returns {*}
     * @memberof EMWPListCost
     */
    unitid?: any;
}