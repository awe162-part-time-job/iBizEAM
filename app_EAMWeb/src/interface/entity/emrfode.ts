/**
 * 现象
 *
 * @export
 * @interface EMRFODE
 */
export interface EMRFODE {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    description?: any;

    /**
     * 现象代码
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    rfodecode?: any;

    /**
     * 对象编号
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    objid?: any;

    /**
     * 现象信息
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    rfodeinfo?: any;

    /**
     * 现象标识
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    emrfodeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    createdate?: any;

    /**
     * 现象名称
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    emrfodename?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    enable?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFODE
     */
    updateman?: any;
}