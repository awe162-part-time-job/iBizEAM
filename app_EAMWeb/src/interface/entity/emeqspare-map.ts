/**
 * 备件包引用
 *
 * @export
 * @interface EMEQSpareMap
 */
export interface EMEQSpareMap {

    /**
     * 备件包引用标识
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    emeqsparemapid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    orgid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    description?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    updateman?: any;

    /**
     * 备件包引用
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    emeqsparemapname?: any;

    /**
     * 引用对象
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    refobjname?: any;

    /**
     * 备件包
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    eqsparename?: any;

    /**
     * 对象标识
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    refobjid?: any;

    /**
     * 备件包
     *
     * @returns {*}
     * @memberof EMEQSpareMap
     */
    eqspareid?: any;
}