/**
 * 测点记录
 *
 * @export
 * @interface EMDPRCT
 */
export interface EMDPRCT {

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    createdate?: any;

    /**
     * 测点记录类型
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    emdprcttype?: any;

    /**
     * 测点记录名称
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    emdprctname?: any;

    /**
     * 区间起始
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    bdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    updateman?: any;

    /**
     * 区间截至
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    edate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    updatedate?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    val?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    orgid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    description?: any;

    /**
     * 数值
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    nval?: any;

    /**
     * 测点记录标识
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    emdprctid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    createman?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    equipname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    woname?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    dpname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    objname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    woid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    equipid?: any;

    /**
     * 测点
     *
     * @returns {*}
     * @memberof EMDPRCT
     */
    dpid?: any;
}