import EMWPList_en_US_Base from './emwplist_en_US_base';

function getLocaleResource(){
    const EMWPList_en_US_OwnData = {};
    const targetData = Object.assign(EMWPList_en_US_Base(), EMWPList_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
