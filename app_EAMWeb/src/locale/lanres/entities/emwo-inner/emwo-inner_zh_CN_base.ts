import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("内部工单", null),
		fields: {
			content: commonLogic.appcommonhandle("详细内容",null),
			emwotype: commonLogic.appcommonhandle("工单分组",null),
			emwo_innerid: commonLogic.appcommonhandle("工单编号",null),
			worklength: commonLogic.appcommonhandle("实际工时(分)",null),
			wogroup: commonLogic.appcommonhandle("工单分组",null),
			wotype: commonLogic.appcommonhandle("工单类型",null),
			waitbuy: commonLogic.appcommonhandle("等待配件时(分)",null),
			wostate: commonLogic.appcommonhandle("工单状态",null),
			val: commonLogic.appcommonhandle("值",null),
			waitmodi: commonLogic.appcommonhandle("等待修理时(分)",null),
			expiredate: commonLogic.appcommonhandle("过期日期",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			closeflag: commonLogic.appcommonhandle("是否直接关闭工单",null),
			mdate: commonLogic.appcommonhandle("制定时间",null),
			wodesc: commonLogic.appcommonhandle("工单内容",null),
			wfstate: commonLogic.appcommonhandle("工作流状态",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			priority: commonLogic.appcommonhandle("优先级",null),
			yxcb: commonLogic.appcommonhandle("影响船舶",null),
			nval: commonLogic.appcommonhandle("数值",null),
			activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
			emwo_innername: commonLogic.appcommonhandle("工单名称",null),
			wodate: commonLogic.appcommonhandle("执行日期",null),
			archive: commonLogic.appcommonhandle("归档",null),
			regionenddate: commonLogic.appcommonhandle("结束时间",null),
			cplanflag: commonLogic.appcommonhandle("转计划标志",null),
			woteam: commonLogic.appcommonhandle("工单组",null),
			wfstep: commonLogic.appcommonhandle("流程步骤",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			description: commonLogic.appcommonhandle("描述",null),
			prefee: commonLogic.appcommonhandle("预算(￥)",null),
			regionbegindate: commonLogic.appcommonhandle("起始时间",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			vrate: commonLogic.appcommonhandle("倍率",null),
			eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
			wfinstanceid: commonLogic.appcommonhandle("工作流实例",null),
			wresult: commonLogic.appcommonhandle("执行结果",null),
			mfee: commonLogic.appcommonhandle("约合材料费(￥)",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			rfodename: commonLogic.appcommonhandle("现象",null),
			rfoacname: commonLogic.appcommonhandle("方案",null),
			rfomoname: commonLogic.appcommonhandle("模式",null),
			rteamname: commonLogic.appcommonhandle("责任班组",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			emeqlctgssname: commonLogic.appcommonhandle("钢丝绳位置",null),
			rservicename: commonLogic.appcommonhandle("服务商",null),
			wopname: commonLogic.appcommonhandle("上级工单",null),
			acclassname: commonLogic.appcommonhandle("总帐科目",null),
			emeitiresname: commonLogic.appcommonhandle("轮胎清单",null),
			objname: commonLogic.appcommonhandle("位置",null),
			wooriname: commonLogic.appcommonhandle("工单来源",null),
			rfocaname: commonLogic.appcommonhandle("原因",null),
			dptype: commonLogic.appcommonhandle("测点类型",null),
			emeqlcttiresname: commonLogic.appcommonhandle("轮胎位置",null),
			wooritype: commonLogic.appcommonhandle("来源类型",null),
			objid: commonLogic.appcommonhandle("位置",null),
			wooriid: commonLogic.appcommonhandle("工单来源",null),
			emeqlctgssid: commonLogic.appcommonhandle("钢丝绳位置",null),
			emeitiresid: commonLogic.appcommonhandle("轮胎清单",null),
			acclassid: commonLogic.appcommonhandle("总帐科目",null),
			rfoacid: commonLogic.appcommonhandle("方案",null),
			emeqlcttiresid: commonLogic.appcommonhandle("轮胎位置",null),
			rfomoid: commonLogic.appcommonhandle("模式",null),
			dpid: commonLogic.appcommonhandle("测点",null),
			equipid: commonLogic.appcommonhandle("设备",null),
			rfocaid: commonLogic.appcommonhandle("原因",null),
			wopid: commonLogic.appcommonhandle("上级工单",null),
			rfodeid: commonLogic.appcommonhandle("现象",null),
			rteamid: commonLogic.appcommonhandle("责任班组",null),
			rserviceid: commonLogic.appcommonhandle("服务商",null),
			rdeptid: commonLogic.appcommonhandle("责任部门",null),
			rdeptname: commonLogic.appcommonhandle("责任部门",null),
			mpersonid: commonLogic.appcommonhandle("制定人",null),
			mpersonname: commonLogic.appcommonhandle("制定人",null),
			rempid: commonLogic.appcommonhandle("责任人",null),
			rempname: commonLogic.appcommonhandle("责任人",null),
			recvpersonid: commonLogic.appcommonhandle("接收人",null),
			recvpersonname: commonLogic.appcommonhandle("接收人",null),
			wpersonid: commonLogic.appcommonhandle("执行人",null),
			wpersonname: commonLogic.appcommonhandle("执行人",null),
		},
			views: {
				confirmedgridview: {
					caption: commonLogic.appcommonhandle("内部工单-已完成",null),
					title: commonLogic.appcommonhandle("内部工单表格视图",null),
				},
				wovieweditview: {
					caption: commonLogic.appcommonhandle("内部工单",null),
					title: commonLogic.appcommonhandle("内部工单",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("内部工单",null),
					title: commonLogic.appcommonhandle("内部工单",null),
				},
				tabexpview: {
					caption: commonLogic.appcommonhandle("内部工单",null),
					title: commonLogic.appcommonhandle("内部工单分页导航视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("内部工单",null),
					title: commonLogic.appcommonhandle("内部工单",null),
				},
				toconfirmgridview: {
					caption: commonLogic.appcommonhandle("内部工单-执行中",null),
					title: commonLogic.appcommonhandle("内部工单表格视图",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("内部工单",null),
					title: commonLogic.appcommonhandle("内部工单",null),
				},
				draftgridview: {
					caption: commonLogic.appcommonhandle("内部工单-草稿",null),
					title: commonLogic.appcommonhandle("内部工单表格视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("工单信息",null), 
					grouppanel18: commonLogic.appcommonhandle("执行信息",null), 
					grouppanel30: commonLogic.appcommonhandle("责任信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("工单编号",null), 
					srfmajortext: commonLogic.appcommonhandle("工单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					emwo_innername: commonLogic.appcommonhandle("工单名称",null), 
					emwo_innerid: commonLogic.appcommonhandle("工单编号(自动)",null), 
					wogroup: commonLogic.appcommonhandle("工单分组",null), 
					activelengths: commonLogic.appcommonhandle("安排工时(H)",null), 
					wotype: commonLogic.appcommonhandle("工单类型",null), 
					emeqlcttiresname: commonLogic.appcommonhandle("轮胎位置",null), 
					closeflag: commonLogic.appcommonhandle("直接完成",null), 
					emeqlctgssname: commonLogic.appcommonhandle("钢丝绳位置",null), 
					emeitiresname: commonLogic.appcommonhandle("轮胎清单",null), 
					priority: commonLogic.appcommonhandle("优先级",null), 
					yxcb: commonLogic.appcommonhandle("影响船舶",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					wodesc: commonLogic.appcommonhandle("工单内容",null), 
					wodate: commonLogic.appcommonhandle("安排执行日期",null), 
					regionbegindate: commonLogic.appcommonhandle("起始时间",null), 
					regionenddate: commonLogic.appcommonhandle("结束时间",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					waitmodi: commonLogic.appcommonhandle("等待修理时(分)",null), 
					waitbuy: commonLogic.appcommonhandle("等待配件时(分)",null), 
					worklength: commonLogic.appcommonhandle("实际工时(分)",null), 
					wresult: commonLogic.appcommonhandle("执行结果",null), 
					mfee: commonLogic.appcommonhandle("约合材料费(￥)",null), 
					cplanflag: commonLogic.appcommonhandle("转计划标志",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					mpersonid: commonLogic.appcommonhandle("制定人",null), 
					mpersonname: commonLogic.appcommonhandle("制定人",null), 
					wpersonid: commonLogic.appcommonhandle("执行人",null), 
					wpersonname: commonLogic.appcommonhandle("执行人",null), 
					rdeptid: commonLogic.appcommonhandle("责任部门",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("工单信息",null), 
					grouppanel18: commonLogic.appcommonhandle("执行信息",null), 
					grouppanel30: commonLogic.appcommonhandle("责任信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					grouppanel37: commonLogic.appcommonhandle("故障信息",null), 
					grouppanel42: commonLogic.appcommonhandle("高级信息",null), 
					grouppanel49: commonLogic.appcommonhandle("操作信息",null), 
					formpage36: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("工单编号",null), 
					srfmajortext: commonLogic.appcommonhandle("工单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					emwo_innername: commonLogic.appcommonhandle("工单名称",null), 
					emwo_innerid: commonLogic.appcommonhandle("工单编号(自动)",null), 
					wogroup: commonLogic.appcommonhandle("工单分组",null), 
					activelengths: commonLogic.appcommonhandle("安排工时(H)",null), 
					wotype: commonLogic.appcommonhandle("工单类型",null), 
					emeqlcttiresname: commonLogic.appcommonhandle("轮胎位置",null), 
					closeflag: commonLogic.appcommonhandle("直接完成",null), 
					emeqlctgssname: commonLogic.appcommonhandle("钢丝绳位置",null), 
					emeitiresname: commonLogic.appcommonhandle("轮胎清单",null), 
					priority: commonLogic.appcommonhandle("优先级",null), 
					yxcb: commonLogic.appcommonhandle("影响船舶",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					wodesc: commonLogic.appcommonhandle("工单内容",null), 
					wodate: commonLogic.appcommonhandle("安排执行日期",null), 
					regionbegindate: commonLogic.appcommonhandle("起始时间",null), 
					regionenddate: commonLogic.appcommonhandle("结束时间",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					waitmodi: commonLogic.appcommonhandle("等待修理时(分)",null), 
					waitbuy: commonLogic.appcommonhandle("等待配件时(分)",null), 
					worklength: commonLogic.appcommonhandle("实际工时(分)",null), 
					wresult: commonLogic.appcommonhandle("执行结果",null), 
					mfee: commonLogic.appcommonhandle("约合材料费(￥)",null), 
					cplanflag: commonLogic.appcommonhandle("转计划标志",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					mpersonid: commonLogic.appcommonhandle("制定人",null), 
					mpersonname: commonLogic.appcommonhandle("制定人",null), 
					wpersonid: commonLogic.appcommonhandle("执行人",null), 
					wpersonname: commonLogic.appcommonhandle("执行人",null), 
					rdeptid: commonLogic.appcommonhandle("责任部门",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					rteamid: commonLogic.appcommonhandle("责任班组",null), 
					rfodename: commonLogic.appcommonhandle("现象",null), 
					rfomoname: commonLogic.appcommonhandle("模式",null), 
					rfocaname: commonLogic.appcommonhandle("原因",null), 
					rfoacname: commonLogic.appcommonhandle("方案",null), 
					wooriname: commonLogic.appcommonhandle("工单来源",null), 
					wooritype: commonLogic.appcommonhandle("来源类型",null), 
					wopname: commonLogic.appcommonhandle("上级工单",null), 
					prefee: commonLogic.appcommonhandle("预算(￥)",null), 
					mdate: commonLogic.appcommonhandle("制定时间",null), 
					expiredate: commonLogic.appcommonhandle("过期日期",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emeqlctgssid: commonLogic.appcommonhandle("钢丝绳位置",null), 
					rfodeid: commonLogic.appcommonhandle("现象",null), 
					wooriid: commonLogic.appcommonhandle("工单来源",null), 
					emeitiresid: commonLogic.appcommonhandle("轮胎清单",null), 
					rfoacid: commonLogic.appcommonhandle("方案",null), 
					rfomoid: commonLogic.appcommonhandle("模式",null), 
					emeqlcttiresid: commonLogic.appcommonhandle("轮胎位置",null), 
					rfocaid: commonLogic.appcommonhandle("原因",null), 
					wopid: commonLogic.appcommonhandle("上级工单",null), 
				},
				uiactions: {
				},
			},
			main4_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("工单信息",null), 
					grouppanel18: commonLogic.appcommonhandle("执行信息",null), 
					grouppanel30: commonLogic.appcommonhandle("责任信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("工单编号",null), 
					srfmajortext: commonLogic.appcommonhandle("工单名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					emwo_innername: commonLogic.appcommonhandle("工单名称",null), 
					emwo_innerid: commonLogic.appcommonhandle("工单编号(自动)",null), 
					wogroup: commonLogic.appcommonhandle("工单分组",null), 
					activelengths: commonLogic.appcommonhandle("安排工时(H)",null), 
					wotype: commonLogic.appcommonhandle("工单类型",null), 
					emeqlcttiresname: commonLogic.appcommonhandle("轮胎位置",null), 
					closeflag: commonLogic.appcommonhandle("直接完成",null), 
					emeqlctgssname: commonLogic.appcommonhandle("钢丝绳位置",null), 
					emeitiresname: commonLogic.appcommonhandle("轮胎清单",null), 
					priority: commonLogic.appcommonhandle("优先级",null), 
					yxcb: commonLogic.appcommonhandle("影响船舶",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					wodesc: commonLogic.appcommonhandle("工单内容",null), 
					wodate: commonLogic.appcommonhandle("安排执行日期",null), 
					regionbegindate: commonLogic.appcommonhandle("起始时间",null), 
					regionenddate: commonLogic.appcommonhandle("结束时间",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					waitmodi: commonLogic.appcommonhandle("等待修理时(分)",null), 
					waitbuy: commonLogic.appcommonhandle("等待配件时(分)",null), 
					worklength: commonLogic.appcommonhandle("实际工时(分)",null), 
					wresult: commonLogic.appcommonhandle("执行结果",null), 
					mfee: commonLogic.appcommonhandle("约合材料费(￥)",null), 
					cplanflag: commonLogic.appcommonhandle("转计划标志",null), 
					rempid: commonLogic.appcommonhandle("责任人",null), 
					rempname: commonLogic.appcommonhandle("责任人",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					wpersonid: commonLogic.appcommonhandle("执行人",null), 
					wpersonname: commonLogic.appcommonhandle("执行人",null), 
					rdeptname: commonLogic.appcommonhandle("责任部门",null), 
					rteamname: commonLogic.appcommonhandle("责任班组",null), 
					mpersonid: commonLogic.appcommonhandle("制定人",null), 
					mpersonname: commonLogic.appcommonhandle("制定人",null), 
				},
				uiactions: {
				},
			},
			main6_grid: {
				columns: {
					emwo_innerid: commonLogic.appcommonhandle("工单编号",null),
					priority: commonLogic.appcommonhandle("优先级",null),
					emwo_innername: commonLogic.appcommonhandle("工单名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					wodate: commonLogic.appcommonhandle("执行日期",null),
					wfstep: commonLogic.appcommonhandle("流程步骤",null),
					rteamname: commonLogic.appcommonhandle("责任班组",null),
					wresult: commonLogic.appcommonhandle("执行结果",null),
					regionbegindate: commonLogic.appcommonhandle("起始时间",null),
					regionenddate: commonLogic.appcommonhandle("结束时间",null),
					worklength: commonLogic.appcommonhandle("实际工时(分)",null),
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
					mfee: commonLogic.appcommonhandle("约合材料费(￥)",null),
					cplanflag: commonLogic.appcommonhandle("转计划标志",null),
					wogroup: commonLogic.appcommonhandle("工单分组",null),
					wotype: commonLogic.appcommonhandle("工单类型",null),
					wopname: commonLogic.appcommonhandle("上级工单",null),
					wooriname: commonLogic.appcommonhandle("工单来源",null),
					woteam: commonLogic.appcommonhandle("工单组",null),
					waitmodi: commonLogic.appcommonhandle("等待修理时(分)",null),
					waitbuy: commonLogic.appcommonhandle("等待配件时(分)",null),
					wostate: commonLogic.appcommonhandle("工单状态",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emwo_innername_like: commonLogic.appcommonhandle("工单名称(文本包含(%))",null), 
					n_equipname_like: commonLogic.appcommonhandle("设备(文本包含(%))",null), 
					n_objname_like: commonLogic.appcommonhandle("位置(文本包含(%))",null), 
					n_wogroup_eq: commonLogic.appcommonhandle("工单分组(等于(=))",null), 
					n_wooriname_like: commonLogic.appcommonhandle("工单来源(文本包含(%))",null), 
					n_wostate_eq: commonLogic.appcommonhandle("工单状态(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem17_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				deuiaction2_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			draftgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_acceptance: {
					caption: commonLogic.appcommonhandle("验收",null),
					tip: commonLogic.appcommonhandle("验收",null),
				},
				tbitem1_unacceptance: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			toconfirmgridviewtoolbar_toolbar: {
				tbitem1_submit: {
					caption: commonLogic.appcommonhandle("提交",null),
					tip: commonLogic.appcommonhandle("提交",null),
				},
				tbitem1_acceptance: {
					caption: commonLogic.appcommonhandle("验收",null),
					tip: commonLogic.appcommonhandle("验收",null),
				},
				tbitem1_unacceptance: {
					caption: commonLogic.appcommonhandle("驳回",null),
					tip: commonLogic.appcommonhandle("驳回",null),
				},
				seperator1: {
					caption: commonLogic.appcommonhandle("",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("全部内部工单",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("草稿",null),
					},
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("执行中",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("已完成",null),
					}
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;