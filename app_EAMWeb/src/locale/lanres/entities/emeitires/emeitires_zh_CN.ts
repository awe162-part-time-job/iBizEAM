import EMEITIRes_zh_CN_Base from './emeitires_zh_CN_base';

function getLocaleResource(){
    const EMEITIRes_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEITIRes_zh_CN_Base(), EMEITIRes_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;