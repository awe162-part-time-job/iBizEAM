import EMItemPRtn_zh_CN_Base from './emitem-prtn_zh_CN_base';

function getLocaleResource(){
    const EMItemPRtn_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemPRtn_zh_CN_Base(), EMItemPRtn_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;