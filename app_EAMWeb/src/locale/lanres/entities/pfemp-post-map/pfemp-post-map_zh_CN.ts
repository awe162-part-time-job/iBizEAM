import PFEmpPostMap_zh_CN_Base from './pfemp-post-map_zh_CN_base';

function getLocaleResource(){
    const PFEmpPostMap_zh_CN_OwnData = {};
    const targetData = Object.assign(PFEmpPostMap_zh_CN_Base(), PFEmpPostMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;