import EMDRWG_zh_CN_Base from './emdrwg_zh_CN_base';

function getLocaleResource(){
    const EMDRWG_zh_CN_OwnData = {};
    const targetData = Object.assign(EMDRWG_zh_CN_Base(), EMDRWG_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;