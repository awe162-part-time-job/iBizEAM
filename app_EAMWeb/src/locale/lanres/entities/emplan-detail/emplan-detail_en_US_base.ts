import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emplandetailname: commonLogic.appcommonhandle("计划步骤名称",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			recvpersonname: commonLogic.appcommonhandle("接收人",null),
			orderflag: commonLogic.appcommonhandle("排序",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			emplandetailid: commonLogic.appcommonhandle("计划步骤标识",null),
			recvpersonid: commonLogic.appcommonhandle("接收人",null),
			archive: commonLogic.appcommonhandle("归档",null),
			emwotype: commonLogic.appcommonhandle("生成工单种类",null),
			content: commonLogic.appcommonhandle("详细内容",null),
			eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
			plandetaildesc: commonLogic.appcommonhandle("计划步骤内容",null),
			planname: commonLogic.appcommonhandle("计划",null),
			dptype: commonLogic.appcommonhandle("测点类型",null),
			equipname: commonLogic.appcommonhandle("设备",null),
			dpname: commonLogic.appcommonhandle("测点",null),
			objname: commonLogic.appcommonhandle("位置",null),
			objid: commonLogic.appcommonhandle("位置",null),
			dpid: commonLogic.appcommonhandle("测点",null),
			planid: commonLogic.appcommonhandle("计划",null),
			equipid: commonLogic.appcommonhandle("设备",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("计划步骤",null),
					title: commonLogic.appcommonhandle("计划步骤",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("计划步骤",null),
					title: commonLogic.appcommonhandle("计划步骤",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("计划步骤信息",null), 
					grouppanel11: commonLogic.appcommonhandle("对象信息",null), 
					grouppanel16: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					formpage23: commonLogic.appcommonhandle("详细信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("计划步骤标识",null), 
					srfmajortext: commonLogic.appcommonhandle("计划步骤名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplandetailname: commonLogic.appcommonhandle("计划步骤名称",null), 
					orderflag: commonLogic.appcommonhandle("排序",null), 
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null), 
					recvpersonid: commonLogic.appcommonhandle("接收人",null), 
					recvpersonname: commonLogic.appcommonhandle("接收人",null), 
					plandetaildesc: commonLogic.appcommonhandle("计划步骤内容",null), 
					archive: commonLogic.appcommonhandle("归档",null), 
					planname: commonLogic.appcommonhandle("计划",null), 
					emwotype: commonLogic.appcommonhandle("生成工单种类",null), 
					equipname: commonLogic.appcommonhandle("设备",null), 
					objname: commonLogic.appcommonhandle("位置",null), 
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null), 
					dpname: commonLogic.appcommonhandle("测点",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					content: commonLogic.appcommonhandle("详细内容",null), 
					planid: commonLogic.appcommonhandle("计划",null), 
					objid: commonLogic.appcommonhandle("位置",null), 
					dpid: commonLogic.appcommonhandle("测点",null), 
					equipid: commonLogic.appcommonhandle("设备",null), 
					emplandetailid: commonLogic.appcommonhandle("计划步骤标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					orderflag: commonLogic.appcommonhandle("排序",null),
					emplandetailname: commonLogic.appcommonhandle("计划步骤名称",null),
					equipname: commonLogic.appcommonhandle("设备",null),
					objname: commonLogic.appcommonhandle("位置",null),
					dpname: commonLogic.appcommonhandle("测点",null),
					dptype: commonLogic.appcommonhandle("测点类型",null),
					plandetaildesc: commonLogic.appcommonhandle("计划步骤内容",null),
					recvpersonname: commonLogic.appcommonhandle("接收人",null),
					activelengths: commonLogic.appcommonhandle("持续时间(H)",null),
					eqstoplength: commonLogic.appcommonhandle("停运时间(分)",null),
					archive: commonLogic.appcommonhandle("归档",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			byplan_list: {
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			byplan_portlet: {
				byplan: {
					title: commonLogic.appcommonhandle("计划步骤", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;