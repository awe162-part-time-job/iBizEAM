import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("动态图表", null),
		fields: {
			createdate: commonLogic.appcommonhandle("建立时间",null),
			dynachartname: commonLogic.appcommonhandle("动态图表名称",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			modelid: commonLogic.appcommonhandle("模型标识",null),
			appid: commonLogic.appcommonhandle("应用标识",null),
			model: commonLogic.appcommonhandle("模型",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			userid: commonLogic.appcommonhandle("用户标识",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			dynachartid: commonLogic.appcommonhandle("动态图表标识",null),
		},
		};
		return data;
}
export default getLocaleResourceBase;