import DynaChart_zh_CN_Base from './dyna-chart_zh_CN_base';

function getLocaleResource(){
    const DynaChart_zh_CN_OwnData = {};
    const targetData = Object.assign(DynaChart_zh_CN_Base(), DynaChart_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;