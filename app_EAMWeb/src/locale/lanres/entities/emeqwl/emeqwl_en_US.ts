import EMEQWL_en_US_Base from './emeqwl_en_US_base';

function getLocaleResource(){
    const EMEQWL_en_US_OwnData = {};
    const targetData = Object.assign(EMEQWL_en_US_Base(), EMEQWL_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
