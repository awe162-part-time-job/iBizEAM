import EMBerth_zh_CN_Base from './emberth_zh_CN_base';

function getLocaleResource(){
    const EMBerth_zh_CN_OwnData = {};
    const targetData = Object.assign(EMBerth_zh_CN_Base(), EMBerth_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;