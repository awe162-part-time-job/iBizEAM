import EMPO_en_US_Base from './empo_en_US_base';

function getLocaleResource(){
    const EMPO_en_US_OwnData = {};
    const targetData = Object.assign(EMPO_en_US_Base(), EMPO_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
