import EMWO_EN_en_US_Base from './emwo-en_en_US_base';

function getLocaleResource(){
    const EMWO_EN_en_US_OwnData = {};
    const targetData = Object.assign(EMWO_EN_en_US_Base(), EMWO_EN_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
