import EMEQKP_zh_CN_Base from './emeqkp_zh_CN_base';

function getLocaleResource(){
    const EMEQKP_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQKP_zh_CN_Base(), EMEQKP_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;