import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			createman: commonLogic.appcommonhandle("建立人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			description: commonLogic.appcommonhandle("描述",null),
			rfodecode: commonLogic.appcommonhandle("现象代码",null),
			objid: commonLogic.appcommonhandle("对象编号",null),
			rfodeinfo: commonLogic.appcommonhandle("现象信息",null),
			emrfodeid: commonLogic.appcommonhandle("现象标识",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			emrfodename: commonLogic.appcommonhandle("现象名称",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
		},
			views: {
				tabexpview: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象",null),
				},
				quickview: {
					caption: commonLogic.appcommonhandle("快速新建",null),
					title: commonLogic.appcommonhandle("快速新建视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象选择表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("现象信息",null),
					title: commonLogic.appcommonhandle("现象信息",null),
				},
				editview9_edit: {
					caption: commonLogic.appcommonhandle("现象信息",null),
					title: commonLogic.appcommonhandle("现象信息",null),
				},
				dashboardview9: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象数据看板视图",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象数据选择视图",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("现象信息",null),
					title: commonLogic.appcommonhandle("现象信息",null),
				},
				gridexpview: {
					caption: commonLogic.appcommonhandle("现象",null),
					title: commonLogic.appcommonhandle("现象表格导航视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("现象信息",null),
					title: commonLogic.appcommonhandle("现象信息",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("现象信息",null), 
					grouppanel5: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("现象标识",null), 
					srfmajortext: commonLogic.appcommonhandle("现象名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					rfodecode: commonLogic.appcommonhandle("现象代码",null), 
					emrfodename: commonLogic.appcommonhandle("现象名称",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emrfodeid: commonLogic.appcommonhandle("现象标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("现象信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("现象标识",null), 
					srfmajortext: commonLogic.appcommonhandle("现象名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					rfodecode: commonLogic.appcommonhandle("现象代码",null), 
					emrfodename: commonLogic.appcommonhandle("现象名称",null), 
					emrfodeid: commonLogic.appcommonhandle("现象标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("现象基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("现象标识",null), 
					srfmajortext: commonLogic.appcommonhandle("现象名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					rfodecode: commonLogic.appcommonhandle("现象代码",null), 
					emrfodename: commonLogic.appcommonhandle("现象名称",null), 
					emrfodeid: commonLogic.appcommonhandle("现象标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					rfodeinfo: commonLogic.appcommonhandle("现象信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					rfodecode: commonLogic.appcommonhandle("现象代码",null),
					emrfodename: commonLogic.appcommonhandle("现象名称",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main3_grid: {
				columns: {
					rfodecode: commonLogic.appcommonhandle("现象代码",null),
					emrfodename: commonLogic.appcommonhandle("现象名称",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emrfodename_like: commonLogic.appcommonhandle("现象名称(文本包含(%))",null), 
				},
				uiactions: {
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			gridexpviewgridexpbar_toolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem26: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
			},
			editview9_edittoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			tabexpviewtabexppanel_tabexppanel: {
				tabviewpanels: {
					tabviewpanel3: {
						caption: commonLogic.appcommonhandle("原因",null),
					},
					tabviewpanel4: {
						caption: commonLogic.appcommonhandle("方案",null),
					},
					tabviewpanel: {
						caption: commonLogic.appcommonhandle("现象应用",null),
					},
					tabviewpanel2: {
						caption: commonLogic.appcommonhandle("模式",null),
					}
				},
				uiactions: {
				},
			},
			editview9_portlet: {
				editview9: {
					title: commonLogic.appcommonhandle("现象信息", null)
			  	},
				uiactions: {
				},
			},
			tabexpview_portlet: {
				tabexpview: {
					title: commonLogic.appcommonhandle("现象明细", null)
			  	},
				uiactions: {
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;