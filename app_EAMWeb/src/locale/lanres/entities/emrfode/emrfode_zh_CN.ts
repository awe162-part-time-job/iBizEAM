import EMRFODE_zh_CN_Base from './emrfode_zh_CN_base';

function getLocaleResource(){
    const EMRFODE_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFODE_zh_CN_Base(), EMRFODE_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;