import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("库存", null),
		fields: {
			batcode: commonLogic.appcommonhandle("批次",null),
			emstockid: commonLogic.appcommonhandle("库存标识",null),
			storepartgl: commonLogic.appcommonhandle("库存货架管理",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			stockinfo: commonLogic.appcommonhandle("库存信息",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emstockname: commonLogic.appcommonhandle("库存名称",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			amount: commonLogic.appcommonhandle("库存金额",null),
			description: commonLogic.appcommonhandle("描述",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			stockcnt: commonLogic.appcommonhandle("库存数量",null),
			storepartname: commonLogic.appcommonhandle("库位",null),
			itembtypeid: commonLogic.appcommonhandle("物品一级类型",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			price: commonLogic.appcommonhandle("平均价",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			itembtypename: commonLogic.appcommonhandle("物品一级类型",null),
			storepartid: commonLogic.appcommonhandle("库位",null),
			itemid: commonLogic.appcommonhandle("物品",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("库存",null),
					title: commonLogic.appcommonhandle("库存数据选择视图",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("库存物品",null),
					title: commonLogic.appcommonhandle("库存物品",null),
				},
				bystorepartgridview: {
					caption: commonLogic.appcommonhandle("库存明细",null),
					title: commonLogic.appcommonhandle("库存明细",null),
				},
				bystoregridview: {
					caption: commonLogic.appcommonhandle("库存明细",null),
					title: commonLogic.appcommonhandle("库存明细",null),
				},
				bycabgridview: {
					caption: commonLogic.appcommonhandle("库存明细",null),
					title: commonLogic.appcommonhandle("库存明细",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("库存",null),
					title: commonLogic.appcommonhandle("库存选择表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("库存明细",null),
					title: commonLogic.appcommonhandle("库存明细",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("库存信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("库存标识",null), 
					srfmajortext: commonLogic.appcommonhandle("库存信息",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					storepartname: commonLogic.appcommonhandle("库位",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					stockcnt: commonLogic.appcommonhandle("库存数量",null), 
					price: commonLogic.appcommonhandle("平均价",null), 
					amount: commonLogic.appcommonhandle("库存金额",null), 
					batcode: commonLogic.appcommonhandle("批次",null), 
					storepartid: commonLogic.appcommonhandle("库位",null), 
					storeid: commonLogic.appcommonhandle("仓库",null), 
					emstockid: commonLogic.appcommonhandle("库存标识",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					itemname: commonLogic.appcommonhandle("物品",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					storepartname: commonLogic.appcommonhandle("库位",null),
					stockcnt: commonLogic.appcommonhandle("库存数量",null),
					price: commonLogic.appcommonhandle("平均价",null),
					amount: commonLogic.appcommonhandle("库存金额",null),
					batcode: commonLogic.appcommonhandle("批次",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			stockcntbar_chart: {
				nodata:commonLogic.appcommonhandle("",null),
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_itemname_like: commonLogic.appcommonhandle("物品(文本包含(%))",null), 
					n_storeid_eq: commonLogic.appcommonhandle("仓库(等于(=))",null), 
					n_storepartid_eq: commonLogic.appcommonhandle("库位(等于(=))",null), 
				},
				uiactions: {
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			bystorepartgridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			bystoregridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			bycabgridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			stoclcntbar_portlet: {
				stoclcntbar: {
					title: commonLogic.appcommonhandle("物品库存统计", null)
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;