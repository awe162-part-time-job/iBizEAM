import EMBrand_zh_CN_Base from './embrand_zh_CN_base';

function getLocaleResource(){
    const EMBrand_zh_CN_OwnData = {};
    const targetData = Object.assign(EMBrand_zh_CN_Base(), EMBrand_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;