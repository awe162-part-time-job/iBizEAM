import PLANSCHEDULE_M_en_US_Base from './planschedule-m_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_M_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_M_en_US_Base(), PLANSCHEDULE_M_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
