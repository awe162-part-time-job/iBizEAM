import EMPODetail_zh_CN_Base from './empodetail_zh_CN_base';

function getLocaleResource(){
    const EMPODetail_zh_CN_OwnData = {};
    const targetData = Object.assign(EMPODetail_zh_CN_Base(), EMPODetail_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;