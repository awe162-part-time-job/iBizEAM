import EMItemTrade_zh_CN_Base from './emitem-trade_zh_CN_base';

function getLocaleResource(){
    const EMItemTrade_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemTrade_zh_CN_Base(), EMItemTrade_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;