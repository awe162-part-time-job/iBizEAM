import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("机种编号", null),
		fields: {
			emmachinecategoryid: commonLogic.appcommonhandle("流水号",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			emmachinecategoryname: commonLogic.appcommonhandle("机种名称",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			remarks: commonLogic.appcommonhandle("备注",null),
			jzinfo: commonLogic.appcommonhandle("机种信息",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			machtypecode: commonLogic.appcommonhandle("机种编码",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
		},
			views: {
				pickupview: {
					caption: commonLogic.appcommonhandle("机种编号",null),
					title: commonLogic.appcommonhandle("机种编号数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("机种编号",null),
					title: commonLogic.appcommonhandle("机种编号选择表格视图",null),
				},
			},
			main_grid: {
				columns: {
					jzinfo: commonLogic.appcommonhandle("机种信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;