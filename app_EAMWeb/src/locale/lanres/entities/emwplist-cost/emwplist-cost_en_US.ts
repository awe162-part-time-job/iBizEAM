import EMWPListCost_en_US_Base from './emwplist-cost_en_US_base';

function getLocaleResource(){
    const EMWPListCost_en_US_OwnData = {};
    const targetData = Object.assign(EMWPListCost_en_US_Base(), EMWPListCost_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
