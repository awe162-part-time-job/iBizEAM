import EMItemRIn_zh_CN_Base from './emitem-rin_zh_CN_base';

function getLocaleResource(){
    const EMItemRIn_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemRIn_zh_CN_Base(), EMItemRIn_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;