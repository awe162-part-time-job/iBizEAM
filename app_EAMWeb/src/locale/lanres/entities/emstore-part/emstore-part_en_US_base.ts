import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			emstorepartid: commonLogic.appcommonhandle("仓库库位标识",null),
			storepartid: commonLogic.appcommonhandle("库位编号",null),
			emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			storepartinfo: commonLogic.appcommonhandle("仓库库位信息",null),
			storepartcode: commonLogic.appcommonhandle("仓库库位代码",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			description: commonLogic.appcommonhandle("描述",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			storepartname: commonLogic.appcommonhandle("仓库库位名称",null),
			storename: commonLogic.appcommonhandle("仓库",null),
			storeid: commonLogic.appcommonhandle("仓库",null),
		},
			views: {
				editview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位数据选择视图",null),
				},
				editview_9836: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位选择表格视图",null),
				},
				optionview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位选项操作视图",null),
				},
				editview_7215: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("仓库库位",null),
					title: commonLogic.appcommonhandle("仓库库位",null),
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("仓库库位信息",null), 
					grouppanel6: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("仓库库位标识",null), 
					srfmajortext: commonLogic.appcommonhandle("仓库库位名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storepartcode: commonLogic.appcommonhandle("仓库库位代码",null), 
					emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emstorepartid: commonLogic.appcommonhandle("仓库库位标识",null), 
				},
				uiactions: {
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("仓库库位信息",null), 
					grouppanel6: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("仓库库位标识",null), 
					srfmajortext: commonLogic.appcommonhandle("仓库库位名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					storepartcode: commonLogic.appcommonhandle("仓库库位代码",null), 
					emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null), 
					storename: commonLogic.appcommonhandle("仓库",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					storeid: commonLogic.appcommonhandle("仓库",null), 
					emstorepartid: commonLogic.appcommonhandle("仓库库位标识",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("仓库库位基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("仓库库位标识",null), 
					srfmajortext: commonLogic.appcommonhandle("仓库库位名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emstorepartid: commonLogic.appcommonhandle("仓库库位标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					storepartcode: commonLogic.appcommonhandle("仓库库位代码",null),
					emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null),
					storename: commonLogic.appcommonhandle("仓库",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					emstorepartname: commonLogic.appcommonhandle("仓库库位名称",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editview_9836toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("New",null),
					tip: commonLogic.appcommonhandle("New",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("Edit",null),
					tip: commonLogic.appcommonhandle("Edit {0}",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("Remove",null),
					tip: commonLogic.appcommonhandle("Remove {0}",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("Export",null),
					tip: commonLogic.appcommonhandle("Export {0} Data To Excel",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("Filter",null),
					tip: commonLogic.appcommonhandle("Filter",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editview_7215toolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("Save And Close",null),
					tip: commonLogic.appcommonhandle("Save And Close Window",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;