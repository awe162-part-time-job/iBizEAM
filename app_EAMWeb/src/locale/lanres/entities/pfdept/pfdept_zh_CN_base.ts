import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("部门", null),
		fields: {
			deptcode: commonLogic.appcommonhandle("部门代码",null),
			mgrempname: commonLogic.appcommonhandle("主管",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			deptinfo: commonLogic.appcommonhandle("部门信息",null),
			deptfn: commonLogic.appcommonhandle("职能",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			maindeptcode: commonLogic.appcommonhandle("主部门编码",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			deptpid: commonLogic.appcommonhandle("上级部门",null),
			pfdeptid: commonLogic.appcommonhandle("部门标识",null),
			mgrempid: commonLogic.appcommonhandle("主管",null),
			pfdeptname: commonLogic.appcommonhandle("部门名称",null),
			sdept: commonLogic.appcommonhandle("统计归口部门",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			description: commonLogic.appcommonhandle("描述",null),
			deptpname: commonLogic.appcommonhandle("上级部门",null),
		},
			views: {
				pickupgridview: {
					caption: commonLogic.appcommonhandle("部门",null),
					title: commonLogic.appcommonhandle("部门选择表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("部门",null),
					title: commonLogic.appcommonhandle("部门",null),
				},
				gridview: {
					caption: commonLogic.appcommonhandle("部门",null),
					title: commonLogic.appcommonhandle("部门",null),
				},
				editview_editmode: {
					caption: commonLogic.appcommonhandle("部门",null),
					title: commonLogic.appcommonhandle("部门",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("部门",null),
					title: commonLogic.appcommonhandle("部门数据选择视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("部门信息",null), 
					grouppanel8: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("部门标识",null), 
					srfmajortext: commonLogic.appcommonhandle("部门名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					deptcode: commonLogic.appcommonhandle("部门代码",null), 
					pfdeptname: commonLogic.appcommonhandle("部门名称",null), 
					deptpname: commonLogic.appcommonhandle("上级部门",null), 
					sdept: commonLogic.appcommonhandle("统计归口部门",null), 
					deptfn: commonLogic.appcommonhandle("职能",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					pfdeptid: commonLogic.appcommonhandle("部门标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("部门信息",null), 
					grouppanel8: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("部门标识",null), 
					srfmajortext: commonLogic.appcommonhandle("部门名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					deptcode: commonLogic.appcommonhandle("部门代码",null), 
					pfdeptname: commonLogic.appcommonhandle("部门名称",null), 
					deptpname: commonLogic.appcommonhandle("上级部门",null), 
					sdept: commonLogic.appcommonhandle("统计归口部门",null), 
					deptfn: commonLogic.appcommonhandle("职能",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					pfdeptid: commonLogic.appcommonhandle("部门标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					deptcode: commonLogic.appcommonhandle("部门代码",null),
					pfdeptname: commonLogic.appcommonhandle("部门名称",null),
					deptpname: commonLogic.appcommonhandle("上级部门",null),
					sdept: commonLogic.appcommonhandle("统计归口部门",null),
					orgid: commonLogic.appcommonhandle("组织",null),
					deptfn: commonLogic.appcommonhandle("职能",null),
					description: commonLogic.appcommonhandle("描述",null),
					maindeptcode: commonLogic.appcommonhandle("主部门编码",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			depttree_treeview: {
				nodata:commonLogic.appcommonhandle("",null),
				nodes: {
					root: commonLogic.appcommonhandle("默认根节点",null),
					all: commonLogic.appcommonhandle("全部职员",null),
				},
				uiactions: {
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;