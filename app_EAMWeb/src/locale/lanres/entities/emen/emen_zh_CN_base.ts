import commonLogic from '@/locale/logic/common/common-logic';
function getLocaleResourceBase(){
	const data:any = {
		appdename: commonLogic.appcommonhandle("能源", null),
		fields: {
			createdate: commonLogic.appcommonhandle("建立时间",null),
			energydesc: commonLogic.appcommonhandle("能源备注",null),
			emenid: commonLogic.appcommonhandle("能源标识",null),
			description: commonLogic.appcommonhandle("描述",null),
			energycode: commonLogic.appcommonhandle("能源代码",null),
			energytypeid: commonLogic.appcommonhandle("能源类型",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			vrate: commonLogic.appcommonhandle("倍率",null),
			enable: commonLogic.appcommonhandle("逻辑有效标志",null),
			orgid: commonLogic.appcommonhandle("组织",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			price: commonLogic.appcommonhandle("能源单价",null),
			emenname: commonLogic.appcommonhandle("能源名称",null),
			energyinfo: commonLogic.appcommonhandle("能源信息",null),
			unitname: commonLogic.appcommonhandle("单位",null),
			itemmtypeid: commonLogic.appcommonhandle("物品二级类",null),
			itemname: commonLogic.appcommonhandle("物品",null),
			itemprice: commonLogic.appcommonhandle("物品库存单价",null),
			itemmtypename: commonLogic.appcommonhandle("物品二级类",null),
			itemtypeid: commonLogic.appcommonhandle("物品类型",null),
			itembtypeid: commonLogic.appcommonhandle("物品大类",null),
			itembtypename: commonLogic.appcommonhandle("物品大类",null),
			itemid: commonLogic.appcommonhandle("物品",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源编辑视图",null),
				},
				editview9_editmode: {
					caption: commonLogic.appcommonhandle("能源信息",null),
					title: commonLogic.appcommonhandle("能源信息",null),
				},
				editview9: {
					caption: commonLogic.appcommonhandle("能源信息",null),
					title: commonLogic.appcommonhandle("能源信息",null),
				},
				pickupview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源数据选择视图",null),
				},
				pickupgridview: {
					caption: commonLogic.appcommonhandle("能源",null),
					title: commonLogic.appcommonhandle("能源选择表格视图",null),
				},
			},
			main2_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能源信息",null), 
					grouppanel10: commonLogic.appcommonhandle("操作信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energycode: commonLogic.appcommonhandle("能源代码",null), 
					emenname: commonLogic.appcommonhandle("能源名称",null), 
					energytypeid: commonLogic.appcommonhandle("能源类型",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					price: commonLogic.appcommonhandle("能源单价",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					energydesc: commonLogic.appcommonhandle("能源备注",null), 
					orgid: commonLogic.appcommonhandle("组织",null), 
					description: commonLogic.appcommonhandle("描述",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
				},
				uiactions: {
				},
			},
			main3_form: {
				details: {
					grouppanel2: commonLogic.appcommonhandle("能源信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energycode: commonLogic.appcommonhandle("能源代码",null), 
					emenname: commonLogic.appcommonhandle("能源名称",null), 
					energytypeid: commonLogic.appcommonhandle("能源类型",null), 
					vrate: commonLogic.appcommonhandle("倍率",null), 
					price: commonLogic.appcommonhandle("能源单价",null), 
					itemname: commonLogic.appcommonhandle("物品",null), 
					energydesc: commonLogic.appcommonhandle("能源备注",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
					itemid: commonLogic.appcommonhandle("物品",null), 
				},
				uiactions: {
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("能源基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					group2: commonLogic.appcommonhandle("操作信息",null), 
					formpage2: commonLogic.appcommonhandle("其它",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("能源标识",null), 
					srfmajortext: commonLogic.appcommonhandle("能源名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					energyinfo: commonLogic.appcommonhandle("能源信息",null), 
					createman: commonLogic.appcommonhandle("建立人",null), 
					createdate: commonLogic.appcommonhandle("建立时间",null), 
					updateman: commonLogic.appcommonhandle("更新人",null), 
					updatedate: commonLogic.appcommonhandle("更新时间",null), 
					emenid: commonLogic.appcommonhandle("能源标识",null), 
				},
				uiactions: {
				},
			},
			main2_grid: {
				columns: {
					energycode: commonLogic.appcommonhandle("能源代码",null),
					emenname: commonLogic.appcommonhandle("能源名称",null),
					energytypeid: commonLogic.appcommonhandle("能源类型",null),
					vrate: commonLogic.appcommonhandle("倍率",null),
					energydesc: commonLogic.appcommonhandle("能源备注",null),
					price: commonLogic.appcommonhandle("能源单价",null),
					itemname: commonLogic.appcommonhandle("物品",null),
					description: commonLogic.appcommonhandle("描述",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					energyinfo: commonLogic.appcommonhandle("能源信息",null),
					updateman: commonLogic.appcommonhandle("更新人",null),
					updatedate: commonLogic.appcommonhandle("更新时间",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
					n_emenname_like: commonLogic.appcommonhandle("能源名称(文本包含(%))",null), 
					n_energytypeid_eq: commonLogic.appcommonhandle("能源类型(等于(=))",null), 
				},
				uiactions: {
				},
			},
			editview9toolbar_toolbar: {
				deuiaction1: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
			},
			gridviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("编辑",null),
					tip: commonLogic.appcommonhandle("编辑",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("删除",null),
					tip: commonLogic.appcommonhandle("删除",null),
				},
				tbitem9: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("导出",null),
					tip: commonLogic.appcommonhandle("导出",null),
				},
				tbitem10: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem19: {
					caption: commonLogic.appcommonhandle("过滤",null),
					tip: commonLogic.appcommonhandle("过滤",null),
				},
			},
			editview9_editmodetoolbar_toolbar: {
				tbitem1: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
			editviewtoolbar_toolbar: {
				tbitem3: {
					caption: commonLogic.appcommonhandle("保存",null),
					tip: commonLogic.appcommonhandle("保存",null),
				},
				tbitem4: {
					caption: commonLogic.appcommonhandle("保存并新建",null),
					tip: commonLogic.appcommonhandle("保存并新建",null),
				},
				tbitem5: {
					caption: commonLogic.appcommonhandle("保存并关闭",null),
					tip: commonLogic.appcommonhandle("保存并关闭",null),
				},
				tbitem6: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem7: {
					caption: commonLogic.appcommonhandle("删除并关闭",null),
					tip: commonLogic.appcommonhandle("删除并关闭",null),
				},
				tbitem8: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem12: {
					caption: commonLogic.appcommonhandle("新建",null),
					tip: commonLogic.appcommonhandle("新建",null),
				},
				tbitem13: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem14: {
					caption: commonLogic.appcommonhandle("拷贝",null),
					tip: commonLogic.appcommonhandle("拷贝",null),
				},
				tbitem16: {
					caption: commonLogic.appcommonhandle("-",null),
					tip: commonLogic.appcommonhandle("",null),
				},
				tbitem22: {
					caption: commonLogic.appcommonhandle("帮助",null),
					tip: commonLogic.appcommonhandle("帮助",null),
				},
			},
		};
		return data;
}
export default getLocaleResourceBase;