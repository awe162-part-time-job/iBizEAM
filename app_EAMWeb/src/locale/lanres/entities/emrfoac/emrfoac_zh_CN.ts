import EMRFOAC_zh_CN_Base from './emrfoac_zh_CN_base';

function getLocaleResource(){
    const EMRFOAC_zh_CN_OwnData = {};
    const targetData = Object.assign(EMRFOAC_zh_CN_Base(), EMRFOAC_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;