import EMPLANRECORD_en_US_Base from './emplanrecord_en_US_base';

function getLocaleResource(){
    const EMPLANRECORD_en_US_OwnData = {};
    const targetData = Object.assign(EMPLANRECORD_en_US_Base(), EMPLANRECORD_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
