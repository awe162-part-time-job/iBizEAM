import PFTeam_en_US_Base from './pfteam_en_US_base';

function getLocaleResource(){
    const PFTeam_en_US_OwnData = {};
    const targetData = Object.assign(PFTeam_en_US_Base(), PFTeam_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
