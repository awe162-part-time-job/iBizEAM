
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { GridView9Base } from '@/studio-core';
import EMPlanService from '@/service/emplan/emplan-service';
import EMPlanAuthService from '@/authservice/emplan/emplan-auth-service';
import GridView9Engine from '@engine/view/grid-view9-engine';
import EMPlanUIService from '@/uiservice/emplan/emplan-ui-service';
import CodeListService from '@service/app/codelist-service';


/**
 * 计划表格视图视图基类
 *
 * @export
 * @class EMPlanEquipGridView9Base
 * @extends {GridView9Base}
 */
export class EMPlanEquipGridView9Base extends GridView9Base {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */
    protected appDeName: string = 'emplan';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */
    protected appDeKey: string = 'emplanid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */
    protected appDeMajor: string = 'emplanname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMPlanService}
     * @memberof EMPlanEquipGridView9Base
     */
    protected appEntityService: EMPlanService = new EMPlanService;

    /**
     * 实体权限服务对象
     *
     * @type EMPlanUIService
     * @memberof EMPlanEquipGridView9Base
     */
    public appUIService: EMPlanUIService = new EMPlanUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPlanEquipGridView9Base
     */
    protected model: any = {
        srfCaption: 'entities.emplan.views.equipgridview9.caption',
        srfTitle: 'entities.emplan.views.equipgridview9.title',
        srfSubTitle: 'entities.emplan.views.equipgridview9.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPlanEquipGridView9Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMPlanEquipGridView9
     */
    public toolBarModels: any = {
    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */
	protected viewtag: string = '8fc5fb61719a9eafbf7d64f92cb65d26';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPlanEquipGridView9Base
     */ 
    protected viewName: string = 'EMPlanEquipGridView9';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPlanEquipGridView9Base
     */
    public engine: GridView9Engine = new GridView9Engine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPlanEquipGridView9Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPlanEquipGridView9Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.opendata(args, fullargs, params, $event, xData);
            },
            newdata: (args: any[], fullargs?: any[], params?: any, $event?: any, xData?: any) => {
                this.newdata(args, fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            keyPSDEField: 'emplan',
            majorPSDEField: 'emplanname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEquipGridView9Base
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEquipGridView9Base
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEquipGridView9Base
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEquipGridView9Base
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPlanEquipGridView9Base
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMPlanEquipGridView9
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.emplan;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emacclass && true){
            deResParameters = [
            { pathName: 'emacclasses', parameterName: 'emacclass' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emplans', parameterName: 'emplan' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emplan-edit-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emplan.views.editview.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof EMPlanEquipGridView9
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.emacclass && true){
            deResParameters = [
            { pathName: 'emacclasses', parameterName: 'emacclass' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'emplans', parameterName: 'emplan' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'emplan-edit-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.emplan.views.editview.title'),
            placement: 'DRAWER_TOP',
        };
        openDrawer(view, data);
    }


}