import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMPOService from '@/service/empo/empo-service';
import EMPOAuthService from '@/authservice/empo/empo-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMPOUIService from '@/uiservice/empo/empo-ui-service';

/**
 * 订单选择表格视图视图基类
 *
 * @export
 * @class EMPOPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMPOPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */
    protected appDeName: string = 'empo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */
    protected appDeKey: string = 'empoid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */
    protected appDeMajor: string = 'emponame';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMPOService}
     * @memberof EMPOPickupGridViewBase
     */
    protected appEntityService: EMPOService = new EMPOService;

    /**
     * 实体权限服务对象
     *
     * @type EMPOUIService
     * @memberof EMPOPickupGridViewBase
     */
    public appUIService: EMPOUIService = new EMPOUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMPOPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.empo.views.pickupgridview.caption',
        srfTitle: 'entities.empo.views.pickupgridview.title',
        srfSubTitle: 'entities.empo.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMPOPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */
	protected viewtag: string = 'bfef160d5a90d65b6d6b1cf7b5c04dfa';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMPOPickupGridViewBase
     */ 
    protected viewName: string = 'EMPOPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMPOPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMPOPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMPOPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'empo',
            majorPSDEField: 'emponame',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMPOPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMPOPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}