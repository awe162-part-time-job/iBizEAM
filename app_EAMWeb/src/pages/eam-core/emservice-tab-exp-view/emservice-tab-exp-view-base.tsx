import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import EMServiceAuthService from '@/authservice/emservice/emservice-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';

/**
 * 服务商分页导航视图视图基类
 *
 * @export
 * @class EMServiceTabExpViewBase
 * @extends {TabExpViewBase}
 */
export class EMServiceTabExpViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceTabExpViewBase
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMServiceTabExpViewBase
     */
    protected appDeKey: string = 'emserviceid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMServiceTabExpViewBase
     */
    protected appDeMajor: string = 'emservicename';

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof EMServiceTabExpViewBase
     */
    protected appEntityService: EMServiceService = new EMServiceService;

    /**
     * 实体权限服务对象
     *
     * @type EMServiceUIService
     * @memberof EMServiceTabExpViewBase
     */
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMServiceTabExpViewBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMServiceTabExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emservice.views.tabexpview.caption',
        srfTitle: 'entities.emservice.views.tabexpview.title',
        srfSubTitle: 'entities.emservice.views.tabexpview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMServiceTabExpViewBase
     */
    protected containerModel: any = {
        view_tabexppanel: {
            name: 'tabexppanel',
            type: 'TABEXPPANEL',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMServiceTabExpViewBase
     */
	protected viewtag: string = 'ee32a00c8c3c0f0703294b298c8b273f';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMServiceTabExpViewBase
     */ 
    protected viewName: string = 'EMServiceTabExpView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMServiceTabExpViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMServiceTabExpViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMServiceTabExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'emservice',
            majorPSDEField: 'emservicename',
            isLoadDefault: true,
        });
    }


}