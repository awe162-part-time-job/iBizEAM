import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import EMStockService from '@/service/emstock/emstock-service';
import EMStockAuthService from '@/authservice/emstock/emstock-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import EMStockUIService from '@/uiservice/emstock/emstock-ui-service';

/**
 * 库存选择表格视图视图基类
 *
 * @export
 * @class EMStockPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class EMStockPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */
    protected appDeName: string = 'emstock';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */
    protected appDeKey: string = 'emstockid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */
    protected appDeMajor: string = 'emstockname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */ 
    protected dataControl: string = 'grid';

    /**
     * 实体服务对象
     *
     * @type {EMStockService}
     * @memberof EMStockPickupGridViewBase
     */
    protected appEntityService: EMStockService = new EMStockService;

    /**
     * 实体权限服务对象
     *
     * @type EMStockUIService
     * @memberof EMStockPickupGridViewBase
     */
    public appUIService: EMStockUIService = new EMStockUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMStockPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstock.views.pickupgridview.caption',
        srfTitle: 'entities.emstock.views.pickupgridview.title',
        srfSubTitle: 'entities.emstock.views.pickupgridview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMStockPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: {
            name: 'grid',
            type: 'GRID',
        },
        view_searchform: {
            name: 'searchform',
            type: 'SEARCHFORM',
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */
	protected viewtag: string = 'a2d3b73797351b53b3c6c18c5521e588';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMStockPickupGridViewBase
     */ 
    protected viewName: string = 'EMStockPickupGridView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMStockPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMStockPickupGridViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMStockPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'emstock',
            majorPSDEField: 'emstockname',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMStockPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof EMStockPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}