import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import EMEQTypeAuthService from '@/authservice/emeqtype/emeqtype-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * 设备类型数据选择视图视图基类
 *
 * @export
 * @class EMEQTYPEPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMEQTYPEPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected appDeKey: string = 'emeqtypeid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected appDeMajor: string = 'emeqtypename';

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected appEntityService: EMEQTypeService = new EMEQTypeService;

    /**
     * 实体权限服务对象
     *
     * @type EMEQTypeUIService
     * @memberof EMEQTYPEPickupViewBase
     */
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emeqtype.views.pickupview.caption',
        srfTitle: 'entities.emeqtype.views.pickupview.title',
        srfSubTitle: 'entities.emeqtype.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMEQTYPEPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupViewBase
     */
	protected viewtag: string = '8699750ab7e52363ec30b0e681f58ed0';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMEQTYPEPickupViewBase
     */ 
    protected viewName: string = 'EMEQTYPEPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMEQTYPEPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMEQTYPEPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMEQTYPEPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emeqtype',
            majorPSDEField: 'emeqtypename',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMEQTYPEPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}