import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMWO_INNERService from '@/service/emwo-inner/emwo-inner-service';
import EMWO_INNERAuthService from '@/authservice/emwo-inner/emwo-inner-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMWO_INNERUIService from '@/uiservice/emwo-inner/emwo-inner-ui-service';

/**
 * 内部工单视图基类
 *
 * @export
 * @class EMWO_INNEREditView_EditModeBase
 * @extends {EditViewBase}
 */
export class EMWO_INNEREditView_EditModeBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected appDeName: string = 'emwo_inner';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected appDeKey: string = 'emwo_innerid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected appDeMajor: string = 'emwo_innername';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMWO_INNERService}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected appEntityService: EMWO_INNERService = new EMWO_INNERService;

    /**
     * 实体权限服务对象
     *
     * @type EMWO_INNERUIService
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public appUIService: EMWO_INNERUIService = new EMWO_INNERUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected model: any = {
        srfCaption: 'entities.emwo_inner.views.editview_editmode.caption',
        srfTitle: 'entities.emwo_inner.views.editview_editmode.title',
        srfSubTitle: 'entities.emwo_inner.views.editview_editmode.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMWO_INNEREditView_EditMode
     */
    public toolBarModels: any = {
        tbitem1: { name: 'tbitem1', caption: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.tbitem1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.tbitem1.tip', iconcls: 'fa fa-save', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Save', target: '', class: '' } },

        deuiaction2_submit: { name: 'deuiaction2_submit', caption: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.deuiaction2_submit.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.deuiaction2_submit.tip', iconcls: 'fa fa-check', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:1,dataaccaction: 'SUBMIT', uiaction: { tag: 'Submit', target: 'SINGLEKEY', class: '' } },

        deuiaction1: { name: 'deuiaction1', caption: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emwo_inner.editview_editmodetoolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
	protected viewtag: string = '7499f8fe484f4182cff42890c9cd0c58';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMWO_INNEREditView_EditModeBase
     */ 
    protected viewName: string = 'EMWO_INNEREditView_EditMode';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMWO_INNEREditView_EditModeBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emwo_inner',
            majorPSDEField: 'emwo_innername',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1')) {
            this.toolbar_tbitem1_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'deuiaction2_submit')) {
            this.toolbar_deuiaction2_submit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Save(datas, contextJO,paramJO,  $event, xData,this,"EMWO_INNER");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction2_submit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:EMWO_INNERUIService  = new EMWO_INNERUIService();
        curUIService.EMWO_INNER_Submit(datas,contextJO, paramJO,  $event, xData,this,"EMWO_INNER");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMWO_INNER");
    }

    /**
     * 保存
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public Save(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        // 界面行为容器对象 _this
        const _this: any = this;
        if (xData && xData.save instanceof Function) {
            xData.save().then((response: any) => {
                if (!response || response.status !== 200) {
                    return;
                }
                _this.$emit('viewdataschange', [{ ...response.data }]);
            });
        } else if (_this.save && _this.save instanceof Function) {
            _this.save();
        }
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMWO_INNEREditView_EditModeBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}