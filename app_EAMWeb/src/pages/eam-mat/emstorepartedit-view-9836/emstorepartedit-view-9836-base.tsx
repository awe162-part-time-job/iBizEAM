import { Subject } from 'rxjs';
import { UIActionTool, ViewTool, Util } from '@/utils';
import { EditViewBase } from '@/studio-core';
import EMStorePartService from '@/service/emstore-part/emstore-part-service';
import EMStorePartAuthService from '@/authservice/emstore-part/emstore-part-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import EMStorePartUIService from '@/uiservice/emstore-part/emstore-part-ui-service';

/**
 * 仓库库位视图基类
 *
 * @export
 * @class EMSTOREPARTEditView_9836Base
 * @extends {EditViewBase}
 */
export class EMSTOREPARTEditView_9836Base extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected appDeName: string = 'emstorepart';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected appDeKey: string = 'emstorepartid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected appDeMajor: string = 'emstorepartname';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */ 
    protected dataControl: string = 'form';

    /**
     * 实体服务对象
     *
     * @type {EMStorePartService}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected appEntityService: EMStorePartService = new EMStorePartService;

    /**
     * 实体权限服务对象
     *
     * @type EMStorePartUIService
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public appUIService: EMStorePartUIService = new EMStorePartUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof EMSTOREPARTEditView_9836Base
     */
    isShowDataInfoBar: boolean = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected model: any = {
        srfCaption: 'entities.emstorepart.views.editview_9836.caption',
        srfTitle: 'entities.emstorepart.views.editview_9836.title',
        srfSubTitle: 'entities.emstorepart.views.editview_9836.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    protected containerModel: any = {
        view_toolbar: {
            name: 'toolbar',
            type: 'TOOLBAR',
        },
        view_form: {
            name: 'form',
            type: 'FORM',
        },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof EMSTOREPARTEditView_9836
     */
    public toolBarModels: any = {
        deuiaction1: { name: 'deuiaction1', caption: 'entities.emstorepart.editview_9836toolbar_toolbar.deuiaction1.caption', 'isShowCaption': true, 'isShowIcon': true, tooltip: 'entities.emstorepart.editview_9836toolbar_toolbar.deuiaction1.tip', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visible: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */
	protected viewtag: string = '571dd277c558698c2ca05150b9eb7841';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOREPARTEditView_9836Base
     */ 
    protected viewName: string = 'EMSTOREPARTEditView_9836';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMSTOREPARTEditView_9836Base
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'emstorepart',
            majorPSDEField: 'emstorepartname',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'deuiaction1')) {
            this.toolbar_deuiaction1_click(null, '', $event2);
        }
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_deuiaction1_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this.$refs.form;
        if (xData.getDatas && xData.getDatas instanceof Function) {
            datas = [...xData.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"EMStorePart");
    }

    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof EMSTOREPARTEditView_9836Base
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}