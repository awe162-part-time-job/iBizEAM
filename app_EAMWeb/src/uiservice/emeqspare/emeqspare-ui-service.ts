import EMEQSpareUIServiceBase from './emeqspare-ui-service-base';

/**
 * 备件包UI服务对象
 *
 * @export
 * @class EMEQSpareUIService
 */
export default class EMEQSpareUIService extends EMEQSpareUIServiceBase {

    /**
     * Creates an instance of  EMEQSpareUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSpareUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}