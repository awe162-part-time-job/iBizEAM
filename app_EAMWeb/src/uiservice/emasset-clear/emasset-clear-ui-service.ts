import EMAssetClearUIServiceBase from './emasset-clear-ui-service-base';

/**
 * 资产清盘记录UI服务对象
 *
 * @export
 * @class EMAssetClearUIService
 */
export default class EMAssetClearUIService extends EMAssetClearUIServiceBase {

    /**
     * Creates an instance of  EMAssetClearUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClearUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}