import EMMachineCategoryUIServiceBase from './emmachine-category-ui-service-base';

/**
 * 机种编号UI服务对象
 *
 * @export
 * @class EMMachineCategoryUIService
 */
export default class EMMachineCategoryUIService extends EMMachineCategoryUIServiceBase {

    /**
     * Creates an instance of  EMMachineCategoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachineCategoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}