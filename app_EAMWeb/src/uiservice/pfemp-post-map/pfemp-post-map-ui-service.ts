import PFEmpPostMapUIServiceBase from './pfemp-post-map-ui-service-base';

/**
 * 人事关系UI服务对象
 *
 * @export
 * @class PFEmpPostMapUIService
 */
export default class PFEmpPostMapUIService extends PFEmpPostMapUIServiceBase {

    /**
     * Creates an instance of  PFEmpPostMapUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFEmpPostMapUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}