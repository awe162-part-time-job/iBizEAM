import EMMachModelUIServiceBase from './emmach-model-ui-service-base';

/**
 * 机型UI服务对象
 *
 * @export
 * @class EMMachModelUIService
 */
export default class EMMachModelUIService extends EMMachModelUIServiceBase {

    /**
     * Creates an instance of  EMMachModelUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMMachModelUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}