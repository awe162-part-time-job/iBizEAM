/**
 * AmountByTypeByEQ 部件模型
 *
 * @export
 * @class AmountByTypeByEQModel
 */
export default class AmountByTypeByEQModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof AmountByTypeByEQModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'price',
      },
      {
        name: 'amount',
      },
      {
        name: 'emresitemname',
      },
      {
        name: 'description',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'pnum',
      },
      {
        name: 'bdate',
      },
      {
        name: 'snum',
      },
      {
        name: 'createman',
      },
      {
        name: 'emresitem',
        prop: 'emresitemid',
      },
      {
        name: 'orgid',
      },
      {
        name: 'edate',
      },
      {
        name: 'enable',
      },
      {
        name: 'datafrom',
      },
      {
        name: 'createdate',
      },
      {
        name: 'resrefobjname',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'sname',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'itembtypeid',
      },
      {
        name: 'resname_show',
      },
      {
        name: 'itemmtypename',
      },
      {
        name: 'unitname',
      },
      {
        name: 'itemmtypeid',
      },
      {
        name: 'equipname',
      },
      {
        name: 'resname',
      },
      {
        name: 'itemtypeid',
      },
      {
        name: 'teamname',
      },
      {
        name: 'eqenable',
      },
      {
        name: 'resid',
      },
      {
        name: 'resrefobjid',
      },
      {
        name: 'equipid',
      },
    ]
  }


}
