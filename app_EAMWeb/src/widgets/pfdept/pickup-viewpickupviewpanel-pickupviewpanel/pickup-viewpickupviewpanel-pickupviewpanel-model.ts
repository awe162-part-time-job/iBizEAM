/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'deptcode',
      },
      {
        name: 'mgrempname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'createman',
      },
      {
        name: 'deptinfo',
      },
      {
        name: 'deptfn',
      },
      {
        name: 'updateman',
      },
      {
        name: 'maindeptcode',
      },
      {
        name: 'enable',
      },
      {
        name: 'deptpid',
      },
      {
        name: 'pfdept',
        prop: 'pfdeptid',
      },
      {
        name: 'mgrempid',
      },
      {
        name: 'pfdeptname',
      },
      {
        name: 'sdept',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'deptpname',
      },
    ]
  }


}