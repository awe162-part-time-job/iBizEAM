import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import Main3Service from './main3-form-service';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main3EditFormBase}
 */
export class Main3EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main3Service}
     * @memberof Main3EditFormBase
     */
    public service: Main3Service = new Main3Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof Main3EditFormBase
     */
    public appEntityService: EMEQTypeService = new EMEQTypeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeLogicName: string = '设备类型';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQTypeUIService}
     * @memberof Main3Base
     */  
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        eqtypecode: null,
        emeqtypename: null,
        eqtypepname: null,
        eqtypegroup: null,
        stype: null,
        sname: null,
        eqtypepid: null,
        emeqtypeid: null,
        emeqtype: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public majorMessageField: string = 'emeqtypename';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public rules(): any{
        return {
            eqtypecode: [
                {
                    required: this.detailsModel.eqtypecode.required,
                    type: 'string',
                    message: `${this.$t('entities.emeqtype.main3_form.details.eqtypecode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.eqtypecode.required,
                    type: 'string',
                    message: `${this.$t('entities.emeqtype.main3_form.details.eqtypecode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '设备类型信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emeqtype.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '设备类型标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '设备类型名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqtypecode: new FormItemModel({
    caption: '类型代码', detailType: 'FORMITEM', name: 'eqtypecode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        emeqtypename: new FormItemModel({
    caption: '设备类型名称', detailType: 'FORMITEM', name: 'emeqtypename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqtypepname: new FormItemModel({
    caption: '上级设备类型', detailType: 'FORMITEM', name: 'eqtypepname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqtypegroup: new FormItemModel({
    caption: '类型分组', detailType: 'FORMITEM', name: 'eqtypegroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        stype: new FormItemModel({
    caption: '统计归口分组', detailType: 'FORMITEM', name: 'stype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sname: new FormItemModel({
    caption: '统计归口名称', detailType: 'FORMITEM', name: 'sname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqtypepid: new FormItemModel({
    caption: '上级设备类型', detailType: 'FORMITEM', name: 'eqtypepid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emeqtypeid: new FormItemModel({
    caption: '设备类型标识', detailType: 'FORMITEM', name: 'emeqtypeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main3Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}