/**
 * TabExpViewtabviewpanel3 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel3Model
 */
export default class TabExpViewtabviewpanel3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel3Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'sdate',
      },
      {
        name: 'price',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'amount',
      },
      {
        name: 'updateman',
      },
      {
        name: 'batcode',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'psum',
      },
      {
        name: 'tradestate',
      },
      {
        name: 'sap',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'emitempl',
        prop: 'emitemplid',
      },
      {
        name: 'itemroutinfo',
      },
      {
        name: 'emitemplname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'sapreason1',
      },
      {
        name: 'inoutflag',
      },
      {
        name: 'description',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'sapcontrol',
      },
      {
        name: 'createman',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'storename',
      },
      {
        name: 'rname',
      },
      {
        name: 'rid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'sempid',
      },
      {
        name: 'sempname',
      },
    ]
  }


}