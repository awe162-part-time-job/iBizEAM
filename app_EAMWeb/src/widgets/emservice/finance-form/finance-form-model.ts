/**
 * Finance 部件模型
 *
 * @export
 * @class FinanceModel
 */
export default class FinanceModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof FinanceModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emservicename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'accode',
        prop: 'accode',
        dataType: 'TEXT',
      },
      {
        name: 'accodedesc',
        prop: 'accodedesc',
        dataType: 'TEXT',
      },
      {
        name: 'payway',
        prop: 'payway',
        dataType: 'SSCODELIST',
      },
      {
        name: 'paywaydesc',
        prop: 'paywaydesc',
        dataType: 'TEXT',
      },
      {
        name: 'taxcode',
        prop: 'taxcode',
        dataType: 'TEXT',
      },
      {
        name: 'taxtypeid',
        prop: 'taxtypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'taxdesc',
        prop: 'taxdesc',
        dataType: 'TEXT',
      },
      {
        name: 'emserviceid',
        prop: 'emserviceid',
        dataType: 'GUID',
      },
      {
        name: 'emservice',
        prop: 'emserviceid',
        dataType: 'FONTKEY',
      },
    ]
  }

}