import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import FinanceService from './finance-form-service';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {FinanceEditFormBase}
 */
export class FinanceEditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof FinanceEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {FinanceService}
     * @memberof FinanceEditFormBase
     */
    public service: FinanceService = new FinanceService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof FinanceEditFormBase
     */
    public appEntityService: EMServiceService = new EMServiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof FinanceEditFormBase
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof FinanceEditFormBase
     */
    protected appDeLogicName: string = '服务商';

    /**
     * 界面UI服务对象
     *
     * @type {EMServiceUIService}
     * @memberof FinanceBase
     */  
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof FinanceEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        accode: null,
        accodedesc: null,
        payway: null,
        paywaydesc: null,
        taxcode: null,
        taxtypeid: null,
        taxdesc: null,
        emserviceid: null,
        emservice: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof FinanceEditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof FinanceEditFormBase
     */
    public rules(): any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof FinanceBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof FinanceEditFormBase
     */
    public detailsModel: any = {
        grouppanel21: new FormGroupPanelModel({ caption: '财务信息', detailType: 'GROUPPANEL', name: 'grouppanel21', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emservice.finance_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '服务商标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '服务商名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        accode: new FormItemModel({
    caption: '帐号', detailType: 'FORMITEM', name: 'accode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        accodedesc: new FormItemModel({
    caption: '帐号备注', detailType: 'FORMITEM', name: 'accodedesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        payway: new FormItemModel({
    caption: '付款方式', detailType: 'FORMITEM', name: 'payway', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        paywaydesc: new FormItemModel({
    caption: '付款方式备注', detailType: 'FORMITEM', name: 'paywaydesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        taxcode: new FormItemModel({
    caption: '税码', detailType: 'FORMITEM', name: 'taxcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        taxtypeid: new FormItemModel({
    caption: '税类型', detailType: 'FORMITEM', name: 'taxtypeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        taxdesc: new FormItemModel({
    caption: '税备注', detailType: 'FORMITEM', name: 'taxdesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emserviceid: new FormItemModel({
    caption: '服务商标识', detailType: 'FORMITEM', name: 'emserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof FinanceBase
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}