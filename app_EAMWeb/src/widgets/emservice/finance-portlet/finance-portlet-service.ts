import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Finance 部件服务对象
 *
 * @export
 * @class FinanceService
 */
export default class FinanceService extends ControlService {
}
