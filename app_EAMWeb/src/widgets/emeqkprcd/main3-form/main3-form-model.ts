/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqkprcdid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqkprcdname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'kpname',
        prop: 'kpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'edate',
        prop: 'edate',
        dataType: 'DATETIME',
      },
      {
        name: 'val',
        prop: 'val',
        dataType: 'TEXT',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'woid',
        prop: 'woid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqkprcdid',
        prop: 'emeqkprcdid',
        dataType: 'GUID',
      },
      {
        name: 'kpid',
        prop: 'kpid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqkprcd',
        prop: 'emeqkprcdid',
        dataType: 'FONTKEY',
      },
    ]
  }

}