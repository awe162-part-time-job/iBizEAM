import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMWOService from '@/service/emwo/emwo-service';
import Main2Service from './main2-form-service';
import EMWOUIService from '@/uiservice/emwo/emwo-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main2EditFormBase}
 */
export class Main2EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main2Service}
     * @memberof Main2EditFormBase
     */
    public service: Main2Service = new Main2Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWOService}
     * @memberof Main2EditFormBase
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeName: string = 'emwo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main2EditFormBase
     */
    protected appDeLogicName: string = '工单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWOUIService}
     * @memberof Main2Base
     */  
    public appUIService: EMWOUIService = new EMWOUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main2EditFormBase
     */
    protected formKeyItemName: string = 'emwoid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emwoid: null,
        emwoname: null,
        wooriname: null,
        wooritype: null,
        emwotype: null,
        wogroup: null,
        wotype: null,
        wopname: null,
        wodate: null,
        expiredate: null,
        activelengths: null,
        priority: null,
        mdate: null,
        prefee: null,
        wodesc: null,
        equipname: null,
        objname: null,
        eqstoplength: null,
        dpname: null,
        rfodename: null,
        rfomoname: null,
        rfocaname: null,
        rfoacname: null,
        rempid: null,
        rempname: null,
        rdeptname: null,
        rteamname: null,
        rservicename: null,
        mpersonid: null,
        mpersonname: null,
        recvpersonid: null,
        recvpersonname: null,
        regionbegindate: null,
        regionenddate: null,
        waitbuy: null,
        waitmodi: null,
        worklength: null,
        wpersonid: null,
        wpersonname: null,
        val: null,
        vrate: null,
        wresult: null,
        orgid: null,
        archive: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        content: null,
        rfodeid: null,
        objid: null,
        wooriid: null,
        rfoacid: null,
        rserviceid: null,
        rfomoid: null,
        dpid: null,
        rfocaid: null,
        equipid: null,
        wopid: null,
        rteamid: null,
        emwo: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public majorMessageField: string = 'emwoname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public rules(): any{
        return {
            emwotype: [
                {
                    required: this.detailsModel.emwotype.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.emwotype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.emwotype.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.emwotype')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wogroup: [
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wogroup.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.wogroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            wodate: [
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.wodate.required,
                    type: 'string',
                    message: `${this.$t('entities.emwo.main2_form.details.wodate')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main2Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main2EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '工单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel18: new FormGroupPanelModel({ caption: '对象信息', detailType: 'GROUPPANEL', name: 'grouppanel18', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel23: new FormGroupPanelModel({ caption: '故障信息', detailType: 'GROUPPANEL', name: 'grouppanel23', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel28: new FormGroupPanelModel({ caption: '责任信息', detailType: 'GROUPPANEL', name: 'grouppanel28', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        grouppanel35: new FormGroupPanelModel({ caption: '执行信息', detailType: 'GROUPPANEL', name: 'grouppanel35', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel46: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel46', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emwo.main2_form', extractMode: 'ITEM', details: [] } }),

        formpage45: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage45', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        formpage54: new FormPageModel({ caption: '详细信息', detailType: 'FORMPAGE', name: 'formpage54', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '工单编号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwoid: new FormItemModel({
    caption: '工单号(自动)', detailType: 'FORMITEM', name: 'emwoid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        emwoname: new FormItemModel({
    caption: '工单名称', detailType: 'FORMITEM', name: 'emwoname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooriname: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooritype: new FormItemModel({
    caption: '来源类型', detailType: 'FORMITEM', name: 'wooritype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emwotype: new FormItemModel({
    caption: '工单种类', detailType: 'FORMITEM', name: 'emwotype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        wogroup: new FormItemModel({
    caption: '工单分组', detailType: 'FORMITEM', name: 'wogroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        wotype: new FormItemModel({
    caption: '工单类型', detailType: 'FORMITEM', name: 'wotype', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 1,
}),

        wopname: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodate: new FormItemModel({
    caption: '执行日期', detailType: 'FORMITEM', name: 'wodate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        expiredate: new FormItemModel({
    caption: '过期日期', detailType: 'FORMITEM', name: 'expiredate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        activelengths: new FormItemModel({
    caption: '持续时间(H)', detailType: 'FORMITEM', name: 'activelengths', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        priority: new FormItemModel({
    caption: '优先级', detailType: 'FORMITEM', name: 'priority', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mdate: new FormItemModel({
    caption: '制定时间', detailType: 'FORMITEM', name: 'mdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        prefee: new FormItemModel({
    caption: '预算(￥)', detailType: 'FORMITEM', name: 'prefee', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wodesc: new FormItemModel({
    caption: '工单内容', detailType: 'FORMITEM', name: 'wodesc', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipname: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqstoplength: new FormItemModel({
    caption: '停运时间(分)', detailType: 'FORMITEM', name: 'eqstoplength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpname: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfodename: new FormItemModel({
    caption: '现象', detailType: 'FORMITEM', name: 'rfodename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfomoname: new FormItemModel({
    caption: '模式', detailType: 'FORMITEM', name: 'rfomoname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfocaname: new FormItemModel({
    caption: '原因', detailType: 'FORMITEM', name: 'rfocaname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfoacname: new FormItemModel({
    caption: '方案', detailType: 'FORMITEM', name: 'rfoacname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempid: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rempname: new FormItemModel({
    caption: '责任人', detailType: 'FORMITEM', name: 'rempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rdeptname: new FormItemModel({
    caption: '责任部门', detailType: 'FORMITEM', name: 'rdeptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamname: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rservicename: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rservicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonid: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        mpersonname: new FormItemModel({
    caption: '制定人', detailType: 'FORMITEM', name: 'mpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonid: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        recvpersonname: new FormItemModel({
    caption: '接收人', detailType: 'FORMITEM', name: 'recvpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionbegindate: new FormItemModel({
    caption: '起始时间', detailType: 'FORMITEM', name: 'regionbegindate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        regionenddate: new FormItemModel({
    caption: '结束时间', detailType: 'FORMITEM', name: 'regionenddate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        waitbuy: new FormItemModel({
    caption: '等待配件时(小时)', detailType: 'FORMITEM', name: 'waitbuy', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        waitmodi: new FormItemModel({
    caption: '等待修理时(小时)', detailType: 'FORMITEM', name: 'waitmodi', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        worklength: new FormItemModel({
    caption: '实际工时(分)', detailType: 'FORMITEM', name: 'worklength', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonid: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wpersonname: new FormItemModel({
    caption: '执行人', detailType: 'FORMITEM', name: 'wpersonname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        val: new FormItemModel({
    caption: '值', detailType: 'FORMITEM', name: 'val', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        vrate: new FormItemModel({
    caption: '倍率', detailType: 'FORMITEM', name: 'vrate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wresult: new FormItemModel({
    caption: '执行结果', detailType: 'FORMITEM', name: 'wresult', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        archive: new FormItemModel({
    caption: '归档', detailType: 'FORMITEM', name: 'archive', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        content: new FormItemModel({
    caption: '详细内容', detailType: 'FORMITEM', name: 'content', visible: true, isShowCaption: false, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfodeid: new FormItemModel({
    caption: '现象', detailType: 'FORMITEM', name: 'rfodeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        objid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'objid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wooriid: new FormItemModel({
    caption: '工单来源', detailType: 'FORMITEM', name: 'wooriid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfoacid: new FormItemModel({
    caption: '方案', detailType: 'FORMITEM', name: 'rfoacid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rserviceid: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'rserviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfomoid: new FormItemModel({
    caption: '模式', detailType: 'FORMITEM', name: 'rfomoid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        dpid: new FormItemModel({
    caption: '测点', detailType: 'FORMITEM', name: 'dpid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rfocaid: new FormItemModel({
    caption: '原因', detailType: 'FORMITEM', name: 'rfocaid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipid: new FormItemModel({
    caption: '设备', detailType: 'FORMITEM', name: 'equipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        wopid: new FormItemModel({
    caption: '上级工单', detailType: 'FORMITEM', name: 'wopid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rteamid: new FormItemModel({
    caption: '责任班组', detailType: 'FORMITEM', name: 'rteamid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage45',
                    index: 1,
                    visible: true,
                },
                {
                    name: 'formpage54',
                    index: 2,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main2Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}