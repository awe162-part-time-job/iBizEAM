/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'batcode',
      },
      {
        name: 'emstock',
        prop: 'emstockid',
      },
      {
        name: 'storepartgl',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'stockinfo',
      },
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'emstockname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'amount',
      },
      {
        name: 'description',
      },
      {
        name: 'updateman',
      },
      {
        name: 'stockcnt',
      },
      {
        name: 'storepartname',
      },
      {
        name: 'storename',
      },
      {
        name: 'price',
      },
      {
        name: 'itemname',
      },
      {
        name: 'itembtypename',
      },
      {
        name: 'storepartid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'storeid',
      },
      {
        name: 'itembtypeid',
      },
    ]
  }


}