import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import EMPlanDetailService from '@/service/emplan-detail/emplan-detail-service';
import ByPlanService from './by-plan-list-service';
import EMPlanDetailUIService from '@/uiservice/emplan-detail/emplan-detail-ui-service';

/**
 * dashboard_sysportlet4_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {ByPlanListBase}
 */
export class ByPlanListBase extends ListControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByPlanListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {ByPlanService}
     * @memberof ByPlanListBase
     */
    public service: ByPlanService = new ByPlanService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMPlanDetailService}
     * @memberof ByPlanListBase
     */
    public appEntityService: EMPlanDetailService = new EMPlanDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByPlanListBase
     */
    protected appDeName: string = 'emplandetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByPlanListBase
     */
    protected appDeLogicName: string = '计划步骤';

    /**
     * 界面UI服务对象
     *
     * @type {EMPlanDetailUIService}
     * @memberof ByPlanBase
     */  
    public appUIService: EMPlanDetailUIService = new EMPlanDetailUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ByPlanListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof ByPlanListBase
     */
    public minorSortDir: string = 'ASC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof ByPlanListBase
     */
    public minorSortPSDEF: string = 'orderflag';




}