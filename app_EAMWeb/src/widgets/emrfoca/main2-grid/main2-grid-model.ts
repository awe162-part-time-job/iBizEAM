/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'rfocacode',
          prop: 'rfocacode',
          dataType: 'TEXT',
        },
        {
          name: 'emrfocaname',
          prop: 'emrfocaname',
          dataType: 'TEXT',
        },
        {
          name: 'rfodename',
          prop: 'rfodename',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'rfomoname',
          prop: 'rfomoname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'rfodeid',
          prop: 'rfodeid',
          dataType: 'PICKUP',
        },
        {
          name: 'rfomoid',
          prop: 'rfomoid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emrfocaname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emrfocaid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emrfocaid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emrfoca',
          prop: 'emrfocaid',
        },
      {
        name: 'n_emrfocaname_like',
        prop: 'n_emrfocaname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_rfodename_like',
        prop: 'n_rfodename_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_rfomoname_like',
        prop: 'n_rfomoname_like',
        dataType: 'PICKUPTEXT',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}