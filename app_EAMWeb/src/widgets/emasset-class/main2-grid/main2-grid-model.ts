/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'emassetclassname',
          prop: 'emassetclassname',
          dataType: 'TEXT',
        },
        {
          name: 'assetclasscode',
          prop: 'assetclasscode',
          dataType: 'TEXT',
        },
        {
          name: 'life',
          prop: 'life',
          dataType: 'FLOAT',
        },
        {
          name: 'assetclasspname',
          prop: 'assetclasspname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'assetclasspcode',
          prop: 'assetclasspcode',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'assetclassgroup',
          prop: 'assetclassgroup',
          dataType: 'SSCODELIST',
        },
        {
          name: 'assetclasspid',
          prop: 'assetclasspid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emassetclassname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emassetclassid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emassetclassid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emassetclass',
          prop: 'emassetclassid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}