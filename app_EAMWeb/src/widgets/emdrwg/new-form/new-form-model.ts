/**
 * New 部件模型
 *
 * @export
 * @class NewModel
 */
export default class NewModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof NewModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emdrwgid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emdrwgname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'drwgcode',
        prop: 'drwgcode',
        dataType: 'TEXT',
      },
      {
        name: 'emdrwgname',
        prop: 'emdrwgname',
        dataType: 'TEXT',
      },
      {
        name: 'drwgtype',
        prop: 'drwgtype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'drwgstate',
        prop: 'drwgstate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'TEXT',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'TEXT',
      },
      {
        name: 'bpersonname',
        prop: 'bpersonname',
        dataType: 'TEXT',
      },
      {
        name: 'bpersonid',
        prop: 'bpersonid',
        dataType: 'TEXT',
      },
      {
        name: 'lct',
        prop: 'lct',
        dataType: 'TEXT',
      },
      {
        name: 'emdrwgid',
        prop: 'emdrwgid',
        dataType: 'GUID',
      },
      {
        name: 'emdrwg',
        prop: 'emdrwgid',
        dataType: 'FONTKEY',
      },
    ]
  }

}