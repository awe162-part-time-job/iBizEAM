/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqsparemapid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqsparemapname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eqsparename',
        prop: 'eqsparename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'refobjname',
        prop: 'refobjname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emeqsparemapid',
        prop: 'emeqsparemapid',
        dataType: 'GUID',
      },
      {
        name: 'refobjid',
        prop: 'refobjid',
        dataType: 'PICKUP',
      },
      {
        name: 'eqspareid',
        prop: 'eqspareid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqsparemap',
        prop: 'emeqsparemapid',
        dataType: 'FONTKEY',
      },
    ]
  }

}