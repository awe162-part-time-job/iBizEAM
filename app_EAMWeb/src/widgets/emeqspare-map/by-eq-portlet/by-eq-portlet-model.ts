/**
 * ByEQ 部件模型
 *
 * @export
 * @class ByEQModel
 */
export default class ByEQModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ByEQModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emeqsparemap',
        prop: 'emeqsparemapid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'enable',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emeqsparemapname',
      },
      {
        name: 'refobjname',
      },
      {
        name: 'eqsparename',
      },
      {
        name: 'refobjid',
      },
      {
        name: 'eqspareid',
      },
    ]
  }


}
