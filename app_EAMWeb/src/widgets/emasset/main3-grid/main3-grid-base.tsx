import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import EMAssetService from '@/service/emasset/emasset-service';
import Main3Service from './main3-grid-service';
import EMAssetUIService from '@/uiservice/emasset/emasset-ui-service';
import { FormItemModel } from '@/model/form-detail';

/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {Main3GridBase}
 */
export class Main3GridBase extends GridControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main3GridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {Main3Service}
     * @memberof Main3GridBase
     */
    public service: Main3Service = new Main3Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMAssetService}
     * @memberof Main3GridBase
     */
    public appEntityService: EMAssetService = new EMAssetService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main3GridBase
     */
    protected appDeName: string = 'emasset';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main3GridBase
     */
    protected appDeLogicName: string = '资产';

    /**
     * 界面UI服务对象
     *
     * @type {EMAssetUIService}
     * @memberof Main3Base
     */  
    public appUIService: EMAssetUIService = new EMAssetUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof Main3Base
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof Main3Base
     */  
    public majorInfoColName:string = "emassetname";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof Main3Base
     */
    protected localStorageTag: string = 'emasset_main3_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof Main3GridBase
     */
    public allColumns: any[] = [
        {
            name: 'assetsort',
            label: '排序',
            langtag: 'entities.emasset.main3_grid.columns.assetsort',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'assetcode',
            label: '资产代码',
            langtag: 'entities.emasset.main3_grid.columns.assetcode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'emassetname',
            label: '资产名称',
            langtag: 'entities.emasset.main3_grid.columns.emassetname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'num',
            label: '第几号',
            langtag: 'entities.emasset.main3_grid.columns.num',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'assettype',
            label: '资产类别',
            langtag: 'entities.emasset.main3_grid.columns.assettype',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'assetlct',
            label: '存放地点',
            langtag: 'entities.emasset.main3_grid.columns.assetlct',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'eqmodelcode',
            label: '规格型号',
            langtag: 'entities.emasset.main3_grid.columns.eqmodelcode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'assetclassname',
            label: '资产科目',
            langtag: 'entities.emasset.main3_grid.columns.assetclassname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'assetstate',
            label: '资产状态',
            langtag: 'entities.emasset.main3_grid.columns.assetstate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'eqlife',
            label: '使用期限',
            langtag: 'entities.emasset.main3_grid.columns.eqlife',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'eqstartdate',
            label: '投运日期',
            langtag: 'entities.emasset.main3_grid.columns.eqstartdate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'originalcost',
            label: '资产原值',
            langtag: 'entities.emasset.main3_grid.columns.originalcost',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'deptname',
            label: '使用部门',
            langtag: 'entities.emasset.main3_grid.columns.deptname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'purchdate',
            label: '采购日期',
            langtag: 'entities.emasset.main3_grid.columns.purchdate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'empname',
            label: '使用人',
            langtag: 'entities.emasset.main3_grid.columns.empname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'unitname',
            label: '单位',
            langtag: 'entities.emasset.main3_grid.columns.unitname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'ytzj',
            label: '已提折旧',
            langtag: 'entities.emasset.main3_grid.columns.ytzj',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'replacerate',
            label: '残值率(%)',
            langtag: 'entities.emasset.main3_grid.columns.replacerate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'mgrdeptname',
            label: '管理部门',
            langtag: 'entities.emasset.main3_grid.columns.mgrdeptname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'rempname',
            label: '经办人',
            langtag: 'entities.emasset.main3_grid.columns.rempname',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'eqserialcode',
            label: '车架号',
            langtag: 'entities.emasset.main3_grid.columns.eqserialcode',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'blsystemdesc',
            label: '发动机号',
            langtag: 'entities.emasset.main3_grid.columns.blsystemdesc',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'keyattparam',
            label: '设备代码',
            langtag: 'entities.emasset.main3_grid.columns.keyattparam',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'now',
            label: '资产余值',
            langtag: 'entities.emasset.main3_grid.columns.now',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'replacecost',
            label: '预计残值',
            langtag: 'entities.emasset.main3_grid.columns.replacecost',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'lastzjdate',
            label: '最后折旧日期',
            langtag: 'entities.emasset.main3_grid.columns.lastzjdate',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'syqx',
            label: '剩余期限',
            langtag: 'entities.emasset.main3_grid.columns.syqx',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'jtsb',
            label: '集团设备编码',
            langtag: 'entities.emasset.main3_grid.columns.jtsb',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof Main3GridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 是否启用分组
     *
     * @type {boolean}
     * @memberof Main3Base
     */
    public isEnableGroup:boolean = false;

    /**
     * 分组属性
     *
     * @type {string}
     * @memberof Main3Base
     */
    public groupAppField:string ="";

    /**
     * 分组属性代码表标识
     *
     * @type {string}
     * @memberof Main3Base
     */
    public groupAppFieldCodelistTag:string ="";

    /**
     * 分组属性代码表类型
     * 
     * @type {string}
     * @memberof Main3Base
     */
    public groupAppFieldCodelistType: string = "";

    /**
     * 分组模式
     *
     * @type {string}
     * @memberof Main3Base
     */
    public groupMode:string ="NONE";

    /**
     * 分组代码表标识
     * 
     * @type {string}
     * @memberof Main3Base
     */
    public codelistTag: string = "";

    /**
     * 分组代码表类型
     * 
     * @type {string}
     * @memberof Main3Base
     */
    public codelistType: string = "";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3GridBase
     */
    public rules() {
        return {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '资产标识 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '资产标识 值不能为空', trigger: 'blur' },
        ],
    }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3Base
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof Main3Base
     */
    public hasRowEdit: any = {
        'assetsort':false,
        'assetcode':false,
        'emassetname':false,
        'num':false,
        'assettype':false,
        'assetlct':false,
        'eqmodelcode':false,
        'assetclassname':false,
        'assetstate':false,
        'eqlife':false,
        'eqstartdate':false,
        'originalcost':false,
        'deptname':false,
        'purchdate':false,
        'empname':false,
        'unitname':false,
        'ytzj':false,
        'replacerate':false,
        'mgrdeptname':false,
        'rempname':false,
        'eqserialcode':false,
        'blsystemdesc':false,
        'keyattparam':false,
        'now':false,
        'replacecost':false,
        'lastzjdate':false,
        'syqx':false,
        'jtsb':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof Main3Base
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        let className: string = '';
        if(args.column.property){
          let col = this.allColumns.find((item:any)=>{
              return Object.is(args.column.property,item.name);
          })
          if(col !== undefined){
              if(col.isEnableRowEdit && this.actualIsOpenEdit ){
                className += 'edit-cell ';
              }
          } else {
              className += 'info-cell';
          }
        }
        if(this.groupAppField && args.columnIndex === 0 && !this.isSingleSelect) {
            if(args.row.children && args.row.children.length > 0) {
                className += this.computeGroupRow(args.row.children, args.row);
            }
        }
        return className;
    }
    
    /**
     * 计算分组行checkbox选中样式
     *
     * @param {*} rows 当前分组行下的所有数据
     * @returns {*} currentRow 当前分组行
     * @memberof MainBase
     */
    public computeGroupRow(rows: any[], currentRow: any) {
        let count: number = 0;
        this.selections.forEach((select: any) => {
            rows.forEach((row: any) => {
                if(row.groupById === select.groupById) {
                    count++;
                }
            })
        })
        if(count === rows.length) {
            (this.$refs.multipleTable as any).toggleRowSelection(currentRow, true);
            return 'cell-select-all ';
        } else if(count !== 0 && count < rows.length) {
            return 'cell-indeterminate '
        } else if(count === 0) {
            (this.$refs.multipleTable as any).toggleRowSelection(currentRow, false);
            return '';
        }
    }

    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof Main3GridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'assettype',
                srfkey: 'EMASSETTYPE',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
            {
                name: 'assetstate',
                srfkey: 'EMASSETSTATE',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof Main3Base
     */
    public updateDefault(row: any){                    
    }

    /**
    * 合并分组行
    * 
    * @memberof Main3Base
    */
    public arraySpanMethod({row, column, rowIndex, columnIndex} : any) {
        let allColumns:Array<any> = ['assetsort','assetcode','emassetname','num','assettype','assetlct','eqmodelcode','assetclassname','assetstate','eqlife','eqstartdate','originalcost','deptname','purchdate','empname','unitname','ytzj','replacerate','mgrdeptname','rempname','eqserialcode','blsystemdesc','keyattparam','now','replacecost','lastzjdate','syqx','jtsb'];
        if(row && row.children) {
            if(columnIndex == (this.isSingleSelect ? 0:1)) {
                return [1, allColumns.length+1];
            } else if(columnIndex > (this.isSingleSelect ? 0:1)) {
                return [0,0];
            }
        }
    }

	/**
     * 分组方法
     * 
     * @memberof Main3Base
     */
    public group(){
        if(Object.is(this.groupMode,"AUTO")){
            this.drawGroup();
        }else if(Object.is(this.groupMode,"CODELIST")){
            this.drawCodelistGroup();
        }
    }

    /**
     * 获取表格分组相关代码表
     * 
     * @param {string}  codelistType 代码表类型
     * @param {string}  codelistTag 代码表标识
     * @memberof Main3Base
     */
    public async getGroupCodelist(codelistType: string,codelistTag:string){
        let codelist: Array<any> = [];
        // 动态代码表
        if (Object.is(codelistType, "DYNAMIC")) {
             codelist = await this.codeListService.getItems(codelistTag);
        // 静态代码表
        } else if(Object.is(codelistType, "STATIC")){
            codelist = this.$store.getters.getCodeListItems(codelistTag);
        }
        return codelist;
    }

    /**
     * 根据分组代码表绘制分组列表
     * 
     * @memberof Main3Base
     */
    public async drawCodelistGroup(){
        if(!this.isEnableGroup) return;
        // 分组
        let allGroup: Array<any> = [];
        let allGroupField: Array<any> =[];
        let groupTree:Array<any> = [];
        allGroup = await this.getGroupCodelist(this.codelistType,this.codelistTag);
        allGroupField = await this.getGroupCodelist(this.groupAppFieldCodelistType,this.groupAppFieldCodelistTag);
        if(allGroup.length == 0){
            console.warn("分组数据无效");
        }
        allGroup.forEach((group: any,i: number)=>{
            let children:Array<any> = [];
            this.items.forEach((item: any,j: number)=>{
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    if(arr && arr.length>0) {
                        if(Object.is(group.value,arr[0].value)){
                            item.groupById = Number((i+1) * 100 + (j+1) * 1);
                            item.group = '';
                            children.push(item);
                        }
                    }
                }else if(Object.is(group.value,item[this.groupAppField])){
                    item.groupById = Number((i+1) * 100 + (j+1) * 1);
                    item.group = '';
                    children.push(item);
                }
            });
            const tree: any ={
                groupById: Number((i+1)*100),
                group: group.label,
                assetsort:'',
                assetcode:'',
                emassetname:'',
                num:'',
                assettype:'',
                assetlct:'',
                eqmodelcode:'',
                assetclassname:'',
                assetstate:'',
                eqlife:'',
                eqstartdate:'',
                originalcost:'',
                deptname:'',
                purchdate:'',
                empname:'',
                unitname:'',
                ytzj:'',
                replacerate:'',
                mgrdeptname:'',
                rempname:'',
                eqserialcode:'',
                blsystemdesc:'',
                keyattparam:'',
                now:'',
                replacecost:'',
                lastzjdate:'',
                syqx:'',
                jtsb:'',
                children: children
            }
            groupTree.push(tree);
        });
        let child:Array<any> = [];
        this.items.forEach((item: any,index: number)=>{
            let i: number = 0;
            if(allGroupField && allGroupField.length > 0){
                const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                if(arr && arr.length>0) {
                    i = allGroup.findIndex((group: any)=>Object.is(group.value,arr[0].value));
                }
            }else{
                i = allGroup.findIndex((group: any)=>Object.is(group.value,item[this.groupAppField]));
            }
            if(i < 0){
                item.groupById = Number((allGroup.length+1) * 100 + (index+1) * 1);
                item.group = '';
                child.push(item);
            }
        })
        const Tree: any = {
            groupById: Number((allGroup.length+1)*100),
            group: '其他',
            assetsort:'',
            assetcode:'',
            emassetname:'',
            num:'',
            assettype:'',
            assetlct:'',
            eqmodelcode:'',
            assetclassname:'',
            assetstate:'',
            eqlife:'',
            eqstartdate:'',
            originalcost:'',
            deptname:'',
            purchdate:'',
            empname:'',
            unitname:'',
            ytzj:'',
            replacerate:'',
            mgrdeptname:'',
            rempname:'',
            eqserialcode:'',
            blsystemdesc:'',
            keyattparam:'',
            now:'',
            replacecost:'',
            lastzjdate:'',
            syqx:'',
            jtsb:'',
            children: child
        }
        if(child && child.length > 0){
            groupTree.push(Tree);
        }
        this.items = groupTree;
        if(this.actualIsOpenEdit) {
            for(let i = 0; i < this.items.length; i++) {
                this.gridItemsModel.push(this.getGridRowModel());
            }
        }
    }

    /**
     * 绘制分组
     * 
     * @memberof Main3Base
     */
    public async drawGroup(){
        if(!this.isEnableGroup) return;
        // 分组
        let allGroup: Array<any> = [];
        let allGroupField: Array<any> =[];
        allGroupField = await this.getGroupCodelist(this.groupAppFieldCodelistType,this.groupAppFieldCodelistTag);
        this.items.forEach((item: any)=>{
            if(item.hasOwnProperty(this.groupAppField)){
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    allGroup.push(arr[0].label);
                }else{
                    allGroup.push(item[this.groupAppField]);
                }
            }
        });
        let groupTree:Array<any> = [];
        allGroup = [...new Set(allGroup)];
        if(allGroup.length == 0){
            console.warn("分组数据无效");
        }
        // 组装数据
        allGroup.forEach((group: any, groupIndex: number)=>{
            let children:Array<any> = [];
            this.items.forEach((item: any,itemIndex: number)=>{
                if(allGroupField && allGroupField.length > 0){
                    const arr:Array<any> = allGroupField.filter((field:any)=>{return field.value == item[this.groupAppField]});
                    if(Object.is(group,arr[0].label)){
                        item.groupById = Number((groupIndex+1) * 100 + (itemIndex+1) * 1);
                        item.group = '';
                        children.push(item);
                    }
                }else if(Object.is(group,item[this.groupAppField])){
                    item.groupById = Number((groupIndex+1) * 100 + (itemIndex+1) * 1);
                    item.group = '';
                    children.push(item);
                }
            });
            group = group ? group : '其他';
            const tree: any ={
                groupById: Number((groupIndex+1)*100),
                group: group,
                assetsort:'',
                assetcode:'',
                emassetname:'',
                num:'',
                assettype:'',
                assetlct:'',
                eqmodelcode:'',
                assetclassname:'',
                assetstate:'',
                eqlife:'',
                eqstartdate:'',
                originalcost:'',
                deptname:'',
                purchdate:'',
                empname:'',
                unitname:'',
                ytzj:'',
                replacerate:'',
                mgrdeptname:'',
                rempname:'',
                eqserialcode:'',
                blsystemdesc:'',
                keyattparam:'',
                now:'',
                replacecost:'',
                lastzjdate:'',
                syqx:'',
                jtsb:'',
                children: children,
            }
            groupTree.push(tree);
        });
        this.items = groupTree;
        if(this.actualIsOpenEdit) {
            for(let i = 0; i < this.items.length; i++) {
                this.gridItemsModel.push(this.getGridRowModel());
            }
        }
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof Main3Base
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.emassetid === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}