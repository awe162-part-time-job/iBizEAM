/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'gssxm',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'eqmodelcode',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'valve',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'len',
      },
      {
        name: 'zj',
      },
      {
        name: 'orgid',
      },
      {
        name: 'equipname',
      },
      {
        name: 'eqlocationinfo',
      },
      {
        name: 'emeqlctgss',
        prop: 'emeqlocationid',
      },
      {
        name: 'equipid',
      },
    ]
  }


}