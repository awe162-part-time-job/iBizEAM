/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqlocationid',
        dataType: 'PICKUP',
      },
      {
        name: 'srfmajortext',
        prop: 'eqlocationinfo',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emeqlocationid',
        prop: 'emeqlocationid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'gssxm',
        prop: 'gssxm',
        dataType: 'SSCODELIST',
      },
      {
        name: 'eqmodelcode',
        prop: 'eqmodelcode',
        dataType: 'TEXT',
      },
      {
        name: 'zj',
        prop: 'zj',
        dataType: 'FLOAT',
      },
      {
        name: 'len',
        prop: 'len',
        dataType: 'FLOAT',
      },
      {
        name: 'valve',
        prop: 'valve',
        dataType: 'INT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'emeqlctgss',
        prop: 'emeqlocationid',
        dataType: 'FONTKEY',
      },
    ]
  }

}