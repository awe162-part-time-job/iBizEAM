/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emstoreid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emstorename',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'storecode',
        prop: 'storecode',
        dataType: 'TEXT',
      },
      {
        name: 'emstoreid',
        prop: 'emstoreid',
        dataType: 'GUID',
      },
      {
        name: 'emstorename',
        prop: 'emstorename',
        dataType: 'TEXT',
      },
      {
        name: 'storetypeid',
        prop: 'storetypeid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'PICKUP',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emstore',
        prop: 'emstoreid',
        dataType: 'FONTKEY',
      },
    ]
  }

}