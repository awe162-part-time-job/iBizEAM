/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'pic9',
      },
      {
        name: 'efcheck',
      },
      {
        name: 'efcheckndate',
      },
      {
        name: 'replacecost',
      },
      {
        name: 'eqpriority',
      },
      {
        name: 'efficiency_y',
      },
      {
        name: 'intactrate_y',
      },
      {
        name: 'intactrate_q',
      },
      {
        name: 'efficiency_m',
      },
      {
        name: 'emequipname',
      },
      {
        name: 'emequip',
        prop: 'emequipid',
      },
      {
        name: 'efcheckdesc',
      },
      {
        name: 'failurerate_m',
      },
      {
        name: 'deptid',
      },
      {
        name: 'pic6',
      },
      {
        name: 'materialcost',
      },
      {
        name: 'haltstate',
      },
      {
        name: 'intactrate_m',
      },
      {
        name: 'pic8',
      },
      {
        name: 'purchdate',
      },
      {
        name: 'haltcause',
      },
      {
        name: 'productparam',
      },
      {
        name: 'pic4',
      },
      {
        name: 'equip_bh',
      },
      {
        name: 'maintenancecost',
      },
      {
        name: 'eqstate',
      },
      {
        name: 'eqlife',
      },
      {
        name: 'originalcost',
      },
      {
        name: 'createdate',
      },
      {
        name: 'params',
      },
      {
        name: 'updateman',
      },
      {
        name: 'outputrct_dj',
      },
      {
        name: 'eqserialcode',
      },
      {
        name: 'orgid',
      },
      {
        name: 'pic2',
      },
      {
        name: 'pic3',
      },
      {
        name: 'equipcode',
      },
      {
        name: 'outputrct_dy',
      },
      {
        name: 'costcenterid',
      },
      {
        name: 'eqstartdate',
      },
      {
        name: 'warrantydate',
      },
      {
        name: 'eqisservice1',
      },
      {
        name: 'failurerate_q',
      },
      {
        name: 'pic7',
      },
      {
        name: 'pic',
      },
      {
        name: 'blsystemdesc',
      },
      {
        name: 'efficiency_q',
      },
      {
        name: 'eqmodelcode',
      },
      {
        name: 'pic5',
      },
      {
        name: 'equipdesc',
      },
      {
        name: 'description',
      },
      {
        name: 'enable',
      },
      {
        name: 'createman',
      },
      {
        name: 'innerlaborcost',
      },
      {
        name: 'keyattparam',
      },
      {
        name: 'empid',
      },
      {
        name: 'techcode',
      },
      {
        name: 'outputrct_dn',
      },
      {
        name: 'eqisservice',
      },
      {
        name: 'failurerate_y',
      },
      {
        name: 'efcheckdate',
      },
      {
        name: 'foreignlaborcost',
      },
      {
        name: 'equipgroup',
      },
      {
        name: 'deptname',
      },
      {
        name: 'haltstateinfo',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'efcheckcdate',
      },
      {
        name: 'equipinfo',
      },
      {
        name: 'eqsumstoptime',
      },
      {
        name: 'empname',
      },
      {
        name: 'emberthcode',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'jzbh1',
      },
      {
        name: 'emmachinecategoryname',
      },
      {
        name: 'stype',
      },
      {
        name: 'mservicename',
      },
      {
        name: 'eqtypecode',
      },
      {
        name: 'assetclasscode',
      },
      {
        name: 'equippcode',
      },
      {
        name: 'eqtypename',
      },
      {
        name: 'emmachmodelname',
      },
      {
        name: 'embrandcode',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'assetname',
      },
      {
        name: 'assetclassname',
      },
      {
        name: 'embrandname',
      },
      {
        name: 'eqlocationname',
      },
      {
        name: 'equippname',
      },
      {
        name: 'assetclassid',
      },
      {
        name: 'contractname',
      },
      {
        name: 'emberthname',
      },
      {
        name: 'assetcode',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'eqlocationcode',
      },
      {
        name: 'machtypecode',
      },
      {
        name: 'eqtypepid',
      },
      {
        name: 'sname',
      },
      {
        name: 'eqlocationid',
      },
      {
        name: 'emmachinecategoryid',
      },
      {
        name: 'emberthid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'embrandid',
      },
      {
        name: 'equippid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'emmachmodelid',
      },
      {
        name: 'contractid',
      },
      {
        name: 'eqtypeid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'assetid',
      },
      {
        name: 'mserviceid',
      },
    ]
  }


}