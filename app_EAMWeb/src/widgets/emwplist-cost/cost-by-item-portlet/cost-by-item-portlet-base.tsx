import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, MainControlBase } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import CostByItemService from './cost-by-item-portlet-service';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';
import { Environment } from '@/environments/environment';
import UIService from '@/uiservice/ui-service';

/**
 * dashboard_sysportlet6部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {CostByItemPortletBase}
 */
export class CostByItemPortletBase extends MainControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPortletBase
     */
    protected controlType: string = 'PORTLET';

    /**
     * 建构部件服务对象
     *
     * @type {CostByItemService}
     * @memberof CostByItemPortletBase
     */
    public service: CostByItemService = new CostByItemService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof CostByItemPortletBase
     */
    public appEntityService: EMWPListCostService = new EMWPListCostService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPortletBase
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemPortletBase
     */
    protected appDeLogicName: string = '询价单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListCostUIService}
     * @memberof CostByItemBase
     */  
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);


    /**
     * 长度
     *
     * @type {number}
     * @memberof CostByItem
     */
    @Prop() public height?: number;

    /**
     * 宽度
     *
     * @type {number}
     * @memberof CostByItem
     */
    @Prop() public width?: number;

    /**
     * 门户部件类型
     *
     * @type {number}
     * @memberof CostByItemBase
     */
    public portletType: string = 'list';

    /**
     * 界面行为模型数据
     *
     * @memberof CostByItemBase
     */
    public uiactionModel: any = {
    }



    /**
     * 是否自适应大小
     *
     * @returns {boolean}
     * @memberof CostByItemBase
     */
    @Prop({default: false})public isAdaptiveSize!: boolean;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof CostByItemBase
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof CostByItemBase
     */
    public getData(): any {
        return {};
    }

    /**
     * 获取高度
     *
     * @returns {any[]}
     * @memberof CostByItemBase
     */
    get getHeight(): any{
        if(!this.$util.isEmpty(this.height) && !this.$util.isNumberNaN(this.height)){
            if(this.height == 0){
                return 'auto';
            } else {
                return this.height+'px';
            }
        } else {
            return 'auto';
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof CostByItemBase
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof CostByItemBase
     */    
    public afterCreated(){
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if(Object.is(tag, "all-portlet") && Object.is(action,'loadmodel')){
                   this.calcUIActionAuthState(data);
                }
                if (!Object.is(tag, this.name)) {
                    return;
                }
                const refs: any = this.$refs;
                Object.keys(refs).forEach((_name: string) => {
                    this.viewState.next({ tag: _name, action: action, data: data });
                });
            });
        }
    }

    /**
     * vue 生命周期
     *
     * @memberof CostByItemBase
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof CostByItemBase
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }

    /**
     * 计算界面行为权限
     *
     * @memberof CostByItemBase
     */
    public calcUIActionAuthState(data:any = {}) {
        //  如果是操作栏，不计算权限
        if(this.portletType && Object.is('actionbar', this.portletType)) {
            return;
        }
        let _this: any = this;
        let uiservice: any = _this.appUIService ? _this.appUIService : new UIService(_this.$store);
        if(_this.uiactionModel){
            ViewTool.calcActionItemAuthState(data,_this.uiactionModel,uiservice);
        }
    }


    /**
     * 刷新
     *
     * @memberof CostByItemBase
     */
    public refresh(args?: any) {
      this.viewState.next({ tag: 'dashboard_sysportlet6_list', action: 'refresh', data: args });
    }

}
