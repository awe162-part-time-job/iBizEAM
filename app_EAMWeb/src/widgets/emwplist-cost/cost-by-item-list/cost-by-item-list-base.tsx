import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import EMWPListCostService from '@/service/emwplist-cost/emwplist-cost-service';
import CostByItemService from './cost-by-item-list-service';
import EMWPListCostUIService from '@/uiservice/emwplist-cost/emwplist-cost-ui-service';

/**
 * dashboard_sysportlet6_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {CostByItemListBase}
 */
export class CostByItemListBase extends ListControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof CostByItemListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {CostByItemService}
     * @memberof CostByItemListBase
     */
    public service: CostByItemService = new CostByItemService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMWPListCostService}
     * @memberof CostByItemListBase
     */
    public appEntityService: EMWPListCostService = new EMWPListCostService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemListBase
     */
    protected appDeName: string = 'emwplistcost';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof CostByItemListBase
     */
    protected appDeLogicName: string = '询价单';

    /**
     * 界面UI服务对象
     *
     * @type {EMWPListCostUIService}
     * @memberof CostByItemBase
     */  
    public appUIService: EMWPListCostUIService = new EMWPListCostUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof CostByItemListBase
     */
    public limit: number = 5;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof CostByItemListBase
     */
    public minorSortDir: string = 'DESC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof CostByItemListBase
     */
    public minorSortPSDEF: string = 'adate';




}