/**
 * Default 部件模型
 *
 * @export
 * @class DefaultModel
 */
export default class DefaultModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DefaultModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'n_emwo_oscname_like',
        prop: 'emwo_oscname',
        dataType: 'TEXT',
      },
      {
        name: 'n_equipname_like',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_objname_like',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wogroup_eq',
        prop: 'wogroup',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_wooriname_like',
        prop: 'wooriname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wostate_eq',
        prop: 'wostate',
        dataType: 'NSCODELIST',
      },
    ]
  }

}