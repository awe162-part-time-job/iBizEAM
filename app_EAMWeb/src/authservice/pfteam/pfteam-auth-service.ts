import PFTeamAuthServiceBase from './pfteam-auth-service-base';


/**
 * 班组权限服务对象
 *
 * @export
 * @class PFTeamAuthService
 * @extends {PFTeamAuthServiceBase}
 */
export default class PFTeamAuthService extends PFTeamAuthServiceBase {

    /**
     * Creates an instance of  PFTeamAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFTeamAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}