import EMEQKPAuthServiceBase from './emeqkp-auth-service-base';


/**
 * 设备关键点权限服务对象
 *
 * @export
 * @class EMEQKPAuthService
 * @extends {EMEQKPAuthServiceBase}
 */
export default class EMEQKPAuthService extends EMEQKPAuthServiceBase {

    /**
     * Creates an instance of  EMEQKPAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKPAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}