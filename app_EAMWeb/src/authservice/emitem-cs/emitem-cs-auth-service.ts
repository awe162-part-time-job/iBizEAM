import EMItemCSAuthServiceBase from './emitem-cs-auth-service-base';


/**
 * 库间调整单权限服务对象
 *
 * @export
 * @class EMItemCSAuthService
 * @extends {EMItemCSAuthServiceBase}
 */
export default class EMItemCSAuthService extends EMItemCSAuthServiceBase {

    /**
     * Creates an instance of  EMItemCSAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemCSAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}