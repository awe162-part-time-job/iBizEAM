import EMEQLCTTIResAuthServiceBase from './emeqlcttires-auth-service-base';


/**
 * 轮胎位置权限服务对象
 *
 * @export
 * @class EMEQLCTTIResAuthService
 * @extends {EMEQLCTTIResAuthServiceBase}
 */
export default class EMEQLCTTIResAuthService extends EMEQLCTTIResAuthServiceBase {

    /**
     * Creates an instance of  EMEQLCTTIResAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTTIResAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}