import EMRFOCAAuthServiceBase from './emrfoca-auth-service-base';


/**
 * 原因权限服务对象
 *
 * @export
 * @class EMRFOCAAuthService
 * @extends {EMRFOCAAuthServiceBase}
 */
export default class EMRFOCAAuthService extends EMRFOCAAuthServiceBase {

    /**
     * Creates an instance of  EMRFOCAAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOCAAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}