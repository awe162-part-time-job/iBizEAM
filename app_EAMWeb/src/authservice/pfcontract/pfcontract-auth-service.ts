import PFContractAuthServiceBase from './pfcontract-auth-service-base';


/**
 * 合同权限服务对象
 *
 * @export
 * @class PFContractAuthService
 * @extends {PFContractAuthServiceBase}
 */
export default class PFContractAuthService extends PFContractAuthServiceBase {

    /**
     * Creates an instance of  PFContractAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFContractAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}