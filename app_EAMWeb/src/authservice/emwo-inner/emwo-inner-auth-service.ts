import EMWO_INNERAuthServiceBase from './emwo-inner-auth-service-base';


/**
 * 内部工单权限服务对象
 *
 * @export
 * @class EMWO_INNERAuthService
 * @extends {EMWO_INNERAuthServiceBase}
 */
export default class EMWO_INNERAuthService extends EMWO_INNERAuthServiceBase {

    /**
     * Creates an instance of  EMWO_INNERAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_INNERAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}