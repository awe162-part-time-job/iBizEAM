import EMDPRCTAuthServiceBase from './emdprct-auth-service-base';


/**
 * 测点记录权限服务对象
 *
 * @export
 * @class EMDPRCTAuthService
 * @extends {EMDPRCTAuthServiceBase}
 */
export default class EMDPRCTAuthService extends EMDPRCTAuthServiceBase {

    /**
     * Creates an instance of  EMDPRCTAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDPRCTAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}