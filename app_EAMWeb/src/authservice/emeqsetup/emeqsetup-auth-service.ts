import EMEQSetupAuthServiceBase from './emeqsetup-auth-service-base';


/**
 * 更换安装权限服务对象
 *
 * @export
 * @class EMEQSetupAuthService
 * @extends {EMEQSetupAuthServiceBase}
 */
export default class EMEQSetupAuthService extends EMEQSetupAuthServiceBase {

    /**
     * Creates an instance of  EMEQSetupAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQSetupAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}