import EMEquipAuthServiceBase from './emequip-auth-service-base';


/**
 * 设备档案权限服务对象
 *
 * @export
 * @class EMEquipAuthService
 * @extends {EMEquipAuthServiceBase}
 */
export default class EMEquipAuthService extends EMEquipAuthServiceBase {

    /**
     * Creates an instance of  EMEquipAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEquipAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}