package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKPRCD;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKPRCDService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKPRCDSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"关键点记录" })
@RestController("WebApi-emeqkprcd")
@RequestMapping("")
public class EMEQKPRCDResource {

    @Autowired
    public IEMEQKPRCDService emeqkprcdService;

    @Autowired
    @Lazy
    public EMEQKPRCDMapping emeqkprcdMapping;

    @PreAuthorize("hasPermission(this.emeqkprcdMapping.toDomain(#emeqkprcddto),'eam-EMEQKPRCD-Create')")
    @ApiOperation(value = "新建关键点记录", tags = {"关键点记录" },  notes = "新建关键点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkprcds")
    public ResponseEntity<EMEQKPRCDDTO> create(@Validated @RequestBody EMEQKPRCDDTO emeqkprcddto) {
        EMEQKPRCD domain = emeqkprcdMapping.toDomain(emeqkprcddto);
		emeqkprcdService.create(domain);
        EMEQKPRCDDTO dto = emeqkprcdMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkprcdMapping.toDomain(#emeqkprcddtos),'eam-EMEQKPRCD-Create')")
    @ApiOperation(value = "批量新建关键点记录", tags = {"关键点记录" },  notes = "批量新建关键点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkprcds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQKPRCDDTO> emeqkprcddtos) {
        emeqkprcdService.createBatch(emeqkprcdMapping.toDomain(emeqkprcddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkprcd" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkprcdService.get(#emeqkprcd_id),'eam-EMEQKPRCD-Update')")
    @ApiOperation(value = "更新关键点记录", tags = {"关键点记录" },  notes = "更新关键点记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkprcds/{emeqkprcd_id}")
    public ResponseEntity<EMEQKPRCDDTO> update(@PathVariable("emeqkprcd_id") String emeqkprcd_id, @RequestBody EMEQKPRCDDTO emeqkprcddto) {
		EMEQKPRCD domain  = emeqkprcdMapping.toDomain(emeqkprcddto);
        domain .setEmeqkprcdid(emeqkprcd_id);
		emeqkprcdService.update(domain );
		EMEQKPRCDDTO dto = emeqkprcdMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkprcdService.getEmeqkprcdByEntities(this.emeqkprcdMapping.toDomain(#emeqkprcddtos)),'eam-EMEQKPRCD-Update')")
    @ApiOperation(value = "批量更新关键点记录", tags = {"关键点记录" },  notes = "批量更新关键点记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkprcds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQKPRCDDTO> emeqkprcddtos) {
        emeqkprcdService.updateBatch(emeqkprcdMapping.toDomain(emeqkprcddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkprcdService.get(#emeqkprcd_id),'eam-EMEQKPRCD-Remove')")
    @ApiOperation(value = "删除关键点记录", tags = {"关键点记录" },  notes = "删除关键点记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkprcds/{emeqkprcd_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqkprcd_id") String emeqkprcd_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqkprcdService.remove(emeqkprcd_id));
    }

    @PreAuthorize("hasPermission(this.emeqkprcdService.getEmeqkprcdByIds(#ids),'eam-EMEQKPRCD-Remove')")
    @ApiOperation(value = "批量删除关键点记录", tags = {"关键点记录" },  notes = "批量删除关键点记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkprcds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqkprcdService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkprcdMapping.toDomain(returnObject.body),'eam-EMEQKPRCD-Get')")
    @ApiOperation(value = "获取关键点记录", tags = {"关键点记录" },  notes = "获取关键点记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkprcds/{emeqkprcd_id}")
    public ResponseEntity<EMEQKPRCDDTO> get(@PathVariable("emeqkprcd_id") String emeqkprcd_id) {
        EMEQKPRCD domain = emeqkprcdService.get(emeqkprcd_id);
        EMEQKPRCDDTO dto = emeqkprcdMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取关键点记录草稿", tags = {"关键点记录" },  notes = "获取关键点记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkprcds/getdraft")
    public ResponseEntity<EMEQKPRCDDTO> getDraft(EMEQKPRCDDTO dto) {
        EMEQKPRCD domain = emeqkprcdMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkprcdMapping.toDto(emeqkprcdService.getDraft(domain)));
    }

    @ApiOperation(value = "检查关键点记录", tags = {"关键点记录" },  notes = "检查关键点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkprcds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQKPRCDDTO emeqkprcddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkprcdService.checkKey(emeqkprcdMapping.toDomain(emeqkprcddto)));
    }

    @PreAuthorize("hasPermission(this.emeqkprcdMapping.toDomain(#emeqkprcddto),'eam-EMEQKPRCD-Save')")
    @ApiOperation(value = "保存关键点记录", tags = {"关键点记录" },  notes = "保存关键点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkprcds/save")
    public ResponseEntity<EMEQKPRCDDTO> save(@RequestBody EMEQKPRCDDTO emeqkprcddto) {
        EMEQKPRCD domain = emeqkprcdMapping.toDomain(emeqkprcddto);
        emeqkprcdService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkprcdMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqkprcdMapping.toDomain(#emeqkprcddtos),'eam-EMEQKPRCD-Save')")
    @ApiOperation(value = "批量保存关键点记录", tags = {"关键点记录" },  notes = "批量保存关键点记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkprcds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQKPRCDDTO> emeqkprcddtos) {
        emeqkprcdService.saveBatch(emeqkprcdMapping.toDomain(emeqkprcddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKPRCD-searchDefault-all') and hasPermission(#context,'eam-EMEQKPRCD-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"关键点记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqkprcds/fetchdefault")
	public ResponseEntity<List<EMEQKPRCDDTO>> fetchDefault(EMEQKPRCDSearchContext context) {
        Page<EMEQKPRCD> domains = emeqkprcdService.searchDefault(context) ;
        List<EMEQKPRCDDTO> list = emeqkprcdMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKPRCD-searchDefault-all') and hasPermission(#context,'eam-EMEQKPRCD-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"关键点记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqkprcds/searchdefault")
	public ResponseEntity<Page<EMEQKPRCDDTO>> searchDefault(@RequestBody EMEQKPRCDSearchContext context) {
        Page<EMEQKPRCD> domains = emeqkprcdService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkprcdMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

