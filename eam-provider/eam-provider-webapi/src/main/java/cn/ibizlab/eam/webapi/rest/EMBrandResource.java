package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMBrand;
import cn.ibizlab.eam.core.eam_core.service.IEMBrandService;
import cn.ibizlab.eam.core.eam_core.filter.EMBrandSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"品牌" })
@RestController("WebApi-embrand")
@RequestMapping("")
public class EMBrandResource {

    @Autowired
    public IEMBrandService embrandService;

    @Autowired
    @Lazy
    public EMBrandMapping embrandMapping;

    @PreAuthorize("hasPermission(this.embrandMapping.toDomain(#embranddto),'eam-EMBrand-Create')")
    @ApiOperation(value = "新建品牌", tags = {"品牌" },  notes = "新建品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/embrands")
    public ResponseEntity<EMBrandDTO> create(@Validated @RequestBody EMBrandDTO embranddto) {
        EMBrand domain = embrandMapping.toDomain(embranddto);
		embrandService.create(domain);
        EMBrandDTO dto = embrandMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embrandMapping.toDomain(#embranddtos),'eam-EMBrand-Create')")
    @ApiOperation(value = "批量新建品牌", tags = {"品牌" },  notes = "批量新建品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/embrands/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMBrandDTO> embranddtos) {
        embrandService.createBatch(embrandMapping.toDomain(embranddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "embrand" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.embrandService.get(#embrand_id),'eam-EMBrand-Update')")
    @ApiOperation(value = "更新品牌", tags = {"品牌" },  notes = "更新品牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/embrands/{embrand_id}")
    public ResponseEntity<EMBrandDTO> update(@PathVariable("embrand_id") String embrand_id, @RequestBody EMBrandDTO embranddto) {
		EMBrand domain  = embrandMapping.toDomain(embranddto);
        domain .setEmbrandid(embrand_id);
		embrandService.update(domain );
		EMBrandDTO dto = embrandMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embrandService.getEmbrandByEntities(this.embrandMapping.toDomain(#embranddtos)),'eam-EMBrand-Update')")
    @ApiOperation(value = "批量更新品牌", tags = {"品牌" },  notes = "批量更新品牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/embrands/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMBrandDTO> embranddtos) {
        embrandService.updateBatch(embrandMapping.toDomain(embranddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.embrandService.get(#embrand_id),'eam-EMBrand-Remove')")
    @ApiOperation(value = "删除品牌", tags = {"品牌" },  notes = "删除品牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embrands/{embrand_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("embrand_id") String embrand_id) {
         return ResponseEntity.status(HttpStatus.OK).body(embrandService.remove(embrand_id));
    }

    @PreAuthorize("hasPermission(this.embrandService.getEmbrandByIds(#ids),'eam-EMBrand-Remove')")
    @ApiOperation(value = "批量删除品牌", tags = {"品牌" },  notes = "批量删除品牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embrands/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        embrandService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.embrandMapping.toDomain(returnObject.body),'eam-EMBrand-Get')")
    @ApiOperation(value = "获取品牌", tags = {"品牌" },  notes = "获取品牌")
	@RequestMapping(method = RequestMethod.GET, value = "/embrands/{embrand_id}")
    public ResponseEntity<EMBrandDTO> get(@PathVariable("embrand_id") String embrand_id) {
        EMBrand domain = embrandService.get(embrand_id);
        EMBrandDTO dto = embrandMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取品牌草稿", tags = {"品牌" },  notes = "获取品牌草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/embrands/getdraft")
    public ResponseEntity<EMBrandDTO> getDraft(EMBrandDTO dto) {
        EMBrand domain = embrandMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(embrandMapping.toDto(embrandService.getDraft(domain)));
    }

    @ApiOperation(value = "检查品牌", tags = {"品牌" },  notes = "检查品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/embrands/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMBrandDTO embranddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(embrandService.checkKey(embrandMapping.toDomain(embranddto)));
    }

    @PreAuthorize("hasPermission(this.embrandMapping.toDomain(#embranddto),'eam-EMBrand-Save')")
    @ApiOperation(value = "保存品牌", tags = {"品牌" },  notes = "保存品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/embrands/save")
    public ResponseEntity<EMBrandDTO> save(@RequestBody EMBrandDTO embranddto) {
        EMBrand domain = embrandMapping.toDomain(embranddto);
        embrandService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(embrandMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.embrandMapping.toDomain(#embranddtos),'eam-EMBrand-Save')")
    @ApiOperation(value = "批量保存品牌", tags = {"品牌" },  notes = "批量保存品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/embrands/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMBrandDTO> embranddtos) {
        embrandService.saveBatch(embrandMapping.toDomain(embranddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBrand-searchDefault-all') and hasPermission(#context,'eam-EMBrand-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"品牌" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/embrands/fetchdefault")
	public ResponseEntity<List<EMBrandDTO>> fetchDefault(EMBrandSearchContext context) {
        Page<EMBrand> domains = embrandService.searchDefault(context) ;
        List<EMBrandDTO> list = embrandMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBrand-searchDefault-all') and hasPermission(#context,'eam-EMBrand-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"品牌" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/embrands/searchdefault")
	public ResponseEntity<Page<EMBrandDTO>> searchDefault(@RequestBody EMBrandSearchContext context) {
        Page<EMBrand> domains = embrandService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(embrandMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

