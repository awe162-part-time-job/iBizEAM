package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIGSJRB;
import cn.ibizlab.eam.core.eam_core.service.IEMEIGSJRBService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIGSJRBSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工索具日报" })
@RestController("WebApi-emeigsjrb")
@RequestMapping("")
public class EMEIGSJRBResource {

    @Autowired
    public IEMEIGSJRBService emeigsjrbService;

    @Autowired
    @Lazy
    public EMEIGSJRBMapping emeigsjrbMapping;

    @PreAuthorize("hasPermission(this.emeigsjrbMapping.toDomain(#emeigsjrbdto),'eam-EMEIGSJRB-Create')")
    @ApiOperation(value = "新建工索具日报", tags = {"工索具日报" },  notes = "新建工索具日报")
	@RequestMapping(method = RequestMethod.POST, value = "/emeigsjrbs")
    public ResponseEntity<EMEIGSJRBDTO> create(@Validated @RequestBody EMEIGSJRBDTO emeigsjrbdto) {
        EMEIGSJRB domain = emeigsjrbMapping.toDomain(emeigsjrbdto);
		emeigsjrbService.create(domain);
        EMEIGSJRBDTO dto = emeigsjrbMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeigsjrbMapping.toDomain(#emeigsjrbdtos),'eam-EMEIGSJRB-Create')")
    @ApiOperation(value = "批量新建工索具日报", tags = {"工索具日报" },  notes = "批量新建工索具日报")
	@RequestMapping(method = RequestMethod.POST, value = "/emeigsjrbs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIGSJRBDTO> emeigsjrbdtos) {
        emeigsjrbService.createBatch(emeigsjrbMapping.toDomain(emeigsjrbdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeigsjrb" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeigsjrbService.get(#emeigsjrb_id),'eam-EMEIGSJRB-Update')")
    @ApiOperation(value = "更新工索具日报", tags = {"工索具日报" },  notes = "更新工索具日报")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeigsjrbs/{emeigsjrb_id}")
    public ResponseEntity<EMEIGSJRBDTO> update(@PathVariable("emeigsjrb_id") String emeigsjrb_id, @RequestBody EMEIGSJRBDTO emeigsjrbdto) {
		EMEIGSJRB domain  = emeigsjrbMapping.toDomain(emeigsjrbdto);
        domain .setEmeigsjrbid(emeigsjrb_id);
		emeigsjrbService.update(domain );
		EMEIGSJRBDTO dto = emeigsjrbMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeigsjrbService.getEmeigsjrbByEntities(this.emeigsjrbMapping.toDomain(#emeigsjrbdtos)),'eam-EMEIGSJRB-Update')")
    @ApiOperation(value = "批量更新工索具日报", tags = {"工索具日报" },  notes = "批量更新工索具日报")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeigsjrbs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIGSJRBDTO> emeigsjrbdtos) {
        emeigsjrbService.updateBatch(emeigsjrbMapping.toDomain(emeigsjrbdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeigsjrbService.get(#emeigsjrb_id),'eam-EMEIGSJRB-Remove')")
    @ApiOperation(value = "删除工索具日报", tags = {"工索具日报" },  notes = "删除工索具日报")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeigsjrbs/{emeigsjrb_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeigsjrb_id") String emeigsjrb_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeigsjrbService.remove(emeigsjrb_id));
    }

    @PreAuthorize("hasPermission(this.emeigsjrbService.getEmeigsjrbByIds(#ids),'eam-EMEIGSJRB-Remove')")
    @ApiOperation(value = "批量删除工索具日报", tags = {"工索具日报" },  notes = "批量删除工索具日报")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeigsjrbs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeigsjrbService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeigsjrbMapping.toDomain(returnObject.body),'eam-EMEIGSJRB-Get')")
    @ApiOperation(value = "获取工索具日报", tags = {"工索具日报" },  notes = "获取工索具日报")
	@RequestMapping(method = RequestMethod.GET, value = "/emeigsjrbs/{emeigsjrb_id}")
    public ResponseEntity<EMEIGSJRBDTO> get(@PathVariable("emeigsjrb_id") String emeigsjrb_id) {
        EMEIGSJRB domain = emeigsjrbService.get(emeigsjrb_id);
        EMEIGSJRBDTO dto = emeigsjrbMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工索具日报草稿", tags = {"工索具日报" },  notes = "获取工索具日报草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeigsjrbs/getdraft")
    public ResponseEntity<EMEIGSJRBDTO> getDraft(EMEIGSJRBDTO dto) {
        EMEIGSJRB domain = emeigsjrbMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeigsjrbMapping.toDto(emeigsjrbService.getDraft(domain)));
    }

    @ApiOperation(value = "检查工索具日报", tags = {"工索具日报" },  notes = "检查工索具日报")
	@RequestMapping(method = RequestMethod.POST, value = "/emeigsjrbs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIGSJRBDTO emeigsjrbdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeigsjrbService.checkKey(emeigsjrbMapping.toDomain(emeigsjrbdto)));
    }

    @PreAuthorize("hasPermission(this.emeigsjrbMapping.toDomain(#emeigsjrbdto),'eam-EMEIGSJRB-Save')")
    @ApiOperation(value = "保存工索具日报", tags = {"工索具日报" },  notes = "保存工索具日报")
	@RequestMapping(method = RequestMethod.POST, value = "/emeigsjrbs/save")
    public ResponseEntity<EMEIGSJRBDTO> save(@RequestBody EMEIGSJRBDTO emeigsjrbdto) {
        EMEIGSJRB domain = emeigsjrbMapping.toDomain(emeigsjrbdto);
        emeigsjrbService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeigsjrbMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeigsjrbMapping.toDomain(#emeigsjrbdtos),'eam-EMEIGSJRB-Save')")
    @ApiOperation(value = "批量保存工索具日报", tags = {"工索具日报" },  notes = "批量保存工索具日报")
	@RequestMapping(method = RequestMethod.POST, value = "/emeigsjrbs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIGSJRBDTO> emeigsjrbdtos) {
        emeigsjrbService.saveBatch(emeigsjrbMapping.toDomain(emeigsjrbdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIGSJRB-searchDefault-all') and hasPermission(#context,'eam-EMEIGSJRB-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工索具日报" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeigsjrbs/fetchdefault")
	public ResponseEntity<List<EMEIGSJRBDTO>> fetchDefault(EMEIGSJRBSearchContext context) {
        Page<EMEIGSJRB> domains = emeigsjrbService.searchDefault(context) ;
        List<EMEIGSJRBDTO> list = emeigsjrbMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEIGSJRB-searchDefault-all') and hasPermission(#context,'eam-EMEIGSJRB-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工索具日报" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeigsjrbs/searchdefault")
	public ResponseEntity<Page<EMEIGSJRBDTO>> searchDefault(@RequestBody EMEIGSJRBSearchContext context) {
        Page<EMEIGSJRB> domains = emeigsjrbService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeigsjrbMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

