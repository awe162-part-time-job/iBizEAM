package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMAssessDTO]
 */
@Data
@ApiModel("设备故障考核")
public class EMAssessDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [EXPONENT]
     *
     */
    @JSONField(name = "exponent")
    @JsonProperty("exponent")
    @ApiModelProperty("指数")
    private Double exponent;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [EMASSESSNAME]
     *
     */
    @JSONField(name = "emassessname")
    @JsonProperty("emassessname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备故障考核名称")
    private String emassessname;

    /**
     * 属性 [FAULTINDEX]
     *
     */
    @JSONField(name = "faultindex")
    @JsonProperty("faultindex")
    @ApiModelProperty("本月故障指数")
    private Double faultindex;

    /**
     * 属性 [FAULTAMOUNT]
     *
     */
    @JSONField(name = "faultamount")
    @JsonProperty("faultamount")
    @ApiModelProperty("故障数")
    private Double faultamount;

    /**
     * 属性 [PFEMPNAME]
     *
     */
    @JSONField(name = "pfempname")
    @JsonProperty("pfempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("填写人")
    private String pfempname;

    /**
     * 属性 [OPERATIONVOLUME]
     *
     */
    @JSONField(name = "operationvolume")
    @JsonProperty("operationvolume")
    @ApiModelProperty("本月作业箱量")
    private Double operationvolume;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [REMARK]
     *
     */
    @JSONField(name = "remark")
    @JsonProperty("remark")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [EMASSESSID]
     *
     */
    @JSONField(name = "emassessid")
    @JsonProperty("emassessid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备故障考核标识")
    private String emassessid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [PFEMPID]
     *
     */
    @JSONField(name = "pfempid")
    @JsonProperty("pfempid")
    @NotBlank(message = "[填写人]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("填写人")
    private String pfempid;

    /**
     * 属性 [VOLUME]
     *
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    @ApiModelProperty("箱量")
    private Double volume;

    /**
     * 属性 [FAULTTIME]
     *
     */
    @JSONField(name = "faulttime")
    @JsonProperty("faulttime")
    @ApiModelProperty("设备故障次数")
    private Double faulttime;

    /**
     * 属性 [INDEXBANLANCE]
     *
     */
    @JSONField(name = "indexbanlance")
    @JsonProperty("indexbanlance")
    @ApiModelProperty("指数差额")
    private Double indexbanlance;

    /**
     * 属性 [PFTEAMNAME]
     *
     */
    @JSONField(name = "pfteamname")
    @JsonProperty("pfteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备班组")
    private String pfteamname;

    /**
     * 属性 [PFTEAMID]
     *
     */
    @JSONField(name = "pfteamid")
    @JsonProperty("pfteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备班组")
    private String pfteamid;


    /**
     * 设置 [EXPONENT]
     */
    public void setExponent(Double  exponent){
        this.exponent = exponent ;
        this.modify("exponent",exponent);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [EMASSESSNAME]
     */
    public void setEmassessname(String  emassessname){
        this.emassessname = emassessname ;
        this.modify("emassessname",emassessname);
    }

    /**
     * 设置 [FAULTINDEX]
     */
    public void setFaultindex(Double  faultindex){
        this.faultindex = faultindex ;
        this.modify("faultindex",faultindex);
    }

    /**
     * 设置 [FAULTAMOUNT]
     */
    public void setFaultamount(Double  faultamount){
        this.faultamount = faultamount ;
        this.modify("faultamount",faultamount);
    }

    /**
     * 设置 [PFEMPNAME]
     */
    public void setPfempname(String  pfempname){
        this.pfempname = pfempname ;
        this.modify("pfempname",pfempname);
    }

    /**
     * 设置 [OPERATIONVOLUME]
     */
    public void setOperationvolume(Double  operationvolume){
        this.operationvolume = operationvolume ;
        this.modify("operationvolume",operationvolume);
    }

    /**
     * 设置 [REMARK]
     */
    public void setRemark(String  remark){
        this.remark = remark ;
        this.modify("remark",remark);
    }

    /**
     * 设置 [PFEMPID]
     */
    public void setPfempid(String  pfempid){
        this.pfempid = pfempid ;
        this.modify("pfempid",pfempid);
    }

    /**
     * 设置 [VOLUME]
     */
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }

    /**
     * 设置 [FAULTTIME]
     */
    public void setFaulttime(Double  faulttime){
        this.faulttime = faulttime ;
        this.modify("faulttime",faulttime);
    }

    /**
     * 设置 [INDEXBANLANCE]
     */
    public void setIndexbanlance(Double  indexbanlance){
        this.indexbanlance = indexbanlance ;
        this.modify("indexbanlance",indexbanlance);
    }

    /**
     * 设置 [PFTEAMID]
     */
    public void setPfteamid(String  pfteamid){
        this.pfteamid = pfteamid ;
        this.modify("pfteamid",pfteamid);
    }


}


