package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFContract;
import cn.ibizlab.eam.core.eam_pf.service.IPFContractService;
import cn.ibizlab.eam.core.eam_pf.filter.PFContractSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合同" })
@RestController("WebApi-pfcontract")
@RequestMapping("")
public class PFContractResource {

    @Autowired
    public IPFContractService pfcontractService;

    @Autowired
    @Lazy
    public PFContractMapping pfcontractMapping;

    @PreAuthorize("hasPermission(this.pfcontractMapping.toDomain(#pfcontractdto),'eam-PFContract-Create')")
    @ApiOperation(value = "新建合同", tags = {"合同" },  notes = "新建合同")
	@RequestMapping(method = RequestMethod.POST, value = "/pfcontracts")
    public ResponseEntity<PFContractDTO> create(@Validated @RequestBody PFContractDTO pfcontractdto) {
        PFContract domain = pfcontractMapping.toDomain(pfcontractdto);
		pfcontractService.create(domain);
        PFContractDTO dto = pfcontractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfcontractMapping.toDomain(#pfcontractdtos),'eam-PFContract-Create')")
    @ApiOperation(value = "批量新建合同", tags = {"合同" },  notes = "批量新建合同")
	@RequestMapping(method = RequestMethod.POST, value = "/pfcontracts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFContractDTO> pfcontractdtos) {
        pfcontractService.createBatch(pfcontractMapping.toDomain(pfcontractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfcontract" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfcontractService.get(#pfcontract_id),'eam-PFContract-Update')")
    @ApiOperation(value = "更新合同", tags = {"合同" },  notes = "更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfcontracts/{pfcontract_id}")
    public ResponseEntity<PFContractDTO> update(@PathVariable("pfcontract_id") String pfcontract_id, @RequestBody PFContractDTO pfcontractdto) {
		PFContract domain  = pfcontractMapping.toDomain(pfcontractdto);
        domain .setPfcontractid(pfcontract_id);
		pfcontractService.update(domain );
		PFContractDTO dto = pfcontractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfcontractService.getPfcontractByEntities(this.pfcontractMapping.toDomain(#pfcontractdtos)),'eam-PFContract-Update')")
    @ApiOperation(value = "批量更新合同", tags = {"合同" },  notes = "批量更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfcontracts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFContractDTO> pfcontractdtos) {
        pfcontractService.updateBatch(pfcontractMapping.toDomain(pfcontractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfcontractService.get(#pfcontract_id),'eam-PFContract-Remove')")
    @ApiOperation(value = "删除合同", tags = {"合同" },  notes = "删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfcontracts/{pfcontract_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfcontract_id") String pfcontract_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfcontractService.remove(pfcontract_id));
    }

    @PreAuthorize("hasPermission(this.pfcontractService.getPfcontractByIds(#ids),'eam-PFContract-Remove')")
    @ApiOperation(value = "批量删除合同", tags = {"合同" },  notes = "批量删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfcontracts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfcontractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfcontractMapping.toDomain(returnObject.body),'eam-PFContract-Get')")
    @ApiOperation(value = "获取合同", tags = {"合同" },  notes = "获取合同")
	@RequestMapping(method = RequestMethod.GET, value = "/pfcontracts/{pfcontract_id}")
    public ResponseEntity<PFContractDTO> get(@PathVariable("pfcontract_id") String pfcontract_id) {
        PFContract domain = pfcontractService.get(pfcontract_id);
        PFContractDTO dto = pfcontractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合同草稿", tags = {"合同" },  notes = "获取合同草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfcontracts/getdraft")
    public ResponseEntity<PFContractDTO> getDraft(PFContractDTO dto) {
        PFContract domain = pfcontractMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(pfcontractMapping.toDto(pfcontractService.getDraft(domain)));
    }

    @ApiOperation(value = "检查合同", tags = {"合同" },  notes = "检查合同")
	@RequestMapping(method = RequestMethod.POST, value = "/pfcontracts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFContractDTO pfcontractdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfcontractService.checkKey(pfcontractMapping.toDomain(pfcontractdto)));
    }

    @PreAuthorize("hasPermission(this.pfcontractMapping.toDomain(#pfcontractdto),'eam-PFContract-Save')")
    @ApiOperation(value = "保存合同", tags = {"合同" },  notes = "保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/pfcontracts/save")
    public ResponseEntity<PFContractDTO> save(@RequestBody PFContractDTO pfcontractdto) {
        PFContract domain = pfcontractMapping.toDomain(pfcontractdto);
        pfcontractService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(pfcontractMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.pfcontractMapping.toDomain(#pfcontractdtos),'eam-PFContract-Save')")
    @ApiOperation(value = "批量保存合同", tags = {"合同" },  notes = "批量保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/pfcontracts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFContractDTO> pfcontractdtos) {
        pfcontractService.saveBatch(pfcontractMapping.toDomain(pfcontractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFContract-searchDefault-all') and hasPermission(#context,'eam-PFContract-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"合同" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfcontracts/fetchdefault")
	public ResponseEntity<List<PFContractDTO>> fetchDefault(PFContractSearchContext context) {
        Page<PFContract> domains = pfcontractService.searchDefault(context) ;
        List<PFContractDTO> list = pfcontractMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFContract-searchDefault-all') and hasPermission(#context,'eam-PFContract-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"合同" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfcontracts/searchdefault")
	public ResponseEntity<Page<PFContractDTO>> searchDefault(@RequestBody PFContractSearchContext context) {
        Page<PFContract> domains = pfcontractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfcontractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

