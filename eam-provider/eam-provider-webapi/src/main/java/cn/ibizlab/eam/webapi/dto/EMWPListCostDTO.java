package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMWPListCostDTO]
 */
@Data
@ApiModel("询价单")
public class EMWPListCostDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WPLISTCOSTINFO]
     *
     */
    @JSONField(name = "wplistcostinfo")
    @JsonProperty("wplistcostinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("询价信息")
    private String wplistcostinfo;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("询价人")
    private String rempname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("询价人")
    private String rempid;

    /**
     * 属性 [ADATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "adate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("adate")
    @NotNull(message = "[询价时间]不允许为空!")
    @ApiModelProperty("询价时间")
    private Timestamp adate;

    /**
     * 属性 [EMWPLISTCOSTNAME]
     *
     */
    @JSONField(name = "emwplistcostname")
    @JsonProperty("emwplistcostname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("询价单名称")
    private String emwplistcostname;

    /**
     * 属性 [DISCNT]
     *
     */
    @JSONField(name = "discnt")
    @JsonProperty("discnt")
    @NotNull(message = "[折扣(%)]不允许为空!")
    @ApiModelProperty("折扣(%)")
    private Double discnt;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [ITEMDESC]
     *
     */
    @JSONField(name = "itemdesc")
    @JsonProperty("itemdesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("物品备注")
    private String itemdesc;

    /**
     * 属性 [UNITRATE]
     *
     */
    @JSONField(name = "unitrate")
    @JsonProperty("unitrate")
    @NotNull(message = "[单位转换率]不允许为空!")
    @ApiModelProperty("单位转换率")
    private Double unitrate;

    /**
     * 属性 [BEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "begindate" , format="yyyy-MM-dd")
    @JsonProperty("begindate")
    @NotNull(message = "[有效期起始]不允许为空!")
    @ApiModelProperty("有效期起始")
    private Timestamp begindate;

    /**
     * 属性 [LISTPRICE]
     *
     */
    @JSONField(name = "listprice")
    @JsonProperty("listprice")
    @ApiModelProperty("标价")
    private Double listprice;

    /**
     * 属性 [ITEMS]
     *
     */
    @JSONField(name = "items")
    @JsonProperty("items")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品")
    private String items;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [PRICECDT]
     *
     */
    @JSONField(name = "pricecdt")
    @JsonProperty("pricecdt")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("价格条件")
    private String pricecdt;

    /**
     * 属性 [INTUNITFLAG]
     *
     */
    @JSONField(name = "intunitflag")
    @JsonProperty("intunitflag")
    @NotNull(message = "[整单位购买]不允许为空!")
    @ApiModelProperty("整单位购买")
    private Integer intunitflag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [EMWPLISTCOSTID]
     *
     */
    @JSONField(name = "emwplistcostid")
    @JsonProperty("emwplistcostid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("询价单标识")
    private String emwplistcostid;

    /**
     * 属性 [WPLISTCOSTEVAL]
     *
     */
    @JSONField(name = "wplistcosteval")
    @JsonProperty("wplistcosteval")
    @ApiModelProperty("采购试算(单价排序)")
    private Double wplistcosteval;

    /**
     * 属性 [UNITDESC]
     *
     */
    @JSONField(name = "unitdesc")
    @JsonProperty("unitdesc")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("询价单位备注")
    private String unitdesc;

    /**
     * 属性 [ENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "enddate" , format="yyyy-MM-dd")
    @JsonProperty("enddate")
    @NotNull(message = "[有效期截至]不允许为空!")
    @ApiModelProperty("有效期截至")
    private Timestamp enddate;

    /**
     * 属性 [WPLISTCOSTRESULT]
     *
     */
    @JSONField(name = "wplistcostresult")
    @JsonProperty("wplistcostresult")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("-")
    private String wplistcostresult;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [TAXRATE]
     *
     */
    @JSONField(name = "taxrate")
    @JsonProperty("taxrate")
    @ApiModelProperty("税率")
    private Double taxrate;

    /**
     * 属性 [SUNITPRICE]
     *
     */
    @JSONField(name = "sunitprice")
    @JsonProperty("sunitprice")
    @ApiModelProperty("标准单位单价")
    private Double sunitprice;

    /**
     * 属性 [SUNITID]
     *
     */
    @JSONField(name = "sunitid")
    @JsonProperty("sunitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("标准单位")
    private String sunitid;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("询价单位")
    private String unitname;

    /**
     * 属性 [SUNITNAME]
     *
     */
    @JSONField(name = "sunitname")
    @JsonProperty("sunitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("标准单位")
    private String sunitname;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品")
    private String itemname;

    /**
     * 属性 [LABSERVICENAME]
     *
     */
    @JSONField(name = "labservicename")
    @JsonProperty("labservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("产品供应商")
    private String labservicename;

    /**
     * 属性 [AVGPRICE]
     *
     */
    @JSONField(name = "avgprice")
    @JsonProperty("avgprice")
    @ApiModelProperty("物品均价")
    private Double avgprice;

    /**
     * 属性 [WPLISTID]
     *
     */
    @JSONField(name = "wplistid")
    @JsonProperty("wplistid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("采购申请号")
    private String wplistid;

    /**
     * 属性 [LABSERVICEID]
     *
     */
    @JSONField(name = "labserviceid")
    @JsonProperty("labserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("产品供应商")
    private String labserviceid;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品")
    private String itemid;

    /**
     * 属性 [UNITID]
     *
     */
    @JSONField(name = "unitid")
    @JsonProperty("unitid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("询价单位")
    private String unitid;


    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [REMPNAME]
     */
    public void setRempname(String  rempname){
        this.rempname = rempname ;
        this.modify("rempname",rempname);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [ADATE]
     */
    public void setAdate(Timestamp  adate){
        this.adate = adate ;
        this.modify("adate",adate);
    }

    /**
     * 设置 [EMWPLISTCOSTNAME]
     */
    public void setEmwplistcostname(String  emwplistcostname){
        this.emwplistcostname = emwplistcostname ;
        this.modify("emwplistcostname",emwplistcostname);
    }

    /**
     * 设置 [DISCNT]
     */
    public void setDiscnt(Double  discnt){
        this.discnt = discnt ;
        this.modify("discnt",discnt);
    }

    /**
     * 设置 [ITEMDESC]
     */
    public void setItemdesc(String  itemdesc){
        this.itemdesc = itemdesc ;
        this.modify("itemdesc",itemdesc);
    }

    /**
     * 设置 [UNITRATE]
     */
    public void setUnitrate(Double  unitrate){
        this.unitrate = unitrate ;
        this.modify("unitrate",unitrate);
    }

    /**
     * 设置 [BEGINDATE]
     */
    public void setBegindate(Timestamp  begindate){
        this.begindate = begindate ;
        this.modify("begindate",begindate);
    }

    /**
     * 设置 [LISTPRICE]
     */
    public void setListprice(Double  listprice){
        this.listprice = listprice ;
        this.modify("listprice",listprice);
    }

    /**
     * 设置 [PRICECDT]
     */
    public void setPricecdt(String  pricecdt){
        this.pricecdt = pricecdt ;
        this.modify("pricecdt",pricecdt);
    }

    /**
     * 设置 [INTUNITFLAG]
     */
    public void setIntunitflag(Integer  intunitflag){
        this.intunitflag = intunitflag ;
        this.modify("intunitflag",intunitflag);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [UNITDESC]
     */
    public void setUnitdesc(String  unitdesc){
        this.unitdesc = unitdesc ;
        this.modify("unitdesc",unitdesc);
    }

    /**
     * 设置 [ENDDATE]
     */
    public void setEnddate(Timestamp  enddate){
        this.enddate = enddate ;
        this.modify("enddate",enddate);
    }

    /**
     * 设置 [TAXRATE]
     */
    public void setTaxrate(Double  taxrate){
        this.taxrate = taxrate ;
        this.modify("taxrate",taxrate);
    }

    /**
     * 设置 [WPLISTID]
     */
    public void setWplistid(String  wplistid){
        this.wplistid = wplistid ;
        this.modify("wplistid",wplistid);
    }

    /**
     * 设置 [LABSERVICEID]
     */
    public void setLabserviceid(String  labserviceid){
        this.labserviceid = labserviceid ;
        this.modify("labserviceid",labserviceid);
    }

    /**
     * 设置 [ITEMID]
     */
    public void setItemid(String  itemid){
        this.itemid = itemid ;
        this.modify("itemid",itemid);
    }

    /**
     * 设置 [UNITID]
     */
    public void setUnitid(String  unitid){
        this.unitid = unitid ;
        this.modify("unitid",unitid);
    }


}


