package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFODE;
import cn.ibizlab.eam.core.eam_core.service.IEMRFODEService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFODESearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"现象" })
@RestController("WebApi-emrfode")
@RequestMapping("")
public class EMRFODEResource {

    @Autowired
    public IEMRFODEService emrfodeService;

    @Autowired
    @Lazy
    public EMRFODEMapping emrfodeMapping;

    @PreAuthorize("hasPermission(this.emrfodeMapping.toDomain(#emrfodedto),'eam-EMRFODE-Create')")
    @ApiOperation(value = "新建现象", tags = {"现象" },  notes = "新建现象")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes")
    public ResponseEntity<EMRFODEDTO> create(@Validated @RequestBody EMRFODEDTO emrfodedto) {
        EMRFODE domain = emrfodeMapping.toDomain(emrfodedto);
		emrfodeService.create(domain);
        EMRFODEDTO dto = emrfodeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodeMapping.toDomain(#emrfodedtos),'eam-EMRFODE-Create')")
    @ApiOperation(value = "批量新建现象", tags = {"现象" },  notes = "批量新建现象")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFODEDTO> emrfodedtos) {
        emrfodeService.createBatch(emrfodeMapping.toDomain(emrfodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfode" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfodeService.get(#emrfode_id),'eam-EMRFODE-Update')")
    @ApiOperation(value = "更新现象", tags = {"现象" },  notes = "更新现象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}")
    public ResponseEntity<EMRFODEDTO> update(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFODEDTO emrfodedto) {
		EMRFODE domain  = emrfodeMapping.toDomain(emrfodedto);
        domain .setEmrfodeid(emrfode_id);
		emrfodeService.update(domain );
		EMRFODEDTO dto = emrfodeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfodeService.getEmrfodeByEntities(this.emrfodeMapping.toDomain(#emrfodedtos)),'eam-EMRFODE-Update')")
    @ApiOperation(value = "批量更新现象", tags = {"现象" },  notes = "批量更新现象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFODEDTO> emrfodedtos) {
        emrfodeService.updateBatch(emrfodeMapping.toDomain(emrfodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfodeService.get(#emrfode_id),'eam-EMRFODE-Remove')")
    @ApiOperation(value = "删除现象", tags = {"现象" },  notes = "删除现象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfode_id") String emrfode_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfodeService.remove(emrfode_id));
    }

    @PreAuthorize("hasPermission(this.emrfodeService.getEmrfodeByIds(#ids),'eam-EMRFODE-Remove')")
    @ApiOperation(value = "批量删除现象", tags = {"现象" },  notes = "批量删除现象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfodeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfodeMapping.toDomain(returnObject.body),'eam-EMRFODE-Get')")
    @ApiOperation(value = "获取现象", tags = {"现象" },  notes = "获取现象")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}")
    public ResponseEntity<EMRFODEDTO> get(@PathVariable("emrfode_id") String emrfode_id) {
        EMRFODE domain = emrfodeService.get(emrfode_id);
        EMRFODEDTO dto = emrfodeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取现象草稿", tags = {"现象" },  notes = "获取现象草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/getdraft")
    public ResponseEntity<EMRFODEDTO> getDraft(EMRFODEDTO dto) {
        EMRFODE domain = emrfodeMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodeMapping.toDto(emrfodeService.getDraft(domain)));
    }

    @ApiOperation(value = "检查现象", tags = {"现象" },  notes = "检查现象")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFODEDTO emrfodedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfodeService.checkKey(emrfodeMapping.toDomain(emrfodedto)));
    }

    @PreAuthorize("hasPermission(this.emrfodeMapping.toDomain(#emrfodedto),'eam-EMRFODE-Save')")
    @ApiOperation(value = "保存现象", tags = {"现象" },  notes = "保存现象")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/save")
    public ResponseEntity<EMRFODEDTO> save(@RequestBody EMRFODEDTO emrfodedto) {
        EMRFODE domain = emrfodeMapping.toDomain(emrfodedto);
        emrfodeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfodeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfodeMapping.toDomain(#emrfodedtos),'eam-EMRFODE-Save')")
    @ApiOperation(value = "批量保存现象", tags = {"现象" },  notes = "批量保存现象")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFODEDTO> emrfodedtos) {
        emrfodeService.saveBatch(emrfodeMapping.toDomain(emrfodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODE-searchDefault-all') and hasPermission(#context,'eam-EMRFODE-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"现象" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/fetchdefault")
	public ResponseEntity<List<EMRFODEDTO>> fetchDefault(EMRFODESearchContext context) {
        Page<EMRFODE> domains = emrfodeService.searchDefault(context) ;
        List<EMRFODEDTO> list = emrfodeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFODE-searchDefault-all') and hasPermission(#context,'eam-EMRFODE-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"现象" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/searchdefault")
	public ResponseEntity<Page<EMRFODEDTO>> searchDefault(@RequestBody EMRFODESearchContext context) {
        Page<EMRFODE> domains = emrfodeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfodeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

