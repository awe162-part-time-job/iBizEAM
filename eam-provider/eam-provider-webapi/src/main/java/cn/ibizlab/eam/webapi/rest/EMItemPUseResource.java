package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;
import cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemPUseSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"领料单" })
@RestController("WebApi-emitempuse")
@RequestMapping("")
public class EMItemPUseResource {

    @Autowired
    public IEMItemPUseService emitempuseService;

    @Autowired
    @Lazy
    public EMItemPUseMapping emitempuseMapping;

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "新建领料单", tags = {"领料单" },  notes = "新建领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses")
    public ResponseEntity<EMItemPUseDTO> create(@Validated @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "批量新建领料单", tags = {"领料单" },  notes = "批量新建领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemPUseDTO> emitempusedtos) {
        emitempuseService.createBatch(emitempuseMapping.toDomain(emitempusedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "更新领料单", tags = {"领料单" },  notes = "更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> update(@PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
		EMItemPUse domain  = emitempuseMapping.toDomain(emitempusedto);
        domain .setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain );
		EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "批量更新领料单", tags = {"领料单" },  notes = "批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemPUseDTO> emitempusedtos) {
        emitempuseService.updateBatch(emitempuseMapping.toDomain(emitempusedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "删除领料单", tags = {"领料单" },  notes = "删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitempuse_id") String emitempuse_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "批量删除领料单", tags = {"领料单" },  notes = "批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "获取领料单", tags = {"领料单" },  notes = "获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> get(@PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取领料单草稿", tags = {"领料单" },  notes = "获取领料单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraft(EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "检查领料单", tags = {"领料单" },  notes = "检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "领料单物品信息", tags = {"领料单" },  notes = "领料单物品信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMID(@PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEmitempuseid(emitempuse_id);
        domain = emitempuseService.formUpdateByITEMID(domain);
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "确认发料", tags = {"领料单" },  notes = "确认发料")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issue(@PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEmitempuseid(emitempuse_id);
        domain = emitempuseService.issue(domain);
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "批量处理[确认发料]", tags = {"领料单" },  notes = "批量处理[确认发料]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueBatch(@RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"领料单" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejected(@PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEmitempuseid(emitempuse_id);
        domain = emitempuseService.rejected(domain);
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "保存领料单", tags = {"领料单" },  notes = "保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> save(@RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "批量保存领料单", tags = {"领料单" },  notes = "批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemPUseDTO> emitempusedtos) {
        emitempuseService.saveBatch(emitempuseMapping.toDomain(emitempusedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "提交", tags = {"领料单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submit(@PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEmitempuseid(emitempuse_id);
        domain = emitempuseService.submit(domain);
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"领料单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchDefault(EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"领料单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchDefault(@RequestBody EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "获取草稿", tags = {"领料单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchDraft(EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "查询草稿", tags = {"领料单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchDraft(@RequestBody EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "获取已发料", tags = {"领料单" } ,notes = "获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchIssued(EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "查询已发料", tags = {"领料单" } ,notes = "查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchIssued(@RequestBody EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "获取待发料", tags = {"领料单" } ,notes = "获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchWaitIssue(EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "查询待发料", tags = {"领料单" } ,notes = "查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchWaitIssue(@RequestBody EMItemPUseSearchContext context) {
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据设备档案建立领料单", tags = {"领料单" },  notes = "根据设备档案建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据设备档案批量建立领料单", tags = {"领料单" },  notes = "根据设备档案批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据设备档案更新领料单", tags = {"领料单" },  notes = "根据设备档案更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据设备档案批量更新领料单", tags = {"领料单" },  notes = "根据设备档案批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据设备档案删除领料单", tags = {"领料单" },  notes = "根据设备档案删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据设备档案批量删除领料单", tags = {"领料单" },  notes = "根据设备档案批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据设备档案获取领料单", tags = {"领料单" },  notes = "根据设备档案获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取领料单草稿", tags = {"领料单" },  notes = "根据设备档案获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查领料单", tags = {"领料单" },  notes = "根据设备档案检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据设备档案领料单", tags = {"领料单" },  notes = "根据设备档案领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据设备档案领料单", tags = {"领料单" },  notes = "根据设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据设备档案领料单]", tags = {"领料单" },  notes = "批量处理[根据设备档案领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据设备档案领料单", tags = {"领料单" },  notes = "根据设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据设备档案保存领料单", tags = {"领料单" },  notes = "根据设备档案保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据设备档案批量保存领料单", tags = {"领料单" },  notes = "根据设备档案批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据设备档案领料单", tags = {"领料单" },  notes = "根据设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"领料单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"领料单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案获取草稿", tags = {"领料单" } ,notes = "根据设备档案获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMEquip(@PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案查询草稿", tags = {"领料单" } ,notes = "根据设备档案查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案获取已发料", tags = {"领料单" } ,notes = "根据设备档案获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMEquip(@PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案查询已发料", tags = {"领料单" } ,notes = "根据设备档案查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案获取待发料", tags = {"领料单" } ,notes = "根据设备档案获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMEquip(@PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据设备档案查询待发料", tags = {"领料单" } ,notes = "根据设备档案查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据物品建立领料单", tags = {"领料单" },  notes = "根据物品建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据物品批量建立领料单", tags = {"领料单" },  notes = "根据物品批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据物品更新领料单", tags = {"领料单" },  notes = "根据物品更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据物品批量更新领料单", tags = {"领料单" },  notes = "根据物品批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据物品删除领料单", tags = {"领料单" },  notes = "根据物品删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据物品批量删除领料单", tags = {"领料单" },  notes = "根据物品批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据物品获取领料单", tags = {"领料单" },  notes = "根据物品获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取领料单草稿", tags = {"领料单" },  notes = "根据物品获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查领料单", tags = {"领料单" },  notes = "根据物品检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据物品领料单", tags = {"领料单" },  notes = "根据物品领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据物品领料单", tags = {"领料单" },  notes = "根据物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据物品领料单]", tags = {"领料单" },  notes = "批量处理[根据物品领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据物品领料单", tags = {"领料单" },  notes = "根据物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据物品保存领料单", tags = {"领料单" },  notes = "根据物品保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据物品批量保存领料单", tags = {"领料单" },  notes = "根据物品批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据物品领料单", tags = {"领料单" },  notes = "根据物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"领料单" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"领料单" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品获取草稿", tags = {"领料单" } ,notes = "根据物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品查询草稿", tags = {"领料单" } ,notes = "根据物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品获取已发料", tags = {"领料单" } ,notes = "根据物品获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品查询已发料", tags = {"领料单" } ,notes = "根据物品查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品获取待发料", tags = {"领料单" } ,notes = "根据物品获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据物品查询待发料", tags = {"领料单" } ,notes = "根据物品查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据计划修理建立领料单", tags = {"领料单" },  notes = "根据计划修理建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据计划修理批量建立领料单", tags = {"领料单" },  notes = "根据计划修理批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setPurplanid(empurplan_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据计划修理更新领料单", tags = {"领料单" },  notes = "根据计划修理更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据计划修理批量更新领料单", tags = {"领料单" },  notes = "根据计划修理批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurplans/{empurplan_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setPurplanid(empurplan_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据计划修理删除领料单", tags = {"领料单" },  notes = "根据计划修理删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据计划修理批量删除领料单", tags = {"领料单" },  notes = "根据计划修理批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/empurplans/{empurplan_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMPurPlan(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据计划修理获取领料单", tags = {"领料单" },  notes = "根据计划修理获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据计划修理获取领料单草稿", tags = {"领料单" },  notes = "根据计划修理获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/empurplans/{empurplan_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setPurplanid(empurplan_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据计划修理检查领料单", tags = {"领料单" },  notes = "根据计划修理检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据计划修理领料单", tags = {"领料单" },  notes = "根据计划修理领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据计划修理领料单", tags = {"领料单" },  notes = "根据计划修理领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据计划修理领料单]", tags = {"领料单" },  notes = "批量处理[根据计划修理领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据计划修理领料单", tags = {"领料单" },  notes = "根据计划修理领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据计划修理保存领料单", tags = {"领料单" },  notes = "根据计划修理保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据计划修理批量保存领料单", tags = {"领料单" },  notes = "根据计划修理批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setPurplanid(empurplan_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据计划修理领料单", tags = {"领料单" },  notes = "根据计划修理领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/empurplans/{empurplan_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setPurplanid(empurplan_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理获取DEFAULT", tags = {"领料单" } ,notes = "根据计划修理获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/empurplans/{empurplan_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id,EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理查询DEFAULT", tags = {"领料单" } ,notes = "根据计划修理查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/empurplans/{empurplan_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理获取草稿", tags = {"领料单" } ,notes = "根据计划修理获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/empurplans/{empurplan_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id,EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理查询草稿", tags = {"领料单" } ,notes = "根据计划修理查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/empurplans/{empurplan_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理获取已发料", tags = {"领料单" } ,notes = "根据计划修理获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/empurplans/{empurplan_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id,EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理查询已发料", tags = {"领料单" } ,notes = "根据计划修理查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/empurplans/{empurplan_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理获取待发料", tags = {"领料单" } ,notes = "根据计划修理获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/empurplans/{empurplan_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id,EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据计划修理查询待发料", tags = {"领料单" } ,notes = "根据计划修理查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/empurplans/{empurplan_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMPurPlan(@PathVariable("empurplan_id") String empurplan_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_purplanid_eq(empurplan_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据服务商建立领料单", tags = {"领料单" },  notes = "根据服务商建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据服务商批量建立领料单", tags = {"领料单" },  notes = "根据服务商批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setMserviceid(emservice_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据服务商更新领料单", tags = {"领料单" },  notes = "根据服务商更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据服务商批量更新领料单", tags = {"领料单" },  notes = "根据服务商批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setMserviceid(emservice_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据服务商删除领料单", tags = {"领料单" },  notes = "根据服务商删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据服务商批量删除领料单", tags = {"领料单" },  notes = "根据服务商批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据服务商获取领料单", tags = {"领料单" },  notes = "根据服务商获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取领料单草稿", tags = {"领料单" },  notes = "根据服务商获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setMserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查领料单", tags = {"领料单" },  notes = "根据服务商检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据服务商领料单", tags = {"领料单" },  notes = "根据服务商领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据服务商领料单", tags = {"领料单" },  notes = "根据服务商领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据服务商领料单]", tags = {"领料单" },  notes = "批量处理[根据服务商领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据服务商领料单", tags = {"领料单" },  notes = "根据服务商领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据服务商保存领料单", tags = {"领料单" },  notes = "根据服务商保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据服务商批量保存领料单", tags = {"领料单" },  notes = "根据服务商批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setMserviceid(emservice_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据服务商领料单", tags = {"领料单" },  notes = "根据服务商领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setMserviceid(emservice_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"领料单" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"领料单" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商获取草稿", tags = {"领料单" } ,notes = "根据服务商获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMService(@PathVariable("emservice_id") String emservice_id,EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商查询草稿", tags = {"领料单" } ,notes = "根据服务商查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商获取已发料", tags = {"领料单" } ,notes = "根据服务商获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMService(@PathVariable("emservice_id") String emservice_id,EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商查询已发料", tags = {"领料单" } ,notes = "根据服务商查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商获取待发料", tags = {"领料单" } ,notes = "根据服务商获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMService(@PathVariable("emservice_id") String emservice_id,EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商查询待发料", tags = {"领料单" } ,notes = "根据服务商查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库库位建立领料单", tags = {"领料单" },  notes = "根据仓库库位建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库库位批量建立领料单", tags = {"领料单" },  notes = "根据仓库库位批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库库位更新领料单", tags = {"领料单" },  notes = "根据仓库库位更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库库位批量更新领料单", tags = {"领料单" },  notes = "根据仓库库位批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库库位删除领料单", tags = {"领料单" },  notes = "根据仓库库位删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库库位批量删除领料单", tags = {"领料单" },  notes = "根据仓库库位批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePart(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库库位获取领料单", tags = {"领料单" },  notes = "根据仓库库位获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位获取领料单草稿", tags = {"领料单" },  notes = "根据仓库库位获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位检查领料单", tags = {"领料单" },  notes = "根据仓库库位检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库库位领料单", tags = {"领料单" },  notes = "根据仓库库位领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库库位领料单", tags = {"领料单" },  notes = "根据仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库库位领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库库位领料单", tags = {"领料单" },  notes = "根据仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库库位保存领料单", tags = {"领料单" },  notes = "根据仓库库位保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库库位批量保存领料单", tags = {"领料单" },  notes = "根据仓库库位批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库库位领料单", tags = {"领料单" },  notes = "根据仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位获取草稿", tags = {"领料单" } ,notes = "根据仓库库位获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位查询草稿", tags = {"领料单" } ,notes = "根据仓库库位查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位获取已发料", tags = {"领料单" } ,notes = "根据仓库库位获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位查询已发料", tags = {"领料单" } ,notes = "根据仓库库位查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位获取待发料", tags = {"领料单" } ,notes = "根据仓库库位获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位查询待发料", tags = {"领料单" } ,notes = "根据仓库库位查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库建立领料单", tags = {"领料单" },  notes = "根据仓库建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库批量建立领料单", tags = {"领料单" },  notes = "根据仓库批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库更新领料单", tags = {"领料单" },  notes = "根据仓库更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库批量更新领料单", tags = {"领料单" },  notes = "根据仓库批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库删除领料单", tags = {"领料单" },  notes = "根据仓库删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库批量删除领料单", tags = {"领料单" },  notes = "根据仓库批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStore(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库获取领料单", tags = {"领料单" },  notes = "根据仓库获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库获取领料单草稿", tags = {"领料单" },  notes = "根据仓库获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStore(@PathVariable("emstore_id") String emstore_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setStoreid(emstore_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库检查领料单", tags = {"领料单" },  notes = "根据仓库检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库领料单", tags = {"领料单" },  notes = "根据仓库领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库领料单", tags = {"领料单" },  notes = "根据仓库领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库领料单", tags = {"领料单" },  notes = "根据仓库领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库保存领料单", tags = {"领料单" },  notes = "根据仓库保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库批量保存领料单", tags = {"领料单" },  notes = "根据仓库批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setStoreid(emstore_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库领料单", tags = {"领料单" },  notes = "根据仓库领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStoreid(emstore_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库获取草稿", tags = {"领料单" } ,notes = "根据仓库获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库查询草稿", tags = {"领料单" } ,notes = "根据仓库查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库获取已发料", tags = {"领料单" } ,notes = "根据仓库获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库查询已发料", tags = {"领料单" } ,notes = "根据仓库查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库获取待发料", tags = {"领料单" } ,notes = "根据仓库获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库查询待发料", tags = {"领料单" } ,notes = "根据仓库查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据班组建立领料单", tags = {"领料单" },  notes = "根据班组建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据班组批量建立领料单", tags = {"领料单" },  notes = "根据班组批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setTeamid(pfteam_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据班组更新领料单", tags = {"领料单" },  notes = "根据班组更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据班组批量更新领料单", tags = {"领料单" },  notes = "根据班组批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setTeamid(pfteam_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据班组删除领料单", tags = {"领料单" },  notes = "根据班组删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据班组批量删除领料单", tags = {"领料单" },  notes = "根据班组批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeam(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据班组获取领料单", tags = {"领料单" },  notes = "根据班组获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组获取领料单草稿", tags = {"领料单" },  notes = "根据班组获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setTeamid(pfteam_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组检查领料单", tags = {"领料单" },  notes = "根据班组检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据班组领料单", tags = {"领料单" },  notes = "根据班组领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据班组领料单", tags = {"领料单" },  notes = "根据班组领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据班组领料单]", tags = {"领料单" },  notes = "批量处理[根据班组领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据班组领料单", tags = {"领料单" },  notes = "根据班组领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据班组保存领料单", tags = {"领料单" },  notes = "根据班组保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据班组批量保存领料单", tags = {"领料单" },  notes = "根据班组批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setTeamid(pfteam_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据班组领料单", tags = {"领料单" },  notes = "根据班组领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setTeamid(pfteam_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组获取DEFAULT", tags = {"领料单" } ,notes = "根据班组获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组查询DEFAULT", tags = {"领料单" } ,notes = "根据班组查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组获取草稿", tags = {"领料单" } ,notes = "根据班组获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组查询草稿", tags = {"领料单" } ,notes = "根据班组查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组获取已发料", tags = {"领料单" } ,notes = "根据班组获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组查询已发料", tags = {"领料单" } ,notes = "根据班组查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组获取待发料", tags = {"领料单" } ,notes = "根据班组获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByPFTeam(@PathVariable("pfteam_id") String pfteam_id,EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组查询待发料", tags = {"领料单" } ,notes = "根据班组查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByPFTeam(@PathVariable("pfteam_id") String pfteam_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_teamid_eq(pfteam_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据班组设备档案建立领料单", tags = {"领料单" },  notes = "根据班组设备档案建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立领料单", tags = {"领料单" },  notes = "根据班组设备档案批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据班组设备档案更新领料单", tags = {"领料单" },  notes = "根据班组设备档案更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新领料单", tags = {"领料单" },  notes = "根据班组设备档案批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据班组设备档案删除领料单", tags = {"领料单" },  notes = "根据班组设备档案删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除领料单", tags = {"领料单" },  notes = "根据班组设备档案批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据班组设备档案获取领料单", tags = {"领料单" },  notes = "根据班组设备档案获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取领料单草稿", tags = {"领料单" },  notes = "根据班组设备档案获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查领料单", tags = {"领料单" },  notes = "根据班组设备档案检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据班组设备档案领料单", tags = {"领料单" },  notes = "根据班组设备档案领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据班组设备档案领料单", tags = {"领料单" },  notes = "根据班组设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案领料单]", tags = {"领料单" },  notes = "批量处理[根据班组设备档案领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据班组设备档案领料单", tags = {"领料单" },  notes = "根据班组设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据班组设备档案保存领料单", tags = {"领料单" },  notes = "根据班组设备档案保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存领料单", tags = {"领料单" },  notes = "根据班组设备档案批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据班组设备档案领料单", tags = {"领料单" },  notes = "根据班组设备档案领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setEquipid(emequip_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"领料单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"领料单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案获取草稿", tags = {"领料单" } ,notes = "根据班组设备档案获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案查询草稿", tags = {"领料单" } ,notes = "根据班组设备档案查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案获取已发料", tags = {"领料单" } ,notes = "根据班组设备档案获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案查询已发料", tags = {"领料单" } ,notes = "根据班组设备档案查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案获取待发料", tags = {"领料单" } ,notes = "根据班组设备档案获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据班组设备档案查询待发料", tags = {"领料单" } ,notes = "根据班组设备档案查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据服务商物品建立领料单", tags = {"领料单" },  notes = "根据服务商物品建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据服务商物品批量建立领料单", tags = {"领料单" },  notes = "根据服务商物品批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据服务商物品更新领料单", tags = {"领料单" },  notes = "根据服务商物品更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据服务商物品批量更新领料单", tags = {"领料单" },  notes = "根据服务商物品批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据服务商物品删除领料单", tags = {"领料单" },  notes = "根据服务商物品删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除领料单", tags = {"领料单" },  notes = "根据服务商物品批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据服务商物品获取领料单", tags = {"领料单" },  notes = "根据服务商物品获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取领料单草稿", tags = {"领料单" },  notes = "根据服务商物品获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查领料单", tags = {"领料单" },  notes = "根据服务商物品检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据服务商物品领料单", tags = {"领料单" },  notes = "根据服务商物品领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据服务商物品领料单", tags = {"领料单" },  notes = "根据服务商物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品领料单]", tags = {"领料单" },  notes = "批量处理[根据服务商物品领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据服务商物品领料单", tags = {"领料单" },  notes = "根据服务商物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据服务商物品保存领料单", tags = {"领料单" },  notes = "根据服务商物品保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据服务商物品批量保存领料单", tags = {"领料单" },  notes = "根据服务商物品批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据服务商物品领料单", tags = {"领料单" },  notes = "根据服务商物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"领料单" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"领料单" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品获取草稿", tags = {"领料单" } ,notes = "根据服务商物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品查询草稿", tags = {"领料单" } ,notes = "根据服务商物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品获取已发料", tags = {"领料单" } ,notes = "根据服务商物品获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品查询已发料", tags = {"领料单" } ,notes = "根据服务商物品查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品获取待发料", tags = {"领料单" } ,notes = "根据服务商物品获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据服务商物品查询待发料", tags = {"领料单" } ,notes = "根据服务商物品查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库库位物品建立领料单", tags = {"领料单" },  notes = "根据仓库库位物品建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立领料单", tags = {"领料单" },  notes = "根据仓库库位物品批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库库位物品更新领料单", tags = {"领料单" },  notes = "根据仓库库位物品更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新领料单", tags = {"领料单" },  notes = "根据仓库库位物品批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除领料单", tags = {"领料单" },  notes = "根据仓库库位物品删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除领料单", tags = {"领料单" },  notes = "根据仓库库位物品批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库库位物品获取领料单", tags = {"领料单" },  notes = "根据仓库库位物品获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取领料单草稿", tags = {"领料单" },  notes = "根据仓库库位物品获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查领料单", tags = {"领料单" },  notes = "根据仓库库位物品检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库库位物品领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库库位物品保存领料单", tags = {"领料单" },  notes = "根据仓库库位物品保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存领料单", tags = {"领料单" },  notes = "根据仓库库位物品批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品获取草稿", tags = {"领料单" } ,notes = "根据仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品查询草稿", tags = {"领料单" } ,notes = "根据仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已发料", tags = {"领料单" } ,notes = "根据仓库库位物品获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已发料", tags = {"领料单" } ,notes = "根据仓库库位物品查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待发料", tags = {"领料单" } ,notes = "根据仓库库位物品获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待发料", tags = {"领料单" } ,notes = "根据仓库库位物品查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库物品建立领料单", tags = {"领料单" },  notes = "根据仓库物品建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库物品批量建立领料单", tags = {"领料单" },  notes = "根据仓库物品批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库物品更新领料单", tags = {"领料单" },  notes = "根据仓库物品更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库物品批量更新领料单", tags = {"领料单" },  notes = "根据仓库物品批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库物品删除领料单", tags = {"领料单" },  notes = "根据仓库物品删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除领料单", tags = {"领料单" },  notes = "根据仓库物品批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库物品获取领料单", tags = {"领料单" },  notes = "根据仓库物品获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取领料单草稿", tags = {"领料单" },  notes = "根据仓库物品获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查领料单", tags = {"领料单" },  notes = "根据仓库物品检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库物品领料单", tags = {"领料单" },  notes = "根据仓库物品领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库物品领料单", tags = {"领料单" },  notes = "根据仓库物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库物品领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库物品领料单", tags = {"领料单" },  notes = "根据仓库物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库物品保存领料单", tags = {"领料单" },  notes = "根据仓库物品保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库物品批量保存领料单", tags = {"领料单" },  notes = "根据仓库物品批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库物品领料单", tags = {"领料单" },  notes = "根据仓库物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品获取草稿", tags = {"领料单" } ,notes = "根据仓库物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品查询草稿", tags = {"领料单" } ,notes = "根据仓库物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品获取已发料", tags = {"领料单" } ,notes = "根据仓库物品获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品查询已发料", tags = {"领料单" } ,notes = "根据仓库物品查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品获取待发料", tags = {"领料单" } ,notes = "根据仓库物品获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库物品查询待发料", tags = {"领料单" } ,notes = "根据仓库物品查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库仓库库位建立领料单", tags = {"领料单" },  notes = "根据仓库仓库库位建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库仓库库位批量建立领料单", tags = {"领料单" },  notes = "根据仓库仓库库位批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库仓库库位更新领料单", tags = {"领料单" },  notes = "根据仓库仓库库位更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库仓库库位批量更新领料单", tags = {"领料单" },  notes = "根据仓库仓库库位批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库仓库库位删除领料单", tags = {"领料单" },  notes = "根据仓库仓库库位删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库仓库库位批量删除领料单", tags = {"领料单" },  notes = "根据仓库仓库库位批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePart(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库仓库库位获取领料单", tags = {"领料单" },  notes = "根据仓库仓库库位获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位获取领料单草稿", tags = {"领料单" },  notes = "根据仓库仓库库位获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位检查领料单", tags = {"领料单" },  notes = "根据仓库仓库库位检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库仓库库位领料单", tags = {"领料单" },  notes = "根据仓库仓库库位领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库仓库库位领料单", tags = {"领料单" },  notes = "根据仓库仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库仓库库位领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库仓库库位领料单", tags = {"领料单" },  notes = "根据仓库仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库仓库库位保存领料单", tags = {"领料单" },  notes = "根据仓库仓库库位保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库仓库库位批量保存领料单", tags = {"领料单" },  notes = "根据仓库仓库库位批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库仓库库位领料单", tags = {"领料单" },  notes = "根据仓库仓库库位领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setStorepartid(emstorepart_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取草稿", tags = {"领料单" } ,notes = "根据仓库仓库库位获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询草稿", tags = {"领料单" } ,notes = "根据仓库仓库库位查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取已发料", tags = {"领料单" } ,notes = "根据仓库仓库库位获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询已发料", tags = {"领料单" } ,notes = "根据仓库仓库库位查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取待发料", tags = {"领料单" } ,notes = "根据仓库仓库库位获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询待发料", tags = {"领料单" } ,notes = "根据仓库仓库库位查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses")
    public ResponseEntity<EMItemPUseDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
		emitempuseService.create(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品批量建立领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitempuse" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain.setEmitempuseid(emitempuse_id);
		emitempuseService.update(domain);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByEntities(this.emitempuseMapping.toDomain(#emitempusedtos)),'eam-EMItemPUse-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品批量更新领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitempuseService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitempuseService.get(#emitempuse_id),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitempuseService.remove(emitempuse_id));
    }

    @PreAuthorize("hasPermission(this.emitempuseService.getEmitempuseByIds(#ids),'eam-EMItemPUse-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品批量删除领料单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emitempuseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitempuseMapping.toDomain(returnObject.body),'eam-EMItemPUse-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品获取领料单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}")
    public ResponseEntity<EMItemPUseDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id) {
        EMItemPUse domain = emitempuseService.get(emitempuse_id);
        EMItemPUseDTO dto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取领料单草稿", tags = {"领料单" },  notes = "根据仓库仓库库位物品获取领料单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/getdraft")
    public ResponseEntity<EMItemPUseDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemPUseDTO dto) {
        EMItemPUse domain = emitempuseMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(emitempuseService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品检查领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitempuseService.checkKey(emitempuseMapping.toDomain(emitempusedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-FormUpdateByITEMID-all')")
    @ApiOperation(value = "根据仓库仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/formupdatebyitemid")
    public ResponseEntity<EMItemPUseDTO> formUpdateByITEMIDByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.formUpdateByITEMID(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Issue-all')")
    @ApiOperation(value = "根据仓库仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/issue")
    public ResponseEntity<EMItemPUseDTO> issueByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.issue(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品领料单]", tags = {"领料单" },  notes = "批量处理[根据仓库仓库库位物品领料单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/issuebatch")
    public ResponseEntity<Boolean> issueByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domains = emitempuseMapping.toDomain(emitempusedtos);
        boolean result = emitempuseService.issueBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Rejected-all')")
    @ApiOperation(value = "根据仓库仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/rejected")
    public ResponseEntity<EMItemPUseDTO> rejectedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.rejected(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedto),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/save")
    public ResponseEntity<EMItemPUseDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        emitempuseService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempuseMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitempuseMapping.toDomain(#emitempusedtos),'eam-EMItemPUse-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品批量保存领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemPUseDTO> emitempusedtos) {
        List<EMItemPUse> domainlist=emitempuseMapping.toDomain(emitempusedtos);
        for(EMItemPUse domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitempuseService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-Submit-all')")
    @ApiOperation(value = "根据仓库仓库库位物品领料单", tags = {"领料单" },  notes = "根据仓库仓库库位物品领料单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/{emitempuse_id}/submit")
    public ResponseEntity<EMItemPUseDTO> submitByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitempuse_id") String emitempuse_id, @RequestBody EMItemPUseDTO emitempusedto) {
        EMItemPUse domain = emitempuseMapping.toDomain(emitempusedto);
        domain.setItemid(emitem_id);
        domain = emitempuseService.submit(domain) ;
        emitempusedto = emitempuseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitempusedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"领料单" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchdefault")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDefault-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"领料单" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchdefault")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取草稿", tags = {"领料单" } ,notes = "根据仓库仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchdraft")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchDraft-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询草稿", tags = {"领料单" } ,notes = "根据仓库仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchdraft")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已发料", tags = {"领料单" } ,notes = "根据仓库仓库库位物品获取已发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchissued")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseIssuedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchIssued-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已发料", tags = {"领料单" } ,notes = "根据仓库仓库库位物品查询已发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchissued")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseIssuedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchIssued(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待发料", tags = {"领料单" } ,notes = "根据仓库仓库库位物品获取待发料")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/fetchwaitissue")
	public ResponseEntity<List<EMItemPUseDTO>> fetchEMItemPUseWaitIssueByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
        List<EMItemPUseDTO> list = emitempuseMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemPUse-searchWaitIssue-all') and hasPermission(#context,'eam-EMItemPUse-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待发料", tags = {"领料单" } ,notes = "根据仓库仓库库位物品查询待发料")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitempuses/searchwaitissue")
	public ResponseEntity<Page<EMItemPUseDTO>> searchEMItemPUseWaitIssueByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemPUseSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemPUse> domains = emitempuseService.searchWaitIssue(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitempuseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

