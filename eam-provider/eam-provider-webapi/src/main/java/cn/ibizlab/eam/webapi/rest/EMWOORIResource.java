package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWOORI;
import cn.ibizlab.eam.core.eam_core.service.IEMWOORIService;
import cn.ibizlab.eam.core.eam_core.filter.EMWOORISearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工单来源" })
@RestController("WebApi-emwoori")
@RequestMapping("")
public class EMWOORIResource {

    @Autowired
    public IEMWOORIService emwooriService;

    @Autowired
    @Lazy
    public EMWOORIMapping emwooriMapping;

    @PreAuthorize("hasPermission(this.emwooriMapping.toDomain(#emwooridto),'eam-EMWOORI-Create')")
    @ApiOperation(value = "新建工单来源", tags = {"工单来源" },  notes = "新建工单来源")
	@RequestMapping(method = RequestMethod.POST, value = "/emwooris")
    public ResponseEntity<EMWOORIDTO> create(@Validated @RequestBody EMWOORIDTO emwooridto) {
        EMWOORI domain = emwooriMapping.toDomain(emwooridto);
		emwooriService.create(domain);
        EMWOORIDTO dto = emwooriMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwooriMapping.toDomain(#emwooridtos),'eam-EMWOORI-Create')")
    @ApiOperation(value = "批量新建工单来源", tags = {"工单来源" },  notes = "批量新建工单来源")
	@RequestMapping(method = RequestMethod.POST, value = "/emwooris/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWOORIDTO> emwooridtos) {
        emwooriService.createBatch(emwooriMapping.toDomain(emwooridtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwoori" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwooriService.get(#emwoori_id),'eam-EMWOORI-Update')")
    @ApiOperation(value = "更新工单来源", tags = {"工单来源" },  notes = "更新工单来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwooris/{emwoori_id}")
    public ResponseEntity<EMWOORIDTO> update(@PathVariable("emwoori_id") String emwoori_id, @RequestBody EMWOORIDTO emwooridto) {
		EMWOORI domain  = emwooriMapping.toDomain(emwooridto);
        domain .setEmwooriid(emwoori_id);
		emwooriService.update(domain );
		EMWOORIDTO dto = emwooriMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwooriService.getEmwooriByEntities(this.emwooriMapping.toDomain(#emwooridtos)),'eam-EMWOORI-Update')")
    @ApiOperation(value = "批量更新工单来源", tags = {"工单来源" },  notes = "批量更新工单来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwooris/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWOORIDTO> emwooridtos) {
        emwooriService.updateBatch(emwooriMapping.toDomain(emwooridtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwooriService.get(#emwoori_id),'eam-EMWOORI-Remove')")
    @ApiOperation(value = "删除工单来源", tags = {"工单来源" },  notes = "删除工单来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwooris/{emwoori_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwoori_id") String emwoori_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwooriService.remove(emwoori_id));
    }

    @PreAuthorize("hasPermission(this.emwooriService.getEmwooriByIds(#ids),'eam-EMWOORI-Remove')")
    @ApiOperation(value = "批量删除工单来源", tags = {"工单来源" },  notes = "批量删除工单来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwooris/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwooriService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwooriMapping.toDomain(returnObject.body),'eam-EMWOORI-Get')")
    @ApiOperation(value = "获取工单来源", tags = {"工单来源" },  notes = "获取工单来源")
	@RequestMapping(method = RequestMethod.GET, value = "/emwooris/{emwoori_id}")
    public ResponseEntity<EMWOORIDTO> get(@PathVariable("emwoori_id") String emwoori_id) {
        EMWOORI domain = emwooriService.get(emwoori_id);
        EMWOORIDTO dto = emwooriMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工单来源草稿", tags = {"工单来源" },  notes = "获取工单来源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwooris/getdraft")
    public ResponseEntity<EMWOORIDTO> getDraft(EMWOORIDTO dto) {
        EMWOORI domain = emwooriMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwooriMapping.toDto(emwooriService.getDraft(domain)));
    }

    @ApiOperation(value = "检查工单来源", tags = {"工单来源" },  notes = "检查工单来源")
	@RequestMapping(method = RequestMethod.POST, value = "/emwooris/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWOORIDTO emwooridto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwooriService.checkKey(emwooriMapping.toDomain(emwooridto)));
    }

    @PreAuthorize("hasPermission(this.emwooriMapping.toDomain(#emwooridto),'eam-EMWOORI-Save')")
    @ApiOperation(value = "保存工单来源", tags = {"工单来源" },  notes = "保存工单来源")
	@RequestMapping(method = RequestMethod.POST, value = "/emwooris/save")
    public ResponseEntity<EMWOORIDTO> save(@RequestBody EMWOORIDTO emwooridto) {
        EMWOORI domain = emwooriMapping.toDomain(emwooridto);
        emwooriService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwooriMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwooriMapping.toDomain(#emwooridtos),'eam-EMWOORI-Save')")
    @ApiOperation(value = "批量保存工单来源", tags = {"工单来源" },  notes = "批量保存工单来源")
	@RequestMapping(method = RequestMethod.POST, value = "/emwooris/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWOORIDTO> emwooridtos) {
        emwooriService.saveBatch(emwooriMapping.toDomain(emwooridtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWOORI-searchDefault-all') and hasPermission(#context,'eam-EMWOORI-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工单来源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwooris/fetchdefault")
	public ResponseEntity<List<EMWOORIDTO>> fetchDefault(EMWOORISearchContext context) {
        Page<EMWOORI> domains = emwooriService.searchDefault(context) ;
        List<EMWOORIDTO> list = emwooriMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWOORI-searchDefault-all') and hasPermission(#context,'eam-EMWOORI-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工单来源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwooris/searchdefault")
	public ResponseEntity<Page<EMWOORIDTO>> searchDefault(@RequestBody EMWOORISearchContext context) {
        Page<EMWOORI> domains = emwooriService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwooriMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWOORI-searchIndexDER-all') and hasPermission(#context,'eam-EMWOORI-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"工单来源" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emwooris/fetchindexder")
	public ResponseEntity<List<EMWOORIDTO>> fetchIndexDER(EMWOORISearchContext context) {
        Page<EMWOORI> domains = emwooriService.searchIndexDER(context) ;
        List<EMWOORIDTO> list = emwooriMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWOORI-searchIndexDER-all') and hasPermission(#context,'eam-EMWOORI-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"工单来源" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emwooris/searchindexder")
	public ResponseEntity<Page<EMWOORIDTO>> searchIndexDER(@RequestBody EMWOORISearchContext context) {
        Page<EMWOORI> domains = emwooriService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwooriMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

