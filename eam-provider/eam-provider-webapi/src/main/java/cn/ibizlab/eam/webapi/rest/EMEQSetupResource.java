package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSetup;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSetupService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSetupSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"更换安装" })
@RestController("WebApi-emeqsetup")
@RequestMapping("")
public class EMEQSetupResource {

    @Autowired
    public IEMEQSetupService emeqsetupService;

    @Autowired
    @Lazy
    public EMEQSetupMapping emeqsetupMapping;

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "新建更换安装", tags = {"更换安装" },  notes = "新建更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsetups")
    public ResponseEntity<EMEQSetupDTO> create(@Validated @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
		emeqsetupService.create(domain);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "批量新建更换安装", tags = {"更换安装" },  notes = "批量新建更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsetups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        emeqsetupService.createBatch(emeqsetupMapping.toDomain(emeqsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "更新更换安装", tags = {"更换安装" },  notes = "更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> update(@PathVariable("emeqsetup_id") String emeqsetup_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
		EMEQSetup domain  = emeqsetupMapping.toDomain(emeqsetupdto);
        domain .setEmeqsetupid(emeqsetup_id);
		emeqsetupService.update(domain );
		EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByEntities(this.emeqsetupMapping.toDomain(#emeqsetupdtos)),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "批量更新更换安装", tags = {"更换安装" },  notes = "批量更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqsetups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        emeqsetupService.updateBatch(emeqsetupMapping.toDomain(emeqsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "删除更换安装", tags = {"更换安装" },  notes = "删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqsetup_id") String emeqsetup_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.remove(emeqsetup_id));
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByIds(#ids),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "批量删除更换安装", tags = {"更换安装" },  notes = "批量删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqsetups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqsetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsetupMapping.toDomain(returnObject.body),'eam-EMEQSetup-Get')")
    @ApiOperation(value = "获取更换安装", tags = {"更换安装" },  notes = "获取更换安装")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> get(@PathVariable("emeqsetup_id") String emeqsetup_id) {
        EMEQSetup domain = emeqsetupService.get(emeqsetup_id);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取更换安装草稿", tags = {"更换安装" },  notes = "获取更换安装草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqsetups/getdraft")
    public ResponseEntity<EMEQSetupDTO> getDraft(EMEQSetupDTO dto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(emeqsetupService.getDraft(domain)));
    }

    @ApiOperation(value = "检查更换安装", tags = {"更换安装" },  notes = "检查更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsetups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSetupDTO emeqsetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.checkKey(emeqsetupMapping.toDomain(emeqsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "保存更换安装", tags = {"更换安装" },  notes = "保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsetups/save")
    public ResponseEntity<EMEQSetupDTO> save(@RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        emeqsetupService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "批量保存更换安装", tags = {"更换安装" },  notes = "批量保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqsetups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        emeqsetupService.saveBatch(emeqsetupMapping.toDomain(emeqsetupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"更换安装" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emeqsetups/fetchcalendar")
	public ResponseEntity<List<EMEQSetupDTO>> fetchCalendar(EMEQSetupSearchContext context) {
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"更换安装" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emeqsetups/searchcalendar")
	public ResponseEntity<Page<EMEQSetupDTO>> searchCalendar(@RequestBody EMEQSetupSearchContext context) {
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"更换安装" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqsetups/fetchdefault")
	public ResponseEntity<List<EMEQSetupDTO>> fetchDefault(EMEQSetupSearchContext context) {
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"更换安装" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqsetups/searchdefault")
	public ResponseEntity<Page<EMEQSetupDTO>> searchDefault(@RequestBody EMEQSetupSearchContext context) {
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "根据设备档案建立更换安装", tags = {"更换安装" },  notes = "根据设备档案建立更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqsetups")
    public ResponseEntity<EMEQSetupDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
		emeqsetupService.create(domain);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "根据设备档案批量建立更换安装", tags = {"更换安装" },  notes = "根据设备档案批量建立更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqsetupService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "根据设备档案更新更换安装", tags = {"更换安装" },  notes = "根据设备档案更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqsetupid(emeqsetup_id);
		emeqsetupService.update(domain);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByEntities(this.emeqsetupMapping.toDomain(#emeqsetupdtos)),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "根据设备档案批量更新更换安装", tags = {"更换安装" },  notes = "根据设备档案批量更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqsetupService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "根据设备档案删除更换安装", tags = {"更换安装" },  notes = "根据设备档案删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.remove(emeqsetup_id));
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByIds(#ids),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "根据设备档案批量删除更换安装", tags = {"更换安装" },  notes = "根据设备档案批量删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqsetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsetupMapping.toDomain(returnObject.body),'eam-EMEQSetup-Get')")
    @ApiOperation(value = "根据设备档案获取更换安装", tags = {"更换安装" },  notes = "根据设备档案获取更换安装")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id) {
        EMEQSetup domain = emeqsetupService.get(emeqsetup_id);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取更换安装草稿", tags = {"更换安装" },  notes = "根据设备档案获取更换安装草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqsetups/getdraft")
    public ResponseEntity<EMEQSetupDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQSetupDTO dto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(emeqsetupService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查更换安装", tags = {"更换安装" },  notes = "根据设备档案检查更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqsetups/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.checkKey(emeqsetupMapping.toDomain(emeqsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "根据设备档案保存更换安装", tags = {"更换安装" },  notes = "根据设备档案保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqsetups/save")
    public ResponseEntity<EMEQSetupDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
        emeqsetupService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "根据设备档案批量保存更换安装", tags = {"更换安装" },  notes = "根据设备档案批量保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqsetups/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqsetupService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"更换安装" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqsetups/fetchcalendar")
	public ResponseEntity<List<EMEQSetupDTO>> fetchEMEQSetupCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"更换安装" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqsetups/searchcalendar")
	public ResponseEntity<Page<EMEQSetupDTO>> searchEMEQSetupCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"更换安装" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqsetups/fetchdefault")
	public ResponseEntity<List<EMEQSetupDTO>> fetchEMEQSetupDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"更换安装" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqsetups/searchdefault")
	public ResponseEntity<Page<EMEQSetupDTO>> searchEMEQSetupDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "根据班组设备档案建立更换安装", tags = {"更换安装" },  notes = "根据班组设备档案建立更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups")
    public ResponseEntity<EMEQSetupDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
		emeqsetupService.create(domain);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立更换安装", tags = {"更换安装" },  notes = "根据班组设备档案批量建立更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqsetupService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqsetup" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "根据班组设备档案更新更换安装", tags = {"更换安装" },  notes = "根据班组设备档案更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqsetupid(emeqsetup_id);
		emeqsetupService.update(domain);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByEntities(this.emeqsetupMapping.toDomain(#emeqsetupdtos)),'eam-EMEQSetup-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新更换安装", tags = {"更换安装" },  notes = "根据班组设备档案批量更新更换安装")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqsetupService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.get(#emeqsetup_id),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "根据班组设备档案删除更换安装", tags = {"更换安装" },  notes = "根据班组设备档案删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.remove(emeqsetup_id));
    }

    @PreAuthorize("hasPermission(this.emeqsetupService.getEmeqsetupByIds(#ids),'eam-EMEQSetup-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除更换安装", tags = {"更换安装" },  notes = "根据班组设备档案批量删除更换安装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqsetupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqsetupMapping.toDomain(returnObject.body),'eam-EMEQSetup-Get')")
    @ApiOperation(value = "根据班组设备档案获取更换安装", tags = {"更换安装" },  notes = "根据班组设备档案获取更换安装")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/{emeqsetup_id}")
    public ResponseEntity<EMEQSetupDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqsetup_id") String emeqsetup_id) {
        EMEQSetup domain = emeqsetupService.get(emeqsetup_id);
        EMEQSetupDTO dto = emeqsetupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取更换安装草稿", tags = {"更换安装" },  notes = "根据班组设备档案获取更换安装草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/getdraft")
    public ResponseEntity<EMEQSetupDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQSetupDTO dto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(emeqsetupService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查更换安装", tags = {"更换安装" },  notes = "根据班组设备档案检查更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqsetupService.checkKey(emeqsetupMapping.toDomain(emeqsetupdto)));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdto),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "根据班组设备档案保存更换安装", tags = {"更换安装" },  notes = "根据班组设备档案保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/save")
    public ResponseEntity<EMEQSetupDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupDTO emeqsetupdto) {
        EMEQSetup domain = emeqsetupMapping.toDomain(emeqsetupdto);
        domain.setEquipid(emequip_id);
        emeqsetupService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqsetupMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqsetupMapping.toDomain(#emeqsetupdtos),'eam-EMEQSetup-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存更换安装", tags = {"更换安装" },  notes = "根据班组设备档案批量保存更换安装")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQSetupDTO> emeqsetupdtos) {
        List<EMEQSetup> domainlist=emeqsetupMapping.toDomain(emeqsetupdtos);
        for(EMEQSetup domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqsetupService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"更换安装" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/fetchcalendar")
	public ResponseEntity<List<EMEQSetupDTO>> fetchEMEQSetupCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchCalendar-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"更换安装" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/searchcalendar")
	public ResponseEntity<Page<EMEQSetupDTO>> searchEMEQSetupCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"更换安装" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/fetchdefault")
	public ResponseEntity<List<EMEQSetupDTO>> fetchEMEQSetupDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
        List<EMEQSetupDTO> list = emeqsetupMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSetup-searchDefault-all') and hasPermission(#context,'eam-EMEQSetup-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"更换安装" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqsetups/searchdefault")
	public ResponseEntity<Page<EMEQSetupDTO>> searchEMEQSetupDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQSetupSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQSetup> domains = emeqsetupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqsetupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

