package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory;
import cn.ibizlab.eam.core.eam_core.service.IEMMachineCategoryService;
import cn.ibizlab.eam.core.eam_core.filter.EMMachineCategorySearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"机种编号" })
@RestController("WebApi-emmachinecategory")
@RequestMapping("")
public class EMMachineCategoryResource {

    @Autowired
    public IEMMachineCategoryService emmachinecategoryService;

    @Autowired
    @Lazy
    public EMMachineCategoryMapping emmachinecategoryMapping;

    @PreAuthorize("hasPermission(this.emmachinecategoryMapping.toDomain(#emmachinecategorydto),'eam-EMMachineCategory-Create')")
    @ApiOperation(value = "新建机种编号", tags = {"机种编号" },  notes = "新建机种编号")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachinecategories")
    public ResponseEntity<EMMachineCategoryDTO> create(@Validated @RequestBody EMMachineCategoryDTO emmachinecategorydto) {
        EMMachineCategory domain = emmachinecategoryMapping.toDomain(emmachinecategorydto);
		emmachinecategoryService.create(domain);
        EMMachineCategoryDTO dto = emmachinecategoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryMapping.toDomain(#emmachinecategorydtos),'eam-EMMachineCategory-Create')")
    @ApiOperation(value = "批量新建机种编号", tags = {"机种编号" },  notes = "批量新建机种编号")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachinecategories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMMachineCategoryDTO> emmachinecategorydtos) {
        emmachinecategoryService.createBatch(emmachinecategoryMapping.toDomain(emmachinecategorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emmachinecategory" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emmachinecategoryService.get(#emmachinecategory_id),'eam-EMMachineCategory-Update')")
    @ApiOperation(value = "更新机种编号", tags = {"机种编号" },  notes = "更新机种编号")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmachinecategories/{emmachinecategory_id}")
    public ResponseEntity<EMMachineCategoryDTO> update(@PathVariable("emmachinecategory_id") String emmachinecategory_id, @RequestBody EMMachineCategoryDTO emmachinecategorydto) {
		EMMachineCategory domain  = emmachinecategoryMapping.toDomain(emmachinecategorydto);
        domain .setEmmachinecategoryid(emmachinecategory_id);
		emmachinecategoryService.update(domain );
		EMMachineCategoryDTO dto = emmachinecategoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryService.getEmmachinecategoryByEntities(this.emmachinecategoryMapping.toDomain(#emmachinecategorydtos)),'eam-EMMachineCategory-Update')")
    @ApiOperation(value = "批量更新机种编号", tags = {"机种编号" },  notes = "批量更新机种编号")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmachinecategories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMMachineCategoryDTO> emmachinecategorydtos) {
        emmachinecategoryService.updateBatch(emmachinecategoryMapping.toDomain(emmachinecategorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryService.get(#emmachinecategory_id),'eam-EMMachineCategory-Remove')")
    @ApiOperation(value = "删除机种编号", tags = {"机种编号" },  notes = "删除机种编号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmachinecategories/{emmachinecategory_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emmachinecategory_id") String emmachinecategory_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emmachinecategoryService.remove(emmachinecategory_id));
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryService.getEmmachinecategoryByIds(#ids),'eam-EMMachineCategory-Remove')")
    @ApiOperation(value = "批量删除机种编号", tags = {"机种编号" },  notes = "批量删除机种编号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmachinecategories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emmachinecategoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emmachinecategoryMapping.toDomain(returnObject.body),'eam-EMMachineCategory-Get')")
    @ApiOperation(value = "获取机种编号", tags = {"机种编号" },  notes = "获取机种编号")
	@RequestMapping(method = RequestMethod.GET, value = "/emmachinecategories/{emmachinecategory_id}")
    public ResponseEntity<EMMachineCategoryDTO> get(@PathVariable("emmachinecategory_id") String emmachinecategory_id) {
        EMMachineCategory domain = emmachinecategoryService.get(emmachinecategory_id);
        EMMachineCategoryDTO dto = emmachinecategoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取机种编号草稿", tags = {"机种编号" },  notes = "获取机种编号草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emmachinecategories/getdraft")
    public ResponseEntity<EMMachineCategoryDTO> getDraft(EMMachineCategoryDTO dto) {
        EMMachineCategory domain = emmachinecategoryMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emmachinecategoryMapping.toDto(emmachinecategoryService.getDraft(domain)));
    }

    @ApiOperation(value = "检查机种编号", tags = {"机种编号" },  notes = "检查机种编号")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachinecategories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMMachineCategoryDTO emmachinecategorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emmachinecategoryService.checkKey(emmachinecategoryMapping.toDomain(emmachinecategorydto)));
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryMapping.toDomain(#emmachinecategorydto),'eam-EMMachineCategory-Save')")
    @ApiOperation(value = "保存机种编号", tags = {"机种编号" },  notes = "保存机种编号")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachinecategories/save")
    public ResponseEntity<EMMachineCategoryDTO> save(@RequestBody EMMachineCategoryDTO emmachinecategorydto) {
        EMMachineCategory domain = emmachinecategoryMapping.toDomain(emmachinecategorydto);
        emmachinecategoryService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emmachinecategoryMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emmachinecategoryMapping.toDomain(#emmachinecategorydtos),'eam-EMMachineCategory-Save')")
    @ApiOperation(value = "批量保存机种编号", tags = {"机种编号" },  notes = "批量保存机种编号")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachinecategories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMMachineCategoryDTO> emmachinecategorydtos) {
        emmachinecategoryService.saveBatch(emmachinecategoryMapping.toDomain(emmachinecategorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMachineCategory-searchDefault-all') and hasPermission(#context,'eam-EMMachineCategory-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"机种编号" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emmachinecategories/fetchdefault")
	public ResponseEntity<List<EMMachineCategoryDTO>> fetchDefault(EMMachineCategorySearchContext context) {
        Page<EMMachineCategory> domains = emmachinecategoryService.searchDefault(context) ;
        List<EMMachineCategoryDTO> list = emmachinecategoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMachineCategory-searchDefault-all') and hasPermission(#context,'eam-EMMachineCategory-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"机种编号" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emmachinecategories/searchdefault")
	public ResponseEntity<Page<EMMachineCategoryDTO>> searchDefault(@RequestBody EMMachineCategorySearchContext context) {
        Page<EMMachineCategory> domains = emmachinecategoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emmachinecategoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

