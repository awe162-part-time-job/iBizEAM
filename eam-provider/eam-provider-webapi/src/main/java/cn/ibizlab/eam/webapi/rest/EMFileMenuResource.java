package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMFileMenu;
import cn.ibizlab.eam.core.eam_core.service.IEMFileMenuService;
import cn.ibizlab.eam.core.eam_core.filter.EMFileMenuSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"文件夹目录" })
@RestController("WebApi-emfilemenu")
@RequestMapping("")
public class EMFileMenuResource {

    @Autowired
    public IEMFileMenuService emfilemenuService;

    @Autowired
    @Lazy
    public EMFileMenuMapping emfilemenuMapping;

    @PreAuthorize("hasPermission(this.emfilemenuMapping.toDomain(#emfilemenudto),'eam-EMFileMenu-Create')")
    @ApiOperation(value = "新建文件夹目录", tags = {"文件夹目录" },  notes = "新建文件夹目录")
	@RequestMapping(method = RequestMethod.POST, value = "/emfilemenus")
    public ResponseEntity<EMFileMenuDTO> create(@Validated @RequestBody EMFileMenuDTO emfilemenudto) {
        EMFileMenu domain = emfilemenuMapping.toDomain(emfilemenudto);
		emfilemenuService.create(domain);
        EMFileMenuDTO dto = emfilemenuMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emfilemenuMapping.toDomain(#emfilemenudtos),'eam-EMFileMenu-Create')")
    @ApiOperation(value = "批量新建文件夹目录", tags = {"文件夹目录" },  notes = "批量新建文件夹目录")
	@RequestMapping(method = RequestMethod.POST, value = "/emfilemenus/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMFileMenuDTO> emfilemenudtos) {
        emfilemenuService.createBatch(emfilemenuMapping.toDomain(emfilemenudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emfilemenu" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emfilemenuService.get(#emfilemenu_id),'eam-EMFileMenu-Update')")
    @ApiOperation(value = "更新文件夹目录", tags = {"文件夹目录" },  notes = "更新文件夹目录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emfilemenus/{emfilemenu_id}")
    public ResponseEntity<EMFileMenuDTO> update(@PathVariable("emfilemenu_id") String emfilemenu_id, @RequestBody EMFileMenuDTO emfilemenudto) {
		EMFileMenu domain  = emfilemenuMapping.toDomain(emfilemenudto);
        domain .setEmfilemenuid(emfilemenu_id);
		emfilemenuService.update(domain );
		EMFileMenuDTO dto = emfilemenuMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emfilemenuService.getEmfilemenuByEntities(this.emfilemenuMapping.toDomain(#emfilemenudtos)),'eam-EMFileMenu-Update')")
    @ApiOperation(value = "批量更新文件夹目录", tags = {"文件夹目录" },  notes = "批量更新文件夹目录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emfilemenus/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMFileMenuDTO> emfilemenudtos) {
        emfilemenuService.updateBatch(emfilemenuMapping.toDomain(emfilemenudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emfilemenuService.get(#emfilemenu_id),'eam-EMFileMenu-Remove')")
    @ApiOperation(value = "删除文件夹目录", tags = {"文件夹目录" },  notes = "删除文件夹目录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emfilemenus/{emfilemenu_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emfilemenu_id") String emfilemenu_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emfilemenuService.remove(emfilemenu_id));
    }

    @PreAuthorize("hasPermission(this.emfilemenuService.getEmfilemenuByIds(#ids),'eam-EMFileMenu-Remove')")
    @ApiOperation(value = "批量删除文件夹目录", tags = {"文件夹目录" },  notes = "批量删除文件夹目录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emfilemenus/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emfilemenuService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emfilemenuMapping.toDomain(returnObject.body),'eam-EMFileMenu-Get')")
    @ApiOperation(value = "获取文件夹目录", tags = {"文件夹目录" },  notes = "获取文件夹目录")
	@RequestMapping(method = RequestMethod.GET, value = "/emfilemenus/{emfilemenu_id}")
    public ResponseEntity<EMFileMenuDTO> get(@PathVariable("emfilemenu_id") String emfilemenu_id) {
        EMFileMenu domain = emfilemenuService.get(emfilemenu_id);
        EMFileMenuDTO dto = emfilemenuMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取文件夹目录草稿", tags = {"文件夹目录" },  notes = "获取文件夹目录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emfilemenus/getdraft")
    public ResponseEntity<EMFileMenuDTO> getDraft(EMFileMenuDTO dto) {
        EMFileMenu domain = emfilemenuMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emfilemenuMapping.toDto(emfilemenuService.getDraft(domain)));
    }

    @ApiOperation(value = "检查文件夹目录", tags = {"文件夹目录" },  notes = "检查文件夹目录")
	@RequestMapping(method = RequestMethod.POST, value = "/emfilemenus/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMFileMenuDTO emfilemenudto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emfilemenuService.checkKey(emfilemenuMapping.toDomain(emfilemenudto)));
    }

    @PreAuthorize("hasPermission(this.emfilemenuMapping.toDomain(#emfilemenudto),'eam-EMFileMenu-Save')")
    @ApiOperation(value = "保存文件夹目录", tags = {"文件夹目录" },  notes = "保存文件夹目录")
	@RequestMapping(method = RequestMethod.POST, value = "/emfilemenus/save")
    public ResponseEntity<EMFileMenuDTO> save(@RequestBody EMFileMenuDTO emfilemenudto) {
        EMFileMenu domain = emfilemenuMapping.toDomain(emfilemenudto);
        emfilemenuService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emfilemenuMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emfilemenuMapping.toDomain(#emfilemenudtos),'eam-EMFileMenu-Save')")
    @ApiOperation(value = "批量保存文件夹目录", tags = {"文件夹目录" },  notes = "批量保存文件夹目录")
	@RequestMapping(method = RequestMethod.POST, value = "/emfilemenus/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMFileMenuDTO> emfilemenudtos) {
        emfilemenuService.saveBatch(emfilemenuMapping.toDomain(emfilemenudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMFileMenu-searchDefault-all') and hasPermission(#context,'eam-EMFileMenu-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"文件夹目录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emfilemenus/fetchdefault")
	public ResponseEntity<List<EMFileMenuDTO>> fetchDefault(EMFileMenuSearchContext context) {
        Page<EMFileMenu> domains = emfilemenuService.searchDefault(context) ;
        List<EMFileMenuDTO> list = emfilemenuMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMFileMenu-searchDefault-all') and hasPermission(#context,'eam-EMFileMenu-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"文件夹目录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emfilemenus/searchdefault")
	public ResponseEntity<Page<EMFileMenuDTO>> searchDefault(@RequestBody EMFileMenuSearchContext context) {
        Page<EMFileMenu> domains = emfilemenuService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emfilemenuMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

