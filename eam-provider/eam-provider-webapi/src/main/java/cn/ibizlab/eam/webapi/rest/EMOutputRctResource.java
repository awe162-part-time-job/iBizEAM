package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMOutputRct;
import cn.ibizlab.eam.core.eam_core.service.IEMOutputRctService;
import cn.ibizlab.eam.core.eam_core.filter.EMOutputRctSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产能" })
@RestController("WebApi-emoutputrct")
@RequestMapping("")
public class EMOutputRctResource {

    @Autowired
    public IEMOutputRctService emoutputrctService;

    @Autowired
    @Lazy
    public EMOutputRctMapping emoutputrctMapping;

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdto),'eam-EMOutputRct-Create')")
    @ApiOperation(value = "新建产能", tags = {"产能" },  notes = "新建产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputrcts")
    public ResponseEntity<EMOutputRctDTO> create(@Validated @RequestBody EMOutputRctDTO emoutputrctdto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(emoutputrctdto);
		emoutputrctService.create(domain);
        EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdtos),'eam-EMOutputRct-Create')")
    @ApiOperation(value = "批量新建产能", tags = {"产能" },  notes = "批量新建产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputrcts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        emoutputrctService.createBatch(emoutputrctMapping.toDomain(emoutputrctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emoutputrct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emoutputrctService.get(#emoutputrct_id),'eam-EMOutputRct-Update')")
    @ApiOperation(value = "更新产能", tags = {"产能" },  notes = "更新产能")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<EMOutputRctDTO> update(@PathVariable("emoutputrct_id") String emoutputrct_id, @RequestBody EMOutputRctDTO emoutputrctdto) {
		EMOutputRct domain  = emoutputrctMapping.toDomain(emoutputrctdto);
        domain .setEmoutputrctid(emoutputrct_id);
		emoutputrctService.update(domain );
		EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.getEmoutputrctByEntities(this.emoutputrctMapping.toDomain(#emoutputrctdtos)),'eam-EMOutputRct-Update')")
    @ApiOperation(value = "批量更新产能", tags = {"产能" },  notes = "批量更新产能")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputrcts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        emoutputrctService.updateBatch(emoutputrctMapping.toDomain(emoutputrctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.get(#emoutputrct_id),'eam-EMOutputRct-Remove')")
    @ApiOperation(value = "删除产能", tags = {"产能" },  notes = "删除产能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emoutputrct_id") String emoutputrct_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emoutputrctService.remove(emoutputrct_id));
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.getEmoutputrctByIds(#ids),'eam-EMOutputRct-Remove')")
    @ApiOperation(value = "批量删除产能", tags = {"产能" },  notes = "批量删除产能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputrcts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emoutputrctService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emoutputrctMapping.toDomain(returnObject.body),'eam-EMOutputRct-Get')")
    @ApiOperation(value = "获取产能", tags = {"产能" },  notes = "获取产能")
	@RequestMapping(method = RequestMethod.GET, value = "/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<EMOutputRctDTO> get(@PathVariable("emoutputrct_id") String emoutputrct_id) {
        EMOutputRct domain = emoutputrctService.get(emoutputrct_id);
        EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产能草稿", tags = {"产能" },  notes = "获取产能草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emoutputrcts/getdraft")
    public ResponseEntity<EMOutputRctDTO> getDraft(EMOutputRctDTO dto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputrctMapping.toDto(emoutputrctService.getDraft(domain)));
    }

    @ApiOperation(value = "检查产能", tags = {"产能" },  notes = "检查产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputrcts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMOutputRctDTO emoutputrctdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emoutputrctService.checkKey(emoutputrctMapping.toDomain(emoutputrctdto)));
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdto),'eam-EMOutputRct-Save')")
    @ApiOperation(value = "保存产能", tags = {"产能" },  notes = "保存产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputrcts/save")
    public ResponseEntity<EMOutputRctDTO> save(@RequestBody EMOutputRctDTO emoutputrctdto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(emoutputrctdto);
        emoutputrctService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputrctMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdtos),'eam-EMOutputRct-Save')")
    @ApiOperation(value = "批量保存产能", tags = {"产能" },  notes = "批量保存产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputrcts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        emoutputrctService.saveBatch(emoutputrctMapping.toDomain(emoutputrctdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutputRct-searchDefault-all') and hasPermission(#context,'eam-EMOutputRct-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"产能" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emoutputrcts/fetchdefault")
	public ResponseEntity<List<EMOutputRctDTO>> fetchDefault(EMOutputRctSearchContext context) {
        Page<EMOutputRct> domains = emoutputrctService.searchDefault(context) ;
        List<EMOutputRctDTO> list = emoutputrctMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutputRct-searchDefault-all') and hasPermission(#context,'eam-EMOutputRct-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"产能" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emoutputrcts/searchdefault")
	public ResponseEntity<Page<EMOutputRctDTO>> searchDefault(@RequestBody EMOutputRctSearchContext context) {
        Page<EMOutputRct> domains = emoutputrctService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emoutputrctMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdto),'eam-EMOutputRct-Create')")
    @ApiOperation(value = "根据能力建立产能", tags = {"产能" },  notes = "根据能力建立产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/{emoutput_id}/emoutputrcts")
    public ResponseEntity<EMOutputRctDTO> createByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody EMOutputRctDTO emoutputrctdto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(emoutputrctdto);
        domain.setOutputid(emoutput_id);
		emoutputrctService.create(domain);
        EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdtos),'eam-EMOutputRct-Create')")
    @ApiOperation(value = "根据能力批量建立产能", tags = {"产能" },  notes = "根据能力批量建立产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/{emoutput_id}/emoutputrcts/batch")
    public ResponseEntity<Boolean> createBatchByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        List<EMOutputRct> domainlist=emoutputrctMapping.toDomain(emoutputrctdtos);
        for(EMOutputRct domain:domainlist){
            domain.setOutputid(emoutput_id);
        }
        emoutputrctService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emoutputrct" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emoutputrctService.get(#emoutputrct_id),'eam-EMOutputRct-Update')")
    @ApiOperation(value = "根据能力更新产能", tags = {"产能" },  notes = "根据能力更新产能")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputs/{emoutput_id}/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<EMOutputRctDTO> updateByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @PathVariable("emoutputrct_id") String emoutputrct_id, @RequestBody EMOutputRctDTO emoutputrctdto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(emoutputrctdto);
        domain.setOutputid(emoutput_id);
        domain.setEmoutputrctid(emoutputrct_id);
		emoutputrctService.update(domain);
        EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.getEmoutputrctByEntities(this.emoutputrctMapping.toDomain(#emoutputrctdtos)),'eam-EMOutputRct-Update')")
    @ApiOperation(value = "根据能力批量更新产能", tags = {"产能" },  notes = "根据能力批量更新产能")
	@RequestMapping(method = RequestMethod.PUT, value = "/emoutputs/{emoutput_id}/emoutputrcts/batch")
    public ResponseEntity<Boolean> updateBatchByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        List<EMOutputRct> domainlist=emoutputrctMapping.toDomain(emoutputrctdtos);
        for(EMOutputRct domain:domainlist){
            domain.setOutputid(emoutput_id);
        }
        emoutputrctService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.get(#emoutputrct_id),'eam-EMOutputRct-Remove')")
    @ApiOperation(value = "根据能力删除产能", tags = {"产能" },  notes = "根据能力删除产能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputs/{emoutput_id}/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<Boolean> removeByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @PathVariable("emoutputrct_id") String emoutputrct_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emoutputrctService.remove(emoutputrct_id));
    }

    @PreAuthorize("hasPermission(this.emoutputrctService.getEmoutputrctByIds(#ids),'eam-EMOutputRct-Remove')")
    @ApiOperation(value = "根据能力批量删除产能", tags = {"产能" },  notes = "根据能力批量删除产能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emoutputs/{emoutput_id}/emoutputrcts/batch")
    public ResponseEntity<Boolean> removeBatchByEMOutput(@RequestBody List<String> ids) {
        emoutputrctService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emoutputrctMapping.toDomain(returnObject.body),'eam-EMOutputRct-Get')")
    @ApiOperation(value = "根据能力获取产能", tags = {"产能" },  notes = "根据能力获取产能")
	@RequestMapping(method = RequestMethod.GET, value = "/emoutputs/{emoutput_id}/emoutputrcts/{emoutputrct_id}")
    public ResponseEntity<EMOutputRctDTO> getByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @PathVariable("emoutputrct_id") String emoutputrct_id) {
        EMOutputRct domain = emoutputrctService.get(emoutputrct_id);
        EMOutputRctDTO dto = emoutputrctMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据能力获取产能草稿", tags = {"产能" },  notes = "根据能力获取产能草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emoutputs/{emoutput_id}/emoutputrcts/getdraft")
    public ResponseEntity<EMOutputRctDTO> getDraftByEMOutput(@PathVariable("emoutput_id") String emoutput_id, EMOutputRctDTO dto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(dto);
        domain.setOutputid(emoutput_id);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputrctMapping.toDto(emoutputrctService.getDraft(domain)));
    }

    @ApiOperation(value = "根据能力检查产能", tags = {"产能" },  notes = "根据能力检查产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/{emoutput_id}/emoutputrcts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody EMOutputRctDTO emoutputrctdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emoutputrctService.checkKey(emoutputrctMapping.toDomain(emoutputrctdto)));
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdto),'eam-EMOutputRct-Save')")
    @ApiOperation(value = "根据能力保存产能", tags = {"产能" },  notes = "根据能力保存产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/{emoutput_id}/emoutputrcts/save")
    public ResponseEntity<EMOutputRctDTO> saveByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody EMOutputRctDTO emoutputrctdto) {
        EMOutputRct domain = emoutputrctMapping.toDomain(emoutputrctdto);
        domain.setOutputid(emoutput_id);
        emoutputrctService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emoutputrctMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emoutputrctMapping.toDomain(#emoutputrctdtos),'eam-EMOutputRct-Save')")
    @ApiOperation(value = "根据能力批量保存产能", tags = {"产能" },  notes = "根据能力批量保存产能")
	@RequestMapping(method = RequestMethod.POST, value = "/emoutputs/{emoutput_id}/emoutputrcts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody List<EMOutputRctDTO> emoutputrctdtos) {
        List<EMOutputRct> domainlist=emoutputrctMapping.toDomain(emoutputrctdtos);
        for(EMOutputRct domain:domainlist){
             domain.setOutputid(emoutput_id);
        }
        emoutputrctService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutputRct-searchDefault-all') and hasPermission(#context,'eam-EMOutputRct-Get')")
	@ApiOperation(value = "根据能力获取DEFAULT", tags = {"产能" } ,notes = "根据能力获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emoutputs/{emoutput_id}/emoutputrcts/fetchdefault")
	public ResponseEntity<List<EMOutputRctDTO>> fetchEMOutputRctDefaultByEMOutput(@PathVariable("emoutput_id") String emoutput_id,EMOutputRctSearchContext context) {
        context.setN_outputid_eq(emoutput_id);
        Page<EMOutputRct> domains = emoutputrctService.searchDefault(context) ;
        List<EMOutputRctDTO> list = emoutputrctMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMOutputRct-searchDefault-all') and hasPermission(#context,'eam-EMOutputRct-Get')")
	@ApiOperation(value = "根据能力查询DEFAULT", tags = {"产能" } ,notes = "根据能力查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emoutputs/{emoutput_id}/emoutputrcts/searchdefault")
	public ResponseEntity<Page<EMOutputRctDTO>> searchEMOutputRctDefaultByEMOutput(@PathVariable("emoutput_id") String emoutput_id, @RequestBody EMOutputRctSearchContext context) {
        context.setN_outputid_eq(emoutput_id);
        Page<EMOutputRct> domains = emoutputrctService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emoutputrctMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

