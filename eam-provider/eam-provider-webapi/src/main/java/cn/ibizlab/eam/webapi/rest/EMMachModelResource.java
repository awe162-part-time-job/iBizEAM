package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMMachModel;
import cn.ibizlab.eam.core.eam_core.service.IEMMachModelService;
import cn.ibizlab.eam.core.eam_core.filter.EMMachModelSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"机型" })
@RestController("WebApi-emmachmodel")
@RequestMapping("")
public class EMMachModelResource {

    @Autowired
    public IEMMachModelService emmachmodelService;

    @Autowired
    @Lazy
    public EMMachModelMapping emmachmodelMapping;

    @PreAuthorize("hasPermission(this.emmachmodelMapping.toDomain(#emmachmodeldto),'eam-EMMachModel-Create')")
    @ApiOperation(value = "新建机型", tags = {"机型" },  notes = "新建机型")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachmodels")
    public ResponseEntity<EMMachModelDTO> create(@Validated @RequestBody EMMachModelDTO emmachmodeldto) {
        EMMachModel domain = emmachmodelMapping.toDomain(emmachmodeldto);
		emmachmodelService.create(domain);
        EMMachModelDTO dto = emmachmodelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmachmodelMapping.toDomain(#emmachmodeldtos),'eam-EMMachModel-Create')")
    @ApiOperation(value = "批量新建机型", tags = {"机型" },  notes = "批量新建机型")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachmodels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMMachModelDTO> emmachmodeldtos) {
        emmachmodelService.createBatch(emmachmodelMapping.toDomain(emmachmodeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emmachmodel" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emmachmodelService.get(#emmachmodel_id),'eam-EMMachModel-Update')")
    @ApiOperation(value = "更新机型", tags = {"机型" },  notes = "更新机型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmachmodels/{emmachmodel_id}")
    public ResponseEntity<EMMachModelDTO> update(@PathVariable("emmachmodel_id") String emmachmodel_id, @RequestBody EMMachModelDTO emmachmodeldto) {
		EMMachModel domain  = emmachmodelMapping.toDomain(emmachmodeldto);
        domain .setEmmachmodelid(emmachmodel_id);
		emmachmodelService.update(domain );
		EMMachModelDTO dto = emmachmodelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emmachmodelService.getEmmachmodelByEntities(this.emmachmodelMapping.toDomain(#emmachmodeldtos)),'eam-EMMachModel-Update')")
    @ApiOperation(value = "批量更新机型", tags = {"机型" },  notes = "批量更新机型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emmachmodels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMMachModelDTO> emmachmodeldtos) {
        emmachmodelService.updateBatch(emmachmodelMapping.toDomain(emmachmodeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emmachmodelService.get(#emmachmodel_id),'eam-EMMachModel-Remove')")
    @ApiOperation(value = "删除机型", tags = {"机型" },  notes = "删除机型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmachmodels/{emmachmodel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emmachmodel_id") String emmachmodel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emmachmodelService.remove(emmachmodel_id));
    }

    @PreAuthorize("hasPermission(this.emmachmodelService.getEmmachmodelByIds(#ids),'eam-EMMachModel-Remove')")
    @ApiOperation(value = "批量删除机型", tags = {"机型" },  notes = "批量删除机型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emmachmodels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emmachmodelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emmachmodelMapping.toDomain(returnObject.body),'eam-EMMachModel-Get')")
    @ApiOperation(value = "获取机型", tags = {"机型" },  notes = "获取机型")
	@RequestMapping(method = RequestMethod.GET, value = "/emmachmodels/{emmachmodel_id}")
    public ResponseEntity<EMMachModelDTO> get(@PathVariable("emmachmodel_id") String emmachmodel_id) {
        EMMachModel domain = emmachmodelService.get(emmachmodel_id);
        EMMachModelDTO dto = emmachmodelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取机型草稿", tags = {"机型" },  notes = "获取机型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emmachmodels/getdraft")
    public ResponseEntity<EMMachModelDTO> getDraft(EMMachModelDTO dto) {
        EMMachModel domain = emmachmodelMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emmachmodelMapping.toDto(emmachmodelService.getDraft(domain)));
    }

    @ApiOperation(value = "检查机型", tags = {"机型" },  notes = "检查机型")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachmodels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMMachModelDTO emmachmodeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emmachmodelService.checkKey(emmachmodelMapping.toDomain(emmachmodeldto)));
    }

    @PreAuthorize("hasPermission(this.emmachmodelMapping.toDomain(#emmachmodeldto),'eam-EMMachModel-Save')")
    @ApiOperation(value = "保存机型", tags = {"机型" },  notes = "保存机型")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachmodels/save")
    public ResponseEntity<EMMachModelDTO> save(@RequestBody EMMachModelDTO emmachmodeldto) {
        EMMachModel domain = emmachmodelMapping.toDomain(emmachmodeldto);
        emmachmodelService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emmachmodelMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emmachmodelMapping.toDomain(#emmachmodeldtos),'eam-EMMachModel-Save')")
    @ApiOperation(value = "批量保存机型", tags = {"机型" },  notes = "批量保存机型")
	@RequestMapping(method = RequestMethod.POST, value = "/emmachmodels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMMachModelDTO> emmachmodeldtos) {
        emmachmodelService.saveBatch(emmachmodelMapping.toDomain(emmachmodeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMachModel-searchDefault-all') and hasPermission(#context,'eam-EMMachModel-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"机型" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emmachmodels/fetchdefault")
	public ResponseEntity<List<EMMachModelDTO>> fetchDefault(EMMachModelSearchContext context) {
        Page<EMMachModel> domains = emmachmodelService.searchDefault(context) ;
        List<EMMachModelDTO> list = emmachmodelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMMachModel-searchDefault-all') and hasPermission(#context,'eam-EMMachModel-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"机型" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emmachmodels/searchdefault")
	public ResponseEntity<Page<EMMachModelDTO>> searchDefault(@RequestBody EMMachModelSearchContext context) {
        Page<EMMachModel> domains = emmachmodelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emmachmodelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

