package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmpPostMap;
import cn.ibizlab.eam.core.eam_pf.service.IPFEmpPostMapService;
import cn.ibizlab.eam.core.eam_pf.filter.PFEmpPostMapSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"人事关系" })
@RestController("WebApi-pfemppostmap")
@RequestMapping("")
public class PFEmpPostMapResource {

    @Autowired
    public IPFEmpPostMapService pfemppostmapService;

    @Autowired
    @Lazy
    public PFEmpPostMapMapping pfemppostmapMapping;

    @PreAuthorize("hasPermission(this.pfemppostmapMapping.toDomain(#pfemppostmapdto),'eam-PFEmpPostMap-Create')")
    @ApiOperation(value = "新建人事关系", tags = {"人事关系" },  notes = "新建人事关系")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemppostmaps")
    public ResponseEntity<PFEmpPostMapDTO> create(@Validated @RequestBody PFEmpPostMapDTO pfemppostmapdto) {
        PFEmpPostMap domain = pfemppostmapMapping.toDomain(pfemppostmapdto);
		pfemppostmapService.create(domain);
        PFEmpPostMapDTO dto = pfemppostmapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfemppostmapMapping.toDomain(#pfemppostmapdtos),'eam-PFEmpPostMap-Create')")
    @ApiOperation(value = "批量新建人事关系", tags = {"人事关系" },  notes = "批量新建人事关系")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemppostmaps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFEmpPostMapDTO> pfemppostmapdtos) {
        pfemppostmapService.createBatch(pfemppostmapMapping.toDomain(pfemppostmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfemppostmap" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfemppostmapService.get(#pfemppostmap_id),'eam-PFEmpPostMap-Update')")
    @ApiOperation(value = "更新人事关系", tags = {"人事关系" },  notes = "更新人事关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfemppostmaps/{pfemppostmap_id}")
    public ResponseEntity<PFEmpPostMapDTO> update(@PathVariable("pfemppostmap_id") String pfemppostmap_id, @RequestBody PFEmpPostMapDTO pfemppostmapdto) {
		PFEmpPostMap domain  = pfemppostmapMapping.toDomain(pfemppostmapdto);
        domain .setPfemppostmapid(pfemppostmap_id);
		pfemppostmapService.update(domain );
		PFEmpPostMapDTO dto = pfemppostmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfemppostmapService.getPfemppostmapByEntities(this.pfemppostmapMapping.toDomain(#pfemppostmapdtos)),'eam-PFEmpPostMap-Update')")
    @ApiOperation(value = "批量更新人事关系", tags = {"人事关系" },  notes = "批量更新人事关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfemppostmaps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFEmpPostMapDTO> pfemppostmapdtos) {
        pfemppostmapService.updateBatch(pfemppostmapMapping.toDomain(pfemppostmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfemppostmapService.get(#pfemppostmap_id),'eam-PFEmpPostMap-Remove')")
    @ApiOperation(value = "删除人事关系", tags = {"人事关系" },  notes = "删除人事关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfemppostmaps/{pfemppostmap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfemppostmap_id") String pfemppostmap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfemppostmapService.remove(pfemppostmap_id));
    }

    @PreAuthorize("hasPermission(this.pfemppostmapService.getPfemppostmapByIds(#ids),'eam-PFEmpPostMap-Remove')")
    @ApiOperation(value = "批量删除人事关系", tags = {"人事关系" },  notes = "批量删除人事关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfemppostmaps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfemppostmapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfemppostmapMapping.toDomain(returnObject.body),'eam-PFEmpPostMap-Get')")
    @ApiOperation(value = "获取人事关系", tags = {"人事关系" },  notes = "获取人事关系")
	@RequestMapping(method = RequestMethod.GET, value = "/pfemppostmaps/{pfemppostmap_id}")
    public ResponseEntity<PFEmpPostMapDTO> get(@PathVariable("pfemppostmap_id") String pfemppostmap_id) {
        PFEmpPostMap domain = pfemppostmapService.get(pfemppostmap_id);
        PFEmpPostMapDTO dto = pfemppostmapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取人事关系草稿", tags = {"人事关系" },  notes = "获取人事关系草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfemppostmaps/getdraft")
    public ResponseEntity<PFEmpPostMapDTO> getDraft(PFEmpPostMapDTO dto) {
        PFEmpPostMap domain = pfemppostmapMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(pfemppostmapMapping.toDto(pfemppostmapService.getDraft(domain)));
    }

    @ApiOperation(value = "检查人事关系", tags = {"人事关系" },  notes = "检查人事关系")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemppostmaps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFEmpPostMapDTO pfemppostmapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfemppostmapService.checkKey(pfemppostmapMapping.toDomain(pfemppostmapdto)));
    }

    @PreAuthorize("hasPermission(this.pfemppostmapMapping.toDomain(#pfemppostmapdto),'eam-PFEmpPostMap-Save')")
    @ApiOperation(value = "保存人事关系", tags = {"人事关系" },  notes = "保存人事关系")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemppostmaps/save")
    public ResponseEntity<PFEmpPostMapDTO> save(@RequestBody PFEmpPostMapDTO pfemppostmapdto) {
        PFEmpPostMap domain = pfemppostmapMapping.toDomain(pfemppostmapdto);
        pfemppostmapService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(pfemppostmapMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.pfemppostmapMapping.toDomain(#pfemppostmapdtos),'eam-PFEmpPostMap-Save')")
    @ApiOperation(value = "批量保存人事关系", tags = {"人事关系" },  notes = "批量保存人事关系")
	@RequestMapping(method = RequestMethod.POST, value = "/pfemppostmaps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFEmpPostMapDTO> pfemppostmapdtos) {
        pfemppostmapService.saveBatch(pfemppostmapMapping.toDomain(pfemppostmapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmpPostMap-searchDefault-all') and hasPermission(#context,'eam-PFEmpPostMap-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"人事关系" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfemppostmaps/fetchdefault")
	public ResponseEntity<List<PFEmpPostMapDTO>> fetchDefault(PFEmpPostMapSearchContext context) {
        Page<PFEmpPostMap> domains = pfemppostmapService.searchDefault(context) ;
        List<PFEmpPostMapDTO> list = pfemppostmapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFEmpPostMap-searchDefault-all') and hasPermission(#context,'eam-PFEmpPostMap-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"人事关系" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfemppostmaps/searchdefault")
	public ResponseEntity<Page<PFEmpPostMapDTO>> searchDefault(@RequestBody PFEmpPostMapSearchContext context) {
        Page<PFEmpPostMap> domains = pfemppostmapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfemppostmapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

