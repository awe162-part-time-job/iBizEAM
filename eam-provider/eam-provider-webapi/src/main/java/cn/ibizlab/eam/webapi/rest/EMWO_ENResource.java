package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_EN;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_ENSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"能耗登记工单" })
@RestController("WebApi-emwo_en")
@RequestMapping("")
public class EMWO_ENResource {

    @Autowired
    public IEMWO_ENService emwo_enService;

    @Autowired
    @Lazy
    public EMWO_ENMapping emwo_enMapping;

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "新建能耗登记工单", tags = {"能耗登记工单" },  notes = "新建能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens")
    public ResponseEntity<EMWO_ENDTO> create(@Validated @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
		emwo_enService.create(domain);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "批量新建能耗登记工单", tags = {"能耗登记工单" },  notes = "批量新建能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWO_ENDTO> emwo_endtos) {
        emwo_enService.createBatch(emwo_enMapping.toDomain(emwo_endtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_en" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "更新能耗登记工单", tags = {"能耗登记工单" },  notes = "更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> update(@PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
		EMWO_EN domain  = emwo_enMapping.toDomain(emwo_endto);
        domain .setEmwoEnid(emwo_en_id);
		emwo_enService.update(domain );
		EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByEntities(this.emwo_enMapping.toDomain(#emwo_endtos)),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "批量更新能耗登记工单", tags = {"能耗登记工单" },  notes = "批量更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_ens/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWO_ENDTO> emwo_endtos) {
        emwo_enService.updateBatch(emwo_enMapping.toDomain(emwo_endtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "删除能耗登记工单", tags = {"能耗登记工单" },  notes = "删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_ens/{emwo_en_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwo_en_id") String emwo_en_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwo_enService.remove(emwo_en_id));
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByIds(#ids),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "批量删除能耗登记工单", tags = {"能耗登记工单" },  notes = "批量删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwo_ens/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwo_enService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_enMapping.toDomain(returnObject.body),'eam-EMWO_EN-Get')")
    @ApiOperation(value = "获取能耗登记工单", tags = {"能耗登记工单" },  notes = "获取能耗登记工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> get(@PathVariable("emwo_en_id") String emwo_en_id) {
        EMWO_EN domain = emwo_enService.get(emwo_en_id);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取能耗登记工单草稿", tags = {"能耗登记工单" },  notes = "获取能耗登记工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwo_ens/getdraft")
    public ResponseEntity<EMWO_ENDTO> getDraft(EMWO_ENDTO dto) {
        EMWO_EN domain = emwo_enMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(emwo_enService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Acceptance-all')")
    @ApiOperation(value = "验收通过", tags = {"能耗登记工单" },  notes = "验收通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/{emwo_en_id}/acceptance")
    public ResponseEntity<EMWO_ENDTO> acceptance(@PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEmwoEnid(emwo_en_id);
        domain = emwo_enService.acceptance(domain);
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Acceptance-all')")
    @ApiOperation(value = "批量处理[验收通过]", tags = {"能耗登记工单" },  notes = "批量处理[验收通过]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceBatch(@RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domains = emwo_enMapping.toDomain(emwo_endtos);
        boolean result = emwo_enService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @ApiOperation(value = "检查能耗登记工单", tags = {"能耗登记工单" },  notes = "检查能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWO_ENDTO emwo_endto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_enService.checkKey(emwo_enMapping.toDomain(emwo_endto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "表单项更新-emequipid", tags = {"能耗登记工单" },  notes = "表单项更新-emequipid")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwo_ens/{emwo_en_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_ENDTO> formUpdateByEmequipid(@PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEmwoEnid(emwo_en_id);
        domain = emwo_enService.formUpdateByEmequipid(domain);
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "保存能耗登记工单", tags = {"能耗登记工单" },  notes = "保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/save")
    public ResponseEntity<EMWO_ENDTO> save(@RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        emwo_enService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "批量保存能耗登记工单", tags = {"能耗登记工单" },  notes = "批量保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWO_ENDTO> emwo_endtos) {
        emwo_enService.saveBatch(emwo_enMapping.toDomain(emwo_endtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Submit-all')")
    @ApiOperation(value = "提交", tags = {"能耗登记工单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/{emwo_en_id}/submit")
    public ResponseEntity<EMWO_ENDTO> submit(@PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEmwoEnid(emwo_en_id);
        domain = emwo_enService.submit(domain);
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-UnAcceptance-all')")
    @ApiOperation(value = "验收不通过", tags = {"能耗登记工单" },  notes = "验收不通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emwo_ens/{emwo_en_id}/unacceptance")
    public ResponseEntity<EMWO_ENDTO> unAcceptance(@PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEmwoEnid(emwo_en_id);
        domain = emwo_enService.unAcceptance(domain);
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "获取日历", tags = {"能耗登记工单" } ,notes = "获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_ens/fetchcalendar")
	public ResponseEntity<List<EMWO_ENDTO>> fetchCalendar(EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "查询日历", tags = {"能耗登记工单" } ,notes = "查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_ens/searchcalendar")
	public ResponseEntity<Page<EMWO_ENDTO>> searchCalendar(@RequestBody EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "获取已完成", tags = {"能耗登记工单" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_ens/fetchconfirmed")
	public ResponseEntity<List<EMWO_ENDTO>> fetchConfirmed(EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "查询已完成", tags = {"能耗登记工单" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_ens/searchconfirmed")
	public ResponseEntity<Page<EMWO_ENDTO>> searchConfirmed(@RequestBody EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"能耗登记工单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_ens/fetchdefault")
	public ResponseEntity<List<EMWO_ENDTO>> fetchDefault(EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"能耗登记工单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_ens/searchdefault")
	public ResponseEntity<Page<EMWO_ENDTO>> searchDefault(@RequestBody EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "获取未提交", tags = {"能耗登记工单" } ,notes = "获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_ens/fetchdraft")
	public ResponseEntity<List<EMWO_ENDTO>> fetchDraft(EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "查询未提交", tags = {"能耗登记工单" } ,notes = "查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_ens/searchdraft")
	public ResponseEntity<Page<EMWO_ENDTO>> searchDraft(@RequestBody EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "获取执行中", tags = {"能耗登记工单" } ,notes = "获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/emwo_ens/fetchtoconfirm")
	public ResponseEntity<List<EMWO_ENDTO>> fetchToConfirm(EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "查询执行中", tags = {"能耗登记工单" } ,notes = "查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/emwo_ens/searchtoconfirm")
	public ResponseEntity<Page<EMWO_ENDTO>> searchToConfirm(@RequestBody EMWO_ENSearchContext context) {
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "根据设备档案建立能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案建立能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens")
    public ResponseEntity<EMWO_ENDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
		emwo_enService.create(domain);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "根据设备档案批量建立能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案批量建立能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_enService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_en" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "根据设备档案更新能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain.setEmwoEnid(emwo_en_id);
		emwo_enService.update(domain);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByEntities(this.emwo_enMapping.toDomain(#emwo_endtos)),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "根据设备档案批量更新能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案批量更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_enService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "根据设备档案删除能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_enService.remove(emwo_en_id));
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByIds(#ids),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "根据设备档案批量删除能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案批量删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emwo_enService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_enMapping.toDomain(returnObject.body),'eam-EMWO_EN-Get')")
    @ApiOperation(value = "根据设备档案获取能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案获取能耗登记工单")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id) {
        EMWO_EN domain = emwo_enService.get(emwo_en_id);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取能耗登记工单草稿", tags = {"能耗登记工单" },  notes = "根据设备档案获取能耗登记工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emwo_ens/getdraft")
    public ResponseEntity<EMWO_ENDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMWO_ENDTO dto) {
        EMWO_EN domain = emwo_enMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(emwo_enService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Acceptance-all')")
    @ApiOperation(value = "根据设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/acceptance")
    public ResponseEntity<EMWO_ENDTO> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.acceptance(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @ApiOperation(value = "批量处理[根据设备档案能耗登记工单]", tags = {"能耗登记工单" },  notes = "批量处理[根据设备档案能耗登记工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domains = emwo_enMapping.toDomain(emwo_endtos);
        boolean result = emwo_enService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据设备档案检查能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案检查能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_enService.checkKey(emwo_enMapping.toDomain(emwo_endto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "根据设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_ENDTO> formUpdateByEmequipidByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.formUpdateByEmequipid(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "根据设备档案保存能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/save")
    public ResponseEntity<EMWO_ENDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        emwo_enService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "根据设备档案批量保存能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案批量保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_enService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Submit-all')")
    @ApiOperation(value = "根据设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/submit")
    public ResponseEntity<EMWO_ENDTO> submitByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.submit(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-UnAcceptance-all')")
    @ApiOperation(value = "根据设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/unacceptance")
    public ResponseEntity<EMWO_ENDTO> unAcceptanceByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.unAcceptance(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案获取日历", tags = {"能耗登记工单" } ,notes = "根据设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_ens/fetchcalendar")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案查询日历", tags = {"能耗登记工单" } ,notes = "根据设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_ens/searchcalendar")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案获取已完成", tags = {"能耗登记工单" } ,notes = "根据设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_ens/fetchconfirmed")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案查询已完成", tags = {"能耗登记工单" } ,notes = "根据设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_ens/searchconfirmed")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"能耗登记工单" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_ens/fetchdefault")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"能耗登记工单" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_ens/searchdefault")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案获取未提交", tags = {"能耗登记工单" } ,notes = "根据设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_ens/fetchdraft")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENDraftByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案查询未提交", tags = {"能耗登记工单" } ,notes = "根据设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_ens/searchdraft")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案获取执行中", tags = {"能耗登记工单" } ,notes = "根据设备档案获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emwo_ens/fetchtoconfirm")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据设备档案查询执行中", tags = {"能耗登记工单" } ,notes = "根据设备档案查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emwo_ens/searchtoconfirm")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "根据班组设备档案建立能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案建立能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens")
    public ResponseEntity<EMWO_ENDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
		emwo_enService.create(domain);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案批量建立能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_enService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwo_en" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "根据班组设备档案更新能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain.setEmwoEnid(emwo_en_id);
		emwo_enService.update(domain);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByEntities(this.emwo_enMapping.toDomain(#emwo_endtos)),'eam-EMWO_EN-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案批量更新能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emwo_enService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwo_enService.get(#emwo_en_id),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "根据班组设备档案删除能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwo_enService.remove(emwo_en_id));
    }

    @PreAuthorize("hasPermission(this.emwo_enService.getEmwoEnByIds(#ids),'eam-EMWO_EN-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案批量删除能耗登记工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emwo_enService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwo_enMapping.toDomain(returnObject.body),'eam-EMWO_EN-Get')")
    @ApiOperation(value = "根据班组设备档案获取能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案获取能耗登记工单")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}")
    public ResponseEntity<EMWO_ENDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id) {
        EMWO_EN domain = emwo_enService.get(emwo_en_id);
        EMWO_ENDTO dto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取能耗登记工单草稿", tags = {"能耗登记工单" },  notes = "根据班组设备档案获取能耗登记工单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/getdraft")
    public ResponseEntity<EMWO_ENDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMWO_ENDTO dto) {
        EMWO_EN domain = emwo_enMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(emwo_enService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Acceptance-all')")
    @ApiOperation(value = "根据班组设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/acceptance")
    public ResponseEntity<EMWO_ENDTO> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.acceptance(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @ApiOperation(value = "批量处理[根据班组设备档案能耗登记工单]", tags = {"能耗登记工单" },  notes = "批量处理[根据班组设备档案能耗登记工单]")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/acceptancebatch")
    public ResponseEntity<Boolean> acceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domains = emwo_enMapping.toDomain(emwo_endtos);
        boolean result = emwo_enService.acceptanceBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @ApiOperation(value = "根据班组设备档案检查能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案检查能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwo_enService.checkKey(emwo_enMapping.toDomain(emwo_endto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-FormUpdateByEmequipid-all')")
    @ApiOperation(value = "根据班组设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/formupdatebyemequipid")
    public ResponseEntity<EMWO_ENDTO> formUpdateByEmequipidByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.formUpdateByEmequipid(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endto),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "根据班组设备档案保存能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/save")
    public ResponseEntity<EMWO_ENDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        emwo_enService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_enMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwo_enMapping.toDomain(#emwo_endtos),'eam-EMWO_EN-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案批量保存能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMWO_ENDTO> emwo_endtos) {
        List<EMWO_EN> domainlist=emwo_enMapping.toDomain(emwo_endtos);
        for(EMWO_EN domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emwo_enService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-Submit-all')")
    @ApiOperation(value = "根据班组设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/submit")
    public ResponseEntity<EMWO_ENDTO> submitByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.submit(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-UnAcceptance-all')")
    @ApiOperation(value = "根据班组设备档案能耗登记工单", tags = {"能耗登记工单" },  notes = "根据班组设备档案能耗登记工单")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/{emwo_en_id}/unacceptance")
    public ResponseEntity<EMWO_ENDTO> unAcceptanceByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emwo_en_id") String emwo_en_id, @RequestBody EMWO_ENDTO emwo_endto) {
        EMWO_EN domain = emwo_enMapping.toDomain(emwo_endto);
        domain.setEquipid(emequip_id);
        domain = emwo_enService.unAcceptance(domain) ;
        emwo_endto = emwo_enMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwo_endto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历", tags = {"能耗登记工单" } ,notes = "根据班组设备档案获取日历")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/fetchcalendar")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchCalendar-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历", tags = {"能耗登记工单" } ,notes = "根据班组设备档案查询日历")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/searchcalendar")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案获取已完成", tags = {"能耗登记工单" } ,notes = "根据班组设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/fetchconfirmed")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchConfirmed-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案查询已完成", tags = {"能耗登记工单" } ,notes = "根据班组设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/searchconfirmed")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"能耗登记工单" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/fetchdefault")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDefault-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"能耗登记工单" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/searchdefault")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案获取未提交", tags = {"能耗登记工单" } ,notes = "根据班组设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/fetchdraft")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchDraft-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案查询未提交", tags = {"能耗登记工单" } ,notes = "根据班组设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/searchdraft")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案获取执行中", tags = {"能耗登记工单" } ,notes = "根据班组设备档案获取执行中")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/fetchtoconfirm")
	public ResponseEntity<List<EMWO_ENDTO>> fetchEMWO_ENToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
        List<EMWO_ENDTO> list = emwo_enMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWO_EN-searchToConfirm-all') and hasPermission(#context,'eam-EMWO_EN-Get')")
	@ApiOperation(value = "根据班组设备档案查询执行中", tags = {"能耗登记工单" } ,notes = "根据班组设备档案查询执行中")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emwo_ens/searchtoconfirm")
	public ResponseEntity<Page<EMWO_ENDTO>> searchEMWO_ENToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMWO_ENSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMWO_EN> domains = emwo_enService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwo_enMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

