package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEITool;
import cn.ibizlab.eam.core.eam_core.service.IEMEIToolService;
import cn.ibizlab.eam.core.eam_core.filter.EMEIToolSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工量具" })
@RestController("WebApi-emeitool")
@RequestMapping("")
public class EMEIToolResource {

    @Autowired
    public IEMEIToolService emeitoolService;

    @Autowired
    @Lazy
    public EMEIToolMapping emeitoolMapping;

    @PreAuthorize("hasPermission(this.emeitoolMapping.toDomain(#emeitooldto),'eam-EMEITool-Create')")
    @ApiOperation(value = "新建工量具", tags = {"工量具" },  notes = "新建工量具")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitools")
    public ResponseEntity<EMEIToolDTO> create(@Validated @RequestBody EMEIToolDTO emeitooldto) {
        EMEITool domain = emeitoolMapping.toDomain(emeitooldto);
		emeitoolService.create(domain);
        EMEIToolDTO dto = emeitoolMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeitoolMapping.toDomain(#emeitooldtos),'eam-EMEITool-Create')")
    @ApiOperation(value = "批量新建工量具", tags = {"工量具" },  notes = "批量新建工量具")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitools/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEIToolDTO> emeitooldtos) {
        emeitoolService.createBatch(emeitoolMapping.toDomain(emeitooldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeitool" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeitoolService.get(#emeitool_id),'eam-EMEITool-Update')")
    @ApiOperation(value = "更新工量具", tags = {"工量具" },  notes = "更新工量具")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeitools/{emeitool_id}")
    public ResponseEntity<EMEIToolDTO> update(@PathVariable("emeitool_id") String emeitool_id, @RequestBody EMEIToolDTO emeitooldto) {
		EMEITool domain  = emeitoolMapping.toDomain(emeitooldto);
        domain .setEmeitoolid(emeitool_id);
		emeitoolService.update(domain );
		EMEIToolDTO dto = emeitoolMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeitoolService.getEmeitoolByEntities(this.emeitoolMapping.toDomain(#emeitooldtos)),'eam-EMEITool-Update')")
    @ApiOperation(value = "批量更新工量具", tags = {"工量具" },  notes = "批量更新工量具")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeitools/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEIToolDTO> emeitooldtos) {
        emeitoolService.updateBatch(emeitoolMapping.toDomain(emeitooldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeitoolService.get(#emeitool_id),'eam-EMEITool-Remove')")
    @ApiOperation(value = "删除工量具", tags = {"工量具" },  notes = "删除工量具")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeitools/{emeitool_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeitool_id") String emeitool_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeitoolService.remove(emeitool_id));
    }

    @PreAuthorize("hasPermission(this.emeitoolService.getEmeitoolByIds(#ids),'eam-EMEITool-Remove')")
    @ApiOperation(value = "批量删除工量具", tags = {"工量具" },  notes = "批量删除工量具")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeitools/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeitoolService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeitoolMapping.toDomain(returnObject.body),'eam-EMEITool-Get')")
    @ApiOperation(value = "获取工量具", tags = {"工量具" },  notes = "获取工量具")
	@RequestMapping(method = RequestMethod.GET, value = "/emeitools/{emeitool_id}")
    public ResponseEntity<EMEIToolDTO> get(@PathVariable("emeitool_id") String emeitool_id) {
        EMEITool domain = emeitoolService.get(emeitool_id);
        EMEIToolDTO dto = emeitoolMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工量具草稿", tags = {"工量具" },  notes = "获取工量具草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeitools/getdraft")
    public ResponseEntity<EMEIToolDTO> getDraft(EMEIToolDTO dto) {
        EMEITool domain = emeitoolMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeitoolMapping.toDto(emeitoolService.getDraft(domain)));
    }

    @ApiOperation(value = "检查工量具", tags = {"工量具" },  notes = "检查工量具")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitools/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEIToolDTO emeitooldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeitoolService.checkKey(emeitoolMapping.toDomain(emeitooldto)));
    }

    @PreAuthorize("hasPermission(this.emeitoolMapping.toDomain(#emeitooldto),'eam-EMEITool-Save')")
    @ApiOperation(value = "保存工量具", tags = {"工量具" },  notes = "保存工量具")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitools/save")
    public ResponseEntity<EMEIToolDTO> save(@RequestBody EMEIToolDTO emeitooldto) {
        EMEITool domain = emeitoolMapping.toDomain(emeitooldto);
        emeitoolService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeitoolMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeitoolMapping.toDomain(#emeitooldtos),'eam-EMEITool-Save')")
    @ApiOperation(value = "批量保存工量具", tags = {"工量具" },  notes = "批量保存工量具")
	@RequestMapping(method = RequestMethod.POST, value = "/emeitools/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEIToolDTO> emeitooldtos) {
        emeitoolService.saveBatch(emeitoolMapping.toDomain(emeitooldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEITool-searchDefault-all') and hasPermission(#context,'eam-EMEITool-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"工量具" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeitools/fetchdefault")
	public ResponseEntity<List<EMEIToolDTO>> fetchDefault(EMEIToolSearchContext context) {
        Page<EMEITool> domains = emeitoolService.searchDefault(context) ;
        List<EMEIToolDTO> list = emeitoolMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEITool-searchDefault-all') and hasPermission(#context,'eam-EMEITool-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"工量具" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeitools/searchdefault")
	public ResponseEntity<Page<EMEIToolDTO>> searchDefault(@RequestBody EMEIToolSearchContext context) {
        Page<EMEITool> domains = emeitoolService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeitoolMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

