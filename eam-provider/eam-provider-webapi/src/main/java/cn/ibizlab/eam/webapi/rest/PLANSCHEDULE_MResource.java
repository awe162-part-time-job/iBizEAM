package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_M;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_MService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_MSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划_按月" })
@RestController("WebApi-planschedule_m")
@RequestMapping("")
public class PLANSCHEDULE_MResource {

    @Autowired
    public IPLANSCHEDULE_MService planschedule_mService;

    @Autowired
    @Lazy
    public PLANSCHEDULE_MMapping planschedule_mMapping;

    @PreAuthorize("hasPermission(this.planschedule_mMapping.toDomain(#planschedule_mdto),'eam-PLANSCHEDULE_M-Create')")
    @ApiOperation(value = "新建计划_按月", tags = {"计划_按月" },  notes = "新建计划_按月")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ms")
    public ResponseEntity<PLANSCHEDULE_MDTO> create(@Validated @RequestBody PLANSCHEDULE_MDTO planschedule_mdto) {
        PLANSCHEDULE_M domain = planschedule_mMapping.toDomain(planschedule_mdto);
		planschedule_mService.create(domain);
        PLANSCHEDULE_MDTO dto = planschedule_mMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_mMapping.toDomain(#planschedule_mdtos),'eam-PLANSCHEDULE_M-Create')")
    @ApiOperation(value = "批量新建计划_按月", tags = {"计划_按月" },  notes = "批量新建计划_按月")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULE_MDTO> planschedule_mdtos) {
        planschedule_mService.createBatch(planschedule_mMapping.toDomain(planschedule_mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule_m" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planschedule_mService.get(#planschedule_m_id),'eam-PLANSCHEDULE_M-Update')")
    @ApiOperation(value = "更新计划_按月", tags = {"计划_按月" },  notes = "更新计划_按月")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ms/{planschedule_m_id}")
    public ResponseEntity<PLANSCHEDULE_MDTO> update(@PathVariable("planschedule_m_id") String planschedule_m_id, @RequestBody PLANSCHEDULE_MDTO planschedule_mdto) {
		PLANSCHEDULE_M domain  = planschedule_mMapping.toDomain(planschedule_mdto);
        domain .setPlanscheduleMid(planschedule_m_id);
		planschedule_mService.update(domain );
		PLANSCHEDULE_MDTO dto = planschedule_mMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_mService.getPlanscheduleMByEntities(this.planschedule_mMapping.toDomain(#planschedule_mdtos)),'eam-PLANSCHEDULE_M-Update')")
    @ApiOperation(value = "批量更新计划_按月", tags = {"计划_按月" },  notes = "批量更新计划_按月")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULE_MDTO> planschedule_mdtos) {
        planschedule_mService.updateBatch(planschedule_mMapping.toDomain(planschedule_mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planschedule_mService.get(#planschedule_m_id),'eam-PLANSCHEDULE_M-Remove')")
    @ApiOperation(value = "删除计划_按月", tags = {"计划_按月" },  notes = "删除计划_按月")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ms/{planschedule_m_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_m_id") String planschedule_m_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planschedule_mService.remove(planschedule_m_id));
    }

    @PreAuthorize("hasPermission(this.planschedule_mService.getPlanscheduleMByIds(#ids),'eam-PLANSCHEDULE_M-Remove')")
    @ApiOperation(value = "批量删除计划_按月", tags = {"计划_按月" },  notes = "批量删除计划_按月")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planschedule_mService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planschedule_mMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE_M-Get')")
    @ApiOperation(value = "获取计划_按月", tags = {"计划_按月" },  notes = "获取计划_按月")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ms/{planschedule_m_id}")
    public ResponseEntity<PLANSCHEDULE_MDTO> get(@PathVariable("planschedule_m_id") String planschedule_m_id) {
        PLANSCHEDULE_M domain = planschedule_mService.get(planschedule_m_id);
        PLANSCHEDULE_MDTO dto = planschedule_mMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划_按月草稿", tags = {"计划_按月" },  notes = "获取计划_按月草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ms/getdraft")
    public ResponseEntity<PLANSCHEDULE_MDTO> getDraft(PLANSCHEDULE_MDTO dto) {
        PLANSCHEDULE_M domain = planschedule_mMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_mMapping.toDto(planschedule_mService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划_按月", tags = {"计划_按月" },  notes = "检查计划_按月")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULE_MDTO planschedule_mdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planschedule_mService.checkKey(planschedule_mMapping.toDomain(planschedule_mdto)));
    }

    @PreAuthorize("hasPermission(this.planschedule_mMapping.toDomain(#planschedule_mdto),'eam-PLANSCHEDULE_M-Save')")
    @ApiOperation(value = "保存计划_按月", tags = {"计划_按月" },  notes = "保存计划_按月")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ms/save")
    public ResponseEntity<PLANSCHEDULE_MDTO> save(@RequestBody PLANSCHEDULE_MDTO planschedule_mdto) {
        PLANSCHEDULE_M domain = planschedule_mMapping.toDomain(planschedule_mdto);
        planschedule_mService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_mMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planschedule_mMapping.toDomain(#planschedule_mdtos),'eam-PLANSCHEDULE_M-Save')")
    @ApiOperation(value = "批量保存计划_按月", tags = {"计划_按月" },  notes = "批量保存计划_按月")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULE_MDTO> planschedule_mdtos) {
        planschedule_mService.saveBatch(planschedule_mMapping.toDomain(planschedule_mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_M-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_M-Get')")
	@ApiOperation(value = "获取数据集", tags = {"计划_按月" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedule_ms/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULE_MDTO>> fetchDefault(PLANSCHEDULE_MSearchContext context) {
        Page<PLANSCHEDULE_M> domains = planschedule_mService.searchDefault(context) ;
        List<PLANSCHEDULE_MDTO> list = planschedule_mMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_M-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_M-Get')")
	@ApiOperation(value = "查询数据集", tags = {"计划_按月" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedule_ms/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULE_MDTO>> searchDefault(@RequestBody PLANSCHEDULE_MSearchContext context) {
        Page<PLANSCHEDULE_M> domains = planschedule_mService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planschedule_mMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

