package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMProjectTrack;
import cn.ibizlab.eam.core.eam_core.service.IEMProjectTrackService;
import cn.ibizlab.eam.core.eam_core.filter.EMProjectTrackSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划修理项目跟踪表" })
@RestController("WebApi-emprojecttrack")
@RequestMapping("")
public class EMProjectTrackResource {

    @Autowired
    public IEMProjectTrackService emprojecttrackService;

    @Autowired
    @Lazy
    public EMProjectTrackMapping emprojecttrackMapping;

    @PreAuthorize("hasPermission(this.emprojecttrackMapping.toDomain(#emprojecttrackdto),'eam-EMProjectTrack-Create')")
    @ApiOperation(value = "新建计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "新建计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emprojecttracks")
    public ResponseEntity<EMProjectTrackDTO> create(@Validated @RequestBody EMProjectTrackDTO emprojecttrackdto) {
        EMProjectTrack domain = emprojecttrackMapping.toDomain(emprojecttrackdto);
		emprojecttrackService.create(domain);
        EMProjectTrackDTO dto = emprojecttrackMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emprojecttrackMapping.toDomain(#emprojecttrackdtos),'eam-EMProjectTrack-Create')")
    @ApiOperation(value = "批量新建计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "批量新建计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emprojecttracks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMProjectTrackDTO> emprojecttrackdtos) {
        emprojecttrackService.createBatch(emprojecttrackMapping.toDomain(emprojecttrackdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emprojecttrack" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emprojecttrackService.get(#emprojecttrack_id),'eam-EMProjectTrack-Update')")
    @ApiOperation(value = "更新计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "更新计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emprojecttracks/{emprojecttrack_id}")
    public ResponseEntity<EMProjectTrackDTO> update(@PathVariable("emprojecttrack_id") String emprojecttrack_id, @RequestBody EMProjectTrackDTO emprojecttrackdto) {
		EMProjectTrack domain  = emprojecttrackMapping.toDomain(emprojecttrackdto);
        domain .setEmprojecttrackid(emprojecttrack_id);
		emprojecttrackService.update(domain );
		EMProjectTrackDTO dto = emprojecttrackMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emprojecttrackService.getEmprojecttrackByEntities(this.emprojecttrackMapping.toDomain(#emprojecttrackdtos)),'eam-EMProjectTrack-Update')")
    @ApiOperation(value = "批量更新计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "批量更新计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.PUT, value = "/emprojecttracks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMProjectTrackDTO> emprojecttrackdtos) {
        emprojecttrackService.updateBatch(emprojecttrackMapping.toDomain(emprojecttrackdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emprojecttrackService.get(#emprojecttrack_id),'eam-EMProjectTrack-Remove')")
    @ApiOperation(value = "删除计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "删除计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emprojecttracks/{emprojecttrack_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emprojecttrack_id") String emprojecttrack_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emprojecttrackService.remove(emprojecttrack_id));
    }

    @PreAuthorize("hasPermission(this.emprojecttrackService.getEmprojecttrackByIds(#ids),'eam-EMProjectTrack-Remove')")
    @ApiOperation(value = "批量删除计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "批量删除计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emprojecttracks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emprojecttrackService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emprojecttrackMapping.toDomain(returnObject.body),'eam-EMProjectTrack-Get')")
    @ApiOperation(value = "获取计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "获取计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.GET, value = "/emprojecttracks/{emprojecttrack_id}")
    public ResponseEntity<EMProjectTrackDTO> get(@PathVariable("emprojecttrack_id") String emprojecttrack_id) {
        EMProjectTrack domain = emprojecttrackService.get(emprojecttrack_id);
        EMProjectTrackDTO dto = emprojecttrackMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划修理项目跟踪表草稿", tags = {"计划修理项目跟踪表" },  notes = "获取计划修理项目跟踪表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emprojecttracks/getdraft")
    public ResponseEntity<EMProjectTrackDTO> getDraft(EMProjectTrackDTO dto) {
        EMProjectTrack domain = emprojecttrackMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emprojecttrackMapping.toDto(emprojecttrackService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "检查计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emprojecttracks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMProjectTrackDTO emprojecttrackdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emprojecttrackService.checkKey(emprojecttrackMapping.toDomain(emprojecttrackdto)));
    }

    @PreAuthorize("hasPermission(this.emprojecttrackMapping.toDomain(#emprojecttrackdto),'eam-EMProjectTrack-Save')")
    @ApiOperation(value = "保存计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "保存计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emprojecttracks/save")
    public ResponseEntity<EMProjectTrackDTO> save(@RequestBody EMProjectTrackDTO emprojecttrackdto) {
        EMProjectTrack domain = emprojecttrackMapping.toDomain(emprojecttrackdto);
        emprojecttrackService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emprojecttrackMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emprojecttrackMapping.toDomain(#emprojecttrackdtos),'eam-EMProjectTrack-Save')")
    @ApiOperation(value = "批量保存计划修理项目跟踪表", tags = {"计划修理项目跟踪表" },  notes = "批量保存计划修理项目跟踪表")
	@RequestMapping(method = RequestMethod.POST, value = "/emprojecttracks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMProjectTrackDTO> emprojecttrackdtos) {
        emprojecttrackService.saveBatch(emprojecttrackMapping.toDomain(emprojecttrackdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMProjectTrack-searchDefault-all') and hasPermission(#context,'eam-EMProjectTrack-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划修理项目跟踪表" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emprojecttracks/fetchdefault")
	public ResponseEntity<List<EMProjectTrackDTO>> fetchDefault(EMProjectTrackSearchContext context) {
        Page<EMProjectTrack> domains = emprojecttrackService.searchDefault(context) ;
        List<EMProjectTrackDTO> list = emprojecttrackMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMProjectTrack-searchDefault-all') and hasPermission(#context,'eam-EMProjectTrack-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划修理项目跟踪表" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emprojecttracks/searchdefault")
	public ResponseEntity<Page<EMProjectTrackDTO>> searchDefault(@RequestBody EMProjectTrackSearchContext context) {
        Page<EMProjectTrack> domains = emprojecttrackService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emprojecttrackMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

