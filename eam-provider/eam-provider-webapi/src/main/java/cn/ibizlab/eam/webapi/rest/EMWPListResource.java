package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMWPList;
import cn.ibizlab.eam.core.eam_core.service.IEMWPListService;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购申请" })
@RestController("WebApi-emwplist")
@RequestMapping("")
public class EMWPListResource {

    @Autowired
    public IEMWPListService emwplistService;

    @Autowired
    @Lazy
    public EMWPListMapping emwplistMapping;

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "新建采购申请", tags = {"采购申请" },  notes = "新建采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists")
    public ResponseEntity<EMWPListDTO> create(@Validated @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "批量新建采购申请", tags = {"采购申请" },  notes = "批量新建采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        emwplistService.createBatch(emwplistMapping.toDomain(emwplistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "更新采购申请", tags = {"采购申请" },  notes = "更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> update(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
		EMWPList domain  = emwplistMapping.toDomain(emwplistdto);
        domain .setEmwplistid(emwplist_id);
		emwplistService.update(domain );
		EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "批量更新采购申请", tags = {"采购申请" },  notes = "批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        emwplistService.updateBatch(emwplistMapping.toDomain(emwplistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "删除采购申请", tags = {"采购申请" },  notes = "删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emwplist_id") String emwplist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "批量删除采购申请", tags = {"采购申请" },  notes = "批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emwplists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "获取采购申请", tags = {"采购申请" },  notes = "获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> get(@PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购申请草稿", tags = {"采购申请" },  notes = "获取采购申请草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraft(EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "检查采购申请", tags = {"采购申请" },  notes = "检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "确定询价", tags = {"采购申请" },  notes = "确定询价")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirm(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.confirm(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "批量处理[确定询价]", tags = {"采购申请" },  notes = "批量处理[确定询价]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "询价填报完成", tags = {"采购申请" },  notes = "询价填报完成")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCosted(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.fillCosted(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "批量处理[询价填报完成]", tags = {"采购申请" },  notes = "批量处理[询价填报完成]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "表单更新-aempid", tags = {"采购申请" },  notes = "表单更新-aempid")
	@RequestMapping(method = RequestMethod.PUT, value = "/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempid(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.formUpdateByAempid(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "生成订单", tags = {"采购申请" },  notes = "生成订单")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPO(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.genPO(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "批量处理[生成订单]", tags = {"采购申请" },  notes = "批量处理[生成订单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据物品获取采购员", tags = {"采购申请" },  notes = "根据物品获取采购员")
	@RequestMapping(method = RequestMethod.GET, value = "/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMP(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.getREMP(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "保存采购申请", tags = {"采购申请" },  notes = "保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/save")
    public ResponseEntity<EMWPListDTO> save(@RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "批量保存采购申请", tags = {"采购申请" },  notes = "批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMWPListDTO> emwplistdtos) {
        emwplistService.saveBatch(emwplistMapping.toDomain(emwplistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "提交", tags = {"采购申请" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submit(@PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setEmwplistid(emwplist_id);
        domain = emwplistService.submit(domain);
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取已取消申请", tags = {"采购申请" } ,notes = "获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchCancel(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询已取消申请", tags = {"采购申请" } ,notes = "查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchCancel(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取待确认询价", tags = {"采购申请" } ,notes = "获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchConfimCost(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询待确认询价", tags = {"采购申请" } ,notes = "查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchConfimCost(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"采购申请" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchDefault(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"采购申请" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchDefault(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取草稿", tags = {"采购申请" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchDraft(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询草稿", tags = {"采购申请" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchDraft(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取已入库申请", tags = {"采购申请" } ,notes = "获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchIn(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询已入库申请", tags = {"采购申请" } ,notes = "查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchIn(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取已生成订单采购申请", tags = {"采购申请" } ,notes = "获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchMain6(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询已生成订单采购申请", tags = {"采购申请" } ,notes = "查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchMain6(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取已到货采购申请", tags = {"采购申请" } ,notes = "获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchMain6_8692(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询已到货采购申请", tags = {"采购申请" } ,notes = "查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchMain6_8692(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取待询价", tags = {"采购申请" } ,notes = "获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchWaitCost(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询待询价", tags = {"采购申请" } ,notes = "查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchWaitCost(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "获取待生成订单", tags = {"采购申请" } ,notes = "获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchWaitPo(EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "查询待生成订单", tags = {"采购申请" } ,notes = "查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchWaitPo(@RequestBody EMWPListSearchContext context) {
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取采购情况统计", tags = {"采购申请" } ,notes = "获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchWpStateNum(EMWPListSearchContext context) {
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询采购情况统计", tags = {"采购申请" } ,notes = "查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchWpStateNum(@RequestBody EMWPListSearchContext context) {
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据物品建立采购申请", tags = {"采购申请" },  notes = "根据物品建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists")
    public ResponseEntity<EMWPListDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据物品批量建立采购申请", tags = {"采购申请" },  notes = "根据物品批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据物品更新采购申请", tags = {"采购申请" },  notes = "根据物品更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistid(emwplist_id);
		emwplistService.update(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据物品批量更新采购申请", tags = {"采购申请" },  notes = "根据物品批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据物品删除采购申请", tags = {"采购申请" },  notes = "根据物品删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据物品批量删除采购申请", tags = {"采购申请" },  notes = "根据物品批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "根据物品获取采购申请", tags = {"采购申请" },  notes = "根据物品获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取采购申请草稿", tags = {"采购申请" },  notes = "根据物品获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查采购申请", tags = {"采购申请" },  notes = "根据物品检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.confirm(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCostedByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.fillCosted(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempidByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.formUpdateByAempid(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPOByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.genPO(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMPByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.getREMP(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据物品保存采购申请", tags = {"采购申请" },  notes = "根据物品保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/save")
    public ResponseEntity<EMWPListDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据物品批量保存采购申请", tags = {"采购申请" },  notes = "根据物品批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "根据物品采购申请", tags = {"采购申请" },  notes = "根据物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submitByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.submit(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取已取消申请", tags = {"采购申请" } ,notes = "根据物品获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListCancelByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询已取消申请", tags = {"采购申请" } ,notes = "根据物品查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListCancelByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取待确认询价", tags = {"采购申请" } ,notes = "根据物品获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListConfimCostByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询待确认询价", tags = {"采购申请" } ,notes = "根据物品查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListConfimCostByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"采购申请" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"采购申请" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取草稿", tags = {"采购申请" } ,notes = "根据物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDraftByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询草稿", tags = {"采购申请" } ,notes = "根据物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDraftByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取已入库申请", tags = {"采购申请" } ,notes = "根据物品获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListInByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询已入库申请", tags = {"采购申请" } ,notes = "根据物品查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListInByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取已生成订单采购申请", tags = {"采购申请" } ,notes = "根据物品获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6ByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询已生成订单采购申请", tags = {"采购申请" } ,notes = "根据物品查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6ByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取已到货采购申请", tags = {"采购申请" } ,notes = "根据物品获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6_8692ByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询已到货采购申请", tags = {"采购申请" } ,notes = "根据物品查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6_8692ByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取待询价", tags = {"采购申请" } ,notes = "根据物品获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitCostByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询待询价", tags = {"采购申请" } ,notes = "根据物品查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitCostByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品获取待生成订单", tags = {"采购申请" } ,notes = "根据物品获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitPoByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据物品查询待生成订单", tags = {"采购申请" } ,notes = "根据物品查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitPoByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品获取采购情况统计", tags = {"采购申请" } ,notes = "根据物品获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchEMWPListWpStateNumByEMItem(@PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品查询采购情况统计", tags = {"采购申请" } ,notes = "根据物品查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchEMWPListWpStateNumByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据服务商物品建立采购申请", tags = {"采购申请" },  notes = "根据服务商物品建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists")
    public ResponseEntity<EMWPListDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据服务商物品批量建立采购申请", tags = {"采购申请" },  notes = "根据服务商物品批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据服务商物品更新采购申请", tags = {"采购申请" },  notes = "根据服务商物品更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistid(emwplist_id);
		emwplistService.update(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据服务商物品批量更新采购申请", tags = {"采购申请" },  notes = "根据服务商物品批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据服务商物品删除采购申请", tags = {"采购申请" },  notes = "根据服务商物品删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除采购申请", tags = {"采购申请" },  notes = "根据服务商物品批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "根据服务商物品获取采购申请", tags = {"采购申请" },  notes = "根据服务商物品获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取采购申请草稿", tags = {"采购申请" },  notes = "根据服务商物品获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查采购申请", tags = {"采购申请" },  notes = "根据服务商物品检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.confirm(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据服务商物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCostedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.fillCosted(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据服务商物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempidByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.formUpdateByAempid(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPOByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.genPO(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据服务商物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMPByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.getREMP(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据服务商物品保存采购申请", tags = {"采购申请" },  notes = "根据服务商物品保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/save")
    public ResponseEntity<EMWPListDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据服务商物品批量保存采购申请", tags = {"采购申请" },  notes = "根据服务商物品批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "根据服务商物品采购申请", tags = {"采购申请" },  notes = "根据服务商物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submitByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.submit(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取已取消申请", tags = {"采购申请" } ,notes = "根据服务商物品获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListCancelByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询已取消申请", tags = {"采购申请" } ,notes = "根据服务商物品查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListCancelByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取待确认询价", tags = {"采购申请" } ,notes = "根据服务商物品获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListConfimCostByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询待确认询价", tags = {"采购申请" } ,notes = "根据服务商物品查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListConfimCostByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"采购申请" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"采购申请" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取草稿", tags = {"采购申请" } ,notes = "根据服务商物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询草稿", tags = {"采购申请" } ,notes = "根据服务商物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取已入库申请", tags = {"采购申请" } ,notes = "根据服务商物品获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询已入库申请", tags = {"采购申请" } ,notes = "根据服务商物品查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取已生成订单采购申请", tags = {"采购申请" } ,notes = "根据服务商物品获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6ByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询已生成订单采购申请", tags = {"采购申请" } ,notes = "根据服务商物品查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6ByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取已到货采购申请", tags = {"采购申请" } ,notes = "根据服务商物品获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6_8692ByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询已到货采购申请", tags = {"采购申请" } ,notes = "根据服务商物品查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6_8692ByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取待询价", tags = {"采购申请" } ,notes = "根据服务商物品获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitCostByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询待询价", tags = {"采购申请" } ,notes = "根据服务商物品查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitCostByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品获取待生成订单", tags = {"采购申请" } ,notes = "根据服务商物品获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitPoByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据服务商物品查询待生成订单", tags = {"采购申请" } ,notes = "根据服务商物品查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitPoByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品获取采购情况统计", tags = {"采购申请" } ,notes = "根据服务商物品获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchEMWPListWpStateNumByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品查询采购情况统计", tags = {"采购申请" } ,notes = "根据服务商物品查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchEMWPListWpStateNumByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库库位物品建立采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists")
    public ResponseEntity<EMWPListDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库库位物品更新采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistid(emwplist_id);
		emwplistService.update(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "根据仓库库位物品获取采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取采购申请草稿", tags = {"采购申请" },  notes = "根据仓库库位物品获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.confirm(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCostedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.fillCosted(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempidByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.formUpdateByAempid(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPOByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.genPO(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMPByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.getREMP(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库库位物品保存采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/save")
    public ResponseEntity<EMWPListDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "根据仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submitByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.submit(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已取消申请", tags = {"采购申请" } ,notes = "根据仓库库位物品获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListCancelByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已取消申请", tags = {"采购申请" } ,notes = "根据仓库库位物品查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListCancelByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待确认询价", tags = {"采购申请" } ,notes = "根据仓库库位物品获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListConfimCostByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待确认询价", tags = {"采购申请" } ,notes = "根据仓库库位物品查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListConfimCostByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"采购申请" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"采购申请" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取草稿", tags = {"采购申请" } ,notes = "根据仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询草稿", tags = {"采购申请" } ,notes = "根据仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已入库申请", tags = {"采购申请" } ,notes = "根据仓库库位物品获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已入库申请", tags = {"采购申请" } ,notes = "根据仓库库位物品查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库库位物品获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6ByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库库位物品查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6ByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库库位物品获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6_8692ByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库库位物品查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6_8692ByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待询价", tags = {"采购申请" } ,notes = "根据仓库库位物品获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitCostByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待询价", tags = {"采购申请" } ,notes = "根据仓库库位物品查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitCostByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待生成订单", tags = {"采购申请" } ,notes = "根据仓库库位物品获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitPoByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待生成订单", tags = {"采购申请" } ,notes = "根据仓库库位物品查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitPoByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品获取采购情况统计", tags = {"采购申请" } ,notes = "根据仓库库位物品获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchEMWPListWpStateNumByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品查询采购情况统计", tags = {"采购申请" } ,notes = "根据仓库库位物品查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchEMWPListWpStateNumByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库物品建立采购申请", tags = {"采购申请" },  notes = "根据仓库物品建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists")
    public ResponseEntity<EMWPListDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库物品批量建立采购申请", tags = {"采购申请" },  notes = "根据仓库物品批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库物品更新采购申请", tags = {"采购申请" },  notes = "根据仓库物品更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistid(emwplist_id);
		emwplistService.update(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库物品批量更新采购申请", tags = {"采购申请" },  notes = "根据仓库物品批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库物品删除采购申请", tags = {"采购申请" },  notes = "根据仓库物品删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除采购申请", tags = {"采购申请" },  notes = "根据仓库物品批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "根据仓库物品获取采购申请", tags = {"采购申请" },  notes = "根据仓库物品获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取采购申请草稿", tags = {"采购申请" },  notes = "根据仓库物品获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查采购申请", tags = {"采购申请" },  notes = "根据仓库物品检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.confirm(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCostedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.fillCosted(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempidByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.formUpdateByAempid(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPOByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.genPO(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMPByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.getREMP(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库物品保存采购申请", tags = {"采购申请" },  notes = "根据仓库物品保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/save")
    public ResponseEntity<EMWPListDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库物品批量保存采购申请", tags = {"采购申请" },  notes = "根据仓库物品批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "根据仓库物品采购申请", tags = {"采购申请" },  notes = "根据仓库物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submitByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.submit(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取已取消申请", tags = {"采购申请" } ,notes = "根据仓库物品获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListCancelByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询已取消申请", tags = {"采购申请" } ,notes = "根据仓库物品查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListCancelByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取待确认询价", tags = {"采购申请" } ,notes = "根据仓库物品获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListConfimCostByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询待确认询价", tags = {"采购申请" } ,notes = "根据仓库物品查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListConfimCostByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"采购申请" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"采购申请" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取草稿", tags = {"采购申请" } ,notes = "根据仓库物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询草稿", tags = {"采购申请" } ,notes = "根据仓库物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取已入库申请", tags = {"采购申请" } ,notes = "根据仓库物品获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询已入库申请", tags = {"采购申请" } ,notes = "根据仓库物品查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库物品获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6ByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库物品查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6ByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库物品获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6_8692ByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库物品查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6_8692ByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取待询价", tags = {"采购申请" } ,notes = "根据仓库物品获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitCostByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询待询价", tags = {"采购申请" } ,notes = "根据仓库物品查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitCostByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品获取待生成订单", tags = {"采购申请" } ,notes = "根据仓库物品获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitPoByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库物品查询待生成订单", tags = {"采购申请" } ,notes = "根据仓库物品查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitPoByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品获取采购情况统计", tags = {"采购申请" } ,notes = "根据仓库物品获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchEMWPListWpStateNumByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品查询采购情况统计", tags = {"采购申请" } ,notes = "根据仓库物品查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchEMWPListWpStateNumByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists")
    public ResponseEntity<EMWPListDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
		emwplistService.create(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emwplist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain.setEmwplistid(emwplist_id);
		emwplistService.update(domain);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByEntities(this.emwplistMapping.toDomain(#emwplistdtos)),'eam-EMWPList-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emwplistService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emwplistService.get(#emwplist_id),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emwplistService.remove(emwplist_id));
    }

    @PreAuthorize("hasPermission(this.emwplistService.getEmwplistByIds(#ids),'eam-EMWPList-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emwplistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emwplistMapping.toDomain(returnObject.body),'eam-EMWPList-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}")
    public ResponseEntity<EMWPListDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id) {
        EMWPList domain = emwplistService.get(emwplist_id);
        EMWPListDTO dto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取采购申请草稿", tags = {"采购申请" },  notes = "根据仓库仓库库位物品获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/getdraft")
    public ResponseEntity<EMWPListDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMWPListDTO dto) {
        EMWPList domain = emwplistMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(emwplistService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emwplistService.checkKey(emwplistMapping.toDomain(emwplistdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/confirm")
    public ResponseEntity<EMWPListDTO> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.confirm(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FillCosted-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/fillcosted")
    public ResponseEntity<EMWPListDTO> fillCostedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.fillCosted(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fillcostedbatch")
    public ResponseEntity<Boolean> fillCostedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.fillCostedBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-FormUpdateByAempid-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/formupdatebyaempid")
    public ResponseEntity<EMWPListDTO> formUpdateByAempidByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.formUpdateByAempid(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GenPO-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/genpo")
    public ResponseEntity<EMWPListDTO> genPOByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.genPO(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品采购申请]", tags = {"采购申请" },  notes = "批量处理[根据仓库仓库库位物品采购申请]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/genpobatch")
    public ResponseEntity<Boolean> genPOByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domains = emwplistMapping.toDomain(emwplistdtos);
        boolean result = emwplistService.genPOBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-GetREMP-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/getremp")
    public ResponseEntity<EMWPListDTO> getREMPByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.getREMP(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdto),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/save")
    public ResponseEntity<EMWPListDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        emwplistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emwplistMapping.toDomain(#emwplistdtos),'eam-EMWPList-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMWPListDTO> emwplistdtos) {
        List<EMWPList> domainlist=emwplistMapping.toDomain(emwplistdtos);
        for(EMWPList domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emwplistService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-Submit-all')")
    @ApiOperation(value = "根据仓库仓库库位物品采购申请", tags = {"采购申请" },  notes = "根据仓库仓库库位物品采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/{emwplist_id}/submit")
    public ResponseEntity<EMWPListDTO> submitByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emwplist_id") String emwplist_id, @RequestBody EMWPListDTO emwplistdto) {
        EMWPList domain = emwplistMapping.toDomain(emwplistdto);
        domain.setItemid(emitem_id);
        domain = emwplistService.submit(domain) ;
        emwplistdto = emwplistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emwplistdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已取消申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取已取消申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchcancel")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListCancelByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchCancel-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已取消申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询已取消申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchcancel")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListCancelByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchCancel(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待确认询价", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取待确认询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchconfimcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListConfimCostByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchConfimCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待确认询价", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询待确认询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchconfimcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListConfimCostByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchConfimCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchdefault")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDefault-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchdefault")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取草稿", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchdraft")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchDraft-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询草稿", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchdraft")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已入库申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取已入库申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchin")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchIn-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已入库申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询已入库申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchin")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取已生成订单采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchmain6")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6ByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已生成订单采购申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询已生成订单采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchmain6")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6ByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取已到货采购申请")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchmain6_8692")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListMain6_8692ByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchMain6_8692-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已到货采购申请", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询已到货采购申请")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchmain6_8692")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListMain6_8692ByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchMain6_8692(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待询价", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取待询价")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwaitcost")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitCostByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitCost-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待询价", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询待询价")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwaitcost")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitCostByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitCost(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待生成订单", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取待生成订单")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwaitpo")
	public ResponseEntity<List<EMWPListDTO>> fetchEMWPListWaitPoByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
        List<EMWPListDTO> list = emwplistMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMWPList-searchWaitPo-all') and hasPermission(#context,'eam-EMWPList-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待生成订单", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询待生成订单")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwaitpo")
	public ResponseEntity<Page<EMWPListDTO>> searchEMWPListWaitPoByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMWPList> domains = emwplistService.searchWaitPo(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emwplistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品获取采购情况统计", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品获取采购情况统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/fetchwpstatenum")
	public ResponseEntity<List<Map>> fetchEMWPListWpStateNumByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品查询采购情况统计", tags = {"采购申请" } ,notes = "根据仓库仓库库位物品查询采购情况统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emwplists/searchwpstatenum")
	public ResponseEntity<Page<Map>> searchEMWPListWpStateNumByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMWPListSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emwplistService.searchWpStateNum(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

