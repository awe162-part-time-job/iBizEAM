package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_W;
import cn.ibizlab.eam.webapi.dto.PLANSCHEDULE_WDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPLANSCHEDULE_WMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PLANSCHEDULE_WMapping extends MappingBase<PLANSCHEDULE_WDTO, PLANSCHEDULE_W> {


}

