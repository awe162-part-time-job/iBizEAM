package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWG1;
import cn.ibizlab.eam.core.eam_core.service.IEMDRWG1Service;
import cn.ibizlab.eam.core.eam_core.filter.EMDRWG1SearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"特种设备标准化管理" })
@RestController("WebApi-emdrwg1")
@RequestMapping("")
public class EMDRWG1Resource {

    @Autowired
    public IEMDRWG1Service emdrwg1Service;

    @Autowired
    @Lazy
    public EMDRWG1Mapping emdrwg1Mapping;

    @PreAuthorize("hasPermission(this.emdrwg1Mapping.toDomain(#emdrwg1dto),'eam-EMDRWG1-Create')")
    @ApiOperation(value = "新建特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "新建特种设备标准化管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwg1s")
    public ResponseEntity<EMDRWG1DTO> create(@Validated @RequestBody EMDRWG1DTO emdrwg1dto) {
        EMDRWG1 domain = emdrwg1Mapping.toDomain(emdrwg1dto);
		emdrwg1Service.create(domain);
        EMDRWG1DTO dto = emdrwg1Mapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwg1Mapping.toDomain(#emdrwg1dtos),'eam-EMDRWG1-Create')")
    @ApiOperation(value = "批量新建特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "批量新建特种设备标准化管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwg1s/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMDRWG1DTO> emdrwg1dtos) {
        emdrwg1Service.createBatch(emdrwg1Mapping.toDomain(emdrwg1dtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emdrwg1" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emdrwg1Service.get(#emdrwg1_id),'eam-EMDRWG1-Update')")
    @ApiOperation(value = "更新特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "更新特种设备标准化管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwg1s/{emdrwg1_id}")
    public ResponseEntity<EMDRWG1DTO> update(@PathVariable("emdrwg1_id") String emdrwg1_id, @RequestBody EMDRWG1DTO emdrwg1dto) {
		EMDRWG1 domain  = emdrwg1Mapping.toDomain(emdrwg1dto);
        domain .setEmdrwg1id(emdrwg1_id);
		emdrwg1Service.update(domain );
		EMDRWG1DTO dto = emdrwg1Mapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdrwg1Service.getEmdrwg1ByEntities(this.emdrwg1Mapping.toDomain(#emdrwg1dtos)),'eam-EMDRWG1-Update')")
    @ApiOperation(value = "批量更新特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "批量更新特种设备标准化管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdrwg1s/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMDRWG1DTO> emdrwg1dtos) {
        emdrwg1Service.updateBatch(emdrwg1Mapping.toDomain(emdrwg1dtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emdrwg1Service.get(#emdrwg1_id),'eam-EMDRWG1-Remove')")
    @ApiOperation(value = "删除特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "删除特种设备标准化管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwg1s/{emdrwg1_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emdrwg1_id") String emdrwg1_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emdrwg1Service.remove(emdrwg1_id));
    }

    @PreAuthorize("hasPermission(this.emdrwg1Service.getEmdrwg1ByIds(#ids),'eam-EMDRWG1-Remove')")
    @ApiOperation(value = "批量删除特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "批量删除特种设备标准化管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdrwg1s/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emdrwg1Service.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emdrwg1Mapping.toDomain(returnObject.body),'eam-EMDRWG1-Get')")
    @ApiOperation(value = "获取特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "获取特种设备标准化管理")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwg1s/{emdrwg1_id}")
    public ResponseEntity<EMDRWG1DTO> get(@PathVariable("emdrwg1_id") String emdrwg1_id) {
        EMDRWG1 domain = emdrwg1Service.get(emdrwg1_id);
        EMDRWG1DTO dto = emdrwg1Mapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取特种设备标准化管理草稿", tags = {"特种设备标准化管理" },  notes = "获取特种设备标准化管理草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emdrwg1s/getdraft")
    public ResponseEntity<EMDRWG1DTO> getDraft(EMDRWG1DTO dto) {
        EMDRWG1 domain = emdrwg1Mapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emdrwg1Mapping.toDto(emdrwg1Service.getDraft(domain)));
    }

    @ApiOperation(value = "检查特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "检查特种设备标准化管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwg1s/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMDRWG1DTO emdrwg1dto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emdrwg1Service.checkKey(emdrwg1Mapping.toDomain(emdrwg1dto)));
    }

    @PreAuthorize("hasPermission(this.emdrwg1Mapping.toDomain(#emdrwg1dto),'eam-EMDRWG1-Save')")
    @ApiOperation(value = "保存特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "保存特种设备标准化管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwg1s/save")
    public ResponseEntity<EMDRWG1DTO> save(@RequestBody EMDRWG1DTO emdrwg1dto) {
        EMDRWG1 domain = emdrwg1Mapping.toDomain(emdrwg1dto);
        emdrwg1Service.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emdrwg1Mapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emdrwg1Mapping.toDomain(#emdrwg1dtos),'eam-EMDRWG1-Save')")
    @ApiOperation(value = "批量保存特种设备标准化管理", tags = {"特种设备标准化管理" },  notes = "批量保存特种设备标准化管理")
	@RequestMapping(method = RequestMethod.POST, value = "/emdrwg1s/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMDRWG1DTO> emdrwg1dtos) {
        emdrwg1Service.saveBatch(emdrwg1Mapping.toDomain(emdrwg1dtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWG1-searchDefault-all') and hasPermission(#context,'eam-EMDRWG1-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"特种设备标准化管理" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emdrwg1s/fetchdefault")
	public ResponseEntity<List<EMDRWG1DTO>> fetchDefault(EMDRWG1SearchContext context) {
        Page<EMDRWG1> domains = emdrwg1Service.searchDefault(context) ;
        List<EMDRWG1DTO> list = emdrwg1Mapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDRWG1-searchDefault-all') and hasPermission(#context,'eam-EMDRWG1-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"特种设备标准化管理" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emdrwg1s/searchdefault")
	public ResponseEntity<Page<EMDRWG1DTO>> searchDefault(@RequestBody EMDRWG1SearchContext context) {
        Page<EMDRWG1> domains = emdrwg1Service.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdrwg1Mapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

