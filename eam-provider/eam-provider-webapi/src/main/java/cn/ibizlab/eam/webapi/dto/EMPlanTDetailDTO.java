package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMPlanTDetailDTO]
 */
@Data
@ApiModel("计划模板步骤")
public class EMPlanTDetailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [EMPLANTDETAILID]
     *
     */
    @JSONField(name = "emplantdetailid")
    @JsonProperty("emplantdetailid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("计划模板步骤标识")
    private String emplantdetailid;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;

    /**
     * 属性 [PLANDETAILDESC]
     *
     */
    @JSONField(name = "plandetaildesc")
    @JsonProperty("plandetaildesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("计划步骤内容")
    private String plandetaildesc;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("接收人")
    private String recvpersonid;

    /**
     * 属性 [EMPLANTDETAILNAME]
     *
     */
    @JSONField(name = "emplantdetailname")
    @JsonProperty("emplantdetailname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划模板步骤名称")
    private String emplantdetailname;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [ORDERFLAG]
     *
     */
    @JSONField(name = "orderflag")
    @JsonProperty("orderflag")
    @ApiModelProperty("排序")
    private Integer orderflag;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("生成工单种类")
    private String emwotype;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("接收人")
    private String recvpersonname;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("归档")
    private String archive;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("详细内容")
    private String content;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [PLANTEMPLNAME]
     *
     */
    @JSONField(name = "plantemplname")
    @JsonProperty("plantemplname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划模板")
    private String plantemplname;

    /**
     * 属性 [PLANTEMPLID]
     *
     */
    @JSONField(name = "plantemplid")
    @JsonProperty("plantemplid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("计划模板")
    private String plantemplid;


    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [PLANDETAILDESC]
     */
    public void setPlandetaildesc(String  plandetaildesc){
        this.plandetaildesc = plandetaildesc ;
        this.modify("plandetaildesc",plandetaildesc);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }

    /**
     * 设置 [EMPLANTDETAILNAME]
     */
    public void setEmplantdetailname(String  emplantdetailname){
        this.emplantdetailname = emplantdetailname ;
        this.modify("emplantdetailname",emplantdetailname);
    }

    /**
     * 设置 [ORDERFLAG]
     */
    public void setOrderflag(Integer  orderflag){
        this.orderflag = orderflag ;
        this.modify("orderflag",orderflag);
    }

    /**
     * 设置 [EMWOTYPE]
     */
    public void setEmwotype(String  emwotype){
        this.emwotype = emwotype ;
        this.modify("emwotype",emwotype);
    }

    /**
     * 设置 [RECVPERSONNAME]
     */
    public void setRecvpersonname(String  recvpersonname){
        this.recvpersonname = recvpersonname ;
        this.modify("recvpersonname",recvpersonname);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [PLANTEMPLID]
     */
    public void setPlantemplid(String  plantemplid){
        this.plantemplid = plantemplid ;
        this.modify("plantemplid",plantemplid);
    }


}


