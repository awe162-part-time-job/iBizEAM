package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpare;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"备件包" })
@RestController("WebApi-emeqspare")
@RequestMapping("")
public class EMEQSpareResource {

    @Autowired
    public IEMEQSpareService emeqspareService;

    @Autowired
    @Lazy
    public EMEQSpareMapping emeqspareMapping;

    @PreAuthorize("hasPermission(this.emeqspareMapping.toDomain(#emeqsparedto),'eam-EMEQSpare-Create')")
    @ApiOperation(value = "新建备件包", tags = {"备件包" },  notes = "新建备件包")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares")
    public ResponseEntity<EMEQSpareDTO> create(@Validated @RequestBody EMEQSpareDTO emeqsparedto) {
        EMEQSpare domain = emeqspareMapping.toDomain(emeqsparedto);
		emeqspareService.create(domain);
        EMEQSpareDTO dto = emeqspareMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqspareMapping.toDomain(#emeqsparedtos),'eam-EMEQSpare-Create')")
    @ApiOperation(value = "批量新建备件包", tags = {"备件包" },  notes = "批量新建备件包")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQSpareDTO> emeqsparedtos) {
        emeqspareService.createBatch(emeqspareMapping.toDomain(emeqsparedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqspare" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqspareService.get(#emeqspare_id),'eam-EMEQSpare-Update')")
    @ApiOperation(value = "更新备件包", tags = {"备件包" },  notes = "更新备件包")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/{emeqspare_id}")
    public ResponseEntity<EMEQSpareDTO> update(@PathVariable("emeqspare_id") String emeqspare_id, @RequestBody EMEQSpareDTO emeqsparedto) {
		EMEQSpare domain  = emeqspareMapping.toDomain(emeqsparedto);
        domain .setEmeqspareid(emeqspare_id);
		emeqspareService.update(domain );
		EMEQSpareDTO dto = emeqspareMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqspareService.getEmeqspareByEntities(this.emeqspareMapping.toDomain(#emeqsparedtos)),'eam-EMEQSpare-Update')")
    @ApiOperation(value = "批量更新备件包", tags = {"备件包" },  notes = "批量更新备件包")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqspares/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQSpareDTO> emeqsparedtos) {
        emeqspareService.updateBatch(emeqspareMapping.toDomain(emeqsparedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqspareService.get(#emeqspare_id),'eam-EMEQSpare-Remove')")
    @ApiOperation(value = "删除备件包", tags = {"备件包" },  notes = "删除备件包")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/{emeqspare_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqspare_id") String emeqspare_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqspareService.remove(emeqspare_id));
    }

    @PreAuthorize("hasPermission(this.emeqspareService.getEmeqspareByIds(#ids),'eam-EMEQSpare-Remove')")
    @ApiOperation(value = "批量删除备件包", tags = {"备件包" },  notes = "批量删除备件包")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqspares/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqspareService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqspareMapping.toDomain(returnObject.body),'eam-EMEQSpare-Get')")
    @ApiOperation(value = "获取备件包", tags = {"备件包" },  notes = "获取备件包")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqspares/{emeqspare_id}")
    public ResponseEntity<EMEQSpareDTO> get(@PathVariable("emeqspare_id") String emeqspare_id) {
        EMEQSpare domain = emeqspareService.get(emeqspare_id);
        EMEQSpareDTO dto = emeqspareMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取备件包草稿", tags = {"备件包" },  notes = "获取备件包草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqspares/getdraft")
    public ResponseEntity<EMEQSpareDTO> getDraft(EMEQSpareDTO dto) {
        EMEQSpare domain = emeqspareMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqspareMapping.toDto(emeqspareService.getDraft(domain)));
    }

    @ApiOperation(value = "检查备件包", tags = {"备件包" },  notes = "检查备件包")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQSpareDTO emeqsparedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqspareService.checkKey(emeqspareMapping.toDomain(emeqsparedto)));
    }

    @PreAuthorize("hasPermission(this.emeqspareMapping.toDomain(#emeqsparedto),'eam-EMEQSpare-Save')")
    @ApiOperation(value = "保存备件包", tags = {"备件包" },  notes = "保存备件包")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/save")
    public ResponseEntity<EMEQSpareDTO> save(@RequestBody EMEQSpareDTO emeqsparedto) {
        EMEQSpare domain = emeqspareMapping.toDomain(emeqsparedto);
        emeqspareService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqspareMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqspareMapping.toDomain(#emeqsparedtos),'eam-EMEQSpare-Save')")
    @ApiOperation(value = "批量保存备件包", tags = {"备件包" },  notes = "批量保存备件包")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqspares/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQSpareDTO> emeqsparedtos) {
        emeqspareService.saveBatch(emeqspareMapping.toDomain(emeqsparedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpare-searchDefault-all') and hasPermission(#context,'eam-EMEQSpare-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"备件包" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqspares/fetchdefault")
	public ResponseEntity<List<EMEQSpareDTO>> fetchDefault(EMEQSpareSearchContext context) {
        Page<EMEQSpare> domains = emeqspareService.searchDefault(context) ;
        List<EMEQSpareDTO> list = emeqspareMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQSpare-searchDefault-all') and hasPermission(#context,'eam-EMEQSpare-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"备件包" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqspares/searchdefault")
	public ResponseEntity<Page<EMEQSpareDTO>> searchDefault(@RequestBody EMEQSpareSearchContext context) {
        Page<EMEQSpare> domains = emeqspareService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqspareMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

