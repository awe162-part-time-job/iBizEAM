package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceHist;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceHistService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceHistSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务商历史审批" })
@RestController("WebApi-emservicehist")
@RequestMapping("")
public class EMServiceHistResource {

    @Autowired
    public IEMServiceHistService emservicehistService;

    @Autowired
    @Lazy
    public EMServiceHistMapping emservicehistMapping;

    @PreAuthorize("hasPermission(this.emservicehistMapping.toDomain(#emservicehistdto),'eam-EMServiceHist-Create')")
    @ApiOperation(value = "新建服务商历史审批", tags = {"服务商历史审批" },  notes = "新建服务商历史审批")
	@RequestMapping(method = RequestMethod.POST, value = "/emservicehists")
    public ResponseEntity<EMServiceHistDTO> create(@Validated @RequestBody EMServiceHistDTO emservicehistdto) {
        EMServiceHist domain = emservicehistMapping.toDomain(emservicehistdto);
		emservicehistService.create(domain);
        EMServiceHistDTO dto = emservicehistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emservicehistMapping.toDomain(#emservicehistdtos),'eam-EMServiceHist-Create')")
    @ApiOperation(value = "批量新建服务商历史审批", tags = {"服务商历史审批" },  notes = "批量新建服务商历史审批")
	@RequestMapping(method = RequestMethod.POST, value = "/emservicehists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMServiceHistDTO> emservicehistdtos) {
        emservicehistService.createBatch(emservicehistMapping.toDomain(emservicehistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emservicehist" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emservicehistService.get(#emservicehist_id),'eam-EMServiceHist-Update')")
    @ApiOperation(value = "更新服务商历史审批", tags = {"服务商历史审批" },  notes = "更新服务商历史审批")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservicehists/{emservicehist_id}")
    public ResponseEntity<EMServiceHistDTO> update(@PathVariable("emservicehist_id") String emservicehist_id, @RequestBody EMServiceHistDTO emservicehistdto) {
		EMServiceHist domain  = emservicehistMapping.toDomain(emservicehistdto);
        domain .setEmservicehistid(emservicehist_id);
		emservicehistService.update(domain );
		EMServiceHistDTO dto = emservicehistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emservicehistService.getEmservicehistByEntities(this.emservicehistMapping.toDomain(#emservicehistdtos)),'eam-EMServiceHist-Update')")
    @ApiOperation(value = "批量更新服务商历史审批", tags = {"服务商历史审批" },  notes = "批量更新服务商历史审批")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservicehists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMServiceHistDTO> emservicehistdtos) {
        emservicehistService.updateBatch(emservicehistMapping.toDomain(emservicehistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emservicehistService.get(#emservicehist_id),'eam-EMServiceHist-Remove')")
    @ApiOperation(value = "删除服务商历史审批", tags = {"服务商历史审批" },  notes = "删除服务商历史审批")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservicehists/{emservicehist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emservicehist_id") String emservicehist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emservicehistService.remove(emservicehist_id));
    }

    @PreAuthorize("hasPermission(this.emservicehistService.getEmservicehistByIds(#ids),'eam-EMServiceHist-Remove')")
    @ApiOperation(value = "批量删除服务商历史审批", tags = {"服务商历史审批" },  notes = "批量删除服务商历史审批")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservicehists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emservicehistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emservicehistMapping.toDomain(returnObject.body),'eam-EMServiceHist-Get')")
    @ApiOperation(value = "获取服务商历史审批", tags = {"服务商历史审批" },  notes = "获取服务商历史审批")
	@RequestMapping(method = RequestMethod.GET, value = "/emservicehists/{emservicehist_id}")
    public ResponseEntity<EMServiceHistDTO> get(@PathVariable("emservicehist_id") String emservicehist_id) {
        EMServiceHist domain = emservicehistService.get(emservicehist_id);
        EMServiceHistDTO dto = emservicehistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务商历史审批草稿", tags = {"服务商历史审批" },  notes = "获取服务商历史审批草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emservicehists/getdraft")
    public ResponseEntity<EMServiceHistDTO> getDraft(EMServiceHistDTO dto) {
        EMServiceHist domain = emservicehistMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emservicehistMapping.toDto(emservicehistService.getDraft(domain)));
    }

    @ApiOperation(value = "检查服务商历史审批", tags = {"服务商历史审批" },  notes = "检查服务商历史审批")
	@RequestMapping(method = RequestMethod.POST, value = "/emservicehists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMServiceHistDTO emservicehistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emservicehistService.checkKey(emservicehistMapping.toDomain(emservicehistdto)));
    }

    @PreAuthorize("hasPermission(this.emservicehistMapping.toDomain(#emservicehistdto),'eam-EMServiceHist-Save')")
    @ApiOperation(value = "保存服务商历史审批", tags = {"服务商历史审批" },  notes = "保存服务商历史审批")
	@RequestMapping(method = RequestMethod.POST, value = "/emservicehists/save")
    public ResponseEntity<EMServiceHistDTO> save(@RequestBody EMServiceHistDTO emservicehistdto) {
        EMServiceHist domain = emservicehistMapping.toDomain(emservicehistdto);
        emservicehistService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emservicehistMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emservicehistMapping.toDomain(#emservicehistdtos),'eam-EMServiceHist-Save')")
    @ApiOperation(value = "批量保存服务商历史审批", tags = {"服务商历史审批" },  notes = "批量保存服务商历史审批")
	@RequestMapping(method = RequestMethod.POST, value = "/emservicehists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMServiceHistDTO> emservicehistdtos) {
        emservicehistService.saveBatch(emservicehistMapping.toDomain(emservicehistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceHist-searchDefault-all') and hasPermission(#context,'eam-EMServiceHist-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务商历史审批" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservicehists/fetchdefault")
	public ResponseEntity<List<EMServiceHistDTO>> fetchDefault(EMServiceHistSearchContext context) {
        Page<EMServiceHist> domains = emservicehistService.searchDefault(context) ;
        List<EMServiceHistDTO> list = emservicehistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceHist-searchDefault-all') and hasPermission(#context,'eam-EMServiceHist-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务商历史审批" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservicehists/searchdefault")
	public ResponseEntity<Page<EMServiceHistDTO>> searchDefault(@RequestBody EMServiceHistSearchContext context) {
        Page<EMServiceHist> domains = emservicehistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emservicehistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

