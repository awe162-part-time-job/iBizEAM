package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMBidinquiry;
import cn.ibizlab.eam.core.eam_core.service.IEMBidinquiryService;
import cn.ibizlab.eam.core.eam_core.filter.EMBidinquirySearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"招投标信息" })
@RestController("WebApi-embidinquiry")
@RequestMapping("")
public class EMBidinquiryResource {

    @Autowired
    public IEMBidinquiryService embidinquiryService;

    @Autowired
    @Lazy
    public EMBidinquiryMapping embidinquiryMapping;

    @PreAuthorize("hasPermission(this.embidinquiryMapping.toDomain(#embidinquirydto),'eam-EMBidinquiry-Create')")
    @ApiOperation(value = "新建招投标信息", tags = {"招投标信息" },  notes = "新建招投标信息")
	@RequestMapping(method = RequestMethod.POST, value = "/embidinquiries")
    public ResponseEntity<EMBidinquiryDTO> create(@Validated @RequestBody EMBidinquiryDTO embidinquirydto) {
        EMBidinquiry domain = embidinquiryMapping.toDomain(embidinquirydto);
		embidinquiryService.create(domain);
        EMBidinquiryDTO dto = embidinquiryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embidinquiryMapping.toDomain(#embidinquirydtos),'eam-EMBidinquiry-Create')")
    @ApiOperation(value = "批量新建招投标信息", tags = {"招投标信息" },  notes = "批量新建招投标信息")
	@RequestMapping(method = RequestMethod.POST, value = "/embidinquiries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMBidinquiryDTO> embidinquirydtos) {
        embidinquiryService.createBatch(embidinquiryMapping.toDomain(embidinquirydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "embidinquiry" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.embidinquiryService.get(#embidinquiry_id),'eam-EMBidinquiry-Update')")
    @ApiOperation(value = "更新招投标信息", tags = {"招投标信息" },  notes = "更新招投标信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/embidinquiries/{embidinquiry_id}")
    public ResponseEntity<EMBidinquiryDTO> update(@PathVariable("embidinquiry_id") String embidinquiry_id, @RequestBody EMBidinquiryDTO embidinquirydto) {
		EMBidinquiry domain  = embidinquiryMapping.toDomain(embidinquirydto);
        domain .setEmbidinquiryid(embidinquiry_id);
		embidinquiryService.update(domain );
		EMBidinquiryDTO dto = embidinquiryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embidinquiryService.getEmbidinquiryByEntities(this.embidinquiryMapping.toDomain(#embidinquirydtos)),'eam-EMBidinquiry-Update')")
    @ApiOperation(value = "批量更新招投标信息", tags = {"招投标信息" },  notes = "批量更新招投标信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/embidinquiries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMBidinquiryDTO> embidinquirydtos) {
        embidinquiryService.updateBatch(embidinquiryMapping.toDomain(embidinquirydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.embidinquiryService.get(#embidinquiry_id),'eam-EMBidinquiry-Remove')")
    @ApiOperation(value = "删除招投标信息", tags = {"招投标信息" },  notes = "删除招投标信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embidinquiries/{embidinquiry_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("embidinquiry_id") String embidinquiry_id) {
         return ResponseEntity.status(HttpStatus.OK).body(embidinquiryService.remove(embidinquiry_id));
    }

    @PreAuthorize("hasPermission(this.embidinquiryService.getEmbidinquiryByIds(#ids),'eam-EMBidinquiry-Remove')")
    @ApiOperation(value = "批量删除招投标信息", tags = {"招投标信息" },  notes = "批量删除招投标信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embidinquiries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        embidinquiryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.embidinquiryMapping.toDomain(returnObject.body),'eam-EMBidinquiry-Get')")
    @ApiOperation(value = "获取招投标信息", tags = {"招投标信息" },  notes = "获取招投标信息")
	@RequestMapping(method = RequestMethod.GET, value = "/embidinquiries/{embidinquiry_id}")
    public ResponseEntity<EMBidinquiryDTO> get(@PathVariable("embidinquiry_id") String embidinquiry_id) {
        EMBidinquiry domain = embidinquiryService.get(embidinquiry_id);
        EMBidinquiryDTO dto = embidinquiryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取招投标信息草稿", tags = {"招投标信息" },  notes = "获取招投标信息草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/embidinquiries/getdraft")
    public ResponseEntity<EMBidinquiryDTO> getDraft(EMBidinquiryDTO dto) {
        EMBidinquiry domain = embidinquiryMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(embidinquiryMapping.toDto(embidinquiryService.getDraft(domain)));
    }

    @ApiOperation(value = "检查招投标信息", tags = {"招投标信息" },  notes = "检查招投标信息")
	@RequestMapping(method = RequestMethod.POST, value = "/embidinquiries/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMBidinquiryDTO embidinquirydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(embidinquiryService.checkKey(embidinquiryMapping.toDomain(embidinquirydto)));
    }

    @PreAuthorize("hasPermission(this.embidinquiryMapping.toDomain(#embidinquirydto),'eam-EMBidinquiry-Save')")
    @ApiOperation(value = "保存招投标信息", tags = {"招投标信息" },  notes = "保存招投标信息")
	@RequestMapping(method = RequestMethod.POST, value = "/embidinquiries/save")
    public ResponseEntity<EMBidinquiryDTO> save(@RequestBody EMBidinquiryDTO embidinquirydto) {
        EMBidinquiry domain = embidinquiryMapping.toDomain(embidinquirydto);
        embidinquiryService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(embidinquiryMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.embidinquiryMapping.toDomain(#embidinquirydtos),'eam-EMBidinquiry-Save')")
    @ApiOperation(value = "批量保存招投标信息", tags = {"招投标信息" },  notes = "批量保存招投标信息")
	@RequestMapping(method = RequestMethod.POST, value = "/embidinquiries/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMBidinquiryDTO> embidinquirydtos) {
        embidinquiryService.saveBatch(embidinquiryMapping.toDomain(embidinquirydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBidinquiry-searchDefault-all') and hasPermission(#context,'eam-EMBidinquiry-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"招投标信息" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/embidinquiries/fetchdefault")
	public ResponseEntity<List<EMBidinquiryDTO>> fetchDefault(EMBidinquirySearchContext context) {
        Page<EMBidinquiry> domains = embidinquiryService.searchDefault(context) ;
        List<EMBidinquiryDTO> list = embidinquiryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBidinquiry-searchDefault-all') and hasPermission(#context,'eam-EMBidinquiry-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"招投标信息" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/embidinquiries/searchdefault")
	public ResponseEntity<Page<EMBidinquiryDTO>> searchDefault(@RequestBody EMBidinquirySearchContext context) {
        Page<EMBidinquiry> domains = embidinquiryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(embidinquiryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

