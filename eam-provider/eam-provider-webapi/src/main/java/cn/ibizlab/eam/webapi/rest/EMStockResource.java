package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMStock;
import cn.ibizlab.eam.core.eam_core.service.IEMStockService;
import cn.ibizlab.eam.core.eam_core.filter.EMStockSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存" })
@RestController("WebApi-emstock")
@RequestMapping("")
public class EMStockResource {

    @Autowired
    public IEMStockService emstockService;

    @Autowired
    @Lazy
    public EMStockMapping emstockMapping;

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "新建库存", tags = {"库存" },  notes = "新建库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstocks")
    public ResponseEntity<EMStockDTO> create(@Validated @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "批量新建库存", tags = {"库存" },  notes = "批量新建库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstocks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMStockDTO> emstockdtos) {
        emstockService.createBatch(emstockMapping.toDomain(emstockdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "更新库存", tags = {"库存" },  notes = "更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> update(@PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
		EMStock domain  = emstockMapping.toDomain(emstockdto);
        domain .setEmstockid(emstock_id);
		emstockService.update(domain );
		EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "批量更新库存", tags = {"库存" },  notes = "批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstocks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMStockDTO> emstockdtos) {
        emstockService.updateBatch(emstockMapping.toDomain(emstockdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "删除库存", tags = {"库存" },  notes = "删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emstock_id") String emstock_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "批量删除库存", tags = {"库存" },  notes = "批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstocks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "获取库存", tags = {"库存" },  notes = "获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> get(@PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存草稿", tags = {"库存" },  notes = "获取库存草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraft(EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "检查库存", tags = {"库存" },  notes = "检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "保存库存", tags = {"库存" },  notes = "保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstocks/save")
    public ResponseEntity<EMStockDTO> save(@RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "批量保存库存", tags = {"库存" },  notes = "批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMStockDTO> emstockdtos) {
        emstockService.saveBatch(emstockMapping.toDomain(emstockdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"库存" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchDefault(EMStockSearchContext context) {
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"库存" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchDefault(@RequestBody EMStockSearchContext context) {
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取类型库存统计", tags = {"库存" } ,notes = "获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchTypeStock(EMStockSearchContext context) {
        Page<Map> domains = emstockService.searchTypeStock(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询类型库存统计", tags = {"库存" } ,notes = "查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchTypeStock(@RequestBody EMStockSearchContext context) {
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据物品建立库存", tags = {"库存" },  notes = "根据物品建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据物品批量建立库存", tags = {"库存" },  notes = "根据物品批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据物品更新库存", tags = {"库存" },  notes = "根据物品更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据物品批量更新库存", tags = {"库存" },  notes = "根据物品批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据物品删除库存", tags = {"库存" },  notes = "根据物品删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据物品批量删除库存", tags = {"库存" },  notes = "根据物品批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据物品获取库存", tags = {"库存" },  notes = "根据物品获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取库存草稿", tags = {"库存" },  notes = "根据物品获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查库存", tags = {"库存" },  notes = "根据物品检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据物品保存库存", tags = {"库存" },  notes = "根据物品保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据物品批量保存库存", tags = {"库存" },  notes = "根据物品批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"库存" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"库存" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品获取类型库存统计", tags = {"库存" } ,notes = "根据物品获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMItem(@PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品查询类型库存统计", tags = {"库存" } ,notes = "根据物品查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库库位建立库存", tags = {"库存" },  notes = "根据仓库库位建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库库位批量建立库存", tags = {"库存" },  notes = "根据仓库库位批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库库位更新库存", tags = {"库存" },  notes = "根据仓库库位更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库库位批量更新库存", tags = {"库存" },  notes = "根据仓库库位批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库库位删除库存", tags = {"库存" },  notes = "根据仓库库位删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库库位批量删除库存", tags = {"库存" },  notes = "根据仓库库位批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePart(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库库位获取库存", tags = {"库存" },  notes = "根据仓库库位获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位获取库存草稿", tags = {"库存" },  notes = "根据仓库库位获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位检查库存", tags = {"库存" },  notes = "根据仓库库位检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库库位保存库存", tags = {"库存" },  notes = "根据仓库库位保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库库位批量保存库存", tags = {"库存" },  notes = "根据仓库库位批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库库位获取DEFAULT", tags = {"库存" } ,notes = "根据仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库库位查询DEFAULT", tags = {"库存" } ,notes = "根据仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位获取类型库存统计", tags = {"库存" } ,notes = "根据仓库库位获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位查询类型库存统计", tags = {"库存" } ,notes = "根据仓库库位查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库建立库存", tags = {"库存" },  notes = "根据仓库建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStoreid(emstore_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库批量建立库存", tags = {"库存" },  notes = "根据仓库批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库更新库存", tags = {"库存" },  notes = "根据仓库更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStoreid(emstore_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库批量更新库存", tags = {"库存" },  notes = "根据仓库批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库删除库存", tags = {"库存" },  notes = "根据仓库删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库批量删除库存", tags = {"库存" },  notes = "根据仓库批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStore(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库获取库存", tags = {"库存" },  notes = "根据仓库获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库获取库存草稿", tags = {"库存" },  notes = "根据仓库获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStore(@PathVariable("emstore_id") String emstore_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setStoreid(emstore_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库检查库存", tags = {"库存" },  notes = "根据仓库检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库保存库存", tags = {"库存" },  notes = "根据仓库保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStoreid(emstore_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库批量保存库存", tags = {"库存" },  notes = "根据仓库批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setStoreid(emstore_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库获取DEFAULT", tags = {"库存" } ,notes = "根据仓库获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStore(@PathVariable("emstore_id") String emstore_id,EMStockSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库查询DEFAULT", tags = {"库存" } ,notes = "根据仓库查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStockSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库获取类型库存统计", tags = {"库存" } ,notes = "根据仓库获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStore(@PathVariable("emstore_id") String emstore_id,EMStockSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库查询类型库存统计", tags = {"库存" } ,notes = "根据仓库查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStockSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据服务商物品建立库存", tags = {"库存" },  notes = "根据服务商物品建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据服务商物品批量建立库存", tags = {"库存" },  notes = "根据服务商物品批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据服务商物品更新库存", tags = {"库存" },  notes = "根据服务商物品更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据服务商物品批量更新库存", tags = {"库存" },  notes = "根据服务商物品批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据服务商物品删除库存", tags = {"库存" },  notes = "根据服务商物品删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除库存", tags = {"库存" },  notes = "根据服务商物品批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据服务商物品获取库存", tags = {"库存" },  notes = "根据服务商物品获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取库存草稿", tags = {"库存" },  notes = "根据服务商物品获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查库存", tags = {"库存" },  notes = "根据服务商物品检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据服务商物品保存库存", tags = {"库存" },  notes = "根据服务商物品保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据服务商物品批量保存库存", tags = {"库存" },  notes = "根据服务商物品批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"库存" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"库存" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品获取类型库存统计", tags = {"库存" } ,notes = "根据服务商物品获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品查询类型库存统计", tags = {"库存" } ,notes = "根据服务商物品查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库库位物品建立库存", tags = {"库存" },  notes = "根据仓库库位物品建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立库存", tags = {"库存" },  notes = "根据仓库库位物品批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库库位物品更新库存", tags = {"库存" },  notes = "根据仓库库位物品更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新库存", tags = {"库存" },  notes = "根据仓库库位物品批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除库存", tags = {"库存" },  notes = "根据仓库库位物品删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除库存", tags = {"库存" },  notes = "根据仓库库位物品批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库库位物品获取库存", tags = {"库存" },  notes = "根据仓库库位物品获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取库存草稿", tags = {"库存" },  notes = "根据仓库库位物品获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查库存", tags = {"库存" },  notes = "根据仓库库位物品检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库库位物品保存库存", tags = {"库存" },  notes = "根据仓库库位物品保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存库存", tags = {"库存" },  notes = "根据仓库库位物品批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"库存" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"库存" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品获取类型库存统计", tags = {"库存" } ,notes = "根据仓库库位物品获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品查询类型库存统计", tags = {"库存" } ,notes = "根据仓库库位物品查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库物品建立库存", tags = {"库存" },  notes = "根据仓库物品建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库物品批量建立库存", tags = {"库存" },  notes = "根据仓库物品批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库物品更新库存", tags = {"库存" },  notes = "根据仓库物品更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库物品批量更新库存", tags = {"库存" },  notes = "根据仓库物品批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库物品删除库存", tags = {"库存" },  notes = "根据仓库物品删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除库存", tags = {"库存" },  notes = "根据仓库物品批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库物品获取库存", tags = {"库存" },  notes = "根据仓库物品获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取库存草稿", tags = {"库存" },  notes = "根据仓库物品获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查库存", tags = {"库存" },  notes = "根据仓库物品检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库物品保存库存", tags = {"库存" },  notes = "根据仓库物品保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库物品批量保存库存", tags = {"库存" },  notes = "根据仓库物品批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"库存" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"库存" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品获取类型库存统计", tags = {"库存" } ,notes = "根据仓库物品获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品查询类型库存统计", tags = {"库存" } ,notes = "根据仓库物品查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库仓库库位建立库存", tags = {"库存" },  notes = "根据仓库仓库库位建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库仓库库位批量建立库存", tags = {"库存" },  notes = "根据仓库仓库库位批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库仓库库位更新库存", tags = {"库存" },  notes = "根据仓库仓库库位更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库仓库库位批量更新库存", tags = {"库存" },  notes = "根据仓库仓库库位批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库仓库库位删除库存", tags = {"库存" },  notes = "根据仓库仓库库位删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库仓库库位批量删除库存", tags = {"库存" },  notes = "根据仓库仓库库位批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePart(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库仓库库位获取库存", tags = {"库存" },  notes = "根据仓库仓库库位获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位获取库存草稿", tags = {"库存" },  notes = "根据仓库仓库库位获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位检查库存", tags = {"库存" },  notes = "根据仓库仓库库位检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库仓库库位保存库存", tags = {"库存" },  notes = "根据仓库仓库库位保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setStorepartid(emstorepart_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库仓库库位批量保存库存", tags = {"库存" },  notes = "根据仓库仓库库位批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取DEFAULT", tags = {"库存" } ,notes = "根据仓库仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询DEFAULT", tags = {"库存" } ,notes = "根据仓库仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位获取类型库存统计", tags = {"库存" } ,notes = "根据仓库仓库库位获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位查询类型库存统计", tags = {"库存" } ,notes = "根据仓库仓库库位查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMStockSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立库存", tags = {"库存" },  notes = "根据仓库仓库库位物品建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks")
    public ResponseEntity<EMStockDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
		emstockService.create(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立库存", tags = {"库存" },  notes = "根据仓库仓库库位物品批量建立库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstock" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新库存", tags = {"库存" },  notes = "根据仓库仓库库位物品更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        domain.setEmstockid(emstock_id);
		emstockService.update(domain);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByEntities(this.emstockMapping.toDomain(#emstockdtos)),'eam-EMStock-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新库存", tags = {"库存" },  notes = "根据仓库仓库库位物品批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emstockService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstockService.get(#emstock_id),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除库存", tags = {"库存" },  notes = "根据仓库仓库库位物品删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emstockService.remove(emstock_id));
    }

    @PreAuthorize("hasPermission(this.emstockService.getEmstockByIds(#ids),'eam-EMStock-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除库存", tags = {"库存" },  notes = "根据仓库仓库库位物品批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emstockService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstockMapping.toDomain(returnObject.body),'eam-EMStock-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取库存", tags = {"库存" },  notes = "根据仓库仓库库位物品获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/{emstock_id}")
    public ResponseEntity<EMStockDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emstock_id") String emstock_id) {
        EMStock domain = emstockService.get(emstock_id);
        EMStockDTO dto = emstockMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取库存草稿", tags = {"库存" },  notes = "根据仓库仓库库位物品获取库存草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/getdraft")
    public ResponseEntity<EMStockDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMStockDTO dto) {
        EMStock domain = emstockMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(emstockService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查库存", tags = {"库存" },  notes = "根据仓库仓库库位物品检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstockService.checkKey(emstockMapping.toDomain(emstockdto)));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdto),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存库存", tags = {"库存" },  notes = "根据仓库仓库库位物品保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/save")
    public ResponseEntity<EMStockDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockDTO emstockdto) {
        EMStock domain = emstockMapping.toDomain(emstockdto);
        domain.setItemid(emitem_id);
        emstockService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstockMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstockMapping.toDomain(#emstockdtos),'eam-EMStock-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存库存", tags = {"库存" },  notes = "根据仓库仓库库位物品批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMStockDTO> emstockdtos) {
        List<EMStock> domainlist=emstockMapping.toDomain(emstockdtos);
        for(EMStock domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emstockService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"库存" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/fetchdefault")
	public ResponseEntity<List<EMStockDTO>> fetchEMStockDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
        List<EMStockDTO> list = emstockMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStock-searchDefault-all') and hasPermission(#context,'eam-EMStock-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"库存" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/searchdefault")
	public ResponseEntity<Page<EMStockDTO>> searchEMStockDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMStock> domains = emstockService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstockMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品获取类型库存统计", tags = {"库存" } ,notes = "根据仓库仓库库位物品获取类型库存统计")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/fetchtypestock")
	public ResponseEntity<List<Map>> fetchEMStockTypeStockByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品查询类型库存统计", tags = {"库存" } ,notes = "根据仓库仓库库位物品查询类型库存统计")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emstocks/searchtypestock")
	public ResponseEntity<Page<Map>> searchEMStockTypeStockByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMStockSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<Map> domains = emstockService.searchTypeStock(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

