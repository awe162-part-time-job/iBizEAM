package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMENConsum;
import cn.ibizlab.eam.core.eam_core.service.IEMENConsumService;
import cn.ibizlab.eam.core.eam_core.filter.EMENConsumSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"能耗" })
@RestController("WebApi-emenconsum")
@RequestMapping("")
public class EMENConsumResource {

    @Autowired
    public IEMENConsumService emenconsumService;

    @Autowired
    @Lazy
    public EMENConsumMapping emenconsumMapping;

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "新建能耗", tags = {"能耗" },  notes = "新建能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emenconsums")
    public ResponseEntity<EMENConsumDTO> create(@Validated @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "批量新建能耗", tags = {"能耗" },  notes = "批量新建能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emenconsums/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMENConsumDTO> emenconsumdtos) {
        emenconsumService.createBatch(emenconsumMapping.toDomain(emenconsumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "更新能耗", tags = {"能耗" },  notes = "更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> update(@PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
		EMENConsum domain  = emenconsumMapping.toDomain(emenconsumdto);
        domain .setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain );
		EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "批量更新能耗", tags = {"能耗" },  notes = "批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMENConsumDTO> emenconsumdtos) {
        emenconsumService.updateBatch(emenconsumMapping.toDomain(emenconsumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "删除能耗", tags = {"能耗" },  notes = "删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emenconsum_id") String emenconsum_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "批量删除能耗", tags = {"能耗" },  notes = "批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "获取能耗", tags = {"能耗" },  notes = "获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> get(@PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取能耗草稿", tags = {"能耗" },  notes = "获取能耗草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraft(EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "检查能耗", tags = {"能耗" },  notes = "检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "保存能耗", tags = {"能耗" },  notes = "保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> save(@RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "批量保存能耗", tags = {"能耗" },  notes = "批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMENConsumDTO> emenconsumdtos) {
        emenconsumService.saveBatch(emenconsumMapping.toDomain(emenconsumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"能耗" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchDefault(EMENConsumSearchContext context) {
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"能耗" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchDefault(@RequestBody EMENConsumSearchContext context) {
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取年度设备能耗", tags = {"能耗" } ,notes = "获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEqEnByYear(EMENConsumSearchContext context) {
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询年度设备能耗", tags = {"能耗" } ,notes = "查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEqEnByYear(@RequestBody EMENConsumSearchContext context) {
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据能源建立能耗", tags = {"能耗" },  notes = "根据能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据能源批量建立能耗", tags = {"能耗" },  notes = "根据能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据能源更新能耗", tags = {"能耗" },  notes = "根据能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMEN(@PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据能源批量更新能耗", tags = {"能耗" },  notes = "根据能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据能源删除能耗", tags = {"能耗" },  notes = "根据能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMEN(@PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据能源批量删除能耗", tags = {"能耗" },  notes = "根据能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据能源获取能耗", tags = {"能耗" },  notes = "根据能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMEN(@PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据能源获取能耗草稿", tags = {"能耗" },  notes = "根据能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMEN(@PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据能源检查能耗", tags = {"能耗" },  notes = "根据能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据能源保存能耗", tags = {"能耗" },  notes = "根据能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据能源批量保存能耗", tags = {"能耗" },  notes = "根据能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据能源获取DEFAULT", tags = {"能耗" } ,notes = "根据能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMEN(@PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据能源查询DEFAULT", tags = {"能耗" } ,notes = "根据能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMEN(@PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMEN(@PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据设备档案建立能耗", tags = {"能耗" },  notes = "根据设备档案建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据设备档案批量建立能耗", tags = {"能耗" },  notes = "根据设备档案批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据设备档案更新能耗", tags = {"能耗" },  notes = "根据设备档案更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据设备档案批量更新能耗", tags = {"能耗" },  notes = "根据设备档案批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据设备档案删除能耗", tags = {"能耗" },  notes = "根据设备档案删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据设备档案批量删除能耗", tags = {"能耗" },  notes = "根据设备档案批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据设备档案获取能耗", tags = {"能耗" },  notes = "根据设备档案获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取能耗草稿", tags = {"能耗" },  notes = "根据设备档案获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查能耗", tags = {"能耗" },  notes = "根据设备档案检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据设备档案保存能耗", tags = {"能耗" },  notes = "根据设备档案保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据设备档案批量保存能耗", tags = {"能耗" },  notes = "根据设备档案批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"能耗" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"能耗" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据设备档案获取年度设备能耗", tags = {"能耗" } ,notes = "根据设备档案获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMEquip(@PathVariable("emequip_id") String emequip_id,EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据设备档案查询年度设备能耗", tags = {"能耗" } ,notes = "根据设备档案查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据物品能源建立能耗", tags = {"能耗" },  notes = "根据物品能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据物品能源批量建立能耗", tags = {"能耗" },  notes = "根据物品能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据物品能源更新能耗", tags = {"能耗" },  notes = "根据物品能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据物品能源批量更新能耗", tags = {"能耗" },  notes = "根据物品能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据物品能源删除能耗", tags = {"能耗" },  notes = "根据物品能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据物品能源批量删除能耗", tags = {"能耗" },  notes = "根据物品能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMItemEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据物品能源获取能耗", tags = {"能耗" },  notes = "根据物品能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品能源获取能耗草稿", tags = {"能耗" },  notes = "根据物品能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品能源检查能耗", tags = {"能耗" },  notes = "根据物品能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据物品能源保存能耗", tags = {"能耗" },  notes = "根据物品能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据物品能源批量保存能耗", tags = {"能耗" },  notes = "根据物品能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据物品能源获取DEFAULT", tags = {"能耗" } ,notes = "根据物品能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据物品能源查询DEFAULT", tags = {"能耗" } ,notes = "根据物品能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据物品能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据物品能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据物品能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据物品能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMItemEMEN(@PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据班组设备档案建立能耗", tags = {"能耗" },  notes = "根据班组设备档案建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立能耗", tags = {"能耗" },  notes = "根据班组设备档案批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据班组设备档案更新能耗", tags = {"能耗" },  notes = "根据班组设备档案更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新能耗", tags = {"能耗" },  notes = "根据班组设备档案批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据班组设备档案删除能耗", tags = {"能耗" },  notes = "根据班组设备档案删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除能耗", tags = {"能耗" },  notes = "根据班组设备档案批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据班组设备档案获取能耗", tags = {"能耗" },  notes = "根据班组设备档案获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取能耗草稿", tags = {"能耗" },  notes = "根据班组设备档案获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查能耗", tags = {"能耗" },  notes = "根据班组设备档案检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据班组设备档案保存能耗", tags = {"能耗" },  notes = "根据班组设备档案保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEquipid(emequip_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存能耗", tags = {"能耗" },  notes = "根据班组设备档案批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"能耗" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"能耗" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据班组设备档案获取年度设备能耗", tags = {"能耗" } ,notes = "根据班组设备档案获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据班组设备档案查询年度设备能耗", tags = {"能耗" } ,notes = "根据班组设备档案查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据服务商物品能源建立能耗", tags = {"能耗" },  notes = "根据服务商物品能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据服务商物品能源批量建立能耗", tags = {"能耗" },  notes = "根据服务商物品能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据服务商物品能源更新能耗", tags = {"能耗" },  notes = "根据服务商物品能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据服务商物品能源批量更新能耗", tags = {"能耗" },  notes = "根据服务商物品能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据服务商物品能源删除能耗", tags = {"能耗" },  notes = "根据服务商物品能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据服务商物品能源批量删除能耗", tags = {"能耗" },  notes = "根据服务商物品能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItemEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据服务商物品能源获取能耗", tags = {"能耗" },  notes = "根据服务商物品能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品能源获取能耗草稿", tags = {"能耗" },  notes = "根据服务商物品能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品能源检查能耗", tags = {"能耗" },  notes = "根据服务商物品能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据服务商物品能源保存能耗", tags = {"能耗" },  notes = "根据服务商物品能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据服务商物品能源批量保存能耗", tags = {"能耗" },  notes = "根据服务商物品能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据服务商物品能源获取DEFAULT", tags = {"能耗" } ,notes = "根据服务商物品能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据服务商物品能源查询DEFAULT", tags = {"能耗" } ,notes = "根据服务商物品能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商物品能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据服务商物品能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商物品能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据服务商物品能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMServiceEMItemEMEN(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库库位物品能源建立能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库库位物品能源批量建立能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库库位物品能源更新能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库库位物品能源批量更新能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库库位物品能源删除能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库库位物品能源批量删除能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItemEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据仓库库位物品能源获取能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品能源获取能耗草稿", tags = {"能耗" },  notes = "根据仓库库位物品能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品能源检查能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库库位物品能源保存能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库库位物品能源批量保存能耗", tags = {"能耗" },  notes = "根据仓库库位物品能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库库位物品能源获取DEFAULT", tags = {"能耗" } ,notes = "根据仓库库位物品能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库库位物品能源查询DEFAULT", tags = {"能耗" } ,notes = "根据仓库库位物品能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库库位物品能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据仓库库位物品能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库库位物品能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据仓库库位物品能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMStorePartEMItemEMEN(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库物品能源建立能耗", tags = {"能耗" },  notes = "根据仓库物品能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库物品能源批量建立能耗", tags = {"能耗" },  notes = "根据仓库物品能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库物品能源更新能耗", tags = {"能耗" },  notes = "根据仓库物品能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库物品能源批量更新能耗", tags = {"能耗" },  notes = "根据仓库物品能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库物品能源删除能耗", tags = {"能耗" },  notes = "根据仓库物品能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库物品能源批量删除能耗", tags = {"能耗" },  notes = "根据仓库物品能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItemEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据仓库物品能源获取能耗", tags = {"能耗" },  notes = "根据仓库物品能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品能源获取能耗草稿", tags = {"能耗" },  notes = "根据仓库物品能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品能源检查能耗", tags = {"能耗" },  notes = "根据仓库物品能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库物品能源保存能耗", tags = {"能耗" },  notes = "根据仓库物品能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库物品能源批量保存能耗", tags = {"能耗" },  notes = "根据仓库物品能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库物品能源获取DEFAULT", tags = {"能耗" } ,notes = "根据仓库物品能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库物品能源查询DEFAULT", tags = {"能耗" } ,notes = "根据仓库物品能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库物品能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据仓库物品能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库物品能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据仓库物品能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMStoreEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品能源建立能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums")
    public ResponseEntity<EMENConsumDTO> createByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
		emenconsumService.create(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品能源批量建立能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源批量建立能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emenconsum" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品能源更新能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> updateByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        domain.setEmenconsumid(emenconsum_id);
		emenconsumService.update(domain);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByEntities(this.emenconsumMapping.toDomain(#emenconsumdtos)),'eam-EMENConsum-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品能源批量更新能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源批量更新能耗")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
            domain.setEnid(emen_id);
        }
        emenconsumService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emenconsumService.get(#emenconsum_id),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品能源删除能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emenconsumService.remove(emenconsum_id));
    }

    @PreAuthorize("hasPermission(this.emenconsumService.getEmenconsumByIds(#ids),'eam-EMENConsum-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品能源批量删除能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源批量删除能耗")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItemEMEN(@RequestBody List<String> ids) {
        emenconsumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emenconsumMapping.toDomain(returnObject.body),'eam-EMENConsum-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品能源获取能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源获取能耗")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/{emenconsum_id}")
    public ResponseEntity<EMENConsumDTO> getByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @PathVariable("emenconsum_id") String emenconsum_id) {
        EMENConsum domain = emenconsumService.get(emenconsum_id);
        EMENConsumDTO dto = emenconsumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品能源获取能耗草稿", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源获取能耗草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/getdraft")
    public ResponseEntity<EMENConsumDTO> getDraftByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, EMENConsumDTO dto) {
        EMENConsum domain = emenconsumMapping.toDomain(dto);
        domain.setEnid(emen_id);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(emenconsumService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品能源检查能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源检查能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emenconsumService.checkKey(emenconsumMapping.toDomain(emenconsumdto)));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdto),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品能源保存能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/save")
    public ResponseEntity<EMENConsumDTO> saveByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumDTO emenconsumdto) {
        EMENConsum domain = emenconsumMapping.toDomain(emenconsumdto);
        domain.setEnid(emen_id);
        emenconsumService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emenconsumMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emenconsumMapping.toDomain(#emenconsumdtos),'eam-EMENConsum-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品能源批量保存能耗", tags = {"能耗" },  notes = "根据仓库仓库库位物品能源批量保存能耗")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody List<EMENConsumDTO> emenconsumdtos) {
        List<EMENConsum> domainlist=emenconsumMapping.toDomain(emenconsumdtos);
        for(EMENConsum domain:domainlist){
             domain.setEnid(emen_id);
        }
        emenconsumService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品能源获取DEFAULT", tags = {"能耗" } ,notes = "根据仓库仓库库位物品能源获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetchdefault")
	public ResponseEntity<List<EMENConsumDTO>> fetchEMENConsumDefaultByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
        List<EMENConsumDTO> list = emenconsumMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMENConsum-searchDefault-all') and hasPermission(#context,'eam-EMENConsum-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品能源查询DEFAULT", tags = {"能耗" } ,notes = "根据仓库仓库库位物品能源查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searchdefault")
	public ResponseEntity<Page<EMENConsumDTO>> searchEMENConsumDefaultByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<EMENConsum> domains = emenconsumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emenconsumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据仓库仓库库位物品能源获取年度设备能耗", tags = {"能耗" } ,notes = "根据仓库仓库库位物品能源获取年度设备能耗")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/fetcheqenbyyear")
	public ResponseEntity<List<Map>> fetchEMENConsumEqEnByYearByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id,EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据仓库仓库库位物品能源查询年度设备能耗", tags = {"能耗" } ,notes = "根据仓库仓库库位物品能源查询年度设备能耗")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emen/{emen_id}/emenconsums/searcheqenbyyear")
	public ResponseEntity<Page<Map>> searchEMENConsumEqEnByYearByEMStoreEMStorePartEMItemEMEN(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emen_id") String emen_id, @RequestBody EMENConsumSearchContext context) {
        context.setN_enid_eq(emen_id);
        Page<Map> domains = emenconsumService.searchEqEnByYear(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
}

