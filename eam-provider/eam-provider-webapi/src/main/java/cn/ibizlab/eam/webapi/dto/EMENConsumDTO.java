package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMENConsumDTO]
 */
@Data
@ApiModel("能耗")
public class EMENConsumDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;

    /**
     * 属性 [DEPTID]
     *
     */
    @JSONField(name = "deptid")
    @JsonProperty("deptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("部门")
    private String deptid;

    /**
     * 属性 [EMENCONSUMID]
     *
     */
    @JSONField(name = "emenconsumid")
    @JsonProperty("emenconsumid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("能耗标识")
    private String emenconsumid;

    /**
     * 属性 [PUSETYPE]
     *
     */
    @JSONField(name = "pusetype")
    @JsonProperty("pusetype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("领料分类")
    private String pusetype;

    /**
     * 属性 [VRATE]
     *
     */
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    @ApiModelProperty("倍率")
    private Double vrate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @ApiModelProperty("总金额")
    private Double amount;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [NVAL]
     *
     */
    @JSONField(name = "nval")
    @JsonProperty("nval")
    @ApiModelProperty("能耗值")
    private Double nval;

    /**
     * 属性 [LASTVAL]
     *
     */
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    @ApiModelProperty("上次记录值")
    private Double lastval;

    /**
     * 属性 [DEPTNAME]
     *
     */
    @JSONField(name = "deptname")
    @JsonProperty("deptname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("部门")
    private String deptname;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [CURVAL]
     *
     */
    @JSONField(name = "curval")
    @JsonProperty("curval")
    @ApiModelProperty("本次记录值")
    private Double curval;

    /**
     * 属性 [EMENCONSUMNAME]
     *
     */
    @JSONField(name = "emenconsumname")
    @JsonProperty("emenconsumname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("能耗名称")
    private String emenconsumname;

    /**
     * 属性 [BDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    @ApiModelProperty("上次采集时间")
    private Timestamp bdate;

    /**
     * 属性 [EDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "edate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    @ApiModelProperty("采集时间")
    private Timestamp edate;

    /**
     * 属性 [ENPRICE]
     *
     */
    @JSONField(name = "enprice")
    @JsonProperty("enprice")
    @ApiModelProperty("能源单价")
    private Double enprice;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("位置")
    private String objname;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品二级类")
    private String itemmtypename;

    /**
     * 属性 [ITEMNAME]
     *
     */
    @JSONField(name = "itemname")
    @JsonProperty("itemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品")
    private String itemname;

    /**
     * 属性 [WONAME]
     *
     */
    @JSONField(name = "woname")
    @JsonProperty("woname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单")
    private String woname;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("单位")
    private String unitname;

    /**
     * 属性 [ITEMPRICE]
     *
     */
    @JSONField(name = "itemprice")
    @JsonProperty("itemprice")
    @ApiModelProperty("能源库存单价")
    private Double itemprice;

    /**
     * 属性 [ENERGYTYPEID]
     *
     */
    @JSONField(name = "energytypeid")
    @JsonProperty("energytypeid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("能源类型")
    private String energytypeid;

    /**
     * 属性 [EQUIPCODE]
     *
     */
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipcode;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备")
    private String equipname;

    /**
     * 属性 [ENNAME]
     *
     */
    @JSONField(name = "enname")
    @JsonProperty("enname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("能源")
    private String enname;

    /**
     * 属性 [SNAME]
     *
     */
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备统计归类")
    private String sname;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品类型")
    private String itemtypeid;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品大类")
    private String itembtypeid;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品大类")
    private String itembtypename;

    /**
     * 属性 [ITEMID]
     *
     */
    @JSONField(name = "itemid")
    @JsonProperty("itemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品")
    private String itemid;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品二级类")
    private String itemmtypeid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("位置")
    private String objid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipid;

    /**
     * 属性 [WOID]
     *
     */
    @JSONField(name = "woid")
    @JsonProperty("woid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("工单")
    private String woid;

    /**
     * 属性 [ENID]
     *
     */
    @JSONField(name = "enid")
    @JsonProperty("enid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("能源")
    private String enid;


    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [DEPTID]
     */
    public void setDeptid(String  deptid){
        this.deptid = deptid ;
        this.modify("deptid",deptid);
    }

    /**
     * 设置 [PUSETYPE]
     */
    public void setPusetype(String  pusetype){
        this.pusetype = pusetype ;
        this.modify("pusetype",pusetype);
    }

    /**
     * 设置 [VRATE]
     */
    public void setVrate(Double  vrate){
        this.vrate = vrate ;
        this.modify("vrate",vrate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [NVAL]
     */
    public void setNval(Double  nval){
        this.nval = nval ;
        this.modify("nval",nval);
    }

    /**
     * 设置 [LASTVAL]
     */
    public void setLastval(Double  lastval){
        this.lastval = lastval ;
        this.modify("lastval",lastval);
    }

    /**
     * 设置 [DEPTNAME]
     */
    public void setDeptname(String  deptname){
        this.deptname = deptname ;
        this.modify("deptname",deptname);
    }

    /**
     * 设置 [CURVAL]
     */
    public void setCurval(Double  curval){
        this.curval = curval ;
        this.modify("curval",curval);
    }

    /**
     * 设置 [EMENCONSUMNAME]
     */
    public void setEmenconsumname(String  emenconsumname){
        this.emenconsumname = emenconsumname ;
        this.modify("emenconsumname",emenconsumname);
    }

    /**
     * 设置 [BDATE]
     */
    public void setBdate(Timestamp  bdate){
        this.bdate = bdate ;
        this.modify("bdate",bdate);
    }

    /**
     * 设置 [EDATE]
     */
    public void setEdate(Timestamp  edate){
        this.edate = edate ;
        this.modify("edate",edate);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [WOID]
     */
    public void setWoid(String  woid){
        this.woid = woid ;
        this.modify("woid",woid);
    }

    /**
     * 设置 [ENID]
     */
    public void setEnid(String  enid){
        this.enid = enid ;
        this.modify("enid",enid);
    }


}


