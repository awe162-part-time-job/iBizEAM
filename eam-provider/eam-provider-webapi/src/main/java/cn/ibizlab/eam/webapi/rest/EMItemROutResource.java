package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;
import cn.ibizlab.eam.core.eam_core.service.IEMItemROutService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemROutSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"退货单" })
@RestController("WebApi-emitemrout")
@RequestMapping("")
public class EMItemROutResource {

    @Autowired
    public IEMItemROutService emitemroutService;

    @Autowired
    @Lazy
    public EMItemROutMapping emitemroutMapping;

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "新建退货单", tags = {"退货单" },  notes = "新建退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts")
    public ResponseEntity<EMItemROutDTO> create(@Validated @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "批量新建退货单", tags = {"退货单" },  notes = "批量新建退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemROutDTO> emitemroutdtos) {
        emitemroutService.createBatch(emitemroutMapping.toDomain(emitemroutdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "更新退货单", tags = {"退货单" },  notes = "更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> update(@PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
		EMItemROut domain  = emitemroutMapping.toDomain(emitemroutdto);
        domain .setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain );
		EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "批量更新退货单", tags = {"退货单" },  notes = "批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemROutDTO> emitemroutdtos) {
        emitemroutService.updateBatch(emitemroutMapping.toDomain(emitemroutdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "删除退货单", tags = {"退货单" },  notes = "删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemrout_id") String emitemrout_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "批量删除退货单", tags = {"退货单" },  notes = "批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "获取退货单", tags = {"退货单" },  notes = "获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> get(@PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取退货单草稿", tags = {"退货单" },  notes = "获取退货单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraft(EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "检查退货单", tags = {"退货单" },  notes = "检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "确认", tags = {"退货单" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirm(@PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setEmitemroutid(emitemrout_id);
        domain = emitemroutService.confirm(domain);
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "批量处理[确认]", tags = {"退货单" },  notes = "批量处理[确认]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "入库单信息更新", tags = {"退货单" },  notes = "入库单信息更新")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRID(@PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setEmitemroutid(emitemrout_id);
        domain = emitemroutService.formUpdateByRID(domain);
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"退货单" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejected(@PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setEmitemroutid(emitemrout_id);
        domain = emitemroutService.rejected(domain);
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "保存退货单", tags = {"退货单" },  notes = "保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> save(@RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "批量保存退货单", tags = {"退货单" },  notes = "批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemROutDTO> emitemroutdtos) {
        emitemroutService.saveBatch(emitemroutMapping.toDomain(emitemroutdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "提交", tags = {"退货单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submit(@PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setEmitemroutid(emitemrout_id);
        domain = emitemroutService.submit(domain);
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "获取已确认", tags = {"退货单" } ,notes = "获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchConfirmed(EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "查询已确认", tags = {"退货单" } ,notes = "查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchConfirmed(@RequestBody EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"退货单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchDefault(EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"退货单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchDefault(@RequestBody EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "获取草稿", tags = {"退货单" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchDraft(EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "查询草稿", tags = {"退货单" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchDraft(@RequestBody EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "获取待确认", tags = {"退货单" } ,notes = "获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchToConfirm(EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "查询待确认", tags = {"退货单" } ,notes = "查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchToConfirm(@RequestBody EMItemROutSearchContext context) {
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据物品建立退货单", tags = {"退货单" },  notes = "根据物品建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts")
    public ResponseEntity<EMItemROutDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据物品批量建立退货单", tags = {"退货单" },  notes = "根据物品批量建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据物品更新退货单", tags = {"退货单" },  notes = "根据物品更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain.setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据物品批量更新退货单", tags = {"退货单" },  notes = "根据物品批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据物品删除退货单", tags = {"退货单" },  notes = "根据物品删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据物品批量删除退货单", tags = {"退货单" },  notes = "根据物品批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "根据物品获取退货单", tags = {"退货单" },  notes = "根据物品获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取退货单草稿", tags = {"退货单" },  notes = "根据物品获取退货单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查退货单", tags = {"退货单" },  notes = "根据物品检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "根据物品退货单", tags = {"退货单" },  notes = "根据物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.confirm(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @ApiOperation(value = "批量处理[根据物品退货单]", tags = {"退货单" },  notes = "批量处理[根据物品退货单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "根据物品退货单", tags = {"退货单" },  notes = "根据物品退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRIDByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.formUpdateByRID(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "根据物品退货单", tags = {"退货单" },  notes = "根据物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejectedByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.rejected(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据物品保存退货单", tags = {"退货单" },  notes = "根据物品保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据物品批量保存退货单", tags = {"退货单" },  notes = "根据物品批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemroutService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "根据物品退货单", tags = {"退货单" },  notes = "根据物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submitByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.submit(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品获取已确认", tags = {"退货单" } ,notes = "根据物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutConfirmedByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品查询已确认", tags = {"退货单" } ,notes = "根据物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutConfirmedByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"退货单" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"退货单" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品获取草稿", tags = {"退货单" } ,notes = "根据物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDraftByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品查询草稿", tags = {"退货单" } ,notes = "根据物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDraftByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品获取待确认", tags = {"退货单" } ,notes = "根据物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutToConfirmByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据物品查询待确认", tags = {"退货单" } ,notes = "根据物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutToConfirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据服务商物品建立退货单", tags = {"退货单" },  notes = "根据服务商物品建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts")
    public ResponseEntity<EMItemROutDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据服务商物品批量建立退货单", tags = {"退货单" },  notes = "根据服务商物品批量建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据服务商物品更新退货单", tags = {"退货单" },  notes = "根据服务商物品更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain.setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据服务商物品批量更新退货单", tags = {"退货单" },  notes = "根据服务商物品批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据服务商物品删除退货单", tags = {"退货单" },  notes = "根据服务商物品删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除退货单", tags = {"退货单" },  notes = "根据服务商物品批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "根据服务商物品获取退货单", tags = {"退货单" },  notes = "根据服务商物品获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取退货单草稿", tags = {"退货单" },  notes = "根据服务商物品获取退货单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查退货单", tags = {"退货单" },  notes = "根据服务商物品检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "根据服务商物品退货单", tags = {"退货单" },  notes = "根据服务商物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.confirm(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品退货单]", tags = {"退货单" },  notes = "批量处理[根据服务商物品退货单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "根据服务商物品退货单", tags = {"退货单" },  notes = "根据服务商物品退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRIDByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.formUpdateByRID(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "根据服务商物品退货单", tags = {"退货单" },  notes = "根据服务商物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejectedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.rejected(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据服务商物品保存退货单", tags = {"退货单" },  notes = "根据服务商物品保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据服务商物品批量保存退货单", tags = {"退货单" },  notes = "根据服务商物品批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemroutService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "根据服务商物品退货单", tags = {"退货单" },  notes = "根据服务商物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submitByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.submit(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品获取已确认", tags = {"退货单" } ,notes = "根据服务商物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutConfirmedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品查询已确认", tags = {"退货单" } ,notes = "根据服务商物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutConfirmedByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"退货单" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"退货单" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品获取草稿", tags = {"退货单" } ,notes = "根据服务商物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品查询草稿", tags = {"退货单" } ,notes = "根据服务商物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品获取待确认", tags = {"退货单" } ,notes = "根据服务商物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutToConfirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据服务商物品查询待确认", tags = {"退货单" } ,notes = "根据服务商物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutToConfirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库库位物品建立退货单", tags = {"退货单" },  notes = "根据仓库库位物品建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts")
    public ResponseEntity<EMItemROutDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立退货单", tags = {"退货单" },  notes = "根据仓库库位物品批量建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库库位物品更新退货单", tags = {"退货单" },  notes = "根据仓库库位物品更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain.setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新退货单", tags = {"退货单" },  notes = "根据仓库库位物品批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除退货单", tags = {"退货单" },  notes = "根据仓库库位物品删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除退货单", tags = {"退货单" },  notes = "根据仓库库位物品批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "根据仓库库位物品获取退货单", tags = {"退货单" },  notes = "根据仓库库位物品获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取退货单草稿", tags = {"退货单" },  notes = "根据仓库库位物品获取退货单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查退货单", tags = {"退货单" },  notes = "根据仓库库位物品检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.confirm(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品退货单]", tags = {"退货单" },  notes = "批量处理[根据仓库库位物品退货单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "根据仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRIDByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.formUpdateByRID(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "根据仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejectedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.rejected(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库库位物品保存退货单", tags = {"退货单" },  notes = "根据仓库库位物品保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存退货单", tags = {"退货单" },  notes = "根据仓库库位物品批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemroutService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "根据仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submitByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.submit(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已确认", tags = {"退货单" } ,notes = "根据仓库库位物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutConfirmedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已确认", tags = {"退货单" } ,notes = "根据仓库库位物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutConfirmedByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"退货单" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"退货单" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品获取草稿", tags = {"退货单" } ,notes = "根据仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品查询草稿", tags = {"退货单" } ,notes = "根据仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待确认", tags = {"退货单" } ,notes = "根据仓库库位物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutToConfirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待确认", tags = {"退货单" } ,notes = "根据仓库库位物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutToConfirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库物品建立退货单", tags = {"退货单" },  notes = "根据仓库物品建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts")
    public ResponseEntity<EMItemROutDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库物品批量建立退货单", tags = {"退货单" },  notes = "根据仓库物品批量建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库物品更新退货单", tags = {"退货单" },  notes = "根据仓库物品更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain.setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库物品批量更新退货单", tags = {"退货单" },  notes = "根据仓库物品批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库物品删除退货单", tags = {"退货单" },  notes = "根据仓库物品删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除退货单", tags = {"退货单" },  notes = "根据仓库物品批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "根据仓库物品获取退货单", tags = {"退货单" },  notes = "根据仓库物品获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取退货单草稿", tags = {"退货单" },  notes = "根据仓库物品获取退货单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查退货单", tags = {"退货单" },  notes = "根据仓库物品检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "根据仓库物品退货单", tags = {"退货单" },  notes = "根据仓库物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.confirm(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品退货单]", tags = {"退货单" },  notes = "批量处理[根据仓库物品退货单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "根据仓库物品退货单", tags = {"退货单" },  notes = "根据仓库物品退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRIDByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.formUpdateByRID(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "根据仓库物品退货单", tags = {"退货单" },  notes = "根据仓库物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejectedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.rejected(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库物品保存退货单", tags = {"退货单" },  notes = "根据仓库物品保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库物品批量保存退货单", tags = {"退货单" },  notes = "根据仓库物品批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemroutService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "根据仓库物品退货单", tags = {"退货单" },  notes = "根据仓库物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submitByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.submit(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品获取已确认", tags = {"退货单" } ,notes = "根据仓库物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutConfirmedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品查询已确认", tags = {"退货单" } ,notes = "根据仓库物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutConfirmedByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"退货单" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"退货单" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品获取草稿", tags = {"退货单" } ,notes = "根据仓库物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品查询草稿", tags = {"退货单" } ,notes = "根据仓库物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品获取待确认", tags = {"退货单" } ,notes = "根据仓库物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutToConfirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库物品查询待确认", tags = {"退货单" } ,notes = "根据仓库物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutToConfirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts")
    public ResponseEntity<EMItemROutDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
		emitemroutService.create(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品批量建立退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrout" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain.setEmitemroutid(emitemrout_id);
		emitemroutService.update(domain);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByEntities(this.emitemroutMapping.toDomain(#emitemroutdtos)),'eam-EMItemROut-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品批量更新退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemroutService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemroutService.get(#emitemrout_id),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemroutService.remove(emitemrout_id));
    }

    @PreAuthorize("hasPermission(this.emitemroutService.getEmitemroutByIds(#ids),'eam-EMItemROut-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品批量删除退货单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemroutService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemroutMapping.toDomain(returnObject.body),'eam-EMItemROut-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品获取退货单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}")
    public ResponseEntity<EMItemROutDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id) {
        EMItemROut domain = emitemroutService.get(emitemrout_id);
        EMItemROutDTO dto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取退货单草稿", tags = {"退货单" },  notes = "根据仓库仓库库位物品获取退货单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/getdraft")
    public ResponseEntity<EMItemROutDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemROutDTO dto) {
        EMItemROut domain = emitemroutMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(emitemroutService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品检查退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemroutService.checkKey(emitemroutMapping.toDomain(emitemroutdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/confirm")
    public ResponseEntity<EMItemROutDTO> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.confirm(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品退货单]", tags = {"退货单" },  notes = "批量处理[根据仓库仓库库位物品退货单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domains = emitemroutMapping.toDomain(emitemroutdtos);
        boolean result = emitemroutService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-FormUpdateByRID-all')")
    @ApiOperation(value = "根据仓库仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/formupdatebyrid")
    public ResponseEntity<EMItemROutDTO> formUpdateByRIDByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.formUpdateByRID(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Rejected-all')")
    @ApiOperation(value = "根据仓库仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/rejected")
    public ResponseEntity<EMItemROutDTO> rejectedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.rejected(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdto),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/save")
    public ResponseEntity<EMItemROutDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        emitemroutService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemroutMapping.toDomain(#emitemroutdtos),'eam-EMItemROut-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品批量保存退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemROutDTO> emitemroutdtos) {
        List<EMItemROut> domainlist=emitemroutMapping.toDomain(emitemroutdtos);
        for(EMItemROut domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemroutService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-Submit-all')")
    @ApiOperation(value = "根据仓库仓库库位物品退货单", tags = {"退货单" },  notes = "根据仓库仓库库位物品退货单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/{emitemrout_id}/submit")
    public ResponseEntity<EMItemROutDTO> submitByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrout_id") String emitemrout_id, @RequestBody EMItemROutDTO emitemroutdto) {
        EMItemROut domain = emitemroutMapping.toDomain(emitemroutdto);
        domain.setItemid(emitem_id);
        domain = emitemroutService.submit(domain) ;
        emitemroutdto = emitemroutMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemroutdto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已确认", tags = {"退货单" } ,notes = "根据仓库仓库库位物品获取已确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchconfirmed")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutConfirmedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchConfirmed-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已确认", tags = {"退货单" } ,notes = "根据仓库仓库库位物品查询已确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchconfirmed")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutConfirmedByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"退货单" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchdefault")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDefault-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"退货单" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchdefault")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取草稿", tags = {"退货单" } ,notes = "根据仓库仓库库位物品获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchdraft")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchDraft-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询草稿", tags = {"退货单" } ,notes = "根据仓库仓库库位物品查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchdraft")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待确认", tags = {"退货单" } ,notes = "根据仓库仓库库位物品获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/fetchtoconfirm")
	public ResponseEntity<List<EMItemROutDTO>> fetchEMItemROutToConfirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
        List<EMItemROutDTO> list = emitemroutMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemROut-searchToConfirm-all') and hasPermission(#context,'eam-EMItemROut-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待确认", tags = {"退货单" } ,notes = "根据仓库仓库库位物品查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrouts/searchtoconfirm")
	public ResponseEntity<Page<EMItemROutDTO>> searchEMItemROutToConfirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemROutSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemROut> domains = emitemroutService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemroutMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

