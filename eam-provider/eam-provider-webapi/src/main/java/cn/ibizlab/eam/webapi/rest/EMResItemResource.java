package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMResItem;
import cn.ibizlab.eam.core.eam_core.service.IEMResItemService;
import cn.ibizlab.eam.core.eam_core.filter.EMResItemSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物品资源" })
@RestController("WebApi-emresitem")
@RequestMapping("")
public class EMResItemResource {

    @Autowired
    public IEMResItemService emresitemService;

    @Autowired
    @Lazy
    public EMResItemMapping emresitemMapping;

    @PreAuthorize("hasPermission(this.emresitemMapping.toDomain(#emresitemdto),'eam-EMResItem-Create')")
    @ApiOperation(value = "新建物品资源", tags = {"物品资源" },  notes = "新建物品资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresitems")
    public ResponseEntity<EMResItemDTO> create(@Validated @RequestBody EMResItemDTO emresitemdto) {
        EMResItem domain = emresitemMapping.toDomain(emresitemdto);
		emresitemService.create(domain);
        EMResItemDTO dto = emresitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresitemMapping.toDomain(#emresitemdtos),'eam-EMResItem-Create')")
    @ApiOperation(value = "批量新建物品资源", tags = {"物品资源" },  notes = "批量新建物品资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresitems/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMResItemDTO> emresitemdtos) {
        emresitemService.createBatch(emresitemMapping.toDomain(emresitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emresitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emresitemService.get(#emresitem_id),'eam-EMResItem-Update')")
    @ApiOperation(value = "更新物品资源", tags = {"物品资源" },  notes = "更新物品资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresitems/{emresitem_id}")
    public ResponseEntity<EMResItemDTO> update(@PathVariable("emresitem_id") String emresitem_id, @RequestBody EMResItemDTO emresitemdto) {
		EMResItem domain  = emresitemMapping.toDomain(emresitemdto);
        domain .setEmresitemid(emresitem_id);
		emresitemService.update(domain );
		EMResItemDTO dto = emresitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresitemService.getEmresitemByEntities(this.emresitemMapping.toDomain(#emresitemdtos)),'eam-EMResItem-Update')")
    @ApiOperation(value = "批量更新物品资源", tags = {"物品资源" },  notes = "批量更新物品资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresitems/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMResItemDTO> emresitemdtos) {
        emresitemService.updateBatch(emresitemMapping.toDomain(emresitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emresitemService.get(#emresitem_id),'eam-EMResItem-Remove')")
    @ApiOperation(value = "删除物品资源", tags = {"物品资源" },  notes = "删除物品资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresitems/{emresitem_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emresitem_id") String emresitem_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emresitemService.remove(emresitem_id));
    }

    @PreAuthorize("hasPermission(this.emresitemService.getEmresitemByIds(#ids),'eam-EMResItem-Remove')")
    @ApiOperation(value = "批量删除物品资源", tags = {"物品资源" },  notes = "批量删除物品资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresitems/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emresitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emresitemMapping.toDomain(returnObject.body),'eam-EMResItem-Get')")
    @ApiOperation(value = "获取物品资源", tags = {"物品资源" },  notes = "获取物品资源")
	@RequestMapping(method = RequestMethod.GET, value = "/emresitems/{emresitem_id}")
    public ResponseEntity<EMResItemDTO> get(@PathVariable("emresitem_id") String emresitem_id) {
        EMResItem domain = emresitemService.get(emresitem_id);
        EMResItemDTO dto = emresitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物品资源草稿", tags = {"物品资源" },  notes = "获取物品资源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emresitems/getdraft")
    public ResponseEntity<EMResItemDTO> getDraft(EMResItemDTO dto) {
        EMResItem domain = emresitemMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emresitemMapping.toDto(emresitemService.getDraft(domain)));
    }

    @ApiOperation(value = "检查物品资源", tags = {"物品资源" },  notes = "检查物品资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresitems/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMResItemDTO emresitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emresitemService.checkKey(emresitemMapping.toDomain(emresitemdto)));
    }

    @PreAuthorize("hasPermission(this.emresitemMapping.toDomain(#emresitemdto),'eam-EMResItem-Save')")
    @ApiOperation(value = "保存物品资源", tags = {"物品资源" },  notes = "保存物品资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresitems/save")
    public ResponseEntity<EMResItemDTO> save(@RequestBody EMResItemDTO emresitemdto) {
        EMResItem domain = emresitemMapping.toDomain(emresitemdto);
        emresitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emresitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emresitemMapping.toDomain(#emresitemdtos),'eam-EMResItem-Save')")
    @ApiOperation(value = "批量保存物品资源", tags = {"物品资源" },  notes = "批量保存物品资源")
	@RequestMapping(method = RequestMethod.POST, value = "/emresitems/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMResItemDTO> emresitemdtos) {
        emresitemService.saveBatch(emresitemMapping.toDomain(emresitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取设备-物品消耗金额", tags = {"物品资源" } ,notes = "获取设备-物品消耗金额")
    @RequestMapping(method= RequestMethod.GET , value="/emresitems/fetchamountbytypebyeq")
	public ResponseEntity<List<Map>> fetchAmountByTypeByEQ(EMResItemSearchContext context) {
        Page<Map> domains = emresitemService.searchAmountByTypeByEQ(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询设备-物品消耗金额", tags = {"物品资源" } ,notes = "查询设备-物品消耗金额")
    @RequestMapping(method= RequestMethod.POST , value="/emresitems/searchamountbytypebyeq")
	public ResponseEntity<Page<Map>> searchAmountByTypeByEQ(@RequestBody EMResItemSearchContext context) {
        Page<Map> domains = emresitemService.searchAmountByTypeByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchDefault-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"物品资源" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emresitems/fetchdefault")
	public ResponseEntity<List<EMResItemDTO>> fetchDefault(EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchDefault(context) ;
        List<EMResItemDTO> list = emresitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchDefault-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"物品资源" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emresitems/searchdefault")
	public ResponseEntity<Page<EMResItemDTO>> searchDefault(@RequestBody EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchUsedByEQ-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "获取物品最新使用记录TOP10", tags = {"物品资源" } ,notes = "获取物品最新使用记录TOP10")
    @RequestMapping(method= RequestMethod.GET , value="/emresitems/fetchusedbyeq")
	public ResponseEntity<List<EMResItemDTO>> fetchUsedByEQ(EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchUsedByEQ(context) ;
        List<EMResItemDTO> list = emresitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchUsedByEQ-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "查询物品最新使用记录TOP10", tags = {"物品资源" } ,notes = "查询物品最新使用记录TOP10")
    @RequestMapping(method= RequestMethod.POST , value="/emresitems/searchusedbyeq")
	public ResponseEntity<Page<EMResItemDTO>> searchUsedByEQ(@RequestBody EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchUsedByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchUsedByItem-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "获取物品最新使用记录TOP10", tags = {"物品资源" } ,notes = "获取物品最新使用记录TOP10")
    @RequestMapping(method= RequestMethod.GET , value="/emresitems/fetchusedbyitem")
	public ResponseEntity<List<EMResItemDTO>> fetchUsedByItem(EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchUsedByItem(context) ;
        List<EMResItemDTO> list = emresitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResItem-searchUsedByItem-all') and hasPermission(#context,'eam-EMResItem-Get')")
	@ApiOperation(value = "查询物品最新使用记录TOP10", tags = {"物品资源" } ,notes = "查询物品最新使用记录TOP10")
    @RequestMapping(method= RequestMethod.POST , value="/emresitems/searchusedbyitem")
	public ResponseEntity<Page<EMResItemDTO>> searchUsedByItem(@RequestBody EMResItemSearchContext context) {
        Page<EMResItem> domains = emresitemService.searchUsedByItem(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

