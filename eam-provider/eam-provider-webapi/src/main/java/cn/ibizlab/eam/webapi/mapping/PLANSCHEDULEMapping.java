package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import cn.ibizlab.eam.webapi.dto.PLANSCHEDULEDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPLANSCHEDULEMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PLANSCHEDULEMapping extends MappingBase<PLANSCHEDULEDTO, PLANSCHEDULE> {


}

