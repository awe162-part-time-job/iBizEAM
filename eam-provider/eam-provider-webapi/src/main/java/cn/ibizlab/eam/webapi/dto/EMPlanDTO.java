package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMPlanDTO]
 */
@Data
@ApiModel("计划")
public class EMPlanDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [EMPLANID]
     *
     */
    @JSONField(name = "emplanid")
    @JsonProperty("emplanid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("计划编号")
    private String emplanid;

    /**
     * 属性 [MDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "mdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    @ApiModelProperty("制定时间")
    private Timestamp mdate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("预算(￥)")
    private String prefee;

    /**
     * 属性 [PLANDESC]
     *
     */
    @JSONField(name = "plandesc")
    @JsonProperty("plandesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("计划内容")
    private String plandesc;

    /**
     * 属性 [MTFLAG]
     *
     */
    @JSONField(name = "mtflag")
    @JsonProperty("mtflag")
    @ApiModelProperty("多任务?")
    private Integer mtflag;

    /**
     * 属性 [PLANCVL]
     *
     */
    @JSONField(name = "plancvl")
    @JsonProperty("plancvl")
    @ApiModelProperty("计划周期(天)")
    private Double plancvl;

    /**
     * 属性 [EMPLANNAME]
     *
     */
    @JSONField(name = "emplanname")
    @JsonProperty("emplanname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划名称")
    private String emplanname;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("生成工单种类")
    private String emwotype;

    /**
     * 属性 [PLANTYPE]
     *
     */
    @JSONField(name = "plantype")
    @JsonProperty("plantype")
    @NotBlank(message = "[计划类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划类型")
    private String plantype;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("详细内容")
    private String content;

    /**
     * 属性 [PLANINFO]
     *
     */
    @JSONField(name = "planinfo")
    @JsonProperty("planinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划信息")
    private String planinfo;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("接收人")
    private String recvpersonid;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("归档")
    private String archive;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;

    /**
     * 属性 [PLANSTATE]
     *
     */
    @JSONField(name = "planstate")
    @JsonProperty("planstate")
    @NotBlank(message = "[计划状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划状态")
    private String planstate;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("接收人")
    private String recvpersonname;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("测点")
    private String dpname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("总帐科目")
    private String acclassname;

    /**
     * 属性 [DPTYPE]
     *
     */
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("测点类型")
    private String dptype;

    /**
     * 属性 [PLANTEMPLNAME]
     *
     */
    @JSONField(name = "plantemplname")
    @JsonProperty("plantemplname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("计划模板")
    private String plantemplname;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备")
    private String equipname;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("位置")
    private String objname;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任班组")
    private String rteamname;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("服务商")
    private String rservicename;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("服务商")
    private String rserviceid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("位置")
    private String objid;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("测点")
    private String dpid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("总帐科目")
    private String acclassid;

    /**
     * 属性 [PLANTEMPLID]
     *
     */
    @JSONField(name = "plantemplid")
    @JsonProperty("plantemplid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("计划模板")
    private String plantemplid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任班组")
    private String rteamid;

    /**
     * 属性 [CRON]
     *
     */
    @JSONField(name = "cron")
    @JsonProperty("cron")
    @NotBlank(message = "[任务时刻]不允许为空!")
    @Size(min = 0, max = 50, message = "内容长度必须小于等于[50]")
    @ApiModelProperty("任务时刻")
    private String cron;

    /**
     * 属性 [MPERSONID]
     *
     */
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("制定人")
    private String mpersonid;

    /**
     * 属性 [MPERSONNAME]
     *
     */
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("制定人")
    private String mpersonname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任人")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任人")
    private String rempname;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任部门")
    private String rdeptid;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任部门")
    private String rdeptname;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [MDATE]
     */
    public void setMdate(Timestamp  mdate){
        this.mdate = mdate ;
        this.modify("mdate",mdate);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(String  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [PLANDESC]
     */
    public void setPlandesc(String  plandesc){
        this.plandesc = plandesc ;
        this.modify("plandesc",plandesc);
    }

    /**
     * 设置 [MTFLAG]
     */
    public void setMtflag(Integer  mtflag){
        this.mtflag = mtflag ;
        this.modify("mtflag",mtflag);
    }

    /**
     * 设置 [PLANCVL]
     */
    public void setPlancvl(Double  plancvl){
        this.plancvl = plancvl ;
        this.modify("plancvl",plancvl);
    }

    /**
     * 设置 [EMPLANNAME]
     */
    public void setEmplanname(String  emplanname){
        this.emplanname = emplanname ;
        this.modify("emplanname",emplanname);
    }

    /**
     * 设置 [EMWOTYPE]
     */
    public void setEmwotype(String  emwotype){
        this.emwotype = emwotype ;
        this.modify("emwotype",emwotype);
    }

    /**
     * 设置 [PLANTYPE]
     */
    public void setPlantype(String  plantype){
        this.plantype = plantype ;
        this.modify("plantype",plantype);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [PLANSTATE]
     */
    public void setPlanstate(String  planstate){
        this.planstate = planstate ;
        this.modify("planstate",planstate);
    }

    /**
     * 设置 [RECVPERSONNAME]
     */
    public void setRecvpersonname(String  recvpersonname){
        this.recvpersonname = recvpersonname ;
        this.modify("recvpersonname",recvpersonname);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [PLANTEMPLID]
     */
    public void setPlantemplid(String  plantemplid){
        this.plantemplid = plantemplid ;
        this.modify("plantemplid",plantemplid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [CRON]
     */
    public void setCron(String  cron){
        this.cron = cron ;
        this.modify("cron",cron);
    }

    /**
     * 设置 [MPERSONID]
     */
    public void setMpersonid(String  mpersonid){
        this.mpersonid = mpersonid ;
        this.modify("mpersonid",mpersonid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }


}


