package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMX;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMXService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMXSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备停用明细考核" })
@RestController("WebApi-emassessmx")
@RequestMapping("")
public class EMAssessMXResource {

    @Autowired
    public IEMAssessMXService emassessmxService;

    @Autowired
    @Lazy
    public EMAssessMXMapping emassessmxMapping;

    @PreAuthorize("hasPermission(this.emassessmxMapping.toDomain(#emassessmxdto),'eam-EMAssessMX-Create')")
    @ApiOperation(value = "新建设备停用明细考核", tags = {"设备停用明细考核" },  notes = "新建设备停用明细考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmxes")
    public ResponseEntity<EMAssessMXDTO> create(@Validated @RequestBody EMAssessMXDTO emassessmxdto) {
        EMAssessMX domain = emassessmxMapping.toDomain(emassessmxdto);
		emassessmxService.create(domain);
        EMAssessMXDTO dto = emassessmxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmxMapping.toDomain(#emassessmxdtos),'eam-EMAssessMX-Create')")
    @ApiOperation(value = "批量新建设备停用明细考核", tags = {"设备停用明细考核" },  notes = "批量新建设备停用明细考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssessMXDTO> emassessmxdtos) {
        emassessmxService.createBatch(emassessmxMapping.toDomain(emassessmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassessmx" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassessmxService.get(#emassessmx_id),'eam-EMAssessMX-Update')")
    @ApiOperation(value = "更新设备停用明细考核", tags = {"设备停用明细考核" },  notes = "更新设备停用明细考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessmxes/{emassessmx_id}")
    public ResponseEntity<EMAssessMXDTO> update(@PathVariable("emassessmx_id") String emassessmx_id, @RequestBody EMAssessMXDTO emassessmxdto) {
		EMAssessMX domain  = emassessmxMapping.toDomain(emassessmxdto);
        domain .setEmassessmxid(emassessmx_id);
		emassessmxService.update(domain );
		EMAssessMXDTO dto = emassessmxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmxService.getEmassessmxByEntities(this.emassessmxMapping.toDomain(#emassessmxdtos)),'eam-EMAssessMX-Update')")
    @ApiOperation(value = "批量更新设备停用明细考核", tags = {"设备停用明细考核" },  notes = "批量更新设备停用明细考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessmxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssessMXDTO> emassessmxdtos) {
        emassessmxService.updateBatch(emassessmxMapping.toDomain(emassessmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassessmxService.get(#emassessmx_id),'eam-EMAssessMX-Remove')")
    @ApiOperation(value = "删除设备停用明细考核", tags = {"设备停用明细考核" },  notes = "删除设备停用明细考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessmxes/{emassessmx_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassessmx_id") String emassessmx_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassessmxService.remove(emassessmx_id));
    }

    @PreAuthorize("hasPermission(this.emassessmxService.getEmassessmxByIds(#ids),'eam-EMAssessMX-Remove')")
    @ApiOperation(value = "批量删除设备停用明细考核", tags = {"设备停用明细考核" },  notes = "批量删除设备停用明细考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessmxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassessmxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassessmxMapping.toDomain(returnObject.body),'eam-EMAssessMX-Get')")
    @ApiOperation(value = "获取设备停用明细考核", tags = {"设备停用明细考核" },  notes = "获取设备停用明细考核")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessmxes/{emassessmx_id}")
    public ResponseEntity<EMAssessMXDTO> get(@PathVariable("emassessmx_id") String emassessmx_id) {
        EMAssessMX domain = emassessmxService.get(emassessmx_id);
        EMAssessMXDTO dto = emassessmxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备停用明细考核草稿", tags = {"设备停用明细考核" },  notes = "获取设备停用明细考核草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessmxes/getdraft")
    public ResponseEntity<EMAssessMXDTO> getDraft(EMAssessMXDTO dto) {
        EMAssessMX domain = emassessmxMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmxMapping.toDto(emassessmxService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备停用明细考核", tags = {"设备停用明细考核" },  notes = "检查设备停用明细考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssessMXDTO emassessmxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassessmxService.checkKey(emassessmxMapping.toDomain(emassessmxdto)));
    }

    @PreAuthorize("hasPermission(this.emassessmxMapping.toDomain(#emassessmxdto),'eam-EMAssessMX-Save')")
    @ApiOperation(value = "保存设备停用明细考核", tags = {"设备停用明细考核" },  notes = "保存设备停用明细考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmxes/save")
    public ResponseEntity<EMAssessMXDTO> save(@RequestBody EMAssessMXDTO emassessmxdto) {
        EMAssessMX domain = emassessmxMapping.toDomain(emassessmxdto);
        emassessmxService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmxMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassessmxMapping.toDomain(#emassessmxdtos),'eam-EMAssessMX-Save')")
    @ApiOperation(value = "批量保存设备停用明细考核", tags = {"设备停用明细考核" },  notes = "批量保存设备停用明细考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessmxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssessMXDTO> emassessmxdtos) {
        emassessmxService.saveBatch(emassessmxMapping.toDomain(emassessmxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMX-searchDefault-all') and hasPermission(#context,'eam-EMAssessMX-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备停用明细考核" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassessmxes/fetchdefault")
	public ResponseEntity<List<EMAssessMXDTO>> fetchDefault(EMAssessMXSearchContext context) {
        Page<EMAssessMX> domains = emassessmxService.searchDefault(context) ;
        List<EMAssessMXDTO> list = emassessmxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMX-searchDefault-all') and hasPermission(#context,'eam-EMAssessMX-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备停用明细考核" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassessmxes/searchdefault")
	public ResponseEntity<Page<EMAssessMXDTO>> searchDefault(@RequestBody EMAssessMXSearchContext context) {
        Page<EMAssessMX> domains = emassessmxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassessmxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

