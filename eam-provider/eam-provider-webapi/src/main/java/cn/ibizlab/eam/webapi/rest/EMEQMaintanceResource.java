package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQMaintance;
import cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQMaintanceSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"抢修记录" })
@RestController("WebApi-emeqmaintance")
@RequestMapping("")
public class EMEQMaintanceResource {

    @Autowired
    public IEMEQMaintanceService emeqmaintanceService;

    @Autowired
    @Lazy
    public EMEQMaintanceMapping emeqmaintanceMapping;

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "新建抢修记录", tags = {"抢修记录" },  notes = "新建抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmaintances")
    public ResponseEntity<EMEQMaintanceDTO> create(@Validated @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
		emeqmaintanceService.create(domain);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "批量新建抢修记录", tags = {"抢修记录" },  notes = "批量新建抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmaintances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        emeqmaintanceService.createBatch(emeqmaintanceMapping.toDomain(emeqmaintancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmaintance" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "更新抢修记录", tags = {"抢修记录" },  notes = "更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> update(@PathVariable("emeqmaintance_id") String emeqmaintance_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
		EMEQMaintance domain  = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain .setEmeqmaintanceid(emeqmaintance_id);
		emeqmaintanceService.update(domain );
		EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByEntities(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos)),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "批量更新抢修记录", tags = {"抢修记录" },  notes = "批量更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqmaintances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        emeqmaintanceService.updateBatch(emeqmaintanceMapping.toDomain(emeqmaintancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "删除抢修记录", tags = {"抢修记录" },  notes = "删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqmaintance_id") String emeqmaintance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.remove(emeqmaintance_id));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByIds(#ids),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "批量删除抢修记录", tags = {"抢修记录" },  notes = "批量删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqmaintances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqmaintanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(returnObject.body),'eam-EMEQMaintance-Get')")
    @ApiOperation(value = "获取抢修记录", tags = {"抢修记录" },  notes = "获取抢修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> get(@PathVariable("emeqmaintance_id") String emeqmaintance_id) {
        EMEQMaintance domain = emeqmaintanceService.get(emeqmaintance_id);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取抢修记录草稿", tags = {"抢修记录" },  notes = "获取抢修记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqmaintances/getdraft")
    public ResponseEntity<EMEQMaintanceDTO> getDraft(EMEQMaintanceDTO dto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(emeqmaintanceService.getDraft(domain)));
    }

    @ApiOperation(value = "检查抢修记录", tags = {"抢修记录" },  notes = "检查抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmaintances/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.checkKey(emeqmaintanceMapping.toDomain(emeqmaintancedto)));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "保存抢修记录", tags = {"抢修记录" },  notes = "保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmaintances/save")
    public ResponseEntity<EMEQMaintanceDTO> save(@RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        emeqmaintanceService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "批量保存抢修记录", tags = {"抢修记录" },  notes = "批量保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqmaintances/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        emeqmaintanceService.saveBatch(emeqmaintanceMapping.toDomain(emeqmaintancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"抢修记录" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emeqmaintances/fetchcalendar")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchCalendar(EMEQMaintanceSearchContext context) {
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"抢修记录" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emeqmaintances/searchcalendar")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchCalendar(@RequestBody EMEQMaintanceSearchContext context) {
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"抢修记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqmaintances/fetchdefault")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchDefault(EMEQMaintanceSearchContext context) {
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"抢修记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqmaintances/searchdefault")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchDefault(@RequestBody EMEQMaintanceSearchContext context) {
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "根据设备档案建立抢修记录", tags = {"抢修记录" },  notes = "根据设备档案建立抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqmaintances")
    public ResponseEntity<EMEQMaintanceDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
		emeqmaintanceService.create(domain);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "根据设备档案批量建立抢修记录", tags = {"抢修记录" },  notes = "根据设备档案批量建立抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmaintance" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "根据设备档案更新抢修记录", tags = {"抢修记录" },  notes = "根据设备档案更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
        domain.setEmeqmaintanceid(emeqmaintance_id);
		emeqmaintanceService.update(domain);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByEntities(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos)),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "根据设备档案批量更新抢修记录", tags = {"抢修记录" },  notes = "根据设备档案批量更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "根据设备档案删除抢修记录", tags = {"抢修记录" },  notes = "根据设备档案删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.remove(emeqmaintance_id));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByIds(#ids),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "根据设备档案批量删除抢修记录", tags = {"抢修记录" },  notes = "根据设备档案批量删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqmaintanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(returnObject.body),'eam-EMEQMaintance-Get')")
    @ApiOperation(value = "根据设备档案获取抢修记录", tags = {"抢修记录" },  notes = "根据设备档案获取抢修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id) {
        EMEQMaintance domain = emeqmaintanceService.get(emeqmaintance_id);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取抢修记录草稿", tags = {"抢修记录" },  notes = "根据设备档案获取抢修记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqmaintances/getdraft")
    public ResponseEntity<EMEQMaintanceDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQMaintanceDTO dto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(emeqmaintanceService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查抢修记录", tags = {"抢修记录" },  notes = "根据设备档案检查抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqmaintances/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.checkKey(emeqmaintanceMapping.toDomain(emeqmaintancedto)));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "根据设备档案保存抢修记录", tags = {"抢修记录" },  notes = "根据设备档案保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqmaintances/save")
    public ResponseEntity<EMEQMaintanceDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
        emeqmaintanceService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "根据设备档案批量保存抢修记录", tags = {"抢修记录" },  notes = "根据设备档案批量保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqmaintances/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"抢修记录" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqmaintances/fetchcalendar")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchEMEQMaintanceCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"抢修记录" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqmaintances/searchcalendar")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchEMEQMaintanceCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"抢修记录" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqmaintances/fetchdefault")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchEMEQMaintanceDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"抢修记录" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqmaintances/searchdefault")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchEMEQMaintanceDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "根据班组设备档案建立抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案建立抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances")
    public ResponseEntity<EMEQMaintanceDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
		emeqmaintanceService.create(domain);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案批量建立抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqmaintance" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "根据班组设备档案更新抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
        domain.setEmeqmaintanceid(emeqmaintance_id);
		emeqmaintanceService.update(domain);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByEntities(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos)),'eam-EMEQMaintance-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案批量更新抢修记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.get(#emeqmaintance_id),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "根据班组设备档案删除抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.remove(emeqmaintance_id));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceService.getEmeqmaintanceByIds(#ids),'eam-EMEQMaintance-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案批量删除抢修记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqmaintanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(returnObject.body),'eam-EMEQMaintance-Get')")
    @ApiOperation(value = "根据班组设备档案获取抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案获取抢修记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/{emeqmaintance_id}")
    public ResponseEntity<EMEQMaintanceDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqmaintance_id") String emeqmaintance_id) {
        EMEQMaintance domain = emeqmaintanceService.get(emeqmaintance_id);
        EMEQMaintanceDTO dto = emeqmaintanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取抢修记录草稿", tags = {"抢修记录" },  notes = "根据班组设备档案获取抢修记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/getdraft")
    public ResponseEntity<EMEQMaintanceDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQMaintanceDTO dto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(emeqmaintanceService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案检查抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceService.checkKey(emeqmaintanceMapping.toDomain(emeqmaintancedto)));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedto),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "根据班组设备档案保存抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/save")
    public ResponseEntity<EMEQMaintanceDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceDTO emeqmaintancedto) {
        EMEQMaintance domain = emeqmaintanceMapping.toDomain(emeqmaintancedto);
        domain.setEquipid(emequip_id);
        emeqmaintanceService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqmaintanceMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqmaintanceMapping.toDomain(#emeqmaintancedtos),'eam-EMEQMaintance-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存抢修记录", tags = {"抢修记录" },  notes = "根据班组设备档案批量保存抢修记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQMaintanceDTO> emeqmaintancedtos) {
        List<EMEQMaintance> domainlist=emeqmaintanceMapping.toDomain(emeqmaintancedtos);
        for(EMEQMaintance domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqmaintanceService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"抢修记录" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/fetchcalendar")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchEMEQMaintanceCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchCalendar-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"抢修记录" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/searchcalendar")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchEMEQMaintanceCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"抢修记录" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/fetchdefault")
	public ResponseEntity<List<EMEQMaintanceDTO>> fetchEMEQMaintanceDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
        List<EMEQMaintanceDTO> list = emeqmaintanceMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQMaintance-searchDefault-all') and hasPermission(#context,'eam-EMEQMaintance-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"抢修记录" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqmaintances/searchdefault")
	public ResponseEntity<Page<EMEQMaintanceDTO>> searchEMEQMaintanceDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQMaintanceSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQMaintance> domains = emeqmaintanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqmaintanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

