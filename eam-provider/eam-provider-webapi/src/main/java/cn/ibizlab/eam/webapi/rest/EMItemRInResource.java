package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemRIn;
import cn.ibizlab.eam.core.eam_core.service.IEMItemRInService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemRInSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"入库单" })
@RestController("WebApi-emitemrin")
@RequestMapping("")
public class EMItemRInResource {

    @Autowired
    public IEMItemRInService emitemrinService;

    @Autowired
    @Lazy
    public EMItemRInMapping emitemrinMapping;

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "新建入库单", tags = {"入库单" },  notes = "新建入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins")
    public ResponseEntity<EMItemRInDTO> create(@Validated @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "批量新建入库单", tags = {"入库单" },  notes = "批量新建入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemRInDTO> emitemrindtos) {
        emitemrinService.createBatch(emitemrinMapping.toDomain(emitemrindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "更新入库单", tags = {"入库单" },  notes = "更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> update(@PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
		EMItemRIn domain  = emitemrinMapping.toDomain(emitemrindto);
        domain .setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain );
		EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "批量更新入库单", tags = {"入库单" },  notes = "批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemRInDTO> emitemrindtos) {
        emitemrinService.updateBatch(emitemrinMapping.toDomain(emitemrindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "删除入库单", tags = {"入库单" },  notes = "删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemrin_id") String emitemrin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "批量删除入库单", tags = {"入库单" },  notes = "批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "获取入库单", tags = {"入库单" },  notes = "获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> get(@PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取入库单草稿", tags = {"入库单" },  notes = "获取入库单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraft(EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "检查入库单", tags = {"入库单" },  notes = "检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "确认", tags = {"入库单" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirm(@PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setEmitemrinid(emitemrin_id);
        domain = emitemrinService.confirm(domain);
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "批量处理[确认]", tags = {"入库单" },  notes = "批量处理[确认]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "保存入库单", tags = {"入库单" },  notes = "保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> save(@RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "批量保存入库单", tags = {"入库单" },  notes = "批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemRInDTO> emitemrindtos) {
        emitemrinService.saveBatch(emitemrinMapping.toDomain(emitemrindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "提交", tags = {"入库单" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submit(@PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setEmitemrinid(emitemrin_id);
        domain = emitemrinService.submit(domain);
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"入库单" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchDefault(EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"入库单" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchDefault(@RequestBody EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "获取已入库", tags = {"入库单" } ,notes = "获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchPutIn(EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "查询已入库", tags = {"入库单" } ,notes = "查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchPutIn(@RequestBody EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "获取待入库", tags = {"入库单" } ,notes = "获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchWaitIn(EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "查询待入库", tags = {"入库单" } ,notes = "查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchWaitIn(@RequestBody EMItemRInSearchContext context) {
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据物品建立入库单", tags = {"入库单" },  notes = "根据物品建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins")
    public ResponseEntity<EMItemRInDTO> createByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据物品批量建立入库单", tags = {"入库单" },  notes = "根据物品批量建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> createBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据物品更新入库单", tags = {"入库单" },  notes = "根据物品更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> updateByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain.setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据物品批量更新入库单", tags = {"入库单" },  notes = "根据物品批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据物品删除入库单", tags = {"入库单" },  notes = "根据物品删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> removeByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据物品批量删除入库单", tags = {"入库单" },  notes = "根据物品批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatchByEMItem(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "根据物品获取入库单", tags = {"入库单" },  notes = "根据物品获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> getByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据物品获取入库单草稿", tags = {"入库单" },  notes = "根据物品获取入库单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraftByEMItem(@PathVariable("emitem_id") String emitem_id, EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "根据物品检查入库单", tags = {"入库单" },  notes = "根据物品检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "根据物品入库单", tags = {"入库单" },  notes = "根据物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.confirm(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @ApiOperation(value = "批量处理[根据物品入库单]", tags = {"入库单" },  notes = "批量处理[根据物品入库单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据物品保存入库单", tags = {"入库单" },  notes = "根据物品保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> saveByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据物品批量保存入库单", tags = {"入库单" },  notes = "根据物品批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemrinService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "根据物品入库单", tags = {"入库单" },  notes = "根据物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/{emitem_id}/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submitByEMItem(@PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.submit(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品获取DEFAULT", tags = {"入库单" } ,notes = "根据物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInDefaultByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品查询DEFAULT", tags = {"入库单" } ,notes = "根据物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInDefaultByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品获取已入库", tags = {"入库单" } ,notes = "根据物品获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInPutInByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品查询已入库", tags = {"入库单" } ,notes = "根据物品查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInPutInByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品获取待入库", tags = {"入库单" } ,notes = "根据物品获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/{emitem_id}/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInWaitInByEMItem(@PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据物品查询待入库", tags = {"入库单" } ,notes = "根据物品查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/{emitem_id}/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInWaitInByEMItem(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据服务商物品建立入库单", tags = {"入库单" },  notes = "根据服务商物品建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins")
    public ResponseEntity<EMItemRInDTO> createByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据服务商物品批量建立入库单", tags = {"入库单" },  notes = "根据服务商物品批量建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> createBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据服务商物品更新入库单", tags = {"入库单" },  notes = "根据服务商物品更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> updateByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain.setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据服务商物品批量更新入库单", tags = {"入库单" },  notes = "根据服务商物品批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据服务商物品删除入库单", tags = {"入库单" },  notes = "根据服务商物品删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> removeByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据服务商物品批量删除入库单", tags = {"入库单" },  notes = "根据服务商物品批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatchByEMServiceEMItem(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "根据服务商物品获取入库单", tags = {"入库单" },  notes = "根据服务商物品获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> getByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商物品获取入库单草稿", tags = {"入库单" },  notes = "根据服务商物品获取入库单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraftByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商物品检查入库单", tags = {"入库单" },  notes = "根据服务商物品检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "根据服务商物品入库单", tags = {"入库单" },  notes = "根据服务商物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.confirm(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @ApiOperation(value = "批量处理[根据服务商物品入库单]", tags = {"入库单" },  notes = "批量处理[根据服务商物品入库单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据服务商物品保存入库单", tags = {"入库单" },  notes = "根据服务商物品保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> saveByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据服务商物品批量保存入库单", tags = {"入库单" },  notes = "根据服务商物品批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemrinService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "根据服务商物品入库单", tags = {"入库单" },  notes = "根据服务商物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submitByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.submit(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品获取DEFAULT", tags = {"入库单" } ,notes = "根据服务商物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品查询DEFAULT", tags = {"入库单" } ,notes = "根据服务商物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInDefaultByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品获取已入库", tags = {"入库单" } ,notes = "根据服务商物品获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInPutInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品查询已入库", tags = {"入库单" } ,notes = "根据服务商物品查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInPutInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品获取待入库", tags = {"入库单" } ,notes = "根据服务商物品获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInWaitInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据服务商物品查询待入库", tags = {"入库单" } ,notes = "根据服务商物品查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/{emitem_id}/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInWaitInByEMServiceEMItem(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库库位物品建立入库单", tags = {"入库单" },  notes = "根据仓库库位物品建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins")
    public ResponseEntity<EMItemRInDTO> createByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库库位物品批量建立入库单", tags = {"入库单" },  notes = "根据仓库库位物品批量建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库库位物品更新入库单", tags = {"入库单" },  notes = "根据仓库库位物品更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> updateByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain.setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库库位物品批量更新入库单", tags = {"入库单" },  notes = "根据仓库库位物品批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库库位物品删除入库单", tags = {"入库单" },  notes = "根据仓库库位物品删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> removeByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库库位物品批量删除入库单", tags = {"入库单" },  notes = "根据仓库库位物品批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "根据仓库库位物品获取入库单", tags = {"入库单" },  notes = "根据仓库库位物品获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> getByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位物品获取入库单草稿", tags = {"入库单" },  notes = "根据仓库库位物品获取入库单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraftByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位物品检查入库单", tags = {"入库单" },  notes = "根据仓库库位物品检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "根据仓库库位物品入库单", tags = {"入库单" },  notes = "根据仓库库位物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.confirm(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @ApiOperation(value = "批量处理[根据仓库库位物品入库单]", tags = {"入库单" },  notes = "批量处理[根据仓库库位物品入库单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库库位物品保存入库单", tags = {"入库单" },  notes = "根据仓库库位物品保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> saveByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库库位物品批量保存入库单", tags = {"入库单" },  notes = "根据仓库库位物品批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemrinService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "根据仓库库位物品入库单", tags = {"入库单" },  notes = "根据仓库库位物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submitByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.submit(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取DEFAULT", tags = {"入库单" } ,notes = "根据仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询DEFAULT", tags = {"入库单" } ,notes = "根据仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInDefaultByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取已入库", tags = {"入库单" } ,notes = "根据仓库库位物品获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInPutInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询已入库", tags = {"入库单" } ,notes = "根据仓库库位物品查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInPutInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品获取待入库", tags = {"入库单" } ,notes = "根据仓库库位物品获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInWaitInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库库位物品查询待入库", tags = {"入库单" } ,notes = "根据仓库库位物品查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInWaitInByEMStorePartEMItem(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库物品建立入库单", tags = {"入库单" },  notes = "根据仓库物品建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins")
    public ResponseEntity<EMItemRInDTO> createByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库物品批量建立入库单", tags = {"入库单" },  notes = "根据仓库物品批量建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库物品更新入库单", tags = {"入库单" },  notes = "根据仓库物品更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> updateByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain.setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库物品批量更新入库单", tags = {"入库单" },  notes = "根据仓库物品批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库物品删除入库单", tags = {"入库单" },  notes = "根据仓库物品删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库物品批量删除入库单", tags = {"入库单" },  notes = "根据仓库物品批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMItem(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "根据仓库物品获取入库单", tags = {"入库单" },  notes = "根据仓库物品获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> getByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库物品获取入库单草稿", tags = {"入库单" },  notes = "根据仓库物品获取入库单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraftByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库物品检查入库单", tags = {"入库单" },  notes = "根据仓库物品检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "根据仓库物品入库单", tags = {"入库单" },  notes = "根据仓库物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.confirm(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @ApiOperation(value = "批量处理[根据仓库物品入库单]", tags = {"入库单" },  notes = "批量处理[根据仓库物品入库单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库物品保存入库单", tags = {"入库单" },  notes = "根据仓库物品保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> saveByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库物品批量保存入库单", tags = {"入库单" },  notes = "根据仓库物品批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemrinService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "根据仓库物品入库单", tags = {"入库单" },  notes = "根据仓库物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submitByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.submit(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品获取DEFAULT", tags = {"入库单" } ,notes = "根据仓库物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品查询DEFAULT", tags = {"入库单" } ,notes = "根据仓库物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInDefaultByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品获取已入库", tags = {"入库单" } ,notes = "根据仓库物品获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInPutInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品查询已入库", tags = {"入库单" } ,notes = "根据仓库物品查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInPutInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品获取待入库", tags = {"入库单" } ,notes = "根据仓库物品获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInWaitInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库物品查询待入库", tags = {"入库单" } ,notes = "根据仓库物品查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/{emitem_id}/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInWaitInByEMStoreEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品建立入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins")
    public ResponseEntity<EMItemRInDTO> createByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
		emitemrinService.create(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Create')")
    @ApiOperation(value = "根据仓库仓库库位物品批量建立入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品批量建立入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemrin" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品更新入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> updateByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain.setEmitemrinid(emitemrin_id);
		emitemrinService.update(domain);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByEntities(this.emitemrinMapping.toDomain(#emitemrindtos)),'eam-EMItemRIn-Update')")
    @ApiOperation(value = "根据仓库仓库库位物品批量更新入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品批量更新入库单")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
            domain.setItemid(emitem_id);
        }
        emitemrinService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemrinService.get(#emitemrin_id),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品删除入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemrinService.remove(emitemrin_id));
    }

    @PreAuthorize("hasPermission(this.emitemrinService.getEmitemrinByIds(#ids),'eam-EMItemRIn-Remove')")
    @ApiOperation(value = "根据仓库仓库库位物品批量删除入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品批量删除入库单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePartEMItem(@RequestBody List<String> ids) {
        emitemrinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemrinMapping.toDomain(returnObject.body),'eam-EMItemRIn-Get')")
    @ApiOperation(value = "根据仓库仓库库位物品获取入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品获取入库单")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}")
    public ResponseEntity<EMItemRInDTO> getByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id) {
        EMItemRIn domain = emitemrinService.get(emitemrin_id);
        EMItemRInDTO dto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位物品获取入库单草稿", tags = {"入库单" },  notes = "根据仓库仓库库位物品获取入库单草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/getdraft")
    public ResponseEntity<EMItemRInDTO> getDraftByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, EMItemRInDTO dto) {
        EMItemRIn domain = emitemrinMapping.toDomain(dto);
        domain.setItemid(emitem_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(emitemrinService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位物品检查入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品检查入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemrinService.checkKey(emitemrinMapping.toDomain(emitemrindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Confirm-all')")
    @ApiOperation(value = "根据仓库仓库库位物品入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/confirm")
    public ResponseEntity<EMItemRInDTO> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.confirm(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @ApiOperation(value = "批量处理[根据仓库仓库库位物品入库单]", tags = {"入库单" },  notes = "批量处理[根据仓库仓库库位物品入库单]")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domains = emitemrinMapping.toDomain(emitemrindtos);
        boolean result = emitemrinService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindto),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品保存入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/save")
    public ResponseEntity<EMItemRInDTO> saveByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        emitemrinService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrinMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemrinMapping.toDomain(#emitemrindtos),'eam-EMItemRIn-Save')")
    @ApiOperation(value = "根据仓库仓库库位物品批量保存入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品批量保存入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody List<EMItemRInDTO> emitemrindtos) {
        List<EMItemRIn> domainlist=emitemrinMapping.toDomain(emitemrindtos);
        for(EMItemRIn domain:domainlist){
             domain.setItemid(emitem_id);
        }
        emitemrinService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-Submit-all')")
    @ApiOperation(value = "根据仓库仓库库位物品入库单", tags = {"入库单" },  notes = "根据仓库仓库库位物品入库单")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/{emitemrin_id}/submit")
    public ResponseEntity<EMItemRInDTO> submitByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @PathVariable("emitemrin_id") String emitemrin_id, @RequestBody EMItemRInDTO emitemrindto) {
        EMItemRIn domain = emitemrinMapping.toDomain(emitemrindto);
        domain.setItemid(emitem_id);
        domain = emitemrinService.submit(domain) ;
        emitemrindto = emitemrinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemrindto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取DEFAULT", tags = {"入库单" } ,notes = "根据仓库仓库库位物品获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchdefault")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchDefault-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询DEFAULT", tags = {"入库单" } ,notes = "根据仓库仓库库位物品查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchdefault")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInDefaultByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取已入库", tags = {"入库单" } ,notes = "根据仓库仓库库位物品获取已入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchputin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInPutInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchPutIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询已入库", tags = {"入库单" } ,notes = "根据仓库仓库库位物品查询已入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchputin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInPutInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchPutIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品获取待入库", tags = {"入库单" } ,notes = "根据仓库仓库库位物品获取待入库")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/fetchwaitin")
	public ResponseEntity<List<EMItemRInDTO>> fetchEMItemRInWaitInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id,EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
        List<EMItemRInDTO> list = emitemrinMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemRIn-searchWaitIn-all') and hasPermission(#context,'eam-EMItemRIn-Get')")
	@ApiOperation(value = "根据仓库仓库库位物品查询待入库", tags = {"入库单" } ,notes = "根据仓库仓库库位物品查询待入库")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}/emitemrins/searchwaitin")
	public ResponseEntity<Page<EMItemRInDTO>> searchEMItemRInWaitInByEMStoreEMStorePartEMItem(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemRInSearchContext context) {
        context.setN_itemid_eq(emitem_id);
        Page<EMItemRIn> domains = emitemrinService.searchWaitIn(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemrinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

