package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSTopMoni;
import cn.ibizlab.eam.webapi.dto.EMEQSTopMoniDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMEQSTopMoniMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMEQSTopMoniMapping extends MappingBase<EMEQSTopMoniDTO, EMEQSTopMoni> {


}

