package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMSysCtrl;
import cn.ibizlab.eam.core.eam_core.service.IEMSysCtrlService;
import cn.ibizlab.eam.core.eam_core.filter.EMSysCtrlSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"系统维护" })
@RestController("WebApi-emsysctrl")
@RequestMapping("")
public class EMSysCtrlResource {

    @Autowired
    public IEMSysCtrlService emsysctrlService;

    @Autowired
    @Lazy
    public EMSysCtrlMapping emsysctrlMapping;

    @PreAuthorize("hasPermission(this.emsysctrlMapping.toDomain(#emsysctrldto),'eam-EMSysCtrl-Create')")
    @ApiOperation(value = "新建系统维护", tags = {"系统维护" },  notes = "新建系统维护")
	@RequestMapping(method = RequestMethod.POST, value = "/emsysctrls")
    public ResponseEntity<EMSysCtrlDTO> create(@Validated @RequestBody EMSysCtrlDTO emsysctrldto) {
        EMSysCtrl domain = emsysctrlMapping.toDomain(emsysctrldto);
		emsysctrlService.create(domain);
        EMSysCtrlDTO dto = emsysctrlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emsysctrlMapping.toDomain(#emsysctrldtos),'eam-EMSysCtrl-Create')")
    @ApiOperation(value = "批量新建系统维护", tags = {"系统维护" },  notes = "批量新建系统维护")
	@RequestMapping(method = RequestMethod.POST, value = "/emsysctrls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMSysCtrlDTO> emsysctrldtos) {
        emsysctrlService.createBatch(emsysctrlMapping.toDomain(emsysctrldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emsysctrl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emsysctrlService.get(#emsysctrl_id),'eam-EMSysCtrl-Update')")
    @ApiOperation(value = "更新系统维护", tags = {"系统维护" },  notes = "更新系统维护")
	@RequestMapping(method = RequestMethod.PUT, value = "/emsysctrls/{emsysctrl_id}")
    public ResponseEntity<EMSysCtrlDTO> update(@PathVariable("emsysctrl_id") String emsysctrl_id, @RequestBody EMSysCtrlDTO emsysctrldto) {
		EMSysCtrl domain  = emsysctrlMapping.toDomain(emsysctrldto);
        domain .setEmsysctrlid(emsysctrl_id);
		emsysctrlService.update(domain );
		EMSysCtrlDTO dto = emsysctrlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emsysctrlService.getEmsysctrlByEntities(this.emsysctrlMapping.toDomain(#emsysctrldtos)),'eam-EMSysCtrl-Update')")
    @ApiOperation(value = "批量更新系统维护", tags = {"系统维护" },  notes = "批量更新系统维护")
	@RequestMapping(method = RequestMethod.PUT, value = "/emsysctrls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMSysCtrlDTO> emsysctrldtos) {
        emsysctrlService.updateBatch(emsysctrlMapping.toDomain(emsysctrldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emsysctrlService.get(#emsysctrl_id),'eam-EMSysCtrl-Remove')")
    @ApiOperation(value = "删除系统维护", tags = {"系统维护" },  notes = "删除系统维护")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emsysctrls/{emsysctrl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emsysctrl_id") String emsysctrl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emsysctrlService.remove(emsysctrl_id));
    }

    @PreAuthorize("hasPermission(this.emsysctrlService.getEmsysctrlByIds(#ids),'eam-EMSysCtrl-Remove')")
    @ApiOperation(value = "批量删除系统维护", tags = {"系统维护" },  notes = "批量删除系统维护")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emsysctrls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emsysctrlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emsysctrlMapping.toDomain(returnObject.body),'eam-EMSysCtrl-Get')")
    @ApiOperation(value = "获取系统维护", tags = {"系统维护" },  notes = "获取系统维护")
	@RequestMapping(method = RequestMethod.GET, value = "/emsysctrls/{emsysctrl_id}")
    public ResponseEntity<EMSysCtrlDTO> get(@PathVariable("emsysctrl_id") String emsysctrl_id) {
        EMSysCtrl domain = emsysctrlService.get(emsysctrl_id);
        EMSysCtrlDTO dto = emsysctrlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取系统维护草稿", tags = {"系统维护" },  notes = "获取系统维护草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emsysctrls/getdraft")
    public ResponseEntity<EMSysCtrlDTO> getDraft(EMSysCtrlDTO dto) {
        EMSysCtrl domain = emsysctrlMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emsysctrlMapping.toDto(emsysctrlService.getDraft(domain)));
    }

    @ApiOperation(value = "检查系统维护", tags = {"系统维护" },  notes = "检查系统维护")
	@RequestMapping(method = RequestMethod.POST, value = "/emsysctrls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMSysCtrlDTO emsysctrldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emsysctrlService.checkKey(emsysctrlMapping.toDomain(emsysctrldto)));
    }

    @PreAuthorize("hasPermission(this.emsysctrlMapping.toDomain(#emsysctrldto),'eam-EMSysCtrl-Save')")
    @ApiOperation(value = "保存系统维护", tags = {"系统维护" },  notes = "保存系统维护")
	@RequestMapping(method = RequestMethod.POST, value = "/emsysctrls/save")
    public ResponseEntity<EMSysCtrlDTO> save(@RequestBody EMSysCtrlDTO emsysctrldto) {
        EMSysCtrl domain = emsysctrlMapping.toDomain(emsysctrldto);
        emsysctrlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emsysctrlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emsysctrlMapping.toDomain(#emsysctrldtos),'eam-EMSysCtrl-Save')")
    @ApiOperation(value = "批量保存系统维护", tags = {"系统维护" },  notes = "批量保存系统维护")
	@RequestMapping(method = RequestMethod.POST, value = "/emsysctrls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMSysCtrlDTO> emsysctrldtos) {
        emsysctrlService.saveBatch(emsysctrlMapping.toDomain(emsysctrldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMSysCtrl-searchDefault-all') and hasPermission(#context,'eam-EMSysCtrl-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"系统维护" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emsysctrls/fetchdefault")
	public ResponseEntity<List<EMSysCtrlDTO>> fetchDefault(EMSysCtrlSearchContext context) {
        Page<EMSysCtrl> domains = emsysctrlService.searchDefault(context) ;
        List<EMSysCtrlDTO> list = emsysctrlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMSysCtrl-searchDefault-all') and hasPermission(#context,'eam-EMSysCtrl-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"系统维护" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emsysctrls/searchdefault")
	public ResponseEntity<Page<EMSysCtrlDTO>> searchDefault(@RequestBody EMSysCtrlSearchContext context) {
        Page<EMSysCtrl> domains = emsysctrlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emsysctrlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

