package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMBidding;
import cn.ibizlab.eam.core.eam_core.service.IEMBiddingService;
import cn.ibizlab.eam.core.eam_core.filter.EMBiddingSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"项目招投标" })
@RestController("WebApi-embidding")
@RequestMapping("")
public class EMBiddingResource {

    @Autowired
    public IEMBiddingService embiddingService;

    @Autowired
    @Lazy
    public EMBiddingMapping embiddingMapping;

    @PreAuthorize("hasPermission(this.embiddingMapping.toDomain(#embiddingdto),'eam-EMBidding-Create')")
    @ApiOperation(value = "新建项目招投标", tags = {"项目招投标" },  notes = "新建项目招投标")
	@RequestMapping(method = RequestMethod.POST, value = "/embiddings")
    public ResponseEntity<EMBiddingDTO> create(@Validated @RequestBody EMBiddingDTO embiddingdto) {
        EMBidding domain = embiddingMapping.toDomain(embiddingdto);
		embiddingService.create(domain);
        EMBiddingDTO dto = embiddingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embiddingMapping.toDomain(#embiddingdtos),'eam-EMBidding-Create')")
    @ApiOperation(value = "批量新建项目招投标", tags = {"项目招投标" },  notes = "批量新建项目招投标")
	@RequestMapping(method = RequestMethod.POST, value = "/embiddings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMBiddingDTO> embiddingdtos) {
        embiddingService.createBatch(embiddingMapping.toDomain(embiddingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "embidding" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.embiddingService.get(#embidding_id),'eam-EMBidding-Update')")
    @ApiOperation(value = "更新项目招投标", tags = {"项目招投标" },  notes = "更新项目招投标")
	@RequestMapping(method = RequestMethod.PUT, value = "/embiddings/{embidding_id}")
    public ResponseEntity<EMBiddingDTO> update(@PathVariable("embidding_id") String embidding_id, @RequestBody EMBiddingDTO embiddingdto) {
		EMBidding domain  = embiddingMapping.toDomain(embiddingdto);
        domain .setEmbiddingid(embidding_id);
		embiddingService.update(domain );
		EMBiddingDTO dto = embiddingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.embiddingService.getEmbiddingByEntities(this.embiddingMapping.toDomain(#embiddingdtos)),'eam-EMBidding-Update')")
    @ApiOperation(value = "批量更新项目招投标", tags = {"项目招投标" },  notes = "批量更新项目招投标")
	@RequestMapping(method = RequestMethod.PUT, value = "/embiddings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMBiddingDTO> embiddingdtos) {
        embiddingService.updateBatch(embiddingMapping.toDomain(embiddingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.embiddingService.get(#embidding_id),'eam-EMBidding-Remove')")
    @ApiOperation(value = "删除项目招投标", tags = {"项目招投标" },  notes = "删除项目招投标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embiddings/{embidding_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("embidding_id") String embidding_id) {
         return ResponseEntity.status(HttpStatus.OK).body(embiddingService.remove(embidding_id));
    }

    @PreAuthorize("hasPermission(this.embiddingService.getEmbiddingByIds(#ids),'eam-EMBidding-Remove')")
    @ApiOperation(value = "批量删除项目招投标", tags = {"项目招投标" },  notes = "批量删除项目招投标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/embiddings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        embiddingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.embiddingMapping.toDomain(returnObject.body),'eam-EMBidding-Get')")
    @ApiOperation(value = "获取项目招投标", tags = {"项目招投标" },  notes = "获取项目招投标")
	@RequestMapping(method = RequestMethod.GET, value = "/embiddings/{embidding_id}")
    public ResponseEntity<EMBiddingDTO> get(@PathVariable("embidding_id") String embidding_id) {
        EMBidding domain = embiddingService.get(embidding_id);
        EMBiddingDTO dto = embiddingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取项目招投标草稿", tags = {"项目招投标" },  notes = "获取项目招投标草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/embiddings/getdraft")
    public ResponseEntity<EMBiddingDTO> getDraft(EMBiddingDTO dto) {
        EMBidding domain = embiddingMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(embiddingMapping.toDto(embiddingService.getDraft(domain)));
    }

    @ApiOperation(value = "检查项目招投标", tags = {"项目招投标" },  notes = "检查项目招投标")
	@RequestMapping(method = RequestMethod.POST, value = "/embiddings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMBiddingDTO embiddingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(embiddingService.checkKey(embiddingMapping.toDomain(embiddingdto)));
    }

    @PreAuthorize("hasPermission(this.embiddingMapping.toDomain(#embiddingdto),'eam-EMBidding-Save')")
    @ApiOperation(value = "保存项目招投标", tags = {"项目招投标" },  notes = "保存项目招投标")
	@RequestMapping(method = RequestMethod.POST, value = "/embiddings/save")
    public ResponseEntity<EMBiddingDTO> save(@RequestBody EMBiddingDTO embiddingdto) {
        EMBidding domain = embiddingMapping.toDomain(embiddingdto);
        embiddingService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(embiddingMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.embiddingMapping.toDomain(#embiddingdtos),'eam-EMBidding-Save')")
    @ApiOperation(value = "批量保存项目招投标", tags = {"项目招投标" },  notes = "批量保存项目招投标")
	@RequestMapping(method = RequestMethod.POST, value = "/embiddings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMBiddingDTO> embiddingdtos) {
        embiddingService.saveBatch(embiddingMapping.toDomain(embiddingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBidding-searchDefault-all') and hasPermission(#context,'eam-EMBidding-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"项目招投标" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/embiddings/fetchdefault")
	public ResponseEntity<List<EMBiddingDTO>> fetchDefault(EMBiddingSearchContext context) {
        Page<EMBidding> domains = embiddingService.searchDefault(context) ;
        List<EMBiddingDTO> list = embiddingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBidding-searchDefault-all') and hasPermission(#context,'eam-EMBidding-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"项目招投标" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/embiddings/searchdefault")
	public ResponseEntity<Page<EMBiddingDTO>> searchDefault(@RequestBody EMBiddingSearchContext context) {
        Page<EMBidding> domains = embiddingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(embiddingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

