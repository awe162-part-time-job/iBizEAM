package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQWL;
import cn.ibizlab.eam.core.eam_core.service.IEMEQWLService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQWLSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备运行日志" })
@RestController("WebApi-emeqwl")
@RequestMapping("")
public class EMEQWLResource {

    @Autowired
    public IEMEQWLService emeqwlService;

    @Autowired
    @Lazy
    public EMEQWLMapping emeqwlMapping;

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Create')")
    @ApiOperation(value = "新建设备运行日志", tags = {"设备运行日志" },  notes = "新建设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqwls")
    public ResponseEntity<EMEQWLDTO> create(@Validated @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
		emeqwlService.create(domain);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Create')")
    @ApiOperation(value = "批量新建设备运行日志", tags = {"设备运行日志" },  notes = "批量新建设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqwls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQWLDTO> emeqwldtos) {
        emeqwlService.createBatch(emeqwlMapping.toDomain(emeqwldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqwl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Update')")
    @ApiOperation(value = "更新设备运行日志", tags = {"设备运行日志" },  notes = "更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> update(@PathVariable("emeqwl_id") String emeqwl_id, @RequestBody EMEQWLDTO emeqwldto) {
		EMEQWL domain  = emeqwlMapping.toDomain(emeqwldto);
        domain .setEmeqwlid(emeqwl_id);
		emeqwlService.update(domain );
		EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByEntities(this.emeqwlMapping.toDomain(#emeqwldtos)),'eam-EMEQWL-Update')")
    @ApiOperation(value = "批量更新设备运行日志", tags = {"设备运行日志" },  notes = "批量更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqwls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQWLDTO> emeqwldtos) {
        emeqwlService.updateBatch(emeqwlMapping.toDomain(emeqwldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "删除设备运行日志", tags = {"设备运行日志" },  notes = "删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqwls/{emeqwl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqwl_id") String emeqwl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqwlService.remove(emeqwl_id));
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByIds(#ids),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "批量删除设备运行日志", tags = {"设备运行日志" },  notes = "批量删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqwls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqwlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqwlMapping.toDomain(returnObject.body),'eam-EMEQWL-Get')")
    @ApiOperation(value = "获取设备运行日志", tags = {"设备运行日志" },  notes = "获取设备运行日志")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> get(@PathVariable("emeqwl_id") String emeqwl_id) {
        EMEQWL domain = emeqwlService.get(emeqwl_id);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备运行日志草稿", tags = {"设备运行日志" },  notes = "获取设备运行日志草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqwls/getdraft")
    public ResponseEntity<EMEQWLDTO> getDraft(EMEQWLDTO dto) {
        EMEQWL domain = emeqwlMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(emeqwlService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备运行日志", tags = {"设备运行日志" },  notes = "检查设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqwls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQWLDTO emeqwldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqwlService.checkKey(emeqwlMapping.toDomain(emeqwldto)));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Save')")
    @ApiOperation(value = "保存设备运行日志", tags = {"设备运行日志" },  notes = "保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqwls/save")
    public ResponseEntity<EMEQWLDTO> save(@RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        emeqwlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Save')")
    @ApiOperation(value = "批量保存设备运行日志", tags = {"设备运行日志" },  notes = "批量保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqwls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQWLDTO> emeqwldtos) {
        emeqwlService.saveBatch(emeqwlMapping.toDomain(emeqwldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备运行日志" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqwls/fetchdefault")
	public ResponseEntity<List<EMEQWLDTO>> fetchDefault(EMEQWLSearchContext context) {
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备运行日志" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqwls/searchdefault")
	public ResponseEntity<Page<EMEQWLDTO>> searchDefault(@RequestBody EMEQWLSearchContext context) {
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "获取设备日志-最近30天", tags = {"设备运行日志" } ,notes = "获取设备日志-最近30天")
    @RequestMapping(method= RequestMethod.GET , value="/emeqwls/fetchnearest30daybyeq")
	public ResponseEntity<List<EMEQWLDTO>> fetchNearest30DayByEQ(EMEQWLSearchContext context) {
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "查询设备日志-最近30天", tags = {"设备运行日志" } ,notes = "查询设备日志-最近30天")
    @RequestMapping(method= RequestMethod.POST , value="/emeqwls/searchnearest30daybyeq")
	public ResponseEntity<Page<EMEQWLDTO>> searchNearest30DayByEQ(@RequestBody EMEQWLSearchContext context) {
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Create')")
    @ApiOperation(value = "根据设备档案建立设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案建立设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqwls")
    public ResponseEntity<EMEQWLDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
		emeqwlService.create(domain);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Create')")
    @ApiOperation(value = "根据设备档案批量建立设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案批量建立设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqwlService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqwl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Update')")
    @ApiOperation(value = "根据设备档案更新设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
        domain.setEmeqwlid(emeqwl_id);
		emeqwlService.update(domain);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByEntities(this.emeqwlMapping.toDomain(#emeqwldtos)),'eam-EMEQWL-Update')")
    @ApiOperation(value = "根据设备档案批量更新设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案批量更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqwlService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "根据设备档案删除设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqwlService.remove(emeqwl_id));
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByIds(#ids),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "根据设备档案批量删除设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案批量删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqwlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqwlMapping.toDomain(returnObject.body),'eam-EMEQWL-Get')")
    @ApiOperation(value = "根据设备档案获取设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案获取设备运行日志")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id) {
        EMEQWL domain = emeqwlService.get(emeqwl_id);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取设备运行日志草稿", tags = {"设备运行日志" },  notes = "根据设备档案获取设备运行日志草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqwls/getdraft")
    public ResponseEntity<EMEQWLDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQWLDTO dto) {
        EMEQWL domain = emeqwlMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(emeqwlService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案检查设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqwls/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqwlService.checkKey(emeqwlMapping.toDomain(emeqwldto)));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Save')")
    @ApiOperation(value = "根据设备档案保存设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqwls/save")
    public ResponseEntity<EMEQWLDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
        emeqwlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Save')")
    @ApiOperation(value = "根据设备档案批量保存设备运行日志", tags = {"设备运行日志" },  notes = "根据设备档案批量保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqwls/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqwlService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"设备运行日志" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqwls/fetchdefault")
	public ResponseEntity<List<EMEQWLDTO>> fetchEMEQWLDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"设备运行日志" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqwls/searchdefault")
	public ResponseEntity<Page<EMEQWLDTO>> searchEMEQWLDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据设备档案获取设备日志-最近30天", tags = {"设备运行日志" } ,notes = "根据设备档案获取设备日志-最近30天")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqwls/fetchnearest30daybyeq")
	public ResponseEntity<List<EMEQWLDTO>> fetchEMEQWLNearest30DayByEQByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据设备档案查询设备日志-最近30天", tags = {"设备运行日志" } ,notes = "根据设备档案查询设备日志-最近30天")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqwls/searchnearest30daybyeq")
	public ResponseEntity<Page<EMEQWLDTO>> searchEMEQWLNearest30DayByEQByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Create')")
    @ApiOperation(value = "根据班组设备档案建立设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案建立设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls")
    public ResponseEntity<EMEQWLDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
		emeqwlService.create(domain);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案批量建立设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqwlService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqwl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Update')")
    @ApiOperation(value = "根据班组设备档案更新设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
        domain.setEmeqwlid(emeqwl_id);
		emeqwlService.update(domain);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByEntities(this.emeqwlMapping.toDomain(#emeqwldtos)),'eam-EMEQWL-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案批量更新设备运行日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqwlService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqwlService.get(#emeqwl_id),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "根据班组设备档案删除设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqwlService.remove(emeqwl_id));
    }

    @PreAuthorize("hasPermission(this.emeqwlService.getEmeqwlByIds(#ids),'eam-EMEQWL-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案批量删除设备运行日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqwlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqwlMapping.toDomain(returnObject.body),'eam-EMEQWL-Get')")
    @ApiOperation(value = "根据班组设备档案获取设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案获取设备运行日志")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/{emeqwl_id}")
    public ResponseEntity<EMEQWLDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqwl_id") String emeqwl_id) {
        EMEQWL domain = emeqwlService.get(emeqwl_id);
        EMEQWLDTO dto = emeqwlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取设备运行日志草稿", tags = {"设备运行日志" },  notes = "根据班组设备档案获取设备运行日志草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/getdraft")
    public ResponseEntity<EMEQWLDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQWLDTO dto) {
        EMEQWL domain = emeqwlMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(emeqwlService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案检查设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqwlService.checkKey(emeqwlMapping.toDomain(emeqwldto)));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldto),'eam-EMEQWL-Save')")
    @ApiOperation(value = "根据班组设备档案保存设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/save")
    public ResponseEntity<EMEQWLDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLDTO emeqwldto) {
        EMEQWL domain = emeqwlMapping.toDomain(emeqwldto);
        domain.setEquipid(emequip_id);
        emeqwlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqwlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqwlMapping.toDomain(#emeqwldtos),'eam-EMEQWL-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存设备运行日志", tags = {"设备运行日志" },  notes = "根据班组设备档案批量保存设备运行日志")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQWLDTO> emeqwldtos) {
        List<EMEQWL> domainlist=emeqwlMapping.toDomain(emeqwldtos);
        for(EMEQWL domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqwlService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"设备运行日志" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/fetchdefault")
	public ResponseEntity<List<EMEQWLDTO>> fetchEMEQWLDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchDefault-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"设备运行日志" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/searchdefault")
	public ResponseEntity<Page<EMEQWLDTO>> searchEMEQWLDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据班组设备档案获取设备日志-最近30天", tags = {"设备运行日志" } ,notes = "根据班组设备档案获取设备日志-最近30天")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/fetchnearest30daybyeq")
	public ResponseEntity<List<EMEQWLDTO>> fetchEMEQWLNearest30DayByEQByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
        List<EMEQWLDTO> list = emeqwlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQWL-searchNearest30DayByEQ-all') and hasPermission(#context,'eam-EMEQWL-Get')")
	@ApiOperation(value = "根据班组设备档案查询设备日志-最近30天", tags = {"设备运行日志" } ,notes = "根据班组设备档案查询设备日志-最近30天")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqwls/searchnearest30daybyeq")
	public ResponseEntity<Page<EMEQWLDTO>> searchEMEQWLNearest30DayByEQByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQWLSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQWL> domains = emeqwlService.searchNearest30DayByEQ(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqwlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

