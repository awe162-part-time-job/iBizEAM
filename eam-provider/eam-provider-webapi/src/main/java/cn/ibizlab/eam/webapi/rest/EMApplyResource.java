package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMApply;
import cn.ibizlab.eam.core.eam_core.service.IEMApplyService;
import cn.ibizlab.eam.core.eam_core.filter.EMApplySearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"外委申请" })
@RestController("WebApi-emapply")
@RequestMapping("")
public class EMApplyResource {

    @Autowired
    public IEMApplyService emapplyService;

    @Autowired
    @Lazy
    public EMApplyMapping emapplyMapping;

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Create')")
    @ApiOperation(value = "新建外委申请", tags = {"外委申请" },  notes = "新建外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies")
    public ResponseEntity<EMApplyDTO> create(@Validated @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
		emapplyService.create(domain);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Create')")
    @ApiOperation(value = "批量新建外委申请", tags = {"外委申请" },  notes = "批量新建外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMApplyDTO> emapplydtos) {
        emapplyService.createBatch(emapplyMapping.toDomain(emapplydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emapply" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Update')")
    @ApiOperation(value = "更新外委申请", tags = {"外委申请" },  notes = "更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> update(@PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
		EMApply domain  = emapplyMapping.toDomain(emapplydto);
        domain .setEmapplyid(emapply_id);
		emapplyService.update(domain );
		EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByEntities(this.emapplyMapping.toDomain(#emapplydtos)),'eam-EMApply-Update')")
    @ApiOperation(value = "批量更新外委申请", tags = {"外委申请" },  notes = "批量更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emapplies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMApplyDTO> emapplydtos) {
        emapplyService.updateBatch(emapplyMapping.toDomain(emapplydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Remove')")
    @ApiOperation(value = "删除外委申请", tags = {"外委申请" },  notes = "删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emapplies/{emapply_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emapply_id") String emapply_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emapplyService.remove(emapply_id));
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByIds(#ids),'eam-EMApply-Remove')")
    @ApiOperation(value = "批量删除外委申请", tags = {"外委申请" },  notes = "批量删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emapplies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emapplyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emapplyMapping.toDomain(returnObject.body),'eam-EMApply-Get')")
    @ApiOperation(value = "获取外委申请", tags = {"外委申请" },  notes = "获取外委申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> get(@PathVariable("emapply_id") String emapply_id) {
        EMApply domain = emapplyService.get(emapply_id);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取外委申请草稿", tags = {"外委申请" },  notes = "获取外委申请草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emapplies/getdraft")
    public ResponseEntity<EMApplyDTO> getDraft(EMApplyDTO dto) {
        EMApply domain = emapplyMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(emapplyService.getDraft(domain)));
    }

    @ApiOperation(value = "检查外委申请", tags = {"外委申请" },  notes = "检查外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMApplyDTO emapplydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emapplyService.checkKey(emapplyMapping.toDomain(emapplydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Confirm-all')")
    @ApiOperation(value = "完成", tags = {"外委申请" },  notes = "完成")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/{emapply_id}/confirm")
    public ResponseEntity<EMApplyDTO> confirm(@PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEmapplyid(emapply_id);
        domain = emapplyService.confirm(domain);
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "行为", tags = {"外委申请" },  notes = "行为")
	@RequestMapping(method = RequestMethod.PUT, value = "/emapplies/{emapply_id}/formupdatebyemquipid")
    public ResponseEntity<EMApplyDTO> formUpdateByEmquipId(@PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEmapplyid(emapply_id);
        domain = emapplyService.formUpdateByEmquipId(domain);
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"外委申请" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/{emapply_id}/rejected")
    public ResponseEntity<EMApplyDTO> rejected(@PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEmapplyid(emapply_id);
        domain = emapplyService.rejected(domain);
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Save')")
    @ApiOperation(value = "保存外委申请", tags = {"外委申请" },  notes = "保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/save")
    public ResponseEntity<EMApplyDTO> save(@RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        emapplyService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Save')")
    @ApiOperation(value = "批量保存外委申请", tags = {"外委申请" },  notes = "批量保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMApplyDTO> emapplydtos) {
        emapplyService.saveBatch(emapplyMapping.toDomain(emapplydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Submit-all')")
    @ApiOperation(value = "提交", tags = {"外委申请" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emapplies/{emapply_id}/submit")
    public ResponseEntity<EMApplyDTO> submit(@PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEmapplyid(emapply_id);
        domain = emapplyService.submit(domain);
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "获取已完成", tags = {"外委申请" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emapplies/fetchconfirmed")
	public ResponseEntity<List<EMApplyDTO>> fetchConfirmed(EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "查询已完成", tags = {"外委申请" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emapplies/searchconfirmed")
	public ResponseEntity<Page<EMApplyDTO>> searchConfirmed(@RequestBody EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"外委申请" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emapplies/fetchdefault")
	public ResponseEntity<List<EMApplyDTO>> fetchDefault(EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"外委申请" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emapplies/searchdefault")
	public ResponseEntity<Page<EMApplyDTO>> searchDefault(@RequestBody EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "获取未提交", tags = {"外委申请" } ,notes = "获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emapplies/fetchdraft")
	public ResponseEntity<List<EMApplyDTO>> fetchDraft(EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "查询未提交", tags = {"外委申请" } ,notes = "查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emapplies/searchdraft")
	public ResponseEntity<Page<EMApplyDTO>> searchDraft(@RequestBody EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "获取待完成", tags = {"外委申请" } ,notes = "获取待完成")
    @RequestMapping(method= RequestMethod.GET , value="/emapplies/fetchtoconfirm")
	public ResponseEntity<List<EMApplyDTO>> fetchToConfirm(EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "查询待完成", tags = {"外委申请" } ,notes = "查询待完成")
    @RequestMapping(method= RequestMethod.POST , value="/emapplies/searchtoconfirm")
	public ResponseEntity<Page<EMApplyDTO>> searchToConfirm(@RequestBody EMApplySearchContext context) {
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Create')")
    @ApiOperation(value = "根据设备档案建立外委申请", tags = {"外委申请" },  notes = "根据设备档案建立外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies")
    public ResponseEntity<EMApplyDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
		emapplyService.create(domain);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Create')")
    @ApiOperation(value = "根据设备档案批量建立外委申请", tags = {"外委申请" },  notes = "根据设备档案批量建立外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emapplyService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emapply" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Update')")
    @ApiOperation(value = "根据设备档案更新外委申请", tags = {"外委申请" },  notes = "根据设备档案更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain.setEmapplyid(emapply_id);
		emapplyService.update(domain);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByEntities(this.emapplyMapping.toDomain(#emapplydtos)),'eam-EMApply-Update')")
    @ApiOperation(value = "根据设备档案批量更新外委申请", tags = {"外委申请" },  notes = "根据设备档案批量更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emapplyService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Remove')")
    @ApiOperation(value = "根据设备档案删除外委申请", tags = {"外委申请" },  notes = "根据设备档案删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emapplyService.remove(emapply_id));
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByIds(#ids),'eam-EMApply-Remove')")
    @ApiOperation(value = "根据设备档案批量删除外委申请", tags = {"外委申请" },  notes = "根据设备档案批量删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emapplyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emapplyMapping.toDomain(returnObject.body),'eam-EMApply-Get')")
    @ApiOperation(value = "根据设备档案获取外委申请", tags = {"外委申请" },  notes = "根据设备档案获取外委申请")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id) {
        EMApply domain = emapplyService.get(emapply_id);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取外委申请草稿", tags = {"外委申请" },  notes = "根据设备档案获取外委申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emapplies/getdraft")
    public ResponseEntity<EMApplyDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMApplyDTO dto) {
        EMApply domain = emapplyMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(emapplyService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查外委申请", tags = {"外委申请" },  notes = "根据设备档案检查外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emapplyService.checkKey(emapplyMapping.toDomain(emapplydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Confirm-all')")
    @ApiOperation(value = "根据设备档案外委申请", tags = {"外委申请" },  notes = "根据设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/{emapply_id}/confirm")
    public ResponseEntity<EMApplyDTO> confirmByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.confirm(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "根据设备档案外委申请", tags = {"外委申请" },  notes = "根据设备档案外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emapplies/{emapply_id}/formupdatebyemquipid")
    public ResponseEntity<EMApplyDTO> formUpdateByEmquipIdByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.formUpdateByEmquipId(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Rejected-all')")
    @ApiOperation(value = "根据设备档案外委申请", tags = {"外委申请" },  notes = "根据设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/{emapply_id}/rejected")
    public ResponseEntity<EMApplyDTO> rejectedByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.rejected(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Save')")
    @ApiOperation(value = "根据设备档案保存外委申请", tags = {"外委申请" },  notes = "根据设备档案保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/save")
    public ResponseEntity<EMApplyDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        emapplyService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Save')")
    @ApiOperation(value = "根据设备档案批量保存外委申请", tags = {"外委申请" },  notes = "根据设备档案批量保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emapplyService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Submit-all')")
    @ApiOperation(value = "根据设备档案外委申请", tags = {"外委申请" },  notes = "根据设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emapplies/{emapply_id}/submit")
    public ResponseEntity<EMApplyDTO> submitByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.submit(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案获取已完成", tags = {"外委申请" } ,notes = "根据设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emapplies/fetchconfirmed")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案查询已完成", tags = {"外委申请" } ,notes = "根据设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emapplies/searchconfirmed")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyConfirmedByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"外委申请" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emapplies/fetchdefault")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"外委申请" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emapplies/searchdefault")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案获取未提交", tags = {"外委申请" } ,notes = "根据设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emapplies/fetchdraft")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyDraftByEMEquip(@PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案查询未提交", tags = {"外委申请" } ,notes = "根据设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emapplies/searchdraft")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案获取待完成", tags = {"外委申请" } ,notes = "根据设备档案获取待完成")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emapplies/fetchtoconfirm")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据设备档案查询待完成", tags = {"外委申请" } ,notes = "根据设备档案查询待完成")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emapplies/searchtoconfirm")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyToConfirmByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Create')")
    @ApiOperation(value = "根据班组设备档案建立外委申请", tags = {"外委申请" },  notes = "根据班组设备档案建立外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies")
    public ResponseEntity<EMApplyDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
		emapplyService.create(domain);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立外委申请", tags = {"外委申请" },  notes = "根据班组设备档案批量建立外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emapplyService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emapply" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Update')")
    @ApiOperation(value = "根据班组设备档案更新外委申请", tags = {"外委申请" },  notes = "根据班组设备档案更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain.setEmapplyid(emapply_id);
		emapplyService.update(domain);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByEntities(this.emapplyMapping.toDomain(#emapplydtos)),'eam-EMApply-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新外委申请", tags = {"外委申请" },  notes = "根据班组设备档案批量更新外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emapplyService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emapplyService.get(#emapply_id),'eam-EMApply-Remove')")
    @ApiOperation(value = "根据班组设备档案删除外委申请", tags = {"外委申请" },  notes = "根据班组设备档案删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emapplyService.remove(emapply_id));
    }

    @PreAuthorize("hasPermission(this.emapplyService.getEmapplyByIds(#ids),'eam-EMApply-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除外委申请", tags = {"外委申请" },  notes = "根据班组设备档案批量删除外委申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emapplyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emapplyMapping.toDomain(returnObject.body),'eam-EMApply-Get')")
    @ApiOperation(value = "根据班组设备档案获取外委申请", tags = {"外委申请" },  notes = "根据班组设备档案获取外委申请")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}")
    public ResponseEntity<EMApplyDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id) {
        EMApply domain = emapplyService.get(emapply_id);
        EMApplyDTO dto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取外委申请草稿", tags = {"外委申请" },  notes = "根据班组设备档案获取外委申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/getdraft")
    public ResponseEntity<EMApplyDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMApplyDTO dto) {
        EMApply domain = emapplyMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(emapplyService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查外委申请", tags = {"外委申请" },  notes = "根据班组设备档案检查外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emapplyService.checkKey(emapplyMapping.toDomain(emapplydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Confirm-all')")
    @ApiOperation(value = "根据班组设备档案外委申请", tags = {"外委申请" },  notes = "根据班组设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}/confirm")
    public ResponseEntity<EMApplyDTO> confirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.confirm(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-FormUpdateByEmquipId-all')")
    @ApiOperation(value = "根据班组设备档案外委申请", tags = {"外委申请" },  notes = "根据班组设备档案外委申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}/formupdatebyemquipid")
    public ResponseEntity<EMApplyDTO> formUpdateByEmquipIdByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.formUpdateByEmquipId(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Rejected-all')")
    @ApiOperation(value = "根据班组设备档案外委申请", tags = {"外委申请" },  notes = "根据班组设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}/rejected")
    public ResponseEntity<EMApplyDTO> rejectedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.rejected(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydto),'eam-EMApply-Save')")
    @ApiOperation(value = "根据班组设备档案保存外委申请", tags = {"外委申请" },  notes = "根据班组设备档案保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/save")
    public ResponseEntity<EMApplyDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        emapplyService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplyMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emapplyMapping.toDomain(#emapplydtos),'eam-EMApply-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存外委申请", tags = {"外委申请" },  notes = "根据班组设备档案批量保存外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMApplyDTO> emapplydtos) {
        List<EMApply> domainlist=emapplyMapping.toDomain(emapplydtos);
        for(EMApply domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emapplyService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-Submit-all')")
    @ApiOperation(value = "根据班组设备档案外委申请", tags = {"外委申请" },  notes = "根据班组设备档案外委申请")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/{emapply_id}/submit")
    public ResponseEntity<EMApplyDTO> submitByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emapply_id") String emapply_id, @RequestBody EMApplyDTO emapplydto) {
        EMApply domain = emapplyMapping.toDomain(emapplydto);
        domain.setEquipid(emequip_id);
        domain = emapplyService.submit(domain) ;
        emapplydto = emapplyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emapplydto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案获取已完成", tags = {"外委申请" } ,notes = "根据班组设备档案获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/fetchconfirmed")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchConfirmed-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案查询已完成", tags = {"外委申请" } ,notes = "根据班组设备档案查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/searchconfirmed")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyConfirmedByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"外委申请" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/fetchdefault")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDefault-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"外委申请" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/searchdefault")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案获取未提交", tags = {"外委申请" } ,notes = "根据班组设备档案获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/fetchdraft")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchDraft-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案查询未提交", tags = {"外委申请" } ,notes = "根据班组设备档案查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/searchdraft")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案获取待完成", tags = {"外委申请" } ,notes = "根据班组设备档案获取待完成")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/fetchtoconfirm")
	public ResponseEntity<List<EMApplyDTO>> fetchEMApplyToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
        List<EMApplyDTO> list = emapplyMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMApply-searchToConfirm-all') and hasPermission(#context,'eam-EMApply-Get')")
	@ApiOperation(value = "根据班组设备档案查询待完成", tags = {"外委申请" } ,notes = "根据班组设备档案查询待完成")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emapplies/searchtoconfirm")
	public ResponseEntity<Page<EMApplyDTO>> searchEMApplyToConfirmByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMApplySearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMApply> domains = emapplyService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emapplyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

