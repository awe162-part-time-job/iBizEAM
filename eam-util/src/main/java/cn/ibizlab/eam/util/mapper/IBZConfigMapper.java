package cn.ibizlab.eam.util.mapper;

import cn.ibizlab.eam.util.domain.IBZConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface IBZConfigMapper extends BaseMapper<IBZConfig>{

}